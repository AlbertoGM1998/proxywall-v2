echo -n Aplicando Reglas de Firewall...
## FLUSH de reglas
iptables -F
iptables -X
iptables -Z
iptables -t nat -F
## Establecemos politica por defecto
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
## Empezamos a filtrar
## Nota: eth0 es el interfaz conectado al router y eth1 a la LAN
# El localhost se deja
/sbin/iptables -A INPUT -i lo -j ACCEPT
# Al firewall tenemos acceso desde la red local
iptables -A INPUT -s 10.1.70.0/24 -i eth1 -j ACCEPT

#PERMITIR ICMP
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
iptables -A INPUT -i eth1 -p icmp --icmp-type echo-request -j ACCEPT 
iptables -A INPUT -s 10.1.70.0/24 -p icmp --icmp-type echo-request -j ACCEPT

#iptables -A INPUT -s 192.168.1.0/24 -i eth0 -j DROP
#iptables -A OUTPUT  -o eth0 -j ACCEPT

## Ahora con regla FORWARD filtramos el acceso de la red local
## al exterior. 
# Aceptamos que vayan a puertos 80
iptables -A FORWARD -s 10.1.70.0/24 -i eth1 -p tcp --dport 80 -j ACCEPT
# Aceptamos que vayan a puertos https
iptables -A FORWARD -s 10.1.70.0/24 -i eth1 -p tcp --dport 443 -j ACCEPT
# Aceptamos que consulten los DNS
iptables -A FORWARD -s 10.1.70.0/24 -i eth1 -p tcp --dport 53 -j ACCEPT
iptables -A FORWARD -s 10.1.70.0/24 -i eth1 -p udp --dport 53 -j ACCEPT
# Y denegamos el resto
iptables -A FORWARD -s 10.1.70.0/24 -i eth1 -j DROP
# Ahora hacemos enmascaramiento de la red local
# y activamos el BIT DE FORWARDING 
iptables -t nat -A POSTROUTING -s 10.1.70.0/24 -o eth0 -j MASQUERADE
# Con esto permitimos hacer forward de paquetes en el firewall, o sea
# que otras máquinas puedan salir a traves del firewall.
echo 1 > /proc/sys/net/ipv4/ip_forward
#Aquí podemos cerrar puertos

# Fin del script

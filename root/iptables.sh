echo -n Aplicando reglas...
# DEFAULT POLICY (DROP)
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD ACCEPT
# ALLOW TCP & UDP PROTOCOLS
iptables -A FORWARD -i eth1 -o eth0 -p tcp --dport 80 -j DROP
iptables -A FORWARD -i eth1 -o eth0 -p tcp --dport 443 -j DROP
iptables -A FORWARD -i eth1 -o eth0 -p udp --dport 53 -j DROP


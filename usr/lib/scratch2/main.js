const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')
var ipcMain = require('electron').ipcMain;
var fs = require('fs');


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({
	  width: 1000, 
	  height: 800, 
	  webPreferences: { 
		  plugins: true 
	  }, 
	  icon: '/usr/share/icons/hicolor/48x48/apps/scratch.png' 
  })

  // and load the index.html of the app.
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools.
  //win.webContents.openDevTools()
  win.setMenu(null)

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })

  win.on('close', function(e){
    e.preventDefault();
    win.webContents.executeJavaScript(`
        var ipcRenderer = require('electron').ipcRenderer;
        var bool = Scratch.FlashApp.ASobj.ASisUnchanged();
        ipcRenderer.send('query', bool);
    `);
  });
}

ipcMain.on('query', function (event, value)
{
    if (!value)
    {
        var choice = require('electron').dialog.showMessageBox (win,
            {
                type: 'question',
                buttons: ['Cancel','OK'],
                title: 'Project not saved',
                message: '   Do you really want to quit?   '
            });
        if (choice == 1) win.destroy();
    }
    else win.destroy();
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.

if (fs.existsSync('/usr/lib/chromium-browser/libpepflashplayer.so'))
    app.commandLine.appendSwitch('ppapi-flash-path', '/usr/lib/chromium-browser/libpepflashplayer.so');
else if (fs.existsSync('/usr/lib/chromium/libpepflashplayer.so'))
    app.commandLine.appendSwitch('ppapi-flash-path', '/usr/lib/chromium/libpepflashplayer.so');

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

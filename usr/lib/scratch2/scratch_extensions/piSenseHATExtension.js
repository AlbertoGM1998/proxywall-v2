
new (function() {
    var ext = this;
    var fs = require('fs');
    var nodeimu;
    var IMU;
    var fbfile = "";

    // Cleanup function when the extension is unloaded
    ext._shutdown = function ()
    {
    };

    // Status reporting code
    // Use this to report missing hardware, plugin or unsupported browser
    ext._getStatus = function ()
    {
        // find the framebuffer on the SenseHAT
        var fbtest = 0;
        while (1)
        {
            var fname = "/sys/class/graphics/fb" + fbtest.toString () + "/name";
            if (fs.existsSync (fname))
            {
                var data = fs.readFileSync (fname, 'utf8');
                if (data.indexOf ('RPi-Sense FB') != -1)
                {
                    fbfile = "/dev/fb" + fbtest.toString ();
                    nodeimu  = require('nodeimu');
                    IMU = new nodeimu.IMU();
                    return {status: 2, msg: 'Ready'};
                }
            }
            else
            {
                // fall back to the emulator if possible
                if (fs.existsSync ("/dev/shm/rpi-sense-emu-screen"))
                {
                    fbfile = "/dev/shm/rpi-sense-emu-screen";
                    return {status: 2, msg: 'Ready'};
                }
                else return {status: 0, msg: 'No SenseHAT found'};
            }
            fbtest++;
        }
    };

    // Block and block menu descriptions
    ext.set_pixel = function (x, y, r, g, b) 
    {
        // r, g, b all in range 2 to 7; anything else is off
        var pix = new Uint8Array (2);
        var val = (Math.trunc (b / 32) * 1024) + (Math.trunc (r / 32) * 32) + Math.trunc (g / 32);
        pix[0] = val / 256;
        pix[1] = val % 256;
        fd = fs.openSync (fbfile, "r+");
        fs.writeSync (fd, pix, 0, 2, y * 16 + x * 2);
        fs.closeSync (fd);
    };

    ext.set_pixel_col = function (x, y, col)
    {
        var pix = new Uint8Array (2);
        var val = ext.map_colour (col);
        pix[0] = val / 256;
        pix[1] = val % 256;
        fd = fs.openSync (fbfile, "r+");
        fs.writeSync (fd, pix, 0, 2, y * 16 + x * 2);
        fs.closeSync (fd);
    }

    ext.set_all_pixels = function (r, g, b)
    {
        var pix = new Uint8Array (128);
        var val = (Math.trunc (b / 32) * 1024) + (Math.trunc (r / 32) * 32) + Math.trunc (g / 32);
        var count = 0;
        while (count < 64)
        {
            pix[count * 2] = val / 256;
            pix[count * 2 + 1] = val % 256;
            count++;
        }
        fd = fs.openSync (fbfile, "r+");
        fs.writeSync (fd, pix, 0, 128, 0);
        fs.closeSync (fd);
    }

    ext.set_all_pixels_col = function (col)
    {
        var pix = new Uint8Array (128);
        var val = ext.map_colour (col);
        var count = 0;
        while (count < 64)
        {
            pix[count * 2] = val / 256;
            pix[count * 2 + 1] = val % 256;
            count++;
        }
        fd = fs.openSync (fbfile, "r+");
        fs.writeSync (fd, pix, 0, 128, 0);
        fs.closeSync (fd);    
    }

    ext.map_orient = function (pos, orient)
    {
        if (orient == 0)
        {
            x = pos % 8;
            y = (pos - x) / 8;
            if (x > 4) return 40;
            else return ((x * 1) + 1) * 8 - 1 - (y * 1);
        }
        else if (orient == 90)
        {
            if (pos < 40) return pos;
            else return 40;
        }
        else if (orient == 180)
        {
            x = pos % 8;
            y = (pos - x) / 8;
            if (x < 3) return 40;
            else return (7 - (x * 1)) * 8 + (y * 1);
        }
        else if (orient == 270)
        {
            if (pos > 24) return 63 - pos;
            else return 40;
        }
        return 40;
    }

    ext.map_colour = function (col)
    {
        if (col == 'off') return 0;
        else if (col == 'white') return 0x1CE7;
        else if (col == 'red') return 0x00E0;
        else if (col == 'green') return 0x0007;
        else if (col == 'blue') return 0x1C00;
        else if (col == 'magenta') return 0x1CE0;
        else if (col == 'yellow') return 0x00E7;
        else if (col == 'cyan') return 0x1C07;
        return 0;
    }

    ext.load_letter = function (lett)
    {
        var lgr = new Uint8Array (80);
        const dict = " +-*/!\"#$><0123456789.=)(ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz?,;:|@%[&_']\\~"
        inv = 90 - dict.indexOf (lett);
        if (inv > 90) inv = 90;
        fd = fs.openSync ('/usr/lib/scratch2/scratch_extensions/sense_hat_text.bmp', 'r');
        for (count = 0; count < 5; count++)
            fs.readSync (fd, lgr, count * 16, 16, 3098 + inv * 80 + (64 - count * 16));
        fs.closeSync (fd);
        return lgr;
    }

    ext.show_letter = function (lett, orient, colour, bg)
    {
        if (typeof (lett) != 'string') return;
        if (lett.length != 1) return;
        var pix = new Uint8Array (128);
        var count = 0;
        valf = ext.map_colour (colour);
        valb = ext.map_colour (bg);
        lgr = ext.load_letter (lett);
        while (count < 64)
        {
            map = ext.map_orient (count, orient);
            if (map == 40) val = valb;
            else if (lgr[map * 2] == 0xFF) val = valf;
            else val = valb;
            pix[count * 2] = val / 256;
            pix[count * 2 + 1] = val % 256;
            count++;
        }
        fd = fs.openSync (fbfile, "r+");
        fs.writeSync (fd, pix, 0, 128, 0);
        fs.closeSync (fd);
    };

    ext.scroll_message = function (txt, orient, colour, bg)
    {
        var pix = new Uint8Array (128);
        var char_ind = 0;
        var lett_ind = 0;
        var msg = String (txt);
        valf = ext.map_colour (colour);
        valb = ext.map_colour (bg);
        pix0 = valf / 256;
        pix1 = valf % 256;
        bg0 = valb / 256;
        bg1 = valb % 256;

        // clear the grid to off and output
        for (pel = 0; pel < 64; pel++)
        {
            pix[pel*2] = bg0;
            pix[pel*2 + 1] = bg1;
        }
        fd = fs.openSync (fbfile, "r+");
        fs.writeSync (fd, pix, 0, 128, 0);
        fs.closeSync (fd);

        for (lett_ind = 0; lett_ind < msg.length + 2; lett_ind++)
        {
            lgrid = ext.load_letter (msg[lett_ind]);
            for (char_ind = 0; char_ind < 6; char_ind++)
            {
                // scroll the grid
                for (col = 0; col < 7; col++)
                {
                    for (row = 0; row < 8; row++)
                    {
                        if (orient == 0)
                        {
                            // from right to left
                            pix[(row * 16) + (col * 2)] = pix[(row * 16) + (col + 1) * 2];
                            pix[(row * 16) + (col * 2) + 1] = pix[(row * 16) + (col + 1) * 2 + 1];
                        }
                        else if (orient == 90)
                        {
                            // from the bottom up
                            pix[(col * 16) + (row * 2)] = pix[((col + 1) * 16) + (row * 2)];
                            pix[(col * 16) + (row * 2) + 1] = pix[((col + 1) * 16) + (row * 2) + 1];
                        }
                        else if (orient == 180)
                        {
                            // from left to right inverted
                            pix[(row * 16) + ((7 - col) * 2)] = pix[(row * 16) + (6 - col) * 2];
                            pix[(row * 16) + ((7 - col) * 2) + 1] = pix[(row * 16) + (6 - col) * 2 + 1];
                        }
                        else if (orient == 270)
                        {
                            // from the top down
                            pix[((7 - col) * 16) + (row * 2)] = pix[((6 - col) * 16) + (row * 2)];
                            pix[((7 - col) * 16) + (row * 2) + 1] = pix[((6 - col) * 16) + (row * 2) + 1];
                        }
                    }
                }

                // add the new line of pixels
                for (row = 0; row < 8; row++)
                {
                    if (orient == 0)
                    {
                        if (char_ind > 5)
                        {
                            pix[(row * 16) + 14] = bg0;
                            pix[(row * 16) + 15] = bg1;
                        }
                        else if (lgrid[char_ind * 16 + (14 - row * 2)] == 0xFF)
                        {
                            pix[(row * 16) + 14] = pix0;
                            pix[(row * 16) + 15] = pix1;
                        }
                        else
                        {
                            pix[(row * 16) + 14] = bg0;
                            pix[(row * 16) + 15] = bg1;
                        }
                    }
                    else if (orient == 90)
                    {
                        if (char_ind > 5)
                        {
                            pix[112 + (14 - row * 2)] = bg0;
                            pix[113 + (14 - row * 2)] = bg1;
                        }
                        else if (lgrid[char_ind * 16 + (14 - row * 2)] == 0xFF)
                        {
                            pix[112 + (14 - row * 2)] = pix0;
                            pix[113 + (14 - row * 2)] = pix1;
                        }
                        else
                        {
                            pix[112 + (14 - row * 2)] = bg0;
                            pix[113 + (14 - row * 2)] = bg1;
                        }
                    }
                    else if (orient == 180)
                    {
                        if (char_ind > 5)
                        {
                            pix[((7 - row) * 16)] = bg0;
                            pix[((7 - row) * 16) + 1] = bg1;
                        }
                        else if (lgrid[char_ind * 16 + (14 - row * 2)] == 0xFF)
                        {
                            pix[((7 - row) * 16)] = pix0;
                            pix[((7 - row) * 16) + 1] = pix1;
                        }
                        else
                        {
                            pix[((7 - row) * 16)] = bg0;
                            pix[((7 - row) * 16) + 1] = bg1;
                        }
                    }
                    else if (orient == 270)
                    {
                        if (char_ind > 5)
                        {
                            pix[row * 2] = bg0;
                            pix[row * 2 + 1] = bg1;
                        }
                        else if (lgrid[char_ind * 16 + (14 - row * 2)] == 0xFF)
                        {
                            pix[row * 2] = pix0;
                            pix[row * 2 + 1] = pix1;
                        }
                        else
                        {
                            pix[row * 2] = bg0;
                            pix[row * 2 + 1] = bg1;
                        }
                    }
                }

                // output the buffer
                fd = fs.openSync (fbfile, "r+");
                fs.writeSync (fd, pix, 0, 128, 0);
                fs.closeSync (fd);

                // pause for a bit
                var start = new Date().getTime();
                for (var i = 0; i < 1e7; i++)
                {
                    if ((new Date().getTime() - start) > 100) break;
                }
            }
        }
    }

    ext.get_t = function ()
    {
        if (fbfile == "/dev/shm/rpi-sense-emu-screen")
        {
            var data = new Uint8Array (20);
            var fd = fs.openSync ("/dev/shm/rpi-sense-emu-pressure", "r");
            fs.readSync (fd, data, 0, 20, 0);
            fs.closeSync (fd);
            var view = new DataView (data.buffer, 0, 20);
            return Number((view.getInt16 (16, true) / 480) + 37).toFixed (2);
        }
        var data = IMU.getValueSync();
        return Number (data.temperature).toFixed (2);
    };

    ext.get_p = function ()
    {
        if (fbfile == "/dev/shm/rpi-sense-emu-screen")
        {
            var data = new Uint8Array (20);
            var fd = fs.openSync ("/dev/shm/rpi-sense-emu-pressure", "r");
            fs.readSync (fd, data, 0, 20, 0);
            fs.closeSync (fd);
            var view = new DataView (data.buffer, 0, 20);
            return Number (view.getInt32 (12, true) / 4096).toFixed (2);
        }
        var data = IMU.getValueSync();
        return Number (data.pressure).toFixed (2);
    };

    ext.get_h = function ()
    {
        if (fbfile == "/dev/shm/rpi-sense-emu-screen")
        {
            var data = new Uint8Array (28);
            var fd = fs.openSync ("/dev/shm/rpi-sense-emu-humidity", "r");
            fs.readSync (fd, data, 0, 28, 0);
            fs.closeSync (fd);
            var view = new DataView (data.buffer, 0, 28);
            return Number (view.getInt16 (22, true) / 256).toFixed (2);
        }
        var data = IMU.getValueSync();
        return Number (data.humidity).toFixed (2);
    };

    ext.get_ox = function ()
    {
        if (fbfile == "/dev/shm/rpi-sense-emu-screen")
        {
            var data = new Uint8Array (56);
            var fd = fs.openSync ("/dev/shm/rpi-sense-emu-imu", "r");
            fs.readSync (fd, data, 0, 56, 0);
            fs.closeSync (fd);
            var view = new DataView (data.buffer, 0, 56);
            return Number (view.getInt16 (50, true) * 360 / 32768).toFixed (2);
        }
        var data = IMU.getValueSync();
        return Number (data.fusionPose.x * 180 / Math.PI).toFixed (2);
    };

    ext.get_oy = function ()
    {
        if (fbfile == "/dev/shm/rpi-sense-emu-screen")
        {
            var data = new Uint8Array (56);
            var fd = fs.openSync ("/dev/shm/rpi-sense-emu-imu", "r");
            fs.readSync (fd, data, 0, 56, 0);
            fs.closeSync (fd);
            var view = new DataView (data.buffer, 0, 56);
            return Number (view.getInt16 (52, true) * 360 / 32768).toFixed (2);
        }
        var data = IMU.getValueSync();
        return Number (data.fusionPose.y * 180 / Math.PI).toFixed (2);
    };

    ext.get_oz = function ()
    {
        if (fbfile == "/dev/shm/rpi-sense-emu-screen")
        {
            var data = new Uint8Array (56);
            var fd = fs.openSync ("/dev/shm/rpi-sense-emu-imu", "r");
            fs.readSync (fd, data, 0, 56, 0);
            fs.closeSync (fd);
            var view = new DataView (data.buffer, 0, 56);
            return Number (view.getInt16 (54, true) * 360 / 32768).toFixed (2);
        }
        var data = IMU.getValueSync();
        return Number (data.fusionPose.z * 180 / Math.PI).toFixed (2);
    };

     // Block and block menu descriptions
    var descriptor = {
        blocks: [
            [' ', 'set pixel %n , %n to R %n G %n B %n', 'set_pixel', '0', '0', '255', '255', '255'],
            [' ', 'set pixel %n , %n to %m.colour', 'set_pixel_col', '0', '0', 'white'],
            [' ', 'set all pixels to R %n G %n B %n', 'set_all_pixels', '255', '255', '255'],
            [' ', 'set all pixels to %m.colour', 'set_all_pixels_col', 'white'],
            [' ', 'show letter %s at rotation %m.rotation in colour %m.colour background %m.colour', 'show_letter', 'A', '0', 'white', 'off'],
            [' ', 'scroll message %s at rotation %m.rotation in colour %m.colour background %m.colour', 'scroll_message', 'Hello!', '0', 'white', 'off'],
            ['r', 'roll', 'get_ox'],
            ['r', 'pitch', 'get_oy'],
            ['r', 'yaw', 'get_oz'],
            ['r', 'temperature', 'get_t'],
            ['r', 'pressure', 'get_p'],
            ['r', 'humidity', 'get_h'],
        ],
        menus: {
            colour: ['off', 'red', 'green', 'blue', 'yellow', 'cyan', 'magenta', 'white'],
            direction: ['up', 'down', 'left', 'right'],
            rotation : ['0', '90', '180', '270'],
        }
    };

    // Register the extension
    ScratchExtensions.register('Pi SenseHAT', descriptor, ext);
})();

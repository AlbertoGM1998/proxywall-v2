prefix="/usr"
datadir="/usr/share"
localedir=datadir+"/locale"
pkgdatadir="/usr/share/alacarte"
libdir="/usr/lib/arm-linux-gnueabihf"
libexecdir="/usr/lib/arm-linux-gnueabihf"
PACKAGE="alacarte"
VERSION="3.11.91"
GETTEXT_PACKAGE="alacarte"

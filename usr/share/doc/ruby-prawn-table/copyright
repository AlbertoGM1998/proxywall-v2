Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: prawn-table
Source: https://www.github.com/prawnpdf/prawn-table

Files: *
Copyright: 2009-2011 Brad Ediger <brad@bradediger.com>
           2009-2014 Gregory Brown <gregory.t.brown@gmail.com>
License: Ruby or GPL-2 or GPL-3

Files: debian/*
Copyright: 2014 Cédric Boutillier <boutil@debian.org>
License: Ruby or GPL-2 or GPL-3
Comment: the Debian packaging is licensed under the same terms as the original package.

License: Ruby
 You can redistribute Prawn and/or modify it under either the terms of the GPLv2
 or GPLv3 (see GPLv2 and GPLv3 files), or the conditions below:
 .
   1. You may make and give away verbatim copies of the source form of the
      software without restriction, provided that you duplicate all of the
      original copyright notices and associated disclaimers.
 .
   2. You may modify your copy of the software in any way, provided that
      you do at least ONE of the following:
 .
        a) place your modifications in the Public Domain or otherwise
           make them Freely Available, such as by posting said
     modifications to Usenet or an equivalent medium, or by allowing
     the author to include your modifications in the software.
 .
        b) use the modified software only within your corporation or
           organization.
 .
        c) rename any non-standard executables so the names do not conflict
     with standard executables, which must also be provided.
 .
        d) make other distribution arrangements with the author.
 .
   3. You may distribute the software in object code or executable
      form, provided that you do at least ONE of the following:
 .
        a) distribute the executables and library files of the software,
     together with instructions (in the manual page or equivalent)
     on where to get the original distribution.
 .
        b) accompany the distribution with the machine-readable source of
     the software.
 .
        c) give non-standard executables non-standard names, with
           instructions on where to get the original software distribution.
 .
        d) make other distribution arrangements with the author.
 .
   4. You may modify and include the part of the software into any other
      software (possibly commercial).
 .
   5. The scripts and library files supplied as input to or produced as
      output from the software do not automatically fall under the
      copyright of the software, but belong to whomever generated them,
      and may be sold commercially, and may be aggregated with this
      software.
 .
   6. THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
      IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
      PURPOSE.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License Version 2 as published
 by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License Version 3 as published
 by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, full text of the GPL-3 license can be found
 in `/usr/share/common-licenses/GPL-3'

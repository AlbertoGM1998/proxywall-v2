# -*- encoding: utf-8 -*-
# stub: coderay 1.1.2 ruby lib

Gem::Specification.new do |s|
  s.name = "coderay".freeze
  s.version = "1.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Kornelius Kalnbach".freeze]
  s.date = "2018-01-13"
  s.description = "Fast and easy syntax highlighting for selected languages, written in Ruby. Comes with RedCloth integration and LOC counter.".freeze
  s.email = ["murphy@rubychan.de".freeze]
  s.executables = ["coderay".freeze]
  s.extra_rdoc_files = ["README_INDEX.rdoc".freeze]
  s.files = ["MIT-LICENSE".freeze, "README_INDEX.rdoc".freeze, "bin/coderay".freeze, "lib/coderay.rb".freeze, "lib/coderay/duo.rb".freeze, "lib/coderay/encoders.rb".freeze, "lib/coderay/encoders/_map.rb".freeze, "lib/coderay/encoders/comment_filter.rb".freeze, "lib/coderay/encoders/count.rb".freeze, "lib/coderay/encoders/debug.rb".freeze, "lib/coderay/encoders/debug_lint.rb".freeze, "lib/coderay/encoders/div.rb".freeze, "lib/coderay/encoders/encoder.rb".freeze, "lib/coderay/encoders/filter.rb".freeze, "lib/coderay/encoders/html.rb".freeze, "lib/coderay/encoders/html/css.rb".freeze, "lib/coderay/encoders/html/numbering.rb".freeze, "lib/coderay/encoders/html/output.rb".freeze, "lib/coderay/encoders/json.rb".freeze, "lib/coderay/encoders/lines_of_code.rb".freeze, "lib/coderay/encoders/lint.rb".freeze, "lib/coderay/encoders/null.rb".freeze, "lib/coderay/encoders/page.rb".freeze, "lib/coderay/encoders/span.rb".freeze, "lib/coderay/encoders/statistic.rb".freeze, "lib/coderay/encoders/terminal.rb".freeze, "lib/coderay/encoders/text.rb".freeze, "lib/coderay/encoders/token_kind_filter.rb".freeze, "lib/coderay/encoders/xml.rb".freeze, "lib/coderay/encoders/yaml.rb".freeze, "lib/coderay/for_redcloth.rb".freeze, "lib/coderay/helpers/file_type.rb".freeze, "lib/coderay/helpers/plugin.rb".freeze, "lib/coderay/helpers/plugin_host.rb".freeze, "lib/coderay/helpers/word_list.rb".freeze, "lib/coderay/scanners.rb".freeze, "lib/coderay/scanners/_map.rb".freeze, "lib/coderay/scanners/c.rb".freeze, "lib/coderay/scanners/clojure.rb".freeze, "lib/coderay/scanners/cpp.rb".freeze, "lib/coderay/scanners/css.rb".freeze, "lib/coderay/scanners/debug.rb".freeze, "lib/coderay/scanners/delphi.rb".freeze, "lib/coderay/scanners/diff.rb".freeze, "lib/coderay/scanners/erb.rb".freeze, "lib/coderay/scanners/go.rb".freeze, "lib/coderay/scanners/groovy.rb".freeze, "lib/coderay/scanners/haml.rb".freeze, "lib/coderay/scanners/html.rb".freeze, "lib/coderay/scanners/java.rb".freeze, "lib/coderay/scanners/java/builtin_types.rb".freeze, "lib/coderay/scanners/java_script.rb".freeze, "lib/coderay/scanners/json.rb".freeze, "lib/coderay/scanners/lua.rb".freeze, "lib/coderay/scanners/php.rb".freeze, "lib/coderay/scanners/python.rb".freeze, "lib/coderay/scanners/raydebug.rb".freeze, "lib/coderay/scanners/ruby.rb".freeze, "lib/coderay/scanners/ruby/patterns.rb".freeze, "lib/coderay/scanners/ruby/string_state.rb".freeze, "lib/coderay/scanners/sass.rb".freeze, "lib/coderay/scanners/scanner.rb".freeze, "lib/coderay/scanners/sql.rb".freeze, "lib/coderay/scanners/taskpaper.rb".freeze, "lib/coderay/scanners/text.rb".freeze, "lib/coderay/scanners/xml.rb".freeze, "lib/coderay/scanners/yaml.rb".freeze, "lib/coderay/styles.rb".freeze, "lib/coderay/styles/_map.rb".freeze, "lib/coderay/styles/alpha.rb".freeze, "lib/coderay/styles/style.rb".freeze, "lib/coderay/token_kinds.rb".freeze, "lib/coderay/tokens.rb".freeze, "lib/coderay/tokens_proxy.rb".freeze, "lib/coderay/version.rb".freeze]
  s.homepage = "http://coderay.rubychan.de".freeze
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["-SNw2".freeze, "-mREADME_INDEX.rdoc".freeze, "-t CodeRay Documentation".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 1.8.6".freeze)
  s.rubyforge_project = "coderay".freeze
  s.rubygems_version = "2.5.2.2".freeze
  s.summary = "Fast syntax highlighting for selected languages.".freeze
end

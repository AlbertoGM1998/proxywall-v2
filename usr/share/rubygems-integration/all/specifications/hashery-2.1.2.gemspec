# -*- encoding: utf-8 -*-
# stub: hashery 2.1.2 ruby lib

Gem::Specification.new do |s|
  s.name = "hashery"
  s.version = "2.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Trans", "Kirk Haines", "Robert Klemme", "Jan Molic", "George Moschovitis", "Jeena Paradies", "Erik Veenstra"]
  s.date = "2016-05-05"
  s.description = "The Hashery is a tight collection of Hash-like classes. Included among its many offerings are the auto-sorting Dictionary class, the efficient LRUHash, the flexible OpenHash and the convenient KeyHash. Nearly every class is a subclass of the CRUDHash which defines a CRUD model on top of Ruby's standard Hash making it a snap to subclass and augment to fit any specific use case."
  s.email = ["transfire@gmail.com"]
  s.extra_rdoc_files = ["HISTORY.md", "LICENSE.txt", "NOTICE.txt", "README.md"]
  s.files = [".index", ".yardopts", "HISTORY.md", "Index.yml", "LICENSE.txt", "NOTICE.txt", "README.md", "alt/hashery/castinghash.rb", "alt/hashery/fuzzyhash.rb", "alt/hashery/keyhash.rb", "alt/hashery/linkedlist.rb", "alt/hashery/lruhash.rb", "alt/hashery/opencascade.rb", "alt/hashery/openhash.rb", "alt/hashery/orderedhash.rb", "alt/hashery/propertyhash.rb", "alt/hashery/queryhash.rb", "alt/hashery/statichash.rb", "demo/00_introduction.rdoc", "demo/01_open_hash.rdoc", "demo/02_query_hash.rdoc", "demo/03_casting_hash.rdoc", "demo/04_static_hash.rdoc", "demo/05_key_hash.rdoc", "demo/06_open_cascade.rdoc", "demo/07_fuzzy_hash.rdoc", "demo/08_propery_hash.rdoc", "demo/10_association.rdoc", "demo/applique/ae.rb", "demo/applique/hashery.rb", "lib/hashery.rb", "lib/hashery.yml", "lib/hashery/association.rb", "lib/hashery/casting_hash.rb", "lib/hashery/core_ext.rb", "lib/hashery/crud_hash.rb", "lib/hashery/dictionary.rb", "lib/hashery/fuzzy_hash.rb", "lib/hashery/ini_hash.rb", "lib/hashery/key_hash.rb", "lib/hashery/linked_list.rb", "lib/hashery/lru_hash.rb", "lib/hashery/open_cascade.rb", "lib/hashery/open_hash.rb", "lib/hashery/ordered_hash.rb", "lib/hashery/path_hash.rb", "lib/hashery/property_hash.rb", "lib/hashery/query_hash.rb", "lib/hashery/stash.rb", "lib/hashery/static_hash.rb", "test/case_association.rb", "test/case_casting_hash.rb", "test/case_core_ext.rb", "test/case_crud_hash.rb", "test/case_dictionary.rb", "test/case_ini_hash.rb", "test/case_key_hash.rb", "test/case_linked_list.rb", "test/case_lru_hash.rb", "test/case_open_cascade.rb", "test/case_open_hash.rb", "test/case_property_hash.rb", "test/case_query_hash.rb", "test/fixture/example.ini", "test/helper.rb"]
  s.homepage = "http://rubyworks.github.com/hashery"
  s.licenses = ["BSD-2-Clause"]
  s.rubygems_version = "2.5.1"
  s.summary = "Facets-bread collection of Hash-like classes."
  s.test_files = ["test/case_association.rb", "test/case_casting_hash.rb", "test/case_core_ext.rb", "test/case_crud_hash.rb", "test/case_dictionary.rb", "test/case_ini_hash.rb", "test/case_key_hash.rb", "test/case_linked_list.rb", "test/case_lru_hash.rb", "test/case_open_cascade.rb", "test/case_open_hash.rb", "test/case_property_hash.rb", "test/case_query_hash.rb", "test/helper.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<lemon>, [">= 0"])
      s.add_development_dependency(%q<qed>, [">= 0"])
      s.add_development_dependency(%q<rubytest-cli>, [">= 0"])
    else
      s.add_dependency(%q<lemon>, [">= 0"])
      s.add_dependency(%q<qed>, [">= 0"])
      s.add_dependency(%q<rubytest-cli>, [">= 0"])
    end
  else
    s.add_dependency(%q<lemon>, [">= 0"])
    s.add_dependency(%q<qed>, [">= 0"])
    s.add_dependency(%q<rubytest-cli>, [">= 0"])
  end
end

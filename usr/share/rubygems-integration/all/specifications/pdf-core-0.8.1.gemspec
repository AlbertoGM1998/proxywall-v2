# -*- encoding: utf-8 -*-
# stub: pdf-core 0.8.1 ruby lib

Gem::Specification.new do |s|
  s.name = "pdf-core".freeze
  s.version = "0.8.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.6".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Gregory Brown".freeze, "Brad Ediger".freeze, "Daniel Nelson".freeze, "Jonathan Greenberg".freeze, "James Healy".freeze]
  s.cert_chain = ["certs/pointlessone.pem".freeze]
  s.date = "2018-09-30"
  s.description = "PDF::Core is used by Prawn to render PDF documents".freeze
  s.email = ["gregory.t.brown@gmail.com".freeze, "brad@bradediger.com".freeze, "dnelson@bluejade.com".freeze, "greenberg@entryway.net".freeze, "jimmy@deefa.com".freeze]
  s.files = ["COPYING".freeze, "GPLv2".freeze, "GPLv3".freeze, "Gemfile".freeze, "LICENSE".freeze, "Rakefile".freeze, "lib/pdf".freeze, "lib/pdf/core".freeze, "lib/pdf/core.rb".freeze, "lib/pdf/core/annotations.rb".freeze, "lib/pdf/core/byte_string.rb".freeze, "lib/pdf/core/destinations.rb".freeze, "lib/pdf/core/document_state.rb".freeze, "lib/pdf/core/filter_list.rb".freeze, "lib/pdf/core/filters.rb".freeze, "lib/pdf/core/graphics_state.rb".freeze, "lib/pdf/core/literal_string.rb".freeze, "lib/pdf/core/name_tree.rb".freeze, "lib/pdf/core/object_store.rb".freeze, "lib/pdf/core/outline_item.rb".freeze, "lib/pdf/core/outline_root.rb".freeze, "lib/pdf/core/page.rb".freeze, "lib/pdf/core/page_geometry.rb".freeze, "lib/pdf/core/pdf_object.rb".freeze, "lib/pdf/core/reference.rb".freeze, "lib/pdf/core/renderer.rb".freeze, "lib/pdf/core/stream.rb".freeze, "lib/pdf/core/text.rb".freeze, "lib/pdf/core/utils.rb".freeze, "pdf-core.gemspec".freeze]
  s.homepage = "http://prawn.majesticseacreature.com".freeze
  s.licenses = ["PRAWN".freeze, "GPL-2.0".freeze, "GPL-3.0".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.3".freeze)
  s.rubyforge_project = "prawn".freeze
  s.rubygems_version = "2.7.6".freeze
  s.summary = "PDF::Core is used by Prawn to render PDF documents".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>.freeze, [">= 0"])
      s.add_development_dependency(%q<pdf-inspector>.freeze, ["~> 1.1.0"])
      s.add_development_dependency(%q<pdf-reader>.freeze, ["~> 1.2"])
      s.add_development_dependency(%q<rake>.freeze, [">= 0"])
      s.add_development_dependency(%q<rspec>.freeze, [">= 0"])
      s.add_development_dependency(%q<rubocop>.freeze, ["~> 0.55"])
      s.add_development_dependency(%q<rubocop-rspec>.freeze, ["~> 1.25"])
      s.add_development_dependency(%q<simplecov>.freeze, [">= 0"])
    else
      s.add_dependency(%q<bundler>.freeze, [">= 0"])
      s.add_dependency(%q<pdf-inspector>.freeze, ["~> 1.1.0"])
      s.add_dependency(%q<pdf-reader>.freeze, ["~> 1.2"])
      s.add_dependency(%q<rake>.freeze, [">= 0"])
      s.add_dependency(%q<rspec>.freeze, [">= 0"])
      s.add_dependency(%q<rubocop>.freeze, ["~> 0.55"])
      s.add_dependency(%q<rubocop-rspec>.freeze, ["~> 1.25"])
      s.add_dependency(%q<simplecov>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, [">= 0"])
    s.add_dependency(%q<pdf-inspector>.freeze, ["~> 1.1.0"])
    s.add_dependency(%q<pdf-reader>.freeze, ["~> 1.2"])
    s.add_dependency(%q<rake>.freeze, [">= 0"])
    s.add_dependency(%q<rspec>.freeze, [">= 0"])
    s.add_dependency(%q<rubocop>.freeze, ["~> 0.55"])
    s.add_dependency(%q<rubocop-rspec>.freeze, ["~> 1.25"])
    s.add_dependency(%q<simplecov>.freeze, [">= 0"])
  end
end

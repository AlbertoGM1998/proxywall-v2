# -*- encoding: utf-8 -*-
# stub: prawn 2.2.0 ruby lib

Gem::Specification.new do |s|
  s.name = "prawn".freeze
  s.version = "2.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.6".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Gregory Brown".freeze, "Brad Ediger".freeze, "Daniel Nelson".freeze, "Jonathan Greenberg".freeze, "James Healy".freeze]
  s.cert_chain = ["certs/pointlessone.pem".freeze]
  s.date = "2019-01-07"
  s.description = "  Prawn is a fast, tiny, and nimble PDF generator for Ruby\n".freeze
  s.email = ["gregory.t.brown@gmail.com".freeze, "brad@bradediger.com".freeze, "dnelson@bluejade.com".freeze, "greenberg@entryway.net".freeze, "jimmy@deefa.com".freeze]
  s.files = [".yardopts".freeze, "COPYING".freeze, "GPLv2".freeze, "GPLv3".freeze, "Gemfile".freeze, "LICENSE".freeze, "Rakefile".freeze, "data/fonts/Courier-Bold.afm".freeze, "data/fonts/Courier-BoldOblique.afm".freeze, "data/fonts/Courier-Oblique.afm".freeze, "data/fonts/Courier.afm".freeze, "data/fonts/Helvetica-Bold.afm".freeze, "data/fonts/Helvetica-BoldOblique.afm".freeze, "data/fonts/Helvetica-Oblique.afm".freeze, "data/fonts/Helvetica.afm".freeze, "data/fonts/MustRead.html".freeze, "data/fonts/Symbol.afm".freeze, "data/fonts/Times-Bold.afm".freeze, "data/fonts/Times-BoldItalic.afm".freeze, "data/fonts/Times-Italic.afm".freeze, "data/fonts/Times-Roman.afm".freeze, "data/fonts/ZapfDingbats.afm".freeze, "lib/prawn".freeze, "lib/prawn.rb".freeze, "lib/prawn/document".freeze, "lib/prawn/document.rb".freeze, "lib/prawn/document/bounding_box.rb".freeze, "lib/prawn/document/column_box.rb".freeze, "lib/prawn/document/internals.rb".freeze, "lib/prawn/document/span.rb".freeze, "lib/prawn/encoding.rb".freeze, "lib/prawn/errors.rb".freeze, "lib/prawn/font".freeze, "lib/prawn/font.rb".freeze, "lib/prawn/font/afm.rb".freeze, "lib/prawn/font/dfont.rb".freeze, "lib/prawn/font/ttc.rb".freeze, "lib/prawn/font/ttf.rb".freeze, "lib/prawn/font_metric_cache.rb".freeze, "lib/prawn/graphics".freeze, "lib/prawn/graphics.rb".freeze, "lib/prawn/graphics/blend_mode.rb".freeze, "lib/prawn/graphics/cap_style.rb".freeze, "lib/prawn/graphics/color.rb".freeze, "lib/prawn/graphics/dash.rb".freeze, "lib/prawn/graphics/join_style.rb".freeze, "lib/prawn/graphics/patterns.rb".freeze, "lib/prawn/graphics/transformation.rb".freeze, "lib/prawn/graphics/transparency.rb".freeze, "lib/prawn/grid.rb".freeze, "lib/prawn/image_handler.rb".freeze, "lib/prawn/images".freeze, "lib/prawn/images.rb".freeze, "lib/prawn/images/image.rb".freeze, "lib/prawn/images/jpg.rb".freeze, "lib/prawn/images/png.rb".freeze, "lib/prawn/measurement_extensions.rb".freeze, "lib/prawn/measurements.rb".freeze, "lib/prawn/outline.rb".freeze, "lib/prawn/repeater.rb".freeze, "lib/prawn/security".freeze, "lib/prawn/security.rb".freeze, "lib/prawn/security/arcfour.rb".freeze, "lib/prawn/soft_mask.rb".freeze, "lib/prawn/stamp.rb".freeze, "lib/prawn/text".freeze, "lib/prawn/text.rb".freeze, "lib/prawn/text/box.rb".freeze, "lib/prawn/text/formatted".freeze, "lib/prawn/text/formatted.rb".freeze, "lib/prawn/text/formatted/arranger.rb".freeze, "lib/prawn/text/formatted/box.rb".freeze, "lib/prawn/text/formatted/fragment.rb".freeze, "lib/prawn/text/formatted/line_wrap.rb".freeze, "lib/prawn/text/formatted/parser.rb".freeze, "lib/prawn/text/formatted/wrap.rb".freeze, "lib/prawn/transformation_stack.rb".freeze, "lib/prawn/utilities.rb".freeze, "lib/prawn/version.rb".freeze, "lib/prawn/view.rb".freeze, "manual/absolute_position.pdf".freeze, "manual/basic_concepts".freeze, "manual/basic_concepts/adding_pages.rb".freeze, "manual/basic_concepts/basic_concepts.rb".freeze, "manual/basic_concepts/creation.rb".freeze, "manual/basic_concepts/cursor.rb".freeze, "manual/basic_concepts/measurement.rb".freeze, "manual/basic_concepts/origin.rb".freeze, "manual/basic_concepts/other_cursor_helpers.rb".freeze, "manual/basic_concepts/view.rb".freeze, "manual/bounding_box".freeze, "manual/bounding_box/bounding_box.rb".freeze, "manual/bounding_box/bounds.rb".freeze, "manual/bounding_box/canvas.rb".freeze, "manual/bounding_box/creation.rb".freeze, "manual/bounding_box/indentation.rb".freeze, "manual/bounding_box/nesting.rb".freeze, "manual/bounding_box/russian_boxes.rb".freeze, "manual/bounding_box/stretchy.rb".freeze, "manual/contents.rb".freeze, "manual/cover.rb".freeze, "manual/document_and_page_options".freeze, "manual/document_and_page_options/background.rb".freeze, "manual/document_and_page_options/document_and_page_options.rb".freeze, "manual/document_and_page_options/metadata.rb".freeze, "manual/document_and_page_options/page_margins.rb".freeze, "manual/document_and_page_options/page_size.rb".freeze, "manual/document_and_page_options/print_scaling.rb".freeze, "manual/example_helper.rb".freeze, "manual/graphics".freeze, "manual/graphics/blend_mode.rb".freeze, "manual/graphics/circle_and_ellipse.rb".freeze, "manual/graphics/color.rb".freeze, "manual/graphics/common_lines.rb".freeze, "manual/graphics/fill_and_stroke.rb".freeze, "manual/graphics/fill_rules.rb".freeze, "manual/graphics/gradients.rb".freeze, "manual/graphics/graphics.rb".freeze, "manual/graphics/helper.rb".freeze, "manual/graphics/line_width.rb".freeze, "manual/graphics/lines_and_curves.rb".freeze, "manual/graphics/polygon.rb".freeze, "manual/graphics/rectangle.rb".freeze, "manual/graphics/rotate.rb".freeze, "manual/graphics/scale.rb".freeze, "manual/graphics/soft_masks.rb".freeze, "manual/graphics/stroke_cap.rb".freeze, "manual/graphics/stroke_dash.rb".freeze, "manual/graphics/stroke_join.rb".freeze, "manual/graphics/translate.rb".freeze, "manual/graphics/transparency.rb".freeze, "manual/how_to_read_this_manual.rb".freeze, "manual/images".freeze, "manual/images/absolute_position.rb".freeze, "manual/images/fit.rb".freeze, "manual/images/horizontal.rb".freeze, "manual/images/images.rb".freeze, "manual/images/plain_image.rb".freeze, "manual/images/scale.rb".freeze, "manual/images/vertical.rb".freeze, "manual/images/width_and_height.rb".freeze, "manual/layout".freeze, "manual/layout/boxes.rb".freeze, "manual/layout/content.rb".freeze, "manual/layout/layout.rb".freeze, "manual/layout/simple_grid.rb".freeze, "manual/outline".freeze, "manual/outline/add_subsection_to.rb".freeze, "manual/outline/insert_section_after.rb".freeze, "manual/outline/outline.rb".freeze, "manual/outline/sections_and_pages.rb".freeze, "manual/repeatable_content".freeze, "manual/repeatable_content/alternate_page_numbering.rb".freeze, "manual/repeatable_content/page_numbering.rb".freeze, "manual/repeatable_content/repeatable_content.rb".freeze, "manual/repeatable_content/repeater.rb".freeze, "manual/repeatable_content/stamp.rb".freeze, "manual/security".freeze, "manual/security/encryption.rb".freeze, "manual/security/permissions.rb".freeze, "manual/security/security.rb".freeze, "manual/table.rb".freeze, "manual/text".freeze, "manual/text/alignment.rb".freeze, "manual/text/color.rb".freeze, "manual/text/column_box.rb".freeze, "manual/text/fallback_fonts.rb".freeze, "manual/text/font.rb".freeze, "manual/text/font_size.rb".freeze, "manual/text/font_style.rb".freeze, "manual/text/formatted_callbacks.rb".freeze, "manual/text/formatted_text.rb".freeze, "manual/text/free_flowing_text.rb".freeze, "manual/text/inline.rb".freeze, "manual/text/kerning_and_character_spacing.rb".freeze, "manual/text/leading.rb".freeze, "manual/text/line_wrapping.rb".freeze, "manual/text/paragraph_indentation.rb".freeze, "manual/text/positioned_text.rb".freeze, "manual/text/registering_families.rb".freeze, "manual/text/rendering_and_color.rb".freeze, "manual/text/right_to_left_text.rb".freeze, "manual/text/rotation.rb".freeze, "manual/text/single_usage.rb".freeze, "manual/text/text.rb".freeze, "manual/text/text_box_excess.rb".freeze, "manual/text/text_box_extensions.rb".freeze, "manual/text/text_box_overflow.rb".freeze, "manual/text/utf8.rb".freeze, "manual/text/win_ansi_charset.rb".freeze, "prawn.gemspec".freeze, "spec/data".freeze, "spec/data/curves.pdf".freeze, "spec/extensions".freeze, "spec/extensions/encoding_helpers.rb".freeze, "spec/manual_spec.rb".freeze, "spec/prawn".freeze, "spec/prawn/document".freeze, "spec/prawn/document/bounding_box_spec.rb".freeze, "spec/prawn/document/column_box_spec.rb".freeze, "spec/prawn/document/security_spec.rb".freeze, "spec/prawn/document_annotations_spec.rb".freeze, "spec/prawn/document_destinations_spec.rb".freeze, "spec/prawn/document_grid_spec.rb".freeze, "spec/prawn/document_reference_spec.rb".freeze, "spec/prawn/document_span_spec.rb".freeze, "spec/prawn/document_spec.rb".freeze, "spec/prawn/font_metric_cache_spec.rb".freeze, "spec/prawn/font_spec.rb".freeze, "spec/prawn/graphics".freeze, "spec/prawn/graphics/blend_mode_spec.rb".freeze, "spec/prawn/graphics/transparency_spec.rb".freeze, "spec/prawn/graphics_spec.rb".freeze, "spec/prawn/graphics_stroke_styles_spec.rb".freeze, "spec/prawn/image_handler_spec.rb".freeze, "spec/prawn/images".freeze, "spec/prawn/images/jpg_spec.rb".freeze, "spec/prawn/images/png_spec.rb".freeze, "spec/prawn/images_spec.rb".freeze, "spec/prawn/measurements_extensions_spec.rb".freeze, "spec/prawn/outline_spec.rb".freeze, "spec/prawn/repeater_spec.rb".freeze, "spec/prawn/soft_mask_spec.rb".freeze, "spec/prawn/stamp_spec.rb".freeze, "spec/prawn/text".freeze, "spec/prawn/text/box_spec.rb".freeze, "spec/prawn/text/formatted".freeze, "spec/prawn/text/formatted/arranger_spec.rb".freeze, "spec/prawn/text/formatted/box_spec.rb".freeze, "spec/prawn/text/formatted/fragment_spec.rb".freeze, "spec/prawn/text/formatted/line_wrap_spec.rb".freeze, "spec/prawn/text/formatted/parser_spec.rb".freeze, "spec/prawn/text_draw_text_spec.rb".freeze, "spec/prawn/text_rendering_mode_spec.rb".freeze, "spec/prawn/text_spacing_spec.rb".freeze, "spec/prawn/text_spec.rb".freeze, "spec/prawn/text_with_inline_formatting_spec.rb".freeze, "spec/prawn/transformation_stack_spec.rb".freeze, "spec/prawn/view_spec.rb".freeze, "spec/spec_helper.rb".freeze]
  s.homepage = "http://prawnpdf.org".freeze
  s.licenses = ["PRAWN".freeze, "GPL-2.0".freeze, "GPL-3.0".freeze]
  s.required_ruby_version = Gem::Requirement.new("~> 2.1".freeze)
  s.rubygems_version = "2.7.6".freeze
  s.summary = "A fast and nimble PDF generator for Ruby".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<ttfunk>.freeze, ["~> 1.5"])
      s.add_runtime_dependency(%q<pdf-core>.freeze, ["~> 0.8.1"])
      s.add_development_dependency(%q<pdf-inspector>.freeze, ["~> 1.3"])
      s.add_development_dependency(%q<yard>.freeze, [">= 0"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.0"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 12.0"])
      s.add_development_dependency(%q<simplecov>.freeze, [">= 0"])
      s.add_development_dependency(%q<prawn-manual_builder>.freeze, [">= 0.2.0"])
      s.add_development_dependency(%q<pdf-reader>.freeze, [">= 1.4.1"])
      s.add_development_dependency(%q<rubocop>.freeze, ["~> 0.47.1"])
      s.add_development_dependency(%q<rubocop-rspec>.freeze, ["~> 1.10"])
    else
      s.add_dependency(%q<ttfunk>.freeze, ["~> 1.5"])
      s.add_dependency(%q<pdf-core>.freeze, ["~> 0.8.1"])
      s.add_dependency(%q<pdf-inspector>.freeze, ["~> 1.3"])
      s.add_dependency(%q<yard>.freeze, [">= 0"])
      s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
      s.add_dependency(%q<rake>.freeze, ["~> 12.0"])
      s.add_dependency(%q<simplecov>.freeze, [">= 0"])
      s.add_dependency(%q<prawn-manual_builder>.freeze, [">= 0.2.0"])
      s.add_dependency(%q<pdf-reader>.freeze, [">= 1.4.1"])
      s.add_dependency(%q<rubocop>.freeze, ["~> 0.47.1"])
      s.add_dependency(%q<rubocop-rspec>.freeze, ["~> 1.10"])
    end
  else
    s.add_dependency(%q<ttfunk>.freeze, ["~> 1.5"])
    s.add_dependency(%q<pdf-core>.freeze, ["~> 0.8.1"])
    s.add_dependency(%q<pdf-inspector>.freeze, ["~> 1.3"])
    s.add_dependency(%q<yard>.freeze, [">= 0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
    s.add_dependency(%q<rake>.freeze, ["~> 12.0"])
    s.add_dependency(%q<simplecov>.freeze, [">= 0"])
    s.add_dependency(%q<prawn-manual_builder>.freeze, [">= 0.2.0"])
    s.add_dependency(%q<pdf-reader>.freeze, [">= 1.4.1"])
    s.add_dependency(%q<rubocop>.freeze, ["~> 0.47.1"])
    s.add_dependency(%q<rubocop-rspec>.freeze, ["~> 1.10"])
  end
end

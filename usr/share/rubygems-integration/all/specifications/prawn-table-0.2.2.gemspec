# -*- encoding: utf-8 -*-
# stub: prawn-table 0.2.2 ruby lib

Gem::Specification.new do |s|
  s.name = "prawn-table"
  s.version = "0.2.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.6") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Gregory Brown", "Brad Ediger", "Daniel Nelson", "Jonathan Greenberg", "James Healy", "Hartwig Brandl"]
  s.date = "2016-06-11"
  s.description = "  Prawn::Table provides tables for the Prawn PDF toolkit\n"
  s.email = ["gregory.t.brown@gmail.com", "brad@bradediger.com", "dnelson@bluejade.com", "greenberg@entryway.net", "jimmy@deefa.com", "mail@hartwigbrandl.at"]
  s.files = ["COPYING", "GPLv2", "GPLv3", "Gemfile", "LICENSE", "lib/prawn/table.rb", "lib/prawn/table/cell.rb", "lib/prawn/table/cell/image.rb", "lib/prawn/table/cell/in_table.rb", "lib/prawn/table/cell/span_dummy.rb", "lib/prawn/table/cell/subtable.rb", "lib/prawn/table/cell/text.rb", "lib/prawn/table/cells.rb", "lib/prawn/table/column_width_calculator.rb", "lib/prawn/table/version.rb", "manual/contents.rb", "manual/example_helper.rb", "manual/table/basic_block.rb", "manual/table/before_rendering_page.rb", "manual/table/cell_border_lines.rb", "manual/table/cell_borders_and_bg.rb", "manual/table/cell_dimensions.rb", "manual/table/cell_text.rb", "manual/table/column_widths.rb", "manual/table/content_and_subtables.rb", "manual/table/creation.rb", "manual/table/filtering.rb", "manual/table/flow_and_header.rb", "manual/table/image_cells.rb", "manual/table/position.rb", "manual/table/row_colors.rb", "manual/table/span.rb", "manual/table/style.rb", "manual/table/table.rb", "manual/table/width.rb", "prawn-table.gemspec", "spec/cell_spec.rb", "spec/extensions/encoding_helpers.rb", "spec/extensions/mocha.rb", "spec/spec_helper.rb", "spec/table/span_dummy_spec.rb", "spec/table_spec.rb"]
  s.homepage = "https://github.com/prawnpdf/prawn-table"
  s.licenses = ["RUBY", "GPL-2", "GPL-3"]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.3")
  s.rubyforge_project = "prawn"
  s.rubygems_version = "2.5.1"
  s.summary = "Provides tables for PrawnPDF"
  s.test_files = ["spec/cell_spec.rb", "spec/table_spec.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<prawn>, ["< 3.0.0", ">= 1.3.0"])
      s.add_development_dependency(%q<pdf-inspector>, ["~> 1.1.0"])
      s.add_development_dependency(%q<yard>, [">= 0"])
      s.add_development_dependency(%q<rspec>, ["= 2.14.1"])
      s.add_development_dependency(%q<mocha>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<simplecov>, [">= 0"])
      s.add_development_dependency(%q<prawn-manual_builder>, [">= 0.2.0"])
      s.add_development_dependency(%q<pdf-reader>, ["~> 1.2"])
    else
      s.add_dependency(%q<prawn>, ["< 3.0.0", ">= 1.3.0"])
      s.add_dependency(%q<pdf-inspector>, ["~> 1.1.0"])
      s.add_dependency(%q<yard>, [">= 0"])
      s.add_dependency(%q<rspec>, ["= 2.14.1"])
      s.add_dependency(%q<mocha>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<simplecov>, [">= 0"])
      s.add_dependency(%q<prawn-manual_builder>, [">= 0.2.0"])
      s.add_dependency(%q<pdf-reader>, ["~> 1.2"])
    end
  else
    s.add_dependency(%q<prawn>, ["< 3.0.0", ">= 1.3.0"])
    s.add_dependency(%q<pdf-inspector>, ["~> 1.1.0"])
    s.add_dependency(%q<yard>, [">= 0"])
    s.add_dependency(%q<rspec>, ["= 2.14.1"])
    s.add_dependency(%q<mocha>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<simplecov>, [">= 0"])
    s.add_dependency(%q<prawn-manual_builder>, [">= 0.2.0"])
    s.add_dependency(%q<pdf-reader>, ["~> 1.2"])
  end
end

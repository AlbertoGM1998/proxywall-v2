# -*- encoding: utf-8 -*-
# stub: thread_safe 0.3.6 ruby lib

Gem::Specification.new do |s|
  s.name = "thread_safe".freeze
  s.version = "0.3.6"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Charles Oliver Nutter".freeze, "thedarkone".freeze]
  s.date = "2018-02-23"
  s.description = "A collection of data structures and utilities to make thread-safe programming in Ruby easier".freeze
  s.email = ["headius@headius.com".freeze, "thedarkone2@gmail.com".freeze]
  s.files = ["Gemfile".freeze, "LICENSE".freeze, "README.md".freeze, "Rakefile".freeze, "examples/bench_cache.rb".freeze, "ext/org/jruby/ext/thread_safe/JRubyCacheBackendLibrary.java".freeze, "ext/org/jruby/ext/thread_safe/jsr166e/ConcurrentHashMap.java".freeze, "ext/org/jruby/ext/thread_safe/jsr166e/ConcurrentHashMapV8.java".freeze, "ext/org/jruby/ext/thread_safe/jsr166e/LongAdder.java".freeze, "ext/org/jruby/ext/thread_safe/jsr166e/Striped64.java".freeze, "ext/org/jruby/ext/thread_safe/jsr166e/nounsafe/ConcurrentHashMapV8.java".freeze, "ext/org/jruby/ext/thread_safe/jsr166e/nounsafe/LongAdder.java".freeze, "ext/org/jruby/ext/thread_safe/jsr166e/nounsafe/Striped64.java".freeze, "ext/org/jruby/ext/thread_safe/jsr166y/ThreadLocalRandom.java".freeze, "ext/thread_safe/JrubyCacheBackendService.java".freeze, "lib/thread_safe.rb".freeze, "lib/thread_safe/atomic_reference_cache_backend.rb".freeze, "lib/thread_safe/cache.rb".freeze, "lib/thread_safe/mri_cache_backend.rb".freeze, "lib/thread_safe/non_concurrent_cache_backend.rb".freeze, "lib/thread_safe/synchronized_cache_backend.rb".freeze, "lib/thread_safe/synchronized_delegator.rb".freeze, "lib/thread_safe/util.rb".freeze, "lib/thread_safe/util/adder.rb".freeze, "lib/thread_safe/util/atomic_reference.rb".freeze, "lib/thread_safe/util/cheap_lockable.rb".freeze, "lib/thread_safe/util/power_of_two_tuple.rb".freeze, "lib/thread_safe/util/striped64.rb".freeze, "lib/thread_safe/util/volatile.rb".freeze, "lib/thread_safe/util/volatile_tuple.rb".freeze, "lib/thread_safe/util/xor_shift_random.rb".freeze, "lib/thread_safe/version.rb".freeze, "spec/spec_helper.rb".freeze, "spec/src/thread_safe/SecurityManager.java".freeze, "spec/support/threads.rb".freeze, "spec/support/threadsafe_test.rb".freeze, "spec/thread_safe/array_spec.rb".freeze, "spec/thread_safe/cache_loops_spec.rb".freeze, "spec/thread_safe/cache_spec.rb".freeze, "spec/thread_safe/hash_spec.rb".freeze, "spec/thread_safe/no_unsafe_spec.rb".freeze, "spec/thread_safe/synchronized_delegator_spec.rb".freeze, "tasks/update_doc.rake".freeze, "thread_safe.gemspec".freeze, "yard-template/default/fulldoc/html/css/common.css".freeze, "yard-template/default/layout/html/footer.erb".freeze]
  s.homepage = "https://github.com/ruby-concurrency/thread_safe".freeze
  s.licenses = ["Apache-2.0".freeze]
  s.rubygems_version = "2.5.2.2".freeze
  s.summary = "Thread-safe collections and utilities for Ruby".freeze
  s.test_files = ["spec/spec_helper.rb".freeze, "spec/src/thread_safe/SecurityManager.java".freeze, "spec/support/threads.rb".freeze, "spec/support/threadsafe_test.rb".freeze, "spec/thread_safe/array_spec.rb".freeze, "spec/thread_safe/cache_loops_spec.rb".freeze, "spec/thread_safe/cache_spec.rb".freeze, "spec/thread_safe/hash_spec.rb".freeze, "spec/thread_safe/no_unsafe_spec.rb".freeze, "spec/thread_safe/synchronized_delegator_spec.rb".freeze]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<atomic>.freeze, ["= 1.1.16"])
      s.add_development_dependency(%q<rake>.freeze, ["< 12.0"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.2"])
    else
      s.add_dependency(%q<atomic>.freeze, ["= 1.1.16"])
      s.add_dependency(%q<rake>.freeze, ["< 12.0"])
      s.add_dependency(%q<rspec>.freeze, ["~> 3.2"])
    end
  else
    s.add_dependency(%q<atomic>.freeze, ["= 1.1.16"])
    s.add_dependency(%q<rake>.freeze, ["< 12.0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.2"])
  end
end

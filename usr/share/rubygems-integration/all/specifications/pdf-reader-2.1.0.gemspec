# -*- encoding: utf-8 -*-
# stub: pdf-reader 2.1.0 ruby lib

Gem::Specification.new do |s|
  s.name = "pdf-reader".freeze
  s.version = "2.1.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["James Healy".freeze]
  s.date = "2018-09-30"
  s.description = "The PDF::Reader library implements a PDF parser conforming as much as possible to the PDF specification from Adobe".freeze
  s.email = ["jimmy@deefa.com".freeze]
  s.executables = ["pdf_callbacks".freeze, "pdf_object".freeze, "pdf_text".freeze]
  s.extra_rdoc_files = ["CHANGELOG".freeze, "MIT-LICENSE".freeze, "README.md".freeze, "TODO".freeze]
  s.files = ["CHANGELOG".freeze, "MIT-LICENSE".freeze, "README.md".freeze, "Rakefile".freeze, "TODO".freeze, "bin/pdf_callbacks".freeze, "bin/pdf_object".freeze, "bin/pdf_text".freeze, "examples/callbacks.rb".freeze, "examples/extract_bates.rb".freeze, "examples/extract_fonts.rb".freeze, "examples/extract_images.rb".freeze, "examples/fuzzy_paragraphs.rb".freeze, "examples/hash.rb".freeze, "examples/metadata.rb".freeze, "examples/page_count.rb".freeze, "examples/rspec.rb".freeze, "examples/text.rb".freeze, "examples/version.rb".freeze, "lib/pdf".freeze, "lib/pdf-reader.rb".freeze, "lib/pdf/hash.rb".freeze, "lib/pdf/reader".freeze, "lib/pdf/reader.rb".freeze, "lib/pdf/reader/afm".freeze, "lib/pdf/reader/afm/Courier-Bold.afm".freeze, "lib/pdf/reader/afm/Courier-BoldOblique.afm".freeze, "lib/pdf/reader/afm/Courier-Oblique.afm".freeze, "lib/pdf/reader/afm/Courier.afm".freeze, "lib/pdf/reader/afm/Helvetica-Bold.afm".freeze, "lib/pdf/reader/afm/Helvetica-BoldOblique.afm".freeze, "lib/pdf/reader/afm/Helvetica-Oblique.afm".freeze, "lib/pdf/reader/afm/Helvetica.afm".freeze, "lib/pdf/reader/afm/MustRead.html".freeze, "lib/pdf/reader/afm/Symbol.afm".freeze, "lib/pdf/reader/afm/Times-Bold.afm".freeze, "lib/pdf/reader/afm/Times-BoldItalic.afm".freeze, "lib/pdf/reader/afm/Times-Italic.afm".freeze, "lib/pdf/reader/afm/Times-Roman.afm".freeze, "lib/pdf/reader/afm/ZapfDingbats.afm".freeze, "lib/pdf/reader/buffer.rb".freeze, "lib/pdf/reader/cid_widths.rb".freeze, "lib/pdf/reader/cmap.rb".freeze, "lib/pdf/reader/encoding.rb".freeze, "lib/pdf/reader/encodings".freeze, "lib/pdf/reader/encodings/mac_expert.txt".freeze, "lib/pdf/reader/encodings/mac_roman.txt".freeze, "lib/pdf/reader/encodings/pdf_doc.txt".freeze, "lib/pdf/reader/encodings/standard.txt".freeze, "lib/pdf/reader/encodings/symbol.txt".freeze, "lib/pdf/reader/encodings/win_ansi.txt".freeze, "lib/pdf/reader/encodings/zapf_dingbats.txt".freeze, "lib/pdf/reader/error.rb".freeze, "lib/pdf/reader/filter".freeze, "lib/pdf/reader/filter.rb".freeze, "lib/pdf/reader/filter/ascii85.rb".freeze, "lib/pdf/reader/filter/ascii_hex.rb".freeze, "lib/pdf/reader/filter/depredict.rb".freeze, "lib/pdf/reader/filter/flate.rb".freeze, "lib/pdf/reader/filter/lzw.rb".freeze, "lib/pdf/reader/filter/null.rb".freeze, "lib/pdf/reader/filter/run_length.rb".freeze, "lib/pdf/reader/font.rb".freeze, "lib/pdf/reader/font_descriptor.rb".freeze, "lib/pdf/reader/form_xobject.rb".freeze, "lib/pdf/reader/glyph_hash.rb".freeze, "lib/pdf/reader/glyphlist.txt".freeze, "lib/pdf/reader/lzw.rb".freeze, "lib/pdf/reader/null_security_handler.rb".freeze, "lib/pdf/reader/object_cache.rb".freeze, "lib/pdf/reader/object_hash.rb".freeze, "lib/pdf/reader/object_stream.rb".freeze, "lib/pdf/reader/orientation_detector.rb".freeze, "lib/pdf/reader/page.rb".freeze, "lib/pdf/reader/page_layout.rb".freeze, "lib/pdf/reader/page_state.rb".freeze, "lib/pdf/reader/page_text_receiver.rb".freeze, "lib/pdf/reader/pages_strategy.rb".freeze, "lib/pdf/reader/parser.rb".freeze, "lib/pdf/reader/print_receiver.rb".freeze, "lib/pdf/reader/reference.rb".freeze, "lib/pdf/reader/register_receiver.rb".freeze, "lib/pdf/reader/resource_methods.rb".freeze, "lib/pdf/reader/standard_security_handler.rb".freeze, "lib/pdf/reader/standard_security_handler_v5.rb".freeze, "lib/pdf/reader/stream.rb".freeze, "lib/pdf/reader/synchronized_cache.rb".freeze, "lib/pdf/reader/text_run.rb".freeze, "lib/pdf/reader/token.rb".freeze, "lib/pdf/reader/transformation_matrix.rb".freeze, "lib/pdf/reader/unimplemented_security_handler.rb".freeze, "lib/pdf/reader/width_calculator".freeze, "lib/pdf/reader/width_calculator.rb".freeze, "lib/pdf/reader/width_calculator/built_in.rb".freeze, "lib/pdf/reader/width_calculator/composite.rb".freeze, "lib/pdf/reader/width_calculator/true_type.rb".freeze, "lib/pdf/reader/width_calculator/type_one_or_three.rb".freeze, "lib/pdf/reader/width_calculator/type_zero.rb".freeze, "lib/pdf/reader/xref.rb".freeze]
  s.homepage = "http://github.com/yob/pdf-reader".freeze
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["--title".freeze, "PDF::Reader Documentation".freeze, "--main".freeze, "README.md".freeze, "-q".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.3".freeze)
  s.rubygems_version = "2.7.6".freeze
  s.summary = "A library for accessing the content of PDF files".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>.freeze, [">= 0"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.5"])
      s.add_development_dependency(%q<cane>.freeze, ["~> 3.0"])
      s.add_development_dependency(%q<morecane>.freeze, ["~> 0.2"])
      s.add_development_dependency(%q<ir_b>.freeze, [">= 0"])
      s.add_development_dependency(%q<rdoc>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<Ascii85>.freeze, ["~> 1.0.0"])
      s.add_runtime_dependency(%q<ruby-rc4>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<hashery>.freeze, ["~> 2.0"])
      s.add_runtime_dependency(%q<ttfunk>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<afm>.freeze, ["~> 0.2.1"])
    else
      s.add_dependency(%q<rake>.freeze, [">= 0"])
      s.add_dependency(%q<rspec>.freeze, ["~> 3.5"])
      s.add_dependency(%q<cane>.freeze, ["~> 3.0"])
      s.add_dependency(%q<morecane>.freeze, ["~> 0.2"])
      s.add_dependency(%q<ir_b>.freeze, [">= 0"])
      s.add_dependency(%q<rdoc>.freeze, [">= 0"])
      s.add_dependency(%q<Ascii85>.freeze, ["~> 1.0.0"])
      s.add_dependency(%q<ruby-rc4>.freeze, [">= 0"])
      s.add_dependency(%q<hashery>.freeze, ["~> 2.0"])
      s.add_dependency(%q<ttfunk>.freeze, [">= 0"])
      s.add_dependency(%q<afm>.freeze, ["~> 0.2.1"])
    end
  else
    s.add_dependency(%q<rake>.freeze, [">= 0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.5"])
    s.add_dependency(%q<cane>.freeze, ["~> 3.0"])
    s.add_dependency(%q<morecane>.freeze, ["~> 0.2"])
    s.add_dependency(%q<ir_b>.freeze, [">= 0"])
    s.add_dependency(%q<rdoc>.freeze, [">= 0"])
    s.add_dependency(%q<Ascii85>.freeze, ["~> 1.0.0"])
    s.add_dependency(%q<ruby-rc4>.freeze, [">= 0"])
    s.add_dependency(%q<hashery>.freeze, ["~> 2.0"])
    s.add_dependency(%q<ttfunk>.freeze, [">= 0"])
    s.add_dependency(%q<afm>.freeze, ["~> 0.2.1"])
  end
end

# -*- encoding: utf-8 -*-
# stub: wavefile 0.6.0 ruby lib

Gem::Specification.new do |s|
  s.name = "wavefile"
  s.version = "0.6.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Joel Strait"]
  s.date = "2013-12-08"
  s.description = "You can use this gem to create Ruby programs that produce audio. Since it is written in pure Ruby (as opposed to wrapping an existing C library), you can use it without having to compile a separate extension."
  s.email = "joel dot strait at Google's popular web mail service"
  s.files = ["LICENSE", "README.markdown", "Rakefile", "lib/wavefile.rb", "lib/wavefile/buffer.rb", "lib/wavefile/duration.rb", "lib/wavefile/format.rb", "lib/wavefile/info.rb", "lib/wavefile/reader.rb", "lib/wavefile/writer.rb", "test/buffer_test.rb", "test/buffer_test.rbc", "test/duration_test.rb", "test/duration_test.rbc", "test/fixtures/actual_output/valid_mono_pcm_8_44100_with_padding_byte.wav", "test/fixtures/invalid/README.markdown", "test/fixtures/invalid/bad_riff_header.wav", "test/fixtures/invalid/bad_wavefile_format.wav", "test/fixtures/invalid/empty.wav", "test/fixtures/invalid/empty_format_chunk.wav", "test/fixtures/invalid/incomplete_riff_header.wav", "test/fixtures/invalid/insufficient_format_chunk.wav", "test/fixtures/invalid/no_data_chunk.wav", "test/fixtures/invalid/no_format_chunk.wav", "test/fixtures/unsupported/README.markdown", "test/fixtures/unsupported/bad_audio_format.wav", "test/fixtures/unsupported/bad_channel_count.wav", "test/fixtures/unsupported/bad_sample_rate.wav", "test/fixtures/unsupported/unsupported_audio_format.wav", "test/fixtures/unsupported/unsupported_bits_per_sample.wav", "test/fixtures/valid/README.markdown", "test/fixtures/valid/no_samples.wav", "test/fixtures/valid/valid_mono_float_32_44100.wav", "test/fixtures/valid/valid_mono_float_64_44100.wav", "test/fixtures/valid/valid_mono_pcm_16_44100.wav", "test/fixtures/valid/valid_mono_pcm_16_44100_junk_chunk_with_padding_byte.wav", "test/fixtures/valid/valid_mono_pcm_24_44100.wav", "test/fixtures/valid/valid_mono_pcm_32_44100.wav", "test/fixtures/valid/valid_mono_pcm_8_44100.wav", "test/fixtures/valid/valid_mono_pcm_8_44100_with_padding_byte.wav", "test/fixtures/valid/valid_stereo_float_32_44100.wav", "test/fixtures/valid/valid_stereo_float_64_44100.wav", "test/fixtures/valid/valid_stereo_pcm_16_44100.wav", "test/fixtures/valid/valid_stereo_pcm_24_44100.wav", "test/fixtures/valid/valid_stereo_pcm_32_44100.wav", "test/fixtures/valid/valid_stereo_pcm_8_44100.wav", "test/fixtures/valid/valid_tri_float_32_44100.wav", "test/fixtures/valid/valid_tri_float_64_44100.wav", "test/fixtures/valid/valid_tri_pcm_16_44100.wav", "test/fixtures/valid/valid_tri_pcm_24_44100.wav", "test/fixtures/valid/valid_tri_pcm_32_44100.wav", "test/fixtures/valid/valid_tri_pcm_8_44100.wav", "test/format_test.rb", "test/format_test.rbc", "test/info_test.rb", "test/info_test.rbc", "test/reader_test.rb", "test/reader_test.rbc", "test/wavefile_io_test_helper.rb", "test/wavefile_io_test_helper.rbc", "test/writer_test.rb", "test/writer_test.rbc"]
  s.homepage = "http://www.joelstrait.com/"
  s.rubygems_version = "2.4.5.1"
  s.summary = "A pure Ruby library for reading and writing Wave sound files (*.wav)"
end

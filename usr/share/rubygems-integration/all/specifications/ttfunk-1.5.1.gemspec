# -*- encoding: utf-8 -*-
# stub: ttfunk 1.5.1 ruby lib

Gem::Specification.new do |s|
  s.name = "ttfunk".freeze
  s.version = "1.5.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Gregory Brown".freeze, "Brad Ediger".freeze, "Daniel Nelson".freeze, "Jonathan Greenberg".freeze, "James Healy".freeze]
  s.cert_chain = ["certs/pointlessone.pem".freeze]
  s.date = "2018-09-30"
  s.description = "Font Metrics Parser for the Prawn PDF generator".freeze
  s.email = ["gregory.t.brown@gmail.com".freeze, "brad@bradediger.com".freeze, "dnelson@bluejade.com".freeze, "greenberg@entryway.net".freeze, "jimmy@deefa.com".freeze]
  s.files = ["CHANGELOG.md".freeze, "COPYING".freeze, "GPLv2".freeze, "GPLv3".freeze, "LICENSE".freeze, "README.md".freeze, "lib/ttfunk".freeze, "lib/ttfunk.rb".freeze, "lib/ttfunk/collection.rb".freeze, "lib/ttfunk/directory.rb".freeze, "lib/ttfunk/encoding".freeze, "lib/ttfunk/encoding/mac_roman.rb".freeze, "lib/ttfunk/encoding/windows_1252.rb".freeze, "lib/ttfunk/reader.rb".freeze, "lib/ttfunk/resource_file.rb".freeze, "lib/ttfunk/subset".freeze, "lib/ttfunk/subset.rb".freeze, "lib/ttfunk/subset/base.rb".freeze, "lib/ttfunk/subset/mac_roman.rb".freeze, "lib/ttfunk/subset/unicode.rb".freeze, "lib/ttfunk/subset/unicode_8bit.rb".freeze, "lib/ttfunk/subset/windows_1252.rb".freeze, "lib/ttfunk/subset_collection.rb".freeze, "lib/ttfunk/table".freeze, "lib/ttfunk/table.rb".freeze, "lib/ttfunk/table/cmap".freeze, "lib/ttfunk/table/cmap.rb".freeze, "lib/ttfunk/table/cmap/format00.rb".freeze, "lib/ttfunk/table/cmap/format04.rb".freeze, "lib/ttfunk/table/cmap/format06.rb".freeze, "lib/ttfunk/table/cmap/format10.rb".freeze, "lib/ttfunk/table/cmap/format12.rb".freeze, "lib/ttfunk/table/cmap/subtable.rb".freeze, "lib/ttfunk/table/glyf".freeze, "lib/ttfunk/table/glyf.rb".freeze, "lib/ttfunk/table/glyf/compound.rb".freeze, "lib/ttfunk/table/glyf/simple.rb".freeze, "lib/ttfunk/table/head.rb".freeze, "lib/ttfunk/table/hhea.rb".freeze, "lib/ttfunk/table/hmtx.rb".freeze, "lib/ttfunk/table/kern".freeze, "lib/ttfunk/table/kern.rb".freeze, "lib/ttfunk/table/kern/format0.rb".freeze, "lib/ttfunk/table/loca.rb".freeze, "lib/ttfunk/table/maxp.rb".freeze, "lib/ttfunk/table/name.rb".freeze, "lib/ttfunk/table/os2.rb".freeze, "lib/ttfunk/table/post".freeze, "lib/ttfunk/table/post.rb".freeze, "lib/ttfunk/table/post/format10.rb".freeze, "lib/ttfunk/table/post/format20.rb".freeze, "lib/ttfunk/table/post/format30.rb".freeze, "lib/ttfunk/table/post/format40.rb".freeze, "lib/ttfunk/table/sbix.rb".freeze, "lib/ttfunk/table/simple.rb".freeze]
  s.homepage = "https://prawnpdf.org".freeze
  s.licenses = ["Nonstandard".freeze, "GPL-2.0".freeze, "GPL-3.0".freeze]
  s.required_ruby_version = Gem::Requirement.new("~> 2.1".freeze)
  s.rubygems_version = "2.7.6".freeze
  s.summary = "TrueType Font Metrics Parser".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>.freeze, ["~> 12"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.5"])
      s.add_development_dependency(%q<rubocop>.freeze, ["~> 0.46"])
      s.add_development_dependency(%q<yard>.freeze, ["~> 0.9"])
    else
      s.add_dependency(%q<rake>.freeze, ["~> 12"])
      s.add_dependency(%q<rspec>.freeze, ["~> 3.5"])
      s.add_dependency(%q<rubocop>.freeze, ["~> 0.46"])
      s.add_dependency(%q<yard>.freeze, ["~> 0.9"])
    end
  else
    s.add_dependency(%q<rake>.freeze, ["~> 12"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.5"])
    s.add_dependency(%q<rubocop>.freeze, ["~> 0.46"])
    s.add_dependency(%q<yard>.freeze, ["~> 0.9"])
  end
end

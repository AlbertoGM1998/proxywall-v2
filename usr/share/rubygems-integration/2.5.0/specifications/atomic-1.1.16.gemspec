# -*- encoding: utf-8 -*-
# stub: atomic 1.1.16 ruby lib
# stub: ext/extconf.rb

Gem::Specification.new do |s|
  s.name = "atomic".freeze
  s.version = "1.1.16"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Charles Oliver Nutter".freeze, "MenTaLguY".freeze, "Sokolov Yura".freeze]
  s.date = "2014-04-08"
  s.description = "An atomic reference implementation for JRuby, Rubinius, and MRI".freeze
  s.email = ["headius@headius.com".freeze, "mental@rydia.net".freeze, "funny.falcon@gmail.com".freeze]
  s.extensions = ["ext/extconf.rb".freeze]
  s.files = [".gitignore".freeze, ".travis.yml".freeze, "Gemfile".freeze, "LICENSE".freeze, "README.md".freeze, "Rakefile".freeze, "atomic.gemspec".freeze, "examples/atomic_example.rb".freeze, "examples/bench_atomic.rb".freeze, "examples/bench_atomic_1.rb".freeze, "examples/graph_atomic_bench.rb".freeze, "ext/AtomicReferenceService.java".freeze, "ext/atomic_reference.c".freeze, "ext/extconf.rb".freeze, "ext/org/jruby/ext/atomic/AtomicReferenceLibrary.java".freeze, "lib/atomic.rb".freeze, "lib/atomic/concurrent_update_error.rb".freeze, "lib/atomic/delegated_update.rb".freeze, "lib/atomic/direct_update.rb".freeze, "lib/atomic/fallback.rb".freeze, "lib/atomic/jruby.rb".freeze, "lib/atomic/numeric_cas_wrapper.rb".freeze, "lib/atomic/rbx.rb".freeze, "lib/atomic/ruby.rb".freeze, "test/test_atomic.rb".freeze]
  s.homepage = "http://github.com/headius/ruby-atomic".freeze
  s.licenses = ["Apache-2.0".freeze]
  s.rubygems_version = "2.7.6".freeze
  s.summary = "An atomic reference implementation for JRuby, Rubinius, and MRI".freeze
  s.test_files = ["test/test_atomic.rb".freeze]
end

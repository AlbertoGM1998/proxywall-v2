��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u     +  #   8     \     n     �     �  	   �     �     �     �     �     �     �     �     �          3     B     O     ]     w     �     �     �     �     �     �     �     �  @     7   M  =   �     �     �     �     �  "   �          ,     B  
   \     g     ~  	   �     �     �     �     �     �     �  
          
   "  	   -     7  
   C     N     ^     e     t     z     �     �     �  	   �     �     �     �     �             #   !  	   E  	   O     Y     g     z     �  ?   �  	   �  
   �  	   �     �  
   �     
          )     1     A     I     O  
   V     a     e     l      s  ,   �     �                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal 0.1.9
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-25 07:04+0000
Last-Translator: Piotr Strębski <strebski@o2.pl>
Language-Team: Polish <>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495695856.212557
 Zaawansowane Zezwalanie na pogrubioną czcionkę Dzwonek systemowy Kolor drugoplanowy: Czarny Blok Niebieski Na dole Jasnoniebieski Jasny niebieskozielony Jasnozielony Jasnopurpurowy Jasnoczerwony Brązowy Wy_czyść bufor przewijania Wy_czyść bufor przewijania Zamknij kartę Zamknij okno Zamknij _okno Potwierdzenie zamknięcia _Skopiuj Kopiuj Kopiuj adres _URL Prawa autorskie (C) 2008-2017 Miganie kursora Styl kursora: Niebieskozielony Ciemnoszary Domyślna wielkość okna Wyłącz potwierdzenie przed zamknięciem okna z wieloma kartami Wyłączenie skrótu klawiszowego menu (domyślnie F10) Wyłączenie skrótu klawiszowego przełączania kart (Alt-n) Wyświetlanie Kolor pierwszoplanowy: Szary Zielony Ukrycie przycisków zamykania kart Ukrycie paska menu Ukryj wskaźnik myszy Ukrycie paska przewijania LXTerminal Preferencje LXTerminal Po lewej Purpurowy Przemieść kartę w lewo Przemieść kartę w prawo Przemieść w _lewo Przemieść w _prawo Zmień _nazwę Zmiana nazwy _Następna Nowa k_arta Nowa karta Nowe okno Nowa _karta Nowe _okno Następna karta Paleta Nastawa palety Wklej _Poprzednia P_referencje Poprzednia karta Czerwony Po prawej Ilość buforowanych wierszy: Znaki zaznaczanych słów: Skróty Wygląd Położenie paska kart: Terminal Emuluje terminal w środowisku LXDE Czcionka: Na górze Podkreślenie Emulator terminala Biały Żółty Zamierzasz zamknąć %d kart. Czy na pewno chcesz kontynuować? Powiększ Po_mniejsz Pomniejsz Przywróć wielkość P_owiększ P_rzywróć widok _O programie _Anuluj Za_mknij kartę _Edycja _Plik Pomo_c _Nowe okno _OK _Wklej _Karty konsola;wiersz poleceń;wykonaj; Grzegorz Gibas
Piotr Sokół
Piotr Strębski x 
��    +      t  ;   �      �     �     �     �     �     �     �  4   �  ]   3     �     �     �     �  *   �          .     F     U     e  %   v     �  -   �  ;   �     !     ;     @  1   P     �     �     �     �     �     �     �     �  2     /   I  ;   y  g   �  1        O     V     ]  �  e     �	     �	  
   
  "   "
  7   E
     }
  �   �
  �   -     �  R   �  T   )  2   ~  D   �  4   �  =   +     i  :   �  %   �  :   �  O   !  R   q  �   �  ;   M  
   �  -   �  p   �      3  !   T  :   v      �     �  -   �  .         K  C   l  \   �  N       \  m   d     �  
   �     �         (           '                      #      !          )                              &   +             %            	                          *   
                      "                          $    * Apply _effect: Border Border Effect C_opy to Clipboard Drop shadow Effect to add to the border (shadow, border or none) Effect to add to the outside of a border. Possible values are "shadow", "none", and "border". Effects Error loading the help page Error while saving screenshot Grab _after a delay of Grab a window instead of the entire screen Grab the current _window Grab the whole _desktop Include Border Include Pointer Include _pointer Include the pointer in the screenshot Include the window _border Include the window border with the screenshot Include the window manager border along with the screenshot Interactively set options None Save Screenshot Save images of your desktop or individual windows Save in _folder: Screenshot delay Screenshot directory Screenshot.png Select a folder Take Screenshot Take _Screenshot Take a picture of the screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. The number of seconds to wait before taking the screenshot. UI definition file for the screenshot program is missing.
Please check your installation of gnome-utils Unable to take a screenshot of the current window _Name: effect seconds Project-Id-Version: gnome-utils.HEAD.mk
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2008-09-25 00:38+0200
Last-Translator: Jovan Naumovski <jovan@lugola.net>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural= n==1 || n%10==1 ? 0 : 1
X-Generator: KBabel 1.11.4
 * Примени _ефект: Рамка Ефект на границата К_опирај во таблата со исечоци Фрлај сенка Ефект кој што треба да биде додаден на рамката на прозорецот (сенка, рамка или ништо) Ефекти за додавање на надворешната рамка. Можни вредности се "сенка", "ништо" и "рамка". Ефекти Грешка во вчитувањето на страницата за помош Грешка при зачувувањето на сликата од екранот Сними _после задоцнување од Сликај прозорец наместо целиот екран Сними го тековниот _прозорец Сними ја целата _работна површина Вклучи граница Вклучи ја стрелката од глувчето Сликај ја _стрелката Сликај ја стрелката од глувчето Вклучи ја _рамката на прозорецот во сликата Вклучи ја границата на прозорецот во сликата Вклучи ја границата на менаџерот за прозорци заедно со сликата од екранот. Интерактивно постави ги опциите Ништо Зачувај слика од екранот Зачувај слики од работната површина или од одредени прозорци Зачувај во _папка: Задоцнето сликање Директориум за слики од екранот СликаОдЕкранот.png Одберете папка Зачувај слика од екранот Зачувај _слика од екранот Сликај го екранот Сликај по одредено време [во секунди] Директориумот во кој што е снимано последниот пат. Бројот на секунди пред да се слика екранот. Недостасува датотеката со дефиниции за графичкиот интерфејс на програмата за сликање на екранот.
Ве молам проверете ја инсталацијата на gnome-utils Не можам да направам слика од екранот од тековниот прозорец _Име: ефект секунди 
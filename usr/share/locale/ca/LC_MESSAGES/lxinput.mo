��          �   %   �      `     a     y     �     �  -   �  2   �  &        4     9     >      W     x     �     �  /   �     �     �     �     �     �       7        I     O  	   T  8   ^  �  �     #     C     b     r  4     <   �  .   �           '  &   +  '   R     z     �     �  >   �     �     �     �     �          !  @   /     p     u     z  K   �                         	                               
                                                                    <b>Character Repeat</b> <b>Keyboard layout</b> <b>Motion</b> Acceleration: Beep when there is an error of keyboard input Configure keyboard, mouse, and other input devices Delay before each key starts repeating Fast High Input Device Preferences Interval between each key repeat Keyboard Keyboard and Mouse LXInput autostart Left handed (Swap left and right mouse buttons) Long Low Mouse Repeat delay: Repeat interval: Sensitivity: Setup keyboard and mouse using settings done in LXInput Short Slow Touch Pad Type in the following box to test your keyboard settings Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-08-16 11:05+0000
Last-Translator: Davidmp <rbndavid@gmail.com>
Language-Team: LXDE Catalan translators group
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1439723143.000000
 <b>Repetició de caràcters</b> </b>Disposició del teclat</b> <b>Moviment</b> Acceleració Pita quan es detecti un error en l'entrada de teclat Configureu el teclat, ratolí i altres dispositius d'entrada Demora abans de començar a repetir les tecles Ràpid Alt Preferències del dispositiu d'entrada Intèrval entre la repetició de tecles Teclat Teclat i ratolí Autoinici d'LXInput Esquerrà (intercanvia els botons dret i esquerra del ratolí) Llarg Baix Ratolí Repeteix la demora: Repeteix l'interval: Sensibilitat: Configura el teclat i ratolí amb els paràmetres fets a LXInput Curt Lent Touchpad Introduïu text a la següent caixa per verificar els paràmetres de teclat 
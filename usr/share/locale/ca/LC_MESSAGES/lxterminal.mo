��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u     �     �          !     &     ,     2     7     >     L     Z     h     y     �     �     �     �     �     �                    %     2     J     `     q  	   v     �  O   �  =   �  7   .     f  
   o     z          �     �     �      �  
   �                (     0     M     h     �     �     �     �     �     �     �               #     5     <     Z     b     u     �     �  
   �     �  #   �     �     �      �       &   &     M     j  	   q     {     �     �  ?   �     �     �     �     �               *     3     ?     R     Y     a     h     w     �  
   �  !   �     �     �                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-14 14:45+0000
Last-Translator: Davidmp <rbndavid@gmail.com>
Language-Team: Catalan <josep.sanchez@ubuntu.cat>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1505400312.088160
 Avançat Permet la negreta Campana audible Fons Negre Bloca Blau A baix Blau brillant Cian brillant Verd brillant Magenta brillant Vermell brillant Marró _Neteja el desplaçament Neteja el desplaçament e_nrere Tanca la pestanya Tanca la finestra Tanca la _finestra Confirmeu el tancament _Copia Copia Copia l'_URL Copyright (C) 2008-2017 Parpelleig del cursor Estil del cursor Cian Gris fosc Mida de la finestra per defecte Inhabilita la confirmació abans de tancar una finestra amb més d'una pestanya Inhabilita les dreceres de teclat del menú (F10 per defecte) Inhabilita l'ús d'Alt-n per a les pestanyes i el menú Pantalla Primer pla Gris Verd Amaga els botons de tancar Amaga la barra del menú Amaga el cursor del ratolí Amaga la barra del desplaçament LXTerminal Preferències d'LXTerminal A l'esquerra Magenta Mou la pestanya a l'esquerra Mou la pestanya a la dreta Mou la pestanya a _l'esquerra Mou la pestanya a la d_reta Anomena pesta_nya Anomena la pestanya Pestaya següe_nt Pes_tanya nova Pestanya nova Finestra nova Pestanya _nova Finestra _nova Pestanya següent Paleta Preconfiguració de la paleta Enganxa _Pestanya anterior Preferèncie_s Pestanya anterior Vermell A la dreta Línies de desplaçament enrere Caràcters de selecció per paraula Dreceres Estil Posició del plafó de pestanyes Terminal Emulador de terminal del projecte LXDE Tipus de lletra del terminal A dalt Subratlla Utilitzeu la línia d'ordres Blanc Groc Esteu a punt de tancar %d pestanyes. Segur que voleu continuar? Amplia Red_ueix Redueix Restableix el zoom Ampl_ia _Restableix el zoom Quant _a _Cancel·la Tan_ca la pestanya _Edita _Fitxer _Ajuda Finestra _nova _D'acord _Enganxa Pes_tanyes consola; línia d'ordres; executa Crèdits dels traductors x 
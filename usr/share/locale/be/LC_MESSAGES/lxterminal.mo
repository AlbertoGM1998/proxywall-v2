��    3      �  G   L      h     i     r     �  
   �     �     �     �     �     �  	   �     �     �  *   �  %         F  
   N     Y     l     z  
   �     �     �     �     �  	   �     �  	   �     �     �                    #     4     N     T     g  "   p     �     �  	   �     �     �  
   �     �     �     �     �     �     �  �       �  (   �     �     	     (	  
   1	  -   <	  /   j	     �	     �	  !   �	     �	  J   
  O   Q
     �
     �
  ,   �
  "   �
  .     
   E  $   P  
   u  /   �  1   �     �     �           <     T  "   k     �     �     �  8   �  
     0        G  >   X     �     �     �  #   �               9  	   E     O     _     q  q                        !                        #                           1                    
      (   0   2   -   )      .                          $                 /          +      3   &       %   "   	                            ,           '   *    Advanced Allow bold font Audible bell Background Block Bottom Cl_ear scrollback Clear scr_ollback Cop_y Copy _URL Cursor blink Cursor style Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Hide Close buttons Hide menu bar Hide scroll bar LXTerminal LXTerminal Preferences Left Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New _Tab New _Window Pre_vious Tab Preference_s Right Scrollback lines Select-by-word characters Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line _About _Close Tab _Edit _File _Help _Paste _Tabs translator-credits Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-07-08 19:15+0000
Last-Translator: system user <>
Language-Team: LANGUAGE <LL@li.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1404846909.000000
 Дэталі Дазволіць цёмны шрыфт Гукавы званок Колер фону Блок Знізу С_кінуць гісторыю вываду Ачысціць гісторыю выва_ду _Капіраваць Капіраваць _URL Мігаценне курсора Стыль курсора Адключыць хуткі выклік меню (звычайна F10) Адключыць пераключэнне па Alt-n картак і меню Выгляд Колер тэксту Схаваць кнопку закрыцця Схаваць радок меню Схаваць стужку прагорткі LXTerminal Настаўленні LXTerminal-а Злева Перамясціць картку ў_лева Перамясціць картку ў_права Назвац_ь картку Назваць картку На_ступная картка Новая картка Новае _вакно П_апярэдняя картка Настаўленн_і Справа Гісторыя, радкоў Сімвалы, якія выдзяляюць словы Стыль Размяшчэнне панэлі картак Тэрмінал Эмулятар тэрмінала для праекта LXDE Шрыфт тэрмінала Зверху Падкрэсліванне Эмулятар тэрмінала _Пра праграму _Закрыць картку _Праца Ф_айл _Даведка _Уставіць _Карткі Анік Кіна <son_of_the_photographer@tut.by>, 2010
Мікалай Удодаў <crom-a@tut.by>, 2011, 2012. 
��          �   %   �      p  :   q     �     �      �       !     j   >  F   �  [   �     L  $   j     �     �     �     �     �  �   �     �     �  	   �  #   �  O   �  �     $   �  #   �            �  ,  ?   	  !   X	  (   z	  #   �	  !   �	  3   �	  �   
  z   �
  �   i  :   �  >   9  >   x  %   �  '   �  '        -  �   :     #     =     F  5   Y  v   �  !    @   (  8   i     �  ,   �                        
                                                                         	                          "%s" does not appear to be a valid Openbox theme directory "%s" was installed to %s "%s" was successfully created <span weight="bold">Theme</span> Choose an Openbox theme Create a theme _archive (.obt)... Error while parsing the Openbox configuration file. Your configuration file is not valid XML.

Message: %s Failed to load an rc.xml. Openbox is probably not installed correctly. Failed to load the obconf.glade interface file. ObConf is probably not installed correctly. Font for active window title: Font for inactive on-screen display: Font for inactive window title: Font for menu Item: Font for menu header: Font for on-screen display: Misc. N: Window icon
L: Window label (Title)
I: Iconify (Minimize)
M: Maximize
C: Close
S: Shade (Roll up)
D: Omnipresent (On all desktops) Openbox theme archives Theme Title Bar Unable to create directory "%s": %s Unable to create the theme archive "%s".
The following errors were reported:
%s Unable to extract the file "%s".
Please ensure that "%s" is writable and that the file is a valid Openbox theme archive.
The following errors were reported:
%s Unable to move to directory "%s": %s Unable to run the "tar" command: %s _Button order: _Install a new theme... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-11-13 23:54+0100
PO-Revision-Date: 2015-08-17 01:28+0000
Last-Translator: Anonymous Pootle User
Language-Team: LANGUAGE <LL@li.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1439774917.101743
 "%s" не з'яўляецца каталогам тэм Openbox "%s" устаноўлена ў %s Паспяхова створана "%s" <span weight="bold">Тэма</span> Выберыце тэму Openbox Стварэнне _архіва тэмы (.obt)... Памылка падчас разбору канфігурацыйнага файла Openbox. Файл канфігурацыі не ўзгоднены з фарматам XML.

Паведамленне: %s Не ўдалося прачытаць файл rc.xml. Магчыма, Openbox не ўстаноўлены як след. Не ўдалося прачытаць файл інтэрфейсу obconf.glade. Магчыма, ObConf не ўстаноўлены як след. Шрыфт загалоўка актыўнага акна: Шрыфт неактыўнага экраннага меню: Шрыфт загалоўка неактыўнага акна: Шрыфт элемента меню: Шрыфт загалоўка меню: Шрыфт экраннага меню: Рознае N: Значок
L: Загаловак акна
I: Мінімізаваць у значок
M: Максімізаваць
C: Закрыць
S: Скруціць
D: Паказваць на ўсіх працоўных прасторах Архіў тэм Openbox Тэма Загаловак Нельга стварыць каталог "%s": %s Немагчыма стварыць архіў тэмы "%s".
Здарылася наступная памылка:
%s Немагчыма распакаваць файл "%s".
Калі ласка, упэўніцеся, што "%s" адкрыты для запісу і з'яўляецца правільным файлам архіва тэмы Openbox.
Адбылася наступная памылка:
%s Немагчыма перанесці ў каталог "%s": %s Немагчыма выканаць загад "tar": %s _Парадак кнопак: _Устанавіць новую тэму... 
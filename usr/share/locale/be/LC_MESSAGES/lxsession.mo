��          �   %   �      `  �   a     �       :        I  "   X     {  o   �  $        -     F  
   N  	   Y  	   c  -   m  
   �     �     �  	   �     �  
   �     �     �     �  
          �    �   �     �     �  �        �  <   �      �  �   	  =   �	  #   
     '
     8
     L
     [
  O   w
     �
  !   �
  *   �
     #     7     V     i     w     �     �     �        
                                                                                                 	                   <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Applications automatically started after entering desktop: Authentication Automatically Started Applications Banner to show on the dialog Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Custom message to show on the dialog Desktop Session Settings Enabled Error: %s
 Group: %s Identity: Manage applications loaded in desktop session Password:  Position of the banner S_witch User Sh_utdown Window Manager: _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-12-05 13:01+0000
Last-Translator: system user <>
Language-Team: LANGUAGE <LL@li.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1417784514.000000
 <b>Увага:</b> <b>НЕ</b> чапайце гэта, калі вы не ведаеце дакладна, што вы робіце.

<b>Заўвага:</b> Настаўленні набудуць моц пасля наступнага ўваходу. Адмысловыя опцыі Праграма Праграмы, якія запускаюцца аўтаматычна па ўваходзе на працоўную прастору: Аўтарызацыя Аўтаматычна запусканыя праграмы Выява для дыялога Загадны радок, ужываны для запуску ваконнага менеджара
(Звычайны кіраўнік акон для LXDE мусіць быць openbox-lxde) Уласнае паведамленне для дыялога Настаўленні сеанса Уключана Памылка: %s
 Група: %s Ідэнтыфікатар: Кіраванне праграмамі, загружанымі ў сеансе Пароль:  Размяшчэнне выявы З_мяніць карыстальніка Вы_ключыць Аконны менеджар: _Рэмым сну _Выйсці _Перазапусціць _Рэжым чакання файл выявы паведамленне 
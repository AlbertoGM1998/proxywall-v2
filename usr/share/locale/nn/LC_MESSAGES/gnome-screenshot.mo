��    #      4  /   L           	               !     /  4   ;  ]   p     �     �     �  *   	     4     M     e     t  -   �  ;   �     �            1   (     Z     k     �     �     �     �     �  2   �  /     1   @     r     y     �  u  �     �                     #  B   *  h   m     �     �     �  -   	  '   H	  *   p	     �	     �	      �	  0   �	     
     %
     *
  ;   =
     y
     �
     �
     �
     �
     �
     �
  4     )   6  0   `     �     �     �                                  #   !                                                                                              
      	      "              * Apply _effect: Border Border Effect Drop shadow Effect to add to the border (shadow, border or none) Effect to add to the outside of a border. Possible values are "shadow", "none", and "border". Effects Error loading the help page Grab _after a delay of Grab a window instead of the entire screen Grab the current _window Grab the whole _desktop Include Border Include the window _border Include the window border with the screenshot Include the window manager border along with the screenshot Interactively set options None Save Screenshot Save images of your desktop or individual windows Save in _folder: Screenshot directory Screenshot.png Select a folder Take Screenshot Take _Screenshot Take a picture of the screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. Unable to take a screenshot of the current window _Name: effect seconds Project-Id-Version: nn
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2008-04-01 08:41+0200
Last-Translator: Eskild Hustvedt <eskildh@gnome.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms:  nplurals=2; plural=(n != 1);
X-Generator: MagicPO 0.3
 * Bruk _effekt: Kant Effekt for kant Skugge Effekt som skal brukast på vinduskanten (skugge, kant eller inga) Effekt som skal leggjes til på utsida av kanten. Moglege verdiar er «shadow», «none» og «border». Effektar Feil under lasting av hjelpsida T_a eit skjermbilete etter Dump eit vindauge i staden for heile skjermen Ta eit skjermbilete av aktivt _vindauge Ta eit skjermbilete av heile skrivebor_det Ta med kant Ta med vin_daugekant Ta med vinduskant i skjermbilete Ta med vindaugehandsamarens kant i skjermbilete. Spør etter alternativ Inga Lagra skjermbilete Lagra bileta av ditt skrivebord eller individuelle vindauge Lagra i _mappe: Mappe for skjermbilete Skjermbilete.png Vel ei mappe Ta eit skjermbilete Ta eit _skjermbilete Ta eit bilete av skjermen Ta eit skjermbilete etter oppgjeven pause [i sekund] Mappe kor førre skjermbilete vart lagra. Kan ikkje ta eit skjermbilete av dette vindauget _Namn: effekt sekund 
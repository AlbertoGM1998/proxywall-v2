��          L      |       �   /   �      �   =   �   +   5     a  �  i  >   �  *   8  G   c  '   �  
   �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte 0
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-09-02 20:28+0200
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <lokalizacija@linux.hr>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-09-15 11:47+0000
X-Generator: Poedit 2.1.1
 Greška (%s) pri pretvorbi podataka za nadređenog, odbacujem. Greška prilikom čitanja nadređenog: %s. GnuTLS nije omogućen; podaci će biti zapisivani nešifrirano na disk! Nemoguća pretvorba znakova iz %s u %s. UPOZORENJE 
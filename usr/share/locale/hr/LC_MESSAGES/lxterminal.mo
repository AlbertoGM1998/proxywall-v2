��    T      �  q   \            !     *  
   :     E     K     Q     V     ]  
   i     t  	   z     �     �     �     �     �  	   �     �     �     �     �     �       *     %   A     g  
   o     z          �     �     �  
   �     �     �     �     �     �     	     	  	   !	     +	  	   4	     >	     G	  
   O	     Z	     c	     o	     x	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	     
  "   
     2
     @
  	   D
     N
     c
     i
  B   p
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
          -  �  /     �               #     (     1     7     ;     G     V     ]     m     |     �     �     �     �     �     �     �       	          0   *  1   [     �     �     �     �     �     �     �  
   �     �                     7     M     e     |     �     �     �     �     �     �     �     �     �               !     -     ?     F     L     \     z     �     �     �  "   �     �     �  	   �     �          
  B        S  	   _     i     z  	   �     �     �     �  	   �     �      �  #   �                          4   *   T   D   N      Q                           A   K   C       
   2   +      1                    	           M      G   H   ,   0      9                E      L   '       @   ;      "              8       !   =   J   :   O                  ?   &   #   F      >   S   7       R              B   $   I              P      )      .       (      <      3       -         5           6           /   %    Advanced Allow bold font Background Black Block Blue Bottom Bright Blue Bright Red Brown Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: LXterminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-28 20:47+0000
Last-Translator: Ivica Kolić <ikoli@yahoo.com>
Language-Team: <asjlob AT vip.hr> <>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1506631674.479948
 Napredno Dopusti podebljani font Pozadina Crno Blokiraj Plavo Dno Jasno plava Svijetlocrveno Smeđe Zatvori karticu Zatvori prozor Zatvori _prozor Potvrdi zatvaranje Kopira_j Kopiraj Kopiraj _URL Autorska prava (C) 2008-2017 Treptanje pokazivača Stil pokazivača Cijan Tamnosiva Zadana veličina prozora Onemogući tipku prečaca izbornika (F10 zadano) Onemogući koristeći Alt-n za kartice i izbornik Zaslon Prednji plan Sivo Zeleno Sakrij traku izbornika Sakrij pokazivač miša Sakrij kliznu traku LXterminal LXTerminal osobitosti Lijevo Magenta Pomakni karticu lijevo Pomakni karticu desno Pomakni karticu _lijevo Pomakni karticu _desno I_me kartice Ime kartice Sli_jedeća kartica Nova k_artica Nova kartica Novi Prozor Nova _kartica Novi _prozor Slijedeća kartica Paleta Zalijepi Pret_hodna kartica Osobitost_i Prethodna kartica Crveno Desno Povratne linije Odabrano po znakovima riječi Prečaci Stil Pozicija kartične ploče Terminal Emulator terminala za LXDE projekt Font terminala Vrh Podcrtano Koristi naredbeni redak Bijelo Žuto Zatvoroti ćete %d kartica. Jeste li sigurni da želite nastaviti? _O programu _Odustani _Zatvori karticu _Uredi _Datoteka _Pomoć _Novi prozor _U redu _Zalijepi _Kartice konzola;naredbeni redak;izvrši; Boljsa Ivica Kolić ikoli@yahoo.com x 
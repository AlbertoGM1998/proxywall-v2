��    z      �  �   �      H
  (   I
     r
     ~
     �
     �
     �
  �   �
     d     u  @   �  0   �     �       ?   *     j  2   �     �  /   �     	  3   &     Z     t     z     �     �     �  &   �  "   �  	   �          !     >  R   U     �  	   �  
   �     �     �  '   �  o         �     �     �  $   �  "   �     
          3  "   ;     ^     g     m     u  
   {  3   �     �  w   �  	   ?  	   I     S     `     l     �     �     �     �     �     �  -   �       :   %  !   `  %   �  j   �          "     .     3     9     >     J  
   R     ]  
   c     n     �     �     �     �     �  M   �               9  V   U     �  	   �     �  +   �     �     
  %        <     A     [  >   k  D   �     �     �  
   	               +     ;     M  
   T     _     g     o  
   x     �  �  �  '   Y     �     �     �     �     �  �   �     x     �  D   �  0   �            @   <  $   }  5   �      �  8   �  "   2  @   U     �  	   �     �     �     �     �  &   �          3     B     \     w  K   �     �  	   �  
   �     
       !   &  �   H     �     �     �  '   �     %     A     ^  
   ~  %   �     �     �  
   �     �     �  5   �       �   )  	   �  
   �     �     �     �     �                 5      I      R   >   q      �   A   �   '   !  1   6!  v   h!     �!     �!     "     "     "     "     7"     >"     J"  	   Q"     ["     q"     �"     �"     �"     �"  \   �"     #     5#     L#  d   k#     �#     �#     �#  -   �#     $     3$  ,   E$     r$  !   x$     �$  =   �$  D   �$  	   4%     >%     L%  
   [%     f%     z%     �%     �%     �%  
   �%     �%     �%     �%     �%     i   B   f   @       2       e   K   P   Y      m       I   	              `                 0          S      a   g   q   C           R   9   x   A   k          *   +   W   ,   r      o   v   J   c   :   7      3   Z   n   X       z   F      E   d   u                 =       %       .   ;   h   L   /       <   s          b          ^   O   &   6   G              4            ?   5   >   y   #       !         D         N      $   )       Q       [   _   T       "   (   -               t      j   l              \              H                   ]   p              M   
   U           '   w       1   V       8       <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Known Applications </b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Application for taking screeshot of your desktop, like scrot ... Application to create archives, like file-roller Application to display images Application to edit text Application to go to Internet, Google, Facebook, debian.org ... Application to lock your screen Application to manage bittorent, like transmission Application to manage disks Application to manage office text, like abiword Application to manage webcam Application to monitor tasks running on your system Application to send mails Apply Archive Audio application Audio manager Authentication Authentication failed!
Wrong password? Automatically Started Applications Autostart Autostart the application ? Autostart the application ?
 Available applications Available applications : Applications of this type available on your repositories
 Banner to show on the dialog Bittorent Calculator Calculator application Cancel Change the default applications on LXDE Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Communication 1 Communication 2 Core applications Custom message to show on the dialog Default applications for LXSession Desktop Manager Desktop Session Settings Disable Disable autostarted applications ? Document Email Enabled Error Error: %s
 Extra: Add an extra parameter to the launch option
 File Manager File manager is the component which open the files.
See "More" to add options to handle the desktop, or openning files  Group: %s Identity: Image viewer Information LXSession configuration L_ock Screen Laptop mode Launcher manager Launching applications Layout Lock screen manager Manage applications loaded in desktop session Managing proxy support Managing the application to update and upgrade your system Managing your audio configuration Managing your workspace configuration Manual Settings: Manually sets the command (you need to restart lxsession-default-apps to see the change)
 Manual setting Menu prefix Mode Model More Network GUI Options PDF Reader Panel Password:  Position of the banner Power Manager Proxy Reload S_witch User Screensaver Screensaver is a program which displays animations when your computer is idle Screenshot manager Session : specify the session
 Set debian default programs Set default program for Debian system (using update-alternatives, need root password)
 Settings Sh_utdown Tasks monitor Terminal by default to launch command line. Terminal manager Text editor The database is updating, please wait Type Update lxsession database Upgrade manager Use a communication software (an IRC client, an IM client ...) Use another communication software (an IRC client, an IM client ...) Variant Video application Webbrowser Webcam Window Manager: Windows Manager Workspace manager Xrandr _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession-lite_hr
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-30 16:46+0000
Last-Translator: Ivica Kolić <ikoli@yahoo.com>
Language-Team: <asjlob AT vip.hr>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1506789964.898622
 <b><big>Odjavi %s %s sesiju ?</big></b> <b>Dbus</b> <b>Okruženje</b> <b>Opće postavke</b> <b>Poznati programi </b> <b>Postavke</b> <b>Upozorenje:</b> Ne<b>MOJ</b> ovo dirati ukoliko doista ne znaš što radiš.

<b>OBAVIJEST:</b> Ove postavke će postati aktivne nakon iduće prijave. Napredne opcije Program Program koji radi slike zaslona vaše radne povešine, kao scrot ... Program za stvaranje arhiva, poput file-roller-a Program za prikaz slika Program za uređivanje teksta Program za odlazak na internet, Google, Facebook, debian.org ... Program koji zaključava vaš zaslon Program za bitorrent upravljanjem, poput transmission Program za upravljanje diskovima Program za upravljanje uredskim tekstom, poput abiword-a Program za upravljanje web kamerom Program koji nadgleda zadatke koji su pokrenuti u vašem sustavu Program za slanje pošte Primijeni Arhiv Audio program Audio upravitelj Autentifikacija Neuspjela provjera!
Pogrešna lozinka? Automatski pokrenuti programi Samopokretanje Samopokretanje programa ? Samopokretanje programa ?
 Dostupni programi Dostupni programi: Programi ovoga tipa dostupni su u vašim repozitorijima
 Prikaži transparent u dijalogu Bittorent Kalkulator Program kalkulatora Otkaži Promjenite zadane programe u LXDE Naredbeni redak korišten za pokretanje upravitelja prozora
(Zadana naredba upravitelja prozora za LXDE trebala bi biti openbox-lxde) Komunikacija 1 Komunikacija 2 Osnovni programi Prikaži prilagođenu poruku u dijalogu Zadani programi za LXSesiju Upravitelj radnom površinom Postavke sesije radne površine Onemogući Onemogućiti samopokrenute programe ? Dokument Email Omogućeno Greška Pogreška %s
 Dodatno: Dodaj dodatni parametar u opciju pokretanja
 Upravitelj datoteka Upravitelj datotekama je komponenta koja otvara datoteke.
Vidi "Više" za dodavanje opcija na radnu površinu, ili otvaranje datoteka Grupa: %s Identitet: Preglednik slika Informacije LXSession konfiguracija Z_akjlučaj zaslon Laptop način rada Upravitelj pokretača Pokretanje programa Raspored Zaključaj upravitelja zaslona Upravljaj programima koji su učitani u sesiji radne površine Upravljanje proxy podrškom Upravljaj programima za ažuriranje i nadogradnju vašega sustava Upravljanje vašom audio konfiguracijom Upravljanje vašom konfiguracijom radnih prostora Ručne postavke: Ručno postavlja naredbu (morate ponovno pokrenuti lxsession-default-apps da biste vidjeli promjenu)
 Ručno postavljanje Prefiks izbornika Način rada Model Više Grafičko sučelje mreže Opcije PDF čitač Ploča Lozinka:  Pozicija transparenta Upravitelj energijom Proxy Ponovo učitaj P_romijeni Korisnika Čuvar zaslona Čuvar zaslona je program koji prikazuje animacije dok je vaše računalo u stanju mirovanja Upravitelj slika zaslona Sesija: odredi sesiju
 Postavi debian zadane programe Postavi zadani program za Debian sustav ( koristeći update-alternatives, potrebna je root lozinka)
 Postavke Ug_asi Nadzor zadataka Terminal kao zadano pokreće naredbeni redak. Upravitelj terminala Uređivač teksta Baza podataka se ažurira, molim pričekajte Vrsta Ažuriraj lxsession bazu podataka Upravitelj nadogradnji Koristi komunikacijski software ( IRC klijent, IM klijent ... Koristi ostali komunikacijski software (IRC klijent, IM klijent ...) Varijanta Video program Web preglednik Web kamera Upravitelj prozora: Upravitelj prozora Upravitelj radnim prostorom Xrandr _Hibernacija _Odjavi se _Pokreni ponovo _Suspendiranje slikovna datoteka poruka 
��          �   %   �      p     q     }  %   �  1   �     �  	   �                     $     :     C     S     Z     n     t  2   �     �     �     �     �  #   �          5     F  	   `     j    {     �  
   �  '   �  /   �     �     	               (     0     E     N     e     j     }     �  9   �     �     �     �        "        1     J     ^     }     �                                    	                              
                                                         Accessories Applications Applications and sites saved from Web Applications that did not fit in other categories Desktop accessories Education Games Games and amusements Graphics Graphics applications Internet Multimedia menu Office Office Applications Other Programming Programs for Internet access such as web and email Small but useful GNOME tools Sound & Video Sundry System Tools System configuration and monitoring Tools for software development Universal Access Universal Access Settings Utilities Web Applications Project-Id-Version: gnome-menus
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-menus&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-09-22 22:06+0200
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.7.1
 Pomagala Aplikacije Aplikacije i stranice spremljene s weba Aplikacije koji ne pripadaju u druge kategorije Pomagala radne površine Obrazovanje Igre Igre i zabava Grafika Grafičke aplikacije Internet Multimedijski izbornik Ured Uredske aplikacije Ostalo Programiranje Programi za pristup Internetu, web sadržaju ili e-pošti Mala ali korisna GNOME pomagala Zvuk i video Ostalo Alati sustava Podešavanje i nadgledanje sustava Alati za razvoj softvera Univerzalni pristup Postavke univerzalnog pristupa Pomagala Web aplikacije 
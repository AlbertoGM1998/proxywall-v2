��    3      �  G   L      h     i     k     z     �     �  M   �  N   �     ?  4   K  ]   �     �     �       *     7   D     |     �  b   �               /  %   @     f  -   �  ;   �     �  ,   �       1   -     _  
   p     {     �     �     �     �     �     �     	     	     %	     6	  '   S	  %   {	  2   �	  /   �	  1   
     6
     =
     D
    L
     g     i     y     ~     �  R   �  R   �     F  ?   U  `   �     �  !   �  #   !  ,   E  3   r     �     �  t   �  
   V     a     p  %   �  )   �  (   �  8   �     2  (   ?     h  3   �     �     �  #   �          +      H     i     �     �     �     �     �       0   *  /   [  J   �  C   �  >        Y     a  
   j            -         +   2         3               )   1           	                           (               !   "                                               0   *         .         '   %         ,   #   $       
   &               /                   * Apply _effect: Border Border Effect C_opy to Clipboard Conflicting options: --area and --delay should not be used at the same time.
 Conflicting options: --window and --area should not be used at the same time.
 Drop shadow Effect to add to the border (shadow, border or none) Effect to add to the outside of a border. Possible values are "shadow", "none", and "border". Effects Error loading the help page Grab _after a delay of Grab a window instead of the entire screen Grab an area of the screen instead of the entire screen Grab the current _window Grab the whole _desktop Impossible to save the screenshot to %s.
 Error was %s.
 Please choose another location and retry. Include Border Include Pointer Include _pointer Include the pointer in the screenshot Include the window _border Include the window border with the screenshot Include the window manager border along with the screenshot None Remove the window border from the screenshot Save Screenshot Save images of your desktop or individual windows Save in _folder: Screenshot Screenshot at %s - %d.png Screenshot at %s.png Screenshot delay Screenshot directory Screenshot taken Screenshot.png Select _area to grab Select a folder Take Screenshot Take _Screenshot Take a picture of the screen Take a screenshot of the current window Take a screenshot of the whole screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. Unable to take a screenshot of the current window _Name: effect seconds Project-Id-Version: gnome-utils
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screenshot&keywords=I18N+L10N&component=general
PO-Revision-Date: 2012-01-17 15:05+0200
Last-Translator: Andiswa Mvanyashe <andiswamva@webmail.co.za>
Language-Team: translate-discuss-xh@lists.sourceforge.net
Language: xh
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Virtaal 0.7.0
X-DamnedLies-Scope: partial
X-Project-Style: gnome
 * Faka i_siphumo: Umda Isiphumo kumda K_opa kwiclipboard Amacebo aphikisanayo: --area kunye ne---delay akufunekanga zistyenziswa xeshanye.
 Amacebo aphikisanayo: --window kunye ne--area akufunekanga zistyenziswa xeshanye.
 Faka isithunzi Phumeza ngokongeza kumda (isithunzi, umda okanye ungengezi nto) Isiphumo sokongeza ngaphandle komda. Amaxabiso anokubakhona "isithunzi", "none", kunye "border". Iziphumo Impazamo yokufaka iphepha loncedo Yithi hlasi _mveni kolibaziseko lwe Yithi hlasi ifestile endaweni yeskrini sonke Yithi hlasi indawo yeskrini endaweni yeskrini sonke Yithi hlasi i_festile yongoku Yithi hlasi yonke i-_desktop Akukwazeki ukugcina umfanekiso weskrini ku- %s.
 Impazamo ibiyi- %s.
 Nceda ukhethe enye indawo kwaye uphinde uzamo. Quka nomda Quka isikhombi Quka isi_khombi Quka isikhombisi kumfanekiso weskrini Quka _umda wefestile nomfanekiso weskrini Quka umda wefestile nomfanekiso weskrini Quka umphathi womda wefestile kunye nomfanekiso weskrini Akukho nanye Susa umda wefestile kumfanekiso weskrini Gcina umfanekiso weskrini Gcina umfanekiso wedesktop okanye weefestile nganye Gcina kwisi_qulathi sefayili: Umfanekiso weskrini Umfanekiso weskrini ose-%s - %d.png Umfanekiso weskrini ose-%s.png Libazisa umfanekiso weskrini Isikhombisi somfanekiso weskrini Umfanekiso weskrini othathiwe Umfanekiso weskrini.png Khetha i_ndawo uza kuthi hlasi Khetha uvimba weefayili Thatha umfanekiso yesikrini Thatha u_mfanekiso weskrini Thatha umfanekiso wesikrini Thatha umfanekiso weskrini wefestile njengangoku Thatha umfanekiso wesikrini waso sonke isikrini Thatha umfanekiso weskrini emva kokubambezela okucacisiweyo [ngemizuzwana] Uvimba weefayili wokuba umfanekiso weskrini ugqibele ukugcina nini. Akukwazeki ukuthatha umfanekiso weskrini wefestile njengangoku _Igama: isiphumo imizuzwana 
��    V      �     |      x  	   y  	   �  	   �     �  	   �  
   �     �     �     �     �     �     �     �          ,     5  
   B     M     Y     n     w          �     �     �     �     �     �     �  	   �  	   �  
   	     	     "	     3	     G	     Z	     a	     o	     t	     }	  	   �	     �	  
   �	  /   �	  3   �	  	   
     
     
     %
     .
     @
     N
     V
     _
     d
     l
     ~
  (   �
     �
  !   �
     �
     �
            	             '     3      8  ,   Y  -   �  (   �     �     �     �     �          	               ,     4     =     D  ~  _  
   �  	   �  
   �     �  	     	     	        $  	   -     7     C     V     i     �  
   �  	   �  	   �  
   �     �     �  
   �     �                    2     L     b  
   e     p  	   v  
   �     �     �     �     �  
   �     �                    "     .     6  <   >  =   {  	   �     �     �  	   �     �     �     
  	          
   "     -     >  -   G     u  #   �     �  	   �     �     �     �     �     �       &     &   2  $   Y  &   ~  
   �     �     �     �     �     �     �     �  	   	  
             '     C                     6   O          A       "      	                     N   2   B           
      4       I   @       G   0           1   D      #                        =           )       !   <   P   F   %      ,   U   Q   -   /   L   J   >      9   K           5                                                      '          &   (       .      8   V   S       *   ;               ?   R   :   H   $           +   T       E                            3      M       7    (seconds) Agreement Available Blocked Canceling Cancelling Category Changes Cleaning up Command line Copying files Debugging Options Details about the update: Directory not found Disabled Distribution Downloaded Downloading Downloading packages Duration Enabled Failed to parse command line False Fatal error File already exists: %s Getting file list Getting updates ID Icon Important Installed Installing Installing Updates Installing files Installing packages Installing updates Key ID Loading cache Name No files Normal Obsoletes Parent Percentage Please logout and login to complete the update. Please restart the computer to complete the update. Real name Remove package Removed Removing Removing packages Repair System Restart Results: Role Running Searching by name Security Show debugging information for all files Show debugging options Show the program version and exit Show version and exit Starting State Status Succeeded Summary System time Text There are no packages to update. There are no updates available at this time. There are no upgrades available at this time. This tool could not find the package: %s Transaction True Type Unknown state Updated Updates Updating Updating packages User ID Username Vendor Waiting for authentication Project-Id-Version: PackageKit
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-25 08:54+0000
Last-Translator: Richard Hughes <richard@hughsie.com>
Language-Team: Esperanto (http://www.transifex.com/freedesktop/packagekit/language/eo/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eo
Plural-Forms: nplurals=2; plural=(n != 1);
 (sekundoj) Permesilo Disponeble Blokite Nuligante Nuligante Kategorio Ŝanĝoj Vakigante Komandlinio Kopiante dosierojn Sencimigaj agordoj Detaloj pri la ĝisdatigo: Ne trovis dosierujon Elŝaltite Distribuo Elŝutite Elŝutante Elŝutante pakaĵojn Daŭro Enŝaltite Fiaskis analizi komanlinion Falsa Gravega eraro Dosiero ne ekzistas: %s Ricevante la dosierliston Akirante ĝisdatigojn ID Piktogramo Grave Instalite Instalante Instalante ĝisdatigojn Instalante dosierojn Instalaante pakaĵojn Instalante ĝisdatigojn Ŝlosil-ID Ŝargante kaŝmemoron Nomo neniu dosiero Normale Arkaikaĵoj Gepatro Elcento Bonvole elsaluti kaj ensaluti por kompletigi la ĝisdatigon. Bonvole restarti la komputilon por kompletigi la ĝisdatigon. Vera nomo Forigi pakaĵon Forigite Forigante Forigante pakaĵojn Ripari sistemon Restarti Rezultoj: rolo Ruliĝante Serĉo laŭ nomo Sekureco Montri sencimigan informojn de ĉiuj dosieroj Montri sencimigajn agordojn Montri la program-version kaj eliri Montri la version kaj eliri Startante Stato Stato Sukcesis Resumo Sistema horo Teksto Neniu pakaĵo nun estas ĝisdatigebla. Neniu ĝisdatigo nun estas disponebla. Neniu promocio nun estas disponebla. Tiu ilo ne povis trovi la pakaĵon: %s Transakcio Vera Tipo Nekonata stato Ĝisdatigite Ĝisdatigoj Ĝisdatigante Ĝisdatigante pakaĵojn Uzanto-ID Uzantonomo Vendisto Atendas aŭtentigon 
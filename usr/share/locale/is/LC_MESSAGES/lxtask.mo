��    0      �  C         (     )     /     5     :     @     E     Y     ^     f     n     w     �     �     �     �  Y   �  )        2     K     g     k     p     |     �     �     �     �     �     �     �  !   �          ,     <     L     R     W     d     i     n     v     |     �  	   �     �     �     �  �  �     _     e     k     p     v     {     �     �  
   �     �  &   �     �     �     �     �  Z   �  .   W	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	  "   
     *
     I
  1   c
     �
     �
     �
     �
     �
     �
     �
     �
  
                       $     5     B      W            	      *                                                    .   /                 +      #      ,   &   )   0                                  (         "      -      %             $   
       '      !                                 0    10    5   -10   -5 CPU usage: %0.0f %% CPU% Command Confirm Continue Copyright (C) 2008 LXDE team Error General Kill LXTask LXTask is derived from Xfce4 Task Manager by:
  * Johannes Zellner <webmaster@nebulon.de> Lightweight task manager for LXDE project Manage running processes Memory: %d MB of %d MB used PID PPID Preferences Prio Priority Process manager RSS Really kill the task? Really terminate the task? Refresh rate (seconds): Show full cmdline Show memory used by cache as free Show other tasks Show root tasks Show user tasks State Stop Task Manager Term User VM-Size _File _Help _View cpu usage memory usage more details translator-credits Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-12-06 21:44+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
Language: templates
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1439775046.447069
     0    10    5   -10   -5 Örgjörvanotkun: %0.0f %% CPU% Skipun Staðfesta Halda áfram Höfundarréttur (C) 2008 LXDE teymið Villa Almennt Kála LXTask LXTask er afleitt af Xfce4 Task Manager eftir:
  * Johannes Zellner <webmaster@nebulon.de> Léttur verkferlastjóri fyrir LXDE verkefnið Sýsla með keyrandi ferli Minni: %d MB af %d MB used PID PPID Kjörstillingar Forg Forgangur Ferlastjórnun RSS Kála verkefni? Hætta í alvörunni við verkið? Uppfærslutíðni (sekúndur): Sýna alla skipanalínuna Sýna laust það minni notað er sem skyndiminni Birta önnur verk Birta verk kerfisstjóra Birta verk notanda Staða Stöðva Verkefnastjóri Ljúka Notandi VM-stærð _Skrá _Hjálp _Skoða örgjörvanotkun minnisnotkun frekari upplýsingar Sveinn í Felli, sv1@fellsnet.is 
��          �      L      �     �     �  *   �     �  -     ;   9     u     �     �     �     �     �     �  2   �  /   *     Z     a     h  a  p     �     �  7   �     )  D   D  G   �     �     �          *     <     Y     m  2   �  5   �  	   �     �  	   �                        
                                	                                            * Border Effect Grab a window instead of the entire screen Include Border Include the window border with the screenshot Include the window manager border along with the screenshot Save Screenshot Save in _folder: Screenshot directory Screenshot.png Select a folder Take Screenshot Take a picture of the screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. _Name: effect seconds Project-Id-Version: gnome-utils 2.15.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2006-05-29 16:25+0300
Last-Translator: Fano Rajaonarisoa <rajfanhar@yahoo.fr>
Language-Team: Malagasy <i18n-malagasy-gnome@gna.org>
Language: mg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
 * Effet miaraka amin'ny sisiny Fikandrana alaina sary fa tsy ny efijery iray manontolo Ataovy tafiditra ny sisiny Ataovy tafiditra anatin'ny sarin'ny efijery ny sisin'ilay fikandrana Mampiditra ny sisin'ny mpandrindra fikandrana anatin'ilay sarin'efijery Raiketo ilay sarin'efijery Raiketo anatin'ny _laha-tahiry: Laha-tahirin'ny sarin'efijery Sarin'efijery.png Misafidiana laha-tahiry iray Makà sarin'efijery Haka sary ny efijery Makà sary aorian'ny fotoana voatondro [segaondra] Ny laha-tahiry nandraiketana ny sarin'efijery farany. _Anarana: effect segeondra 
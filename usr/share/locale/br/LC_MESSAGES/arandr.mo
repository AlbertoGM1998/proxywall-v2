��    &      L  5   |      P     Q     V     Z  2   ^     �     �     �     �  (   �     �  :       >  S   D     �     �  ,   �     �     �  
             "     )  0   ;  )   l     �  ^   �               %     -     6     >     D     M     b  	   }     �  �  �     5	     :	     >	  ?   B	  (   �	  	   �	     �	     �	  2   �	  /   �	    +
     H  E   O     �     �  4   �     �               $     9     @  1   T  ,   �     �  f   �  	   2     <     R  
   [     f     o     u     �     �     �     �                          
   $         %         	                                !          "                                                        #         &                        1:16 1:4 1:8 A part of an output is outside the virtual screen. ARandR Screen Layout Editor Accelerator Action Active An output is outside the virtual screen. Another XRandR GUI Click on a button in the left column and press a key combination you want to bind to a certain screen layout. (Use backspace to clear accelerators, escape to abort editing.) Then, select one or more layouts in the right column.

This will only work if you use metacity or another program reading its configuration. Dummy In order to configure metacity, you need to have the python gconf module installed. Keybindings (via Metacity) New accelerator... No files in %(folder)r. Save a layout first. Open Layout Orientation Resolution Save Layout Script Script Properties Setting this resolution is not possible here: %s This orientation is not possible here: %s XRandR failed:
%s Your configuration does not include an active monitor. Do you want to apply the configuration? _Help _Keybindings (Metacity) _Layout _Outputs _System _View disabled gconf not available. incompatible configuration no action other application Project-Id-Version: ARandR
Report-Msgid-Bugs-To: chrysn@fsfe.org
PO-Revision-Date: 2013-10-30 11:09+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Breton <http://hosted.weblate.org/projects/arandr/translations/br/>
Language: br
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 1.8-dev
 1:16 1:4 1:8 Ul lodenn eus ar skramm a zo e diavaez eus ar zonenn galloudel. ARandR; kefluniadur aozadur ar skrammoù Berradenn Gwered Gweredekaet Ur skramm a zo en diavaez eus ar zonenn galloudel. Another XRandR GUI (un etrefas all evit XRandR) Klikit war ur bouton eus ar bann kleiz ha lakait ar berradenn a-zere evit un aozadur eus ar skrammoù (implijit an douchenn Distro evit diverkañ ur berradenn pe Echap. evit nullañ). Mont a raio en dro ma implijit metacity nemetken pe ur meziant all oc'h implij ar memes kefluniadur. Skramm Evit kefluniañ metacity, ezhomm ho peus kaout ar modul python gconf. Berradennoù (dre Metacity) Berradenn nevez... Restr ebet e %(folder)r. Enrollit un aozadur a-raok. Digeriñ un aozadur Reteadur Resolvidigezh Enrollañ un aozadur Skript Perzhioù ar skript N'eo ket posupl lakaat ar resolvidigezh-mañ : %s N'eo ket posupl lakaat ar reteadur-mañ : %s C'hwitadenn XRandR : %s Ho kefluniadur n'eus skramm gweredekaet ebet. C'hoant ho peus arloañ ouzh ar c'hefluniadur memestra ? S_koazell _Berradenn (Metacity) _Aozadur _Skrammoù _Reizhad _Gwel Diweredekaet N'eo ket dieub gconf. Kefluniadur digendere Gwered ebet meziant all 
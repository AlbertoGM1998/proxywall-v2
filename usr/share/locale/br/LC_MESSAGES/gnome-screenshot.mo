��    3      �  G   L      h     i     k     z     �     �  N   �     �  4   �  ]   2     �     �     �     �  *   �  7     u   L     �     �  b   �     V     e     u  %   �     �  -   �  ;   �     1     K  ,   P     }  1   �     �     �     �     �     	     	     +	     ;	     K	     \	  2   y	  /   �	  ;   �	  g   
  1   �
  '   �
     �
     �
     �
  5  �
     &     (     ;     C     S  P   e     �  <   �  �        �  /   �  9   �     �  /     9   E  |        �       u   0     �     �     �  +   �       -   7  2   e  1   �  	   �  0   �       >        Z     r     �     �     �     �     �     �       $     7   ?  B   w  9   �  v   �  >   k  )   �     �     �     �     !                 *   2         3               (   1                                           .   %      '           -   
                              	      /   )         +         &   $         0   "   #                         ,                   * Apply _effect: Border Border Effect C_opy to Clipboard Conflicting options: --window and --area should not be used at the same time.
 Drop shadow Effect to add to the border (shadow, border or none) Effect to add to the outside of a border. Possible values are "shadow", "none", and "border". Effects Error loading the help page Error while saving screenshot Grab _after a delay of Grab a window instead of the entire screen Grab an area of the screen instead of the entire screen Grab just the current window, rather than the whole desktop. This key has been deprecated and it is no longer in use. Grab the current _window Grab the whole _desktop Impossible to save the screenshot to %s.
 Error was %s.
 Please choose another location and retry. Include Border Include Pointer Include _pointer Include the pointer in the screenshot Include the window _border Include the window border with the screenshot Include the window manager border along with the screenshot Interactively set options None Remove the window border from the screenshot Save Screenshot Save images of your desktop or individual windows Save in _folder: Screenshot delay Screenshot directory Screenshot taken Screenshot.png Select _area to grab Select a folder Take Screenshot Take _Screenshot Take a picture of the screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. The number of seconds to wait before taking the screenshot. UI definition file for the screenshot program is missing.
Please check your installation of gnome-utils Unable to take a screenshot of the current window Window-specific screenshot (deprecated) _Name: effect seconds Project-Id-Version: gnome-utils
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-07-11 09:06+0000
Last-Translator: Alan <Unknown>
Language-Team: Brenux <brenux@free.fr>
Language: br
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
 * Arloañ an _efed : Riblenn Efed ar riblenn _Eilañ er golver Digeverlc'h eo an dibarzhioù : arabat ober gant --window ha --area war un dro.
 Disheol bannet Efed da ouzhpennañ d'ar riblenn (disheol, riblenn pe netra) Efed da ouzhpennañ da drolinenn ar riblenn. Ar gwerzhioù a c'hell bezañ  "shadow" (disheol), "none" (netra), ha "border" (riblenn). Efedoù Fazi e-pad ma oa o kargañ pajennad ar skoazell Ur fazi ez eus bet e-pad ma oa oc'h enrollañ ar skrammad Pakañ _goude un dale eus Pakañ ur prenestrad e-lec'h ar skrammad a-bezh Pakañ ur maez eus ar skrammad e-lec'h ar skrammad a-bezh Pakañ ar prenestrad bremanel nemetken kentoc'h eget ar burev a-bezh. Dispredet eo an alc'hwez-mañ ha n'eo ket arveret ken. Pakañ ar _prenestr bremanel Pakañ ar burev a-bezh N'hall ket enrollañ al luc'hskeudenn e %s.
 Ar fazi a oa %s.
 Mar plij, dibabit ul lec'hiadur all ha klaskit en-dro. Enlakaat ar riblenn Enlakaat biz al logodenn Enlakaat b_iz al logodenn Lakaat biz al logodenn war al luc'hskeudenn Enlakaat ri_blenn ar prenestr Enlakaat riblenn ar prenestr gant ar skrammad Enlakaat riblenn ardoer ar prenestroù er skeudenn Dibarzhioù arventennet gant un doare etrewezhiat Hini ebet Lemel kuit riblenn ar prenestr diwar ar skrammad Enrollañ ar skrammad Enrollañ ar skeudennoù eus ho purev pe brenestroù hiniennel Enrollañ en _teuliad : Dale evit ur skeudenn Kavlec'hiad ar_skrammad Paket eo bet ar skrammad Skrammad.png Diuzañ ar maez da bakañ Diuzañ un teuliad Pakañ ur skrammad Pakañ ur _skrammad Pakañ ur skeudenn diwar ar skrammad Pakañ ur skeudenn goude un dale erspizet [e eilennoù] Ar c'havlec'hiad ma 'eo bet enrollet ar skeudenn diwezhañ ennañ. An niver a eilennoù da c'hortoz kent pakañ ur skeudenn. Mankout a ra restr ketal an arveriad evit ar goulev da bakañ skrammad.
Mar plij, gwiriit ho staliadur eus gnome-utils N'eo ket gouest da bakañ ur skeudenn eus ar prenestr bremanel Skrammad ur prenestr spesadel (dispredet) _Anv : efed eilenn 
��          �   %   �      `  �   a     �       :        I  "   X     {  o   �  $        -     F  
   N  	   Y  	   c  -   m  
   �     �     �  	   �     �  
   �     �     �     �  
          �      �     �     �  |   �     g  F   �  6   �  �   	  6   �	  4   �	     4
     I
     ]
     p
  n   ~
     �
     �
  +        >     K     j  
   �     �     �     �  
   �        
                                                                                                 	                   <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Applications automatically started after entering desktop: Authentication Automatically Started Applications Banner to show on the dialog Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Custom message to show on the dialog Desktop Session Settings Enabled Error: %s
 Group: %s Identity: Manage applications loaded in desktop session Password:  Position of the banner S_witch User Sh_utdown Window Manager: _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-12-05 13:37+0000
Last-Translator: system user <>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1417786671.000000
 <b>ئاگاھلاندۇرۇش:</b> نېمە قىلىشنى ئېنىق بىلمىگەن ئەھۋال ئاستىدا <b>زادىلا تەگمەڭ</b>.

<b>دىققەت:</b> بۇ تەڭشەكلەر كېيىنكى قېتىم كىرگەندە ئاندىن ئۈنۈملۈكتۇر. ئالىي تاللانما پروگرامما ئۈستەلئۈستىگە كىرگەندىلا ئاپتوماتىك ئىجرا بولىدىغان پروگراممىلار: كىملىك دەلىللەش ئاپتوماتىك قوزغىلىدىغان پروگراممىلار سۆزلەشكۈدە كۆرۈنىدىغان لەۋھە كۆزنەك باشقۇرغۇنى ئىجرا قىلىدىغان بۇيرۇق قۇرى
(LXDE نىڭ كۆڭۈلدىكى كۆزنەك باشقۇرغۇ چوقۇم openbox-lxde بولۇشى كېرەك) سۆزلەشكۈدە كۆرۈنىدىغان ئۇچۇر ئۈستەلئۈستى ئەڭگىمە تەڭشىكى ئىناۋەتلىك خاتالىق: %s
 گۇرۇپپا: %s كىملىك: ئۈستەلئۈستى ئەڭگىمەسىگە ئوقۇلغان پروگراممىلارنى باشقۇرىدۇ ئىم:  لەۋھەنىڭ ئورنى ئىشلەتكۈچى ئالماشتۇر(_W) تاقا(_U) كۆزنەك باشقۇرغۇ: ئۈچەككە كىر(_H) چىق(_L) قايتا قوزغات(_R): توڭلات(_S) سۈرەت ھۆججىتى ئۇچۇر 
��    &      L  5   |      P     Q     V     Z  2   ^     �     �     �     �  (   �     �  :       >  S   D     �     �  ,   �     �     �  
             "     )  0   ;  )   l     �  ^   �               %     -     6     >     D     M     b  	   }     �  �  �     	     !	     %	  7   )	  !   a	     �	     �	     �	  +   �	  (   �	  �  �	     ~  P   �  /   �       B   !     d     w     �     �     �     �  6   �  +   �  !   %  U   G     �  #   �     �     �     �     �     �     �          3     ?                          
   $         %         	                                !          "                                                        #         &                        1:16 1:4 1:8 A part of an output is outside the virtual screen. ARandR Screen Layout Editor Accelerator Action Active An output is outside the virtual screen. Another XRandR GUI Click on a button in the left column and press a key combination you want to bind to a certain screen layout. (Use backspace to clear accelerators, escape to abort editing.) Then, select one or more layouts in the right column.

This will only work if you use metacity or another program reading its configuration. Dummy In order to configure metacity, you need to have the python gconf module installed. Keybindings (via Metacity) New accelerator... No files in %(folder)r. Save a layout first. Open Layout Orientation Resolution Save Layout Script Script Properties Setting this resolution is not possible here: %s This orientation is not possible here: %s XRandR failed:
%s Your configuration does not include an active monitor. Do you want to apply the configuration? _Help _Keybindings (Metacity) _Layout _Outputs _System _View disabled gconf not available. incompatible configuration no action other application Project-Id-Version: arandr
Report-Msgid-Bugs-To: chrysn@fsfe.org
PO-Revision-Date: 2011-12-09 23:40+0000
Last-Translator: Miguel Anxo Bouzada <mbouzada@gmail.com>
Language-Team: Galician <gl@li.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2011-12-10 05:28+0000
X-Generator: Launchpad (build 14450)
 1:16 1:4 1:8 Unha parte dunha saída está fora da pantalla virtual. Editor de disposicións de ARandR Tecla rápida _Acción Activo Unha saída está fora da pantalla virtual. Another XRandR GUI (outra IGU de XRandR) Prema nun botón na columna esquerda e prema unha combinación de teclas que desexe vincular a unha disposición de pantalla concreta. (Use a tecla Retroceso para limpar aceleradores e a tecla Escape para interromper a edición.) De seguido seleccione unha ou máis disposicións na columna da dereita.

Isto só funciona se emprega Metacity ou outro programa para ler a súa configuración. Simulación Coa fin de configurar Metacity, precisa ter instalado o módulo gconf de python. Combinacións de teclas (a través de Metacity) Nova tecla rápida... Non hai ficheiros en %(folder)r. Garde unha disposición primeiro. Abrir disposición Orientación Resolución Gardar disposición Script Propriedades do script Non é posíbel estabelecer aquí esta resolución: %s Esta orientación non é posíbel aquí: %s Produciuse un fallo en XRandR:
%s A súa configuración non inclúe un monitor activo. Desexa aplicar a configuración? _Axuda _Combinacións de teclas (Metacity) _Disposición _Saídas S_istema _Ver desactivado gconf no está dispoñíbel. configuración incompatíbel sen acción outro aplicativo 
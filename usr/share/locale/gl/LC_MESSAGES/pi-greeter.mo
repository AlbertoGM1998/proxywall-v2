��          �      �       0  E   1     w     ~     �  	   �     �  $   �  
   �     �     �  
          t    M   �     �  !   �  (        0     9  '   H     p       	   �     �  	   �                                               	       
    Are you sure you want to close all programs and restart the computer? Cancel Failed to authenticate Failed to start session Hibernate High Contrast Incorrect password, please try again Large Font Other... Restart Restart... Suspend Project-Id-Version: lightdm
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-05-08 06:41+0000
Last-Translator: Miguel Anxo Bouzada <mbouzada@gmail.com>
Language-Team: Galician <gl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-05-01 06:31+0000
X-Generator: Launchpad (build 16985)
Language: gl
 Ten certeza de que desexa pechar todos os programas e reiniciar o computador? Cancelar Produciuse un fallo ao autenticar Produciuse un fallo ao iniciar a sesión Hibernar Contraste alto Contrasinal incorrecto, ténteo de novo Letras grandes Outro... Reiniciar Reiniciar... Suspender 
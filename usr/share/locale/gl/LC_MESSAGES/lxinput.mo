��          �   %   �      `     a     y     �     �  -   �  2   �  &        4     9     >      W     x     �     �  /   �     �     �     �     �     �       7        I     O  	   T  8   ^  �  �      6     W     v     �  .   �  <   �  3        5     =  &   B  *   i     �     �     �  :   �          
               ,     F  G   U     �     �     �  ;   �                         	                               
                                                                    <b>Character Repeat</b> <b>Keyboard layout</b> <b>Motion</b> Acceleration: Beep when there is an error of keyboard input Configure keyboard, mouse, and other input devices Delay before each key starts repeating Fast High Input Device Preferences Interval between each key repeat Keyboard Keyboard and Mouse LXInput autostart Left handed (Swap left and right mouse buttons) Long Low Mouse Repeat delay: Repeat interval: Sensitivity: Setup keyboard and mouse using settings done in LXInput Short Slow Touch Pad Type in the following box to test your keyboard settings Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-11-28 07:53+0000
Last-Translator: Miguel Anxo Bouzada <mbouzada@gmail.com>
Language-Team: Proxecto Trasno <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1417161203.000000
 <b>Repetición de caracteres</b> <b>Disposición do teclado</b> <b>Movemento</b> Aceleración: Pitar cando haxa un erro de entrada de teclado Configurar o teclado, rato, e outros dispositivos de entrada Atraso antes de que cada tecla inicie a repetición Rápida Alta Preferencias do dispositivo de entrada Intervalo entre cada repetición de teclas Teclado Teclado e rato Inicio automático de LXInput Zurdo (Intercambiar os botóns esquerdo e dereito do rato) Longo Baixa Rato Atraso de repetición: Intervalo de repetición: Sensibilidade: Configuración do teclado e do rato usando os axustes feitos en LXInput Curto Lenta Area táctil Escriba na seguinte caixa para probar os axustes do teclado 
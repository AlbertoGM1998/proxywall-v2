��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u               9     H     N     T     [     `     i     x     �     �     �     �  #   �  "   �               ,     =     O     W     ^  "   l     �     �     �     �      �  V   �  1   B  .   t     �     �     �     �     �     �     �  !     
   4     ?     Z     c     k     �     �     �     �     �     �               $     0     =     J     Z     a     z     �     �     �     �     �     �     �     �          	     '  )   0     Z     t  
   }     �     �     �  E   �     �     �                    '     9  	   A     K     \  	   d     n     u     �     �     �      �  �   �     n                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-24 06:08+0000
Last-Translator: Miguel Anxo Bouzada <mbouzada@gmail.com>
Language-Team: Proxecto Trasno <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495606099.867634
 Avanzado Permitir letras en negriña Sinal audíbel Fondo Negro Bloque Azul Inferior Azul brillante Ciano brillante Verde brillante Maxenta brillante Vermello brillante Marrón Limpar _historial de desprazamento: Limpar _historial de desprazamento Pechar a lapela Pechar a xanela _Pechar a xanela Confirmar o peche _Copiar Copiar Copiar o _URL Dereitos de autoría (C) 2008-2017 Intermitencia do cursor Estilo do cursor Ciano Gris escuro Tamaño predeterminado da xanela Desactivar a petición de confirmación cando se peche unha xanela con varias lapelas. Desactivar a tecla de atallo (F10 predeterminada) Desactivar o uso de Alt-N para lapelas e menú Amosar Primeiro plano Gris Verde Agochar o botón de peche Agochar a barra de menú Agochar o punteiro do rato Agochar a barra de desprazamento: LXTerminal Preferencias de LXTerminal Esquerda Maxenta Mover á lapela da esquerda Mover á lapela da dereita Mover á lapela da _esquerda Mover á lapela da _dereita No_me da lapela Nome da lapela Lapela _seguinte _Nova lapela Nova lapela Nova xanela _Nova lapela Nova _xanela Lapela seguinte Paleta Predefinición da paleta Pegar Lapela _anterior Pre_ferencias Lapela anterior Vermello Dereita Liñas de desprazamento Selección por palabra Atallos de teclado Estilo Posición do panel de lapelas Terminal Emulador de terminal para o proxecto LXDE Tipo de letra do terminal Superior Subliñado Usar a liña de ordes Branco Marelo Estás a piques de pechar %d lapelas. Confirmas que queres continuar? Ampliar Red_ucir Reducir Restablecer o zoom Ampl_iar _Restaurar o zoom _Sobre. _Cancelar Pechar a _lapela _Editar _Ficheiro _Axuda Nova _xanela _Aceptar _Pegar _Lapelas consola;liña de ordes;executar; Miguel Anxo Bouzada <mbouzada@gmail.com>, 2009, 2010, 2011,2012,2013,2014, 2015, 2016, 2017.
Indalecio Freiría <ifreiria@gmail.com>, 2009 
Proxecto Trasno <proxecto@trasno.net> x 
��          �      �       0  E   1     w     ~     �  	   �     �  $   �  
   �     �     �  
          �    S   �     �     �          *     3  *   B     m     z  	   �     �  	   �                                               	       
    Are you sure you want to close all programs and restart the computer? Cancel Failed to authenticate Failed to start session Hibernate High Contrast Incorrect password, please try again Large Font Other... Restart Restart... Suspend Project-Id-Version: lightdm
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-05-08 06:39+0000
Last-Translator: Lionel Le Folgoc <lionel@lefolgoc.net>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-05-01 06:31+0000
X-Generator: Launchpad (build 16985)
Language: pt_BR
 Você tem certeza de que deseja fechar todos os programas e reiniciar o computador? Cancelar Falhou ao autenticar Falhou ao iniciar sessão Hibernar Contraste alto Senha incorreta, por favor tente novamente Fonte grande Outro... Reiniciar Reiniciar… Suspender 
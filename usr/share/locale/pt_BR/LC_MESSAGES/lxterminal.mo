��    C      4  Y   L      �     �     �     �  
   �     �     �     �       	             *     8     >  	   C     M     Z  *   g  %   �     �  
   �     �     �     �     �  
             1     6     D     S     b  	   r     |  	   �     �     �  
   �     �     �     �     �     �     �     �     �     �       	   (     2     8     K  "   T     w     �  	   �     �     �  
   �     �     �     �     �     �     �     �     	  �  	  	   �
     �
  	   �
     �
     �
               %  
   @     K     Y     h     p     w     �     �  7   �  :   �               +     B     X     r  
   �     �     �     �     �     �     	     #     0     <  	   J     T     ]  	   i     s     �     �     �     �     �     �     �  !   �     �               '  $   0     U     g  
   o     z     �     �     �     �     �     �     �     �  "   �  �   �     <             5              	   .      C      #            B      -   @   (      '      9   2       
   !              "       >                           3       :      /          ;   =   +                            6   ,   %   *   ?           1      A      )      7       4      &      $               0             8    Advanced Allow bold font Audible bell Background Block Bottom Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Cop_y Copy Copy _URL Cursor blink Cursor style Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Paste Pre_vious Tab Preference_s Previous Tab Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line _About _Close Tab _Edit _File _Help _New Window _Paste _Tabs console;command line;execute; translator-credits Project-Id-Version: LXTerminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-05-19 22:12+0000
Last-Translator: Felipe Braga <fbobraga@gmail.com>
Language-Team: Portuguese (Brazilian) <translation@mailinglist.lxde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1432073577.000000
 Avançado Permitir fonte em negrito Campainha Plano de fundo Bloco Embaixo Ocultar _barra de rolagem _Limpar rolagem para trás Fechar aba Fechar Janela Fechar _janela _Copiar Copiar Copiar _URL Piscar cursor Estilo do cursor Desabilitar a tecla de atalho do menu (F10 por padrão) Desabilitar o uso de Alt-n para mudança entre abas e menu Exibir Primeiro plano Ocultar botões Fechar Ocultar barra de menu Ocultar ponteiro do mouse Ocultar barra de rolagem LXTerminal Preferências do LXTerminal Esquerda Mover aba para a esquerda Mover aba para a direita Mover aba para a _esquerda Mover aba para a _direita No_me da aba Nome da aba Pró_xima aba Nova _aba Nova aba Nova janela No_va aba N_ova janela Próxima aba Colar Aba a_nterior Preferência_s Aba anterior Direita Linhas de histórico Selecionar caracteres por palavra Atalhos Estilo Posição do painel de abas Terminal Emulador de terminal do projeto LXDE Fonte do terminal Em cima Sublinhado Use a linha de comando _Sobre _Fechar aba _Editar _Arquivo Aj_uda _Nova janela C_olar A_bas console;linha de comando;executar; Fábio Antunes <nospam@emal.com>
Og Maciel <ogmaciel@gnome.org>
Henrique P. Machado <hpmachado@gnome.org>
Sérgio Cipolla <secipolla@gmail.com> 
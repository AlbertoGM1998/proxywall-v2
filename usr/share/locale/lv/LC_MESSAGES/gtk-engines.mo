��    0      �  C         (     )     9  
   K     V     \     p     x     �     �     �     �  !   �     �     �     �     �     �  "        )     /     8     E     T     `  1   �     �     �     �  
   �     �  	   �     �     �          %     5     E     T     g     �     �  ?   �     �     �     �  p   �     T  �  b     0	     @	     Z	     f	     l	     �	     �	  	   �	     �	     �	     �	  #   �	     
     
     
     
     
  )   9
     c
     i
     r
     �
     �
     �
  :   �
               "     (     /     8     E  %   S     y     �     �     �     �     �     �     �  E   �     7     D     L  X   R     �     /   +              -         )   %                                   *                        $                            0   ,      "      .   &         '             #             (          	                   !                 
    3d-ish (button) 3d-ish (gradient) Animations Arrow Cell Indicator Size Classic Colorize Scrollbar Contrast Disable focus drawing Dot Edge Thickness Enable Animations on Progressbars Flat Full Glossy Gummy Handlebox Marks If set, button corners are rounded Inset Inverted Inverted Dot Inverted Slash Mark Type 1 Mark Type for Scrollbar Buttons Mark Type for Scrollbar Handles, Handleboxes, etc Menubar Style None Nothing Paned Dots Radius Rectangle Relief Style Requires style Glossy or Gummy Rounded Buttons Scrollbar Color Scrollbar Marks Scrollbar Type Scrollbutton Marks Sets the Color of Scrollbars Shadow Shaped Size of check- and radiobuttons inside treeviews. (Bug #351764) Slash Some Style This option allows to disable the focus drawing. The primary purpose is to create screenshots for documentation. Toolbar Style Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gtk-engines&component=general
PO-Revision-Date: 2010-06-21 12:56+0300
Last-Translator: Anita Reitere <nitalynx@gmail.com>
Language-Team: Latviešu <lata-l10n@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 3d-veida (poga) 3d-veida (krāsu pāreja) Animācijas Bulta Šūnas indikatora izmērs Classic Iekrāsot ritjoslu Kontrasts Atslēgt fokusa pārņemšanu Punkts Malas biezums Ieslēgt progresa joslu animācijas Plakans Pilns Glossy Gummy Atdalāmas joslas atzīmes Ja iestatīts, pogu stūri ir noapaļoti. Inset Inverted Invertēts punkts Apgriezta slīpsvītra Atzīmes tips 1 Atzīmes tips ritjoslas pogām Atzīmes tips ritjoslas turiem, atdalāmajām joslām u.c. Izvēlnes joslas stils Nekas Nekas Punkti Rādiuss Taisnstūris Reljefa stils Tam vajadzīgs Glossy vai Gummy stils Noapaļotas pogas Ritjoslu krāsa Ritjoslas atzīmes Ritjoslas tips Ritpogu atzīmes Nosaka ritjoslu krāsu Ēna Formā Atzīmes rūtiņu un radiopogu izmērs kokveida skatos. (Bug #351764) Slīpsvītra Nedaudz Stils Ļauj atslēgt fokusa pārņemšanu. Paredzēts galvenokārt ekrānattēlu uzņemšanai. Rīkjoslas stils 
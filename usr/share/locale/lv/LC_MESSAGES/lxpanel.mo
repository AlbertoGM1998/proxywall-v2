��    e      D  �   l      �  )   �  '   �     �     �     �     	     $	  
   -	     8	     O	     a	  &   r	     �	  	   �	     �	     �	  
   �	     �	     �	     �	     �	     
     
     1
     9
     J
     \
     j
     y
     �
     �
     �
  !   �
     �
     �
     �
     �
            
   "     -     2  5   8  	   n     x     ~     �     �     �     �     �     �     �     �     �     �     �  
     	        "     9     A     G     Z     h     p  	   �     �     �     �     �     �     �     �     �     �     �                          ,  &   ?     f     z     �     �     �     �     �     �     �     �  #   �                    &     -     4  �  G  1   >  )   p     �     �     �     �     �     �  !        (     <  +   H     t     �     �     �     �     �     �     �     �     	          4     A     W     e     |     �     �  	   �     �  0   �                 	   (     2     L     R  
   _     j  6   r  
   �     �  	   �     �     �     �     �  	   �               :     P     ]     e     z     �     �  
   �     �     �     �     �     �     �               %     8  $   H     m     v     ~  	   �  
   �  
   �     �     �     �     �  #   �          *     B     I  	   O     Y     o     w     �     �  #   �     �     �     �     �     �  O   �         b         H          C       Q   O   :   /       ;       e   J          R   A          
   U   ^   [       >           V      D                 $      K   5               E   #   0      Z       F   T   G   "   %           8   )      ]   a       I      =           ,      .      B   +   1             	       M                3       ?       !      7           -              \   W   *   X   S   _   <       L   '       `       Y             &       (   2   c         N       P       4      d         9   @   6     --help      -- print this help and exit
  --version   -- print version and exit
 "%s" Settings About Add / Remove Panel Items Add plugin to panel Advanced Appearance Application Launch Bar Available plugins Background color Bar with buttons to launch application Battery Monitor Bold font Border width Bottom Broadcast: CPU Usage Monitor Center Charging color 1 Charging color 2 Clock Format Command line options:
 Confirm Create New Panel Delete This Panel Digital Clock Directory Menu Discharging color 1 Discharging color 2 Disconnected Display CPU usage Display battery status using ACPI Error General Geometry Height: Hide if there is no battery Icon Icons only Idle Image Indicators for CapsLock, NumLock, and ScrollLock keys Interface Label Left Logout Menu Minimize All Windows Mute Name No batteries found No network devices found Open in _Terminal Orientation Panel Panel Settings Properties Receiving Remove "%s" From Panel Restart Right Select a directory Select a file Sending Sending/Receiving Separator Show CapsLock Show NumLock Show ScrollLock Show tooltips Show windows from all desktops Signal Size Spacer Spacing State Stats Status: Subnet Mask: System Tray The interface name The interface packets/bytes statistics The interface state The orientation of the tray. Top Type: Unknown Use mouse wheel Volume Volume Control Width: Wireless Wireless signal strength percentage Workspace %d _Browse _Close Window _Name: pixels translator-credits Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-02-26 19:16+0000
Last-Translator: External Translation Source <translation@rep.kiev.ua>
Language-Team: LANGUAGE <LL@li.org>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1551208615.008565
X-Pootle-Path: /lv/lxpanel/po/lv.po
X-Pootle-Revision: 0
  --help      -- Drukāt šo palīdzību un iziet
  --version   -- Drukāt versiju un iziet
 "%s" Uzstādījumi Par programmu Pievienot/Noņemt paneļus Pievienot spraudni panelim Palašināti Izskats Pieprasījumu palaišanas panelis Pieejamie spraudņi Fona krāsa Josla ar pogām, lai uzsāktu pieprasījumu Akumulatora monitors Treknrakstā Apmales platums Apakša Pārraidīšaana: Procesora noslodzes pārraugs Centrs Uzlādes krāsa 1 Uzlādes krāsa 2 Pulksteņa formāts Komandrindas opcijas:
 Apstiprināt Izveidot jaunu paneli Dzēst paneli Digitālais pulkstenis Kataloga izvēlne Izlādes krāsa 1 Izlādes krāsa 2 Atvienots Rādīt procesora noslodzi Parādīt akumulatora stāvokli, izmantojot ACPI Kļūda Vispārīgi Ģeometrija Augstums: Paslēpt ja nav baterijas Ikona Tikai ikonas Dīkstāve Attēls Indikatori CapsLock, NumLock un ScrollLock taustiņiem Interfeiss Etiķete Pa kreisi Darba beigšana Izvēlne Minimizēt visus logus Mēms Nosaukums Baterija nav atrasta  Tīkla ierīces nav atrastas Atvērt  _Terminālī Orientācija Panelis Paneļa iestatījumi Īpašības Saņem Noņemt "%s" No paneļa Restartēt Pa labi Izvēlieties direktoriju Izvēlieties failu Sūta Sūta/Saņem Atdalītājs Rādīt CapsLock Rādīt NumLock Rādīt ScrollLock Rādīt padomus Rādīt logus no visām darbvirsmām Signāls Izmērs Atdalītājs Atstatums Stāvoklis Statistika Status: Apakštīkla maska: Sistēmas Ikonjosla Interfeisa nosaukums Interfeisa pakešu/baitu statistika Interfeisa stāvoklis Paplātes orientācija. Augša Tips: Nezināms Izmantot peles riteni Vienums Skaļuma Kontrole Platums: Bezvadu Bezvadu signāla stiprums procentos Darba laukums%d _Pārlūkot Ai_zvērt logu _Vārds: pikseļu Andrejs Pavars <hackey@apollo.lv>, 2011
Andris Krišlauks <krislauks@gmail.com> 
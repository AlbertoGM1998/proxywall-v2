��    6      �  I   |      �  (   �     �     �     �            &   *     Q  �   a     �       :        I     O  "   ^  	   �     �  '   �  o   �  $   @  "   e     �  "   �     �     �  
   �  	   �  	   �     �     �     
          -  -   4     b     n     s     y  
   �     �     �     �     �  	   �     �     �     �  
   �     �     �     	  
   	     	  j  	  0   �
  #   �
     �
  "   �
       ,   )  �   V     �    �  %        '  {   <     �     �  <   �  +   !  4   M  F   �  �   �  W   �  8     I   K  _   �     �     
          )     8     V  '   k     �  *   �     �  t   �  "   X  
   {  
   �  
   �     �      �     �  %   �               -     4  +   C  $   o     �     �     �     �     �                  +       (                    )          "                      *      	                    3           .   &   ,   $       -              
   5   6   2   0         4       /                '             !                            1      #   %    <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Applications automatically started after entering desktop: Apply Authentication Automatically Started Applications Autostart Banner to show on the dialog Change the default applications on LXDE Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Custom message to show on the dialog Default applications for LXSession Desktop Session Settings Disable autostarted applications ? Enabled Error Error: %s
 Group: %s Identity: Information L_ock Screen Laptop mode Launching applications Layout Manage applications loaded in desktop session Menu prefix Mode Model Options Password:  Position of the banner Reload S_witch User Settings Sh_utdown Type Variant Window Manager: _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-12-05 13:04+0000
Last-Translator: system user <>
Language-Team: MIME-Version: 1.0
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1417784689.000000
 <b><big>Изход от %s сесия?</big></b> <b>Настройки на Dbus</b> <b>Среда</b> <b>Общи настройки</b> <b>Клавиши</b> <b>Известни приложения</b> <b>Тези приложения от автоматично стартиращите, които са пуснати ръчно</b> <b>Настройки</b> <b>Внимание:</b> <b>НЕ</b> променяйте това, особено ако не знаете какво правите.

<b>БЕЛЕЖКА:</b>Тази настройка ще влезе в действие при следващото вписване. Разширени настройки Приложение Приложения, които ще се стартират автоматично с вписване в средата: Приложи Удостоверяване Автоматично стартирани програми Автоматично стартиране Банер за показване в диалога Промени стандартните приложения за LXDE Команда, използвана за стартиране на мениджъра на прозорците
(Командата за мениджърът по подразбиране на LXDE, трябва да бъде openbox-lxde) Потребителско съобщение за показване в диалога Стандартни приложения за LXSession Настройка на сесията на работната среда Деактивиране на автоматично стартираните програми? Активирано Грешка Грешка: %s
 Група: %s Идентифициране: Информация Зак_лючване на екрана Лаптоп режим Стартиране на програми Изглед Управление на приложенията зарадени в сесия на работната среда Представка за меню Режим Модел Опции Парола: Позиция на банера Презареди _Смяна на потребител Настройки _Изключване Вид Вариант Мениджър на прозорците: _Дълбоко приспиване _Излизане _Рестартиране _Приспиване снимка съобщение 
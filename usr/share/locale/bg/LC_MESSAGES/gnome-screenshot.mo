��    A      $  Y   ,      �  0   �     �     �     �     �  M     N   P     �     �  =   �  i        o     w  >   �     �  *   �       7   (  u   `     �     �     	     	     )	     9	  <   J	  %   �	  '   �	     �	  -   �	  ;   
     Z
     t
     �
     �
  "   �
  ,   �
  0   �
  %   '  
   M     X     i     ~     �     �     �     �  '   �            2   -  0   `  =   �  >   �  ;        J     i  '   q     �     �     �     �     �     �  �  �  V   �  #   	  $   -     R     `  �     �     A   �     �  s   �  �   n     <  8   I  �   �  >     F   R     �  B   �  �   �  2   �  (   �  "     5   1  ,   g  -   �  p   �  @   3  @   t  S   �  R   	  d   \  A   �  C        G  H   V  B   �  V   �  g   9  N   �      �  @     2   R     �     �  "   �     �  5   �  Y   &     �     �  ]   �  b     l   �  o   �  G   ]  7   �     �  f   �     \     h  
   t          �  u  �     $      =       7   ?   &            1            -   >       .      @            A   /   *       3              5   9          
                  ;   )                  #                        :   2             +          ,   "   8                             <   0         4   6      %   	          '          (   !        A file named “%s” already exists in “%s” All possible methods failed Apply _effect: Border Border Effect Conflicting options: --area and --delay should not be used at the same time.
 Conflicting options: --window and --area should not be used at the same time.
 Default file type extension Drop shadow Effect to add to the border (shadow, border, vintage or none) Effect to add to the outside of a border. Possible values are “shadow”, “none”, and “border”. Effects Error creating file Error creating file. Please choose another location and retry. Error loading the help page Grab a window instead of the entire screen Grab after a _delay of Grab an area of the screen instead of the entire screen Grab just the current window, rather than the whole desktop. This key has been deprecated and it is no longer in use. Grab the current _window Grab the whole sc_reen Include Border Include ICC Profile Include Pointer Include _pointer Include the ICC profile of the target in the screenshot file Include the pointer in the screenshot Include the pointer with the screenshot Include the window _border Include the window border with the screenshot Include the window manager border along with the screenshot Interactively set options Last save directory None Overwrite existing file? Print version information and exit Remove the window border from the screenshot Save images of your screen or individual windows Save screenshot directly to this file Screenshot Screenshot delay Screenshot directory Screenshot from %s - %d.%s Screenshot from %s.%s Screenshot taken Screenshot.png Select _area to grab Send the grab directly to the clipboard Take Screenshot Take _Screenshot Take screenshot after specified delay [in seconds] The default file type extension for screenshots. The directory where the screenshots will be saved by default. The last directory a screenshot was saved in interactive mode. The number of seconds to wait before taking the screenshot. Unable to capture a screenshot Vintage Window-specific screenshot (deprecated) _Cancel _Help effect filename seconds translator-credits Project-Id-Version: gnome-screenshot master
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screenshot&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-08-25 12:01+0200
Last-Translator: Alexander Shopov <ash@kambanaria.org>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.5.4
 В папка „%2$s“ вече съществува файл на име „%1$s“ Никой метод не успя Прилагане на _ефект: С рамка Ефект на рамката Несъвместими опции: „--ared“ и „--delay“ не могат да бъдат използвани едновременно.
 Несъвместими опции: „--window“ и „--area“ не могат да бъдат използвани едновременно.
 Стандартно разширение на файловете Със сянка Какъв ефект да се добави към снимката (сянка, рамка или никакъв) Ефект, който да се добави извън изображението. Възможни стойности са „shadow“ (сянка), „none“ (нищо), и „border“ (рамка). Ефекти Грешка при запазването на файл Грешка при запазването на файл. Въведете друго местоположение и пробвайте пак. Грешка при зареждането на помощта Снимка на прозорец, а не на целия екран Снимка _след Снимка на област, а не на целия екран Заснемане само на текущия прозорец, а не на целия работен плот. Този ключ е изоставен и вече не се ползва. Снимка на _текущия прозорец Снимка на _целия екран Включване на рамка Включване на цветовия профил Включване на показалеца Включване на _показалеца Включване на цветовия профил на заснетия прозорец в снимката Включване на показалеца в снимката Включване на показалеца в снимката Включване на _рамката на прозореца в снимката Включване на рамката на прозореца в снимката Включване на рамката на прозореца в снимката на екрана Интерактивно задаване на настройки Последна папка за снимките на екрана Никакъв Да се презапише ли съществуващият файл? Извеждане на версията на програмата Изключване на рамката на прозореца от снимката Запазване на снимки на екрана или индивидуални прозорци Запазване на снимката директно в този файл Снимане на екрана Забавяне преди заснемане на екрана Папка за снимките на екрана Снимка от %s — %d.%s Снимка от %s.%s Направена е снимка Снимка.png Избор на _област за заснемане Изпращане на снимката директно в буфера за обмен Снимка на екрана _Снимка на екрана Правене на снимка след определено време (в секунди) Стандартно разширение на файлове за снимки на екрана. Папката, в която снимките на екрана стандартно се запазват. Папката, в която последно са запазване на снимките на екрана. Изчакване в секунди преди заснемането. Неуспешно създаване на снимка Стара снимка Снимка на прозорец (този ключ е изоставен и не се ползва) _Отказ Помо_щ ефект снимка секунди Александър Шопов <ash@kambanaria.org>
Красимир Чонов <mk2616@abv.bg>
Ростислав Райков <zbrox@i-space.org>
Явор Доганов <yavor@doganov.org>
Владимир Петков <kaladan@gmail.com>
Петър Славов <pslavov@i-space.org>
Борислав Александров

Проектът за превод на GNOME има нужда от подкрепа.
Научете повече за нас на <a href="http://gnome.cult.bg">http://gnome.cult.bg</a>
Докладвайте за грешки на <a href="http://gnome.cult.bg/bugs">http://gnome.cult.bg/bugs</a> 
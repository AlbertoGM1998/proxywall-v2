��          �      �           	                  *   (     S  -   b  ;   �     �     �  1   �          $     9     H     X     h  2   �  /   �     �     �     �  I  �     H     J  "   Z     }  t   �  )     s   /  �   �     3  .   J  z   y  .   �  =   #     a     p  )   �  B   �  |   �  �   z	     �	     
      
                                             	                  
                                         * Border Border Effect Effects Grab a window instead of the entire screen Include Border Include the window border with the screenshot Include the window manager border along with the screenshot None Save Screenshot Save images of your desktop or individual windows Save in _folder: Screenshot directory Screenshot.png Select a folder Take Screenshot Take a picture of the screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. _Name: effect seconds Project-Id-Version: gedit.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2008-01-25 13:49+0530
Last-Translator: Sangeeta Kumari
Language-Team:  <en@li.org>
Language: mai
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);


 * किनार किनार प्रभाव प्रभाव संपूर्ण स्क्रीन क' बजाए एकटा विंडो ग्रेब करू किनार सामिल करू स्क्रीनशॉट क' सँग विंडो बार्डर सम्मिलित करू स्क्रीनशॉट क' सँग विंडो प्रबंधक किनारकेँ सम्मिलित करू किछु नहि स्क्रीनशॉट सहेजू अपन डेस्कटाप अथवा निजी विंडो क' चित्रकेँ सहेजू फ़ोल्डरमे देखू (_f): स्क्रीनशॉट निर्देशिका Screenshot.png फोल्डर चुनू स्क्रीनशॉट लिअ' स्क्रीन क' एकटा चित्र लिअ' निर्दिष्ट देरी (सेकण्डमे) क' बाद स्क्रीनशॉट लिअ' निर्देशिका जकरामे अंतिम स्क्रीनशॉट सहेजल गेल छल. नाम (_N): प्रभाव सकेण्ड 
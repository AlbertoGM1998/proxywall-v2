��    `        �         (     )     2     B  
   O     Z     `     f     k     r     ~     �     �  
   �     �     �     �  	   �     �     �      	     	     	  	   	     #	     0	     =	     B	     K	  ?   _	  *   �	  %   �	     �	  
   �	     
     
     
     !
     /
     B
  
   R
     ]
     t
     y
     �
     �
     �
     �
  	   �
     �
  	   �
     �
     �
  
   �
     �
     �
                    +     1     ?     L     Y     ]     c     t  	   �     �     �     �  "   �     �     �  	   �     �            B        ^  	   f     p  
   y     �     �     �  
   �     �     �     �     �     �     �     �     �       �  	     �     �     �  	   �     �     �     �     �  	     	     	        !  	   .     8     ?     S     g     t     �     �  
   �     �     �     �     �     �  	   �     �  *     %   8  %   ^     �  	   �     �     �     �     �     �     �     �     �       	             /     ?     S     g     x     �     �     �     �     �     �     �     �     �     
          %     6     F     M     T  '   a  	   �     �     �  	   �      �     �     �     �     �       	     4        L  
   S     ^     e  
   r     }  
   �     �  
   �  
   �  
   �     �  
   �  
   �  U   �  =   H     �     V      +   Q   L      G   N       &       3   T   R   _   
      P       A      ?      H       7       4   ,      Y          F   M       \   -   [       O   5   E      @                             :          /                    .   >          6       #                 K      <              ]       *   '       0               W   %   S   C      )   D                      Z   	      "          B      =           9       ;   1   X      U   J      `   !       ^      2   (      I   $   8    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Close Tab _Edit _File _Help _New Window _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-08 11:29+0800
Last-Translator: Yao Wei (魏銘廷) <mwei@lxde.org>
Language-Team: LXDE Contributors <admin@lxde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.7
X-Poedit-SourceCharset: utf-8
X-POOTLE-MTIME: 1426979896.000000
 進階 允許粗體字 鈴聲可聞 背景色 黑色 區塊 藍色 底部 亮藍色 亮青色 亮綠色 亮洋紅色 亮紅色 棕色 清除捲動列(_E) 清除捲動列(_O) 關閉分頁 關閉視窗 關閉視窗(_W) 關閉分頁確認 複製(_Y) 複製 複製 _URL 文字游標閃爍 文字游標風格 青色 暗灰色 預設視窗大小 停用關閉多個分頁時的確認視窗 停用選單捷徑鍵 (預設為 F10) 停用以 Alt-n 切換分頁及選單 顯示 前景色 灰色 綠色 隱藏關閉按鈕 隱藏選單列 隱藏滑鼠指標 隱藏捲動列 LX 終端機 LXTerminal 偏好設定 左側 洋紅色 將分頁左移 將分頁右移 將分頁左移(_L) 將分頁右移(_R) 命名分頁(_M) 命名分頁 下一個分頁(_X) 開新分頁(_A) 開新分頁 開新視窗 開新分頁(_T) 開新視窗(_W) 下一個分頁 色盤 預設色盤 貼上 上一個分頁(_V) 偏好設定(_S) 上一個分頁 紅色 右側 保留行數 選取字詞時會選取以下的字元 快捷鍵 風格 分頁顯示位置 終端機 LXDE 專案的終端機模擬器 終端機字型 頂部 底線 使用命令列 白色 亮黃色 您即將關閉 %d 個分頁，確定要離開嗎？ 放大 縮小(_U) 縮小 還原縮放 放大(_I) 還原縮放(_R) 關於(_A) 關閉分頁(_C) 編輯(_E) 檔案(_F) 說明(_H) 開新視窗(_N) 貼上(_P) 分頁(_T) console;command line;execute;終端機;終端;文字介面;執行;小黑窗;命令列 Fred Chien <cfsghost@gmail.com>
Ming-Ting Wei <mwei@lxde.org> x 
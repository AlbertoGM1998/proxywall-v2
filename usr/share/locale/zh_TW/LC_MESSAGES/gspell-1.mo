��    !      $  /   ,      �     �     �  	             #     /     ;     J     V  5   o     �     �     �     �     �  $   �       Y   $     ~     �     �     �     �     �  	   �     �     �     �     �     �          %  �  *     �     �       
   $     /     @     Q     ^     o  4   �  	   �     �     �     �     �          (  N   5     �     �  
   �  
   �  
   �     �  
   �     �  
   �     �     	     	     	     '	                                           !                                                        	                                
                            (correct spelling) (no suggested words) Add w_ord Cha_nge Change A_ll Change _to: Check Spelling Check _Word Completed spell checking Error when checking the spelling of word “%s”: %s Error: Ignore _All Misspelled word: No language selected No misspelled words Select the spell checking _language. Set Language Spell checker error: no language set. It’s maybe because no dictionaries are installed. Suggestions User dictionary: _Add _Cancel _Ignore _Ignore All _Language _More… _Select _Spelling Suggestions… _Suggestions: language%s (%s) languageUnknown (%s) word Project-Id-Version: gspell 3.3.7
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gspell
POT-Creation-Date: 2017-10-29 09:50+0100
PO-Revision-Date: 2017-08-27 12:11+0800
Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>
Language-Team: 漢語 <>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.0.3
 （拼字正確） （不提供任何建議） 加入字詞(_O) 變更(_N) 全部變更(_L) 變更為(_T)： 檢查拼字 字詞檢查(_W) 拼字檢查完成 當檢查「%s」字詞的拼字時發生錯誤：%s 錯誤： 全部忽略(_A) 拼錯的字詞： 未選取語言 沒有拼錯字詞 選取拼字檢查語言(_L)。 設定語言 拼字檢查器錯誤：未設定語言。可能是因為尚未安裝字典。 建議 個人字典： 加入(_A) 取消(_C) 忽略(_I) 全部忽略(_I) 語言(_L) 更多(_M)… 選擇(_S) 拼字建議(_S)… 建議(_S)： %s (%s) 不明 (%s) 文字 
��          �   %   �      p  :   q     �     �      �       !     j   >  F   �  [   �     L  $   j     �     �     �     �     �  �   �     �     �  	   �  #   �  O   �  �     $   �  #   �            �  ,  8   �     "	     :	  '   R	     z	  %   �	  `   �	  =   
  I   [
     �
  !   �
  !   �
               4     J  �   Q     �     �  	          H   .  �   w  "         .     O     c                        
                                                                         	                          "%s" does not appear to be a valid Openbox theme directory "%s" was installed to %s "%s" was successfully created <span weight="bold">Theme</span> Choose an Openbox theme Create a theme _archive (.obt)... Error while parsing the Openbox configuration file. Your configuration file is not valid XML.

Message: %s Failed to load an rc.xml. Openbox is probably not installed correctly. Failed to load the obconf.glade interface file. ObConf is probably not installed correctly. Font for active window title: Font for inactive on-screen display: Font for inactive window title: Font for menu Item: Font for menu header: Font for on-screen display: Misc. N: Window icon
L: Window label (Title)
I: Iconify (Minimize)
M: Maximize
C: Close
S: Shade (Roll up)
D: Omnipresent (On all desktops) Openbox theme archives Theme Title Bar Unable to create directory "%s": %s Unable to create the theme archive "%s".
The following errors were reported:
%s Unable to extract the file "%s".
Please ensure that "%s" is writable and that the file is a valid Openbox theme archive.
The following errors were reported:
%s Unable to move to directory "%s": %s Unable to run the "tar" command: %s _Button order: _Install a new theme... Project-Id-Version: obconf 2.0.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-11-13 23:54+0100
PO-Revision-Date: 2015-03-21 00:15+0000
Last-Translator: wwycheuk <wwycheuk@gmail.com>
Language-Team: Chinese (traditional) <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1426896918.000000
 「%s」沒有出現有效的 Openbox 佈景主題目錄 「%s」已安裝到 %s 「%s」已成功建立 <span weight="bold">佈景主題</span> 選擇 Openbox 佈景主題 建立佈景主題存檔(.obt)(_A)… 解讀 Openbox 設定檔案時出錯。該設定檔可能並非有效 XML 檔案。

訊息：%s 未能載入 rc.xml 檔案。Openbox 可能未安裝妥當。 未能載入 obconf.glade 介面檔案。可能 ObConf 未安裝妥當。 作用中視窗標題字型： 非作用中螢幕顯示字型： 非作用中視窗標題字型： 選單項目字型： 選單標頭字型： 螢幕顯示字型： 雜項 N: 視窗圖示
L: 視窗標籤 (標題)
I: 最小化 (縮到最小)
M: 放到最大
C: 關閉
S: 摺疊 (捲起)
D: 顯示在所有桌面 Openbox 佈景主題存檔 佈景主題 標題列 無法建立目錄「%s」：%s 無法建立佈景主題存檔「%s」。
已經提報以下錯誤：
%s 無法擷取檔案「%s」。
請確保該「%s」可以寫入並且該檔為有效的 Openbox 佈景主題存檔。
已經提報以下錯誤：
%s 無法移動到目錄「%s」：%s 無法執行「tar」命令：%s 按鈕順序(_B)： 安裝新的佈景主題(_I)… 
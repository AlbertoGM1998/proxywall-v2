��    �      T  �   �
        (   	     2     >     Q     i     w  &   �     �  �   �     Y     ^     o  @   {  0   �  0   �          <  ?   U     �  2   �  ,   �       #   1  /   U     �  3   �     �  :   �     +     1     9     K     Y     f  &   u  "   �  	   �     �     �       R        l  	   �     �  
   �     �     �  '   �     �     �       o   "     �     �     �  �   �     K  $   ]  "   �     �     �  �   �     R  "   Z     }  3   �     �  a   �     &     /     5     =  
   C  3   N     �  v   �  	     :        K  	   h     r          �     �     �     �     �     �     �     �  -     A   6     x  |   �       Z   '  -   �  :   �  !   �  %     j   3     �     �     �  L   �               "     '     3     A  
   I     T  �   Z  
   �     �       �        �     �  �   �     �     �     �     �     �  M   �               .  8   M     �  V   �     �  	                  +   &      R      c   %   o      �      �      �   >   �   D   !  u   H!  5   �!     �!     �!     "  2   "  
   N"     Y"     `"     i"     y"  �   �"     "#     4#  
   ;#     F#     N#     V#  
   _#     j#  �  r#  '   %     5%     A%     O%     c%     w%  (   �%     �%  �   �%     T&     a&     n&  -   {&  -   �&  0   �&     '     $'  B   @'     �'  3   �'  *   �'     �'  !   (  /   <(     l(  *   �(     �(  0   �(      )     )     )     !)     1)     A)     H)     h)     �)     �)     �)     �)  =   �)     !*  	   @*     J*  	   W*     a*     w*  $   ~*     �*     �*     �*  `   �*     C+     L+     U+  p   e+     �+  '   �+     ,     .,     >,  �   Q,     �,  $   �,      -  %   -     3-  G   8-     �-     �-  	   �-     �-     �-  %   �-     �-  q   �-     T.  4   ].     �.  	   �.     �.     �.     �.     �.     �.     �.     /     /     */     1/  *   A/  @   l/     �/  q   �/     50  J   H0  !   �0  *   �0     �0     �0  T   1     j1     w1     �1  B   �1     �1     �1     �1     �1     2     2     2     #2  u   *2  	   �2     �2     �2  p   �2     @3     P3  �   `3     �3     �3     4     4     &4  9   -4     g4     }4     �4  1   �4     �4  R   �4     F5  
   M5     X5     e5  !   u5     �5     �5  !   �5     �5     �5     �5  <   6  E   I6  s   �6  0   7     47     ;7     N7  0   ^7     �7  	   �7     �7     �7     �7  �   �7     e8     x8  
   8  
   �8     �8  
   �8     �8     �8             D       y   /       U   b             -   7   �   z   W   l      A             ]   !   S   �   �   a   n       5   �   #   w   �      {   8   �   Q           c   F             I   �   o       �           H          �   N   X   �   O          [   '   J   s                   +   E           r          *   k   m   =   u      ~   �         G   �       T   ?      B   �   \   �          �         p   v      �               9   d       C   �   _   0   �      .       �           (   �       ^   ;          �       P   3   �       Y      �   	      V      �       i                   "           f   M      )          �   L   �   �       1   >       e           6           |   �   }               R   �   ,   $   :   &   j       t   �   �   �   Z       �   �   h   �   q       �   g       
   �   K   2      `   %          �   4   �   <      @   �   x    <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. A11y Advanced Options Application Application for taking screeshot of your desktop, like scrot ... Application to create archives, like file-roller Application to create spreedsheet, like gnumeric Application to display images Application to edit text Application to go to Internet, Google, Facebook, debian.org ... Application to lock your screen Application to manage bittorent, like transmission Application to manage burning CD/DVD utilty  Application to manage disks Application to manage notes utility Application to manage office text, like abiword Application to manage webcam Application to monitor tasks running on your system Application to send mails Applications automatically started after entering desktop: Apply Archive Audio application Audio manager Audio player Authentication Authentication failed!
Wrong password? Automatically Started Applications Autostart Autostart the application ? Autostart the application ?
 Available applications Available applications : Applications of this type available on your repositories
 Banner to show on the dialog Bittorent Burning utility Calculator Calculator application Cancel Change the default applications on LXDE Charmap Charmap application Clipboard manager Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Communication 1 Communication 2 Composite Manager Composite manager enables graphics effects, like transpacency and shadows, if the windows manager doesn't handle it. 
Example: compton Core applications Custom message to show on the dialog Default applications for LXSession Desktop Manager Desktop Session Settings Desktop manager draws the desktop and manages the icons inside it.
You can manage it with the file manager by setting "filemanager" Disable Disable autostarted applications ? Disks utility Do you want to assiociate the following Mimetype ?
 Dock Dock is a second panel. It's used to launch a different program to handle a second type of panel. Document Email Enabled Error Error: %s
 Extra: Add an extra parameter to the launch option
 File Manager File manager is the component which open the files.
See "More" to add options to handle the desktop, or opening files  Group: %s Handle Desktop: Draw the desktop using the file manager ?
 Handle the desktop with it ? Identity: Image viewer Information LXPolKit LXSession configuration L_ock Screen Laptop mode Launcher manager Launching applications Layout Lock screen manager Manage applications loaded in desktop session Managing XRandr parameters. Use a command like xrandr --something Managing clipboard support Managing keyring support.
Standard options available "gnome" for gnome-keyring support  or "ssh-agent" for ssh-agent support Managing proxy support Managing support for accessibility.
Stardart option are gnome, for stardart gnome support. Managing the application to quit your session Managing the application to update and upgrade your system Managing your audio configuration Managing your workspace configuration Manual Settings: Manually sets the command (you need to restart lxsession-default-apps to see the change)
 Manual setting Menu prefix Mime Association Mime association: Automatically associates mime types to this application ?
 Mode Model More Network GUI Notes utility Options PDF Reader Panel Panel is the component usually at the bottom of the screen which manages a list of opened windows, shortcuts, notification area ... Password:  Policykit Authentication Agent Polkit agent Polkit agent provides authorisations to use some actions, like suspend, hibernate, using Consolekit ... It's not advised to make it blank. Position of the banner Power Manager Power Manager helps you to reduce the usage of batteries. You probably don't need one if you have a desktop computer.
Auto option will set it automatically, depending of the laptop mode option. Proxy Quit manager Reload S_witch User Screensaver Screensaver is a program which displays animations when your computer is idle Screenshot manager Security (keyring) Session : specify the session
 Set an utility to manager connections, such as nm-applet Set debian default programs Set default program for Debian system (using update-alternatives, need root password)
 Settings Sh_utdown Spreadsheet Tasks monitor Terminal by default to launch command line. Terminal manager Text editor The database is updating, please wait Type Update lxsession database Upgrade manager Use a communication software (an IRC client, an IM client ...) Use another communication software (an IRC client, an IM client ...) Utility to launch application, like synapse, kupfer ... 
For using lxpanel or lxde default utility, use "lxpanelctl"  Utility to launch gadgets, like conky, screenlets ... Variant Video application Video player Wallpaper: Set an image path to draw the wallpaper Webbrowser Webcam Widget 1 Window Manager: Windows Manager Windows manager draws and manage the windows. 
You can choose openbox, openbox-custom (for a custom openbox configuration, see "More"), kwin, compiz ... Workspace manager Xrandr _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession-lite
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-12-03 12:10+0000
Last-Translator: Wylmer Wang <wantinghard@gmail.com>
Language-Team: zh_CN <i18n-translation@lists.linux.net.cn>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1417608623.000000
 <b><big>注销 %s %s会话？</big></b> <b>Dbus</b> <b>环境</b> <b>常规设置</b> <b>键盘映射</b> <b>已知的应用程序</b> <b>手动设为自动启动的程序</b> <b>设置</b> <b>警告：</b> 如果您不清楚这样做的后果，<b>不要</b>动这项设置。

<b>注：</b> 此设置下次登录时生效。 辅助功能 高级选项 应用程序 对桌面截图的应用程序，如 scrot... 创建归档的应用程序，如 file-roller 创建电子表格的应用程序，如 gnumeric 显示图像的应用程序 编辑文本的应用程序 访问互联网、Google、Facebook、debian.org等的应用程序 锁定屏幕的应用程序 管理 bittorent 的应用程序，如 transmission 管理刻录 CD/DVD 功能的应用程序  管理磁盘的应用程序 管理便笺功能的应用程序 管理办公文档的应用程序，如 abiword 管理摄像头的应用程序 监视系统中任务运行的应用程序 发邮件的应用程序 在进入桌面后自动启动的应用程序： 应用 档案 音频应用程序 音频管理器 音频播放器 认证 认证失败！
密码错误？ 自动启动的应用程序 自动启动 自动启动该应用程序？ 自动启动该应用程序？
 可用应用程序 可用应用程序：您的仓库中有的这类应用程序
 显示在对话框上的图片 Bittorent 刻录工具 计算器 计算器应用程序 取消 更改 LXDE 上的默认应用程序 字符映射表 字符映射表应用程序 剪贴板管理器 启动窗口管理器的命令行语句
(LXDE 默认窗口管理器的命令应为 openbox-lxde) 通讯 1 通讯 2 混合管理器 混合管理器启用图形效果，如透明和阴影(如果窗口管理器没有接管的话)
例子：compton 核心应用程序 自定义在对话框上显示的信息 LXSession 默认应用程序 桌面管理器 桌面会话设置 桌面管理器绘制桌面和管理桌面上的图标。
您可以通过设置“文件管理器”用文件管理器管理桌面 禁用 禁用自动启动的应用程序？ 磁盘工具 您想关联以下 MIME 类型吗？
 Dock Dock 是第二面板。它用于启动另一种类型的面板程序。 文档 邮件 已启用 错误 错误：%s
 额外：添加额外的启动参数
 文件管理器 文件管理器是用来打开文件的组件。
查看“更多”来添加管理桌面或打开文件的选项  组：%s 管理桌面：用桌面管理器绘制桌面吗？
 用它管理桌面？ 身份： 图像查看器 信息 LXPolKit LXSession 配置 锁定屏幕(_O) 笔记本模式 启动器管理器 启动应用程序 布局 锁屏管理器 管理桌面会话中加载的应用程序 管理 XRandr 参数。使用 xrandr --something 形式的命令 管理剪贴板支持 管理密钥环支持。
标准选项有，“gnome”gnome-keyring 支持， 或“ssh-agent”ssh-agent 支持  管理代理支持 管理辅助功能支持。
标准选项有，gnome 标准 gnome 支持。 管理退出会话的应用程序 管理更新和升级系统的应用程序 管理您的音频配置 管理您的工作区配置 手动设置：手动设置命令(需要重启 lxsession-default-apps 才能生效)
 手动设置 菜单前缀 MIME 关联 MIME 关联：自动将 MIME 类型与该应用程序关联吗？
 模式 型号 更多 网络图形用户界面 便笺工具 选项 PDF 阅读器 面板 面板是一个管理打开窗口列表、快捷方式、通知区域等部件的东东，一般位于屏幕底部。 密码： Policykit 认证代理 Polkit 代理 Polkit 代理提供使用某些操作的授权，如挂起、休眠、使用Consolekit ... 建议设置一下。 图片的位置 电源管理器 电源管理器帮您省电。台式机可能不需要电源管理器。
自动 选项会根据笔记本模式选项自动设置电源方案。 代理 退出 管理器 重新载入 切换用户(_W) 屏保 屏保是一种在计算机空闲时显示动画的程序 屏幕截图管理器 安全(密钥环) 会话：指定会话
 设置一个管理连接的工具，如 nm-applet 设置 debian 默认程序 设置 Debian 系统的默认程序(使用 update-alternatives，需 root 密码)
 设置 关机(_U) 电子表格 任务监视器 启动命令行的默认终端。 终端管理器 文本编辑器 正在更新数据库，请等候 类型 更新 lxsession 数据库 更新 管理器 使用通讯软件(IRC 客户端、即时通讯客户端...) 使用另一个通讯软件(IRC 客户端，即时通讯客户端...) 启动应用程序(如 synapse、kupfer)的工具
要使用 lxpanel 或 lxde 默认工具，使用“lxpanelctl”  启动小部件(如 conky、screenlets)的工具 变量 视频应用程序 视频播放器 壁纸：设置要显示为壁纸的图像路径 网络浏览器 摄像头 部件 1 窗口管理器： 窗口管理器 窗口管理器绘制和管理窗口。
您可以选择 openbox、openbox-custom(自定义 openbox 配置参见“更多”)、kwin、compiz... 工作区管理器 Xrandr 休眠(_H) 注销(_L) 重新启动(_R) 挂起(_S) 图片文件 信息 
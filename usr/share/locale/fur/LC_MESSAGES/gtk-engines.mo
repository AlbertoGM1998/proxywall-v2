��    .      �  =   �      �     �       
             $     8     @     S     \     `  !   o     �     �     �     �     �  "   �     �     �     �     �            1   2     d     r     w  
        �  	   �     �     �     �     �     �     �               6     =  ?   D     �     �     �     �  e  �     	     !  
   <     G     M     j     r     �     �     �  +   �     �     �     �     �     �  4   	     8	     F	     R	     c	     u	  2   �	  P   �	     
     
     %
     *
     6
     =
     F
  !   W
     y
     �
     �
     �
     �
  (   �
     "     (  C   0     t     z     �     �     '      #       !                     )   .                           "             	       *         $   ,   (   
                                                     %      +   -                          &       3d-ish (button) 3d-ish (gradient) Animations Arrow Cell Indicator Size Classic Colorize Scrollbar Contrast Dot Edge Thickness Enable Animations on Progressbars Flat Full Glossy Gummy Handlebox Marks If set, button corners are rounded Inset Inverted Inverted Dot Inverted Slash Mark Type 1 Mark Type for Scrollbar Buttons Mark Type for Scrollbar Handles, Handleboxes, etc Menubar Style None Nothing Paned Dots Radius Rectangle Relief Style Requires style Glossy or Gummy Rounded Buttons Scrollbar Color Scrollbar Marks Scrollbar Type Scrollbutton Marks Sets the Color of Scrollbars Shadow Shaped Size of check- and radiobuttons inside treeviews. (Bug #351764) Slash Some Style Toolbar Style Project-Id-Version: gtk-engines 2.13.x
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2008-03-12 13:52+0100
Last-Translator: Massimo Furlani <massimo.furlani@libero.it>
Language-Team: FRIULIAN <massimo.furlani@libero.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Friulian
X-Poedit-Country: ITALY
 Tridimensionâl (boton) Tridimensionâl (gradient) Animazions Frece Dimension indicadôr de cele Classic Piturâ bare di scoriment Contrast Pont Spessôr da l'ôr Abilite animazions su la bare di avanzament Slis Plen Glossy Gummy Marcjadôr maniglis Se impostât, i cjantons dai botons a son tarondâts Metût dentri Devantdaûr Pont devantdaûr Slash devantdaûr Marcjadôr tipo 1 Tipo di marcjadôr par botons da bare di scoriment Tipo di marcjadôr par maniglis da bare di scoriment, maniglis di caselis, ecc.. Stîl bare di menu Nissun Nuje Ponts paned Ragjio Retangul Stîl in riliêf A covente il stîl Glossy o Gummy Botons tarondâts Colôr bare di scoment Marcadôrs bare di scoriment Tipo di bare di scoriment Marcjadôrs botons di scoriment Imposte il colôr ded baris di scoriment Ombre Sagomat Dimension dai botons radio/check dentri dai treeview (bug #351764). Slash Chualchidun Stîl Stîl bare dai imprescj 
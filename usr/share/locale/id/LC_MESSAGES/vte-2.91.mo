��          L      |       �   /   �      �   =   �   +   5     a  �  i  9   S  $   �  C   �  0   �  
   '                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte.master.id
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=vte&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-05-26 12:56+0700
Last-Translator: Kukuh Syafaat <kukuhsyafaat@gnome.org>
Language-Team: GNOME Indonesian Translation Team <gnome-l10n-id@googlegroups.com>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=1; plural=0;
 Galat (%s) sewaktu mengkonversi data untuk anak, dibuang. Galat sewaktu membaca dari anak: %s. GNUTLS tidak difungsikan; data akan ditulis ke disk tanpa enkripsi! Tidak dapat mengkonversi karakter dari %s ke %s. PERINGATAN 
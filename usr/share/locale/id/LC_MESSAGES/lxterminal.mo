��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u          #     7     D     S     Y     ^     c  
   i  
   t          �     �     �     �     �  	   �     �     �                 
   %     0     H     X     d     i     v  I   �  1   �  /        6     ?     K     R     X     q     �     �  
   �     �     �     �     �               4  	   L     V     _  	   o     y     �  	   �     �     �     �     �     �     �     �     �     �          	          <     E     J     [  #   d     �     �     �     �     �     �  2   �     �  	          	     	   %  
   /     :     C  
   J     U     ^     f     o     }     �     �     �  _   �                         /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-06-04 10:05+0000
Last-Translator: Andika Triwidada <andika@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1496570745.361208
 Tingkat lanjut Ijinkan fonta tebal Bel bersuara Latar belakang Hitam Blok Biru Bawah Biru Cerah Cyan Cerah Hijau Cerah Magenta Cerah Merah Cerah Coklat B_ersihkan gulung balik _Bersihkan gulung balik Tutup Tab Tutup Jendela Tutup _Jendela Konfirmasi penutupan S_alin Salin Salin _URL Hak Cipta (C) 2008-2017 Kursor berkedip Gaya kursor Cyan Kelabu gelap Ukuran jendela baku Nonaktifkan konfirmasi sebelum menutup sebuah jendela dengan beberapa tab Nonaktifkan kunci pintas menu (bawaan adalah F10) Nonaktifkan penggunaan Alt-n untuk tab dan menu Tampilan Latar depan Kelabu Hijau Sembunyikan tombol Tutup Sembunyikan bilah menu Sembunyikan penunjuk tetikus Sembunyikan bilah penggulung LXTerminal Pengaturan LXTerminal Kiri Magenta Pindahkan Tab ke Kiri Pindahkan Tab ke Kanan Pindahkan Tab ke Ki_ri Pindahkan Tab ke Ka_nan Tab Na_ma Tab Nama Ta_b Setelahnya T_ab Baru Tab Baru Jendela Baru _Tab Baru _Jendela Baru Tab Selanjutnya Palet Pratata Palet Tempel _Tab Sebelumnya _Pengaturan Tab Sebelumnya Merah Kanan Baris gulung balik Karakter pilih-berdasarkan-kata Pintasan Gaya Posisi panel tab Terminal Emulator terminal untuk proyek LXDE Fonta terminal Atas Garis bawah Gunakan perintah baris Putih Kuning Anda akan menutup %d tab. Yakin ingin melanjutkan? Perbesar Per_kecil Perkecil Reset Zum Per_besar _Reset Zum _Tentang _Batal _Tutup Tab _Sunting _Berkas Ba_ntuan Je_ndela Baru _OK _Tempel T_ab konsol;baris perintah;eksekusi; Andhika Padmawan <andhika.padmawan@gmail.com>, 2009.
Andika Triwidada <andika@gmail.com>, 2012. x 
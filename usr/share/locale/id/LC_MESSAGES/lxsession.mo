��    p      �  �         p	  (   q	     �	     �	     �	     �	     �	  &   �	     !
  �   1
     �
     �
     �
  :   �
          $     ,     :     G  &   V  "   }  	   �     �  R   �       	   7     A  
   Q  '   \     �     �  o   �               .     @  $   R  "   w     �     �  "   �     �     �     �                 
     3   !     U  	   b  	   l     v     �     �     �     �     �     �     �     �     �  -     j   :     �  L   �     �          	               (  
   0     ;  
   A     L     k     x     �     �     �     �     �     �     �     �  V   �     M  	   V     `     l     z     �  %   �     �     �     �     �     �  
                       ,     <     N  
   U     `     h     p  
   y     �  �  �  $   /     T     `     r     �     �  6   �     �  �        �     �     �  :   �     �     �            
   !  "   ,     O     g     v  P   �  #   �  	   	       
   %     0     M     [  �   n     �                 (   -     V     s     �  .   �     �     �     �     �  
   �     �  
   �  ?        F     U  
   ^     i  	   x     �     �     �     �     �     �  
   �     �  (   �     (     �  J   �                          %     6     >     J     P     X     r     ~     �     �     �  
   �     �     �     �     �  \     
   c     n     w     �     �     �  *   �     �     �                    +     8     ?     H     Y     i     }  
   �     �     �  	   �     �     �     0                   R              4       J   k              #      N   Z   G          @      ;   f   \   ,   :       8                .   5   9      3   !   "      (          B   o          p   E         +   I   H   F   M   h   m                    n   S   g   U   i   ^   >           j   D              ?   L   6       d      Q   '          *   W   c             X                          )       A   	   1   `       _          e   V       l   7      $       O            C   %   a       Y   [           
   ]   &   T   <       -       /   P   2   b   =   K    <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. A11y Advanced Options Application Applications automatically started after entering desktop: Apply Archive Audio manager Audio player Authentication Authentication failed!
Wrong password? Automatically Started Applications Autostart Autostart the application ?
 Available applications : Applications of this type available on your repositories
 Banner to show on the dialog Bittorent Burning utility Calculator Change the default applications on LXDE Charmap Clipboard manager Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Communication 1 Communication 2 Composite Manager Core applications Custom message to show on the dialog Default applications for LXSession Desktop Manager Desktop Session Settings Disable autostarted applications ? Disks utility Dock Document Email Enabled Error Error: %s
 Extra: Add an extra parameter to the launch option
 File Manager Group: %s Identity: Image viewer Information LXPolKit LXSession configuration L_ock Screen Laptop mode Launcher manager Launching applications Layout Lock screen manager Manage applications loaded in desktop session Manual Settings: Manually sets the command (you need to restart lxsession-default-apps to see the change)
 Menu prefix Mime association: Automatically associates mime types to this application ?
 Mode Model More Network GUI Notes utility Options PDF Reader Panel Password:  Policykit Authentication Agent Polkit agent Position of the banner Power Manager Proxy Quit manager Reload S_witch User Screensaver Screenshot manager Security (keyring) Set default program for Debian system (using update-alternatives, need root password)
 Settings Sh_utdown Spreadsheet Tasks monitor Terminal manager Text editor The database is updating, please wait Type Update lxsession database Upgrade manager Variant Video player Webbrowser Webcam Widget 1 Window Manager: Windows Manager Workspace manager Xrandr _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession-lite
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-06-04 13:43+0000
Last-Translator: Andika Triwidada <andika@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1496583796.360850
 <b><big>Keluar sesi %s %s?</big></b> <b>Dbus</b> <b>Lingkungan</b> <b>Pengaturan umum</b> <b>Peta tombol</b> <b>Aplikasi Yang Dikenal</b> <b>Aplikasi yang dijalankan otomatis secara manual</b> <b>Pengaturan</b> <b>Peringatan:</b> <b>JANGAN</b> sentuh ini kecuali Anda tahu persis apa yang Anda lakukan.

<b>CATATAN:</b> Setting ini akan berlaku pada login berikutnya. A11y Opsi Lanjutan Aplikasi Aplikasi dimulai secara otomatis setelah memasuki desktop: Terapkan Arsip Manajer audio Pemutar audio Otentikasi Otentikasi gagal
Kata sandi salah? Aplikasi Otomatis Mulai Start otomatis Jalankan otomatis aplikasi ?
 Aplikasi yang tersedia: Aplikasi bertipe ini yang tersedia pada repositori Anda
 Panji untuk ditampilkan pada dialog Bittorent Utilitas bakar CD Kalkulator Ubah aplikasi baku pada LXDE Peta karakter Manajer papan klip Baris perintah yang digunakan untuk meluncurkan pengelola jendela
(Perintah pengelola jendela standar untuk LXDE adalah openbox-lxde) Komunikasi 1 Komunikasi 2 Manajer Komposit Aplikasi inti Pesan suai untuk ditampilkan pada dialog Aplikasi baku bagi LXSession Manajer Desktop Pengaturan Sesi Desktop Nonaktifkan aplikasi yang otomatis dijalankan? Utilitas disk Dok Dokumen Surel Diaktifkan Galat Galat: %s
 Ekstra: Menambahkan sebuah parameter ekstra ke opsi peluncuran
 Manajer Berkas Grup: %s Identitas: Penampil citra Informasi LXPolKit Konfigurasi LXSession K_unci Layar Mode laptop Manajer peluncur Meluncurkan aplikasi Tata letak Manajer layar kunci Atur aplikasi yang dimuat di sesi dektop Pengaturan Manual: Secara manual menata perintah (Anda perlu menjalankan ulang lxsession-default-apps untuk melihat perubahan)
 Prefiks menu Asosiasi mime: Secara otomatis mengasosiasikan tipe mime ke aplikasi ini?
 Mode Model Lebih banyak GUI Jaringan Utilitas catatan Pilihan Pembaca PDF Panel Sandi:  Agen Otentikasi PolicyKit Agen polkit Posisi panji Manajer Daya Proksi Manajer keluar Muat Ulang P_indah Pengguna Pengaman layar Manajer cuplikan layar Keamanan (ring kunci) Menata program baku bagi sistem Debian (memakai update-alternatives, perlu kata sandi root)
 Pengaturan _Matikan Lembar Kerja Pemantau tugas Manajer terminal Penyunting teks Basis data sedang diperbarui, harap tunggu Tipe Perbarui basis data lxsession Manajer peningkatan Varian Pemutar video Peramban web Webcam Widget 1 Manajer Jendela: Manajer Jendela Manajer ruang kerja Xrandr _Hibernasi _Keluar _Reboot _Suspensi berkas citra pesan 
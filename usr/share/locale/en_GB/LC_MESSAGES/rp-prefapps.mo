��    8      �  O   �      �  ,   �  *        1     H     U     d     p     }     �  	   �     �     �  ;   �  !   �               &  9   .  !   h  h   �     �  \   	  
   f  $   q     �  
   �  5   �  8   �                &     8     D  (   P     y     �     �  "   �     �     �  $   �     "	      /	     P	  %   a	  &   �	     �	     �	     �	     �	     �	     �	     �	     
     
  R  ,
  ,     *   �     �     �     �     
          #     /  	   ;     E     Q  ;   _  !   �     �     �     �  9   �  !     h   0     �  \   �  
     $        <  
   E  5   P  8   �     �     �     �     �     �  (   �          4     E  "   b     �     �  $   �     �      �     �  %     &   -     T     \     `     d     u     �     �     �     �     "   /   2   1              ,   4   7         +          )      &                   .   3   8          
       0   #             !                                *       	                     '                     %      6          $                 5                   -      (           <b><small>(will be installed)</small></b>    <b><small>(will be removed)</small></b> %s %s - please wait... <b>%s</b>
%s <b>%s</b>%s
%s Accessories All Programs Application Downloading Education Engineering Error %s - %s Error synchronising clock - could not sync with time server Finding packages - please wait... Games Graphics Install Install additional programs recommended for use on the Pi Installation and removal complete Installation and removal complete.
An installed package requires a reboot.
Would you like to reboot now? Installation complete Installation complete.
An installed package requires a reboot.
Would you like to reboot now? Installing Installing packages - please wait... Internet More _Info No additional information available for this package. No network connection - applications cannot be installed Office Other Password Required Preferences Programming Reading package details - please wait... Recommended Software Removal complete Removing %s - please wait... Removing packages - please wait... Science & Maths Sound & Video Synchronising clock - please wait... System Tools Unable to open package data file Universal Access Updating application - please wait... Updating package data - please wait... _Cancel _No _OK finding packages installing packages packages reading package details removing packages updating package data Project-Id-Version: rp_prefapps 1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-10-29 07:24+0000
Last-Translator: Simon Long <simon@raspberrypi.org>
Language-Team: English (British)
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
    <b><small>(will be installed)</small></b>    <b><small>(will be removed)</small></b> %s %s - please wait... <b>%s</b>
%s <b>%s</b>%s
%s Accessories All Programs Application Downloading Education Engineering Error %s - %s Error synchronising clock - could not sync with time server Finding packages - please wait... Games Graphics Install Install additional programs recommended for use on the Pi Installation and removal complete Installation and removal complete.
An installed package requires a reboot.
Would you like to reboot now? Installation complete Installation complete.
An installed package requires a reboot.
Would you like to reboot now? Installing Installing packages - please wait... Internet More _Info No additional information available for this package. No network connection - applications cannot be installed Office Other Password Required Preferences Programming Reading package details - please wait... Recommended Software Removal complete Removing %s - please wait... Removing packages - please wait... Science & Maths Sound & Video Synchronising clock - please wait... System Tools Unable to open package data file Universal Access Updating application - please wait... Updating package data - please wait... _Cancel _No _OK finding packages installing packages packages reading package details removing packages updating package data 
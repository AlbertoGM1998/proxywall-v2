��    <      �  S   �      (     )     C  %   U     {     �     �  
   �     �     �     �     �  $     !   4     V     o     �     �     �     �  6   �          #     9     U     l     z     �     �     �  i   �     <     Z  +   x     �     �     �     �     �     	     	      	     2	     M	     j	     z	     �	  .   �	  &   �	     �	     �	     

     #
     <
     C
     P
     V
     u
     �
     �
  Q  �
     �       %   +     Q     X     w  
   �     �     �     �     �  $   �  !   	     +     D     b     n     {     �  ,   �     �     �               6     D     Q     g     �  i   �          $  +   B     n     �     �     �     �     �     �     �     �          4     D     L  .   a  %   �     �     �     �     �     
               $     C     Z     m     7                            2   :         +       <   !   6       %       (   "          #   	              -   
          '                                &   4   $       5   .                           9                                    1   /      0      )         8   ,          *   ;   3       %s: Received scan results %s: WPA status %s Automatically configure empty options C_lear Click here to set Wi-Fi country Config error Configure: Connected to %s-%s Connecting ... Connecting to dhcpcd ... Connection to dhcpcd lost Control for dhcpcd network interface Could not find SSID to disconnect Could not remove network Could not write configuration DNS Search: DNS Servers: Disable IPv6 Disconnect Wi-Fi Network Do you want to disconnect from the Wi-Fi network '%s'? Error creating new GIO Channel
 Error creating watch
 Error disconnecting network Error enabling network Error reading Error saving Failed to disconnect. Failed to enable the network. Failed to reconfigure. Failed to save wpa_supplicant configuration.

You should add update_config=1 to /etc/wpa_supplicant.conf. Failed to select the network. Failed to set key management. Failed to set password, probably too short. Failed to start association. Hide characters IPv4 Address: IPv6 Address: Network Preferences Network event New Access Point New Access Points No APs found - scanning... No wireless interfaces found Pre Shared Key: Router: Status changed to %s The dhcpcd configuration file is not writeable This Wi-Fi device cannot be turned off Turn Off Wi-Fi Turn On Wi-Fi Wi-Fi country is not set Wireless & Wired Network _About _Preferences _Quit dhcpcd WPA connection lost: %s dhcpcd connection lost dhcpcd not running g_try_malloc
 Project-Id-Version: lxplug_dhcpcdui 0.4
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-04-09 19:06+0100
Last-Translator: Simon Long <simon@raspberrypi.org>
Language-Team: English (British)
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=ASCII
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %s: Received scan results %s: WPA status %s Automatically configure empty options C_lear Click here to set WiFi country Config error Configure: Connected to %s-%s Connecting ... Connecting to dhcpcd ... Connection to dhcpcd lost Control for dhcpcd network interface Could not find SSID to disconnect Could not remove network Could not write configuration DNS Search: DNS Servers: Disable IPv6 Disconnect WiFi Network Do you want to forget the WiFi network '%s'? Error creating new GIO Channel
 Error creating watch
 Error disconnecting network Error enabling network Error reading Error saving Failed to disconnect. Failed to enable the network. Failed to reconfigure. Failed to save wpa_supplicant configuration.

You should add update_config=1 to /etc/wpa_supplicant.conf. Failed to select the network. Failed to set key management. Failed to set password, probably too short. Failed to start association. Hide characters IPv4 Address: IPv6 Address: Network Preferences Network event New Access Point New Access Points No APs found - scanning... No wireless interfaces found Pre Shared Key: Router: Status changed to %s The dhcpcd configuration file is not writeable This WiFi device cannot be turned off Turn Off WiFi Turn On WiFi WiFi disabled - country not set Wireless & Wired Network _About _Preferences _Quit dhcpcd WPA connection lost: %s dhcpcd connection lost dhcpcd not running g_try_malloc
 
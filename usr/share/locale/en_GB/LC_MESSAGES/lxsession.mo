��          �   %   �      `  �   a     �       :        I  "   X     {  o   �  $        -     F  
   N  	   Y  	   c  -   m  
   �     �     �  	   �     �  
   �     �     �     �  
          �    �   �     3     D  :   P     �  "   �     �  o   �  &   L     s     �  
   �  	   �  	   �  -   �  
   �     �     	  	   	     	  
   *	     5	     =	     E	  
   N	     Y	        
                                                                                                 	                   <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Applications automatically started after entering desktop: Authentication Automatically Started Applications Banner to show on the dialog Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Custom message to show on the dialog Desktop Session Settings Enabled Error: %s
 Group: %s Identity: Manage applications loaded in desktop session Password:  Position of the banner S_witch User Sh_utdown Window Manager: _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-12-05 13:09+0000
Last-Translator: system user <>
Language-Team: English UK en_GB <yorvik.ubunto@gmail.com>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1417784968.000000
 <b>Warning:</b> Do <b>NOT</b> touch this, unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Applications automatically started after entering desktop: Authentication Automatically Started Applications Banner to show on the dialogue Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Custom message to show on the dialogue Desktop Session Settings Enabled Error: %s
 Group: %s Identity: Manage applications loaded in desktop session Password:  Position of the banner S_witch User Sh_utdown Window Manager: _Hibernate _Logout _Reboot _Suspend image file message 
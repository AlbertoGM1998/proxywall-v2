��    
      l      �       �      �            ,  ,   <  +   i     �  /   �  .   �  '     O  /           �     �  ,   �  +   �     #  /   6  .   f  '   �                               
       	       Battery (pi-top / laptop) Charged : %d%%
On external power Charging : %d%% Charging : %d%%
Time remaining : %0.1f hours Charging : %d%%
Time remaining : %d minutes Discharging : %d%% Discharging : %d%%
Time remaining : %0.1f hours Discharging : %d%%
Time remaining : %d minutes Monitors battery for pi-top and laptops Project-Id-Version: lxplug_ptbatt 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-06 08:04+0100
Last-Translator: Simon Long <simon@raspberrypi.org>
Language-Team: English (British)
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=ASCII
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Battery (pi-top / laptop) Charged : %d%%
On external power Charging : %d%% Charging : %d%%
Time remaining : %0.1f hours Charging : %d%%
Time remaining : %d minutes Discharging : %d%% Discharging : %d%%
Time remaining : %0.1f hours Discharging : %d%%
Time remaining : %d minutes Monitors battery for pi-top and laptops 
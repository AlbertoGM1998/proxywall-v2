��    
      l      �       �      �   "        (     0     F     Y  #   s  '   �  &   �  C  �     *  "   >     a     i          �  #   �  '   �  &   �                  	      
                     %s has been ejected Drive was removed without ejecting Ejecter Ejects mounted drives Failed to eject %s Hide icon when no devices It is now safe to remove the device Please use menu to eject before removal Select a drive in menu to eject safely Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-03-06 16:38+0000
Last-Translator:  <simon@raspberrypi.org>
Language-Team: English (British)
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=ASCII
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %s has been ejected Drive was removed without ejecting Ejecter Ejects mounted drives Failed to eject %s Hide icon when no devices It is now safe to remove the device Please use menu to eject before removal Select a drive in menu to eject safely 
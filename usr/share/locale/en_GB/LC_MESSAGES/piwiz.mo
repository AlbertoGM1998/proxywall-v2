��    f      L  �   |      �     �     �     �     �     	     	     0	     F	     ]	  %   }	  %   �	  +   �	  #   �	  *   
     D
     Z
     m
  +   |
     �
     �
  !   �
  1   �
     /  $   8     ]     r  {   �       -     )   M  #   w     �     �     �     �          $     @     Z     y     �     �     �  i   �     K     i  +   �     �  "   �      �       %   $  #   J  	   n     x     �     �  9   �  	   �     �  +   �  B   )  Y   l  /   �  H   �     ?  $   ]  '   �  '   �  %   �  .   �  4   '  !   \  ,   ~  $   �     �  �   �  m   �  �   �  '   �  3   �  [     	   x  <   �     �     �  K   �     0  �   H  0   �     �          	                          )     /     N     e  L  s     �     �     �          '     :     O     e     |  %   �  %   �  +   �  #     *   8     c     y     �  +   �     �     �  !   �  1        N  $   W     |     �  {   �     !  -   >  )   l  #   �     �     �     �          ,     C     _     y     �     �     �     �  i         j     �  +   �     �  "   �           3  %   C  #   i  	   �     �     �     �  9   �  	            +      B   H   Y   �   /   �   �   !     �!  $   �!  '   �!  '   "  %   7"  .   ]"  4   �"  !   �"  ,   �"  $   #     5#  �   J#  m   �#  �   Z$  '   %%  3   M%  [   �%  	   �%  <   �%     $&     9&  K   I&     �&  �   �&  0   /'     `'     f'     n'     t'     {'     �'     �'     �'     �'     �'     �'     ;   !   3   L   V   T   b   C   d       $   R                  ,                  '       f          e       X      &       ?       M      P   H      K                %   A       W       N           7           J         "   a          0   *   8   E   <      F   O   >   \       ]   -          [       (   5   c   4           2       `           Y                   1   D   ^       U          B       )   :         G   Z   #      +           =       /      @   	   Q   
      S       9   _      6         I   .       %s: Received scan results <b>Change Password</b> <b>Enter WiFi Password</b> <b>Select WiFi Network</b> <b>Set Country</b> <b>Set Up Screen</b> <b>Setup Complete</b> <b>Update Software</b> <span font_desc="10.0"> </span> <span font_desc="10.0">IP : %s</span> Checking for updates - please wait... Click the name of your network to select it Comparing versions - please wait... Configure Raspberry Pi system on first run Confirm new password: Connected to %s-%s Connecting ... Connecting to WiFi network - please wait... Connecting to dhcpcd ... Connection to dhcpcd lost Could not connect to this network Could not sync time - unable to check for updates Country: Downloading updates - please wait... Enter a new password Enter new password: Enter the details of your location. This is used to set the language, time zone, keyboard and other international settings. Enter the new password again Enter the password for the WiFi network "%s". Enter the password for your WiFi network. Enter the password for your network Error checking for updates.
%s Error comparing versions.
%s Error creating new GIO Channel
 Error creating watch
 Error enabling network Error finding languages.
%s Error getting updates.
%s Error installing languages.
%s Faile to reconfigure. Failed to connect to network. Failed to disconnect. Failed to enable the network. Failed to save wpa_supplicant configuration.

You should add update_config=1 to /etc/wpa_supplicant.conf. Failed to select the network. Failed to set key management. Failed to set password, probably too short. Failed to start association. Finding languages - please wait... Getting updates - please wait... Hide characters Installing languages - please wait... Installing updates - please wait... Language: Network event New Access Point New Access Points No network connection found - unable to check for updates Password: Pre Shared Key: Press 'Next' to activate your new password. Press 'Next' to connect, or 'Skip' to continue without connecting. Press 'Next' to save your setting.

The change will take effect when the Pi is restarted. Press 'Next' when you have made your selection. Press 'Restart' to restart your Pi so the new settings will take effect. Raspberry Pi First-Run Wizard Reading update list - please wait... Searching for networks - please wait... Select your WiFi network from the list. Set the closest city to your location Set the country in which you are using your Pi Set the language in which applications should appear Setting location - please wait... Show or hide the characters in your password Synchronising clock - please wait... System is up to date The default 'pi' user account currently has the password 'raspberry'. It is strongly recommended that you change this to a different password that only you know. The desktop should fill the entire screen.
Tick the box below if your screen has a black border at the edges. The operating system and applications will now be checked and updated if necessary. This may involve a large download.

Press 'Next' to check and update software, or 'Skip' to continue without checking. The two passwords entered do not match. This screen shows a black border around the desktop Tick this box if you can see a black border between the edges of the screen and the desktop Timezone: Use English instead of the default language for your country Use English language Use US keyboard Use the US keyboard layout instead of the default keyboard for your country Welcome to Raspberry Pi Welcome to the Raspberry Pi Desktop!

Before you start using it, there are a few things to set up.

Press 'Next' to get started.  Your Raspberry Pi is now set up and ready to go. _Back _Cancel _Done _Later _Next _OK _Restart _Skip dhcpcd WPA connection lost: %s dhcpcd connection lost g_try_malloc
 Project-Id-Version: piwiz 1.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-02-18 15:13+0000
Last-Translator: Simon Long <simon@raspberrypi.org>
Language-Team: English (British)
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %s: Received scan results <b>Change Password</b> <b>Enter WiFi Password</b> <b>Select WiFi Network</b> <b>Set Country</b> <b>Set Up Screen</b> <b>Setup Complete</b> <b>Update Software</b> <span font_desc="10.0"> </span> <span font_desc="10.0">IP : %s</span> Checking for updates - please wait... Click the name of your network to select it Comparing versions - please wait... Configure Raspberry Pi system on first run Confirm new password: Connected to %s-%s Connecting ... Connecting to WiFi network - please wait... Connecting to dhcpcd ... Connection to dhcpcd lost Could not connect to this network Could not sync time - unable to check for updates Country: Downloading updates - please wait... Enter a new password Enter new password: Enter the details of your location. This is used to set the language, time zone, keyboard and other international settings. Enter the new password again Enter the password for the WiFi network "%s". Enter the password for your WiFi network. Enter the password for your network Error checking for updates.
%s Error comparing versions.
%s Error creating new GIO Channel
 Error creating watch
 Error enabling network Error finding languages.
%s Error getting updates.
%s Error installing languages.
%s Faile to reconfigure. Failed to connect to network. Failed to disconnect. Failed to enable the network. Failed to save wpa_supplicant configuration.

You should add update_config=1 to /etc/wpa_supplicant.conf. Failed to select the network. Failed to set key management. Failed to set password, probably too short. Failed to start association. Finding languages - please wait... Getting updates - please wait... Hide characters Installing languages - please wait... Installing updates - please wait... Language: Network event New Access Point New Access Points No network connection found - unable to check for updates Password: Pre Shared Key: Press 'Next' to activate your new password. Press 'Next' to connect, or 'Skip' to continue without connecting. Press 'Next' to save your setting.

The change will take effect when the Pi is restarted. Press 'Next' when you have made your selection. Press 'Restart' to restart your Pi now so the new settings will take effect, or press 'Later' to close the wizard and restart the Pi yourself. Raspberry Pi First-Run Wizard Reading update list - please wait... Searching for networks - please wait... Select your WiFi network from the list. Set the closest city to your location Set the country in which you are using your Pi Set the language in which applications should appear Setting location - please wait... Show or hide the characters in your password Synchronising clock - please wait... System is up to date The default 'pi' user account currently has the password 'raspberry'. It is strongly recommended that you change this to a different password that only you know. The desktop should fill the entire screen.
Tick the box below if your screen has a black border at the edges. The operating system and applications will now be checked and updated if necessary. This may involve a large download.

Press 'Next' to check and update software, or 'Skip' to continue without checking. The two passwords entered do not match. This screen shows a black border around the desktop Tick this box if you can see a black border between the edges of the screen and the desktop Timezone: Use English instead of the default language for your country Use English language Use US keyboard Use the US keyboard layout instead of the default keyboard for your country Welcome to Raspberry Pi Welcome to the Raspberry Pi Desktop!

Before you start using it, there are a few things to set up.

Press 'Next' to get started.  Your Raspberry Pi is now set up and ready to go. _Back _Cancel _Done _Later _Next _OK _Restart _Skip dhcpcd WPA connection lost: %s dhcpcd connection lost g_try_malloc
 
��            )   �      �     �  -   �     �  2   �  &        8     ?     L      Q     r     {     �     �     �  /   �     �     �     �     �                .  !   ?  7   a     �     �  8   �     �  ?  �     &  )   -     W  2   h  &   �     �     �     �      �     �               4     F  /   N     ~     �     �     �     �     �     �  !   �  7   �     ,     2  8   7     p                                                                             	                                            
                 Acceleration: Beep when there is an error of keyboard input Character Repeat Configure keyboard, mouse, and other input devices Delay before each key starts repeating Delay: Double-click Fast Interval between each key repeat Keyboard Keyboard Layout... Keyboard and Mouse LXInput autostart Layout: Left handed (Swap left and right mouse buttons) Long Model: Motion Mouse Mouse and Keyboard Settings Repeat delay: Repeat interval: Setting keyboard - please wait... Setup keyboard and mouse using settings done in LXInput Short Slow Type in the following box to test your keyboard settings Variant: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-29 01:28+0000
Last-Translator: Anonymous Pootle User
Language-Team: English UK en_GB
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Speed: Beep when there is a keyboard input error Character Repeat Configure mouse, keyboard, and other input devices Delay before each key starts repeating Delay: Double-click Fast Interval between each key repeat Keyboard Keyboard Layout... Mouse and Keyboard Settings LXInput autostart Layout: Left handed (Swap left and right mouse buttons) Long Model: Motion Mouse Mouse and Keyboard Settings Repeat delay: Repeat interval: Setting keyboard - please wait... Setup keyboard and mouse using settings done in LXInput Short Slow Type in the following box to test your keyboard settings Variant: 
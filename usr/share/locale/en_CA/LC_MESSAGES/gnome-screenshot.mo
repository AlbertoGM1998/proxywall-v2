��          �            h     i     k  *   y     �  -   �  ;   �          -     >     S     b  2   r  /   �     �  Y  �     6     8  *   F     q  -   �  ;   �     �     �                /  2   ?  /   r     �                                                             
               	    * Border Effect Grab a window instead of the entire screen Include Border Include the window border with the screenshot Include the window manager border along with the screenshot Save Screenshot Save in _folder: Screenshot directory Screenshot.png Take Screenshot Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. _Name: Project-Id-Version: gnome-utils
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2005-07-18 12:11-0500
Last-Translator: Adam Weinberger <adamw@gnome.org>
Language-Team: Canadian English <adamw@gnome.org>
Language: en_CA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 * Border Effect Grab a window instead of the entire screen Include Border Include the window border with the screenshot Include the window manager border along with the screenshot Save Screenshot Save in _folder: Screenshot directory Screenshot.png Take Screenshot Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. _Name: 
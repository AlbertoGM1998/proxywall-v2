��    "      ,  /   <      �     �     	  
        &     ,     @     I     M  ;   \     �     �     �  "   �     �     �     �     �  1        O     ]     b  
   j  	   u          �     �     �     �     �     �  ?   �     5     ;  �  @  #   �  /   �  '   &     N  H   j     �     �  '   �  �   �     �     �  W   	  �   c	  3   Q
  B   �
  '   �
  x   �
  �   i  B   Y     �  '   �  $   �  *   �  c   *  9   �  0   �  -   �  W   '  �     0     �   8          2                                                                        "       !   
          	                                                                  3d-ish (button) 3d-ish (gradient) Animations Arrow Cell Indicator Size Contrast Dot Edge Thickness Enable Animations on Progressbars, Radio-, and CheckButtons Flat Full Handlebox Marks If set, button corners are rounded Inverted Dot Inverted Slash Mark Type 1 Mark Type for Scrollbar Buttons Mark Type for Scrollbar Handles, Handleboxes, etc Menubar Style None Nothing Paned Dots Rectangle Rounded Buttons Scrollbar Color Scrollbar Marks Scrollbar Type Scrollbutton Marks Sets the Color of Scrollbars Shaped Size of check- and radiobuttons inside treeviews. (Bug #351764) Slash Some Project-Id-Version: gtk-engines
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2007-03-16 03:31+0530
Last-Translator: yangka <yanang_ka@hotmail.com>
Language-Team: dzongkha <pgeyleg@dit.gov.bt>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2;plural=(n!=1);
X-Poedit-Language: Dzongkha
X-Poedit-Country: BHUTAN
X-Poedit-SourceCharset: utf-8
 ཌིཤ་༣(ཨེབ་རྟ) ཌིཤ་༣(སྟེགས་རིས་) བསྒུལ་བཟོ་ཚུ། མདའ་རྟགས། ནང་ཐིག་བརྡ་སྟོན་མིའི་ཚད། ཚོན་མདངས། ཚག མཐའམ་སྟུག་ཚད། བསྒུལ་བཟོ་ཚུ་ཡར་འཕེལ་ཕྲ་རིང་ཚུ་ རེཌིའོ་ དང་བརྟག་ཞིབ་ཨེབ་རྟ་ཚུ་གུ་ལྕོགས་ཅན་བཟོ། ལེབ་ཏེམ། གངམ། ལེགས་སྐྱོང་པའི་སྒྲོམ་རྟགས་ཚུ། གཞི་སྒྲིག་འབད་ཡོདཔ་ཨིན་པ་ཅིན་ཨེབ་རྟ་སྒྱིད་ཁུག་ཚུ་ཆ་མཉམ་སྒོར་སྒོརམ་བཟོ་ཡོདཔ་ཨིན། གནས་ལོག་ཡོད་མི་ཚག གནས་ལོག་ཡོད་མི་གཡོ་ཤད། རྟགས་དབྱེ་བ་༡ བཤུད་ཕྲ་ཨེབ་རྟ་ཚུ་གི་དོན་ལུ་རྟགས་དབྱེ་བ། བཤུད་ཕྲ་ལེགས་སྐྱོང་པ་ཚུ་ ལེགས་སྐྱོང་པའི་སྒྲོམ་ཚུ་ ལ་སོགས་པ་གི་དོན་ལུ་རྟགས་དབྱེ་བ། དཀར་ཆག་ཕྲ་རིང་བཟོ་རྣམ། ཅི་མེད། ག་ནི་ཡང་མེདཔ། པེནཌི་ཚག་ཚུ། གྲུ་བཞི་ནར་མོ། སྒོར་སྒོརམ་བཟོ་ཡོད་པའི་ཨེབ་རྟ་ཚུ། རྒྱབ་སྒྲིལ་ཚོས་གཞི། བཤུད་ཕྲ་རྟགས་ཚུ། བཤུད་ཕྲ་དབྱེ་བ། བཤུད་སྒྲིལ་ཨེབ་རྟ་གི་རྟགས་ཚུ། བཤུད་ཕྲ་ཚུ་གི་ཚོས་གཞི་འདི་གཞི་སྒྲིག་འབདཝ་ཨིན། དབྱིབས་བཟོ་ཡོདཔ། བརྟག་ཞིབ་དང་རེཌིའོ་ཨེབ་རྟ་ཚུའི་ནང་ན་རྩ་འབྲེལ་མཐོང་སྣང་ཚུ་གི་ཚད (རྒྱེན་ #༣༥༡༧༦༤) གཡོ་ཤད། ལ་ལོ། 
��    0      �  C         (     )     5     H     `     n     �  �   �     )     :  :   F     �     �  "   �  	   �     �  '   �  o     $   x  "   �     �     �  
   �  	   �  	   �                     %  -   ,     Z     f     k  
   q     |     �     �     �  	   �     �     �     �  
   �     �     �     �  
   �       �       �	     �	     �	     �	     	
     
  �   1
     �
     �
  6   �
     -     6     ?     Z  "   j     �  k   �  '        >     ]  
   u     �     �     �     �     �     �     �  2   �          ,     3     9     A     P     `  
   u     �     �  
   �     �  
   �     �     �     �     �  
   �                                            "   
      !   #      *          .      0                    ,   )       $      &                              /             +       (              '                       -            %   	        <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Applications automatically started after entering desktop: Apply Authentication Automatically Started Applications Autostart Banner to show on the dialog Change the default applications on LXDE Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Custom message to show on the dialog Default applications for LXSession Desktop Session Settings Enabled Error: %s
 Group: %s Identity: Information L_ock Screen Laptop mode Layout Manage applications loaded in desktop session Menu prefix Mode Model Password:  Position of the banner Reload S_witch User Settings Sh_utdown Type Variant Window Manager: _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-10-26 18:29+0000
Last-Translator: system user <>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1414348154.000000
 <b>D-Bus</b> <b>Okolje</b> <b>Splošne nastavitve</b> <b>Razporeditev tipk</b> <b>Znani programi</b> <b>Nastavitve</b> <b>Opozorilo:</b> te možnosti <b>NE</b> spreminjajte, razen če se zavedate, kaj počnete.

<b>Opomba:</b> spremembe bodo začele veljati ob naslednji prijavi v sistem. Napredne možnosti Program Programi, ki se samodejno zaženejo skupaj z namizjem: Uveljavi Overitev Samodejno zagnani programi Samodejni zagon Slika, prikazana v pogovornem oknu Spremenite privzete programe Ukaz, uporabljen za zagon upravljalnika oken
(Privzeti ukaz upravljalnika oken za LXDE je »openbox-lxde«) Sporočilo, prikazano v pogovornem oknu Privzeti programi za LXSession Nastavitve namizne seje Omogočeno Napaka: %s
 Skupina: %s Istovetnost: Podatki _Zakleni zaslon Način prenosnega računalnika Razporeditev Upravljajte s programi, naloženimi v namizni seji Predpona menija Način Model Geslo:  Položaj slike Ponovno naloži Zamenjaj _uporabnika Nastavitve _Izklop Vrsta Različica Upravljalnik oken: _Mirovanje _Odjava Ponovni _zagon _Pripravljenost slika sporočilo 
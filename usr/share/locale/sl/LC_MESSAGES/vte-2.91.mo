��          L      |       �   /   �      �   =   �   +   5     a  6  i  U   �  ,   �  O   #  (   s  	   �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=vte&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-05-30 20:49+0200
Last-Translator: Matej Urbančič <mateju@svn.gnome.org>
Language-Team: Slovenian GNOME Translation Team <gnome-si@googlegroups.com>
Language: sl_SI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 2.0.6
 Napaka (%s) pretvarjanja podatkov za podrejeni predmet; izvajanjeukaza je preklicano. Napaka med branjem podrejenega predmeta: %s. Paket GNUTLS ni omogočen; podatki bodo na disk zapisani na nešifriran način! Ni mogoče pretvoriti znakov iz %s v %s. OPOZORILO 
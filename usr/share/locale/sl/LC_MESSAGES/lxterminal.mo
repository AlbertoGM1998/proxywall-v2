��    3      �  G   L      h     i     r     �  
   �     �     �     �     �     �  	   �     �     �  *   �  %         F  
   N     Y     l     z  
   �     �     �     �     �  	   �     �  	   �     �     �                    #     4     N     T     g  "   p     �     �  	   �     �     �  
   �     �     �     �     �     �     �  �       �     �     �     �     �     �     �     
	     %	     .	     ;	     M	  "   Z	     }	     �	     �	     �	     �	     �	  
   �	  	   �	     �	     �	     
     $
     6
     G
     Z
  
   g
     r
  
   �
     �
     �
  1   �
     �
     �
     �
          &     -  	   1     ;     R     `     o  	   v     �     �     �  #   �                     !                        #                           1                    
      (   0   2   -   )      .                          $                 /          +      3   &       %   "   	                            ,           '   *    Advanced Allow bold font Audible bell Background Block Bottom Cl_ear scrollback Clear scr_ollback Cop_y Copy _URL Cursor blink Cursor style Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Hide Close buttons Hide menu bar Hide scroll bar LXTerminal LXTerminal Preferences Left Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New _Tab New _Window Pre_vious Tab Preference_s Right Scrollback lines Select-by-word characters Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line _About _Close Tab _Edit _File _Help _Paste _Tabs translator-credits Project-Id-Version: lxterminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-10-26 18:55+0000
Last-Translator: system user <>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1414349702.000000
 Napredno Dovoli krepko pisavo Zvok Ozadje Pravokotnik Dno _Počisti pomnjene vrstice Počisti _pomnjene vrstice _Kopiraj Kopiraj _URL Utripanje kazalke Slog kazalke Onemogoči bližnjico menija (F10) Onemogoči Alt+N Izgled Barva pisave Skrij gumb »Zapri« Skrij menijsko vrstico Skrij drsnik LXTerminal Možnosti Levo Premakni zavihek _levo Premakni zavihek _desno Poimenuj _zavihek Poimenuj zavihek _Naslednji zavihek Nov _zavihek _Novo okno _Predhodni zavihek _Možnosti Desno Število pomnjenih vrstic Znaki, ki štejejo kot del besede (pri izbiranju) Splošno Položaj zavihkov Terminal Posnemovalnik terminala za LXDE Pisava Vrh Podčrtaj Uporabi ukazno vrstico _O LXTerminal Zapri za_vihek _Uredi _Datoteka Pomo_č _Prilepi _Zavihki Klemen Košir <klemen913@gmail.com> 
��          �   %   �      @     A     Y     p     ~  -   �  2   �  &   �                     7     X     a  /   t     �     �     �     �     �     �     �     �  	   �  8   �  z  -  !   �     �     �  	   �  J     D   P  <   �  
   �  
   �     �  A     
   G     R  ]   h  
   �  
   �     �     �  (        *     8  
   A     L  s   \                          	                                
                                                                    <b>Character Repeat</b> <b>Keyboard layout</b> <b>Motion</b> Acceleration: Beep when there is an error of keyboard input Configure keyboard, mouse, and other input devices Delay before each key starts repeating Fast High Input Device Preferences Interval between each key repeat Keyboard Keyboard and Mouse Left handed (Swap left and right mouse buttons) Long Low Mouse Repeat delay: Repeat interval: Sensitivity: Short Slow Touch Pad Type in the following box to test your keyboard settings Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-08-17 01:28+0000
Last-Translator: Anonymous Pootle User
Language-Team: LANGUAGE <LL@li.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1439774939.174364
 <b>חזרה על התווים</b> <b>פריסת המקלדת</b> <b>תנועה</b> האצה: ישמע צפצוף כאשר מתרחשת שגיאה בקלט המקלדת הגדרת המקלדת, העכבר והתקני קלט נוספים השהיה בטרם החזרה לאחר לחיצה על תו מהירה גבוהה העדפות התקן קלט משך ההשהיה עד לחזרה לאחר לחיצה על תו מקלדת מקלדת ועכבר מותאם לשמאליים (החלפה בין הלחצן השמאלי לימני בעכבר) ארוכה נמוכה עכבר השהיה עד לחזרה: משך ההמתנה בין החזרות: רגישות: קצרה איטית משטח מגע ניתן להקליד את בתיבת הטקסט שלהלן כדי לבדוק את הגדרות המקלדת שלך 
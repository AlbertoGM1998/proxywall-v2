��    �      �  �   �	      �  
   �     �        N     C   T  3   �  F   �  ?     5   S  :   �     �     �     �       #   -     Q     q  9   �  *   �  #   �       +   6      b  !   �  -   �  6   �  *   
  #   5  <   Y  *   �  #   �      �  #     (   *  "   S  !   v  .   �     �     �     �     �     	       2   %     X  ,   _  +   �  
   �     �     �     �     �     �     �       3   $     X  !   f     �     �  �   �     I     R     W     v  k   �  O   �  d   F     �     �     �     �     �  #        %     6     E     U  
   c     n     �     �     �     �     �     �     �            8   ,  3   e  �   �     _     k     q     ~     �      �     �     �     �     �  '        @     S     n     t     x  	   �  #   �  O   �  �   �  $   �  #   �  *   �            <   ;     x     �  -   �  )   �     �     �  (     +   /     [     q     �     �  5   �     �     �               #     0     F  *   U  
   �  5   �     �  ,   �  :   �     0  )   5     _     b     e     m  R  o     �   D   �      !  b   !  �   }!  D   "  j   d"  v   �"  B   F#  N   �#  '   �#  '    $  1   ($  .   Z$  2   �$  .   �$  &   �$  >   %  E   Q%  4   �%  !   �%  .   �%  %   &  )   C&  D   m&  _   �&  8   '  -   K'  y   y'  :   �'  -   .(  #   \(  <   �(  4   �(  #   �(  B   )  `   Y)  S   �)     *  	   %*     /*     A*     a*  i   y*     �*  *   �*  ,   +     H+  
   ^+     i+     �+     �+     �+  &   �+  !   �+  v   ,     z,  C   �,  1   �,  ,   �,  #  +-     O.  %   i.  >   �.  (   �.  �   �.  t   �/  �   >0  -   �0  2   1  2   ;1  ?   n1     �1  L   �1     2     +2     F2     c2     �2  B   �2  E   �2  E   )3  
   o3     z3     �3     �3  /   �3     �3  7   �3  ]   64  m   �4  E  5     H6     T6     c6  1   w6     �6  J   �6      7     17     >7  @   Z7  9   �7     �7     �7     8     8     (8     @8  9   Z8  g   �8    �8  B   :  #   Z:  j   ~:     �:  7   ;  r   ;;     �;  /   �;  1   �;  8   <     R<     Z<  c   x<  V   �<  .   3=  /   b=  /   �=  #   �=  b   �=     I>  3   i>  *   �>  	   �>     �>  )   �>     ?  L   -?     z?  b   �?     �?  i   �?  z   h@  	   �@  Q   �@     ?A     DA  	   KA     UA     �   .   (   �      B   5       X       P   ~   n               O   i   �           %   L               7   G       �   �           *                              >   8   \   _          E   
   W   0   �       ^   +   g       S   �       A              4          2      �   $   I   J   9   a   �      M       #          [   �   "       �   ,   h   t   |      @           R   k   Z   q       =   `   �   p       -   Q           �   �   �      w   �           C   ;      ]   y      1          N   m   U   <   d          &      �   z       :         b      o   T       �           f       /   H   v   �          {                 	   l       '   3       r   �   x   V   F   u   e          )       j   D   s       K   �   6          Y       }           �       c                      �              ?       !   �    
Options:
 
Please report bugs at %s

        --archive THEME       Create a theme archive from the given theme directory
   --config-file FILE    Specify the path to the config file to use
   --help                Display this help and exit
   --install ARCHIVE.obt Install the given theme archive and select it
   --tab NUMBER          Switch to tab number NUMBER on startup
   --version             Display the version and exit
 "%s" does not appear to be a valid Openbox theme directory "%s" was installed to %s "%s" was successfully created (Unnamed desktop) --archive requires an argument
 --config-file requires an argument
 --install requires an argument
 --tab requires an argument
 <span weight="bold" size="xx-large">ObConf VERSION</span> <span weight="bold">Desktop Margins</span> <span weight="bold">Desktops</span> <span weight="bold">Dock</span> <span weight="bold">Focusing Windows</span> <span weight="bold">Fonts</span> <span weight="bold">Hiding</span> <span weight="bold">Information Dialog</span> <span weight="bold">Moving and Resizing Windows</span> <span weight="bold">Placing Windows</span> <span weight="bold">Position</span> <span weight="bold">Press the key you wish to bind...</span> <span weight="bold">Primary Monitor</span> <span weight="bold">Stacking</span> <span weight="bold">Theme</span> <span weight="bold">Titlebar</span> <span weight="bold">Window Titles</span> <span weight="bold">Windows</span> A preferences manager for Openbox A_llow dock to be both above and below windows A_nimate iconify and restore Abo_ut About ObConf Above the window Active Monitor All monitors Allow _windows to be placed within the dock's area Always Amount of resistance against other _windows: Amount of resistance against screen _edges: Appearance Bottom Bottom Left Bottom Right C:
S:
D: Centered Centered on the window Choose an Openbox theme Close
Shade (Roll up)
Omnipresent (On all desktops) Copyright (c) Create a theme _archive (.obt)... Custom actions Delay before _showing: Desktop margins are reserved areas on the edge of your screen.  New windows will not be placed within a margin, and maximized windows will not cover them. Desktops Dock Double click on the _titlebar: Double click ti_me: Error while parsing the Openbox configuration file.  Your configuration file is not valid XML.

Message: %s Failed to load an rc.xml. You have probably failed to install Openbox properly. Failed to load the obconf.glade interface file. You have probably failed to install ObConf properly. Fixed Monitor Fixed _x position: Fixed _y position: Fixed position on screen Floating Focus _new windows when they appear From bottom edge From left edge From right edge From top edge Horizontal Information dialog's _position: Keep dock _above other windows Keep dock _below other windows Left Margins Maximizes the window Menu _header:  Monitor With Mouse Pointer Mouse Move & Resize Move focus _under the mouse when the mouse is not moving Move focus under the mouse when _switching desktops N - The window's icon
D - The all-desktops (sticky) button
S - The shade (roll up) button
L - The label (window title)
I - The iconify (minimize) button
M - The maximize button
C - The close button N:
L:
I:
M: Never ObConf Error Openbox Configuration Manager Openbox theme archives Prefer to place new windows _on: Primary _monitor: Right Shades the window Show _information dialog: Syntax: obconf [options] [ARCHIVE.obt]
 The active monitor The monitor with the mouse Theme Top Top Left Top Right Unable to create directory "%s": %s Unable to create the theme archive "%s".
The following errors were reported:
%s Unable to extract the file "%s".
Please ensure that "%s" is writable and that the file is a valid Openbox theme archive.
The following errors were reported:
%s Unable to move to directory "%s": %s Unable to run the "tar" command: %s Update the window contents while _resizing Vertical When resizing terminal windows Window icon
Window label (Title)
Iconify (Minimize)
Maximize Windows _Active window title:  _Amount of time to show the notification for: _Amount of time to wait before switching: _Bottom _Button order: _Center new windows when they are placed _Delay before focusing and raising windows: _Delay before hiding: _Desktop names: _Fixed monitor: _Floating position: _Focus windows when the mouse pointer moves over them _Hide off screen _Inactive window title:  _Install a new theme... _Left _Menu Item:  _Number of desktops:  _Orientation:  _Place new windows under the mouse pointer _Position: _Raise windows when the mouse pointer moves over them _Right _Show a notification when switching desktops _Switch desktops when moving a window past the screen edge _Top _Windows retain a border when undecorated ms px window1 x Project-Id-Version: ObConf-2.0.4
Report-Msgid-Bugs-To: http://bugzilla.icculus.org/
PO-Revision-Date: 2015-01-08 19:08+0300
Last-Translator: peregrine <andrej1741@yandex.ru>
Language-Team: Russian <ru@li.org>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
 
Опции:
 
Пожалуйста сообщите об ошибках на %s

        --archive ТЕМА      Создать архив темы из данного каталога
   --config-file ФАЙЛ  Специфический путь к конфигурационному файлу, который будет использован
   --help              Показать справку и выйти
   --install АРХИВ.obt Установить данный архив темы и выбрать его
   --tab НОМЕР           Переключиться на вкладку номер НОМЕР при старте
   --version           Показать версию и выйти
 "%s" кажется, не допустимый каталог темы Openbox "%s" был установлен в  %s "%s" был успешно создан (Рабочий стол без названия) --archive требуется аргумент
 --config-file требуется аргумент
 --install требуется аргумент
 --tab требует аргумент
 <span weight="bold" size="xx-large">Версия ObConf</span> <span weight="bold">Отступы Рабочего стола</span> <span weight="bold">Рабочие столы</span> <span weight="bold">Док</span> <span weight="bold">Фокус окон</span> <span weight="bold">Шрифт</span> <span weight="bold">Скрытие</span> <span weight="bold">Информационный диалог</span> <span weight="bold">Перемещение и изменение размера окон</span> <span weight="bold">Размещение окон</span> <span weight="bold">Положение</span> <span weight="bold">Нажмите клавиши, к которым Вы желаете привязаться...</span> <span weight="bold">Основной монитор</span> <span weight="bold">Наложение</span> <span weight="bold">Тема</span> <span weight="bold">Область заголовка</span> <span weight="bold">Название окна</span> <span weight="bold">Окна</span> Привилегированный менеджер для Openbox _Разрешить области уведомлений быть выше и ниже окон А_нимированое  сворачивание и восстановление О про_грамме О ObConf Выше окна Активный монитор Все мониторы Разре_шить окнам быть размещенными в области уведомлений Всегда Отступ от других _окон:  Отступ от краёв _экрана:  Внешний вид Внизу Внизу слева  Внизу справа C:
S:
D: Центрировать Центрировать на окне Выберите тему Openbox Закрыть
Свернуть в заголовок
Вездесущий (На всех рабочих столах) Copyright (c) Создание нового _архива с темой (.obt)... Пользовательские действия Задержка перед показо_м: Отступы рабочего стола - зарезервированные области на краях Вашего экрана. Новые окна не будет помещены на отступы, и развернутые окна не будет перекрывать их. Рабочие столы Область уведомления Двойной к_лик в области заголовка: Время д_войного клика: Ошибка цикла анализа файл конфигурации Openbox. Ваш конфигурационный файл не действительного формата XML.
Сообщение: %s Ошибка загрузки rc.xml. Вероятно, Openbox был установлен не правильно. Ошибка загрузки файла .glade интерфейса obconf. Вероятно, Openbox был установлен не правильно. Зафиксированный монитор Фиксированная позиция по _x: Фиксированная позиция по _y: Фиксированное положение на экране Раздельно Фокус на _новые окна, когда они появляются По нижнему краю По левому краю По правому краю По верхнему краю По горизонтальи Положение _информационного диалога: Область уведомлений _выше других окон Область уведомлений _ниже других окон Слева Отступы Развернуть окно Заго_ловок меню: Монитор с указателем мыши Мышь Перемещение и измение размера Перемещать фокус под мышь, когда _мышь не двигается Перемещать фокус под мышь при  переключении _рабочих столов N - Значок окна 
D - Все-рабочие столы (липкая) кнопка 
S - Кнопка сворачивания в заголовок
L - Метка (название окна) 
I - Кнопка сворачивания 
М - Кнопка восстановления 
C - Кнопка закрытия N:
L:
I:
M: Никогда Ошибка ObConf Менеджер конфигурации Openbox Архив темы Openbox Пре_дпочтительно помещать новые окна на: Основной _монитор Справа Отбросить тень Отображать и_нформационный диалог: Синтаксис: obconf [опции] [Архив.obt]
 Активный монитор Монитор с мышью Тема Вверху Вверху слева Вверху справа Невозможно создать калаог "%s": %s Невозможно создать архив темы "%s".
Сообщение об ошибке: 
%s Невозможно извлечь файл "%s".
Пожалуйста убедитесь, что "%s "  доступен для записи и, что файл действительно является  архивом темы Openbox. Сообщение об ошибке: 
%s Не удается переместить каталог "%s": %s Unable to run the "tar" command: %s Обновить содержимое  окна, когда  изменетются его _размеры По вертикальи Когда изменяются размеры окна Значок окна
Метка окна (Название)
Сворачивание
Восстановление Окна Название _активного окна:  _Время показа уведомления:  За_держка перед переключением: _Низ _Порядок кнопок  _Выравнивать по центру новые окна, когда они размещены З_адержка перед фокусировкой и поднятием окон:  Задержка перед с_крытием: _Названия  рабочих столов: _Зафиксированный монитор: _Плавающая позиция: _Фокус на окне, когда курсор мыши перемещается по нему _Скрыть на экране Название н_еактивного окна:  _Установка новой темы... _Лево _Элемент меню: _Кол-во рабочих столов: _Ориентация: _Поместить новые окна под указателем мыши Поло_жение: _Поднять окно, когда курсор мыши перемещается над ним  _Право П_оказывать уведомления, при переключении рабочих столов _Переключать рабочие столы, когда окно перемещается на край экрана _Верх _Окно сохраняет границу, когда нет декорации мс пик окно1 x 
��            )   �      �     �     �  	   �     �     �     �     �            5   '     ]     d     p     �     �  $   �     �     �     �     �     �            	        $     ,     :     K     a    f     v     �     �     �     �     �     �     �       :        S     Z     h     �     �     �     �  
   �     �                         &     -     6     C     K     [              	                                                         
                                                                  (correct spelling) (no suggested words) Add w_ord Cha_nge Change A_ll Change _to: Check Spelling Check _Word Completed spell checking Error when checking the spelling of word “%s”: %s Error: Ignore _All Misspelled word: No language selected No misspelled words Select the spell checking _language. Set Language Suggestions User dictionary: _Add _Cancel _Ignore _Ignore All _Language _Select _Suggestions: language%s (%s) languageUnknown (%s) word Project-Id-Version: gspell
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gspell
POT-Creation-Date: 2017-10-29 09:50+0100
PO-Revision-Date: 2016-09-04 16:53+0300
Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>
Language-Team: suomi <lokalisointi-lista@googlegroups.com>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-02-19 15:15:59+0000
X-Generator: Gtranslator 2.91.7
 (oikea kirjoitusasu) (ei suositeltavia sanoja) Lisää _sana M_uuta Ta_llenna kaikki Mihin _muutetaan: Tarkista oikeinkirjoitus Tarkista _sana Oikoluku on valmis Virhe sanan ”%s” oikeinkirjoituksen tarkistuksessa: %s Virhe: Ohita k_aikki Väärinkirjoitettu sana: Kieltä ei ole valittu Ei väärinkirjoitettuja sanoja Valitse _oikoluvun kieli. Aseta kieli Ehdotukset Käyttäjän sanakirja: _Lisää _Peru _Ohita _Ohita kaikki _Kieli _Valitse _Ehdotukset: %s (%s) Tuntematon (%s) sana 
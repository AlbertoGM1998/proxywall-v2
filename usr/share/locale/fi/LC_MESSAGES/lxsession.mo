��    j      l  �   �      	  (   	     :	     F	     Y	     q	     	  &   �	     �	  �   �	     a
     f
     w
  :   �
     �
     �
     �
     �
     �
  &   �
  "     	   @     J  	   g     q  
   �  '   �     �     �  o   �     >     N     ^     p  $   �  "   �     �     �  "   �          $     )     2     8     @  
   F     Q  	   ^  	   h     r          �     �     �     �     �     �     �     �  -        6     B     G     M     R     ^     l  
   t       
   �     �     �     �     �     �     �     �     �               '     :  	   C     M     Y     g     x  %   �     �     �     �     �     �  
   �     �           	          )     ;  
   B     M     U     ]  
   f     q  �  y  7   
     B     N     a     z     �  A   �     �  �        �     �     �  L   �          &     .     >     K  1   T  %   �     �  &   �  
   �     �            	   -     7  k   D     �     �     �     �  )   �          7     M  E   k     �     �  	   �     �     �     �  
   �     �  
             (     6     >     G     ]     o     �     �     �     �  7   �          "     '     -     5     E  	   S     ]     j  
   r     }     �     �     �     �     �     �               -     G  	   a     k     t     �     �     �  "   �     �     �                    $     +  	   7     A     T     f     z  	   �     �     �     �     �     �     .                   M              1       E   e              !      I   T   B          G      8   `   V   *   7       5                ,   2   6      0             &          D   i          j   @         )       C   A   H   b   g                     h   N   a   P   c   X   ;           d   ?              <   ^   3              L   %          (       ]              R                         '       =   	   Q   Z       Y          _           f   4      "       J            >   #   [       S   U           
   W   $   O   9       +       -   K   /   \   :   F    <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. A11y Advanced Options Application Applications automatically started after entering desktop: Apply Archive Audio manager Audio player Authentication Authentication failed!
Wrong password? Automatically Started Applications Autostart Banner to show on the dialog Bittorent Burning utility Calculator Change the default applications on LXDE Charmap Clipboard manager Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Communication 1 Communication 2 Composite Manager Core applications Custom message to show on the dialog Default applications for LXSession Desktop Manager Desktop Session Settings Disable autostarted applications ? Disks utility Dock Document Email Enabled Error Error: %s
 File Manager Group: %s Identity: Image viewer Information LXPolKit LXSession configuration L_ock Screen Laptop mode Launcher manager Launching applications Layout Lock screen manager Manage applications loaded in desktop session Menu prefix Mode Model More Network GUI Notes utility Options PDF Reader Panel Password:  Policykit Authentication Agent Polkit agent Position of the banner Power Manager Proxy Quit manager Reload S_witch User Screensaver Screenshot manager Security (keyring) Settings Sh_utdown Spreadsheet Tasks monitor Terminal manager Text editor The database is updating, please wait Type Update lxsession database Upgrade manager Variant Video player Webbrowser Webcam Widget 1 Window Manager: Windows Manager Workspace manager Xrandr _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-07-19 16:44+0000
Last-Translator: Juhani Numminen <juhaninumminen0@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1500482679.706056
 <b><big>Kirjaudutaanko ulos istunnosta %s %s?</big></b> <b>Dbus</b> <b>Ympäristö</b> <b>Yleiset asetukset</b> <b>Näppäinasettelu</b> <b>Tunnetut sovellukset</b> <b>Käsin asetetut automaattisesti käynnistyvät sovellukset</b> <b>Asetukset</b> <b>Varoitus:</b> <b>ÄLÄ</b> koske tähän ellet tiedä tarkalleen mitä olet tekemässä.

<b>Huom:</b> Tämä asetusta käytetään seuraavan kirjautumisen jälkeen. Esteettömyys Lisäasetukset Sovellus Automaattisestikäynnistettävät ohjelmat työpöydälle pääsyn jälkeen: Toteuta Arkisto Äänenhallinta Äänisoitin Todennus Tunnistautuminen epäonnistui!
Väärä salasana? Automaattisesti avattavat sovellukset Automaattinen käynnistys Valintaikkunassa näytettävä banneri BitTorrent Poltto-ohjelma Laskin Muuta LXDE:n oletussovelluksia Merkistö Leikepöytä Ikkunaohjelman käynnistämiseen käytettävä komento
(Oletuskomennon LXDE:lle pitäisi olla openbox-lxde) Viestintä 1 Viestintä 2 Komposiittihallinta Perussovellukset Valintaikkunassa näytettävä oma viesti LXSessionin oletussovellukset Työpöydän hallinta Työpöytäistunnon asetukset Otetaanko automaattisesti käynnistetyt sovellukset pois käytöstä? Levytyökalu Telakka Asiakirja Sähköposti Käytössä Virhe Virhe: %s
 Tiedostonhallinta Ryhmä: %s Henkilöllisyys: Kuvankatselin Tietoja LXPolKit LXSessionin asetukset _Lukitse näyttö Kannettava tietokone -tila Käynnistimien hallinta Sovellusten käynnistäminen Asettelu Näytön lukitus Hallitse automaattisesti käynnistettäviä sovelluksia Valikon etuliite Tila Malli Lisää Verkkoasetukset Muistiinpanot Asetukset PDF-katselin Paneeli Salasana:  PolicyKit-tunnistautumisohjelma Polkit-agentti Bannerin sijainti Virranhallinta Välipalvelin Lopeta hallintaohjelma Lataa uudelleen _Vaihda käyttäjää Näytönsäästäjä Kuvankaappausten hallinta Turvallisuus (avainnippu) Asetukset _Sammuta Taulukkolaskenta Tehtävienhallinta Päätteenhallinta Tekstimuokkain Tietokanta päivittyy, odota hetki Tyyppi Päivitä lxsession-tietokanta Päivitykset Muunnos Videosoitin Selain Webbikamera Sovelma 1 Ikkunointiohjelma: Ikkunointiohjelma Työtilojenhallinta Xrandr _Lepotila K_irjaudu ulos Käynnistä _uudelleen _Keskeytystila kuvatiedosto viesti 
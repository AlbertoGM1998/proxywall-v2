��    a      $  �   ,      8     9     B     R  
   _     j     p     v     {     �     �     �     �  
   �     �     �     �  	   �     �     	     	     	     $	  	   )	     3	     K	     X	     e	     j	     s	  ?   �	  *   �	  %   �	     
  
    
     +
     0
     6
     I
     W
     j
  
   z
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
  	   �
            
             '     3     <     D     S     Y     g     t     �     �     �     �  	   �     �     �     �  "   �            	        !     6     <  B   C     �  	   �     �  
   �     �     �     �  
   �     �     �     �     �     �     �     �          /  �  1     �     �                    #     +     3     <     M     ]     n          �     �     �     �     �     �     �                    #     <     M     ^     e     s  0   �  7   �  =   �     -     4     @     G     O     j     �     �  
   �     �  
   �     �     �           !     B     a     t     �     �     �     �     �     �     �     �                 
   5     @     U     ^     g     x     �     �     �     �     �     �     �            	   '  	   1  9   ;  	   u  	        �     �  
   �     �     �     �     �  	   �     �     �            ^     U   w     �     W      ,   R   M      H   O       '       4   U   S   `   
      Q   ]   B      @      I       8       5   -      Z          G   N       \   .          P   6   F      A                             ;          0                    /   ?          7       $                 L      =              ^       +   (       1               X   &   T   D      *   E       !              [   	      #          C      >           :       <   2   Y      V   K       a   "       _      3   )      J   %   9    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Close Tab _Edit _File _Help _New Window _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-13 21:47+0200
Last-Translator: Juhani Numminen <juhaninumminen0@gmail.com>
Language-Team: Finnish <gnome-fi-laatu@lists.sourceforge.net>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
X-POOTLE-MTIME: 1433933143.000000
 Lisäasetukset Salli lihavointi Äänimerkki Taustaväri Musta Palikka Sininen Alhaalla Kirkkaan sininen Kirkkaan syaani Kirkkaan vihreä Kirkkaan magenta Kirkkaan punainen Ruskea T_yhjennä rivihistoria T_yhjennä rivihistoria Sulje välilehti Sulje ikkuna _Sulje ikkuna Vahvista sulkeminen _Kopioi Kopioi Kopioi _URL Copyright © 2008–2017 Vilkkuva osoitin Osoittimen muoto Syaani Tumman harmaa Ikkunan oletuskoko Älä varoita ennen usean välilehden sulkemista Älä käytä valikon pikanäppäintä (oletuksena F10) Älä käytä pikanäppäintä Alt-n välilehden vaihtamiseen Ikkuna Edustaväri Harmaa Vihreä Piilota sulkemispainikkeet Piilota valikkopalkki Piilota hiiren osoitin Piilota vierityspalkki LXTerminal LXTerminalin asetukset Vasemmalla Magenta Siirrä välilehteä vasemmalle Siirrä välilehteä oikealle Siirrä välilehteä _vasemmalle Siirrä välilehteä _oikealle _Nimeä välilehti Nimeä välilehti _Seuraava välilehti Uusi _välilehti Uusi välilehti Uusi ikkuna Uusi _välilehti _Uusi ikkuna Seuraava välilehti Valipaletti Paletin esiasetus Liitä _Edellinen välilehti _Asetukset Edellinen välilehti Punainen Oikealla Takaisinvieritys Sanoina valittavat merkit Pikanäppäimet Tyyli Välilehtipalkin sijainti Pääte LXDE-projektin pääteohjelma Päätteen kirjasin Ylhäällä Alaviiva Käytä komentoriviä Valkoinen Keltainen Olet sulkemassa %d välilehteä. Suljetaanko välilehdet? Lähennä L_oitonna Loitonna Palauta suurennus _Lähennä _Palauta suurennus _Tietoja S_ulje välilehti _Muokkaa _Tiedosto _Ohje _Uusi ikkuna _Liitä _Välilehdet konsoli;pääte;terminaali;komentorivi;komentotulkki;suorita;aja;console;command line;execute; Elias Julkunen <elias.julkunen@gmail.com>
Juhani Numminen <juhaninumminen0@gmail.com> x 
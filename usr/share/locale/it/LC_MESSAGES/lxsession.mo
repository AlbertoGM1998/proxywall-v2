��    5      �  G   l      �  (   �     �     �     �     �     �       �   "     �     �  :   �     
       "     	   B     L  '   i  o   �  $     "   &     I  "   b     �     �  
   �  	   �  	   �     �     �     �     �     �  -   �     #     /     4     :  
   B     M     d     k     x  	   �     �     �     �  
   �     �     �     �  
   �     �  p  �  -   P
     ~
     �
     �
     �
     �
     �
  �         �     �  A   �     �       $        5      F  *   g  �   �  0     &   P     w  2   �  	   �     �     �  
   �  
   �     �               )     =  7   J     �  	   �     �     �  
   �     �     �     �     �     �     �           	          #     4  	   =     G  	   U         (   	       -   $      #   '   ,       *          !                            2           3          0                                            "                         
      4   5   &       .                 /   %   1      +   )               <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Applications automatically started after entering desktop: Apply Authentication Automatically Started Applications Autostart Banner to show on the dialog Change the default applications on LXDE Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Custom message to show on the dialog Default applications for LXSession Desktop Session Settings Disable autostarted applications ? Enabled Error Error: %s
 Group: %s Identity: Information L_ock Screen Laptop mode Launching applications Layout Manage applications loaded in desktop session Menu prefix Mode Model Options Password:  Position of the banner Reload S_witch User Settings Sh_utdown Type Variant Window Manager: _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession 0.3.6
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-12-05 13:20+0000
Last-Translator: system user <>
Language-Team: ja_JP <LL@li.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1417785604.000000
 <b><big>Chiudere la sessione %s %s?</big></b> <b>Dbus</b> <b>Ambiente</b> <b>Impostazioni generali</b> <b>Mappa tastiera</b> <b>Applicazioni conosciute</b> <b>Impostazioni</b> <b>Attenzione:</b> <b>NON toccare</b> se non si sa esattamente cosa si sta facendo.

<b>NOTA:</b> questa impostazione avrà effetto al prossimo accesso. Opzioni avanzate Applicazione Applicazioni avviate automaticamente dopo l'ingresso nel desktop: Applica Autenticazione Applicazioni avviate automaticamente Avvio automatico Immagine da mostrare nel dialogo Cambia le applicazioni predefinite in LXDE Riga di comando usata per lanciare il gestore finestre
(il comando predefinito per il gestore finestre di LXDE dovrebbe essere openbox-lxde) Messaggio personalizzato da mostrare nel dialogo Applicazioni predefinite per LXSession Impostazioni sessione desktop Disabilitare applicazioni avviate automaticamente? Abilitato Errore Errore: %s
 Gruppo: %s Identità: Informazione Bl_occa schermo Modalità laptop Lancio applicazioni Disposizione Gestisce le applicazioni avviate nella sessione desktop Prefisso menu Modalità Modello Opzioni Password:  Posizione dell'immagine Ricarica Cambia _utente Impostazioni _Arresta Tipo Variante Gestore finestre: _Iberna _Chiudi sessione _Riavvia _Sospendi FILE_IMMAGINE MESSAGGIO 
��            )   �      �     �  -   �     �  2   �  &        8     ?     L      Q     r     {     �     �     �  /   �     �     �     �     �                .  !   ?  7   a     �     �  8   �     �  ]  �     D  2   S     �  8   �  ,   �                 +        J     S     l     }     �  3   �     �     �  	   �     �     �          .  $   F  <   k     �     �  <   �  	   �                                                                             	                                            
                 Acceleration: Beep when there is an error of keyboard input Character Repeat Configure keyboard, mouse, and other input devices Delay before each key starts repeating Delay: Double-click Fast Interval between each key repeat Keyboard Keyboard Layout... Keyboard and Mouse LXInput autostart Layout: Left handed (Swap left and right mouse buttons) Long Model: Motion Mouse Mouse and Keyboard Settings Repeat delay: Repeat interval: Setting keyboard - please wait... Setup keyboard and mouse using settings done in LXInput Short Slow Type in the following box to test your keyboard settings Variant: Project-Id-Version: lxinput
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-29 12:12+0200
Last-Translator: Emanuele Goldoni <emanuele.goldoni@gmail.com>
Language: it
Language-Team: Italian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.4
 Accelerazione: Emetti un beep per gli errori di input da tastiera Ripetizione Caratteri Configura tastiera, mouse, ed altri dispositivi di input Attesa prima della ripetizione di ogni tasto Ritardo: Doppio clic Veloce Intervallo per la ripetizione di ogni tasto Tastiera Disposizione tastiera... Tastiera e Mouse Avvia automaticamente LXInput Disposizione: Mancino (inverti tasto destro e sinistro del mouse) Lungo Modello: Movimento Mouse Impostazioni mouse e tastiera Attesa ripetizione: Intervallo ripetizione: Impostazione tastiera - attendere... Configura tastiera e mouse usando le impostazioni di LXInput Corto Lento Scrivi in questo box per provare le impostazioni di tastiera Variante: 
��    b      ,  �   <      H     I     R     b  
   o     z     �     �     �     �     �     �     �  
   �     �     �     �  	   �     	     	      	     .	     4	  	   9	     C	     P	     ]	     b	     k	  ?   	  *   �	  %   �	     
  
   
     #
     (
     .
     A
     O
     b
  
   r
     }
     �
     �
     �
     �
     �
     �
     �
  	   �
       	                
   (     3     <     H  	   Q     [     c     r     x     �     �     �     �     �     �  	   �     �     �     �  "        $     2  	   6     @     U     [  B   b     �  	   �     �  
   �     �     �     �     �  
   �     �                               #     )  �  <     �     �                          '     +  
   1     <     I     V     e     r  !   z  !   �     �     �     �     �            
             1     ?     E     R  C   r  <   �  +   �          /     ;     B     H     f     z     �  
   �     �  !   �     �          	     "     9     R     i     z     �     �     �     �     �     �     �  	   �  	             $     -     @     L     _     e  !   l      �     �     �     �  	   �  +   �          )     /     >     U     \  &   c     �     �     �     �     �     �     �     �       	             #     *     :     >     G  �   O     X      ,   S   N      I   P       &       4   V   T   b   
      R   `   C      A      J       8       5   -      \          H   O   _   Z   .   ^       Q   6   +      B                             <          0                    /   @          7       #          G      M      >       Y      a       *   '       1               ;   %   U   E      )   F                      ]   	      "          D      ?           :       =   2   [      W   L          !              3   (      K   $   9    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences LXTerminal Terminal Emulator Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Open _URL Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs translator-credits Project-Id-Version: lxterminal 0.1.4
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-29 13:51+0200
Last-Translator: Emanuele Goldoni <emanuele.goldoni@gmail.com>
Language-Team: Italian
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.4
X-Poedit-SourceCharset: UTF-8
X-POOTLE-MTIME: 1407486790.000000
 Avanzate Permetti font grassetto Avviso acustico Sfondo Nero Blocco Blu Sotto Blu chiaro Ciano chiaro Verde chiaro Magenta chiaro Rosso chiaro Marrone Pu_lisci scorrimento all'indietro Pulisci sc_orrimento all'indietro _Chiudi scheda C_hiudi finestra C_hiudi finestra Conferma chiusura _Copia _Copia Co_pia URL Lampeggiamento cursore Stile cursore Ciano Grigio scuro Dimensione predefinita finestra Disabilita conferma per la chiusura di una finestra con più schede Disabilita il tasto scorciatoia per i menu
(predefinito F10) Disabilita l'uso di Alt-N per schede e menu Visualizzazione Primo piano Grigio Verde Nascondi pulsanti di chiusura Nascondi barra menu Nascondi puntatore del mouse Nascondi barra scorrimento LXTerminal Preferenze di LXTerminal Emulatore di terminale LXTerminal Sinistra Magenta Sposta scheda a sinistra Sposta scheda a destra Sposta scheda a sinistra Sposta scheda a destra Rino_mina scheda Rinomina scheda Scheda _successiva Nuova _scheda Nuova _scheda _Nuova finestra Nuova _scheda _Nuova finestra Scheda _successiva Apri _URL Tavolozza Tavolozza predefinita _Incolla Scheda _precedente Preferen_ze Scheda _precedente Rosso Destra Righe di scorrimento all'indietro Caratteri per selezionare parole Scorciatoie Stile Posizione pannello schede Terminale Emulatore di terminale per il progetto LXDE Carattere del terminale Sopra Trattino basso Usa la riga di comando Bianco Giallo Verranno chiuse %d schede. Proseguire? Aumenta Zoom _Diminuisci Zoom Diminuisci Zoom Dimensione normale _Aumenta Zoom Dimensione _normale I_nformazioni _Annulla _Chiudi scheda _Modifica _File A_iuto _Nuova finestra _OK _Incolla _Schede Emanuele Goldoni <emanuele.goldoni@gmail.com>
Daniele Forsi <daniele@forsi.it>
Andrea Florio <andrea@opensuse.org>
Alessandro Pellizzari <alex@amiran.it>
Fabio Barone <phonky@gmx.net> 
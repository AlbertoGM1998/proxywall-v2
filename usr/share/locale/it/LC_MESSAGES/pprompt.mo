��          ,      <       P   �   Q   V    �   m                     SSH is enabled and the default password for the 'pi' user has not been changed.\n\nThis is a security risk - please login as the 'pi' user and run Raspberry Pi Configuration to set a new password. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-26 15:31+0200
Last-Translator: Emanuele Goldoni <emanuele.goldoni@gmail.com>
Language-Team: Italian
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.4
 SSH è abilitato e la password predefinita per l'utente 'pi' non è stata cambiata.\n\nQuesto è un rischio per la sicurezza - entra come utente 'pi' e avvia Raspberry Pi Configuration per impostare una nuova password. 
��    
      l      �       �      �                 +     L     b     z     �     �  N  �               ,  *   K  "   v     �     �     �     �                            	   
              Background colour CPU Temperature CPU Temperature Monitor Colour when ARM frequency capped Colour when throttled Display CPU temperature Foreground colour Lower temperature bound Upper temperature bound Project-Id-Version: lxplug_cputemp 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-25 23:36+0200
Last-Translator: Emanuele Goldoni <emanuele.goldoni@gmail.com>
Language-Team: Italian
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Colore di sfondo Temperatura CPU Visualizzatore Temperatura CPU Colore quando la frequenza ARM è limitata Colore quando la CPU è rallentata Mostra la temperatura della CPU Colore in primo piano Limite inferiore di temperatura Limite superiore di temperatura 
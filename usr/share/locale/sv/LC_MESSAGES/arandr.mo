��    &      L  5   |      P     Q     V     Z  2   ^     �     �     �     �  (   �     �  :       >  S   D     �     �  ,   �     �     �  
             "     )  0   ;  )   l     �  ^   �               %     -     6     >     D     M     b  	   }     �  �  �     f	     k	     o	  A   s	     �	     �	     �	     �	  8   �	  .   *
  {  Y
     �  S   �  "   /     R  /   e     �     �     �     �     �     �  +   �  -        :  _   R     �     �     �  
   �     �     �     �          "     =     L                          
   $         %         	                                !          "                                                        #         &                        1:16 1:4 1:8 A part of an output is outside the virtual screen. ARandR Screen Layout Editor Accelerator Action Active An output is outside the virtual screen. Another XRandR GUI Click on a button in the left column and press a key combination you want to bind to a certain screen layout. (Use backspace to clear accelerators, escape to abort editing.) Then, select one or more layouts in the right column.

This will only work if you use metacity or another program reading its configuration. Dummy In order to configure metacity, you need to have the python gconf module installed. Keybindings (via Metacity) New accelerator... No files in %(folder)r. Save a layout first. Open Layout Orientation Resolution Save Layout Script Script Properties Setting this resolution is not possible here: %s This orientation is not possible here: %s XRandR failed:
%s Your configuration does not include an active monitor. Do you want to apply the configuration? _Help _Keybindings (Metacity) _Layout _Outputs _System _View disabled gconf not available. incompatible configuration no action other application Project-Id-Version: arandr
Report-Msgid-Bugs-To: chrysn@fsfe.org
PO-Revision-Date: 2013-02-28 13:45+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Swedish <http://hosted.weblate.org/projects/arandr/translations/sv/>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 1.5-dev
X-Launchpad-Export-Date: 2012-03-23 05:10+0000
 1:16 1:4 1:8 En del av utmatningen ligger utanför den virtuella bildskärmen. ARandR Skärm Layout Redigerare Snabbtangent Åtgärd Aktiv En utmatning ligger utanför den virtuella bildskärmen. Another XRandR GUI (Ytterligare en XrandR GUI) Klicka på en knapp i den vänstra kolumnen och trycker på den tangentkombination du vill binda till en viss skärmlayout. (Använd backspace för att ta bort snabbtangent, escape för att avbryta redigeringen.) Välj sedan en eller flera layouter i den högra kolumnen.

Detta fungerar bara om du använder metacity eller något annat program för att läsa dess konfiguration. Dummy För att kunna konfigurera metacity, måste du ha python gconf modulen installerad. Tangentbindningar (genom Metacity) Ny snabbtangent... Ingen fil i %(folder)r. Spara en layout först. Öppna Layout Orientering Upplösning Spara Layout Skript Skriptegenskaper Denna upplösning är inte möjlig här: %s Denna orienteringen är inte möjlig här: %s XRandR misslyckades:
%s Din konfiguration inkluderar inte någon aktiv bildskärm. Vill du verkställa konfigurationen? _Hjälp _Tangentbindningar (Metacity) _Layout _Utgångar _System V_isa inaktiverad gconf är inte tillgänglig. inkompatibel konfiguration ingen åtgärd annat program 
��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u  	         
          (     1     7     =     B     J  	   S  	   ]     g     s     |     �     �     �     �     �     �     �     �     �          "     /     ;  	   @     J  8   h  ;   �  5   �       	        %     *     2     E     T     c  
   p     {     �     �     �     �     �     �     �     
          #     ,     4     B     K     Z     f     m  
   �     �     �     �     �     �     �     �  	                  4  $   =     b     s     {     �     �     �  P   �     �  	               	   +     5     L     P     X  	   e     o     v     ~     �     �     �     �  �   �     O                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-06-26 09:35+0000
Last-Translator: Martin Bagge <brother@bsnet.se>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1498469749.828623
 Avancerad Tillåt fetstilt Terminalljud Bakgrund Svart Block Blå Nederst Ljusblå Ljus cyan Ljusgrön Ljusmagenta Ljusröd Brun Töm _skärmhistorik Töm skärmhist_orik Stäng flik Stäng fönster Stäng _Fönster Bekräfta stängning _Kopiera Kopiera Kopiera _URL Copyright © 2008-2017 Markörblink Markörstil Cyan Mörkgrå Standardstorlek för fönster Varna inte när ett fönster med flera flikar är öppna Avaktivera tangentbordskomando för meny (F10 som standard) Avaktivera användningen av Alt-n förflikar och meny Visning Förgrund Grå Grekisk Göm stängknappar Göm menyfält Göm muspekare Göm rullist LXTerminal Inställningar för LXTerminal Vänster Magenta Flytta flik vänster Flytta flik höger Flytta flik _vänster Flytta flik _höger Namnge _flik Namnge flik _Nästa flik Ny _flik Ny flik Nytt fönster Ny _flik _Nytt fönster Nästa flik Palett Förinställning för palett Klistra in _Föregående flik _Inställningar Föregående flik Röd Höger Rader i rullningslist Tecken för välj-efter-ord Genvägar Stil Panelposition för flikfält Terminal Terminalemulator för LXDE-projektet Terminaltypsnitt Överst Understrykning Använd kommandoraden Vit Gul Du är på väg att stänga %d flikar. Är du säker på att du vill fortsätta? Zooma in Zooma _ut Zooma ut Återställ zoomnivå Zooma _in Åte_rställ zoomnivå _Om _Avbryt S_täng flik _Redigera _Arkiv _Hjälp _Nytt fönster _OK Klistra _in _Flikar console;command line;execute; Daniel Nylander <po@danielnylander.se>
Martin Bagge <brother@bsnet.se>

Skicka synpunkter på översättningen till
<tp-sv@listor.tp-sv.se> x 
��    �      T  �   �
        (   	     2     >     Q     i     w  &   �     �  �   �     Y     ^     o  @   {  0   �  0   �          <  ?   U     �  2   �  ,   �       #   1  /   U     �  3   �     �  :   �     +     1     9     K     Y     f  &   u  "   �  	   �     �     �       R        l  	   �     �  
   �     �     �  '   �     �     �       o   "     �     �     �  �   �     K  $   ]  "   �     �     �  �   �     R  "   Z     }  3   �     �  a   �     &     /     5     =  
   C  3   N     �  w   �  	     :        L  	   i     s     �     �     �     �     �     �     �     �     �  -   	  A   7     y  |   �       Z   (  -   �  :   �  !   �  %     j   4     �     �     �  L   �               #     (     4     B  
   J     U  �   [  
   �     �     	  �        �     �  �   �     �     �     �     �     �  M   �     	          /  8   N     �  V   �     �  	                  +   '      S      d   %   p      �      �      �   >   �   D   !  u   I!  5   �!     �!     �!     "  2   "  
   O"     Z"     a"     j"     z"  �   �"     ##     5#  
   <#     G#     O#     W#  
   `#     k#  �  s#  +   �$     $%     0%     >%     ^%     q%  $   �%     �%  �   �%     a&     f&     |&  9   �&  9   �&  .   �&     ''     D'  E   c'     �'  <   �'  %   (     ,(  %   L(  A   r(  #   �(  D   �(     )  N   <)  
   �)     �)  
   �)     �)     �)     �)  +   �)     �)     *     .*      E*     f*  >   |*      �*  	   �*     �*     +     +     '+      .+     O+  ,   [+     �+  ~   �+     ,     ),     9,  �   O,     �,  *   -     0-     O-  %   c-  q   �-  
   �-  *   .     1.  1   =.     o.  n   u.     �.     �.  	   �.     �.     /  8   /     D/  �   Q/  	   0  8   0     D0  
   a0  
   l0     w0     �0     �0     �0     �0     �0     �0     �0  #   �0  3   #1  D   W1     �1  �   �1     22  H   @2  .   �2  =   �2     �2  )   3  z   ;3     �3     �3     �3  @   �3     -4     34     :4     >4     Z4  
   g4     r4     ~4  �   �4     5  "   5     >5  �   J5     6     36  �   C6     �6     7     7     %7     87  Z   H7  !   �7     �7     �7  R   �7      G8  ^   h8     �8  
   �8  
   �8     �8  +    9     ,9     >9  &   M9     t9     x9     �9  V   �9  Y   :  q   \:  >   �:     ;     ;     ";  U   /;     �;  
   �;     �;     �;     �;  �   �;     s<     �<  
   �<  	   �<  
   �<     �<     �<  
   �<             D       y   /       U   b             -   7   �   z   W   l      A             ]   !   S   �   Z   a   n       5   �   #   w   �      {   8   �   Q           c   F             I   �   o       �           H          �   N   X   �   O          [   '   J   s                   +   E           r          *   k   m   =   u      ~   �         G   �       T   ?      B   �   \   �          �         p   v      �   �           9   d       C   �   _   0   �      .       �           (   �           ;          �       P   3   �       Y      �   	      V      �       i                   "           f   ^      )          �   L   �   �       1   >       e           6           |   �   }               R   �   ,   $   :   &   j       t   �   �   �   M       �   �   h   �   q       �   g       
   �   K   2      `   %          �   4   �   <      @   �   x    <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. A11y Advanced Options Application Application for taking screeshot of your desktop, like scrot ... Application to create archives, like file-roller Application to create spreedsheet, like gnumeric Application to display images Application to edit text Application to go to Internet, Google, Facebook, debian.org ... Application to lock your screen Application to manage bittorent, like transmission Application to manage burning CD/DVD utilty  Application to manage disks Application to manage notes utility Application to manage office text, like abiword Application to manage webcam Application to monitor tasks running on your system Application to send mails Applications automatically started after entering desktop: Apply Archive Audio application Audio manager Audio player Authentication Authentication failed!
Wrong password? Automatically Started Applications Autostart Autostart the application ? Autostart the application ?
 Available applications Available applications : Applications of this type available on your repositories
 Banner to show on the dialog Bittorent Burning utility Calculator Calculator application Cancel Change the default applications on LXDE Charmap Charmap application Clipboard manager Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Communication 1 Communication 2 Composite Manager Composite manager enables graphics effects, like transpacency and shadows, if the windows manager doesn't handle it. 
Example: compton Core applications Custom message to show on the dialog Default applications for LXSession Desktop Manager Desktop Session Settings Desktop manager draws the desktop and manages the icons inside it.
You can manage it with the file manager by setting "filemanager" Disable Disable autostarted applications ? Disks utility Do you want to assiociate the following Mimetype ?
 Dock Dock is a second panel. It's used to launch a different program to handle a second type of panel. Document Email Enabled Error Error: %s
 Extra: Add an extra parameter to the launch option
 File Manager File manager is the component which open the files.
See "More" to add options to handle the desktop, or openning files  Group: %s Handle Desktop: Draw the desktop using the file manager ?
 Handle the desktop with it ? Identity: Image viewer Information LXPolKit LXSession configuration L_ock Screen Laptop mode Launcher manager Launching applications Layout Lock screen manager Manage applications loaded in desktop session Managing XRandr parameters. Use a command like xrandr --something Managing clipboard support Managing keyring support.
Standard options available "gnome" for gnome-keyring support  or "ssh-agent" for ssh-agent support Managing proxy support Managing support for accessibility.
Stardart option are gnome, for stardart gnome support. Managing the application to quit your session Managing the application to update and upgrade your system Managing your audio configuration Managing your workspace configuration Manual Settings: Manually sets the command (you need to restart lxsession-default-apps to see the change)
 Manual setting Menu prefix Mime Association Mime association: Automatically associates mime types to this application ?
 Mode Model More Network GUI Notes utility Options PDF Reader Panel Panel is the component usually at the bottom of the screen which manages a list of opened windows, shortcuts, notification area ... Password:  Policykit Authentication Agent Polkit agent Polkit agent provides authorisations to use some actions, like suspend, hibernate, using Consolekit ... It's not advised to make it blank. Position of the banner Power Manager Power Manager helps you to reduce the usage of batteries. You probably don't need one if you have a desktop computer.
Auto option will set it automatically, depending of the laptop mode option. Proxy Quit manager Reload S_witch User Screensaver Screensaver is a program which displays animations when your computer is idle Screenshot manager Security (keyring) Session : specify the session
 Set an utility to manager connections, such as nm-applet Set debian default programs Set default program for Debian system (using update-alternatives, need root password)
 Settings Sh_utdown Spreadsheet Tasks monitor Terminal by default to launch command line. Terminal manager Text editor The database is updating, please wait Type Update lxsession database Upgrade manager Use a communication software (an IRC client, an IM client ...) Use another communication software (an IRC client, an IM client ...) Utility to launch application, like synapse, kupfer ... 
For using lxpanel or lxde default utility, use "lxpanelctl"  Utility to launch gadgets, like conky, screenlets ... Variant Video application Video player Wallpaper: Set an image path to draw the wallpaper Webbrowser Webcam Widget 1 Window Manager: Windows Manager Windows manager draws and manage the windows. 
You can choose openbox, openbox-custom (for a custom openbox configuration, see "More"), kwin, compiz ... Workspace manager Xrandr _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-08-04 12:47+0000
Last-Translator: Martin Bagge <brother@bsnet.se>
Language-Team: LANGUAGE <LL@li.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1501850852.015224
 <b><big>Logga ut %s %s-sessionen?</big></b> <b>Dbus</b> <b>Miljö</b> <b>Allmänna inställningar</b> <b>Teckenkarta</b> <b>Kända program</b> <b>Manuellt autostartade program</b> <b>Inställningar</b> <b>Varning:</b> Ändra <b>INTE</b> detta om du inte är helt säker på vad du sysslar med.

<b>OBS!</b> Detta alternativ aktiveras vid din nästa inloggning. A11y Avancerade alternativ Program Program för att göra skärmavbildningar, t.ex. scrot... Program för att skapa arkivfiler, exempelvis file-roller Program för att skapa kalkylblad som gnumeric Program för att visa bilder Program för att redigera text Program för att ansluta till webben, Google, Facebook, debian.org... Program för att låsa systemet Program för att hantera bittorrent, exempelvis transmission Program för att bränna CD eller DVD Program för att hantera diskar Program för att hantera anteckningar Program för att hantera större textdokument, exempelvis abiword Program för att hantera webbkamera Program för att övervaka aktuella tjänster som körs på systemet Program för att skicka e-post Program som automatiskt ska starta efter att skrivbordet blivit tillgängligt: Verkställ Arkiv Ljudrogram Ljudhantering Ljudspelare Autentisering Autentisiering misslyckades!
Fel lösenord? Program som startas automatiskt Starta automatiskt Autostarta programmet? Automatisk uppstart av program?
 Tillgängliga program Tillgängliga program: Program av denna typen i dina förråd
 Banderoll att visa i dialogrutan Bittorent Verktyg för att bränna skivor Miniräknare Miniräknar-program Avbryt Ändra standardprogram för LXDE Teckenkarta Program för att visa teckenkartan (charmap) Urklippshanterare Kommandorad för att start fönsterhanterare
(openbox-lxde bör standardkommando för att starta fönsterhanteraren för LXDE) Communication 1 Communication 2 Kompositionshanterare För att få grafiska effekter som genomskinlighet och skuggor behövs en kompositionshanterare om fönsterhanteraren inte själv hanterar detta.
Exempel: compton Huvudapplikationer Anpassat meddelande att visa i dialogrutan Standardprogram för LXSession Skrivbordshanterare Inställningar för skrivbordssession Skrivbordshanteraren ritar upp skrivbordet och hanterar ikonerna på det.
Filhanteraren kan användas för detta. Inaktivera Inaktivera automatisk uppstart av program? Diskverktyg Vill du ange bindning för följande mime-typer?
 Docka Dock är en annan panel. Den används för att starta ett annat program som hanterar denna andra typ av panel. Dokument E-post Aktiverad Fel Fel: %s
 Extra: Lägg till en extra flagga till start-alternativ
 Filhanterare Filehanteraren används för att granska filsystemet och öppna filer.
Läs vidare under "Mer" om att lägga till alternativ för att hantera skrivbordet och att öppna filer. Grupp: %s Hantera skrivbord: Låt filhanteraren visa skrivbordet?
 Hantera skrivbordet med det? Identitet: Bildvisare Information LXPolKit Inställningar för LXSession L_ås skärmen Laptop-läge Hanterare för start av program Startar program Utseende Hanterare för låsning av skärmen Hantera program som läses in i skrivbordssessionen Hantera XRandr-parametrar. Använd ett kommando likt xrandr --något Urklippshantering Hantera stöd för nyckelring
Standardalternativet är "gnome" för gnome-keyring eller "ssh-agent" för att använda en SSH Agent. Hantera proxy Hantera hjälpmedel
Standardalternativet är "gnome" för Gnome-stödet. Hantera programmet som stänger av din session Hantera programmet som uppdaterar och uppgraderar ditt system Hantera ljudinställningar Hantera inställningar för din arbetsyta Manuell inställning: Ange kommandot manuellt (du behöver starta om lxsession-default-apps för att detta ska aktiveras)
 Manuell inställning Prefix för meny Mime-bindningar Mime-hantering: Automatisk koppling av Mime för detta program?
 Läge Modell Mer GUI för nätverkshantering Anteckningar Alternativ PDF-läsare Panel Panelen är vanligen komponenten i nederkant av skärmen som visar en lista med öppna fönster, genvägar och ett utrymme för notiser... Lösenord:  Autentiseringsagent för Policykit Polkitagent Polkit-agenten används för att tilåta att vissa händelser kan styras av vanliga användare. Exempelvis att sätta datorn i vänteläge eller viloläge genom Consolekit. DU bör inte radera detta värde. Position för banderollen Energihantering Strömhanteraren hjälper dig minska användandet av batterier. Detta behövs vanligen endast på bärbara datorer.
Automatiska inställningar kommer att aktivera detta om det behövs. Proxy Hanterare för avslutning Ladda om V_äxla användare Skärmsläckare Skärmsläckare är ett program som hanterar animationer när datorn inte aktivt används. Hanterare för skärmavbildningar Säkerhet (nyckelring) Session : ange session
 Ange ett verktyg som håller reda på nätverksanslutningar, exempelvis nm-applet. Ange standardprogram för Debian Ange standardprogram för Debian-systemet (genom update-alternatives, kräver root-lösenord)
 Inställningar Stäng _av Kalkylblad Aktivitetshanterare Terminalprogram för att köra kommandorad. Terminalhanterare Textredigerare Databasen uppdateras, vänligen vänta Typ Uppdatera lxsession-databas Uppgraderingshanterare Använd ett kommunikationsverktyg (en IRC-klient, en klient för direktmeddelanden...) Använd annan kommunikationsmjukvara (en IRC-klient, en klient för direktmeddelanden...) Verktyg för att starta program, t.ex. synapse, kupfer...
För att använda LXDEs standardverktyg ange lxpanelctl Verktyg för att starta hjälpprogram som conky, screenlets... Variant Videoprogram Videospelare Bakgrundsbild: Ange sökvägen till en bildfil för att sätta den som bakgrundsbild. Webbläsare Webbkamera Widget 1 Fönsterhanterare: Fönsterhanterare Fönsterhanterare ritar upp och hanterar fönster. 
Du kan välja openbox, openbox-custom (för anpassad openbox-inställning, läs vidare under "Mer"), kwin, compiz... Hanterare för arbetsytor Xrandr _Viloläge _Logga ut Starta _om Vä_nteläge bildfil meddelande 
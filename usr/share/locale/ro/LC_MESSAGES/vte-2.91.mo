��          L      |       �   /   �      �   =   �   +   5     a     i  \   �  3   �  @     -   \  
   �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-06-17 21:39+0300
Last-Translator: Florentina Mușat <florentina.musat.28 [at] gmail [dot] com>
Language-Team: Gnome Romanian Translation Team
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Poedit 2.0.8
X-Launchpad-Export-Date: 2009-01-14 02:40+0000
X-Project-Style: gnome
 Eroare (%s) în timpul convertirii datelor pentru procesul copil, se abandonează acțiunea. Eroare la citirea datelor de la procesul copil: %s. GnuTLS nu este activat; datele vor fi scrise pe disc necriptate! Nu se pot converti caracterele din %s în %s. AVERTIZARE 
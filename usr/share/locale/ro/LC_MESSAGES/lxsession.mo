��          �   %   �      `  �   a     �       :        I  "   X     {  o   �  $        -     F  
   N  	   Y  	   c  -   m  
   �     �     �  	   �     �  
   �     �     �     �  
          �    �   �     `     r  :   z     �     �      �  l   �  ,   j  $   �     �     �     �     �  ;   �  	   "	     ,	     @	     P	     X	  
   m	     x	  
   �	     �	     �	     �	        
                                                                                                 	                   <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Applications automatically started after entering desktop: Authentication Automatically Started Applications Banner to show on the dialog Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Custom message to show on the dialog Desktop Session Settings Enabled Error: %s
 Group: %s Identity: Manage applications loaded in desktop session Password:  Position of the banner S_witch User Sh_utdown Window Manager: _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: LXDE
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-12-05 13:30+0000
Last-Translator: system user <>
Language-Team: Romanian <LL@li.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1417786258.000000
 <b>Atenție:</b> <b>NU</b> schimbați această opțiune în necunoștință de cauză.

<b>NOTĂ:</b> Această opțiune va produce efecte după următoarea autentificare. Opțiuni avansate Program Programe pornite automat după inițializarea desktopului: Autentificare Programe pornite automat Banner de afișat utilizatorului Comandă de pornire a managerului de ferestre
(Managerul de ferestre implicit pentru LXDE este openbox-lxde) Mesaj personalizat de afișat utilizatorului Opțiuni pentru sesiunile de desktop Activare Eroare: %s
 Grup: %s Identitate: Alegeți ce programe să pornească în sesiunea de desktop Parolă:  Poziția bannerului Alt _utilizator _Oprire Manager de ferestre: _Hibernare _Ieșire _Repornire _Suspendare fișier imagine mesaj 
��          L      |       �   /   �      �   =   �   +   5     a  �  i  �     Q   �  �   �  L   �     �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: el
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-08-15 14:13+0300
Last-Translator: Efstathios Iosifidis <iosifidis@opensuse.org>
Language-Team: team@gnome.gr
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.9
X-Project-Style: gnome
 Σφάλμα (%s) μετατροπής δεδομένων από θυγατρική διεργασία, απορρίπτεται. Σφάλμα ανάγνωσης από θυγατρική διεργασία: %s. Δεν είναι ενεργοποιημένο το GNUTLS, τα δεδομένα που θα εγγραφούν στο δίσκο δεν θα είναι κρυπτογραφημένα! Αδύνατη η μετατροπή χαρακτήρων από %s σε %s. ΠΡΟΕΙΔΟΠΟΙΗΣΗ 
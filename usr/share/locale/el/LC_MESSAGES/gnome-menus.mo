��          �   %   �      p     q     }  %   �  1   �     �  	   �                     $     :     C     S     Z     n     t  2   �     �     �     �     �  #   �          5     F  	   `     j  �  {     L     _  i   r  ^   �  8   ;     t     �  .   �     �  #   �     �          /  #   >     b     k  �   �  9   !	     [	     x	  %   �	  9   �	  =   �	     %
  4   C
     x
     �
                                    	                              
                                                         Accessories Applications Applications and sites saved from Web Applications that did not fit in other categories Desktop accessories Education Games Games and amusements Graphics Graphics applications Internet Multimedia menu Office Office Applications Other Programming Programs for Internet access such as web and email Small but useful GNOME tools Sound & Video Sundry System Tools System configuration and monitoring Tools for software development Universal Access Universal Access Settings Utilities Web Applications Project-Id-Version: el
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-menus&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-07-07 13:07+0200
Last-Translator: Tom Tryfonidis <tomtryf@gmail.com>
Language-Team: team@lists.gnome.gr
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.5
X-Project-Style: gnome
 Βοηθήματα Εφαρμογές Εφαρμογές και ιστοσελίδες που αποθηκεύτηκαν από τον Ιστό Εφαρμογές που δεν ταιριάζουν στις άλλες κατηγορίες Βοηθήματα επιφάνειας εργασίας Εκπαίδευση Παιχνίδια Παιχνίδια και διασκέδαση Γραφικά Εφαρμογές γραφικών Διαδίκτυο Μενού πολυμέσων Γραφείο Εφαρμογές γραφείου Άλλα Προγραμματισμός Προγράμματα για πρόσβαση στο διαδίκτυο όπως φυλλομετρητής και ηλεκ. αλληλογραφία Μικρά αλλά χρήσιμα εργαλεία GNOME Ήχος και βίντεο Διάφορα Εργαλεία συστήματος Ρύθμιση και έλεγχος συστήματος Εργαλεία για ανάπτυξη λογισμικού Γενική πρόσβαση Ρυθμίσεις γενικής πρόσβασης Εργαλεία Εφαρμογές ιστού 
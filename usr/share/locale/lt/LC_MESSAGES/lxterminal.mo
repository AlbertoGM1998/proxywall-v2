��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u     ;     J     d     v     |     �     �  	   �     �     �     �     �     �     �     �          2     D     T     e  
   }  	   �     �     �     �     �     �     �     �  B     :   Y  .   �     �     �     �     �     �               3  
   H     S     h  
   q     |     �     �     �     �     	     '     6     F     U     c     s     �     �     �     �     �  
   �     �     �  
   �       $        <     P     X  
   q  $   |     �     �  	   �     �     �     �  <   �     +  	   3     =     F     Y     b     v     |     �     �     �  	   �     �     �     �  
   �      �  g   �     g                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-23 09:27+0000
Last-Translator: Moo <hazap@hotmail.com>
Language-Team: Lietuvių <>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Pootle 2.8
X-Language: lt_LT
X-POOTLE-MTIME: 1495531634.233375
 Išplėstiniai Leisti pusjuodį šriftą Garsinis signalas Fonas Juoda Blokas Mėlyna Apačioje Ryškiai mėlyna Ryškiai žydra Ryškiai žalia Ryškiai purpurinė Ryškiai raudona Ruda _Išvalyti slinkimo istoriją Išvalyti _slinkimo istoriją Užverti kortelę Užverti langą Užverti _langą Patvirtinti užvėrimą _Kopijuoti Kopijuoti Kopijuoti _URL Autorių teisės (C) 2008-2017 Žymeklio mirksėjimas Žymeklio stilius Žydra Tamsiai pilka Numatytasis lango dydis Išjungti patvirtinimą, užveriant langą su keliomis kortelėmis Išjungti spartųjį meniu klavišą (pagal numatymą F10) Išjungti Alt-n naudojimą kortelėms ir meniu Rodymas Priekinis planas Pilka Žalia Slėpti užvėrimo mygtukus Slėpti meniu juostą Slėpti pelės rodyklę Slėpti slankjuostę LXTerminal LXTerminal nuostatos Kairėje Purpurinė Perkelti kortelę kairėn Perkelti kortelę dešinėn Perkelti kortelę _kairėn Perkelti kortelę _dešinėn S_uteikti kortelei pavadinimą Suteikti kortelei pavadinimą _Kita kortelė Nauja k_ortelė Nauja kortelė Naujas langas Nauja kor_telė Naujas _langas Kita kortelė Paletė Išankstinė paletės parinktis Įdėti _Ankstesnė kortelė Nuostato_s Ankstesnė kortelė Raudona Dešinėje Slinkti eilutes „Pasirinkti po žodį“ simboliai Spartieji klavišai Stilius Kortelių skydelio vieta Terminalas LXDE projekto terminalo emuliatorius Terminalo šriftas Viršuje Pabraukti Naudoti komandų eilutę Balta Geltona Jūs ketinate užverti %d korteles. Ar tikrai norite tęsti? Didinti _Mažinti Mažinti Atstatyti mastelį D_idinti Atstat_yti mastelį _Apie _Atsisakyti _Užverti kortelę _Taisa _Failas Ž_inynas _Naujas langas _Gerai Į_dėti Kor_telės pultas;komandų eilutė;vykdyti; Julius Vitkauskas <zadintuvas@gmail.com>
Algimantas Margevičius <margevicius.algimantas@gmail.com>
Moo x 
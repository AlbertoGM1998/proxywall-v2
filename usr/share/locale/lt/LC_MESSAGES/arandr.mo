��    &      L  5   |      P     Q     V     Z  2   ^     �     �     �     �  (   �     �  :       >  S   D     �     �  ,   �     �     �  
             "     )  0   ;  )   l     �  ^   �               %     -     6     >     D     M     b  	   }     �  ~  �     	     	     !	  +   %	  '   Q	     y	     �	     �	  $   �	  *   �	  �  �	     �  O   �  "   �       E   8     ~     �     �     �  
   �     �  +   �  !   
     ,  _   @     �      �     �  
   �     �     �  	   �     �          .     <                          
   $         %         	                                !          "                                                        #         &                        1:16 1:4 1:8 A part of an output is outside the virtual screen. ARandR Screen Layout Editor Accelerator Action Active An output is outside the virtual screen. Another XRandR GUI Click on a button in the left column and press a key combination you want to bind to a certain screen layout. (Use backspace to clear accelerators, escape to abort editing.) Then, select one or more layouts in the right column.

This will only work if you use metacity or another program reading its configuration. Dummy In order to configure metacity, you need to have the python gconf module installed. Keybindings (via Metacity) New accelerator... No files in %(folder)r. Save a layout first. Open Layout Orientation Resolution Save Layout Script Script Properties Setting this resolution is not possible here: %s This orientation is not possible here: %s XRandR failed:
%s Your configuration does not include an active monitor. Do you want to apply the configuration? _Help _Keybindings (Metacity) _Layout _Outputs _System _View disabled gconf not available. incompatible configuration no action other application Project-Id-Version: ARandR
Report-Msgid-Bugs-To: chrysn@fsfe.org
PO-Revision-Date: 2012-11-19 07:36+0000
Last-Translator: Mantas Kriaučiūnas <mantas@akl.lt>
Language-Team: LANGUAGE <LL@li.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2012-11-20 05:13+0000
X-Generator: Launchpad (build 16286)
 1:16 1:4 1:8 Dalis išvesties yra už virtualaus ekrano. ARandR ekranų išdėstymų redaktorius Spartusis klavišas Veiksmas Aktyvus Išvestis yra už virtualaus ekrano. Another XRandR GUI (dar vienas XRandR GUI) Spustelėkite ant mygtuko dešiniame stulpelyje ir paspauskite klavišų kombinaciją kurią norėsite naudoti tam ekranų išdėstymui. (Sparčiojo klavišo išvalymui naudokite „Backspace“ klavišą, o redagavimo nutraukimui - „Escape“.) Tada pasirinkite vieną ar kelis išdėstymus, iš dešinio stulpelio.

Tai veiks tik tada jei naudojate „metacity“ ar kitą langų tvarkyklę skaitančią tą konfigūraciją. Butaforinis Norint konfigūruoti „metacity“, turi būti įdiegtas python gconf modulis. Klavišų susiejimai (su Metacity) Naujas spartusis klavišas... Aplanke %(folder)r failų nėra. Pirmiausia įrašykite išdėstymą. Atverti išdėstymą Orientacija Raiška Įrašyti išdėstymą Scenarijus Scenarijaus savybės Nustatyti šią raišką čia, negalima: %s Čia ši orientacija negalima: %s XRandR nepavyko:
%s Jūsų konfigūracijoje nėra aktyvaus monitoriaus. Ar norite pritaikyti šią konfigūraciją? _Pagalba _Klavišų susiejimai (Metacity) _Išdėstymas _Išvestys _Sistema R_odymas išjungta gconf neprieinama. nesuderinama konfigūracija nėra veiksmo kita programa 
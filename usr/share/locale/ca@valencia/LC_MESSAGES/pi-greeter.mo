��          �      �       0  E   1     w     ~     �  	   �     �  $   �  
   �     �     �  
          �    H   �  
   �     �          0     8  1   E     w  	   �     �     �     �                                               	       
    Are you sure you want to close all programs and restart the computer? Cancel Failed to authenticate Failed to start session Hibernate High Contrast Incorrect password, please try again Large Font Other... Restart Restart... Suspend Project-Id-Version: lightdm
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-05-08 06:42+0000
Last-Translator: David Planella <david.planella@ubuntu.com>
Language-Team: Catalan (Valencian) <ca@valencia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-05-01 06:31+0000
X-Generator: Launchpad (build 16985)
Language: 
 Esteu segur que voleu tancar tots els programes i reiniciar l'ordinador? Cancel·la Ha fallat l'autenticació Ha fallat l'inici de la sessió Hiberna Contrast alt La contrasenya és incorrecta, torneu a provar-ho Mida de lletra gran Altres... Reinicia Reinicia... Para temporalment 
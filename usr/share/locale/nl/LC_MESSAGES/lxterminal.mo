��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  p  u  	   �     �               "     (     1     7     @     L     \     h  
   y     �     �     �     �     �     �     �  
   �  	         
     #     >     W  	   k     u     �  G   �  .   �  8        N  	   U     _     e     k     �     �     �  
   �     �     �  
   �     �          &     D     c     q     ~     �     �     �     �     �     �     �     �                    +     9     >     E     Z     z     �     �     �  %   �     �     �     �                  Q   "  	   t     ~  
   �  *   �  
   �  *   �     �  	   �       	        !     *     0     >     B  
   K  !   V  0   x     �                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-22 17:25+0000
Last-Translator: Pjotr <pjotrvertaalt@gmail.com>
Language-Team: Dutch
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495473929.000386
 Gevorderd Vet lettertype toestaan Hoorbare klok Achtergrond Zwart Blokkeer Blauw Onderaan Helderblauw Helderturquoise Heldergroen Helderscharlaken Helderrood Bruin Terugschuiven wissen Terugschuiven wissen Tabblad sluiten Venster sluiten Venster sluiten Bevestig sluiten Kop_iëren Kopiëren Webadres (URL) kopiëren Auteursrecht (C) 2008-2017 Knippering van aanwijzer Stijl van aanwijzer Turquoise Donkergrijs Standaardgrootte van venster Schakel bevestiging uit voor sluiten van venster met meerdere tabbladen Menu-sneltoets uitschakelen (standaard is F10) Schakel het gebruik van Alt-n uit voor tabbladen en menu Scherm Voorgrond Grijs Groen Verberg sluitknoppen Verberg menubalk Verberg muispijl Verberg bladerbalk LXTerminal Voorkeuren van LXTerminal Links Scharlaken Verplaats tabblad naar links Verplaats tabblad naar rechts Verplaats tabblad naar _links Verplaats tabblad naar _rechts Naa_m tabblad Naam tabblad Vol_gend tabblad Nieuw tabblad Nieuw tabblad Nieuw venster Nieuw _tabblad Nieuw _venster Volgend tabblad Palet Voorinstellingen van palet Plakken V_orig tabblad _Voorkeuren Vorig tabblad Rood Rechts Regels terugschuiven Tekens voor op-woord-selecteren Sneltoetsen Stijl Positie van tabbalk Terminalvenster Terminalvenster voor het LXDE-project Terminal-lettertype Bovenaan Onderstrepen Gebruik de opdrachtregel Wit Geel U staat op het punt om %d tabbladen te sluiten. Weet u zeker dat u wilt doorgaan? Vergroten Ver_kleinen Verkleinen Vergrotingsfactor terugzetten op standaard Ver_groten Vergrotingsfactor terugzetten op standaard _Over Af_breken Tabblad _sluiten Be_werken _Bestand _Hulp Nieuw venster _OK _Plakken _Tabbladen terminal;opdrachtregel;uitvoeren; Sander De Voogdt
Pjotr <pjotrvertaalt@gmail.com> x 
��          �   %   �      `     a     t  	   �     �     �     �     �     �     �  5   �          $     0     A     U     b     n          �     �     �     �     �     �     �     �  �  �     �     �     �  	   �     �     
          -     @  ?   [     �     �     �     �     �  
   �       
     
   #     .     7     F     R     _     g     u                                                                             
                  	                             (correct spelling) (no suggested words) Add w_ord Cha_nge Change A_ll Change _to: Check Spelling Check _Word Completed spell checking Error when checking the spelling of word “%s”: %s Error: Ignore _All Misspelled word: No misspelled words Set Language Suggestions User dictionary: _Add _Cancel _Ignore _Ignore All _Select _Suggestions: language%s (%s) languageUnknown (%s) word Project-Id-Version: gspell
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gspell
POT-Creation-Date: 2017-10-29 09:50+0100
PO-Revision-Date: 2015-09-22 12:38+0200
Last-Translator: Hannie Dumoleyn <hannie@ubuntu-nl.org>
Language-Team: Dutch <gnome-nl-list@gnome.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 (correcte spelling) (geen woorden voorgesteld) W_oord toevoegen _Wijzigen A_lles wijzigen Wijzigen _in: Spelling controleren _Woord controleren Spellingscontrole voltooid Fout bij het controleren van de spelling van het woord "%s": %s Fout: _Alles negeren Verkeerd gespeld woord: Geen verkeerd gespelde woorden Taal instellen Suggesties Gebruikerswoordenboek: _Toevoegen _Annuleren Ne_geren Alles _negeren _Selecteren _Suggesties: %s (%s) Onbekend (%s) woord 
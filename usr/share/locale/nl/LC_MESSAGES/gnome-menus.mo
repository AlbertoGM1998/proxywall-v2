��          �   %   �      p     q     }  %   �  1   �     �  	   �                     $     :     C     S     Z     n     t  2   �     �     �     �     �  #   �          5     F  	   `     j  �  {     	           "  I   C     �     �  
   �     �     �     �     �  
   �                    &  5   3     i     �  
   �     �      �  &   �     �          )     6                                    	                              
                                                         Accessories Applications Applications and sites saved from Web Applications that did not fit in other categories Desktop accessories Education Games Games and amusements Graphics Graphics applications Internet Multimedia menu Office Office Applications Other Programming Programs for Internet access such as web and email Small but useful GNOME tools Sound & Video Sundry System Tools System configuration and monitoring Tools for software development Universal Access Universal Access Settings Utilities Web Applications Project-Id-Version: gnome-menus
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-02-02 22:27+0200
Last-Translator: Reinout van Schouwen <reinouts@gnome.org>
Language-Team: Dutch <gnome-nl-list@gnome.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 Accessoires Toepassingen Toepassingen en sites op het web Toepassingen die niet in een andere categorie ondergebracht kunnen worden Bureaubladaccessoires Educatie Spelletjes Spelletjes en amusement Grafisch Grafische toepassingen Internet Multimedia Kantoor Kantoortoepassingen Overig Ontwikkeling Toepassingen voor internettoegang zoals web en e-mail Kleine maar krachtige tools Geluid & Video Allerhande Systeemgereedschap Systeemconfiguratie en -controle Gereedschap voor software-ontwikkeling Toegankelijkheid Toegankelijkheidsinstellingen Hulpmiddelen Webtoepassingen 
��    0      �  C         (     )     9  
   K     V     \     p     x     �     �     �     �  !   �     �     �     �     �     �  "        )     /     8     E     T     `  1   �     �     �     �  
   �     �  	   �     �     �          %     5     E     T     g     �     �  ?   �     �     �     �  p   �     T  !  b     �     �     �     �     �     �     �     �     	     	     "	  $   /	     T	     Y	     ^	  
   d	     o	  0   �	     �	     �	     �	     �	     �	  "   �	  2   
     J
     Z
     `
  	   f
     p
  	   w
     �
  )   �
     �
     �
     �
     �
        "        4     ;  K   B  
   �     �     �  k   �          /   +              -         )   %                                   *                        $                            0   ,      "      .   &         '             #             (          	                   !                 
    3d-ish (button) 3d-ish (gradient) Animations Arrow Cell Indicator Size Classic Colorize Scrollbar Contrast Disable focus drawing Dot Edge Thickness Enable Animations on Progressbars Flat Full Glossy Gummy Handlebox Marks If set, button corners are rounded Inset Inverted Inverted Dot Inverted Slash Mark Type 1 Mark Type for Scrollbar Buttons Mark Type for Scrollbar Handles, Handleboxes, etc Menubar Style None Nothing Paned Dots Radius Rectangle Relief Style Requires style Glossy or Gummy Rounded Buttons Scrollbar Color Scrollbar Marks Scrollbar Type Scrollbutton Marks Sets the Color of Scrollbars Shadow Shaped Size of check- and radiobuttons inside treeviews. (Bug #351764) Slash Some Style This option allows to disable the focus drawing. The primary purpose is to create screenshots for documentation. Toolbar Style Project-Id-Version: gtk-engines
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2009-03-13 22:43+0100
Last-Translator: Kenneth Nielsen <k.nielsen81@gmail.com>
Language-Team: Danish <dansk@dansk-gruppen.dk>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 3d-agtig (knap) 3d-agtig (farveovergang) Animationer Pil Størrelse af celleindikator Klassisk Farvelæg rullebjælke Kontrast Deaktivér fokusoptegning Prik Kanttykkelse Aktivér animation af statusbjælker Flad Fuld Blank Tyggegummi Håndtagsmærker Ved tilvalg vil knappers hjørner blive afrundet Indlagt Omvendt Inverteret prik Inverteret skråstreg Mærketype 1 Mærketype for rullebjælkeknapper Mærketype for rullebjælkehåndtag, håndtag osv. Menubjælkestil Ingen Intet Prik-rude Radius Rektangel Relief-stil Kræver enten blank eller tyggegummi-stil Afrundede knapper Rullebjælkefarve Rullebjælkemærker Rullebjælketype Rulleknapmærker Indstiller farven af rullebjælker Skygge Formet Størrelse af afkrydsnings- og radioknapper i træperspektiv. (Bug #351764) Skråstreg Nogle Stil Dette valg deaktiverer fokusoptegning. Det primære formål er at oprette skærmbilleder til dokumentation. Værktøjsbjælke-stil 
��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u  	             (     7     @     E     J     O  	   T  	   ^  
   h     s  	   �     �     �     �     �  
   �     �     �     �     �     �          $     1     =  
   B     M  F   g  -   �  .   �                          &     7     H     Z  
   n     y     �     �     �     �     �     �                1     B     P  
   ]     h     v     �     �     �     �     �     �     �     �     �     �                (     1     M  #   V     z     �  
   �     �     �     �  H   �                      	   )     3     A  	   E     O     ]     f     k     s          �  
   �  ;   �  j   �     >                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-23 17:49+0000
Last-Translator: scootergrisen <scootergrisen@gmail.com>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495561763.532638
 Avanceret Tillad fed skrift Hørbar klokke Baggrund Sort Blok Blå Bund Klar blå Klar cyan Klar grøn Klar magenta Klar rød Brun Skjul rull_ebjælken Skjul r_ullebjælken Luk faneblad Nyt vindue Luk _vindue Bekræft lukning _Kopiér Kopiér Kopiér _URL Ophavsret (C) 2008-2017 Markørblink Markørstil Cyan Mørkegrå Standardvinduesstørrelse Deaktivér bekræftelse inden lukning af et vindue med flere faneblade Deaktivér menugenvejstast (F10 som standard) Deaktivér brug af Alt-n til faneblade og menu Skærm Forgrund Grå Grøn Skjul lukknapper Skjul menulinjen Skjul musemarkør Skjul rullebjælken LXTerminal Indstillinger for LXTerminal Venstre Magenta Flyt faneblad til venstre Flyt faneblad til højre Flyt faneblad til _venstre Flyt faneblad til _højre _Navngiv faneblad Navngiv faneblad N_æste faneblad Nyt _faneblad Nyt faneblad Nyt vindue Nyt _faneblad Nyt _vindue Næste faneblad Palet Palet forudindstilling Indsæt F_orrige faneblad Indstillin_ger Forrige faneblad Rød Højre Tilbagerulningslinjer Vælg efter ord-tegn Genveje Udseende Placering af fanebladspanel Terminal Terminalemulator for LXDE-projektet Terminalskrifttype Top Understreg Brug kommandolinjen Hvid Gul Du er ved at lukke %d faneblade. Er du sikker på, at du vil fortsætte? Zoom ind Zoom _ud Zoom ud Zoom nulstil Zoom _ind Zoom _nulstil _Om _Annuller _Luk faneblad _Rediger _Fil _Hjælp _Nyt vindue _OK _Indsæt _Faneblade console;command line;execute;konsol;kommandolinje;eksekver; Dansk-gruppen <dansk@dansk-gruppen.dk>
Joe Hansen, 2010
Vidar Jon Bauge
Charlie Tyrrestrup
Peter Jespersen x 
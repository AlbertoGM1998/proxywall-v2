��    �      �  K  <      h     i     n  	   {     �     �     �     �     �  "   �     �     �     �     �          '     :     P     b     q     �     �  N   �  g   �  G   X  :   �  E   �  A   !  *   c  *   �  )   �  (   �  7     s   D  '   �  ?   �  <         ]  ,   n  $   �     �     �     �  /     8   6  \   o  b   �  d   /  ]   �  d   �  f   W  b   �  m   !  `   �     �  #        ,     J  "   ^  !   �  /   �     �     �     �     �  
     >     G   V  5   �     �     �  `      G   d   	   �      �      �       �   #   !  #   '!     K!  5   f!  !   �!  $   �!  '   �!  '   "  9   3"  $   m"  )   �"  5   �"      �"     #     '#  	   F#     P#     ^#  <   r#  <   �#  :   �#     '$     B$     [$     s$     �$     �$     �$  )   �$  ,   �$     %  	   %     %     )%     9%     P%  #   j%     �%     �%  %   �%  (   �%  (   &  +   4&     `&  )   i&     �&  �   �&     �'     �'     �'     �'     �'  *   �'  -   (  -   5(  0   c(     �(  2   �(     �(  (   �(  *   )     E)  
   U)     `)     y)  &   ~)     �)  !   �)     �)     �)     *     *  "   (*     K*      a*     �*     �*     �*     �*     �*     �*     �*     	+  L   $+  8   q+     �+     �+     �+     �+     �+     �+  E   ,     U,     b,     q,  :   ,     �,     �,     �,     �,  <   �,  .   6-     e-  	   i-     s-     u-     �-  
   �-     �-     �-     �-     �-     �-     �-     �-     �-     .     .     #.     +.     9.     G.  
   M.  
   X.  
   c.     n.     w.     �.  	   �.     �.     �.     �.  	   �.     �.     �.     �.     /     /     '/     +/     2/     9/     =/     C/  
   J/     U/     c/     q/     �/     �/     �/     �/     �/     �/     �/  
   �/     �/     �/  
   0     0     0     0     #0     20     >0  '  M0     u1     z1  	   �1     �1     �1     �1     �1     �1  p   �1  &   =2  #   d2  =   �2  9   �2  6    3  D   73  P   |3     �3     �3  <   4  \   C4     �4  �   �4  �   m5  �   O6  �   �6  �   y7  �   .8  k   �8  n   F9  d   �9  e   :  s   �:  �   �:  i   �;  �   <  �   �<      >=  l   _=  c   �=  8   0>  0   i>  !   �>  [   �>  m   ?  �   �?  �   M@  �   A  �   �A  �   �B  �   �C  �   |D    vE  �   �F  B   aG  Z   �G  Z   �G  E   ZH  e   �H  2   I  ^   9I  *   �I      �I     �I      �I     J  �   !J  �   �J  �   �K  J   FL  \   �L    �L  �   �M     �N  4   �N  K   O  x   hO  E   �O  K   'P  6   sP  T   �P  W   �P  �   WQ  N   �Q  W   (R  `   �R  B   �R  W   $S  ~   |S  K   �S  0   GT  N   xT     �T  +   �T  -   
U  �   8U  �   �U  �   �V  C   @W  @   �W  C   �W  C   	X  =   MX     �X     �X  l   �X  �   Y     �Y     �Y  E   �Y  ?   �Y  ,   %Z  8   RZ  c   �Z  .   �Z  `   [  �   [  l   \  u   }\  �   �\     u]  }   ~]  ;   �]  �  8^  )   `  Y   ;`  )   �`     �`  0   �`  �   a  x   �a  �   -b  �   �b  $   =c  �   bc  8   �c  Z    d  f   {d      �d     e  0   e     Ie  N   Ne  N   �e  Z   �e  V   Gf  H   �f  7   �f  +   g  W   Kg  <   �g  Z   �g  "   ;h  9   ^h  0   �h  0   �h  3   �h  =   .i  c   li  -   �i  �   �i  �   �j  6   ;k     rk  '   xk  D   �k  '   �k  )   l  �   7l  '   �l  )   #m  (   Mm  �   vm  5   �m  >   0n  9   on     �n  �   �n  s   .o     �o  	   �o     �o  R   �o  
   p     p     0p  L   >p     �p  @   �p     �p  -   �p     'q  3   @q  9   tq  	   �q     �q  9   �q      r     ,r     ?r  $   Rr  $   wr     �r  3   �r  6   �r      s  3   9s  6   ms     �s     �s     �s  $   �s  E   t     Ht  *   Ut     �t     �t     �t     �t     �t     �t  i   u  6   qu     �u     �u     �u     �u  -    v  H   .v  	   wv  0   �v  0   �v  $   �v  ?   w  B   Hw  !   �w  !   �w     �w     �w  <   x     Ax     Qx     �   T      Q      j   �   �       �       �   �   X   �   �   L   �               �   A       >   �   r   .   �   �              �   �          �   %   $           �   �   �   {   �              �       l   �       ?       �       �   �   P   R   �   ^   �   �       �      �       y          �   �   5   3       �   �           1       Z      �   �   �   �   s   �       �           �   �   2           |   *   �   �   }          �   J   �      �   u   8       (      �   �   :   i   �   �   V   &           g   �   �           �   E   I   B   h   b       �   �   0   �       M   �   ;          c   G   �   e   �   �   p   v          �      �   	   �       �      �   �   Y   F   �   �   �       #      '      �   t   4       \   �   9   x   �   �   /   )           C                      �   @   �              �      n   �              o   U           -   S          D           _   �      d       �   �      �   �   K   7       H       �       �   �   �                   �       �       �       �       `           �   �       "      z   f       �           �   
   q   a           �      �   �   �   �   ~   �   �      �   �       �   m   �           �      �   W   k               �       <   �       6              [   �           ,   =   N   ]   w       �   �   �   O   !   +   �    "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 ,  <name unset> <no EWMH-compliant window manager> <no viewport> <unset> Action not allowed
 Activate the window Activate the workspace Active Window: %s
 Active Workspace: %s
 Alias of --window Always On _Top Always group windows Bottom Neighbor: %s
 CLASS Cannot change the workspace layout on the screen: the layout is already owned
 Cannot interact with application having its group leader with XID %lu: the application cannot be found
 Cannot interact with class group "%s": the class group cannot be found
 Cannot interact with screen %d: the screen does not exist
 Cannot interact with window with XID %lu: the window cannot be found
 Cannot interact with workspace %d: the workspace cannot be found
 Change the X coordinate of the window to X Change the Y coordinate of the window to Y Change the height of the window to HEIGHT Change the name of the workspace to NAME Change the number of workspaces of the screen to NUMBER Change the type of the window to TYPE (valid values: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Change the width of the window to WIDTH Change the workspace layout of the screen to use NUMBER columns Change the workspace layout of the screen to use NUMBER rows Class Group: %s
 Class resource of the class group to examine Click this to switch to workspace %s Click to start dragging "%s" Click to switch to "%s" Close the window Conflicting options are present: --%s and --%s
 Conflicting options are present: --%s or --%s, and --%s
 Conflicting options are present: a window should be interacted with, but --%s has been used
 Conflicting options are present: an application should be interacted with, but --%s has been used
 Conflicting options are present: class group "%s" should be interacted with, but --%s has been used
 Conflicting options are present: screen %d should be interacted with, but --%s has been used
 Conflicting options are present: windows of an application should be listed, but --%s has been used
 Conflicting options are present: windows of class group "%s" should be listed, but --%s has been used
 Conflicting options are present: windows of workspace %d should be listed, but --%s has been used
 Conflicting options are present: windows or workspaces of screen %d should be listed, but --%s has been used
 Conflicting options are present: workspace %d should be interacted with, but --%s has been used
 Current workspace: "%s" Display windows from all workspaces Don't show window in tasklist Enable Transparency Error while parsing arguments: %s
 Geometry (width, height): %d, %d
 Geometry (x, y, width, height): %d, %d, %d, %d
 Group Leader: %lu
 Group Name: %s
 HEIGHT Icon Name: %s
 Icons: %s
 Invalid argument "%d" for --%s: the argument must be positive
 Invalid argument "%d" for --%s: the argument must be strictly positive
 Invalid argument "%s" for --%s, valid values are: %s
 Invalid value "%s" for --%s Left Neighbor: %s
 List windows of the application/class group/workspace/screen (output format: "XID: Window Name") List workspaces of the screen (output format: "Number: Workspace Name") Ma_ximize Ma_ximize All Make the window always on top Make the window appear in pagers Make the window appear in tasklists Make the window below other windows Make the window fullscreen Make the window have a fixed position in the viewport Make the window not always on top Make the window not appear in pagers Make the window not appear in tasklists Make the window not below other windows Make the window not have a fixed position in the viewport Make the window quit fullscreen mode Make the window visible on all workspaces Make the window visible on the current workspace only Maximize horizontally the window Maximize the window Maximize vertically the window Mi_nimize Mi_nimize All Minimize the window Move the viewport of the current workspace to X coordinate X Move the viewport of the current workspace to Y coordinate Y Move the window to workspace NUMBER (first workspace is 0) Move to Another _Workspace Move to Workspace R_ight Move to Workspace _Down Move to Workspace _Left Move to Workspace _Up NAME NUMBER NUMBER of the screen to examine or modify NUMBER of the workspace to examine or modify N_ROWS Name: %s
 Never group windows No Windows Open Number of Windows: %d
 Number of Workspaces: %d
 On Screen: %d (Window Manager: %s)
 On Workspace: %s
 Only show current workspace Options to list windows or workspaces Options to modify properties of a screen Options to modify properties of a window Options to modify properties of a workspace PID: %s
 Position in Layout (row, column): %d, %d
 Possible Actions: %s
 Print or modify the properties of a screen/workspace/window, or interact with it, following the EWMH specification.
For information about this specification, see:
	http://freedesktop.org/wiki/Specifications/wm-spec Resource Class: %s
 Right Neighbor: %s
 Screen Number: %d
 Session ID: %s
 Shade the window Show options to list windows or workspaces Show options to modify properties of a screen Show options to modify properties of a window Show options to modify properties of a workspace Show the desktop Show workspace names instead of workspace contents Showing the desktop: %s
 Start moving the window via the keyboard Start resizing the window via the keyboard Startup ID: %s
 State: %s
 Stop showing the desktop TYPE Tool to switch between visible windows Tool to switch between windows Tool to switch between workspaces Top Neighbor: %s
 Transient for: %lu
 Un_minimize All Unma_ximize Unmaximize horizontally the window Unmaximize the window Unmaximize vertically the window Unmi_nimize Unminimize the window Unshade the window Untitled application Untitled window Use N_ROWS rows Use RTL as default direction Use a vertical orientation Viewport cannot be moved: the current workspace does not contain a viewport
 Viewport cannot be moved: there is no current workspace
 Viewport position (x, y): %s
 WIDTH Window List Window Manager: %s
 Window Selector Window Type: %s
 Window cannot be moved to workspace %d: the workspace does not exist
 Workspace %d Workspace %s%d Workspace 1_0 Workspace Layout (rows, columns, orientation): %d, %d, %s
 Workspace Name: %s
 Workspace Number: %d
 Workspace Switcher X X window ID of the group leader of an application to examine X window ID of the window to examine or modify XID XID: %lu
 Y _Always on Visible Workspace _Close _Close All _Move _Only on This Workspace _Resize _Unmaximize All above all workspaces below change fullscreen mode change workspace close desktop dialog window dock or panel false fullscreen make above make below maximize maximize horizontally maximize vertically maximized maximized horizontally maximized vertically minimize minimized move needs attention no action possible normal normal window pin pinned resize set shade shaded skip pager skip tasklist splash screen startupIDnone stick sticky tearoff menu tearoff toolbar true unmake above unmake below unmaximize unmaximize horizontally unmaximize vertically unminimize unpin unshade unstick utility window windownone workspacenone Project-Id-Version: libwnck
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2009-02-06 20:48+0700
Last-Translator: Theppitak Karoonboonyanan <thep@linux.thai.net>
Language-Team: Thai <thai-l10n@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 ,  <ไม่มีชื่อ> <ไม่มีโปรแกรมจัดการหน้าต่างที่รองรับ EWMH> <ไม่มีช่องมอง> <ไม่ได้กำหนด> ไม่อนุญาตให้กระทำการ
 เรียกหน้าต่างขึ้นมา ย้ายไปพื้นที่ทำงาน หน้าต่างที่ใช้งานอยู่: %s
 พื้นที่ทำงานที่ใช้งานอยู่: %s
 เหมือน --window อยู่_บนสุด จัดกลุ่มหน้าต่างเสมอ พื้นที่ทำงานที่ติดกันด้านล่าง: %s
 CLASS ไม่สามารถเปลี่ยนการจัดวางของพื้นที่ทำงานได้: การจัดวางมีเจ้าของแล้ว
 ไม่สามารถโต้ตอบกับโปรแกรมที่มีหน้าต่างหลักที่มี XID เป็น %lu: ไม่พบโปรแกรมดังกล่าว
 ไม่สามารถโต้ตอบกับกลุ่มคลาส "%s": ไม่พบกลุ่มคลาสดังกล่าว
 ไม่สามารถโต้ตอบกับสกรีน %d: ไม่มีสกรีนดังกล่าวอยู่
 ไม่สามารถโต้ตอบกับหน้าต่างที่มี XID เป็น %lu: ไม่พบหน้าต่างดังกล่าว
 ไม่สามารถโต้ตอบกับพื้นที่ทำงาน %d: ไม่พบพื้นที่ทำงานดังกล่าว
 เปลี่ยนพิกัดแนวนอนของหน้าต่างไปเป็น X เปลี่ยนพิกัดแนวตั้งของหน้าต่างไปเป็น Y เปลี่ยนความสูงของหน้าต่างไปเป็น HEIGHT เปลี่ยนชื่อของพื้นที่ทำงานไปเป็น NAME เปลี่ยนจำนวนพื้นที่ทำงานของสกรีนเป็น NUMBER เปลี่ยนชนิดของหน้าต่างไปเป็น TYPE (ค่าที่ใช้ได้: normal, desktop, dock, dialog, toolbar, menu, utility, splash) เปลี่ยนความกว้างของหน้าต่างไปเป็น WIDTH เปลี่ยนการจัดวางพื้นที่ทำงานของสกรีนเป็น NUMBER คอลัมน์ เปลี่ยนการจัดวางพื้นที่ทำงานของสกรีนเป็น NUMBER แถว กลุ่มคลาส: %s
 ทรัพยากรคลาสของกลุ่มคลาสที่จะตรวจสอบ กดที่นี่แล้วจะสลับไปพื้นที่ทำงาน %s คลิกเพื่อเริ่มลาก "%s" คลิกเพื่อสลับไป %s ปิดหน้าต่าง มีตัวเลือกที่ขัดแย้งกัน: --%s กับ --%s
 มีตัวเลือกที่ขัดแย้งกัน: --%s หรือ --%s กับ --%s
 มีตัวเลือกที่ขัดแย้งกัน: ต้องโต้ตอบกับหน้าต่าง แต่มีการใช้ตัวเลือก --%s
 มีตัวเลือกที่ขัดแย้งกัน: ต้องโต้ตอบกับโปรแกรม แต่มีการใช้ตัวเลือก --%s
 มีตัวเลือกที่ขัดแย้งกัน: ต้องโต้ตอบกับกลุ่มคลาส "%s" แต่มีการใช้ตัวเลือก --%s
 มีตัวเลือกที่ขัดแย้งกัน: ต้องโต้ตอบกับสกรีน %d แต่มีการใช้ตัวเลือก --%s
 มีตัวเลือกที่ขัดแย้งกัน: ต้องแสดงรายการหน้าต่างของโปรแกรม แต่มีการใช้ตัวเลือก --%s
 มีตัวเลือกที่ขัดแย้งกัน: ต้องแสดงรายการหน้าต่างของกลุ่มคลาส "%s" แต่มีการใช้ตัวเลือก --%s
 มีตัวเลือกที่ขัดแย้งกัน: ต้องแสดงรายการหน้าต่างของพื้นที่ทำงาน %d แต่มีการใช้ตัวเลือก --%s
 มีตัวเลือกที่ขัดแย้งกัน: ต้องแสดงรายการหน้าต่างหรือพื้นที่ทำงานของสกรีน %d แต่มีการใช้ตัวเลือก --%s
 มีตัวเลือกที่ขัดแย้งกัน: ต้องโต้ตอบกับพื้นที่ทำงาน %d แต่มีการใช้ตัวเลือก --%s
 พื้นที่ทำงานปัจจุบัน: "%s" แสดงหน้าต่างของทุกพื้นที่ทำงาน ไม่ต้องแสดงหน้าต่างในรายการงาน เปิดใช้หน้าต่างโปร่งแสง เกิดข้อผิดพลาดขณะแจงอาร์กิวเมนต์: %s
 ขนาด (กว้าง, สูง): %d, %d
 ตำแหน่งและขนาด (x, y, กว้าง, สูง): %d, %d, %d, %d
 หน้าต่างหลัก: %lu
 ชื่อกลุ่ม: %s
 HEIGHT ชื่อไอคอน: %s
 ไอคอน: %s
 อาร์กิวเมนต์ "%d" ใช้ไม่ได้กับ --%s: อาร์กิวเมนต์ดังกล่าวต้องเป็นจำนวนบวก
 อาร์กิวเมนต์ "%d" ใช้ไม่ได้กับ --%s: อาร์กิวเมนต์ดังกล่าวต้องเป็นจำนวนบวกเท่านั้น
 อาร์กิวเมนต์ "%s" ใช้ไม่ได้กับ --%s ค่าที่ใช้ได้คือ: %s
 ค่า "%s" ใช้การไม่ได้สำหรับ --%s พื้นที่ทำงานที่ติดกันด้านซ้าย: %s
 แสดงรายการหน้าต่างของโปรแกรม/กลุ่มคลาส/พื้นที่ทำงาน/สกรีน (รูปแบบการแสดงผล: "XID: ชื่อหน้าต่าง") แสดงรายการพื้นที่ทำงานของสกรีน (รูปแบบการแสดงผล: "หมายเลข: ชื่อพื้นที่ทำงาน") ขย_ายเต็มจอ _ขยายเต็มจอทั้งหมด ยกหน้าต่างขึ้นมาอยู่บนสุด แสดงหน้าต่างในเครื่องมือสลับพื้นที่ทำงาน แสดงหน้าต่างในรายการงาน ลดหน้าต่างลงไปอยู่ล่างสุด แสดงหน้าต่างเต็มจอ ตรึงตำแหน่งหน้าต่างในช่องมอง เลิกยกหน้าต่างขึ้นมาอยู่บนสุด ไม่แสดงหน้าต่างในเครื่องมือสลับพื้นที่ทำงาน ไม่แสดงหน้าต่างในรายการงาน เลิกลดหน้าต่างลงไปอยู่ล่างสุด เลิกตรึงตำแหน่งหน้าต่างในช่องมอง เลิกแสดงหน้าต่างเต็มจอ แสดงหน้าต่างบนทุกพื้นที่ทำงาน แสดงหน้าต่างบนพื้นที่ทำงานปัจจุบันเท่านั้น ขยายหน้าต่างเต็มตามแนวนอน ขยายหน้าต่างเต็ม ขยายหน้าต่างเต็มตามแนวตั้ง ย่_อเก็บ _ย่อเก็บทั้งหมด ย่อเก็บหน้าต่าง เลื่อนช่องมอง (viewport) ของพื้นที่ทำงานปัจจุบันไปที่พิกัดแนวนอน X เลื่อนช่องมอง (viewport) ของพื้นที่ทำงานปัจจุบันไปที่พิกัดแนวตั้ง Y ย้ายหน้าต่างไปยังพื้นที่ทำงาน NUMBER (พื้นที่ทำงานแรกคือหมายเลข 0) ย้ายไป_พื้นที่ทำงานอื่น ย้ายไปพื้นที่ทำงานข_วา ย้ายไปพื้นที่ทำงาน_ล่าง ย้ายไปพื้นที่ทำงานซ้_าย ย้ายไปพื้นที่ทำงาน_บน NAME NUMBER หมายเลขของสกรีนที่จะตรวจสอบหรือแก้ไข หมายเลขของพื้นที่ทำงานที่จะตรวจสอบหรือแก้ไข N_ROWS ชื่อ: %s
 ไม่ต้องจัดกลุ่มหน้าต่าง ไม่มีหน้าต่างเปิดอยู่ จำนวนหน้าต่าง: %d
 จำนวนพื้นที่ทำงาน: %d
 ที่สกรีน: %d (โปรแกรมจัดการหน้าต่าง: %s)
 บนพื้นที่ทำงาน %s
 แสดงพื้นที่ทำงานปัจจุบันเท่านั้น ตัวเลือกสำหรับแสดงรายการหน้าต่างหรือพื้นที่ทำงาน ตัวเลือกสำหรับแก้ไขคุณสมบัติของสกรีน ตัวเลือกสำหรับแก้ไขคุณสมบัติของหน้าต่าง ตัวเลือกสำหรับแก้ไขคุณสมบัติของพื้นที่ทำงาน PID: %s
 ตำแหน่งในโครงสร้างการจัดวาง (แถว, คอลัมน์): %d, %d
 ปฏิบัติการที่ทำได้: %s
 พิมพ์หรือแก้ไขคุณสมบัติของสกรีน/พื้นที่ทำงาน/หน้าต่าง หรือโต้ตอบกับออบเจกต์ดังกล่าว ตามข้อกำหนด EWMH
สำหรับข้อมูลเกี่ยวกับข้อกำหนดเหล่านี้ โปรดอ่าน:
	http://freedesktop.org/wiki/Specifications/wm-spec คลาสทรัพยากร: %s
 พื้นที่ทำงานที่ติดกันด้านขวา: %s
 หมายเลขสกรีน: %d
 ID ของวาระ: %s
 ม้วนหน้าต่างขึ้น แสดงตัวเลือกสำหรับการแสดงรายการหน้าต่างหรือพื้นที่ทำงาน แสดงตัวเลือกสำหรับแก้ไขคุณสมบัติของสกรีน แสดงตัวเลือกสำหรับแก้ไขคุณสมบัติของหน้าต่าง แสดงตัวเลือกสำหรับแก้ไขคุณสมบัติของพื้นที่ทำงาน แสดงพื้นโต๊ะ แสดงชื่อพื้นที่ทำงานแทนเนื้อหาในพื้นที่ทำงาน กำลังแสดงพื้นโต๊ะ: %s
 เริ่มย้ายหน้าต่างด้วยแป้นพิมพ์ เริ่มปรับขนาดหน้าต่างด้วยแป้นพิมพ์ ID เริ่มแรก; %s
 สถานะ: %s
 เลิกแสดงพื้นโต๊ะ TYPE ใช้สลับไปมาระหว่างหน้าต่าง ใช้สลับไปมาระหว่างหน้าต่าง ใช้สลับไปมาระหว่างพื้นที่ทำงาน พื้นที่ทำงานที่ติดกันด้านบน: %s
 หน้าต่างชั่วคราวสำหรับ: %lu
 เ_ลิกย่อเก็บทั้งหมด เลิกขย_ายเต็มจอ เลิกขยายหน้าต่างเต็มตามแนวนอน เลิกขยายหน้าต่างเต็ม เลิกขยายหน้าต่างเต็มตามแนวตั้ง เลิกย่_อเก็บ เลิกย่อเก็บหน้าต่าง คลี่หน้าต่างกลับ โปรแกรมไม่มีชื่อ หน้าต่างไม่มีชื่อ ใช้จำนวนแถวเท่ากับ N_ROWS เขียนข้อความแบบขวาไปซ้ายโดยปริยาย จัดวางในแนวตั้ง ไม่สามารถย้ายช่องมองได้: พื้นที่ทำงานปัจจุบันไม่มีช่องมอง
 ไม่สามารถย้ายช่องมองได้: ไม่มีพื้นที่ทำงานปัจจุบัน
 ตำแหน่งช่องมอง (x, y): %s
 WIDTH ลิสต์หน้าต่าง โปรแกรมจัดการหน้าต่าง: %s
 เลือกหน้าต่าง ชนิดหน้าต่าง: %s
 ไม่สามารถย้ายหน้าต่างไปยังพื้นที่ทำงาน %d: ไม่มีพื้นที่ทำงานดังกล่าว
 พื้นที่ทำงาน %d พื้นที่ทำงาน %s%d พื้นที่ทำงาน 1_0 การจัดวางพื้นที่ทำงาน (แถว, คอลัมน์, แนววาง): %d, %d, %s
 ชื่อพื้นที่ทำงาน: %s
 หมายเลขพื้นที่ทำงาน: %d
 ตัวสลับพื้นที่ทำงาน X ID ใน X Window ของหน้าต่างหลักของโปรแกรมที่จะตรวจสอบ ID ใน X Window ของหน้าต่างที่จะตรวจสอบหรือแก้ไข XID XID: %lu
 Y บน_ทุกพื้นที่ทำงานที่มองเห็น ปิ_ด ปิ_ดทั้งหมด ย้า_ย บน_พื้นที่ทำงานนี้เท่านั้น _ปรับขนาด เลิ_กขยายเต็มจอทั้งหมด อยู่บน ทุกพื้นที่ทำงาน อยู่ล่าง เปลี่ยนโหมดเต็มจอ เปลี่ยนพื้นที่ทำงาน ปิด พื้นโต๊ะ หน้าต่างกล่องโต้ตอบ dock หรือพาเนล ไม่ใช่ เต็มจอ ยกขึ้นข้างบน ลดลงข้างล่าง ขยายเต็ม ขยายเต็มตามแนวนอน ขยายเต็มตามแนวตั้ง ขยายเต็ม ขยายเต็มตามแนวนอน ขยายเต็มตามแนวตั้ง ย่อเก็บ ย่อเก็บ ย้าย เตือนให้สนใจ ไม่มีปฏิบัติการที่ทำได้ ปกติ หน้าต่างธรรมดา ปักหมุด ปักหมุด ปรับขนาด กำหนด ม้วนขึ้น ม้วนขึ้น ไม่แสดงในเครื่องมือสลับพื้นที่ทำงาน ไม่แสดงในรายการงาน ภาพไตเติล ไม่มี ติดหนึบ ติดหนึบ เมนูที่ดึงออกมา แถบเครื่องมือที่ดึงออกมา ใช่ เลิกยกขึ้นข้างบน เลิกลดลงข้างล่าง เลิกขยายเต็ม เลิกขยายเต็มตามแนวนอน เลิกขยายเต็มตามแนวตั้ง เลิกย่อเก็บ เลิกปักหมุด คลี่ออก ไม่ติดหนึบ หน้าต่างอรรถประโยชน์ ไม่มี ไม่มี 
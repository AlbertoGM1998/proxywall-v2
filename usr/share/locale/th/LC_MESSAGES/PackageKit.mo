��          �      |      �     �  +   �     )     8     O      h     �     �     �     �     �     �       $     $   9     ^     m  !   �     �     �  #   �  r  �  H   h  ~   �  '   0  B   X  Q   �  J   �  o   8     �  0   �     �  J     >   _     �  s   �  G   0	     x	  Y   �	  K   �	  -   :
     h
  U   �
                                                    	            
                                 Accept EULA Authentication is required to accept a EULA Command failed Disable the idle timer Exit after a small delay Exit after the engine has loaded Install untrusted local file No files Package description Package files PackageKit Console Interface PackageKit Monitor PackageKit service Packaging backend to use, e.g. dummy Please enter a number from 1 to %i:  Remove package Set the filter, e.g. installed Show the program version and exit Show version and exit Subcommands: The daemon crashed mid-transaction! Project-Id-Version: PackageKit
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-25 08:54+0000
Last-Translator: Richard Hughes <richard@hughsie.com>
Language-Team: Thai (http://www.transifex.com/freedesktop/packagekit/language/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
 ยอมรับข้อตกลงสัญญาอนุญาต ต้องยึนยันตัวเพื่อยอมรับข้อตกลงสัญญาอนุญาต คำสั่งล้มเหลว ปิดไทเมอร์ที่ไม่ใช้งาน ออกหลังจากหน่วงเวลาเล็กน้อย ออกหลังจาก engine ถูกเรียกแล้ว ติดตั้งไฟล์ที่ไม่น่าเชื่อถือบนเครื่อง ไม่มีไฟล์ รายละเอียดแพคเกจ ไฟล์แพคเกจ ส่วนติดต่อบนคอนโซลของ PackageKit โปรแกรมเฝ้าสังเกต PackageKit บริการ PackageKit แบ็คเอนด์ตัวจัดการแพคเกจที่จะใช้ เช่น dummy กรุณาป้อนตัวเลขจาก 1 ถึง %i:  ถอดแพคเกจ ตั้งตัวกรอง เช่น ติดตั้งไว้แล้ว แสดงรุ่นของโปรแกรมแล้วออก แสดงรุ่นแล้วออก คำสั่งย่อย: ดีมอนขัดข้องระหว่างกระบวนการ! 
��    z      �  �   �      H
  (   I
     r
     ~
     �
     �
     �
  &   �
     �
  �   	     �     �     �  0   �  2   �  :        Z     `     h     z     �     �  &   �  "   �  	   �     �          1     H  	   e     o  
        �     �  '   �     �     �     �  o   �     n     ~     �     �  $   �  "   �     �     
     #  "   +     N     \     a     j     p     x  
   ~  3   �     �  	   �  	   �     �     �     �                %     1     B     Y     `  -   t  j   �                    $     )     5     C  
   K     V  
   \     g     �     �     �     �     �     �     �     �     �     �       8   0     i  	   r     |     �     �     �  %   �     �     �     �  >        G     O     a  
   n     y     �     �     �     �     �  
   �     �     �     �  
   �     �  �  �  D   �     �      �  $   �        ,   2  E   _     �  �   �     �  )   �     �  b   �  ]   S  |   �     .  
   B     M  !   m     �     �  F   �  P        X  A   m  >   �  -   �  6        S  .   f     �  +   �     �  O   �     :  4   Z  2   �  �   �     m     �  '   �  %   �  U   �  E   ;  ,   �  C   �     �  ^     !   `     �  
   �     �     �     �     �  a   �  %   M   
   s      ~   <   �      �      �      �      !     -!  %   I!  )   o!  
   �!  4   �!  m   �!  �   G"     #     !#     .#     ;#     J#  '   f#     �#     �#     �#     �#  3   �#     	$     $  +   =$     i$     v$     �$  ,   �$     �$  -   �$  '   %  4   D%  o   y%     �%     �%  !   &  -   .&  '   \&     �&  @   �&     �&  -   �&  +   '  T   F'     �'     �'     �'     �'     �'     (  *   (  )   G(  0   q(     �(     �(  	   �(     �(     �(     �(     )     X   C   e   A       1       d   H   L   Y      [                  %       `                  /          P       T   f   q   D          N   :   I   B   j          (   )   V   +   r   
   n   v   M   c       8      4   ;   l   W       z   G   h   _       &   m   a       y   =               -   3   g   u   .   >   <   s   F   ?   O          ^   K   !   7   U              5                 6      $             	      E         J         '           S   Z       Q       ,   #   k   b           t      i   *             p       ]                  x      \   o       @            R       2   "   w       0           9       <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. A11y Advanced Options Application Application to create spreedsheet, like gnumeric Application to manage bittorent, like transmission Applications automatically started after entering desktop: Apply Archive Audio application Audio manager Audio player Authentication Authentication failed!
Wrong password? Automatically Started Applications Autostart Autostart the application ? Autostart the application ?
 Available applications Banner to show on the dialog Bittorent Burning utility Calculator Calculator application Cancel Change the default applications on LXDE Charmap Charmap application Clipboard manager Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Communication 1 Communication 2 Composite Manager Core applications Custom message to show on the dialog Default applications for LXSession Desktop Manager Desktop Session Settings Disable Disable autostarted applications ? Disks utility Dock Document Email Enabled Error Error: %s
 Extra: Add an extra parameter to the launch option
 File Manager Group: %s Identity: Image viewer Information LXPolKit LXSession configuration L_ock Screen Laptop mode Launcher manager Launching applications Layout Lock screen manager Manage applications loaded in desktop session Manual Settings: Manually sets the command (you need to restart lxsession-default-apps to see the change)
 Menu prefix Mode Model More Network GUI Notes utility Options PDF Reader Panel Password:  Policykit Authentication Agent Polkit agent Position of the banner Power Manager Proxy Quit manager Reload S_witch User Screensaver Screenshot manager Security (keyring) Session : specify the session
 Set an utility to manager connections, such as nm-applet Settings Sh_utdown Spreadsheet Tasks monitor Terminal manager Text editor The database is updating, please wait Type Update lxsession database Upgrade manager Use a communication software (an IRC client, an IM client ...) Variant Video application Video player Webbrowser Webcam Widget 1 Window Manager: Windows Manager Workspace manager Xrandr _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-28 17:23+0000
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh <kk_KZ@googlegroups.com>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495992226.868729
 <b><big>%s %s сеансын аяқтау керек пе?</big></b> <b>Dbus</b> <b>Қоршам ортасы</b> <b>Жалпы баптаулар</b> <b>Жайма</b> <b>Белгілі қолданбалар</b> <b>Қолдан автоқосылатын қолданбалар</b> <b>Баптаулар</b> <b>Ескерту:</b> Неге үшін білмесеңіз, осыны <b>ӨЗГЕРТПЕҢІЗ</b>.

<b>ЕСТЕЛІК:</b> Бұл баптаулар жүйеге келесі рет кірген кезде іске асырылады. A11y Кеңейтілген баптаулар Қолданба Электрондық кестелерді жасау қолданбасы, gnumeric сияқты Битторренттерді басқару қолданбасы, transmission сияқты Жүйеге кіргеннен кейін автоматты түрде іске қосылатын қолданбалар: Іске асыру Архив Аудио қолданбасы Аудио басқарушысы Аудио плеері Аутенфикация Аутентификация сәтсіз!
Пароль қате ма? Автоматты түрде іске қосылатын қолданбалар Автоқосылу Қолданба іске автоқосылуы керек пе? Қолданбаны автожөнелту керек пе ?
 Қолжетерлік қолданбалар Сұхбатта көрсетілетін баннер Битторент Дисктерді жазу утилитасы Калькулятор Калькулятор қолданбасы Бас тарту LXDE үшін үнсіз келісім қолданбаларын таңдау Таңбалар кестесі Таңбалар кестесі қолданбасы Алмасу буферін басқарушысы Терезелер басқарушысын іске қосу командасы
(LXDE үшін үнсіз келісім бойынша openbox-lxde болуы тиіс) Байланысу 1 Байланысу 2 Композиттік менеджер Базалық қолданбалар Сұхбатта көрсетілетін таңдауыңызша хабарлама LXSession үшін үнсіз келісім қолданбалары Жұмыс үстел басқарушысы Жұмыс үстелі сессиясының баптаулары Сөндіру Қолданбалардың іске автоқосылуын сөндіру керек пе? Дисктер утилитасы Қалқымалы панель Құжат Эл. пошта Іске қосулы Қате Қате: %s
 Экстра: жөнелту опциясына экстра параметрді қосыңыз
 Файлдар басқарушысы Топ: %s Идентификация: Суреттерді көрсету бағдарламасы Ақпарат LXPolKit LXSession баптаулары Экранды бл_октау Ноутбук режимі Жөнелту басқарушысы Қолданбаларды жөнелту Жайма Экранды блоктау басқарушысы Жұмыс үстелі сессиясында жүктелетін қолданбаларды басқару Қолдан баптаулар: Қолмен көрсетілген команда (өзгерісті көру үшін, сізге lxsession-default-apps қайта қосу керек)
 Мәзір префиксі Режимі Модель Көбірек Желіні басқару Естеліктер утилитасы Опциялар PDF оқу сайманы Панель Пароль:  PolicyKit аутентификация агенті Polkit агенті Баннер орналасуы Эл. қоректі басқарушысы Прокси Шығу менеджері Жаңарту Па_йдаланушыны ауыстыру Экран қорғаушысы Скриншоттар басқарушысы Қауіпсіздік (кілттер) Сессия: сессияны көрсетіңіз
 Байланыстар басқарушысы утилитасын көрсетіңіз, nm-applet сияқты Баптаулар Сө_ндіру Электрондық кесте Тапсырмалар бақылаушысы Терминал басқарушысы Мәтін түзеткіші Дерекқор жаңартылуда, күте тұрыңыз Түрі lxsession дерекқорын жаңарту Жаңартулар басқарушысы Хабарласу БҚ қолдану (IRC не IM клиенті сияқты ...) Нұсқасы Видео қолданбасы Видео плеері Веб браузері Вебкамера Виджет 1 Терезелер басқарушысы: Терезелер басқарушысы Жұмыс орындар басқарушысы Xrandr Г_ибернация Ш_ығу Қай_та қосу Ұ_йықтату сурет файлы хабарлама 
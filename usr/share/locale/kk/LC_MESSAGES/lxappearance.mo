��    J      l  e   �      P      Q     r     �     �  �   �     X     l     �  
   �     �     �     �  A   �     �  (   	  9   2     l     z          �     �  
   �     �     �  
   �  
   �     �     �     �     �     	     	     !	     '	     -	     3	  +   E	  $   q	  "   �	  $   �	     �	     �	     �	     �	     
     
     )
     <
     Q
     a
     v
     �
     �
     �
     �
     �
     �
     �
  	   �
               #  	   3     =     Y     ^     c     j     x     ~     �     �     �  �  �  7   C     {     �     �  4  �       2        M     Q     X  !   k     �  w   �  $     H   7  h   �     �       .     &   >  
   e     p  
   �     �     �     �     �  .   �     #     0     L  "   [  
   ~     �     �  7   �  t   �  K   B  S   �  Q   �     4  #   8  
   \  !   g     �  .   �  *   �  4     $   8  8   ]  <   �     �  
   �  ,   �     +  ,   :  4   g  4   �     �      �  -     1   2  $   d  ?   �     �     �     �     �     �  	               4   )            B   #   "   9      6                /                  =          A   %      &   >   +       F          G           0   	   (         C       5      .   )       I      ?              
   '          <      1   7   2   ,              4   H       3   J             -      *   ;   @              E   :             $                    !       8                 D        *.tar.gz, *.tar.bz2 (Icon Theme) <b>Antialiasing</b> <b>GUI Options</b> <b>Hinting</b> <b>Note:</b> Not all of the desktop applications support changing cursor theme on-the-fly. So your changes here might not be fully applied to all applications till next login. <b>Sound Effect</b> <b>Sub-pixel geometry</b> BGR Background Bigger Check Button Color Color scheme is not supported by currently selected widget theme. Customize Look and Feel Customizes look and feel of your desktop Customizes look and feel of your desktop and applications Default font: Demo Enable antialiasing Enable hinting Font Foreground Full Hinting style:  Icon Theme Icons only Install Large toolbar icon Medium Mouse Cursor None Normal windows: Other Page1 Page2 Play event sounds Play event sounds as feedback to user input Preview of the selected cursor theme Preview of the selected icon theme Preview of the selected widget style RGB Radio Button Remove Same as buttons Same as dialogs Same as drag icons Same as menu items Select an icon theme Selected items: Show images in menus Show images on buttons Size of cursors Slight Small toolbar icon Smaller Sub-pixel geometry:  Text below icons Text beside icons Text only Text windows: Toolbar Icon Size:  Toolbar Style:  Tooltips: Use customized color scheme VBGR VRGB Widget Window Border _Edit _File _Help button translator-credits Project-Id-Version: master
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-10-14 18:23+0300
PO-Revision-Date: 2015-08-17 01:28+0000
Last-Translator: Anonymous Pootle User
Language-Team: Kazakh <kk_KZ@googlegroups.com>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1439774913.367300
 *.tar.gz, *.tar.bz2 (Таңбашалар темасы) <b>Антиалиасинг</b> <b>Интерфейс</b> <b>Тегістеу</b> <b>Ескерту:</b> Қолданбалардың барлығы курсор темасын ауыстыруды жұмыс істеу кезінде қолдай бермейді. Өзгерістер сіз жүйеге келесі рет кіргенше дейін іске аспауы мүмкін. <b>Дыбыстар</b> <b>Субпиксель геометриясы</b> BGR Фон Үлкенірек Белгілеу батымасы Түстер Ағымдағы таңдалған интерфейс темасы түстер схемасын қолдамайды. Сыртқы түрін баптау Жұмыс үстеліңіздің сыртқы түрін баптау Жұмыс үстелі мен қолданбаларыңыздың сыртқы түрін баптау Бастапқы қаріп: Демо Антиалиасингті іске қосу Тегістеуді іске қосу Қаріп Алдыңғы көрінісі Толық Тегістеу түрі: Таңбашалар Тек таңбашалар Орнату Үлкен панель таңбашалары Орташа Тышқан курсоры Ешнәрсе Қалыпты терезелер: Басқа Page1 Page2 Оқиғалар дыбыстарын іске қосу Пайдаланушы енгізуге жауап ретінде оқиғалар дыбыстарын ойнату Таңдалған курсор темасын алдын-ала қарау Таңдалған таңбашалар темасын алдын-ала қарау Таңдалған интерфейс темасын алдын-ала қарау RGB Ауыстыру батырмасы Өшіру Батырмалар сияқты Сұхбаттар сияқты Тарту таңбашалары сияқты Мәзір нәрселері сияқты Таңбашалар темасын таңдаңыз Таңдалған нәрселер: Мәзірлерде суреттерді көрсету Батырмаларда суреттерді көрсету Курсорлар өлшемі Әлсіз Кіші панель таңбашалары Кішірек Субпиксель геометриясы: Мәтін таңбашалардың астында Мәтін таңбашалардың қасында Тек мәтін Мәтін терезелері: Панель таңбашасы өлшемі: Саймандар панелінің стилі: Қалқымалы кеңестер: Бапталған түстер схемасын қолдану VBGR VRGB Виджет Терезе шегі Тү_зету _Файл _Көмек батырма Baurzhan Muftakhidinov <baurthefirst@gmail.com> 2012 
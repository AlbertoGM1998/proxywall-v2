��    Q      �  m   ,      �     �  !   �          #  #   7     [  
   b     m     ~     �  1   �     �     �     �  '   �     '     <     P  
   Y     d     t     {  
   �  	   �     �     �  2   �     �     	     		  	   	     	  
   	  	   )	     3	  	   A	     K	     i	     {	     �	     �	     �	     �	     �	     �	     
     
     0
     E
  $   V
     {
  "   �
     �
     �
     �
     �
               0     E     V     [     c  �   i  �   [     S     f     �  	   �     �     �     �     �     �  @   �  �        �     �     �     �  �  	     �  A   �  0   6  *   g  @   �  
   �     �  #   �          5  v   G  :   �     �  0      X   1  %   �  '   �     �     �     �       3   %     Y     q     �     �  `   �           %     ,     1     E     N     _  *   m     �  0   �  (   �  ,     *   2  ;   ]  &   �  (   �  &   �  (        9  (   Q  &   z     �  9   �     �  D     7   _  .   �  &   �     �  .     4   2  .   g  !   �  
   �     �  
   �  �  �  i  r  )   �  R        Y     ]     u  "   ~     �     �     �  s   �  *  0     [     y     �  4   �            >          .      @      
   8   G   I         <       $   /   !   	       +       -       M   ,       5      9      1   H   (             E       '   O   P                        *      #                      D             3      L   A                     =      7      2   "             ?   %          0   6   C       F       )       J   :      K   Q   B          N   ;              4       &    About zenity Activate directory-only selection Activate save mode Add an extra button Allow multiple files to be selected COLUMN C_alendar: Calendar options Calendar selection Cancel Confirm file selection if filename already exists Could not parse message
 DAY Display calendar dialog Display dialog boxes from shell scripts Display notification Enable HTML support FILENAME Field name General options HEIGHT Hide Cancel button Hide value ICON-NAME ICONPATH MONTH Maximum value must be greater than minimum value.
 NAME | PATTERN1 PATTERN2 ... NUMBER OK Open file PATTERN PERCENTAGE Password: Print version SEPARATOR Set dialog timeout in seconds Set initial value Set maximum value Set minimum value Set output separator character Set step size Set the calendar day Set the calendar month Set the calendar year Set the color Set the dialog text Set the dialog title Set the filename Set the format for the returned date Set the height Set the label of the Cancel button Set the label of the OK button Set the notification text Set the text font Set the width Set the window icon Show calendar options Show general options Show the palette TEXT TIMEOUT TITLE This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 This program is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 Type your password Type your username and password URL Username: VALUE Value out of range.
 WIDTH WINDOW YEAR You must specify a dialog type. See 'zenity --help' for details
 You should have received a copy of the GNU Lesser General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. Zenity notification _Cancel _OK translator-credits Project-Id-Version: zenity master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=zenity&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-10-04 01:12+0000
PO-Revision-Date: 2015-10-04 17:02+0500
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh <kk_KZ@googlegroups.com>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
 Zenity туралы Тек буманы таңдау режимін іске қосу Сақтау режимін белсендіру Қосымша батырманы қосу Бірнеше файлды таңдауды рұқсат ету БАҒАН Кү_нтізбе: Күнтізбе опциялары Күнтізбе таңдауы Бас тарту Файл аты бар болып тұрған кезде, файлды таңдаудың растауын сұрау Хабарламаны талдау мүмкін емес
 КҮН Күнтізбе сұхбатын көрсету Қоршам скриптерінен сұхбат терезелерін көрсету Хабарламаны көрсету HTML қолдауын іске қосу ФАЙЛ_АТЫ Өріс аты Жалпы опциялар БИІКТІГІ Бас тарту батырмасын жасыру Мәнін жасыру ТАҢБАША-АТЫ ТАҢБАШАЖОЛЫ АЙ Максималды мәні минималды мәнінен үлкен болуы тиіс.
 АТЫ | ҮЛГІ1 ҮЛГІ2 ... САН ОК Файлды ашу ҮЛГІ ПАЙЫЗДЫҚ Пароль: Нұсқа ақпаратын шығару АЖЫРАТҚЫШ Сұхбат уақытын орнату, сек Бастапқы мәнін орнату Максималды мәнін орнату Минималды мәнін орнату Шығыс ажыратқыш таңбасын орнату Қадам өлшемін орнату Күнтізбе күнін орнату Күнтізбе айын орнату Күнтізбе жылын орнату Түсті орнату Сұхбат мәтінін орнату Сұхбат атауын орнату Файл атын орнату Қайтарылған күн пішімін орнату Биіктігін орнату Бас тарту батырмасынын жазуын орнату ОК батырмасынын жазуын орнату Хабарлама мәтінін орнату Мәтін қарібін орнату Енін орнату Терезе таңбашасын орнату Күнтізбе опцияларын көрсету Жалпы опцияларды көрсету Палитраны көрсету МӘТІН ТАЙМАУТ АТАУЫ Бұл бағларлама пайдалы болады деген сеніммен таратылады, бірақ ЕШҚАНДАЙ КЕПІЛДЕМЕ берілмейді; КОММЕРЦИЯЛЫҚ ҚҰНДЫЛЫҚ немесе белгілі бір МАҚСАТТАРҒА СӘЙКЕС КЕЛЕТІНІ үшін де.  Көбірек білу үшін GNU Lesser General Public License қараңыз.
 Бұл бағдарлама - еркін бағдарлама қамтама; сіз оны Free Software Foundation шығарған GNU Lesser General Public License аясында еркін тарата не/және өзгерте аласыз; лицензия нұсқасы 2 не (тандауыңызша) кез-келген кейін шыққан.
 Пароліңізді енгізіңіз Пайдаланушы атын және пароліңізді енгізіңіз URL Пайдаланушы: МӘНІ Мәні ауқымнан тыс.
 ЕНІ ТЕРЕЗЕ ЖЫЛ Сұхбат түрін көрсету керек. Көбірек білу үшін 'zenity --help' қараңыз
 Сіз бұл бағдарламамен бірге GNU Lesser General Public License көшірмесін алуыңыз керек еді; олай болмаса, Free Software Foundation, Inc. ұйымына хабарласыңыз, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. Zenity хабарламасы Ба_с тарту О_К Baurzhan Muftakhidinov <baurthefirst@gmail.com> 2015 
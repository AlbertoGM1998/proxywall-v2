��    g      T  �   �      �  (   �     �     �     �     	     	  &   :	     a	     q	     v	     �	  :   �	     �	     �	     �	     �	  &   �	  "   %
  	   H
     R
  	   o
     y
  
   �
  '   �
     �
     �
  o   �
     F     V     f     x  $   �  "   �     �     �  "   �          ,     1     :     @     H  
   N     Y  	   f  	   p     z     �     �     �     �     �     �     �     �     �  -        >     J     O     U     Z     f     t  
   |     �  
   �     �     �     �     �     �     �     �     �     �          #  	   ,     6     B     P     a  %   m     �     �     �     �     �  
   �     �     �     �               $  
   +     6     >     F  
   O     Z  �  b  o   �     Y     p  /   �     �  >   �          �     �  +   �     �  �   �  !   �  .   �  .        M  b   c  k   �  '   2  Q   Z  $   �  1   �       �        �  C   �    �  )     )   6  1   `  :   �  g   �  {   5  =   �  S   �  �   C  .   �     �            '   (     P     ]  .   o     �     �  "   �     �  6     O   ?  -   �  .   �  4   �  U   !     w  2   �  �   �  1   [     �     �     �  "   �  +   �       (   +     T     a  (   |  8   �  4   �        7   /   !   g   3   �      �   4   �   *   !     6!     L!  '   j!  .   �!  7   �!  %   �!     "  	   �"  q   �"  7   #     S#  +   f#     �#  !   �#     �#  /   �#  1   "$  @   T$     �$  #   �$  )   �$  2   �$  H   %     f%     �%         [       9   :   \   2         I       c   3   7   K   !   O   J   b                  Y   (   _       8      /   =      e          U   L       M                    @         )   N   E       g       Q   &       C      ^   V   Z   D   1       R   	   0   P   X   $      
   S       %          "   >                   <                 H       `   *             A      B       G   d   a   +       F      W           '   #   4              ,   6              5   T           f   ?      ;   -   ]   .                   <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> A11y Advanced Options Application Applications automatically started after entering desktop: Apply Audio manager Audio player Authentication Authentication failed!
Wrong password? Automatically Started Applications Autostart Banner to show on the dialog Bittorent Burning utility Calculator Change the default applications on LXDE Charmap Clipboard manager Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Communication 1 Communication 2 Composite Manager Core applications Custom message to show on the dialog Default applications for LXSession Desktop Manager Desktop Session Settings Disable autostarted applications ? Disks utility Dock Document Email Enabled Error Error: %s
 File Manager Group: %s Identity: Image viewer Information LXPolKit LXSession configuration L_ock Screen Laptop mode Launcher manager Launching applications Layout Lock screen manager Manage applications loaded in desktop session Menu prefix Mode Model More Network GUI Notes utility Options PDF Reader Panel Password:  Polkit agent Position of the banner Power Manager Proxy Quit manager Reload S_witch User Screensaver Screenshot manager Security (keyring) Settings Sh_utdown Spreadsheet Tasks monitor Terminal manager Text editor The database is updating, please wait Type Update lxsession database Upgrade manager Variant Video player Webbrowser Webcam Widget 1 Window Manager: Windows Manager Workspace manager Xrandr _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-06-10 12:21+0000
Last-Translator: Praveen Illa <mail2ipn@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: te
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1528633283.780211
 <b><big>%s %s చర్యాకాలం నుండి నిష్క్రమించాలా?</big></b> <b>డీబస్</b> <b>ఆవరణం</b> <b>సాధారణ అమరికలు</b> <b>మీటలపటం</b> <b>తెలిసిన అనువర్తనాలు</b> <b>మానవీయంగా స్వయంగా ప్రారంభమయ్యే అనువర్తనాలు</b> <b>అమరికలు</b> A11y ఉన్నత ఐచ్ఛికాలు అనువర్తనం డెస్క్‌టాపులోకి ప్రవేశించిన తరువాత స్వయంచాలకంగా ప్రారంభమయ్యే అనువర్తనాలు: అనువర్తించు శ్రవ్య నిర్వాహకం శ్రవ్య ప్రదర్శకం ధృవీకరణ ధృవీకరణ విఫలమైంది!
తప్పుడు సంకేతపదం? స్వయంచాలకంగా ప్రారంభమయ్యే అనువర్తనాలు స్వీయప్రారంభం డైలాగు మీద చూపించుటకు బ్యానర్ బిట్‌టోరెంటు బర్నింగు యుటిలిటీ గణనయంత్రం ఎల్ఎక్స్‌డియి పై అప్రమేయ అనువర్తనాలను మార్చండి అక్షరపటం క్లిప్‌బోర్డ్ నిర్వాహకం కిటికీ నిర్వాహకాన్ని తెరుచుటకు ఉపయోగించే కమాండ్ లైన్
(ఎల్ఎక్స్ డియి అప్రమేయ కిటికీ నిర్వాహకం openbox-lxde) సమాచారవ్యవస్థ 1 సమాచారవ్యవస్థ 2 కూర్పుల నిర్వాహకం అంతర్భాగ అనువర్తనాలు డైలాగు మీద చూపించుటకు అనురూపిత సందేశం ఎల్ఎక్స్‌చర్యాకాలం కోసం అప్రమేయ అనువర్తనాలు డెస్క్‌టాప్ నిర్వాహకం డెస్క్‌టాప్ చర్యాకాలం అమరికలు స్వయంగా ప్రారంభమయ్యే అనువర్తనాలను అచేతనించాలా? డిస్కుల వినియోగం డాక్ పత్రం ఈమెయిలు చేతనించబడింది దోషం దోషం: %s
 దస్త్ర నిర్వాహకం సమూహం: %s గుర్తింపు: బొమ్మల చూపకం సమాచారం ఎల్ఎక్స్‌పోల్‌కిట్ ఎల్ఎక్స్‌చర్యాకాలం స్వరూపణం తెరకు తాళంవేయి (_o) ల్యాప్‌టాప్ రీతి ప్రారంభక నిర్వాహకం అనువర్తనాలను ప్రారంభిస్తున్నా నమూనా తెర తాళం నిర్వాహకం డెస్క్‌టాప్ చర్యాకాలంలో లోడయిన అనువర్తనాలను నిర్వహించండి జాబితా మునుజేర్పు రీతి మోడలు మరిన్ని నెట్‌వర్క్ GUI గమనికల వినియోగం ఐచ్ఛికాలు పిడియఫ్ చదువకం ఫలకం సంకేతపదం:  పోల్కిట్ వాహకం బ్యానర్ యొక్క స్థానం విద్యుత్ నిర్వాహకం ప్రతినిధి నిర్వాహకం నిష్క్రమణ తిరిగినింపు వాడుకరిని మార్చు (_w) తెరకాపరి తెరపట్టు నిర్వాహకం భద్రత (కీలకవలయం) అమరికలు మూసివేయి (_u) స్ప్రెడ్‌షీటు పనుల పర్యవేక్షకం టెర్మినల్ నిర్వాహకం పాఠ్య కూర్పకం దత్తకోశం నవీకరించబడుతోంది, దయచేసి వేచివుండండి రకం ఎల్ఎక్స్‌చర్యాకాలం దత్తకోశం నవీకరించండి ఉన్నతీకరణ నిర్వాహకం చరాంకం దృశ్య ప్రదర్శకం జాల విహారకం వెబ్‌క్యామ్ కీలుబొమ్మ 1 కిటికీ నిర్వాహకం: కిటికీల నిర్వాహకం కార్యక్షేత్ర నిర్వాహకం Xrandr సుప్తావస్థ (_H) నిష్క్రమించు (_L) పునఃప్రారంభించు (_R) తాత్కాలికంగా నిలిపివేయి (_S) బొమ్మ ఫైల్ సందేశం 
��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  |  u  	   �     �          (     .     4     :     ?     H     W     g     w     �     �     �     �     �     �     �               #     *     6     N     g     x     ~     �  J   �  :   �  :   .     i  	   o     y     �     �     �     �     �  
   �     �               #     C      b     �     �     �     �     �     �     �     
          '     :     A     W     ]     q     �     �     �     �  #   �     �     �     �                /     A  
   J     U     r     y  K   �     �     �     �  
   �     �     �       	             )  	   1     ;     B     O     S     Z  "   g  O   �     �                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-23 12:48+0000
Last-Translator: Sérgio Marques <smarquespt@gmail.com>
Language-Team: Portuguese
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495543695.828547
 Avançado Permitir letra a negrito Campainha audível Fundo Preto Bloco Azul Inferior Azul brilhante Ciano brilhante Verde brilhante Magenta brilhante Vermelho brilhante Castanho Limpar barra de desl_ocação Limpar barra de desl_ocação Fechar separador Nova janela Fechar _janela Confirmação Cop_iar Copiar Copiar _URL Copyright (C) 2008-2017 Intermitência do cursor Estilo do cursor Ciano Cinzento escuro Tamanho padrão da janela Desativar confirmação antes de fechar uma janela com vários separadores Desativar tecla de atalho de menu (por definição é F10) Desativar a utilização de Alt-n para separadores e menus Ecrã Principal Cinzento Verde Ocultar botão Fechar Ocultar barra de menu Ocultar ponteiro do rato Ocultar barra de deslocação LXTerminal Preferências do LxTerminal Esquerda Magenta Mover separador para a esquerda Mover separador para a direita Mover separador para a _esquerda Mover separador para a _direita No_mear separador Dar nome ao separador Separador _seguinte Novo _separador Novo separador Nova janela Novo _separador Nova _janela Separador seguinte Paleta Pré-ajuste da paleta Colar Separador _anterior Preferência_s Separador anterior Vermelho Direita Linhas de deslocação Caracteres de seleção por palavra Atalhos Estilo Posição do separador Terminal Emulador de terminal para o LXDE Letra do terminal Superior Sublinhado Utilizar a linha de comandos Branco Amarelo Está prestes a fechar %d separadores. Tem certeza de que deseja continuar? Ampliar Re_duzir Reduzir Repor zoom Ampl_iar _Repor zoom So_bre _Cancelar Fe_char separador _Editar _Ficheiro _Ajuda Nova _janela _OK Co_lar _Separadores consola;linha de comandos;executar Álvaro Morais <alvarommorais@gmail.com>
Sérgio Marques <smarquespt@gmail.com> x 
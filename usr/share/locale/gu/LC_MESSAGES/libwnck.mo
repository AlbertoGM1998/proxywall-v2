��    �      �  K  <      h     i     n  	   {     �     �     �     �     �  "   �     �     �     �     �          '     :     P     b     q     �     �  N   �  g   �  G   X  :   �  E   �  A   !  *   c  *   �  )   �  (   �  7     s   D  '   �  ?   �  <         ]  ,   n  $   �     �     �     �  /     8   6  \   o  b   �  d   /  ]   �  d   �  f   W  b   �  m   !  `   �     �  #        ,     J  "   ^  !   �  /   �     �     �     �     �  
     >     G   V  5   �     �     �  `      G   d   	   �      �      �       �   #   !  #   '!     K!  5   f!  !   �!  $   �!  '   �!  '   "  9   3"  $   m"  )   �"  5   �"      �"     #     '#  	   F#     P#     ^#  <   r#  <   �#  :   �#     '$     B$     [$     s$     �$     �$     �$  )   �$  ,   �$     %  	   %     %     )%     9%     P%  #   j%     �%     �%  %   �%  (   �%  (   &  +   4&     `&  )   i&     �&  �   �&     �'     �'     �'     �'     �'  *   �'  -   (  -   5(  0   c(     �(  2   �(     �(  (   �(  *   )     E)  
   U)     `)     y)  &   ~)     �)  !   �)     �)     �)     *     *  "   (*     K*      a*     �*     �*     �*     �*     �*     �*     �*     	+  L   $+  8   q+     �+     �+     �+     �+     �+     �+  E   ,     U,     b,     q,  :   ,     �,     �,     �,     �,  <   �,  .   6-     e-  	   i-     s-     u-     �-  
   �-     �-     �-     �-     �-     �-     �-     �-     �-     .     .     #.     +.     9.     G.  
   M.  
   X.  
   c.     n.     w.     �.  	   �.     �.     �.     �.  	   �.     �.     �.     �.     /     /     '/     +/     2/     9/     =/     C/  
   J/     U/     c/     q/     �/     �/     �/     �/     �/     �/     �/  
   �/     �/     �/  
   0     0     0     0     #0     20     >0  �  M0     2      2  	   -2     72     ?2     H2     Q2     T2  "   a2     �2     �2  -   �2  /   �2  8   �2  *   13  3   \3  "   �3  (   �3  B   �3  *   4     J4  �   W4  �   5  �   �5  �   �6  �   /7  �   �7  @   y8  @   �8  F   �8  J   B9  k   �9    �9  H   ;  �   X;  �   �;     y<  l   �<  }   =  ^   �=  E   �=  &   %>  \   L>  o   �>  �   ?  �   �?  �   �@  �   �A  �   �B  �   �C  �   �D    iE  �   uF  7   gG  `   �G  Z    H  ;   [H  Z   �H  A   �H  O   4I     �I     �I     �I     �I     �I  t   �I  �   mJ  j   �J  =   dK  !   �K  �   �K  �   �L  !   cM  .   �M  Z   �M  X   N  j   hN  X   �N  M   ,O  �   zO  g   �O  e   dP  w   �P  v   BQ  �   �Q  s   HR  o   �R  �   ,S  R   �S  5   T  R   BT  $   �T  1   �T  8   �T  ~   %U  ~   �U  �   #V  Q   �V  Q   W  W   SW  Q   �W  T   �W     RX     WX  �   dX  �   �X     |Y     �Y  U   �Y  9   �Y  0   "Z  <   SZ  Y   �Z  '   �Z  Q   [  m   d[  �   �[  p   V\  y   �\     A]  Q   J]  '   �]  �  �]  !   |_  !   �_  '   �_     �_  /   �_  �   -`  �   �`  �   ?a  �   �a  (   Jb  �   sb  G   �b  k   Ec  x   �c     *d     Bd  G   Zd     �d  [   �d  [   e  R   _e  $   �e  %   �e  P   �e  @   Nf  k   �f  N   �f  k   Jg  C   �g  Q   �g  9   Lh  =   �h  4   �h  &   �h  J    i  &   ki  �   �i  �   Ej  7   �j     k     $k  6   Dk  /   {k  *   �k  �   �k  /   �l  1   �l  4   �l  l   m  *   �m  -   �m  ?   �m      n  �   "n  �   �n     Lo  	   Po     Zo  ^   \o     �o  %   �o     �o  L   p     \p  M   xp  	   �p  (   �p     �p  J   q  (   Qq     zq     �q  "   �q  #   �q     �q  *   �q     )r     Cr     `r  3   }r  3   �r  %   �r  <   s  <   Hs     �s  (   �s     �s     �s  3   �s     2t  (   Ht  	   qt     {t     �t     �t     �t     �t     �t  .   u  "   4u     Wu     nu     �u     �u  (   �u     �u  &   �u  )   v  ;   8v  =   tv  =   �v  >   �v     /w      Mw     nw  +   {w     �w     �w     �   T      Q      j   �   �       �       �   �   X   �   �   L   �               �   A       >   �   r   .   �   �              �   �          �   %   $           �   �   �   {   �              �       l   �       ?       �       �   �   P   R   �   ^   �   �       �      �       y          �   �   5   3       �   �           1       Z      �   �   �   �   s   �       �           �   �   2           |   *   �   �   }          �   J   �      �   u   8       (      �   �   :   i   �   �   V   &           g   �   �           �   E   I   B   h   b       �   �   0   �       M   �   ;          c   G   �   e   �   �   p   v          �      �   	   �       �      �   �   Y   F   �   �   �       #      '      �   t   4       \   �   9   x   �   �   /   )           C                      �   @   �              �      n   �              o   U           -   S          D           _   �      d       �   �      �   �   K   7       H       �       �   �   �                   �       �       �       �       `           �   �       "      z   f       �           �   
   q   a           �      �   �   �   �   ~   �   �      �   �       �   m   �           �      �   W   k               �       <   �       6              [   �           ,   =   N   ]   w       �   �   �   O   !   +   �    "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 ,  <name unset> <no EWMH-compliant window manager> <no viewport> <unset> Action not allowed
 Activate the window Activate the workspace Active Window: %s
 Active Workspace: %s
 Alias of --window Always On _Top Always group windows Bottom Neighbor: %s
 CLASS Cannot change the workspace layout on the screen: the layout is already owned
 Cannot interact with application having its group leader with XID %lu: the application cannot be found
 Cannot interact with class group "%s": the class group cannot be found
 Cannot interact with screen %d: the screen does not exist
 Cannot interact with window with XID %lu: the window cannot be found
 Cannot interact with workspace %d: the workspace cannot be found
 Change the X coordinate of the window to X Change the Y coordinate of the window to Y Change the height of the window to HEIGHT Change the name of the workspace to NAME Change the number of workspaces of the screen to NUMBER Change the type of the window to TYPE (valid values: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Change the width of the window to WIDTH Change the workspace layout of the screen to use NUMBER columns Change the workspace layout of the screen to use NUMBER rows Class Group: %s
 Class resource of the class group to examine Click this to switch to workspace %s Click to start dragging "%s" Click to switch to "%s" Close the window Conflicting options are present: --%s and --%s
 Conflicting options are present: --%s or --%s, and --%s
 Conflicting options are present: a window should be interacted with, but --%s has been used
 Conflicting options are present: an application should be interacted with, but --%s has been used
 Conflicting options are present: class group "%s" should be interacted with, but --%s has been used
 Conflicting options are present: screen %d should be interacted with, but --%s has been used
 Conflicting options are present: windows of an application should be listed, but --%s has been used
 Conflicting options are present: windows of class group "%s" should be listed, but --%s has been used
 Conflicting options are present: windows of workspace %d should be listed, but --%s has been used
 Conflicting options are present: windows or workspaces of screen %d should be listed, but --%s has been used
 Conflicting options are present: workspace %d should be interacted with, but --%s has been used
 Current workspace: "%s" Display windows from all workspaces Don't show window in tasklist Enable Transparency Error while parsing arguments: %s
 Geometry (width, height): %d, %d
 Geometry (x, y, width, height): %d, %d, %d, %d
 Group Leader: %lu
 Group Name: %s
 HEIGHT Icon Name: %s
 Icons: %s
 Invalid argument "%d" for --%s: the argument must be positive
 Invalid argument "%d" for --%s: the argument must be strictly positive
 Invalid argument "%s" for --%s, valid values are: %s
 Invalid value "%s" for --%s Left Neighbor: %s
 List windows of the application/class group/workspace/screen (output format: "XID: Window Name") List workspaces of the screen (output format: "Number: Workspace Name") Ma_ximize Ma_ximize All Make the window always on top Make the window appear in pagers Make the window appear in tasklists Make the window below other windows Make the window fullscreen Make the window have a fixed position in the viewport Make the window not always on top Make the window not appear in pagers Make the window not appear in tasklists Make the window not below other windows Make the window not have a fixed position in the viewport Make the window quit fullscreen mode Make the window visible on all workspaces Make the window visible on the current workspace only Maximize horizontally the window Maximize the window Maximize vertically the window Mi_nimize Mi_nimize All Minimize the window Move the viewport of the current workspace to X coordinate X Move the viewport of the current workspace to Y coordinate Y Move the window to workspace NUMBER (first workspace is 0) Move to Another _Workspace Move to Workspace R_ight Move to Workspace _Down Move to Workspace _Left Move to Workspace _Up NAME NUMBER NUMBER of the screen to examine or modify NUMBER of the workspace to examine or modify N_ROWS Name: %s
 Never group windows No Windows Open Number of Windows: %d
 Number of Workspaces: %d
 On Screen: %d (Window Manager: %s)
 On Workspace: %s
 Only show current workspace Options to list windows or workspaces Options to modify properties of a screen Options to modify properties of a window Options to modify properties of a workspace PID: %s
 Position in Layout (row, column): %d, %d
 Possible Actions: %s
 Print or modify the properties of a screen/workspace/window, or interact with it, following the EWMH specification.
For information about this specification, see:
	http://freedesktop.org/wiki/Specifications/wm-spec Resource Class: %s
 Right Neighbor: %s
 Screen Number: %d
 Session ID: %s
 Shade the window Show options to list windows or workspaces Show options to modify properties of a screen Show options to modify properties of a window Show options to modify properties of a workspace Show the desktop Show workspace names instead of workspace contents Showing the desktop: %s
 Start moving the window via the keyboard Start resizing the window via the keyboard Startup ID: %s
 State: %s
 Stop showing the desktop TYPE Tool to switch between visible windows Tool to switch between windows Tool to switch between workspaces Top Neighbor: %s
 Transient for: %lu
 Un_minimize All Unma_ximize Unmaximize horizontally the window Unmaximize the window Unmaximize vertically the window Unmi_nimize Unminimize the window Unshade the window Untitled application Untitled window Use N_ROWS rows Use RTL as default direction Use a vertical orientation Viewport cannot be moved: the current workspace does not contain a viewport
 Viewport cannot be moved: there is no current workspace
 Viewport position (x, y): %s
 WIDTH Window List Window Manager: %s
 Window Selector Window Type: %s
 Window cannot be moved to workspace %d: the workspace does not exist
 Workspace %d Workspace %s%d Workspace 1_0 Workspace Layout (rows, columns, orientation): %d, %d, %s
 Workspace Name: %s
 Workspace Number: %d
 Workspace Switcher X X window ID of the group leader of an application to examine X window ID of the window to examine or modify XID XID: %lu
 Y _Always on Visible Workspace _Close _Close All _Move _Only on This Workspace _Resize _Unmaximize All above all workspaces below change fullscreen mode change workspace close desktop dialog window dock or panel false fullscreen make above make below maximize maximize horizontally maximize vertically maximized maximized horizontally maximized vertically minimize minimized move needs attention no action possible normal normal window pin pinned resize set shade shaded skip pager skip tasklist splash screen startupIDnone stick sticky tearoff menu tearoff toolbar true unmake above unmake below unmaximize unmaximize horizontally unmaximize vertically unminimize unpin unshade unstick utility window windownone workspacenone Project-Id-Version: libwnck.HEAD.gu
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libwnck&component=general
PO-Revision-Date: 2009-03-15 11:42+0530
Last-Translator: Ankit Patel <ankit@redhat.com>
Language-Team: Gujarati <fedora-trans-gu@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
<magnet@magnet-i.com>
X-Generator: KBabel 1.11.4
Plural-Forms:  nplurals=2; plural=(n!=1);







 "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 ,  <name unset> <no EWMH-compliant window manager> <no viewport> <unset> ક્રિયા માન્ય નથી
 વિન્ડો સક્રિય કરો કાર્યસ્થળ સક્રિય કરો સક્રિય વિન્ડો: %s
 સક્રિય કાર્યસ્થળ: %s
 --window નું ઉપનામ હંમેશા ટોચ પર (_T) હંમેશા વિન્ડોનું જૂથ કરો તળિયેનો પડોશી: %s
 વર્ગ સ્ક્રીન પર કાર્યસ્થળ લેઆઉટ બદલી શકતા નથી: લેઆઉટ પહેલાથી જ માલિકીનું છે
 XID %lu સાથેના જૂથ નેતા ધરાવતા કાર્યક્રમ સાથે સંપર્ક કરી શકતા નથી: કાર્યક્રમ શોધી શકતા નથી
 વર્ગ જૂથ "%s" સાથે સંપર્ક કરી શકતા નથી: વર્ગ જૂથ શોધી શકતા નથી
 સ્ક્રીન %d સાથે સંપર્ક કરી શકતા નથી: સ્ક્રીન અસ્તિત્વમાં નથી
 XID %lu સાથેની વિન્ડો સાથે સંપર્ક કરી શકતા નથી: વિન્ડો શોધી શકતા નથી
 કાર્યસ્થળ %d સાથે સંપર્ક કરી શકતા નથી: કાર્યસ્થળ શોધી શકતા નથી
 વિન્ડોનો X અક્ષ X માં બદલો વિન્ડોનો Y અક્ષ Y માં બદલો વિન્ડોની ઊંચાઈ HEIGHT માં બદલો કાર્યસ્થળનું નામ NAME માં બદલો સ્ક્રીનના કાર્યસ્થળોનો નંબર NUMBER માં બદલો વિન્ડોનો પ્રકાર TYPE માં બદલો (માન્ય કિંમતો: સામાન્ય, ડેસ્કટોપ, ડોક, સંવાદ, સાધનપટ્ટી, મેનુ, ઉપયોગીતા, સ્પ્લેશ) વિન્ડોની પહોળાઈ WIDTH માં બદલો સ્ક્રીનનું કાર્યસ્થળ લેઆઉટ NUMBER સ્તંભો વાપરવા માટે બદલો સ્ક્રીનનું કાર્યસ્થળ લેઆઉટ NUMBER હરોળો વાપરવા માટે બદલો વર્ગ જૂથ: %s
 પરીક્ષણ કરવા માટે વર્ગ જૂથનું વર્ગ સ્રોત કામ કરવાની જગ્યા %s માં બદલવા માટે અહિં ક્લિક કરો "%s" ખેંચવાનું શરૂ કરવા માટે ક્લિક કરો "%s" માં બદલવા માટે ક્લિક કરો વિન્ડો બંધ કરો તકરાર કરતા વિકલ્પો હાજર છે: --%s અને --%s
 તકરાર કરતા વિકલ્પો હાજર છે: --%s અથવા --%s, અને --%s
 તકરાર કરતા વિકલ્પો હાજર છે: વિન્ડો આની સાથે સંપર્કમાં આવવી જોઈએ, પરંતુ  --%s વપરાઈ ગયેલ છે
 તકરાર કરતા વિકલ્પો હાજર છે: કાર્યક્રમ આની સાથે સંપર્કમાં આવવો જોઈએ, પરંતુ %s વપરાઈ ગયેલ છે
 તકરાર કરતા વિકલ્પો હાજર છે: વર્ગ જૂથ "%s" આની સાથે સંપર્કમાં આવવું જોઈએ, પરંતુ --%s વપરાઈ ગયેલ છે
 તકરાર કરતા વિકલ્પો હાજર છે: સ્ક્રીન %d આની સાથે સંપર્કમાં આવવી જોઈએ, પરંતુ --%s વપરાઈ ગયેલ છે
 તકરાર કરતા વિકલ્પો હાજર છે: કાર્યક્રમની વિન્ડોની યાદી થવી જોઈએ, પરંતુ --%s વપરાઈ ગયેલ છે
 તકરાર કરતા વિકલ્પો હાજર છે: જૂથ "%s" વર્ગની વિન્ડોની યાદી થવી જોઈએ, પરંતુ --%s વપરાઈ ગયેલ છે
 તકરાર કરતા વિકલ્પો હાજર છે: કાર્યસ્થળ %d ની વિન્ડોની યાદી થવી જોઈએ, પરંતુ --%s વપરાઈ ગયેલ છે
 તકરાર કરતા વિકલ્પો હાજર છે: સ્ક્રીન %d ની વિન્ડો અથવા કાર્યસ્થળોની યાદી થવી જોઈએ, પરંતુ --%s વપરાઈ ગયેલ છે
 તકરાર કરતા વિકલ્પો હાજર છે: કાર્યસ્થળ %d આની સાથે સંપર્કમાં આવવું જોઈએ, પરંતુ --%s વપરાઈ ગયેલ છે
 વર્તમાન કાર્યસ્થળ: "%s" બધા કાર્યસ્થળોમાંથી વિન્ડો દર્શાવો ક્રિયાયાદીમાં વિન્ડો બતાવશો નહિં પારદર્શકતા સક્રિય કરો દલીલોનું પદચ્છેદન કરતી વખતે ભૂલ: %s
 ભૂમિતી (પહોળાઈ, ઊંચાઈ): %d, %d
 ભૂમિતિ (x, y, પહોળાઈ, ઊંચાઈ): %d, %d, %d, %d
 જૂથ નેતા: %lu
 જૂથ નામ: %s
 HEIGHT ચિહ્ન નામ: %s
 ચિહ્નો: %s
 અયોગ્ય દલીલ "%d" એ --%s માટે: દલીલ સારી હોવી જ જોઈએ
 અયોગ્ય દલીલ "%d" એ --%s માટે: દલીલ સખત રીતે સારી હોવી જ જોઈએ
 અયોગ્ય દલીલ "%s" એ --%s માટે, માન્ય કિંમતો છે: %s
 અયોગ્ય કિંમત "%s" એ --%s માટે ડાબો પડોશી: %s
 કાર્યક્રમ/વર્ગ જૂથ/કાર્યસ્થળ/સ્ક્રીનની વિન્ડોની યાદી આપો (આઉટપુટ બંધારણ: "XID: વિન્ડો નામ") સ્ક્રીનના કાર્યસ્થળોની યાદી આપો (આઉટપુટ બંધારણ: "નંબર: કાર્યસ્થળ નામ") મહત્તમ કરો (_x) બધું મહત્તમ કરો (_x) વિન્ડોને હંમેશા ટોચ પર રહે એમ રાખો વિન્ડોને પેજરોમાં દેખાય એમ બનાવો વિન્ડોને ક્રિયાયાદીઓમાં દેખાય એમ બનાવો વિન્ડોને અન્ય વિન્ડોની નીચે રાખો વિન્ડો સંપૂર્ણસ્ક્રીન બનાવો વિન્ડોને વ્યુપોર્ટમાં ચોક્કસ સ્થાન હોય એમ બનાવો વિન્ડોને હંમેશા ટોચ પર રહે નહિં એમ રાખો વિન્ડોને પેજરોમાં નહિં દેખાય એમ બનાવો વિન્ડોને ક્રિયાયાદીઓમાં નહિં દેખાય એમ બનાવો વિન્ડોને અન્ય વિન્ડોની નીચે નહિં રહે એમ રાખો વિન્ડોને વ્યુપોર્ટમાં ચોક્કસ સ્થાન નહિં હોય એમ બનાવો વિન્ડો સંપૂર્ણસ્ક્રીન સ્થિતિમાંથી બંધ કરો વિન્ડોને બધા કાર્યસ્થળો પર દેખાય એમ બનાવો વિન્ડોને માત્ર વર્તમાન કાર્યસ્થળ પર જ દેખાય એમ બનાવો વિન્ડોને આડી રીતે મહત્તમ બનાવો વિન્ડો મહત્તમ બનાવો વિન્ડોને ઊભી રીતે મહત્તમ બનાવો ન્યૂનતમ કરો (_n) બધું ન્યૂનતમ કરો (_n) વિન્ડો ન્યૂનતમ બનાવો વર્તમાન કાર્યસ્થળનો વ્યુપોર્ટ X અક્ષ X આગળ ખસેડો વર્તમાન કાર્યસ્થળનો વ્યુપોર્ટ Y અક્ષ Y માં ખસેડો વિન્ડોને કાર્યસ્થળ NUMBER પર ખસેડો (પ્રથમ કાર્યસ્થળ ૦ છે) બીજી કામ કરવાની જગ્યામાં ખસો (_W) જમણી કામ કરવાની જગ્યામાં ખસો (_i) નીચેની કામ કરવાની જગ્યામાં ખસો (_D) ડાબી કામ કરવાની જગ્યામાં ખસો (_L) ઉપરની કામ કરવાની જગ્યામાં ખસો (_U) NAME નંબર પરીક્ષણ કરવા માટે કે સુધારવા માટે સ્ક્રીનની સંખ્યા પરીક્ષણ કરવા માટે કે સુધારવા માટે કાર્યસ્થળની સંખ્યા N_ROWS નામ: %s
 ક્યારેય વિન્ડોનું જૂથ કરશો નહિં કોઈ વિન્ડો ખૂલેલી નથી વિન્ડોની સંખ્યા: %d
 કાર્યસ્થળોની સંખ્યા: %d
 સ્ક્રીન પર: %d (વિન્ડો વ્યવસ્થાપક: %s)
 કાર્યસ્થળ પર: %s
 માત્ર વર્તમાન કાર્યસ્થળ બતાવો વિન્ડો અથવા કાર્યસ્થળોની યાદીના વિકલ્પો સ્ક્રીનના ગુણધર્મો સુધારવા માટેના વિકલ્પો બતાવો વિન્ડોના ગુણધર્મો સુધારવા માટેના વિકલ્પો કાર્યસ્થળના ગુણધર્મો સુધારવા માટેના વિકલ્પો PID: %s
 લેઆઉટમાં સ્થાન (હરોળ, સ્તંભ): %d, %d
 શક્ય ક્રિયાઓ: %s
 સ્ક્રીન/કાર્યસ્થળ/વિન્ડોના ગુણધર્મો છાપો અથવા સુધારો, અથવા તેની સાથે સંપર્ક કરો, EWMH સ્પષ્ટીકરણને અનુસરીને.
આ સ્પષ્ટીકરણ વિશે વધુ જાણકારી માટે, જુઓ:
	http://freedesktop.org/wiki/Specifications/wm-spec સ્રોત વર્ગ: %s
 જમણો પડોશી: %s
 સ્ક્રીન નંબર: %d
 સત્ર ID: %s
 વિન્ડોને છાયા આપો વિન્ડો અથવા કાર્યસ્થળોની યાદી બતાવવા માટેના વિકલ્પો સ્ક્રીનના ગુણધર્મો સુધારવા માટેના વિકલ્પો બતાવો વિન્ડોના ગુણધર્મો સુધારવા માટેના વિકલ્પો બતાવો કાર્યસ્થળના ગુણધર્મો સુધારવા માટેના વિકલ્પો બતાવો ડેસ્કટોપ બતાવો કાર્યસ્થળ સમાવિષ્ટોની જગ્યાએ કાર્યસ્થળ નામો બતાવો ડેસ્કટોપ બતાવી રહ્યા છીએ: %s
 વિન્ડો કીબોર્ડ મારફતે ખસેડવાનું શરૂ કરો વિન્ડોને કીબોર્ડ મારફતે માપ બદલવાનું શરૂ કરો શરૂઆત ID: %s
 સ્થિતિ: %s
 ડેસ્કટોપ બતાવવાનું અટકાવો TYPE દેખીતી વિન્ડો વચ્ચે બદલવાનું સાધન વિન્ડો વચ્ચે ફેરબદલી કરવાનું સાધન કામ કરવાની જગ્યા બદલવાનું સાધન ટોચનો પડોશી: %s
 માટે પરિવહન: %lu
 બધું ન્યૂનતમમાંથી પાછુ લાવો (_m) મહત્તમમાંથી પાછુ લાવો (_x) વિન્ડોને આડી રીતે મહત્તમમાંથી પાછી લાવો વિન્ડો મહત્તમમાંથી પાછી લાવો વિન્ડોને ઊભી રીતે મહત્તમમાંથી પાછી લાવો ન્યૂનતમમાંથી પાછુ લાવો (_n) વિન્ડો ન્યૂનતમમાંથી પાછી લાવો વિન્ડોની છાયા દૂર કરો શીર્ષકવીહિન કાર્યક્રમ શીર્ષકવીહિન વિન્ડો N_ROWS હરોળો વાપરો RTL ને મૂળભૂત દિશા તરીકે વાપરો ઊભી દિશા વાપરો વ્યુપોર્ટ ખસેડી શકતા નથી: વર્તમાન કાર્યસ્થળ વ્યુપોર્ટ સમાવતું નથી
 વ્યુપોર્ટ ખસેડી શકાતું નથી: ત્યાં કોઈ વર્તમાન કાર્યસ્થળ નથી
 વ્યુપોર્ટ સ્થાન (x, y): %s
 WIDTH વિન્ડો યાદી વિન્ડો વ્યવસ્થાપક: %s
 વિન્ડો પસંદ કરનાર વિન્ડો પ્રકાર: %s
 વિન્ડો કાર્યસ્થળ %d માં ખસેડી શકતા નથી: કાર્યસ્થળ અસ્તિત્વમાં નથી
 કામ કરવાની જગ્યા %d કામ કરવાની જગ્યા %s%d કામ કરવાની જગ્યા ૧_૦ કાર્યસ્થળ લેઆઉટ (હરોળો, સ્તંભો, દિશા): %d, %d, %s
 કાર્યસ્થળ નામ: %s
 કાર્યસ્થળ નંબર: %d
 કામ કરવાની જગ્યા બદલનાર X કાર્યક્રમનું પરીક્ષણ કરવા માટે તેના દલનેતાનું X વિન્ડો ID પરીક્ષણ કરવા માટે અથવા સુધારવા માટે વિન્ડોનું X વિન્ડો ID XID XID: %lu
 Y હંમેશા દેખીતી કામ કરવાની જગ્યા પર (_A) બંધ કરો (_C) બધું બંધ કરો (_C) ખસેડો (_M) માત્ર આ કામ કરવાની જગ્યા પર (_O) માપ બદલો (_R) બધું મહત્તમમાંથી પાછુ લાવો (_U) ઉપર બધા કાર્યસ્થળો નીચે સંપૂર્ણસ્ક્રીન સ્થિતિ બદલો કાર્યસ્થળ બદલો બંધ કરો ડેસ્કટોપ સંવાદ વિન્ડો ડોક અથવા પેનલ ખોટું સંપૂર્ણસ્ક્રીન ઉપર બનાવો નીચે બનાવો મહત્તમ કરો આડી રીતે મહત્તમ કરો ઊભી રીતે મહત્તમ કરો મહત્તમ કરાયેલ આડી રીતે મહત્તમ કરાયેલ ઊભી રીતે મહત્તમ કરાયેલ ન્યૂનતમ કરો ન્યૂનતમ કરાયેલ ખસેડો ધ્યાન જરૂરી કોઈ ક્રિયા શક્ય નથી સામાન્ય સામાન્ય વિન્ડો પીન પીન થયેલ માપ બદલો સુયોજીત છાયા છાયાવાળું પેજર અવગણો ક્રિયાયાદી અવગણો ઝબૂક સ્ક્રીન કંઈ નહિં ચોંટાડો ચોંટેલ મેનુ કાપો સાધનપટ્ટી કાપો ખરું ઉપર નહિં બનાવો નીચે નહિં બનાવો મહત્તમમાંથી પાછુ લાવો આડી રીતે મહત્તમ દૂર કરો ઊભી રીતે મહત્તમ દૂર કરો ન્યૂનતમમાંથી પાછુ લાવો પીન દૂર કરો છાયા દૂર કરો છોડો ઉપયોગીતા વિન્ડો કંઈ નહિં કંઈ નહિં 
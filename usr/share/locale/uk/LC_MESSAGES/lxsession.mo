��    \      �     �      �  (   �               !     9     G  &   b     �  �   �     )	     :	  :   F	     �	     �	     �	     �	  &   �	  "   �	  	   �	     �	     
  	   3
     =
  
   M
     X
  '   _
     �
     �
  o   �
          !     1  $   C  "   h     �     �     �  "   �     �     �     �     �  
   �       	     	        (     5     A     J     W     c     t     �  -   �     �     �     �     �     �     �  
   �     �  
             .     E     S     Y     `     m     y     �  	   �     �     �     �     �     �     �     �     �     �            
        #     +     3  
   <     G  �  O  1        G     S  0   k     �  $   �  N   �     ,  �   F  +   6     b  g   s     �  
   �     �       R   5  F   �     �  !   �  F     	   M  @   W     �     �  :   �     �  .     �   F     �       '     O   D  .   �  #   �  ?   �     '  J   8     �     �     �     �     �     �     �       '         H     ]  "   f     �     �     �     �  ]   �     X     r  
   �     �     �     �     �     �     �  +   �  %   #  %   I     o     |  &   �     �  ,   �     �       #   *  #   N     r  )   y     �     �     �     �     �  !        1     8     T     n     �     �     �     9       2   B   Q          5              ,          &   J          P      =           A         *      \   D   )           8   L   4          $   3   U   W           #       >   .   Y             I           N           %   F   @       K   6           -      ;   7   ?   <   +             V   (                    :                       /   0      O   C   Z       "   G      1      !   E       X           
   T   '                 [      S   R              M   H       	    <b><big>Logout %s %s session ?</big></b> <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. Advanced Options Application Applications automatically started after entering desktop: Apply Archive Audio player Authentication Authentication failed!
Wrong password? Automatically Started Applications Autostart Available applications Banner to show on the dialog Bittorent Burning utility Calculator Cancel Change the default applications on LXDE Charmap Clipboard manager Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Communication 1 Communication 2 Composite Manager Custom message to show on the dialog Default applications for LXSession Desktop Manager Desktop Session Settings Disable Disable autostarted applications ? Document Email Enabled Error Error: %s
 File Manager Group: %s Identity: Image viewer Information LXPolKit L_ock Screen Laptop mode Launcher manager Launching applications Layout Manage applications loaded in desktop session Manual setting Menu prefix Mode Model More Options PDF Reader Panel Password:  Policykit Authentication Agent Position of the banner Power Manager Proxy Reload S_witch User Screensaver Screenshot manager Settings Sh_utdown Spreadsheet Text editor Type Upgrade manager Variant Video player Webcam Widget 1 Window Manager: Windows Manager Xrandr _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-22 22:13+0000
Last-Translator: Yarema aka Knedlyk <yupadmin@gmail.com>
Language-Team: Ukrainian
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495491198.566601
 <b><big>Вийти з %s %s сесії?</big></b> <b>Dbus</b> <b>Оточення</b> <b>Загальні налаштування</b> <b>Клавіатура</b> <b>Відомі програми</b> <b>Обрані програми автоматичного запуску</b> <b>Параметри</b> <b>Попередження:</b> <b>НЕ</b> чіпайте це, якщо Ви не певні своїх дій.

<b>ПРИМІТКА:</b> Це налаштування застосується після наступного входу. Розширені налаштування Програма Програми, що автоматично запускаються при вході в сесію: Застосувати Архів Аудіопрогравач Ідентифікація Авторизація не вдалася!
Неправильний пароль? Програми, що автоматично запускаються Автозапуск Доступні програми Банер для показу під час цього діалогу Bittorent Знаряддя для запису компакт-дисків Калькулятор Скасувати Виберіть типові програми для LXDE Мапа символів Керування буфером обміну Командна стрічка для запуску менеджера вікон
(Типовою командою для запуску LXDE повинна бути openbox-lxde) Зв'язок 1 Зв'язок 2 Композитний менеджер Приватне повідомлення для показу в діалозі Типові програми для LXSession Менеджер стільниці Налаштування сесії робочого столу Вимкнути Вимкнути запущені автоматично програми? Документ Ел. пошта Включено Помилка Помилка: %s
 Менеджер файлів Група: %s Ідентичність: Переглядач зображень Інформація LXPolKit З_аблокувати екран Режим ноутбука Менеджер запуску Запуск програм Компонування Керування програмами, що завантажені під час сесії Задати вручну Префікс меню Режим Модель Більше Параметри Переглядач PDF Панель Пароль:  Агент авторизації Policykit Розташування банера Керування живленням Проксі Оновити _Змінити користувача Зберігач екрана Менеджер знімків екрану Налаштування Вик_лючити Електронні таблиці Текстовий редактор Тип Керування оновленнями Варіант Відеопрогравач Веб-камера Віджет 1 Менеджер вікон: Віконний менеджер Xrandr _В стан сплячки _Вийти з сесії _Перезапустити П_ризупинити файл зображення повідомлення 
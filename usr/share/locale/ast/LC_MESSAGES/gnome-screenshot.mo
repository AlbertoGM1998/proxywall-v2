��    ;      �  O   �           	               !     /  N   B     �  4   �  ]   �     0     8     T     r  *   �  7   �  u   �     b     {  b   �     �               )  <   :  %   w     �  -   �  ;   �     "	     <	  ,   A	     n	  1   ~	     �	  
   �	     �	     �	     �	     
     !
     2
     A
     V
  '   f
     �
     �
     �
  '   �
  %   �
  2     /   M  ;   }  g   �  1   !  '   S     {     �     �  {  �                     (     ;  K   Q     �  :   �  n   �     U  "   ]  '   �      �  3   �  1   �  m   /  #   �      �  u   �     X     i     }     �  E   �  *   �  "     7   4  I   l  $   �     �  9   �       :   .     i     ~     �     �     �     �     �  	   �     �       -   3     a     o     ~  2   �  "   �  D   �  -   7  @   e  u   �  =     )   Z     �     �     �        	   #         ;       +          7      (   )                           0             6       2                1       /                 $   9          :   &                       8   -       !   "   '                        
   .   %       5         *       4      3       ,       * Apply _effect: Border Border Effect C_opy to Clipboard Conflicting options: --window and --area should not be used at the same time.
 Drop shadow Effect to add to the border (shadow, border or none) Effect to add to the outside of a border. Possible values are "shadow", "none", and "border". Effects Error loading the help page Error while saving screenshot Grab _after a delay of Grab a window instead of the entire screen Grab an area of the screen instead of the entire screen Grab just the current window, rather than the whole desktop. This key has been deprecated and it is no longer in use. Grab the current _window Grab the whole _desktop Impossible to save the screenshot to %s.
 Error was %s.
 Please choose another location and retry. Include Border Include ICC Profile Include Pointer Include _pointer Include the ICC profile of the target in the screenshot file Include the pointer in the screenshot Include the window _border Include the window border with the screenshot Include the window manager border along with the screenshot Interactively set options None Remove the window border from the screenshot Save Screenshot Save images of your desktop or individual windows Save in _folder: Screenshot Screenshot at %s - %d.png Screenshot at %s.png Screenshot delay Screenshot directory Screenshot taken Screenshot.png Select _area to grab Select a folder Send the grab directly to the clipboard Take Screenshot Take _Screenshot Take a picture of the screen Take a screenshot of the current window Take a screenshot of the whole screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. The number of seconds to wait before taking the screenshot. UI definition file for the screenshot program is missing.
Please check your installation of gnome-utils Unable to take a screenshot of the current window Window-specific screenshot (deprecated) _Name: effect seconds Project-Id-Version: gnome-utils
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-09-14 17:03+0200
Last-Translator: Xandru Armesto <xandru@softastur.org>
Language-Team: Softastur <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Virtaal 0.5.2
 * Aplicar e_feutu: Berbesu Efeutu del berbesu C_opiar al cartafueyu Opciones con conflictos: --window y --area nun tendríen d'usase al empar.
 Solombra base Efeutu p'amestar al berbersu (solombra, berbesu o dengún) Efeutu p'amestar al contornu exterior d'un berbesu. Los valores dables son "solombra", "dengún", y "berbesu". Efeutos Fallu al cargar la páxina d'ayuda Fallu al atroxar la captura de pantalla Facer captura con un _retardu de Capturar una ventana n'arróu de la pantalla entera Capturar un área en llugar de la pantalla entera Capturar namái la ventana actual n'arróu del escritoriu enteru. Esta clave ta obsoleta y nun s'usará más. Facer captura de la _ventana actual Facer captura de tol _escritoriu Nun pudo guardase la captura de pantalla en %s.
 L'error foi %s.
 Escueya otra llocalización y téntelo otra vegada. Incluyir Berbesu Incluyir perfil ICC Incluyir punteru Incluyir _punteru Incluyir el perfil ICC del oxetivu nel ficheru de captura de pantalla Incluyir el punteru na captura de pantalla Incluyir el berbesu de la _ventana Incluyir el berbesu de ventana cola captura de pantalla Incluyir el berbesu del xestor de ventanes xunto cola captura de pantalla Afitar les opciones interactivamente Dengún Quitar el berbesu de la ventana de la captura de pantalla Guardar la imaxe Guarda imáxenes del to escritoriu o ventanes individuales Guardar na _carpeta: Captura pantalla Captura de %s - %d.png Captura de %s.png Retardu de la captura Direutoriu de captures Captura tomada imaxe.png Seleicio_nar área a capturar Seleiciona una carpeta Unviar direutamente lo obtenío al cartafueyu Facer Captura Facer _captura Capturar imaxe de la pantalla Tomar una captura de pantalla de la ventana actual Tomar una captura de tola pantalla Capturar la pantalla dempués del retardu especificáu [en segundos] El direutoriu au s'atroxó la cabera captura. El númberu de segundos qu'esperar enantes d'obtener la captura. Falta'l ficheru de definición de la IU pal programa de captura de pantalla.
Revise la so instalación de gnome-utils Nun se pue tomar una captura de pantalla de la ventana actual Captura específica de ventana (obsoleto) _Nome: efeutu segundos 
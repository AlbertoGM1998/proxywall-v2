��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u          -     B     T     \     d     m     t     y     �     �     �     �     �     �     �                    -     @  
   L     W     g     �     �     �     �     �  9   �  6     -   H     v  	   ~     �     �     �     �     �     �  
   �  	   �       
             .     F     ^     w     �     �     �     �  
   �     �     �     �     �     �               -     9  	   K     U     \     m     �     �     �  	   �  %   �     �     �               -     4  ;   <     x  	   �     �     �     �     �     �     �     �     �                    '     +     4  )   ;  .   e     �                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal 0.1.7
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-07-20 19:55+0000
Last-Translator: Pavel Fric <pavelfric@seznam.cz>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1500580521.271817
 Rozšířené Povolit tučný font Akustický zvonek Pozadí Černá Blokovat Modrá Dole Světlemodrá Světlemodrozelená Světlezelená Světlepurpurová Světlečervená Hnědá Vy_čistit rolování Vyčistit ro_lování Zavřít kartu Zavřít okno Zavřít _okno Potvrdit zavření Kopíro_vat Kopírovat Kopírovat _URL Autorské právo (C) 2008-2017 Blikání kurzoru Styl kurzoru Modrozelená Tmavěšedá Výchozí velikost okna Zakázat potvrzení před zavřením okna s více kartami Zakázat klávesovou zkratku pro zobrazení menu (F10) Zakázat použití Alt-n pro záložky a menu Displej Popředí Šedá Zelená Skrýt zavírací tlačítka Skrýt panel nabídky Skrýt ukazatel myši Skrýt posuvník LXTerminal Možnosti Vlevo Purpurová Přesunout kartu vlevo Přesunout kartu vpravo Přesunout kartu v_levo Přesunout kartu vp_ravo Poj_menovat kartu Pojmenovat kartu Da_lší karta Nová kar_ta Nová karta Nové okno Nová kar_ta Nové _okno Další karta Paleta Přednastavení palety Vložit Před_chozí karta Na_stavení Předchozí karta Červená Vpravo Paměť řádků Znaky pro výběr po slovech Klávesové zkratky Styl Pozice panelu karet Terminál Emulátor terminálu pro projekt LXDE Písmo terminálu Nahoře Podtržené Používat příkazový řádek Bílá Žlutá Chystáte se zavřít %d karet. Opravdu chcete pokračovat? Přiblížit _Oddálit Oddálit Výchozí přiblížení _Přiblížit _Výchozí přiblížení _O aplikaci _Zrušit _Zavřít kartu _Upravit _Soubor _Nápověda _Nové okno _OK _Vložit _Karty konzole;příkazový řádek;spuštění; Michal Várady, Radek Tříška, Alois Nešpor x 
��          L      |       �   /   �      �   =   �   +   5     a  �  i  6   M  "   �  G   �  !   �  
                                            Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=vte&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-05-22 13:53+0200
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: čeština <gnome-cs-list@gnome.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Gtranslator 2.91.7
 Chyba (%s) při převodu dat pro potomka, zahazuje se. Chyba při čtení od potomka: %s. Není povoleno GnuTLS – data budou na disku uložena nezašifrovaná! Nelze převést znaky z %s do %s. VAROVÁNÍ 
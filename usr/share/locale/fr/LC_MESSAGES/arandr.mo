��    &      L  5   |      P     Q     V     Z  2   ^     �     �     �     �  (   �     �  :       >  S   D     �     �  ,   �     �     �  
             "     )  0   ;  )   l     �  ^   �               %     -     6     >     D     M     b  	   }     �  �  �     /	     4	     8	  @   <	  3   }	  	   �	     �	     �	  3   �	  =   �	  &  ;
     b  F   i     �     �  C   �     #     :     F     R     n     u  -   �  .   �     �  f   �     f     l     �     �  	   �     �     �     �     �     �     �                          
   $         %         	                                !          "                                                        #         &                        1:16 1:4 1:8 A part of an output is outside the virtual screen. ARandR Screen Layout Editor Accelerator Action Active An output is outside the virtual screen. Another XRandR GUI Click on a button in the left column and press a key combination you want to bind to a certain screen layout. (Use backspace to clear accelerators, escape to abort editing.) Then, select one or more layouts in the right column.

This will only work if you use metacity or another program reading its configuration. Dummy In order to configure metacity, you need to have the python gconf module installed. Keybindings (via Metacity) New accelerator... No files in %(folder)r. Save a layout first. Open Layout Orientation Resolution Save Layout Script Script Properties Setting this resolution is not possible here: %s This orientation is not possible here: %s XRandR failed:
%s Your configuration does not include an active monitor. Do you want to apply the configuration? _Help _Keybindings (Metacity) _Layout _Outputs _System _View disabled gconf not available. incompatible configuration no action other application Project-Id-Version: ARandR
Report-Msgid-Bugs-To: chrysn@fsfe.org
PO-Revision-Date: 2015-03-09 11:00+0200
Last-Translator: Tuux <tuxa@galaxie.eu.org>
Language-Team: French <https://hosted.weblate.org/projects/arandr/translations/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 2.3-dev
 1:16 1:4 1:8 Une partie d'un écran se trouve en dehors de la zone virtuelle. ARandR, configuration de la disposition des écrans Raccourci Action Active Un écran se trouve en dehors de la zone virtuelle. Another XRandR GUI (Un autre interface graphique pour XRandR) Cliquez sur un bouton de la colonne de gauche puis saisissez le raccourci voulu pour une disposition des écrans (utilisez la touche RetArr pour supprimer un raccourci ou Echap. pour annuler).

Cela ne fonctionnera que si vous utilisez metacity ou un autre programme utilisant sa configuration. Écran Pour configurer metacity, le module python gconf doit être installé. Raccourcis (via Metacity) Nouveau raccourci… Aucun fichier dans %(folder)r. Enregistrez d'abord une disposition. Ouvrir une disposition Orientation Résolution Enregistrer une disposition Script Propriétés du script Cette résolution n'est pas possible ici : %s Cette orientation n'est pas possible ici : %s Échec de XRandR :
%s Votre configuration n'inclut aucun écran actif, Voulez vous quand même appliquer la configuration ? _Aide _Raccourci (Metacity) _Disposition É_crans _Système _Vue désactivé gconf n'est pas disponible. configuration incompatible pas d'action autre application 
��    _                   *   	     4     H     N  *   Z  6   �     �     �     �     �  )   �     	     '	     E	     [	     d	  0   l	  (   �	  :   �	  %   
  6   '
  6   ^
  <   �
     �
     �
  /   �
  '     9   @  $   z  5   �  5   �  =        I  /   ]     �     �     �     �     �  	   �     �  
   �  	   �  	     4        @     G     O  	   \     f     y     �     �     �     �     �     �  	   �  	   �     �               (  	   >     H     V     [     `     h     ~     �     �     �  "   �  0   �  1     *   J  1   u     �  !   �     �  z  �  q   k  j   �     H  	   Q     [  
   b     m  	   }     �     �     �     �  �  �  /   �     �            F   #  1   j     �     �     �     �  9   �       &   -  !   T     v     |  B   �  J   �  I     #   `  9   �  9   �  P   �     I     Y  6   a  F   �  E   �     %  5   E  5   {  G   �     �  3        L     l     �     �     �  	   �     �  
   �     �     �  >   �     +     8     F     S     `     ~     �     �     �     �     �  
   �  	   �     �     �         '     4  	   L     V     j     o     t     |     �     �     �     �  #     >   /  ?   n  3   �  <   �  8      7   X      �   �  �   �   8"  m   �"     >#     M#     ]#     m#     |#     �#     �#     �#     �#     �#     =          4      R   <   5   B   *      E   F      V      "      \   8          N       &          C   _       
   ,   L      %   :         W   -       I   )   ;   7   .   T   2   #             Q                      >              J   !   1   (   U   $   X   0   ?   D   '   ]           M   [             K   S      /      G              H             6              A      3   +       P   @   O                                    ^   	       Y              Z       9        Add this Pi to the online Raspberry Pi map Add to _Rastrack... Area: Auto login: Automatically log in the default user (pi) Boot into a command line shell rather than the desktop Boot: Camera: Change Password Change _Password... Change password for the default user (pi) Character Set: Configure Raspberry Pi system Confirm new password: Country: Disable Disable overscan to hide black outline on screen Disable remote access to this Pi via SSH Disable shell and kernel messages on the serial connection Disable the Raspberry Pi Camera Board Disable the automatic loading of the I2C kernel module Disable the automatic loading of the SPI kernel module Do not complete boot until network connection is established Email Address: Enable Enable overscan to show the edges of the screen Enable remote access to this Pi via SSH Enable shell and kernel messages on the serial connection Enable the Raspberry Pi Camera Board Enable the automatic loading of the I2C kernel module Enable the automatic loading of the SPI kernel module Ensure that all of the SD card storage is available to the OS Enter new password: Enter the amount of memory available to the GPU Expand _Filesystem Filesystem Expanded Filesystem: High (1000MHz) High (950MHz) Hostname: I2C: Interfaces Keyboard: Language: Launch the desktop environment automatically on boot Locale Locale: Localisation Location: Login as user 'pi' Medium (900MHz) Modest (800MHz) Network at Boot: None (700MHz) None (900MHz) Not available O_verclock: Overscan: Password: Performance Raspberry Pi Configuration Rastrack (http://www.rastrack.co.uk) is a website which tracks the location of Raspberry Pis around the world. If you would like to be added to it, enter your name and email address below.

This is entirely optional and does not form an official registration process.
 Rastrack Registration Rastrack: Reboot needed SPI: SSH: Serial: Set WiFi country code Set _Keyboard... Set _Locale... Set _Timezone... Set _WiFi Country... Set international keyboard options Set internationalisation settings for units etc. Set the level of overclock to control performance Set the name of your Pi on a local network Set the timezone to be used by the internal clock Setting locale - please wait... Setting timezone - please wait... System The Raspberry Pi Configuration application can only modify a standard configuration.

Your configuration appears to have been modified by other tools, and so this application cannot be used on your system.

In order to use this application, you need to have the latest firmware installed, Device Tree enabled, the default "pi" user set up and the lightdm application installed.  The changes you have made require the Raspberry Pi to be rebooted to take effect.

Would you like to reboot now?  The filesystem has been expanded.

The new space will not be available until you reboot your Raspberry Pi. Timezone Timezone: To CLI To Desktop Turbo (1000MHz) Username: Wait for network WiFi Country Code WiFi Country: _GPU Memory: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: Wed Mar 16 2016 14:12:27 GMT+0100 (Paris, Madrid)
Last-Translator: Louis Desplanche <l.desplanche@gmail.com>
Language-Team: 
Language: French
Plural-Forms: nplurals=2; plural=n > 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Loco-Source-Locale: fr_FR
X-Loco-Parser: loco_parse_po
X-Loco-Target-Locale: fr
X-Generator: Loco - https://localise.biz/ Ajouter ce Pi à la carte en ligne Raspberry Pi Ajouter à _Rastrack... Région: Connexion automatique: Se connecter automatiquement en tant qu’utilisateur par défaut (pi) Booter dans la console plutôt que dans le bureau Boot: Caméra: Changer le Mot de Passe Changer de _Mot de passe... Changer le mot de passe de l'utilisateur par défaut (pi) Jeu de caractères: Configurer le système du Raspberry Pi Confirmer le nouveau mot de passe Pays: Désactivé Désactiver l'overscan pour masquer les contours noirs de l'écran Désactiver le point d’accès à distance vers le Pi par le biais de SSH Désactiver les messages du shell et du kernel sur les connexions séries Désactiver la Caméra Raspberry Pi Désactiver le chargement automatique du module noyau I2C Désactiver le chargement automatique du module noyau SPI Ne pas compléter le démarrage tant que la connexion réseau n'est pas établie Adresse E-Mail: Activé Activer l'overscan pour afficher les bords de l'écran Activer le point d’accès à distance vers le Pi par le biais de SSH Activer les messages du shell et du kernel sur les connexions séries Activer la Caméra Raspberry Pi Activer le chargement automatique du module noyau I2C Activer le chargement automatique du module noyau SPI Assurez-vous que tout le stockage de la carte SD est disponible à l'OS Entrez un nouveau mot de passe Entrez la valeur de la mémoire allouée par le GPU Étendre le système de fichier Système de Fichier étendu  Système de fichier: Élevé (1000MHz) Élevé (950MHz) Hostname: I2C: Interfaces Clavier: Langue: Lancer automatiquement l'environnement de bureau au démarrage Localisation Localisation: Localisation Emplacement: Se connecter en tant que 'pi' Moyen (900MHz) Faible (800MHz) Network at Boot: Aucun (700MHz) Aucun (900MHz) Indisponible Overclock: Overscan: Mot de passe: Performance Configuration du Raspberry Pi  Rastrack (http://www.rastrack.co.uk) is a website which tracks the location of Raspberry Pis around the world. If you would like to be added to it, enter your name and email address below.

This is entirely optional and does not form an official registration process.
 Inscription à Rastrack Rastrack: Redémarrage requis SPI: SSH: Série: Définir le code de pays WiFi Définir le _clavier... Définir la _localisation... Définir le _fuseau horaire... Définir le pays du _WiFi... Définir les paramètres du clavier Définir les paramètres internationaux pour les unités, etc. Définir le niveau d'overclock pour contrôler les performances Définir le nom du votre Pi sur votre réseau local Définir le fuseau horaire à utiliser par l'horloge interne Configuration de la localisation - Merci de patienter... Configuration du fuseau horaire - Merci de patienter... Système L'application de Configuration du Raspberry Pi ne peut modifier qu'une configuration standard.

Votre configuration semble avoir été modifiée par d'autres outils, et cette application ne peut donc pas être utilisée sur votre système.

Pour utiliser cette application, vous devez installer le dernier firmware, activer Device Tree, configurer par défaut l'utilisateur "pi" et installer l'application lightdm. Les changements que vous avez faits sur votre Raspberry Pi requièrent un redémarrage pour être opérationnels.

Voulez-vous redémarrer maintenant ? Système de Fichier a été étendu.

Le nouvel espace ne sera pas disponible avant le prochain redémarrage. Fuseau horaire Fuseau Horaire: Vers la Console Vers le Bureau Turbo (1000MHz) Nom d'utilisateur: Attendre la connexion internet Code de région WiFi Région du WiFi: Mémoire du GPU (Mo): 
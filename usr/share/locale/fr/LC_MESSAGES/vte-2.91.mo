��          L      |       �   /   �      �   =   �   +   5     a  �  i  D   6  0   {  a   �  1        @                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte 0.14.1
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=vte&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-05-21 21:39+0200
Last-Translator: Charles Monzat <superboa@hotmail.fr>
Language-Team: français <gnomefr@traduc.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Gtranslator 2.91.7
 Erreur (%s) lors de la conversion de données pour le fils, abandon. Erreur lors de la lecture du fils : « %s ». GnuTLS n’est pas activé ; les données seront écrites sur le disque sans être chiffrées ! Impossible de convertir les caractères %s en %s. AVERTISSEMENT 
��    a      $  �   ,      8     9     B     R  
   _     j     p     v     {     �     �     �     �  
   �     �     �     �  	   �     �     	     	     	  	   	     %	     =	     J	     W	     \	     e	  ?   y	  *   �	  %   �	     

  
   
     
     "
     (
     ;
     I
     \
  
   l
     w
     �
     �
     �
     �
     �
     �
  	   �
     �
  	   �
     �
     �
  
                  %     .     6     E     K     Y     f     s     w     }     �  	   �     �     �     �  "   �     �       	   	          (     .  B   5     x  	   �     �  
   �     �     �     �  
   �     �     �     �     �     �     �     �     �          %  �  '     �     �     �     �     �     �     �     �     �  
             $  	   2     <     A     a     �     �     �     �     �     �     �     �                 !   *  E   L  -   �  >   �  	   �     	                      A     Z     x  
   �     �     �     �     �     �          &     D     U     e     u     �     �     �     �     �     �     �     �     �               -     3     :  &   S  
   z  	   �     �     �  *   �     �     �     �                $  =   *  
   h     s     �     �     �     �  
   �     �  	   �     �     �     �                 %   "      H     i     V      +   Q   L      G   N       &       3   T   R   `   
      P   ]   A      ?       H       7       4   ,      Y          F   M   \   [   -          O   5   E      @                             :          /                    .   >          6       #                 K      <              ^       *   '       0               W   %   S   C      )   D                      Z   	      "          B      =           9       ;   1   X      U   J      a   !       _      2   (      I   $   8    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: LXTerminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-23 09:30+0000
Last-Translator: YvanM <yvan.masson@openmailbox.org>
Language-Team: fr <gaknar@gmail.com>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495531811.785068
 Avancé Autoriser une police grasse Son système Arrière-plan Noir Bloc Bleu Bas Bleu Primaire Cyan Clair Vert Primaire Magenta Clair Rouge Vif Brun Supprimer les anciennes _lignes Supprimer les anciennes _lignes Fermer l'onglet Fermer la fenêtre Confirmer la fermeture _Copier Copier Copier l'_URL Copyright (C) 2008-2017 Clignotement du curseur Style du curseur Cyan Gris Foncé Taille par défaut de la fenêtre Désactiver la confirmation avant de fermer une fenêtre multi-onglet Désactiver la touche menu (F10, par défaut) Désactiver l'utilisation de Alt-n pour les onglets et le menu Affichage Premier plan Gris Vert Masquer les boutons de fermeture Masquer la barre de menu Masquer le pointeur de souris Masquer la barre de défilement LXTerminal Préférences de LXTerminal Gauche Magenta Déplacer l'onglet à gauche Déplacer l'onglet à droite Déplacer l'onglet à _gauche Déplacer l'onglet à _droite No_mmer l'onglet Nommer l'onglet Onglet _suivant Nouvel ongle_t Nouvel onglet Nouvelle fenêtre Nouvel _onglet _Nouvelle fenêtre Onglet suivant Palette Gabarit de Palette Coller Onglet _précédent Préférence_s Onglet précédent Rouge Droite Lignes dans l'historique Caractères pour la sélection par mot Raccourcis Apparence Position des onglets Terminal Émulateur de terminal pour le projet LXDE Police du terminal Haut Blanc souligné Utiliser la ligne de commande Blanc Jaune Vous vous appretez à fermer %d onglets.  Êtes vous certain? Zoom avant Zoom a_rrière Zoom arrière Réinitialiser Zoom Zoom a_vant Ré_initialiser Zoom À pr_opos _Fermer l'onglet Éditio_n _Fichier _Aide _Nouvelle fenêtre _OK C_oller _Onglets console; ligne de commande; exécuter Cilyan Olowen <gaknar@gmail.com> x 
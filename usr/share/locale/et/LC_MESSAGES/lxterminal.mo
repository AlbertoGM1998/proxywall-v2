��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u  
        '     <     H     N     S     Y     `  	   d     n     z     �  
   �     �     �     �     �  
   �     �     �          
               :     L     Z     j     s  .   �  ,   �  +   �                    !     *     E     Z     p  
   �     �     �     �     �     �     �     �                ,  
   =  	   H     R  
   [  	   f     p     �     �     �     �     �     �     �     �     �     �     	               ,      5     V     l  	   t     ~     �     �  =   �     �  	   �     �       	        %  
   ?  	   J     T     a     p     v  	   {     �     �     �     �     �     �                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-01 12:10+0000
Last-Translator: Anari Jalakas <anari.jalakas@gmail.com>
Language-Team: Gnome Estonian Translation Team <gnome-et linux ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1506859809.967837
 Laiendatud Paksu kirja lubamine Helisignaal Taust Must Blokk Sinine All Erksinine Heletsüaan Heleroheline Helemagenta Helepunane Pruun Kerimisriba _peitmine _Peida kerimisriba Sulge kaart Sulge aken Sulge _aken Sulgemiskinnitus _Kopeeri Kopeeri Kopeeri _URL Autoriõigus (C) 2008-2017 Kursori vilkumine Kursori stiil Sinakasroheline Tumehall Akna vaikimisi suurus Keela mitme akna sulgemise kinnituse küsimine Menüü kiirklahvi keelamine (F10 vaikimisi) Alt-n on keelatud kaartide ja menüü jaoks Kuva Tekst Hall Roheline Sulgemise nuppude peitmine Menüüriba peitmine Hiirekursori peitmine Kerimisriba peitmine LXTerminal LXTerminali eelistused Vasakul Magenta Liiguta kaart vasakule Liiguta kaart paremale Tõsta kaart _vasakule Tõsta kaart _paremale _Kaardi nimi Kaardi nimi _Järgmine kaart Uus _kaart Uus kaart Uus aken Uus _kaart Uus _aken Järgmine kaart Värvipalett Paleti eelseade Aseta _Eelmine kaart Eeli_stused Eelmine kaart Punane Paremal Tagasikerimise ridu Sõnu moodustavad märgid Kiirklahvid Stiil Sakiriba asukoht Terminal LXDE projekti terminaliemulaator Terminali kirjatüüp Üleval Alakriips Käsurea kasutamine Valge Kollane Sa kavatsed sulgeda %d kaarti. Kas soovid kindlasti jätkata? Suurendamine _Vähenda Vähendamine Suurenduse lähtestamine _Suurenda Suurenduse _lähtestamine _Lähemalt _Tühista _Sulge kaart _Redigeerimine _Fail A_bi Uus _aken _Olgu _Aseta _Kaardid konsool;käsurida;käivita; Mattias Põldaru
Anari Jalakas x 
��          �   %   �      @  2   A     t     �  (   �     �     �     �     �  
   �     �     �     �  0     )   B  ^   l     �     �     �     �     �            	          i  -  N   �     �     �  E     	   R  !   \     ~     �  	   �     �     �  !   �  1   �  .   %  �   T     �      �          +     <  
   M     X     _  $   u                                                      
                                     	                                   A part of an output is outside the virtual screen. Accelerator Active An output is outside the virtual screen. Dummy New accelerator... Open Layout Orientation Resolution Save Layout Script Script Properties Setting this resolution is not possible here: %s This orientation is not possible here: %s Your configuration does not include an active monitor. Do you want to apply the configuration? _Help _Keybindings (Metacity) _Layout _Outputs _System _View disabled no action other application Project-Id-Version: arandr
Report-Msgid-Bugs-To: chrysn@fsfe.org
PO-Revision-Date: 2011-12-12 08:10+0000
Last-Translator: o-157 <Unknown>
Language-Team: Japanese <ja@li.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2011-12-13 05:49+0000
X-Generator: Launchpad (build 14458)
 出力位置の一部が仮想スクリーンからはみ出しています。 アクセラレータ アクティブ 出力位置が仮想スクリーンからはみ出しています。 ダミー 新しいアクセラレータ... レイアウトを開く 向き 解像度 レイアウトの保存 スクリプト スクリプトのプロパティ この解像度は可能ではありません: %s この向きは可能ではありません: %s あなたのコンフィグはアクティブなモニターを含んでいません。このコンフィグを適用してよろしいですか？ ヘルプ(_H) キーバインド(Metacity)(_K) レイアウト(_L) モニター(_O) システム(_S) 表示(_V) 無効 アクションなし その他のアプリケーション 
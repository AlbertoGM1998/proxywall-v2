��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  |  u  
   �     �  
        %     .     4     9     >     B     N     a     o     w  
   �     �     �     �     �     �     �                    ,     G     _     o     |     �  =   �  =   �  L     
   i     t     }     �     �     �     �     �  
   �     �                    '     <     Q     g     |     �     �  
   �     �     �     �     �     �     �            
        (  
   6     A  #   F     j     �     �     �     �  -   �     �     �     �     
     #     )  J   /     z     �     �     �     �     �  
   �     �       	             !     *     8     ?  	   L  #   V  s   z     �                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-22 14:38+0000
Last-Translator: nyucel <necdetyucel@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495463915.179729
 Gelişmiş Kalın yazı tipine izin ver Sesli çan Arkaplan Siyah Blok Mavi Alt Parlak Mavi Açık Camgöbeği Açık Yeşil Eflatun Açık Kırmızı Kahverengi G_eri kaydırmayı temizle Geri kaydırmayı _temizle Sekmeyi Kapat Pencereyi Kapat _Pencereyi Kapat Kapatmayı onayla Kop_yala Kopyala _URL'yi Kopyala Telif Hakkı (C) 2008-2017 İmleç yanıp sönmesi İmleç biçemi Cam göbeği Koyu Gri Varsayılan pencere boyutu Birden fazla sekmeye sahip pencereleri kapatmadan önce sorma Menü kısayol tuşunu (varsayılan F10) devre dışı bırak Sekme ve menü değiştirmek için Alt-n kullanımını devre dışı bırak Görüntü Ön plan Gri Yeşil Kapatma düğmelerini gizle Menü çubuğunu gizle Fare imlecini gizle Kaydırma çubuğunu gizle LXTerminal LXTerminal Tercihleri Sol Mor Sekmeyi Sola Taşı Sekmeyi Sağa Taşı Sekmeyi So_la Taşı Sekmeyi S_ağa Taşı Sek_meyi İsimlendir Sekmeyi İsimlendir Sonra_ki Sekme Yeni _Sekme Yeni Sekme Yeni Pencere Yeni _Sekme Yeni _Pencere Sonraki Sekme Palet Paleti Sıfırla Yapıştır Ön_ceki Sekme _Tercihler Önceki Sekme Kırmızı Sağ Geri kaydırılacak satır sayısı Düzenli ifade karakterleri Kısayollar Biçem Sekme panel konumu Uçbirim LXDE girişimi için uçbirim öykünücüsü Uçbirim yazı tipi Üst Altını çiz Komut satırını kullan Beyaz Sarı %d sekmeyi kapatmak üzeresiniz. Devam etmek istediğinizden emin misiniz? Yaklaştır _Uzaklaştır Uzaklaştır Yakınlaştırmayı Sıfırla _Yakınlaştır Yakınlaştırmayı _Sıfırla _Hakkında _İptal Sekmeyi _Kapat _Düzenle _Dosya _Yardım _Yeni Pencere _Tamam Ya_pıştır _Sekmeler konsol;komut satırı;çalıştır; Mehmet Gülmen (memetgulmen@gmail.com)
Kılıç Köken (kilickoken@gmail.com)
Necdet Yücel (necdetyucel@gmail.com) x 
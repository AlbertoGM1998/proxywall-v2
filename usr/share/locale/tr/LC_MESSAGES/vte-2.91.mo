��          L      |       �   /   �      �   =   �   +   5     a  �  i  B     %   F  E   l  0   �     �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte.HEAD.tr
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-06-14 12:27+0300
Last-Translator: Emin Tufan Çetin <etcetin@gmail.com>
Language-Team: Türkçe <gnome-turk@gnome.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 2.91.7
Plural-Forms: nplurals=1; plural=0;
 Çocuk süreç için veriyi çevirirken hata (%s), bırakılıyor. Çocuk süreçten okunurken hata: %s. GnuTLS etkin değil; veriler diske şifrelenmemiş olarak yazılacak! Karakterler %sʼden %sʼe dönüştürülemiyor. UYARI 
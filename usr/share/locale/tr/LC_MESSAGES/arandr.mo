��    &      L  5   |      P     Q     V     Z  2   ^     �     �     �     �  (   �     �  :       >  S   D     �     �  ,   �     �     �  
             "     )  0   ;  )   l     �  ^   �               %     -     6     >     D     M     b  	   }     �  '  �     �     �     �  7   �  $   	     +	     <	     B	  '   H	  7   p	  �  �	     P  [   V  (   �     �  E   �     :     P     \     l     �     �  0   �  "   �     �  c        p     y  
   �     �     �     �     �     �     �  	   �                               
   $         %         	                                !          "                                                        #         &                        1:16 1:4 1:8 A part of an output is outside the virtual screen. ARandR Screen Layout Editor Accelerator Action Active An output is outside the virtual screen. Another XRandR GUI Click on a button in the left column and press a key combination you want to bind to a certain screen layout. (Use backspace to clear accelerators, escape to abort editing.) Then, select one or more layouts in the right column.

This will only work if you use metacity or another program reading its configuration. Dummy In order to configure metacity, you need to have the python gconf module installed. Keybindings (via Metacity) New accelerator... No files in %(folder)r. Save a layout first. Open Layout Orientation Resolution Save Layout Script Script Properties Setting this resolution is not possible here: %s This orientation is not possible here: %s XRandR failed:
%s Your configuration does not include an active monitor. Do you want to apply the configuration? _Help _Keybindings (Metacity) _Layout _Outputs _System _View disabled gconf not available. incompatible configuration no action other application Project-Id-Version: 0.1.3
Report-Msgid-Bugs-To: chrysn@fsfe.org
PO-Revision-Date: 
Last-Translator: Mehmet Gülmen <memetgulmen@gmail.com>
Language-Team: tr <tr@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Country: TURKEY
 1:16 1:4 1:8 Bir çıktının bir kısmı sanal ekranın dışında. ARandR Ekran Yerleşimi Düzenleyici Hızlandırıcı Eylem Etkin Bir çıktı sanal ekranın dışında. Another XRandR GUI (Bir başka XRandR grafik arayüzü) Eğer belli bir ekran yerleşimine kısayol oluşturmak istiyorsanız sol sütundaki bir düğmeye ve bir tuş kombinasyonuna basın. (Hızlandırıcıları silmek için backspace tuşuna, düzenlemeyi bırakmak için escape tuşuna basın.) Daha sonra sağ sütunda bir ya da daha fazla yerleşim düzeni seçin.

Bu sadece eğer bu yapılandırmayı okuyacak metacity veya başka bir program kullanıyorsanız işe yarar. Dummy Metacity'yi yapılandırabilmeniz için python gconf modülünün kurulu olması gerekiyor. Kısayollar (Metacity'nin sağladığı) Yeni Hızlandırıcı... %(folder)r konumunda dosya yok. Önce bir yerleşim düzeni kaydedin. Yerleşim Düzeni Aç Yön Ayarı Çözünürlük Yerleşim Düzenini Kaydet Betik Betik Özellikleri Bu çözünürlüğü atamak mümkün değil: %s Bu yön ayarı mümkün değil: %s XRandR başarısız:
%s Yapılandırmanız etkin bir ekran içermiyor. Yine de yapılandırmayı kaydetmek istiyor musunuz? _Yardım _Kısayollar (Metacity) _Yerleşim _Çıktılar _Sistem _Görünüm devre dışı gconf erişilemez durumda. uyumsuz yapılandırma eylem yok diğer uygulama 
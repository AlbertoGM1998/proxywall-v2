��          �      l      �     �  +   �          (     ?      X     y     �     �     �     �     �  $   �  $        1     @  !   _     �     �  #   �  �  �     O  5   ]      �      �  "   �  !   �          &     >     S     q     �  -   �  !   �     �  0   �     ,     J     h  *   y                                                                 	      
                           Accept EULA Authentication is required to accept a EULA Command failed Disable the idle timer Exit after a small delay Exit after the engine has loaded No files Package description Package files PackageKit Console Interface PackageKit Monitor PackageKit service Packaging backend to use, e.g. dummy Please enter a number from 1 to %i:  Remove package Set the filter, e.g. installed Show the program version and exit Show version and exit Subcommands: The daemon crashed mid-transaction! Project-Id-Version: PackageKit
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-25 08:54+0000
Last-Translator: Richard Hughes <richard@hughsie.com>
Language-Team: Norwegian Bokmål (http://www.transifex.com/freedesktop/packagekit/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Godkjenn EULA Kan ikke godkjenne en EULA uten identitetsbekreftelse Klarte ikke å kjøre kommandoen Deaktiver inaktivitetskontrollen Avslutt etter en liten forsinkelse Avslutt etter maskinen har lastet Ingen filer Programpakkebeskrivelse Programpakkens filer PackageKit Konsollgrensesnitt PackageKit Monitor PackageKit-tjeneste Velg backend for programpakkene, f.eks foobar Skriv inn ett tall fra 1 til %i:  Fjern programpakke Bruk filteret, f.eks installerte (programpakker) Vis versjonsnummer og avslutt Vis versjonsnummer og avslutt Underkommandoer: Nissen (daemon) krasjet under operasjonen! 
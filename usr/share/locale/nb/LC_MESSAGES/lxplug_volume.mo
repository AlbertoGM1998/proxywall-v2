��          �            x     y     �  ,   �     �  9   �  ]     7   {     �     �     �      �     �          %     4  y  8     �     �  (   �  ,   �  2   #  _   V  C   �     �     �       %        B     b     z     �     
                                                              	            Analog Connecting Audio Device Connecting to Bluetooth audio device '%s'... Could not get BlueZ interface Display and control volume for ALSA and Bluetooth devices Error, you need to install an application to configure the sound (pavucontrol, alsamixer ...) Failed to connect to device - %s. Try to connect again. HDMI Mute No audio devices found No volume control on this device USB Device Settings... Volume Control (ALSA/BT) Volume control _OK Project-Id-Version: lxplug_volumealsabt 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-08-14 19:14+0200
Language-Team: Norwegian Bokmål
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.3
Last-Translator: Imre Kristoffer Eilertsen <imreeil42@gmail.com>
Language: nb
 Analog Kobler til lydenhet Kobler til '%s'-Bluetooth-lydenheten … Klarte ikke å hente inn BlueZ-grensesnittet Vis og styr volumet til ALSA- og Bluetooth-enheter Feilmelding, du må installere et program for å konfigurere lyden (pavucontrol, alsamixer …) Klarte ikke å koble til enheten - %s. Prøv å koble til på nytt. HDMI Demp Finner ingen lydenheter Ingen volumkontroll på denne enheten Innstillinger for USB-enhet … Volumkontroll (ALSA/BT) Lydstyrkekontroll _OK 
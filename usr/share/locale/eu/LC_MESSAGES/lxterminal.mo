��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u  
   5     @     Z     o     }     �     �     �     �  
   �     �     �     �     �     �     �  
             (     5     D     M     U     c     }     �     �  
   �     �  <   �  2     .   D  	   s     }     �     �     �     �     �     �     �               %     -     D     [     s     �     �     �     �     �     �     �     �     �               (     0     ?     K     Y     `     h     �     �     �     �  	   �  %   �     �       	        #     :     @  6   F  
   }     �  
   �     �     �     �     �     �     �     �     �  	                       (  "   0  ]   S     �                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-10 12:42+0000
Last-Translator: Asier Iturralde Sarasola <asier.iturralde@gmail.com>
Language-Team: Basque <alainmendi@gmail.com>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-Poedit-SourceCharset: utf-8
X-POOTLE-MTIME: 1505047363.747173
 Aurreratua Baimendu letra-tipo lodia Seinale entzungarria Atzeko planoa Beltza Blokea Urdina Behea Urdin argia Cyan argia Berde argia Magenta argia Gorri argia Marroia _Garbitu atzera-korritzea Garbitu _atzera-korritzea Itxi fitxa Itxi leihoa Itxi _leihoa Berretsi ixtea _Kopiatu Kopiatu Kopiatu _URLa Copyright-a (C) 2008-2017 Kurtsorearen keinua Kurtsorearen estiloa Cyana Gris iluna Leihoaren tamaina lehenetsia Desgaitu hainbat fitxa dituen leihoa itxi aurreko berrespena Desgaitu menuaren laster-tekla (lehenetsia F10 da) Desgaitu Alt-n erabiltzea fitxa eta menuentzat Bistaratu Aurreko planoa Grisa Berdea Ezkutatu Itxi botoiak Ezkutatu menu-barra Ezkutatu saguaren erakuslea Ezkutatu korritze-barra LXTerminala LXTerminalaren hobespenak Ezkerra Magenta Mugitu fitxa ezkerrera Mugitu fitxa eskuinera Mugitu fitxa e_zkerrera Mugitu fitxa e_skuinera _Izendatu fitxa Izendatu fitxa _Hurrengo fitxa _Fitxa berria Fitxa berria Leiho berria _Fitxa berria _Leiho berria Hurrengo fitxa Paleta Paleta aurrezarpena Itsatsi _Aurreko fitxa _Hobespenak Aurreko fitxa Gorria Eskuina Atzera-korritze lerroak Hitzak hautatzeko karaktereak Lasterbideak Estiloa Fitxa-panelaren posizioa Terminala LXDE proiekturako terminal-emuladorea Terminaleko letra-tipoa Goia Azpimarra Erabili komando-lerroa Zuria Horia %d fitxa ixtera zoaz. Ziur zaude jarraitu nahi duzula? Handiagotu _Txikiagotu Txikiagotu Berrezarri zooma _Handiagotu _Berrezarri zooma Honi _buruz _Utzi _Itxi fitxa _Editatu _Fitxategia _Laguntza _Leiho berria _Ados _Itsatsi Fi_txak kontsola;komando-lerroa;exekutatu; Alain Mendizabal <alainmendi@gmail.com>,
Asier Iturralde Sarasola <asier.iturralde@gmail.com> x 
��    {      �  �   �      h
  (   i
     �
     �
     �
     �
  &   �
       �        �     �     �  @   �  ?        P  :   j     �     �     �     �     �  &   �  "     	   '     1  R   N     �  	   �     �  
   �  '   �            o   %     �     �     �  �   �     N  $   `  "   �     �     �  "   �     �                      
     3   &     Z  v   g  	   �  :   �  	   #     -     :     F     ^     k     w     �     �     �  -   �  j   �     S  L   _     �     �     �     �     �     �  
   �     �  �   �  
   s     ~     �  �   �     5     L  �   Z          "     /     6     C     O     b     u  8   �     �  	   �     �     �  +   �     &     7  %   C     i     n     �  >   �  D   �          $     6  
   C     N     U     ^     n  �   ~       
   )     4     <     D  
   M     X  �  `  2   ;     n  (   �  "   �  ,   �  <   �     8  �   N          %     A  T   R  n   �  *     e   A     �     �     �     �     �  S     3   \     �  +   �  ~   �  /   S     �     �     �  9   �     �        �   $      �      �      �   �   !     �!  8   �!  ;   /"     k"  +   �"  B   �"     �"     #  #   #  
   B#     M#     T#  P   `#     �#  �   �#     �$  _   �$     %     %     -%     <%     Y%  $   r%     �%     �%     �%     �%  P   �%  �   N&     �&  [   '  
   k'     v'     �'  .   �'     �'     �'     �'     �'  �   (     �(  %   �(     )  �   -)     �)     *    *     4+     A+     Y+     q+     �+      �+  &   �+  "   �+  D   ,     V,  	   e,     o,     �,  I   �,     �,     -  B   %-     h-  6   u-     �-  ]   �-  f   $.     �.     �.     �.     �.     �.     /     /     2/    L/      \0     }0  	   �0     �0  	   �0     �0     �0         (           !   p   4       \   B   E                  o       T         )       n   m       X   i   "       -   =   A   7           P       2           r      H   L   @   w      &   ?         {      Z       0       S   u   f                     C   v   6       G   q   <   ]      y   I       h   ;   :   `       >      j       K   ,   s   b   O   M   9   8   5   k   N   /   t      #   a           .   x   D                                J      Q              U      $   +   ^      W   	      
   [   l      R              d   e   _                  '   Y       1       3   %   F              V       z   c      g         *        <b><big>Logout %s %s session ?</big></b> <b>Environment</b> <b>General settings</b> <b>Keymap</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. A11y Advanced Options Application Application for taking screeshot of your desktop, like scrot ... Application to go to Internet, Google, Facebook, debian.org ... Application to send mails Applications automatically started after entering desktop: Apply Archive Audio manager Audio player Authentication Authentication failed!
Wrong password? Automatically Started Applications Autostart Autostart the application ?
 Available applications : Applications of this type available on your repositories
 Banner to show on the dialog Bittorent Burning utility Calculator Change the default applications on LXDE Charmap Clipboard manager Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Communication 1 Communication 2 Composite Manager Composite manager enables graphics effects, like transpacency and shadows, if the windows manager doesn't handle it. 
Example: compton Core applications Custom message to show on the dialog Default applications for LXSession Desktop Manager Desktop Session Settings Disable autostarted applications ? Disks utility Dock Email Enabled Error Error: %s
 Extra: Add an extra parameter to the launch option
 File Manager File manager is the component which open the files.
See "More" to add options to handle the desktop, or opening files  Group: %s Handle Desktop: Draw the desktop using the file manager ?
 Identity: Image viewer Information LXSession configuration L_ock Screen Laptop mode Launcher manager Launching applications Layout Lock screen manager Manage applications loaded in desktop session Manual Settings: Manually sets the command (you need to restart lxsession-default-apps to see the change)
 Menu prefix Mime association: Automatically associates mime types to this application ?
 Mode Model More Network GUI Notes utility Options PDF Reader Panel Panel is the component usually at the bottom of the screen which manages a list of opened windows, shortcuts, notification area ... Password:  Policykit Authentication Agent Polkit agent Polkit agent provides authorisations to use some actions, like suspend, hibernate, using Consolekit ... It's not advised to make it blank. Position of the banner Power Manager Power Manager helps you to reduce the usage of batteries. You probably don't need one if you have a desktop computer.
Auto option will set it automatically, depending of the laptop mode option. Proxy Quit manager Reload S_witch User Screensaver Screenshot manager Security (keyring) Session : specify the session
 Set an utility to manager connections, such as nm-applet Settings Sh_utdown Spreadsheet Tasks monitor Terminal by default to launch command line. Terminal manager Text editor The database is updating, please wait Type Update lxsession database Upgrade manager Use a communication software (an IRC client, an IM client ...) Use another communication software (an IRC client, an IM client ...) Variant Video application Video player Webbrowser Webcam Widget 1 Window Manager: Windows Manager Windows manager draws and manage the windows. 
You can choose openbox, openbox-custom (for a custom openbox configuration, see "More"), kwin, compiz ... Workspace manager _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: LXSession
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-15 06:30+0000
Last-Translator: صفا الفليج <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1434349829.000000
 <b><big>أأخرج من جلسة %s %s؟</big></b> <b>البيئة</b> <b>الإعدادات العامّة</b> <b>خريطة المفاتيح</b> <b>التّطبيقات المعروفة</b> <b>التّطبيقات التي تبدأ يدويًّا</b> <b>إعدادات</b> <b>تحذير:</b> <b>لا</b> تمسّ هذا الخيار إلّا إذا كنت تعي ما تفعل.

<b>ملاحظة:</b> يُطبًّق هذا الإعداد بعد الولوج التّالي. الإتاحة خيارات متقدّمة التّطبيق التّطبيق لأخذ لقطاة شاشة من سطح مكتبك، كـ scrot... التّطبيق للوصوص إلى الإنترنت، وغوغل، وفيسبوك، وdebian.org، إلخ... التّطبيق لإرسال البريد التّطبيقات التي تبدأ آليًّا بعد الدّخول إلى سطح المكتب: طبّق الأرشفة مدير الصّوت مشغّل الصّوتيّات الاستيثاق فشل الاستيثاق!
لربّما تكون كلمة المرور خاطئة؟ التّطبيقات التي تبدأ آليًّا البدء الآليّ أأبدأ التّطبيق آليًّا؟
 التّطبيقات المتوفّرة: ستتوفّر التّطبيقات من هذا النّوع في مستودعاتك
 الشّعار ليظهر في الحواريّ بِتّ‌تورنت أداة الحرق الحاسبة غيّر تطبيقات لكسدي الافتراضيّة طقم المحارف مدير الحافظة سطر الأوامر المستخدم لإطلاق مدير النّوافذ
(أمر مدير النّوافذ الافتراضي لِـلكسدي هو openbox-lxde) التّواصل 1 التّواصل 2 مدير التّراكب مدير التّراكب يمكّن التّأثيرات الرّسوميّة، كالشّفافيّة والظّلال، وهذا إن لم يتعامل معها مدير النّوافذ.
مثال: compton تطبيقات اللبّ رسالة مخصّصة لتظهر في الحواريّ تطبيقات جلسة‌لكسدي الافتراضيّة مدير سطح المكتب إعدادات جلسة سطح المكتب أأعطّل التّطبيقات التي تبدأ آليًّا؟ أداة الأقراص الرّصيف البريد الإلكترونيّ ممكّن خطأ خطأ: %s
 إضافيّ: أضف معامل إضافيّ إلى خيارات الإطلاق
 مدير الملفّات مدير الملفّات هو المكوّن الذي يفتح الملفّات.
طالع "أكثر" لإضافة خيارات تتعامل مع سطح المكتب، أو تفتح الملفات  المجموعة: %s معاملة سطح المكتب: أأرسم سطح المكتب بمدير الملفّات؟
 الهويّة: عارض الصّور معلومات ضبط جلسة‌لكسدي ا_قفل الشّاشة وضع الحاسوب المحمول مدير الإطلاق تطبيقات الإطلاق التّخطيط مدير قفل الشّاشة أدِر التّطبيقات المحمّلة في جلسة سطح المكتب الإعدادات اليدويّة: يعيّن الأمر يدويًّا (عليك إعادة تشغيل lxsession-default-apps لتُطبّق التّغييرات)
 سابقة القائمة ارتباطات Mime: أأربط أنواع Mime بهذا التّطبيق آليًّا؟
 الوضع الطّراز أكثر واجهة الشّبكة الرّسوميّة أداة الملاحظات الخيارات قارئ PDF اللوحة اللوحة هي المكوّن الموجود عادةً في أسفل الشّاشة حيث تدير قائمة النّوافذ المفتوحة، والاختصارات، ومنطقة الإخطارات، إلخ... كلمة المرور: وكيل الاستيثاق Policykit وكيل Polkit وكيل Polkit يوفّر الاستيثاقات لبعض الإجراءات كالتّعليق، والإسبات واستخدام Consolekit... لا يُنصح بتركه فارغًا. موقع الشّعار مدير الطّاقة مدير الطّاقة يساعدك في تخفيض استخدام البطّاريّات. غالبًا لن تحتاج واحدةً إن كنت على حاسوب سطح مكتب.
خيار "آليّ" سيعيّنه آليًّا حسب خيار وضع الحاسوب المحمول. الوسيط مدير الإنهاء أعد التّحميل _بدّل المستخدم حافظة الشّاشة مدير لقطة الشّاشة الأمن (حلقة المفاتيح) الجلسة: حدّد جلسةً
 عيّن أداة لإدارة الاتّصالات، كـ nm-applet إعدادات أ_طفئ الجدول الممتدّ معراض المهامّ افتراضيًّا، الطّرفيّة تطلق سطر الأوامر. مدير الطّرفيّة محرّر النّصوص قاعدة البيانات تُحدَّث، فضلًا انتظر النّوع حدّث قاعدة بيانات جلسة‌لكسدي مدير التّرقية استخدم برمجيّة تواصل (عميل آي‌آر‌سي، عميل ت‌آ IM...) استخدم برمجيّة تواصل أخرى (عميل آي‌آر‌سي، عميل ت‌آ IM...) التّنويعة تطبيق الفيديو مشغّل المرئيّات متصفّح الوبّ كاميرا الوِبّ الودجة 1 مدير النّوافذ: مدير النّوافذ يرسم مدير النّوافذ النّوافذَ ويديرها.
يمكنك اختيار أوپن‌بوكس، أو أوپن‌بوكس-مخصّص (لضبط أوپن‌بوكس مخصّص، طالع "أكثر")، أو «نوافذك»، أو كومبيز، إلخ... مدير مساحات العمل أ_سبِت ا_خرج أ_عد التّشغيل _علّق ملفّ صورة الرّسالة 
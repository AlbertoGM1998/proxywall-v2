��    Q      �  m   ,      �     �     �     �  
                       #     *     6     B     O  
   ^     i     o     }  	   �     �     �     �     �     �  ?   �  *   	     4  
   <     G     L     _     m     �  
   �     �     �     �     �     �  	   �     �  	   �     �     	     	     	     '	     5	     B	     F	     L	     ]	  	   w	     �	     �	     �	  "   �	     �	     �	  	   �	     �	     �	     �	  B   
     G
  	   O
     Y
  
   b
     m
     v
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  �  �
     �  "   �                /     8     A     J     W     i     �     �     �     �     �  	   �     �     �          *     >  (   Z  W   �  Z   �  
   6     A  
   J     U      t      �      �  
   �     �     �     	  *     *   C     n     �     �     �     �     �     �     �          $     -  $   :  1   _     �     �      �     �  '   �          $  
   1  $   <     a     j  t   s  
   �  
   �  
   �      	  
   *      5     V     \     e     |     �     �  
   �  	   �     �     �  ^   �     <             
   6   E       &   )            *          8          G         F   4      :   %   1   3   -             7   I           $   .          O   J   N       ;      H   A   K   M          <   Q       (           +           0   5   C                                       2      ,   '   >                  #          P   L                B   	   "                ?   D   9   !   @   /   =              Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Confirm close Cop_y Copy _URL Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Display Foreground Gray Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New _Tab New _Window Palette Palette Preset Pre_vious Tab Preference_s Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: LXTerminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-03-23 23:53+0000
Last-Translator: Jerome Leclanche <jerome@leclan.ch>
Language-Team: Arabic <trans-ar@lists.fedoraproject.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1427154790.000000
 الوضع المتقدم اسمَح بالخط العريض جرس مسموع الخلفية أسود كتلة أزرق الأسفل أزرق فاتح السماوي الساطع اخضر فاتح أرجواني فاتح أحمر فاتح بني تأكيد الخروج ان_سخ تسخ الرابط ومضات المؤشر شكل المؤشر ازرق سماوي الرمادي الداكن حجم النافذة الإفتراضي تعطيل التأكيد قبل إغلاق النافذة مع نوافذ متعددة عطِّل مفتاح اختصار القائمة (F10 في الوضع الافتراضي) العرض النص رمادي إخفاء زر الإغلاق اخفِ شريط القوائم إخفاء مؤشر الفأرة اخفِ شريط التمرير LXTerminal تفضيلات LXTerminal اليسار أرجواني انقل اللسان إلى ال_يسار انقل اللسان إلى اليمي_ن _لسان الاسم لسان الاسم اللسان ال_تالي _لسان جديد _نافذة جديدة لوحة لوحة مسبقا اللسان ال_سابق تف_ضيلات أحمر اليمين سطور التمرير الخلفي اختر باعتبار محارف الكلمات الإختصارات المظهر موقع شريط الألسنة شاشة طرفية محاكي طرفية لمشروع LXDE خط الطرفية الأعلى تسطير استخدِم سطر الأوامر أبيض أصفر أنت على وشك إغلاق النافذة %d. هل أنتَ متاكد من أنك تريد المتابعة ؟ تكبير تصغير تصغير إعادة تعيين الحجم تكبير إعادة تعيين الحجم _عن خروج أ_غلق اللسان ت_حرير _ملف م_ساعدة موافق ا_لصق أ_لسنة console;command line;execute; أحمد فرغل <ahmad.farghal@gmail.com>أحمد نوراللّه <ahmadnurallah@gmail.com> x 
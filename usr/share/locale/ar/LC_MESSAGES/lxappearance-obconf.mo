��          �   %   �      p  :   q     �     �      �       !     j   >  F   �  [   �     L  $   j     �     �     �     �     �  �   �     �     �  	   �  #   �  O   �  �     $   �  #   �            �  ,  D   '	     l	     �	  '   �	  #   �	  (   �	  �   
  c   �
  t     2   {  <   �  9   �     %  #   E  (   i     �  �   �  +   p     �     �  0   �  g   �  �   Z  9   5  -   o     �     �                        
                                                                         	                          "%s" does not appear to be a valid Openbox theme directory "%s" was installed to %s "%s" was successfully created <span weight="bold">Theme</span> Choose an Openbox theme Create a theme _archive (.obt)... Error while parsing the Openbox configuration file. Your configuration file is not valid XML.

Message: %s Failed to load an rc.xml. Openbox is probably not installed correctly. Failed to load the obconf.glade interface file. ObConf is probably not installed correctly. Font for active window title: Font for inactive on-screen display: Font for inactive window title: Font for menu Item: Font for menu header: Font for on-screen display: Misc. N: Window icon
L: Window label (Title)
I: Iconify (Minimize)
M: Maximize
C: Close
S: Shade (Roll up)
D: Omnipresent (On all desktops) Openbox theme archives Theme Title Bar Unable to create directory "%s": %s Unable to create the theme archive "%s".
The following errors were reported:
%s Unable to extract the file "%s".
Please ensure that "%s" is writable and that the file is a valid Openbox theme archive.
The following errors were reported:
%s Unable to move to directory "%s": %s Unable to run the "tar" command: %s _Button order: _Install a new theme... Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-11-13 23:54+0100
PO-Revision-Date: 2015-04-10 15:47+0000
Last-Translator: صفا الفليج <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1428680867.000000
 لا يبدو أن "%s" دليل سمة أوبن‌بوكس صالح ثُبّت "%s" إلى %s أُنشئ "%s" بنجاح <span weight="bold">السّمة</span> اختر سمة أوبن‌بوكس أن_شئ أرشيف سمة (‎.obt)... خطأ أثناء تحليل ملفّ ضبط أوبن‌بوكس. ملفّ الضّبط ليس ملفّ XML صالح.

الرّسالة: %s فشل تحميل rc.xml. ربما لم يثبّت أوبن‌بوكس تثبيتًا صحيحًا. فشل تحميل ملفّ الواجهة obconf.glade. ربما لم يثبّت ObConf تثبيتًا صحيحًا. خطّ عنوان النّافذة النّشطة: خطّ العرض على الشّاشة غير النّشط: خطّ عنوان النّافذة غير النّشطة: خطّ عنصر القائمة: خطّ ترويسة القائمة: خطّ العرض على الشّاشة: متنوّع N: أيقونة النّافذة
L: لصيقة النّافذة (العنوان)
I: تصغير
M: تكبير
C: إغلاق
S: ظلّ (لفّ)
D: موجودة دائمًا (على كلّ أسطح المكتب) أرشيفات سمات أوبن‌بوكس السّمة شريط العنوان تعذّر إنشاء الدّليل "%s": ‏%s تعذّر إنشاء أرشيف السّمة "%s".
أُبلغ عن الأخطاء التّالية:
%s تعذّر استخراج الملفّ "%s".
فضلًا تأكّد من أنّ "%s" قابل للقراءة وأنّه أرشيف سمة أوبن‌بوكس صالح.
أُبلغ عن الأخطاء التّالية:
%s تعذّر النّقل إلى الدّليل "%s": ‏%s تعذّر تشغيل الأمر "tar":‏ %s _ترتيب الأزرار: ثبّت _سمة جديدة... 
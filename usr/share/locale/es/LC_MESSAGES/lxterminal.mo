��    c      4  �   L      p     q     z     �  
   �     �     �     �     �     �     �     �     �  
   �     �     �     	  	   #	     -	     :	     H	     V	     \	  	   a	     k	     �	     �	     �	     �	     �	  ?   �	  *   �	  %   *
     P
  
   X
     c
     h
     n
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
       	        '  	   0     :     C  
   K     V     _     k     t     |     �     �     �     �     �     �     �     �  	   �     �     �       "        =     K  	   O     Y     n     t  B   {     �  	   �     �  
   �     �     �     �        
                       %     1     5     <     B     `     s  �  u           	     "     1     7     =     D     I     R     _     l     z     �     �     �     �     �     �     �                    $     3     B     V     h     m  !   y  C   �  *   �  1   
     <     D     Q     V     \     x     �  "   �  
   �     �  	   �                -     L     k     �     �     �     �     �     �     �               $     +     G     M     `     n     �     �     �  $   �     �     �      �     �  &         '     B  	   K     U     s     z  F   �     �     �     �     �     �     �  
     	        (     <     D     M     T     c     l  
   s  $   ~  �   �     �                    /          X             E   M   &   W       R      b   ;           .   O              _       >   =                          1   !   2      C   \   4   V   Z       :   <   ^      6      7   3       '   `                       Y   P         0   [             J   T          Q   	          L   (          ?   H                        D       #       G   ,       $   K   A   c       S      -       *              %       9   I       @   "       5   8       )   ]   F   a   
      +       N      U   B    Advanced Allow bold font Audible bell Background Black Block Blue Bottom Bright Blue Bright Cyan Bright Green Bright Magenta Bright Red Brown Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Confirm close Cop_y Copy Copy _URL Copyright (C) 2008-2017 Cursor blink Cursor style Cyan Darkgray Default window size Disable confirmation before closing a window with multiple tabs Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Gray Green Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Magenta Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Palette Palette Preset Paste Pre_vious Tab Preference_s Previous Tab Red Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line White Yellow You are about to close %d tabs. Are you sure you want to continue? Zoom In Zoom O_ut Zoom Out Zoom Reset Zoom _In Zoom _Reset _About _Cancel _Close Tab _Edit _File _Help _New Window _OK _Paste _Tabs console;command line;execute; translator-credits x Project-Id-Version: lxterminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-23 02:24+0000
Last-Translator: Adolfo Jayme Barrientos <fitojb@ubuntu.com>
Language-Team: Spanish <es@li.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-POOTLE-MTIME: 1495506279.239273
 Avanzado Permitir letras negritas Señal audible Fondo Negro Bloque Azul Inferior Azul intenso Cian intenso Verde intenso Magenta intenso Rojo intenso Marrón _Limpiar el histórico _Limpiar el historial Cerrar la pestaña Cerrar la ventana Cerrar la _ventana Confirmar cierre C_opiar Copiar Copiar el _URL © 2008–2017 Parpadeo del cursor Estilo del cursor Cian Gris oscuro Tamaño predeterminado de ventana Desactivar confirmación de cierre de ventanas con varias pestañas Desactivar la tecla de atajo (predet. F10) Desactivar el uso de Alt+N para pestañas y menú Mostrar Primer plano Gris Verde Ocultar el botón de cierre Ocultar la barra de menús Ocultar el puntero del ratón Ocultar la barra de desplazamiento LXTerminal Preferencias de LXTerminal Izquierda Magenta Mover la pestaña a la izquierda Mover la pestaña a la derecha Mo_ver pestaña a la izquierda Mover pestaña a la _derecha Nombrar la pes_taña Nombrar la pestaña Pestaña s_iguiente _Pestaña nueva Pestaña nueva Ventana nueva _Pestaña nueva _Ventana nueva Pestaña siguiente Paleta Preconfiguración de paleta Pegar Pestaña a_nterior Pre_ferencias Pestaña anterior Rojo Derecha Líneas del historial Caracteres de selección por palabra Atajos Estilo Posición del panel de pestañas Terminal Emulador de terminal del proyecto LXDE Tipo de letra del terminal Superior Subrayado Utilice la línea de órdenes Blanco Amarillo Está a punto de cerrar %d pestañas. ¿Confirma que quiere continuar? Ampliar _Reducir Reducir Restablecer escala _Ampliar R_establecer escala _Acerca de _Cancelar _Cerrar la pestaña _Editar _Archivo Ay_uda _Ventana nueva _Aceptar _Pegar _Pestañas consola;línea de órdenes;ejecutar; Adolfo Jayme Barrientos <fitojb@ubuntu.com>, 2013-2017
Hugo Florentino <sysadmin@cips.cu>, 2009-2011
Guillermo Iguarán <tronador@cuteam.org>, 2009
Daniel Pacheco <daniel@tunki.org>, 2008
Julio Napurí Carlos <julionc@gmail.com>, 2008 x 
��          L      |       �   /   �      �   =   �   +   5     a  �  i  7   <      t  I   �  -   �                                              Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte.HEAD
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=vte&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-05-22 13:02+0200
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: es <gnome-es-list@gnome.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 2.91.6
Plural-Forms: nplurals=2; plural=(n != 1);
 Error (%s) al convertir datos desde el hijo, omitiendo. Error al leer desde el hijo: %s. GNUTLS no está activado; lo datos se escribirán en el disco sin cifrar. No se pueden convertir caracteres de %s a %s. ADVERTENCIA 
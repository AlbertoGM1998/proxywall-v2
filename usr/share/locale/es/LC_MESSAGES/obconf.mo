��    �      �  �   �	        
        $     @  N   E  C   �  3   �  F     ?   S  5   �  :   �               ;     M  #   m     �     �  9   �  *     #   2     V  +   v      �  !   �  -   �  6     *   J  #   u  <   �  *   �  #         %  #   F  (   j  "   �  !   �  .   �          $     +     8     I     X     t  2   �     �  ,   �  +   �  
             &     2     ?     H     _  3   w     �  y   �  !   3     U     d  �   {               $     C     W  k   q  O   �  d   -     �     �     �     �     �  #   �               ,     <  
   J     U     s     �     �     �     �     �     �               #  8   1  3   j  �   �     d     j     w     �      �     �     �     �     �  '        9  �   L        b   ;     �     �     �  	   �  #   �  O   �  �   /  $   �  #   �  *        C     L  <   k     �     �  -   �  )   �          '  (   6  +   _     �     �     �     �  5   �                 5      M      S      `      v   *   �   
   �   5   �      �   ,   �   :   %!     `!  )   e!     �!     �!     �!     �!  ~  �!     #  )   *#     T#  R   Y#  X   �#  3   $  I   9$  F   �$  4   �$  <   �$     <%  "   U%     x%      �%  $   �%      �%     �%  9   &  7   N&  &   �&     �&  -   �&  "   �&  $   '  3   C'  J   w'  -   �'  $   �'  A   (  +   W(  #   �(     �(  *   �(  3   �(  #   &)  -   J)  <   x)     �)     �)     �)     �)     �)     *     **  G   >*     �*  /   �*  :   �*  
   �*     +     
+     +     0+     9+     P+  @   o+     �+  y   �+  "   8,     [,     s,  �   �,     I-     U-  #   \-     �-  #   �-  �   �-  I   A.  k   �.     �.     /     /     */     E/  0   N/     /     �/     �/     �/  
   �/     �/  #   0  (   )0  '   R0  	   z0  	   �0     �0     �0     �0     �0     �0  I   �0  >   ?1  �   ~1     z2     �2  -   �2     �2  "   �2     �2     3     3     +3  *   J3     u3  �   �3     �4  ^   �4     �4     �4     5     5  +   .5  Z   Z5  �   �5  -   h6  ,   �6  E   �6     	7  4   7  O   G7     �7     �7  2   �7  2   �7     %8     ,8  8   B8  ,   {8     �8     �8     �8     �8  G   9     P9     b9     �9  
   �9     �9     �9     �9  :   �9  
   !:  F   ,:     s:  A   |:  K   �:     
;  :   ;     M;     P;     S;     \;     �   /   (   �      C   �       [       R   �   r               Q   k   �           %   ;           �   7   I       �   �       n   *                              ?   8   _   a   �       G   
   Z   1           |   +   i   l   V   �       B              5          3      �   $   K   L   9   ,   �      O       #          ^   �   "       �   -   j   x   �      A           U   o   ]   u       >   b   �   t       .   T       w   �   �   S      {   �           E   <      `   }      2          P   q   X   =   f          &      �   ~       :         d      s   W       �           h       0   J   z   �                           	   p       '   4       v   �       Y   H   y   g          )       m   F   D       M   �   6          \   �   N           �       e                      �   c          @       !   �    
Options:
 
Please report bugs at %s

        --archive THEME       Create a theme archive from the given theme directory
   --config-file FILE    Specify the path to the config file to use
   --help                Display this help and exit
   --install ARCHIVE.obt Install the given theme archive and select it
   --tab NUMBER          Switch to tab number NUMBER on startup
   --version             Display the version and exit
 "%s" does not appear to be a valid Openbox theme directory "%s" was installed to %s "%s" was successfully created (Unnamed desktop) --archive requires an argument
 --config-file requires an argument
 --install requires an argument
 --tab requires an argument
 <span weight="bold" size="xx-large">ObConf VERSION</span> <span weight="bold">Desktop Margins</span> <span weight="bold">Desktops</span> <span weight="bold">Dock</span> <span weight="bold">Focusing Windows</span> <span weight="bold">Fonts</span> <span weight="bold">Hiding</span> <span weight="bold">Information Dialog</span> <span weight="bold">Moving and Resizing Windows</span> <span weight="bold">Placing Windows</span> <span weight="bold">Position</span> <span weight="bold">Press the key you wish to bind...</span> <span weight="bold">Primary Monitor</span> <span weight="bold">Stacking</span> <span weight="bold">Theme</span> <span weight="bold">Titlebar</span> <span weight="bold">Window Titles</span> <span weight="bold">Windows</span> A preferences manager for Openbox A_llow dock to be both above and below windows A_nimate iconify and restore Abo_ut About ObConf Above the window Active Monitor Active _On-screen display:  All monitors Allow _windows to be placed within the dock's area Always Amount of resistance against other _windows: Amount of resistance against screen _edges: Appearance Bottom Bottom Left Bottom Right Centered Centered on the window Choose an Openbox theme Close
Shade (Roll up)
Omnipresent (On all desktops) Copyright (c) Copyright (c) 2003-2008
Dana Jansens <danakj@orodu.net>
Tim Riley <tr@slackzone.org>
Javeed Shaikh <syscrash2k@gmail.com> Create a theme _archive (.obt)... Custom actions Delay before _showing: Desktop margins are reserved areas on the edge of your screen.  New windows will not be placed within a margin, and maximized windows will not cover them. Desktops Dock Double click on the _titlebar: Double click ti_me: Drag _threshold distance: Error while parsing the Openbox configuration file.  Your configuration file is not valid XML.

Message: %s Failed to load an rc.xml. You have probably failed to install Openbox properly. Failed to load the obconf.glade interface file. You have probably failed to install ObConf properly. Fixed Monitor Fixed _x position: Fixed _y position: Fixed position on screen Floating Focus _new windows when they appear From bottom edge From left edge From right edge From top edge Horizontal Inactive O_n-screen display:  Information dialog's _position: Keep dock _above other windows Keep dock _below other windows Left Margins Maximizes the window Menu _header:  Monitor With Mouse Pointer Mouse Move & Resize Move focus _under the mouse when the mouse is not moving Move focus under the mouse when _switching desktops N - The window's icon
D - The all-desktops (sticky) button
S - The shade (roll up) button
L - The label (window title)
I - The iconify (minimize) button
M - The maximize button
C - The close button Never ObConf Error Openbox Configuration Manager Openbox theme archives Prefer to place new windows _on: Primary _monitor: Right Shades the window Show _information dialog: Syntax: obconf [options] [ARCHIVE.obt]
 The active monitor The dock is a special container for "dockapps", or dock applications.  It is not visible on screen until a dockapp is run.  Dockapps can be used to show things like a clock, or to provide you with a system tray. The monitor with the mouse The primary monitor is where Openbox will place dialogs, such as the one used for cycling windows. Theme Top Top Left Top Right Unable to create directory "%s": %s Unable to create the theme archive "%s".
The following errors were reported:
%s Unable to extract the file "%s".
Please ensure that "%s" is writable and that the file is a valid Openbox theme archive.
The following errors were reported:
%s Unable to move to directory "%s": %s Unable to run the "tar" command: %s Update the window contents while _resizing Vertical When resizing terminal windows Window icon
Window label (Title)
Iconify (Minimize)
Maximize Windows _Active window title:  _Amount of time to show the notification for: _Amount of time to wait before switching: _Bottom _Button order: _Center new windows when they are placed _Delay before focusing and raising windows: _Delay before hiding: _Desktop names: _Fixed monitor: _Floating position: _Focus windows when the mouse pointer moves over them _Hide off screen _Inactive window title:  _Install a new theme... _Left _Menu Item:  _Number of desktops:  _Orientation:  _Place new windows under the mouse pointer _Position: _Raise windows when the mouse pointer moves over them _Right _Show a notification when switching desktops _Switch desktops when moving a window past the screen edge _Top _Windows retain a border when undecorated ms px window1 x Project-Id-Version: obconf 2.0.2
Report-Msgid-Bugs-To: http://bugzilla.icculus.org/
PO-Revision-Date: 2014-04-25 21:38-0300
Last-Translator: Martín Aranciaga <malakian14@live.com>
Language-Team: spanish
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.5.5
Language: Español
 
Opciones:
 
Por favor reportar errores (bugs) a %s

        --archive THEME       Crear un archivo de tema desde el directorio de tema dado
   --config-file ARCHIVO    Especifica la ruta de el archivo de configuración para usar
   --help                Mostrar esta ayuda y salir
   --install ARCHIVO.obt Instalar el archivo de tema dado y seleccionarlo
   --tab NUMERO          Cambia al numero de pestaña NUMERO en inicio
   --version             Mostrar la versión y salir
 "%s" no parece ser un directorio válido de temas de Openbox "%s" fue instalado en %s "%s" fue creado satisfactoriamente (Escritorio sin nombre) --archive requiere un argumento
 --config-file requiere un argumento
 --install requiere un argumento
 --tab requiere un argumento
 <span weight="bold" size="xx-large">ObConf VERSION</span> <span weight="bold">Márgenes de los Escritorios</span> <span weight="bold">Escritorios</span> <span weight="bold">Dock</span> <span weight="bold">Enfocando Ventanas</span> <span weight="bold">Fuentes</span> <span weight="bold">Ocultando</span> <span weight="bold">Diálogo de información</span> <span weight="bold">Moviendo y cambiando el tamaño de las ventanas</span> <span weight="bold">Colocando Ventanas</span> <span weight="bold">Posición</span> <span weight="bold">Presione la letra que desea enlazar...</span> <span weight="bold">Monitor primario</span> <span weight="bold">Apilando</span> <span weight="bold">Tema</span> <span weight="bold">Barra de titulo</span> <span weight="bold">Títulos de las Ventanas</span> <span weight="bold">Ventanas</span> Un administrador de preferencias para Openbox Per_mitir que el muelle esté sobre y debajo de las ventanas A_nimar iconificar y restaurar Acer_ca Acerca de ObConf Sobre la ventana Monitor activo Pantalla _On-screen activa:  Todos los monitores Permitir que las _ventanas sean colocadas dentro de el área del muelle Siempre Cantidad de resistencia contra otras _ventanas: Cantidad d_e resistencia contra los bordes de la pantalla: Apariencia Abajo Abajo a la derecha Abajo a la derecha Centrado Centrado en la ventana Seleccionar un tema de Openbox Cerrar
Sombra (Arrollar)
Omnipresente (En todos los escritorios) Copyright (c) Copyright (c) 2003-2008
Dana Jansens <danakj@orodu.net>
Tim Riley <tr@slackzone.org>
Javeed Shaikh <syscrash2k@gmail.com> Crear un archivo de tema (.obt)... Acciones personalizadas Atra_sar antes de mostrar: Los márgenes de los escritorios son áreas reservadas en el borde de su pantalla.  Las ventanas nuevas no serán colocadas dentro de un margen y las ventanas maximizas no las cubrirán. Escritorios Muelle Doble click en la barra de _titulo: Dura_ción del doble click: Arras_trar la distancia del umbral: Error mientras se analizaba el archivo de configuración Openbox. Tu archivo de configuración no es un XML valido.

Mensaje: %s Falló al cargar rc.xml. Probablemente no instaló Openbox correctamente. Fallo al cargar el archivo de interfaz obconf.glade. Probablemente no se pudo instalar ObConf correctamente Monitor Fijo Posición _x fija: Posición _y fija: Posición fija en pantalla Flotando E_nfocar ventanas nuevas cuando éstas aparezcan Desde esquina inferior Desde esquina izquierda Desde esquina derecha Desde esquina superior Horizontal Pantalla O_n-screen inactiva:  Diálogo de información _position: Mantener el muelle _sobre otras ventanas Mantener el muelle _bajo otras ventanas Izquierda Márgenes Maximiza la ventana Menú de _cabecera:  Monitor con cursor de raton Ratón Mover & Cambiar de Tamaño Mover el enfoq_ue debajo del ratón cuando el ratón no se está moviendo Mover el enfoque bajo el ratón cuando _se cambian escritorios N - El icono de la ventana
D - El botón de todos los (sticky) escritorios
S - El botón de(arrollar hacia arriba) ocultar
L - La etiqueta(titulo de la ventana)
I - El botón de (minimizar) iconificar
M - El botón de maximizar
C - El botón de cerrar Nunca Error de ObConf Administrador de la Configuración de Openbox Archivos de temas de Openbox Prefiero poner nuevas ventanas _en Primario _monitor: Derecha Oculta la ventala Mostrar _information diálogo: Sintaxis: obconf [opciones] [ARCHIVO.obt]
 El monitor activo El dock es un contenedor especial para "dockapps", o aplicaciones dock.  Este no es visible hasta que la aplicación dock esté corriendo.  Las aplicaciones dock pueden ser usadas para mostrar cosas como un reloj, o proveerle una bandeja de sistema El monitor con el ratón El monitor primario es donde Openbox pondrá los dialogos, como el usado para cambiar ventanas Tema Arriba Arriba a la derecha Arriba a la izquierda No fue posible crear el directorio "%s": %s No fue posible crear el archivo de tema "%s".
Los errores siguientes fueron reportados:
%s No fue posible extraer el archivo "%s".
Por favor asegúrese de que "%s" es escribirle y que el archivo es un tema válido de Openbox
Los siguientes errores fueron reportados:
%s No fue posible mover a el directorio "%s": %s No fue posible ejecutar el comando "tar": %s Actualiza_r los contenidos de las ventanas mientras cambia el tamaño Vertical Cuando cambia el tamaño de las ventanas de terminal Icono de ventana
Etiqueta de ventana (Título)
Iconificar (Minimizar)
Maximizar Ventanas _Titulo activo de la ventana:  _Cantidad de tiempo para mostrar la notificación: _Cantidad de tiempo para esperar antes de cambiar: A_bajo _Orden de los botones _Centrar las ventanas nuevas cuando éstas son colocadas Atrasar antes _de enfocar y elevar ventanas: _Atrasar antes de ocultar: _Nombre de los escritorios: _Monitor fijo: Posición de _flotación: En_focar las ventanas cuando el puntero del ratón se mueve sobre estas _Ocultar pantalla Titulo in_activo de la ventana  _Instalar un nuevo tema _Izquierda _Elemento del Menú:  _Cantidad de escritorios:  _Orientación:  Colocar las ventanas nuevas debajo del _puntero del ratón _Posición Alza_r la ventanas cuando el puntero del ratón se mueve sobre éstas. _Derecha _Mostrar una notificación cuando se este cambiado de escritorios _Cambia entre escritorios cuando una ventana pasa la esquina de la pantalla _Arriba _Las ventanas mantienen un borde cuando están sin decorar ms px ventana1 x 
��    3      �  G   L      h     i     k     z     �     �  N   �     �  4   �  ]   2     �     �     �     �  *   �  7     u   L     �     �  b   �     V     e     u  %   �     �  -   �  ;   �     1     K  ,   P     }  1   �     �     �     �     �     	     	     +	     ;	     K	     \	  2   y	  /   �	  ;   �	  g   
  1   �
  '   �
     �
     �
     �
  R  �
     C  ?   E     �  .   �  J   �  �     3   �  �   �    �     �  ?   �  R     8   a  �   �  �   6  I  �  ^     g   m  0  �  K     N   R  S   �  q   �  c   g  �   �  �   M  r        �  �   �  ?   !  �   a  ]   �  c   A  7   �  8   �       a   %  H   �  &   �  +   �  L   #  �   p  �     �   �  J  O  [   �  x   �     o            �      !                 *   2         3               (   1                                           .   %      '           -   
                              	      /   )         +         &   $         0   "   #                         ,                   * Apply _effect: Border Border Effect C_opy to Clipboard Conflicting options: --window and --area should not be used at the same time.
 Drop shadow Effect to add to the border (shadow, border or none) Effect to add to the outside of a border. Possible values are "shadow", "none", and "border". Effects Error loading the help page Error while saving screenshot Grab _after a delay of Grab a window instead of the entire screen Grab an area of the screen instead of the entire screen Grab just the current window, rather than the whole desktop. This key has been deprecated and it is no longer in use. Grab the current _window Grab the whole _desktop Impossible to save the screenshot to %s.
 Error was %s.
 Please choose another location and retry. Include Border Include Pointer Include _pointer Include the pointer in the screenshot Include the window _border Include the window border with the screenshot Include the window manager border along with the screenshot Interactively set options None Remove the window border from the screenshot Save Screenshot Save images of your desktop or individual windows Save in _folder: Screenshot delay Screenshot directory Screenshot taken Screenshot.png Select _area to grab Select a folder Take Screenshot Take _Screenshot Take a picture of the screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. The number of seconds to wait before taking the screenshot. UI definition file for the screenshot program is missing.
Please check your installation of gnome-utils Unable to take a screenshot of the current window Window-specific screenshot (deprecated) _Name: effect seconds Project-Id-Version: bn
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-03-24 16:04+0600
Last-Translator: Israt Jahan <israt@ankur.org.bd>
Language-Team: Bengali <ankur-bd-l10n@googlegroups.com>
Language: bn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 * ইফেক্ট প্রয়োগ করা হবে: (_e) সীমানা সীমারেখার ইফেক্ট ক্লিপ-বোর্ডে অনুলিপি করুন (_o) দ্বন্দ্বপূর্ণ অপশন: --window ও --area অপশন দুটি একসাথে ব্যবহার করা যাবে না।
 ছায়া প্রয়োগ করা হবে সীমারেখার জন্য প্রয়োগের উদ্দেশ্যে ইফেক্ট (ছায়া, প্রান্তরেখা অথবা শূণ্য) সীমারেখার বাহির অংশে যে ইফেক্ট প্রয়োগ করা হবে। সম্ভাব্য মান "shadow (ছায়া)", "none(শূণ্য)" এবং "black-line(কালো-রেখা)"। ইফেক্ট সহায়িকা লোড করতে সমস্যা পর্দার ছবি সংরক্ষণ করতে সমস্যা বিরতির পর টেনে আনুন (_a) পুরো পর্দার পরিবর্তে শুধুমাত্র একটি উইন্ডোর ছবি নেওয়া হবে পুরো পর্দার পরিবর্তে, পর্দার একটি অংশের ছবি নেওয়া হবে সম্পূর্ণ ডেস্কটপের পরিবর্তে, শুধুমাত্র বর্তমান উইন্ডোর ছবি গ্রহণ করা হবে। এই কী অনুমোদিত নয় এবং বর্তমানে ব্যবহার করা হয় না। বর্তমান উইন্ডোর ছবি গ্রহণ করা হবে (_w) সম্পূর্ণ ডেস্কটপের ছবি গ্রহণ করা হবে (_d) %s-এ পর্দার ছবি সংরক্ষণ করা সম্ভব নয়।
 উৎপন্ন ত্রুটি %s
 অনুগ্রহ করে ভিন্ন অবস্থান নির্বাচন করে পুনরায় প্রচেষ্টা করুন। সীমরেখা অন্তর্ভুক্ত করা হবে পয়েন্টার অন্তর্ভুক্ত করা হবে পয়েন্টার অন্তর্ভুক্ত করা হবে (_p) পর্দার ছবিতে পয়েন্টার অন্তর্ভুক্ত করা হবে উইন্ডোর সীমানা অন্তর্ভুক্ত করা হবে (_b) পর্দার ছবিতে উইন্ডোর সীমানা অন্তর্ভুক্ত করা হবে স্ক্রিনশটের মধ্যে উইন্ডো পরিচালনব্যবস্থার সীমরেখা অন্তর্ভুক্ত করা হবে ইন্টারেক্টিভ ভাবে অপশনের মান ধার্য করা হবে শূণ্য পর্দার ছবিতে উইন্ডোর সীমারেখা অন্তর্ভুক্ত করা হবে না পর্দার ছবি সংরক্ষণ করুন ডেস্কটপ অথবা স্বতন্ত্র উইন্ডোর ছবি সংরক্ষণ করুন চিহ্নিত ফোল্ডারে সংরক্ষণ করা হবে (_f) স্ক্রিনশট গ্রহণের পূর্বে অপেক্ষাকাল স্ক্রিনশট ডিরেক্টরি স্ক্রিনশট নেয়া হয়েছে Screenshot.png টেনে আনার জন্য এলাকা নির্বাচন করুন (_a) একটি ফোল্ডার নির্বাচন করুন পর্দার ছবি নিন পর্দার ছবি নিন (_S) সম্পূর্ণ পর্দার একটি ছবি নিন সুনির্দিষ্ট সময় [সেকেন্ডে] অতিক্রান্ত হওয়ার পরে ছবি নেওয়া হবে স্ক্রিনশট সংরক্ষণের উদ্দেশ্যে ব্যবহৃত সর্বশেষ ফোল্ডার। স্ক্রিনশট তোলার পূর্বে অপেক্ষার কাল, সেকেন্ড অনুযায়ী ধার্য। পর্দার ছবি গ্রহণকারী প্রোগ্রামের জন্য আবশ্যক UI ডেফিনিশন ফাইল উপস্থিত নেই।
অনুগ্রহ করে সিস্টেমে gnome-utils ইনস্টলেশন পরীক্ষা করুন বর্তমান উইন্ডোর ছবি তুলতে ব্যর্থ। উইন্ডোর ক্ষেত্রে নির্দিষ্ট স্ক্রিনশট (অবচিত) নাম (_N): ইফেক্ট সেকেন্ড 
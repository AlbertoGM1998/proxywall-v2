��    Q      �  m   ,      �      �               +     >     M  �   e          )     C  
   G     R     Y     f  A   l  $   �     �  (   �  9   	     N	     \	  *   a	     �	     �	     �	  
   �	     �	     �	  
   �	  
   �	     �	     �	     
     
     
     )
     .
     >
     D
     J
     P
  +   b
  $   �
  "   �
  $   �
     �
     �
               #     3     F     Y     n  K   ~     �     �     �                     (     =     N  	   `     j     x     �  	   �     �     �     �     �     �     �     �     �     �     �  4     �  B     �               3     M  #   \  �   �     b  #   x     �     �     �     �     �  A   �  $   
     /  -   J  B   x     �     �  =   �          ;     S     a     j     q  	   �     �     �     �     �     �     �     �     �     �          
       J   /  #   z  !   �  #   �     �     �     �                <     W     o     �  Y   �  "   �  !        9     J     P     e     l     �     �     �     �     �     �               ;     @     E  
   J     U     c     j     r  C   w  B   �     C                     (      !      H   G   :       .   8   )   ,   I         ?   
       K   N      F      -                 *   3                M   ;   +              6                        7       B          '               	          Q   /       <      0           %       2      5       >      L   9   #   $   P   &          =                     A   @         E   4   J       1   D   O   "       *.tar.gz, *.tar.bz2 (Icon Theme) <b>Accessibility</b> <b>Antialiasing</b> <b>GUI Options</b> <b>Hinting</b> <b>Keyboard Options</b> <b>Note:</b> Not all of the desktop applications support changing cursor theme on-the-fly. So your changes here might not be fully applied to all applications till next login. <b>Sound Effect</b> <b>Sub-pixel geometry</b> BGR Background Bigger Check Button Color Color scheme is not supported by currently selected widget theme. Copyright (C) 2008-2014 LXDE Project Customize Look and Feel Customizes look and feel of your desktop Customizes look and feel of your desktop and applications Default font: Demo Enable _accessibility in GTK+ applications Enable antialiasing Enable hinting Font Foreground Full Hinting style:  Icon Theme Icons only Install Keyboard theme: Large toolbar icon Medium Mouse Cursor None Normal windows: Other Page1 Page2 Play event sounds Play event sounds as feedback to user input Preview of the selected cursor theme Preview of the selected icon theme Preview of the selected widget style RGB Radio Button Remove Same as buttons Same as dialogs Same as drag icons Same as menu items Select an icon theme Selected items: Setting color scheme is not available without lxsession as session manager. Show images in menus Show images on buttons Size of cursors Slight Small toolbar icon Smaller Sub-pixel geometry:  Text below icons Text beside icons Text only Text windows: Toolbar Icon Size:  Toolbar Style:  Tooltips: Use customized color scheme VBGR VRGB Widget Window Border _Edit _File _Help button translator-credits windows;preferences;settings;theme;style;appearance; Project-Id-Version: lxappearance
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-10-14 18:23+0300
PO-Revision-Date: 2015-05-04 18:51+0000
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1430765488.000000
 *.tar.gz, *.tar.bz2 (ikontéma) <b>Akadálymentesítés</b> <b>Élsimítás</b> <b>GUI beállításai</b> <b>Hinting</b> <b>Billentyűzet beállítások</b> <b>Megjegyzés:</b> Nem minden munkaasztal alkalmazás támogatja a menet közbeni kurzorváltást. Ezért az itteni változtatások a következő bejelentkezésig lehet, hogy nem minden alkalmazásban fognak érvényesülni. <b>Hangeffektusok</b> <b>Képponton belüli geometria</b> BGR Háttér Nagyobb Jelölőnégyzet Szín A színséma nem támogatott a jelenleg kijelölt elemtémánál. Copyright (C) 2008-2014 LXDE Projekt Megjelenés testreszabása A munkaasztal megjelenésének testreszabása A munkaasztal és az alkalmazások megjelenésének testreszabása Alapértelmezett betűkészlet: Demó _Akadálymentesítés engedélyezése a GTK+ alkalmazásokban Elsimítás engedélyezése Hinting engedélyezése Betűkészlet Előtér Teljes Hintingstílus:  Ikontéma Csak ikonok Telepítés Billentyűzettéma: Nagy eszköztár ikon Közepes Egérkurzor Nincs Normál ablakok: Egyéb 1. oldal 2. oldal Eseményhangok lejátszása Eseményhangok lejátszása visszajelzésként a felhasználói bevitelhez A kijelölt kurzortéma előnézete A kijelölt ikontéma előnézete A kijelölt elemstílus előnézete RGB Választógomb Eltávolítás Azonos a gombokéval Azonos a párbeszédablakokéval Azonos a húzás ikonokkal Azonos a menüelemekkel Válassz egy ikontémát Kijelölt elemek: A színséma beállítása nem érhető el az lxsession mint munkamenet-kezelő nélkül. Képek megjelenítése a menükben Képek megjelenítése a gombokon Kurzorok mérete Enyhe Kis eszköztár ikon Kisebb Képponton belüli geometria:  Szöveg az ikonok alatt Szöveg az ikonok mellett Csak szöveg Szöveges ablakok: Eszköztárikon mérete:  Eszköztár stílusa:  Eszköztippek: Egyedi színséma használata VBGR VRGB Elem Ablakkeret S_zerkesztés _Fájl _Súgó gomb Dvornik László <rezuri@zoho.com>
Úr Balázs <urbalazs@gmail.com> ablakok;tulajdonságok;beállítások;témák;stílus;megjelenés; 
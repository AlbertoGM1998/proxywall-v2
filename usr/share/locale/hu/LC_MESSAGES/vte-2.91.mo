��          L      |       �   /   �      �   =   �   +   5     a  �  i  K     (   N  Q   w  :   �                                              Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-08-31 12:51+0200
Last-Translator: Meskó Balázs <mesko.balazs@fsf.hu>
Language-Team: Hungarian <gnome at fsf dot hu>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.8
Plural-Forms: nplurals=2; plural=(n != 1);
 Hiba (%s) történt az adatkonverzió közben a gyermek számára, eldobva. Hiba a gyermekből olvasás közben: %s. A GnuTLS nincs engedélyezve. Az adatok titkosítatlanul lesznek a lemezre írva! Nem lehet átalakítani a karaktereket %s és %s között. FIGYELMEZTETÉS 
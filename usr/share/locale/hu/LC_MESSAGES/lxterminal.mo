��    C      4  Y   L      �     �     �     �  
   �     �     �     �       	             *     8     >  	   C     M     Z  *   g  %   �     �  
   �     �     �     �     �  
             1     6     D     S     b  	   r     |  	   �     �     �  
   �     �     �     �     �     �     �     �     �     �       	   (     2     8     K  "   T     w     �  	   �     �     �  
   �     �     �     �     �     �     �     �     	  u  	     �
  !   �
     �
     �
  	   �
     �
     �
     �
          #     4  
   F  	   Q     [     k     {  7   �  5   �     �           	     $     9     Q  
   k     v     �     �     �     �     �     �     �     �            	   !     +  
   4     ?     O     \     i     y     �     �  "   �     �     �     �  	   �  %   �     $     4     9     E  	   \     f     v     �     �  
   �     �     �      �  ?   �     <             5              	   .      C      #            B      -   @   (      '      9   2       
   !              "       >                           3       :      /          ;   =   +                            6   ,   %   *   ?           1      A      )      7       4      &      $               0             8    Advanced Allow bold font Audible bell Background Block Bottom Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Cop_y Copy Copy _URL Cursor blink Cursor style Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Paste Pre_vious Tab Preference_s Previous Tab Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line _About _Close Tab _Edit _File _Help _New Window _Paste _Tabs console;command line;execute; translator-credits Project-Id-Version: lxterminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-05-04 16:31+0000
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1430757086.000000
 Haladó Félkövér betű engedélyezése Hallható csengő Háttér Téglalap Lent Gördítősáv elrejtése Gördítősáv elrejtése Lap bezárása Ablak bezárása _Ablak bezárása _Másolás Másolás _URL másolása Villogó kurzor Kurzorstílus Menü-gyorsbillentyű tiltása (alapértelmezetten F10) Alt-n használatának tiltása lapokhoz és menühöz Kijelző Előtér Bezáró gombok elrejtése Menüsáv elrejtése Egérmutató elrejtése Gördítősáv elrejtése LXTerminal LXTerminal beállításai Balra Lap mozgatása balra Lap mozgatása jobbra Lap _balra mozgatása Lap _jobbra mozgatása Lap_név Lapnév _Következő lap Új _lap Új lap Új ablak Új _lap Új _ablak Következő lap Beillesztés _Előző lap _Beállítások Előző lap Jobbra Visszagörgetendő sorok Szó-alapú-választás karakterek Gyorsbillentyűk Stílus Lap panel helyzete Terminál Terminálemulátor az LXDE projekthez Terminál betű Fent Aláhúzás Parancssor használata _Névjegy Lap be_zárása Sz_erkesztés _Fájl _Súgó Új _ablak Be_illesztés _Lapok konzol;parancssor;végrehajtás; Dvornik László <rezuri@zoho.com>
Szervác Attila <sas@321.hu> 
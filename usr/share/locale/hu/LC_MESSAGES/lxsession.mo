��    <      �  S   �      (  (   )     R     c     o     w     �  &   �     �  	   �     �  
   �     �            $   $     I     Y     g     l     u     {     �  
   �     �  	   �  	   �     �     �     �     �     �     �       
           
   &     1     H     V     ]     j     v  	   �     �     �     �     �     �  
   �     �     �     �       
             %     -  
   6     A  �  I  :   �	     
     
  	   +
     5
     D
  *   R
  5   }
  	   �
     �
     �
     �
     �
       5        M     [     q  
   v     �     �     �  	   �     �     �     �     �     �     �     �               7     O     [  	   a     k     �     �     �     �     �     �     �     �               /     ?  	   N     X     i     w     �     �     �     �  	   �     �     3   "         '          7      +   /          $   (   8                         4      0   6      )      
   ;                                       #                  	                        %   *   &      :          -   !             9   <                       1   .          ,          5   2    <b><big>Logout %s %s session ?</big></b> Advanced Options Application Archive Audio player Authentication Authentication failed!
Wrong password? Banner to show on the dialog Bittorent Burning utility Calculator Charmap Communication 1 Communication 2 Custom message to show on the dialog Desktop Manager Disks utility Dock Document Email Enabled Error Error: %s
 File Manager Group: %s Identity: Image viewer Information Launcher manager Launching applications More Network GUI Notes utility PDF Reader Panel Password:  Position of the banner Power Manager Reload S_witch User Screensaver Screenshot manager Sh_utdown Spreadsheet Tasks monitor Terminal manager Text editor Video player Webbrowser Webcam Widget 1 Window Manager: Windows Manager _Hibernate _Logout _Reboot _Suspend image file message Project-Id-Version: lxsession
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-05-04 19:58+0000
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian <hu@li.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1430769520.000000
 <b><big>Kijelentkezik a(z) %s %s munkamenetből?</big></b> Speciális beállítások Alkalmazás Archívum Hanglejátszó Hitelesítés A hitelesítés sikertelen!
Rossz jelszó? A párbeszédablakon megjelenítendő egyéni fejléc Bittorent Lemezíró segédprogram Számológép Karaktertérkép Kommunikáció 1 Kommunikáció 2 A párbeszédablakon megjelenítendő egyéni üzenet Asztalkezelő Lemezek segédprogram Dokk Dokumentum E-mail Engedélyezve Hiba Hiba: %s
 Fájlkezelő Csoport: %s Azonosító: Képmegjelenítő Információk Indítókezelő Alkalmazások indítása Több Hálózati grafikus felület Jegyzetek segédprogram PDF olvasó Panel Jelszó:  A fejléc pozíciója Energiakezelő Újratöltés Felhasználó_váltás Képernyővédő Képernyőkép-kezelő _Leállítás Munkafüzet Feladat megfigyelő Terminálkezelő Szövegszerkesztő Videolejátszó Webböngésző Webkamera Felületi elem 1 Ablakkezelő: Ablakkezelő _Hibernálás _Kijelentkezés Új_raindítás _Felfüggesztés képfájl üzenet 
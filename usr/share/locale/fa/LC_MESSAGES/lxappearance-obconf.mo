��          �   %   �      `  :   a     �     �      �     �  !     j   .  F   �  [   �     <     Z     z     �     �     �  �   �     L     c  	   i  #   s  O   �  �   �  $   �  #   �     �     �  �  �  W   �     �  &   	     5	  #   U	  '   y	  �   �	  �   5
  �   �
  )   G  8   q     �     �      �     �  �     '   �            4   *  ^   _  �   �  @   �  6   �          '                         
                                                                         	                          "%s" does not appear to be a valid Openbox theme directory "%s" was installed to %s "%s" was successfully created <span weight="bold">Theme</span> Choose an Openbox theme Create a theme _archive (.obt)... Error while parsing the Openbox configuration file. Your configuration file is not valid XML.

Message: %s Failed to load an rc.xml. Openbox is probably not installed correctly. Failed to load the obconf.glade interface file. ObConf is probably not installed correctly. Font for active window title: Font for inactive window title: Font for menu Item: Font for menu header: Font for on-screen display: Misc. N: Window icon
L: Window label (Title)
I: Iconify (Minimize)
M: Maximize
C: Close
S: Shade (Roll up)
D: Omnipresent (On all desktops) Openbox theme archives Theme Title Bar Unable to create directory "%s": %s Unable to create the theme archive "%s".
The following errors were reported:
%s Unable to extract the file "%s".
Please ensure that "%s" is writable and that the file is a valid Openbox theme archive.
The following errors were reported:
%s Unable to move to directory "%s": %s Unable to run the "tar" command: %s _Button order: _Install a new theme... Project-Id-Version: 1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-11-13 23:54+0100
PO-Revision-Date: 2015-08-17 01:28+0000
Last-Translator: Anonymous Pootle User
Language-Team: Persian <trans-fa@lists.fedoraproject.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1439774918.721147
 "%s" به نظر می رسد یک شاخه تم اوپن باکس معتبر نباشد "%s" در  %s نصب شد با موفقیت "%s" ساخته شد <span weight="bold">تم</span> انتخاب تم اوپن باکس ساخت یک آرشیو تم (.obt)... خطا در تحلیل فایل پیکره بندی اوپن باکس. فایل پیکره بندی شما XML معتبری نیست.

پیام: %s خطا در بارگذاری rc.xml. شما احتمالا در نصب درست اوپن باکس دچار اشکال شده اید. خطا در بارگذاری فایل میانگر obconf.glade. شما احتمالا به درستی ObConf را نصب نکرده اید. قلم عنوان پنجره ی فعال: قلم برای عنوان پنجره ی غیرفعال: قلم ایتم منو: قلم منو هدر: قلم نمایش بر صفحه: غیره N: آیکن پنجره
L: برچسب پنجره (عنوان)
I: ایکن دار کردن (کوچک کردن)
M: بزرگ کردن
C: بستن
S: محو (جمع کردن)
D: حاضر در همه جا (در همه رومیزی ها) آرشیوهای تم اوپن باکس تم نوار عنوان قادر به ساختن شاخه نیست  "%s": %s قادر به ساخت آرشیو تم  "%s" نیست.
خطاهای ذیل گزارش شد:
%s قادر به استخراج فایل "%s" نیست.
لطفا مطمئن شوید که "%s" قابل خواندن است و فایل یک آرشیو تم اوپن باکس است.
خطاهای ذیل گزارش شد:
%s قادر نیست به جا به جایی شاخه به "%s": %s قادر به اجرای فرمان "tar" نیست: %s ترتیب دکمه: نصب تم جدید... 
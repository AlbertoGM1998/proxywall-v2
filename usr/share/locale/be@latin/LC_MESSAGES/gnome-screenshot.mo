��    +      t  ;   �      �     �     �     �     �     �     �  4   �  ]   3     �     �     �     �  *   �          .     F     U     e  %   v     �  -   �  ;   �     !     ;     @  1   P     �     �     �     �     �     �     �     �  2     /   I  ;   y  g   �  1        O     V     ]  �  e     !
     #
     1
     7
     D
  	   V
  X   `
  u   �
     /  #   6  "   Z     }  0   �     �     �     �     �               +     @  5   ^     �     �     �  0   �     �          4     I     [     j     ~     �  (   �  +   �  -      r   .  *   �     �     �  	   �         (           '                      #      !          )                              &   +             %            	                          *   
                      "                          $    * Apply _effect: Border Border Effect C_opy to Clipboard Drop shadow Effect to add to the border (shadow, border or none) Effect to add to the outside of a border. Possible values are "shadow", "none", and "border". Effects Error loading the help page Error while saving screenshot Grab _after a delay of Grab a window instead of the entire screen Grab the current _window Grab the whole _desktop Include Border Include Pointer Include _pointer Include the pointer in the screenshot Include the window _border Include the window border with the screenshot Include the window manager border along with the screenshot Interactively set options None Save Screenshot Save images of your desktop or individual windows Save in _folder: Screenshot delay Screenshot directory Screenshot.png Select a folder Take Screenshot Take _Screenshot Take a picture of the screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. The number of seconds to wait before taking the screenshot. UI definition file for the screenshot program is missing.
Please check your installation of gnome-utils Unable to take a screenshot of the current window _Name: effect seconds Project-Id-Version: gnome-utils.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2007-10-01 23:55+0300
Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>
Language-Team: Belarusian Latin <be-latin.open-tran.eu>
Language: be@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 * Užyj _efekt: Abrys Efekt abrysu _Skapijuj u bufer Adcianiaj Efekt, dadavany da abrysu ("shadow" (cień), "border" (abrys) ci "none" (biez efektaŭ)) Efekt dla vonkavaj častki abrysu. Mahčymyja vartaści: "shadow" (cień), "none" (biez efektaŭ) i "border" (abrys). Efekty Pamyłka zahruzki staronki dapamohi Pamyłka pry zapisie zdymka ekranu Zdymi _praz Zrabi zdymak vakna, a nie ŭsiaho ekranu całkam Zdymi dziejnaje _vakno Zdymi ŭvieś _stoł Ułučy abrys Ułučy kursor Ułučy _kursor Ułučy kursor u zdymak Ułučy _abrys vakna Ułučy abrys vakna ŭ zdymak Ułučy ŭ zdymak ekranu abrysy ad kiraŭnika voknaŭ Interaktyŭna akreślaj opcyi Niama Zapišy zdymak ekranu Zapišy vyjavy svajho stała ci peŭnych voknaŭ Zapišy ŭ _katalohu: Čas ustrymańnia dla zdymkaŭ Kataloh dla zdymkaŭ Zdymak_ekranu.png Abiary kataloh Zrabi zdymak ekranu Zrabi _zdymak ekranu Zrabi malunak z ekranam Zdymi praz akreśleny čas [u sekundach] Kataloh, dzie byŭ zapisany apošni zdymak. Kolki sekundaŭ čakać da zdymańnia ekranu. Nie staje fajłu apisańnia interfejsu dla prahramy zdymańnia ekranu.
Pravier svaju instalacyju „gnome-utils” Niemahčyma zrabić zdymak dziejnaha vakna _Nazva: efekt sekundaŭ 
��          |      �             !     #  *   1     \  -   k  ;   �     �     �     �  2        8  {  ?     �     �  7   �       '     G   C     �     �     �  >   �                          
                           	           * Border Effect Grab a window instead of the entire screen Include Border Include the window border with the screenshot Include the window manager border along with the screenshot Save Screenshot Save in _folder: Screenshot.png Take screenshot after specified delay [in seconds] _Name: Project-Id-Version: gnome-utils.HEAD.sq
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2005-08-17 12:40+0200
Last-Translator: Laurent Dhima <laurenti@alblinux.net>
Language-Team: Albanian <gnome-albanian-perkthyesit@lists.sourceforge.net>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 * Efekti i kornizës Fotografo një dritare në vend të të gjithë ekranit Përfshi kornizën Përfshi kornizën e dritares tek pamja Përfshi dhe kornizën e organizuesit të dritareve tek pamja e ekranit Ruaj Pamjen e Ekranit Ruaje tek _kartela: PamjaEkranit.png Merr pamjen e ekranit mbas një kohe të caktuar [në sekonda] _Emri: 
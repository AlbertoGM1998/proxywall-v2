��          �      \      �     �     �  *   �       -     ;   I     �  1   �     �     �     �     �            2   9  /   l     �     �     �  �  �     L     N  +   ]     �  /   �  9   �       6        H     \     r     �     �     �  1   �  1   �                (                                                    	   
                                            * Border Effect Grab a window instead of the entire screen Include Border Include the window border with the screenshot Include the window manager border along with the screenshot Save Screenshot Save images of your desktop or individual windows Save in _folder: Screenshot directory Screenshot.png Select a folder Take Screenshot Take a picture of the screen Take screenshot after specified delay [in seconds] The directory the last screenshot was saved in. _Name: effect seconds Project-Id-Version: gnome-utils
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2006-06-30 23:13-0000
Last-Translator: Rhys Jones <rhys@sucs.org>
Language-Team: Cymraeg <gnome-cy@lists.linux.org.uk>
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural= (n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != 11) ? 2 : 3;
X-Poedit-Language: Welsh
 * Effaith Border Cipio'r ffenest yn hytrach na'r sgrin gyfan Cynnwys Border Cynnwys border y ffenest yn rhan o'r sgrînlun. Cynnwys border y rheolwr ffenestri fel rhan o'r sgrînlun Cadw Sgrînlun Cadw delweddau o'r bwrdd gwaith neu o ffenestri unigol Cadw mewn _plygell: Cyfeiriadur sgrînlun Sgrînlun.png Dewiswch blygell Tynnu Sgrînlun Cipio llun o'r sgrin Oedi'r nifer yma o eiliadau cyn tynnu'r sgrînlun Y cyfeiriadur y cadwyd sgrînlun ynddi ddiwethaf. _Enw: effaith eiliad 
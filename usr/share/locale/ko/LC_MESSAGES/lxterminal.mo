��    C      4  Y   L      �     �     �     �  
   �     �     �     �       	             *     8     >  	   C     M     Z  *   g  %   �     �  
   �     �     �     �     �  
             1     6     D     S     b  	   r     |  	   �     �     �  
   �     �     �     �     �     �     �     �     �     �       	   (     2     8     K  "   T     w     �  	   �     �     �  
   �     �     �     �     �     �     �     �     	  �  	     �
     �
     �
  	   �
     �
     �
     �
       
   3  
   >     I  
   X     c     j     y     �  6   �  1   �       	             '     <     T  
   h     s     �     �     �     �  !   �               *     9     E     M     U     a  
   m     x     �     �  
   �  	   �     �     �     �     �       	     .   !     P     a     e     l  
   �     �  
   �  
   �     �     �     �     �  5   �  @   #     <             5              	   .      C      #            B      -   @   (      '      9   2       
   !              "       >                           3       :      /          ;   =   +                            6   ,   %   *   ?           1      A      )      7       4      &      $               0             8    Advanced Allow bold font Audible bell Background Block Bottom Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Cop_y Copy Copy _URL Cursor blink Cursor style Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Paste Pre_vious Tab Preference_s Previous Tab Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line _About _Close Tab _Edit _File _Help _New Window _Paste _Tabs console;command line;execute; translator-credits Project-Id-Version: lxterminal.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-07-20 14:03+0000
Last-Translator: Seong-ho Cho <darkcircle.0426@gmail.com>
Language-Team: Seong-ho Cho <darkcircle.0426@gmail.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1437400999.000000
 고급 굵은 글씨 허용 소리 알림 배경색 블럭 아래 이전 스크롤 지우기(_E) 이전 스크롤 지우기(_O) 탭 닫기 창 닫기 창 닫기(_W) 복사(_Y) 복사 URL 복사(_U) 커서 깜빡임 커서 모양 메뉴 바로 가기 키 비활성화 (기본 F10 키) 탭과 메뉴에 대해 Alt-n 사용 비활성화 표시 글자색 닫기 단추 숨김 메뉴 막대 숨김 마우스 커서 숨김 스크롤바 숨김 LXTerminal LXTerminal 기본 설정 왼쪽 탭 왼쪽으로 옮기기 탭 오른쪽으로 옮기기 탭 왼쪽으로 옮기기(_L) 탭 오른쪽으로 옮기기(_R) 탭 이름(_M) 제목 설정 다음 탭(_X) 새 탭(_A) 새 탭 새 창 새 탭(_T) 새 창(_W) 다음 탭 붙여넣기 이전 탭(_V) 기본 설정(_S) 이전 탭 오른쪽 이전 스크롤 줄 수 단어 단위 선택 문자 바로 가기 형식 탭 패널 위치 터미널 LXDE 프로젝트의 터미널 에뮬레이터 터미널 글꼴 위 밑줄 명령 행을 사용합니다 정보(_A) 탭 닫기(_C) 편집(_E) 파일(_F) 도움말(_H) 새 창(_N) 붙여넣기(_P) 탭(_T) console;콘솔;command line;명령줄;execute;실행; Seo Sanghyeon <sanxiyn@gmail.com>
Seong-ho Cho <shcho@gnome.org> 
��            )   �      �     �     �     �     �  N   �       4   "  ]   W     �     �     �  *   �  7     u   S     �     �     �     	          *     E  1   J     |     �     �     �     �     �     �  c  �     I  '   K     s  %   �  �   �  !   \  t   ~  �   �     �	  @   �	  =   
  w   A
  �   �
  D  L  D   �  @   �  )     -   A  .   o  D   �     �  �   �  ,   �  <   �  2   �  E   %     k     z     �                      
                                        	                                                                           * Apply _effect: Border Border Effect Conflicting options: --window and --area should not be used at the same time.
 Drop shadow Effect to add to the border (shadow, border or none) Effect to add to the outside of a border. Possible values are "shadow", "none", and "border". Effects Error loading the help page Grab _after a delay of Grab a window instead of the entire screen Grab an area of the screen instead of the entire screen Grab just the current window, rather than the whole desktop. This key has been deprecated and it is no longer in use. Grab the current _window Grab the whole _desktop Include Border Include Pointer Include _pointer Include the window _border None Save images of your desktop or individual windows Save in _folder: Select _area to grab Select a folder Take a picture of the screen _Name: effect seconds Project-Id-Version: gnome-utils
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-05-12 18:36 -0400
Last-Translator: Thomas Thurman <tthurman@gnome.org>
Language-Team: Shavian <ubuntu-l10n-en-shaw@launchpad.net>
Language: en@shaw
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n!=1;
 * 𐑩𐑐𐑤𐑲 _𐑦𐑓𐑧𐑒𐑑: 𐑚𐑹𐑛𐑼 𐑚𐑹𐑛𐑼 𐑦𐑓𐑧𐑒𐑑 𐑒𐑩𐑯𐑓𐑤𐑦𐑒𐑑𐑦𐑙 𐑪𐑐𐑖𐑩𐑯𐑟: --window 𐑯 --area 𐑖𐑫𐑛 𐑯𐑪𐑑 𐑚𐑰 𐑿𐑟𐑛 𐑨𐑑 𐑞 𐑕𐑱𐑥 𐑑𐑲𐑥.
 𐑛𐑮𐑪𐑐 𐑖𐑨𐑛𐑴 𐑦𐑓𐑧𐑒𐑑 𐑑 𐑨𐑛 𐑑 𐑞 𐑚𐑹𐑛𐑼 (𐑖𐑨𐑛𐑴, 𐑚𐑹𐑛𐑼 𐑹 𐑯𐑳𐑯) 𐑦𐑓𐑧𐑒𐑑 𐑑 𐑨𐑛 𐑑 𐑞 𐑬𐑑𐑕𐑲𐑛 𐑝 𐑩 𐑚𐑹𐑛𐑼. 𐑐𐑪𐑕𐑩𐑚𐑩𐑤 𐑝𐑨𐑤𐑿𐑟 𐑸 "shadow", "none", 𐑯 "border". 𐑦𐑓𐑧𐑒𐑑𐑕 𐑻𐑼 𐑤𐑴𐑛𐑦𐑙 𐑞 𐑣𐑧𐑤𐑐 𐑐𐑱𐑡 𐑜𐑮𐑨𐑚 _𐑭𐑓𐑑𐑼 𐑩 𐑛𐑦𐑤𐑱 𐑝 𐑜𐑮𐑨𐑚 𐑩 𐑢𐑦𐑯𐑛𐑴 𐑦𐑯𐑕𐑑𐑧𐑛 𐑝 𐑞 𐑧𐑯𐑑𐑲𐑼 𐑕𐑒𐑮𐑰𐑯 𐑜𐑮𐑨𐑚 𐑩𐑯 𐑺𐑦𐑩 𐑝 𐑞 𐑕𐑒𐑮𐑰𐑯 𐑦𐑯𐑕𐑑𐑧𐑛 𐑝 𐑞 𐑧𐑯𐑑𐑲𐑼 𐑕𐑒𐑮𐑰𐑯 𐑜𐑮𐑨𐑚 𐑡𐑳𐑕𐑑 𐑞 𐑒𐑳𐑮𐑩𐑯𐑑 𐑢𐑦𐑯𐑛𐑴, 𐑮𐑭𐑞𐑼 𐑞𐑨𐑯 𐑞 𐑣𐑴𐑤 𐑛𐑧𐑕𐑒𐑑𐑪𐑐. 𐑞𐑦𐑕 𐑒𐑰 𐑣𐑨𐑟 𐑚𐑰𐑯 𐑛𐑧𐑐𐑮𐑩𐑒𐑱𐑑𐑩𐑛 𐑯 𐑦𐑑 𐑦𐑟 𐑯𐑴 𐑤𐑪𐑙𐑜𐑼 𐑦𐑯 𐑿𐑕. 𐑜𐑮𐑨𐑚 𐑞 𐑒𐑳𐑮𐑩𐑯𐑑 _𐑢𐑦𐑯𐑛𐑴 𐑜𐑮𐑨𐑚 𐑞 𐑣𐑴𐑤 _𐑛𐑧𐑕𐑒𐑑𐑪𐑐 𐑦𐑯𐑒𐑤𐑵𐑛 𐑚𐑹𐑛𐑼 𐑦𐑯𐑒𐑤𐑵𐑛 𐑐𐑶𐑯𐑑𐑼 𐑦𐑯𐑒𐑤𐑵𐑛 _𐑐𐑶𐑯𐑑𐑼 𐑦𐑯𐑒𐑤𐑵𐑛 𐑞 𐑢𐑦𐑯𐑛𐑴 _𐑚𐑹𐑛𐑼 𐑯𐑳𐑯 𐑕𐑱𐑝 𐑦𐑥𐑦𐑡𐑩𐑟 𐑝 𐑿𐑼 𐑛𐑧𐑕𐑒𐑑𐑪𐑐 𐑹 𐑦𐑯𐑛𐑦𐑝𐑦𐑛𐑿𐑩𐑤 𐑢𐑦𐑯𐑛𐑴𐑟 𐑕𐑱𐑝 𐑦𐑯 _𐑓𐑴𐑤𐑛𐑼: 𐑕𐑩𐑤𐑧𐑒𐑑 _𐑺𐑦𐑩 𐑑 𐑜𐑮𐑨𐑚 𐑕𐑩𐑤𐑧𐑒𐑑 𐑩 𐑓𐑴𐑤𐑛𐑼 𐑑𐑱𐑒 𐑩 𐑐𐑦𐑒𐑗𐑼 𐑝 𐑞 𐑕𐑒𐑮𐑰𐑯 _𐑯𐑱𐑥: 𐑦𐑓𐑧𐑒𐑑 𐑕𐑧𐑒𐑩𐑯𐑛𐑟 
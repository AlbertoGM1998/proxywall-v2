��    /      �  C                   %     8     P  &   k     �  �   �     2     7     H  :   T     �  &   �  "   �     �  	   �  o   	     y     �     �  
   �     �  	   �  	   �     �     �     �     �  -   �       
   !     ,     2     9     F     R     [     g  
   s     ~     �     �  
   �     �  
   �     �  }  �     @	     L	     \	     |	  D   �	     �	  �   �	     �
     �
  	   �
  <   �
     �
  =     <   M     �  	   �  u   �     "     +  	   2     <     H  
   U     `     l     {     �     �  ,   �     �  
   �     �     �     �     	          )  
   =  
   H     S     Z     m     t     �  	   �     �     )             +                                /   #                               -                     	   "                                       .            ,       '      $                 !   (   *       &            %   
        <b>Dbus</b> <b>Environment</b> <b>General settings</b> <b>Known Applications </b> <b>Manual autostarted applications</b> <b>Settings</b> <b>Warning:</b> Do <b>NOT</b> touch this unless you know exactly what you are doing.

<b>NOTE:</b> This setting will take effect on next login. A11y Advanced Options Application Applications automatically started after entering desktop: Authentication Authentication failed!
Wrong password? Automatically Started Applications Available applications Bittorent Command line used to launch window manager
(The default window manager command for LXDE should be openbox-lxde) Document Email Enabled Error: %s
 File Manager Group: %s Identity: Image viewer Information L_ock Screen Laptop mode Manage applications loaded in desktop session More Password:  Proxy Reload S_witch User Screensaver Settings Spreadsheet Text editor Webbrowser Webcam Window Manager: Xrandr _Hibernate _Reboot image file message Project-Id-Version: lxsession-lite
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-09 17:03+0000
Last-Translator: H.Humpel <H.Humpel@gmx.de>
Language-Team: Deutsch <LL@li.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1433869393.000000
 <b>Dbus</b> <b>Umgebung</b> <b>Allgemeine Einstellungen</b> <b>Bekannte Anwendungen </b> <b>Anwendungen, die beim Starten automatisch ausgeführt werden:</b> <b>Einstellungen</b> <b>Warnung:</b> Ändern Sie dies <b>NICHT</b>, außer Sie wissen genau, was Sie machen.

<b>HINWEIS:</b> Diese Einstellungen treten bei der nächsten Anmeldung in Kraft. A11y Erweiterte Optionen Anwendung Anwendungen die beim Starten automatisch ausgeführt werden: Authentifizierung Die Authentifizierung ist fehlgeschlagen!
Falsches Passwort ? Anwendungen die beim Starten automatisch ausgeführt werden: Verfügbare Anwendungen Bittorent Befehlszeile zum Starten der Fensterverwaltung
(Der Standfensterverwaltungsbefehl für LXDE sollte openbox-lxde sein) Dokument E-Mail Aktiviert Fehler: %s
 Dateimanager Gruppe: %s Identität: Bildbetrachter Information C_omputer sperren Laptop Modus Automatisch gestartete Anwendungen verwalten Mehr Passwort:  Proxy Erneut laden Benutzer _wechseln Bildschirmschoner Einstellungen Tabellenkalkulation Texteditor Webbrowser Webcam Fensterverwallter: Xrandr Ru_hezustand Neu sta_rten Bilddatei Meldung 
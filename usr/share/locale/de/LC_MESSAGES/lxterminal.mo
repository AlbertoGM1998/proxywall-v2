��    H      \  a   �            !     *     :  
   G     R     X     _  
   n     y     �  	   �     �     �     �     �  	   �     �     �     �     �  *     %   9     _  
   g     r     �     �     �  
   �     �     �     �     �     �     	  	        #  	   ,     6     ?  
   G     R     [     g     p     v     �     �     �     �     �  	   �     �     �     �  "   �     	     ,	  	   0	     :	     O	     V	  
   ]	     h	     n	     t	     z	     �	     �	     �	     �	  �  �	     o     }     �     �     �     �     �     �     �     �          #     6  	   J     T     ]     o     �  
   �     �  -   �  7   �           ,     9     V     l     �  
   �     �     �     �     �                @     T     d     v     �     �     �     �     �  	   �     �     �     �               (     H     V     [     v  '        �     �     �     �     �     �     �                      
   -     8      @  �   a         7                 5   >   $              B   F   ;   @           A   !   +   ?   #   <   "   D                    C   8       (                    =       ,   9      	   G   %         
         /   2      '                    H      :   1   *              6   &                  E         0          4                   3   -           )           .    Advanced Allow bold font Audible bell Background Block Bottom Bright Magenta Bright Red Cl_ear scrollback Clear scr_ollback Close Tab Close Window Close _Window Cop_y Copy Copy _URL Cursor blink Cursor style Darkgray Default window size Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Hide Close buttons Hide menu bar Hide mouse pointer Hide scroll bar LXTerminal LXTerminal Preferences Left Move Tab Left Move Tab Right Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New T_ab New Tab New Window New _Tab New _Window Next Tab Paste Pre_vious Tab Preference_s Previous Tab Right Scrollback lines Select-by-word characters Shortcuts Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line Yellow _About _Close Tab _Edit _File _Help _New Window _Paste _Tabs console;command line;execute; translator-credits Project-Id-Version: lxterminal 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-15 08:33+0000
Last-Translator: alfgaida <info@g-com.eu>
Language-Team: de <christoph.wickert@googlemail.de>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.8
X-Poedit-SourceCharset: utf-8
X-POOTLE-MTIME: 1494837197.820816
 Verschiedenes Fett gedruckte Schrift erlauben Akutisches Signal Hintergrund: Block Unten Helles Magenta Hellrot Bildlaufleiste verbergen Bildlaufleiste verbergen Reiter schließen Fenster schließen _Fenster schließen _Kopieren Kopieren _Adresse kopieren Blinkender Cursor: Art des Mauszeigers: Dunkelgrau Standard Fenstergröße Tastenkürzel F10 für das Menü deaktivieren Tastenkürzel Alt-n zum Wechsel der Reiter deaktivieren Darstellung Vordergrund: Schließen-Knöpfe verbergen Menüleiste verbergen Mauszeiger verbergen Bildlaufleiste verbergen LXTerminal LXTerminal Einstellungen Links Reiter nach links verschieben Reiter nach rechts verschieben Reiter nach _links verschieben Reiter nach _rechts verschieben _Titel festlegen… Titel festlegen _Nächster Reiter Neuer _Reiter Neuer Reiter Neues Fenster Neuer _Reiter Neues _Fenster Nächster Reiter Einfügen _Vorheriger Reiter _Einstellungen Vorheriger Reiter Rechts Zeilen zurückrollen: Zeichen für Auswahl nach Wort: Tastenkürzel Stil Position der Reiterleiste: Terminal Terminal-Emulator für das LXDE Projekt Terminal-Schriftart: Oben Unterstrichen Befehlszeile verwenden Gelb _Info Reiter s_chließen _Bearbeiten _Datei _Hilfe _Neues Fenster E_infügen _Reiter Konsole;Befehlszeile;Ausführen; Christoph Wickert <cwickert@fedoraproject.org>
Mario Blättermann <mario.blaettermann@t-online.de>
Lutz Thuns <lthuns@opensuse.org>
Daniel Winzen <d@winzen4.de> 
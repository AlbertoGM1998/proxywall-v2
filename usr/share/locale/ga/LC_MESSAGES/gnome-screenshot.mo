��    %      D  5   l      @  (   A     j     {     �     �     �     �     �     �     �                     1     L     Q     j     o  0        �  
   �     �     �     �               2     B  '   S  %   {     �     �     �     �     �     �  �  �  +   �     �     �     �  $   �  
        $  ,   2     _     y     �     �     �  (   �     	     	     -	     3	  8   I	     �	     �	     �	  "   �	     �	     
     
     ;
     O
     d
     �
     �
  
   �
     �
     �
     �
  '   �
         %                      #      	                                         !                                    "   
                                                          $    A file named "%s" already exists in "%s" About Screenshot Apply _effect: Border C_opy to Clipboard Drop shadow Effects Error loading the help page Grab after a _delay of Grab the current _window Grab the whole sc_reen Help Include _pointer Include the window _border None Overwrite existing file? Quit Save Screenshot Save images of your screen or individual windows Save in _folder: Screenshot Screenshot delay Screenshot from %s - %d.%s Screenshot from %s.%s Screenshot.png Select _area to grab Take Screenshot Take _Screenshot Take a Screenshot of the Current Window Take a Screenshot of the Whole Screen _Name: effect filename seconds snapshot;capture;print; translator-credits Project-Id-Version: gnome-screenshot.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-08-30 15:22-0600
Last-Translator: Seán de Búrca <leftmostcat@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : 4;
 Tá comhad darb ainm "%s" ann i "%s" cheana Maidir le Gabháil Scáileáin Cuir m_aisíocht i bhfeidhm: Imlíne _Cóipeáil go dtí an Ghearrthaisce Cúlscáil Maisíochtaí Earráid agus leathanach cabhrach á luchtú Gabháil tar éis _moill: Gabháil an fhuinneog _reatha Gabháil an scáileán ar _fad Cabhair Cuir _pointeoir san áireamh Cuir _imlíne na fuinneoige san áireamh Neamhní Forscríobh comhad atá in ann? Scoir Sábháil Scáilghraf Sábháil íomhánna do scáileáin nó fhuinneog aonair Sábháil i bh_fillteán: Gabháil Scáileáin Moill scáilghraif Gabháil Scáileáin ó %s - %d.%s Gabháil Scáileáin ó %s.%s Gabháil Scáileáin.png Roghnaigh _achar le gabháil Gabháil Scáileán Gabháil _Scáileán Gabháil an Fhuinneog Reatha Gabháil an Scáileán ar Fad _Ainm: maisíocht ainmcomhaid soicind seat den scáileán;priontáil; Seán de Búrca <leftmostcat@gmail.com> 
��    3      �  G   L      h     i     r     �  
   �     �     �     �     �     �  	   �     �     �  *   �  %         F  
   N     Y     l     z  
   �     �     �     �     �  	   �     �  	   �     �     �                    #     4     N     T     g  "   p     �     �  	   �     �     �  
   �     �     �     �     �     �     �  �       �     �  
   �     �     �     �      �      	     9	     B	     O	     b	  .   u	  ;   �	     �	     �	     �	     
     (
     B
     P
     ]
     b
     v
  
   �
     �
     �
  
   �
     �
     �
  	   �
     �
     �
               #     @     I     g     y  
   ~     �     �     �     �  	   �     �     �     �  <   �                     !                        #                           1                    
      (   0   2   -   )      .                          $                 /          +      3   &       %   "   	                            ,           '   *    Advanced Allow bold font Audible bell Background Block Bottom Cl_ear scrollback Clear scr_ollback Cop_y Copy _URL Cursor blink Cursor style Disable menu shortcut key (F10 by default) Disable using Alt-n for tabs and menu Display Foreground Hide Close buttons Hide menu bar Hide scroll bar LXTerminal LXTerminal Preferences Left Move Tab _Left Move Tab _Right Na_me Tab Name Tab Ne_xt Tab New _Tab New _Window Pre_vious Tab Preference_s Right Scrollback lines Select-by-word characters Style Tab panel position Terminal Terminal emulator for LXDE project Terminal font Top Underline Use the command line _About _Close Tab _Edit _File _Help _Paste _Tabs translator-credits Project-Id-Version: LXTerminal 0.1.x
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-07-08 19:15+0000
Last-Translator: system user <>
Language-Team: 
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1404846950.000000
 Napredno Dozvoli zadebljan font Zvuk zvona Pozadina Blok Dole O_briši „scrollback“ ostavu Obriši „scr_ollback“ ostavu _Umnoži Umnoži _URL Pokazivač trepće Izgled pokazivača Isključi prečice za meni (F10 podrazumevano) Isključi upotrebu „Alt-n“ tastera u listovima i meniju Prikaz Boja teksta Sakrij  dugmad za zatvaranje Sakrij traku sa menijem Sakrij dugmad za klizanje LIks-Terminal Podešavanja Levo Premesti list _levo Premesti list _desno _Ime lista Dajte ime listu _Sledeći list Novi _list Novi _prozor _Prethodni list Po_stavke Desno Broj linija istorije Znakovi koji obrazuju reči Stil Smeštaj trake sa listovima: Terminal Emulator terminala za LIks-DE Font u terminalu: Gore Podvučeno Koristite liniju naredbi _O programu Za_tvori list _Uredi _Datoteka _Pomoć U_baci _Listovi Serbian (sr) — Jay A. Fleming <tito.nehru.naser@gmail.com> 
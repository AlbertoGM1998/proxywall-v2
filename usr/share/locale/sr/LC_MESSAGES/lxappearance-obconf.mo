��          �   %   �      p  :   q     �     �      �       !     j   >  F   �  [   �     L  $   j     �     �     �     �     �  �   �     �     �  	   �  #   �  O   �  �     $   �  #   �            �  ,  ^    	  (   	  0   �	  #   �	  ,   �	  /   *
  �   Z
  �   :  �   �  A   v  N   �  E     9   M  :   �  ;   �  
   �  "  	  &   ,     S     \  I   z  z   �  ,  ?  S   l  @   �  !     (   #                        
                                                                         	                          "%s" does not appear to be a valid Openbox theme directory "%s" was installed to %s "%s" was successfully created <span weight="bold">Theme</span> Choose an Openbox theme Create a theme _archive (.obt)... Error while parsing the Openbox configuration file. Your configuration file is not valid XML.

Message: %s Failed to load an rc.xml. Openbox is probably not installed correctly. Failed to load the obconf.glade interface file. ObConf is probably not installed correctly. Font for active window title: Font for inactive on-screen display: Font for inactive window title: Font for menu Item: Font for menu header: Font for on-screen display: Misc. N: Window icon
L: Window label (Title)
I: Iconify (Minimize)
M: Maximize
C: Close
S: Shade (Roll up)
D: Omnipresent (On all desktops) Openbox theme archives Theme Title Bar Unable to create directory "%s": %s Unable to create the theme archive "%s".
The following errors were reported:
%s Unable to extract the file "%s".
Please ensure that "%s" is writable and that the file is a valid Openbox theme archive.
The following errors were reported:
%s Unable to move to directory "%s": %s Unable to run the "tar" command: %s _Button order: _Install a new theme... Project-Id-Version: lxappearance-obconf
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-11-13 23:54+0100
PO-Revision-Date: 2015-05-01 11:50+0000
Last-Translator: Jay A. Fleming <tito.nehru.naser@gmail.com>
Language-Team: 
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1430481016.000000
 „%s“ изгледа није ваљан директоријум Опенбокс-теме „%s“ је постављено у %s „%s“ је успешно направљен. <span weight="bold">Тема</span> Одаберите Опенбокс-тему Направи _архиву теме (.obt)... Појавила се грешка приликом обраде датоке са подешавањима Опенбокса.  Ваша датотека није ваљана ИксМЛ датотека.

Порука: %s Неуспело учитавање „rc.xml“ датотеке. Највероватније нисте ваљано поставили Опенбокс. Неуспело учитавање „obconf.glade“ датотеке. Највероватније нисте ваљано поставили Обконф. Словолик за наслов активог прозора: Словолик за неактиван ОСД екрански приказ: Словолик за наслов неактивог прозора: Словолик за ставке у изборнику: Словолик за заглавље изборника: Словолик за ОСД екрански приказ: Разно N: Икона прозора
L: Натпис (Наслов прозора)
I: Дугме за умањивање
M: Дугме за увећевање
C: Дугме за затварање
S: Дугме за замотавање
D: Дугме свих радних површина (sticky) Архиве Опенбокс-теме Тема Насловна линија Не могу да направим директоријум „%s“: %s Не могу направити архиву теме „%s“.
Пријављене су следеће грешке:
%s Не могу да распакујем датотеку „%s“.
Обезбедите дозволе за писање у датотеку „%s“ и уверите се да је она ваљана архива Опенбокс-теме.
Пријављене су следеће грешке:
%s Не могу да се преместим у директоријум „%s“: %s Не могу да покренем „tar“ наредбу: %s _Редослед дугмади: Постав_ите нову тему... 
��          �   %   �      0     1     I     W  -   e  2   �  &   �     �     �     �           1     :  /   M     }     �     �     �     �     �     �     �  	   �  8   �  �       �     �     �  (   �  :   �  .   9     h     p      x  2   �     �     �  6   �     "     (     /     4     L     a     m     u     }  Q   �                                                           	                                            
                        <b>Character Repeat</b> <b>Motion</b> Acceleration: Beep when there is an error of keyboard input Configure keyboard, mouse, and other input devices Delay before each key starts repeating Fast High Input Device Preferences Interval between each key repeat Keyboard Keyboard and Mouse Left handed (Swap left and right mouse buttons) Long Low Mouse Repeat delay: Repeat interval: Sensitivity: Short Slow Touch Pad Type in the following box to test your keyboard settings Project-Id-Version: lxinput
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-08-17 01:29+0000
Last-Translator: Anonymous Pootle User
Language-Team: Slovak <sk-i18n@lists.linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Pootle 2.7
X-POOTLE-MTIME: 1439774942.078926
 <b>Opakovanie znakov</b> <b>Pohyb</b> Zrýchľovanie: Pípnutie pri chybe vstupu z klávesnice Nastaviť klávesnicu, myš a ostatné vstupné zariadenia Oneskorenie pred začatím opakovania klávesu Rýchle Vysoké Nastavenia vstupného zariadenia Interval medzi jednotlivými opakovaniami klávesu Klávesnica Klávesnica a myš Pre ľaváka (vymeniť pravé a ľavé tlačilo myši) Dlhé Nízke Myš Oneskorenie opakovania: Interval opakovania: Citlivosť: Krátke Pomalé Touchpad Ak chcete otestovať svoje nastavenia klávesnice, píšte do nasledovného poľa 
��    &      L  5   |      P     Q     V     Z  2   ^     �     �     �     �  (   �     �  :       >  S   D     �     �  ,   �     �     �  
             "     )  0   ;  )   l     �  ^   �               %     -     6     >     D     M     b  	   }     �  f  �      	     	     		  .   	  #   <	     `	     h	     n	  &   w	  *   �	  b  �	     ,  U   5  "   �     �  ?   �     �                -     B     I  -   \  '   �     �  O   �  
             ?  	   L     V  
   _  
   j     u     �     �     �                          
   $         %         	                                !          "                                                        #         &                        1:16 1:4 1:8 A part of an output is outside the virtual screen. ARandR Screen Layout Editor Accelerator Action Active An output is outside the virtual screen. Another XRandR GUI Click on a button in the left column and press a key combination you want to bind to a certain screen layout. (Use backspace to clear accelerators, escape to abort editing.) Then, select one or more layouts in the right column.

This will only work if you use metacity or another program reading its configuration. Dummy In order to configure metacity, you need to have the python gconf module installed. Keybindings (via Metacity) New accelerator... No files in %(folder)r. Save a layout first. Open Layout Orientation Resolution Save Layout Script Script Properties Setting this resolution is not possible here: %s This orientation is not possible here: %s XRandR failed:
%s Your configuration does not include an active monitor. Do you want to apply the configuration? _Help _Keybindings (Metacity) _Layout _Outputs _System _View disabled gconf not available. incompatible configuration no action other application Project-Id-Version: arandr 0.1.3
Report-Msgid-Bugs-To: chrysn@fsfe.org
PO-Revision-Date: 2011-07-15 19:36+0200
Last-Translator: Slavko <linux@slavino.sk>
Language-Team: Slovak <nomail>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 1:16 1:4 1:8 Časť výstupu je mimo virtuálnej obrazovky. Editor rozloženia obrazovky ARandR Skratka Akcia Aktívne Výstup je mimo virtuálnej obrazovky. Another XRandR GUI (ďalšie GUI k XRandR) Kliknite na tlačidlo v ľavom stĺpci a stlačte kombináciu klávesov, ktorú chcete zviazať s určitým rozložením obrazovky. (Použite  backspace na zmazanie skratiek, ESC na zrušenie úprav.) Potom vyberte jeden alebo viac rozložení v pravom stĺpci.

Toto funguje len ak používate metacity alebo iný program, ktorý číta jeho nastavenia. Prázdne Aby ste mohli nastavovať metacity, musíte mať nainštalovaný modul Pythonu gconf. Klávesové Skratky (cez Metacity) Nová skratka... Žiadne súbory v %(folder)r. Najprv uložte svoje rozloženie. Otvoriť rozloženie Orientácia Rozlíšenie Uložiť rozloženie Skript Vlastnosti skriptu Nastavenie rozlíšenia tu nie je možné: %s Táto orientácia tu nie je možná: %s XRandR zlyhal:
%s Vaše nastavenie nezahŕňa aktívny monitor. Chcete i tak použiť nastavenie? _Pomocník _Klávesové skratky (Metacity) _Rozloženie _Výstupy _Systém _Zobraziť zakázané gconf nie je dostupný. nekompatibilné nastavenie žiadna akcia iná aplikácia 
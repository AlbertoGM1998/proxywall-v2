document.getElementById("Contents").innerHTML='\
    <ul><li><input type="checkbox" id="07"><label for="07">Macros and Programming</label><ul>\
    <li><input type="checkbox" id="0701"><label for="0701">General Information and User Interface Usage</label><ul>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/main0601.html">LibreOffice Basic Help</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01000000.html">Programming with LibreOffice Basic</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/00000002.html">LibreOffice Basic Glossary</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01010210.html">Basics</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01020000.html">Syntax</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01050000.html">LibreOffice Basic IDE</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01030100.html">IDE Overview</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01030200.html">The Basic Editor</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01050100.html">Watch Window</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/main0211.html">Macro Toolbar</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/05060700.html">Macro</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/vbasupport.html">Support for VBA Macros</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0702"><label for="0702">Command Reference</label><ul>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01020300.html">Using Procedures and Functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01020500.html">Libraries, Modules and Dialogs</a></li>\
    <li><input type="checkbox" id="070202"><label for="070202">Functions, Statements and Operators</label><ul>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010000.html">Screen I/O Functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020000.html">File I/O Functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030000.html">Date and Time Functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03050000.html">Error-Handling Functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03060000.html">Logical Operators</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03070000.html">Mathematical Operators</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080000.html">Numeric Functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090000.html">Controlling Program Execution</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100000.html">Variables</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03040000.html">Basic Constants</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03110000.html">Comparison Operators</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120000.html">Strings</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/special_vba_func.html">Exclusive VBA functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03130000.html">Other Commands</a></li>\
			</ul></li>\
    <li><input type="checkbox" id="070201"><label for="070201">Alphabetic List of Functions, Statements and Operators</label><ul>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080601.html">Abs Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03060100.html">AND Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03104200.html">Array Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120101.html">Asc Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120111.html">AscW Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080101.html">Atn Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03130100.html">Beep Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010301.html">Blue Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100100.html">CBool Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120105.html">CByte Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100050.html">CCur Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030116.html">CDateFromUnoDateTime Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030115.html">CDateToUnoDateTime Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030114.html">CDateFromUnoTime Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030113.html">CDateToUnoTime Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030112.html">CDateFromUnoDate Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030111.html">CDateToUnoDate Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030108.html">CDateFromIso Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030107.html">CDateToIso Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100300.html">CDate Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100400.html">CDbl Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100060.html">CDec Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100500.html">CInt Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100600.html">CLng Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100900.html">CSng Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03101000.html">CStr Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090401.html">Call Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020401.html">ChDir Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020402.html">ChDrive Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090402.html">Choose Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120102.html">Chr Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120112.html">ChrW Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020101.html">Close Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03110100.html">Comparison Operators</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100700.html">Const Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120313.html">ConvertFromURL Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120312.html">ConvertToURL Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080102.html">Cos Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03132400.html">CreateObject Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03131800.html">CreateUnoDialog Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03132000.html">CreateUnoListener Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03131600.html">CreateUnoService Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03131500.html">CreateUnoStruct Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03132300.html">CreateUnoValue Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020403.html">CurDir Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100070.html">CVar Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03100080.html">CVErr Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030110.html">DateAdd Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030120.html">DateDiff Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030130.html">DatePart Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030101.html">DateSerial Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030102.html">DateValue Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030301.html">Date Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030103.html">Day Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140000.html">DDB Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090403.html">Declare Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03101100.html">DefBool Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03101300.html">DefDate Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03101400.html">DefDbl Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03101500.html">DefInt Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03101600.html">DefLng Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03101700.html">DefObj Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102000.html">DefVar Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03104300.html">DimArray Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102100.html">Dim Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020404.html">Dir Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090201.html">Do...Loop Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03110100.html">Comparison Operators</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090404.html">End Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03130800.html">Environ Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020301.html">Eof Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03104600.html">EqualUnoObjects Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03060200.html">Eqv Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03050100.html">Erl Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03050200.html">Err Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03050300.html">Error Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03050000.html">Error-Handling Functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090412.html">Exit Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080201.html">Exp Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020405.html">FileAttr Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020406.html">FileCopy Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020407.html">FileDateTime Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020415.html">FileExists Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020408.html">FileLen Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103800.html">FindObject Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103900.html">FindPropertyObject Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080501.html">Fix Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090202.html">For...Next Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120301.html">Format Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03150000.html">FormatDateTime Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020102.html">FreeFile Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090405.html">FreeLibrary Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090406.html">Function Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090400.html">Further Statements</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140001.html">FV Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080300.html">Generating Random Numbers</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020409.html">GetAttr Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03132500.html">GetDefaultContext Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03132100.html">GetGuiType Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03131700.html">GetProcessServiceManager Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03131000.html">GetSolarVersion Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03130700.html">GetSystemTicks Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020201.html">Get Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03131900.html">GlobalScope</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090301.html">GoSub...Return Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090302.html">GoTo Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010302.html">Green Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03104400.html">HasUnoInterfaces Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080801.html">Hex Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030201.html">Hour Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090103.html">IIf Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090101.html">If...Then...Else Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03060300.html">Imp Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120401.html">InStr Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120411.html">InStrRev Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03160000.html">Input Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010201.html">InputBox Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020202.html">Input# Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080502.html">Int Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140002.html">IPmt Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140003.html">IRR Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102200.html">IsArray Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102300.html">IsDate Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102400.html">IsEmpty Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03104000.html">IsMissing function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102600.html">IsNull Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102700.html">IsNumeric Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102800.html">IsObject Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03104500.html">IsUnoStruct Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120315.html">Join Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020410.html">Kill Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102900.html">LBound Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120302.html">LCase Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120304.html">LSet Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120305.html">LTrim Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120303.html">Left Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120402.html">Len Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103100.html">Let Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020203.html">Line Input # Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020302.html">Loc Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020303.html">Lof Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080202.html">Log Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120306.html">Mid Function, Mid Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030202.html">Minute Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140004.html">MIRR Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020411.html">MkDir Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03070600.html">Mod Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030104.html">Month Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03150002.html">MonthName Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010102.html">MsgBox Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010101.html">MsgBox Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020412.html">Name Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03060400.html">Not Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030203.html">Now Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140005.html">NPer Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140006.html">NPV Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080000.html">Numeric Functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080802.html">Oct Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03050500.html">On Error GoTo ... Resume Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090303.html">On...GoSub Statement; On...GoTo Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020103.html">Open Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103200.html">Option Base Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103300.html">Option Explicit Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103350.html">Option VBASupport Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03104100.html">Optional (in Function Statement)</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03060500.html">Or Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140007.html">Pmt Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140008.html">PPmt Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140009.html">PV Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010103.html">Print Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103400.html">Public Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020204.html">Put Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010304.html">QBColor Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140010.html">Rate Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010305.html">RGB Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120308.html">RSet Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120309.html">RTrim Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080301.html">Randomize Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03102101.html">ReDim Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03010303.html">Red Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090407.html">Rem Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020104.html">Reset Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120307.html">Right Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020413.html">RmDir Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080302.html">Rnd Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03170000.html">Round Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030204.html">Second Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020304.html">Seek Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020305.html">Seek Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090102.html">Select...Case Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020414.html">SetAttr Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103700.html">Set Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080701.html">Sgn Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03130500.html">Shell Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080103.html">Sin Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140011.html">SLN Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120201.html">Space Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120314.html">Split Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080401.html">Sqr Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080400.html">Square Root Calculation</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103500.html">Static Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090408.html">Stop Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120403.html">StrComp Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120103.html">Str Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120412.html">StrReverse Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120202.html">String Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090409.html">Sub Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090410.html">Switch Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03140012.html">SYD Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080104.html">Tan Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030205.html">TimeSerial Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030206.html">TimeValue Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030302.html">Time Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030303.html">Timer Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03080100.html">Trigonometric Functions</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120311.html">Trim Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03131300.html">TwipsPerPixelX Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03131400.html">TwipsPerPixelY Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090413.html">Type Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103600.html">TypeName Function; VarType Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03103000.html">UBound Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120310.html">UCase Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03120104.html">Val Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03130600.html">Wait Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030105.html">WeekDay Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03150001.html">WeekdayName Function [VBA]</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090203.html">While...Wend Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03090411.html">With Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03020205.html">Write Statement</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03060600.html">XOR Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03030106.html">Year Function</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03070100.html">"-" Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03070200.html">"*" Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03070300.html">"+" Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03070400.html">"/" Operator</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03070500.html">"^" Operator</a></li>\
			</ul></li>\
    <li><input type="checkbox" id="070205"><label for="070205">Advanced Basic Libraries</label><ul>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03/lib_tools.html">Tools Library</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03/lib_depot.html">DEPOT Library</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03/lib_euro.html">EURO Library</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03/lib_formwizard.html">FORMWIZARD Library</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03/lib_gimmicks.html">GIMMICKS Library</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03/lib_schedule.html">SCHEDULE Library</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03/lib_script.html">SCRIPTBINDINGLIBRARY Library</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/03/lib_template.html">TEMPLATE Library</a></li>\
			</ul></li>\
                    </ul></li>\
    <li><input type="checkbox" id="0703"><label for="0703">Guides</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/macro_recording.html">Recording a Macro</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/guide/control_properties.html">Changing the Properties of Controls in the Dialog Editor</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/guide/insert_control.html">Creating Controls in the Dialog Editor</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/guide/sample_code.html">Programming Examples for Controls in the Dialog Editor</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/guide/show_dialog.html">Opening a Dialog With Program Code</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/guide/create_dialog.html">Creating a Basic Dialog</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01030400.html">Organising Libraries and Modules</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01020100.html">Using Variables</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01020200.html">Using Objects</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01030300.html">Debugging a Basic Program</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/shared/01040000.html">Event-Driven Macros</a></li>\
    <li><a target="_top" href="en-GB/text/sbasic/guide/access2base.html">Access2Base</a></li>\
		</ul></li>\
	</ul></li></ul>\
    <ul><li><input type="checkbox" id="08"><label for="08">Spreadsheets</label><ul>\
    <li><input type="checkbox" id="0801"><label for="0801">General Information and User Interface Usage</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/main0000.html">Welcome to the LibreOffice Calc Help</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0503.html">LibreOffice Calc Features</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/keyboard.html">Shortcut Keys (LibreOffice Calc Accessibility)</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/04/01020000.html">Shortcut Keys for Spreadsheets</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/05/02140000.html">Error Codes in LibreOffice Calc</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060112.html">Add-in for Programming in LibreOffice Calc</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/main.html">Instructions for Using LibreOffice Calc</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0802"><label for="0802">Command and Menu Reference</label><ul>\
    <li><input type="checkbox" id="080201"><label for="080201">Menus</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/main0100.html">Menus</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0101.html">File</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0102.html">Edit</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0103.html">View</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0104.html">Insert</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0105.html">Format</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0116.html">Sheet</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0112.html">Data</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0106.html">Tools</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0107.html">Window</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0108.html">Help</a></li>\
			</ul></li>\
    <li><input type="checkbox" id="080202"><label for="080202">Toolbars</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/main0200.html">Toolbars</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0202.html">Formatting Bar</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0203.html">Drawing Object Properties Bar</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0205.html">Text Formatting Bar</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0206.html">Formula Bar</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0208.html">Status Bar</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0210.html">Print Preview Bar</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0214.html">Image Bar</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/main0218.html">Tools Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0201.html">Standard Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0212.html">Table Data Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0213.html">Form Navigation Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0214.html">Query Design Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0226.html">Form Design Toolbar</a></li>\
			</ul></li>\
		</ul></li>\
    <li><input type="checkbox" id="0803"><label for="0803">Functions Types and Operators</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060000.html">Function Wizard</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060100.html">Functions by Category</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060107.html">Array Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060120.html">Bit Operation Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060101.html">Database Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060102.html">Date & Time Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060103.html">Financial Functions Part One</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060119.html">Financial Functions Part Two</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060118.html">Financial Functions Part Three</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060104.html">Information Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060105.html">Logical Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060106.html">Mathematical Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060108.html">Statistics Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060181.html">Statistical Functions Part One</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060182.html">Statistical Functions Part Two</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060183.html">Statistical Functions Part Three</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060184.html">Statistical Functions Part Four</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060185.html">Statistical Functions Part Five</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060109.html">Spreadsheet Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060110.html">Text Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060111.html">Add-in Functions</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060115.html">Add-in Functions, List of Analysis Functions Part One</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060116.html">Add-in Functions, List of Analysis Functions Part Two</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/01/04060199.html">Operators in LibreOffice Calc</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/userdefined_function.html">User-Defined Functions</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0804"><label for="0804">Loading, Saving, Importing and Exporting</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/webquery.html">Inserting External Data in Table (WebQuery)</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/html_doc.html">Saving and Opening Sheets in HTML</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/csv_formula.html">Importing and Exporting Text Files</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0805"><label for="0805">Formatting</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/text_rotate.html">Rotating Text</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/text_wrap.html">Writing Multi-line Text</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/text_numbers.html">Formatting Numbers as Text</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/super_subscript.html">Text Superscript / Subscript</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/row_height.html">Changing Row Height or Column Width</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/cellstyle_conditional.html">Applying Conditional Formatting</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/cellstyle_minusvalue.html">Highlighting Negative Numbers</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/cellstyle_by_formula.html">Assigning Formats by Formula</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/integer_leading_zero.html">Entering a Number with Leading Zeros</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/format_table.html">Formatting Spreadsheets</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/format_value.html">Formatting Numbers With Decimals</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/value_with_name.html">Naming Cells</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/table_rotate.html">Rotating Tables (Transposing)</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/rename_table.html">Renaming Sheets</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/year2000.html">19xx/20xx Years</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/rounding_numbers.html">Using Rounded Off Numbers</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/currency_format.html">Cells in Currency Format</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/autoformat.html">Using AutoFormat for Tables</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/note_insert.html">Inserting and Editing Comments</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/design.html">Selecting Themes for Sheets</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/fraction_enter.html">Entering Fractions</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0806"><label for="0806">Filtering and Sorting</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/filters.html">Applying Filters</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/specialfilter.html">Filter: Applying Advanced Filters</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/autofilter.html">Applying AutoFilter</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/sorted_list.html">Applying Sort Lists</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0807"><label for="0807">Printing</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/print_title_row.html">Printing Rows or Columns on Every Page</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/print_landscape.html">Printing Sheets in Landscape Format</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/print_details.html">Printing Sheet Details</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/print_exact.html">Defining Number of Pages for Printing</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0808"><label for="0808">Data Ranges</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/database_define.html">Defining Database Ranges</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/database_filter.html">Filtering Cell Ranges</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/database_sort.html">Sorting Data</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0809"><label for="0809">Pivot Table</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/datapilot.html">Pivot Table</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/datapilot_createtable.html">Creating Pivot Tables</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/datapilot_deletetable.html">Deleting Pivot Tables</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/datapilot_edittable.html">Editing Pivot Tables</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/datapilot_filtertable.html">Filtering Pivot Tables</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/datapilot_tipps.html">Selecting Pivot Table Output Ranges</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/datapilot_updatetable.html">Updating Pivot Tables</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="08091"><label for="08091">Pivot Chart</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/pivotchart.html">Pivot Chart</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/pivotchart_create.html">Creating Pivot Charts</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/pivotchart_edit.html">Editing Pivot Charts</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/pivotchart_filter.html">Filtering Pivot Charts</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/pivotchart_update.html">Pivot Chart Update</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/pivotchart_delete.html">Deleting Pivot Charts</a></li>\
                </ul></li>\
    <li><input type="checkbox" id="0810"><label for="0810">Scenarios</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/scenario.html">Using Scenarios</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0811"><label for="0811">References</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/relativ_absolut_ref.html">Addresses and References, Absolute and Relative</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/cellreferences.html">Referencing a Cell in Another Document</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/cellreferences_url.html">References to Other Sheets and Referencing URLs</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/cellreference_dragdrop.html">Referencing Cells by Drag-and-Drop</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/address_auto.html">Recognising Names as Addressing</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0812"><label for="0812">Viewing, Selecting, Copying</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/table_view.html">Changing Table Views</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/formula_value.html">Displaying Formulae or Values</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/line_fix.html">Freezing Rows or Columns as Headers</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/multi_tables.html">Navigating Through Sheet Tabs</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/edit_multitables.html">Copying to Multiple Sheets</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/cellcopy.html">Only Copy Visible Cells</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/mark_cells.html">Selecting Multiple Cells</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0813"><label for="0813">Formulae and Calculations</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/formulas.html">Calculating With Formulae</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/formula_copy.html">Copying Formulae</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/formula_enter.html">Entering Formulae</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/formula_value.html">Displaying Formulae or Values</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/calculate.html">Calculating in Spreadsheets</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/calc_date.html">Calculating With Dates and Times</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/calc_series.html">Automatically Calculating Series</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/calc_timevalues.html">Calculating Time Differences</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/matrixformula.html">Entering Matrix Formulae</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0814"><label for="0814">Protection</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/cell_protect.html">Protecting Cells from Changes</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/cell_unprotect.html">Unprotecting Cells</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0815"><label for="0815">Miscellaneous</label><ul>\
    <li><a target="_top" href="en-GB/text/scalc/guide/auto_off.html">Deactivating Automatic Changes</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/consolidate.html">Consolidating Data</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/goalseek.html">Applying Goal Seek</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/multioperation.html">Applying Multiple Operations</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/multitables.html">Applying Multiple Sheets</a></li>\
    <li><a target="_top" href="en-GB/text/scalc/guide/validity.html">Validity of Cell Content</a></li>\
		</ul></li>\
	</ul></li></ul>\
    <ul><li><input type="checkbox" id="05"><label for="05">Charts and Diagrams</label><ul>\
    <li><input type="checkbox" id="0501"><label for="0501">General Information</label><ul>\
    <li><a target="_top" href="en-GB/text/schart/main0000.html">Charts in LibreOffice</a></li>\
    <li><a target="_top" href="en-GB/text/schart/main0503.html">LibreOffice Chart Features</a></li>\
    <li><a target="_top" href="en-GB/text/schart/04/01020000.html">Shortcuts for Charts</a></li>\
		</ul></li>\
	</ul></li></ul>\
    <ul><li><input type="checkbox" id="01"><label for="01">Installation</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/ms_doctypes.html">Changing the Association of Microsoft Office Document Types</a></li>\
    <li><a target="_top" href="en-GB/text/shared/01/profile_safe_mode.html">Safe Mode</a></li>\
	</ul></li></ul>\
    <ul><li><input type="checkbox" id="10"><label for="10">Common Help Topics</label><ul>\
    <li><input type="checkbox" id="1001"><label for="1001">General Information</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/main0400.html">Shortcut Keys</a></li>\
    <li><a target="_top" href="en-GB/text/shared/00/00000005.html">General Glossary</a></li>\
    <li><a target="_top" href="en-GB/text/shared/00/00000002.html">Glossary of Internet Terms</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/accessibility.html">Accessibility in LibreOffice</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/keyboard.html">Shortcuts (LibreOffice Accessibility)</a></li>\
    <li><a target="_top" href="en-GB/text/shared/04/01010000.html">General Shortcut Keys in LibreOffice</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/version_number.html">Versions and Build Numbers</a></li>\
</ul></li>\
    <li><input type="checkbox" id="1002"><label for="1002">LibreOffice and Microsoft Office</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/ms_user.html">Using Microsoft Office and LibreOffice</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/microsoft_terms.html">Comparing Microsoft Office and LibreOffice Terms</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/ms_import_export_limitations.html">About Converting Microsoft Office Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/ms_doctypes.html">Changing the Association of Microsoft Office Document Types</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1004"><label for="1004">LibreOffice Options</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01000000.html">Options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01010100.html">User Data</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01010200.html">General</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01010300.html">Paths</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01010400.html">Writing Aids</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01010500.html">Colour</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01010600.html">General</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01010700.html">Fonts</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01010800.html">View</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01010900.html">Print Options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01012000.html">Application Colours</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01013000.html">Accessibility</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/java.html">Advanced</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/expertconfig.html">Expert Configuration</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/persona_firefox.html">Personalisation</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/BasicIDE.html">Basic IDE</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/opencl.html">Open CL</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01020000.html">Load/Save options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01030000.html">Internet options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01040000.html">Text Document Options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01050000.html">HTML Document Options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01060000.html">Spreadsheet Options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01070000.html">Presentation Options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01080000.html">Drawing Options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01090000.html">Formula</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01110000.html">Chart options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01130100.html">VBA Properties</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01130200.html">Microsoft Office</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01140000.html">Languages</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01150000.html">Language Setting Options</a></li>\
    <li><a target="_top" href="en-GB/text/shared/optionen/01160000.html">Data sources options</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1005"><label for="1005">Wizards</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/autopi/01000000.html">Wizard</a></li>\
    <li><input type="checkbox" id="100501"><label for="100501">Letter Wizard</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/autopi/01010000.html">Letter Wizard</a></li>\
			</ul></li>\
    <li><input type="checkbox" id="100502"><label for="100502">Fax Wizard</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/autopi/01020000.html">Fax Wizard</a></li>\
			</ul></li>\
    <li><input type="checkbox" id="100504"><label for="100504">Agenda Wizard</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/autopi/01040000.html">Agenda Wizard</a></li>\
			</ul></li>\
    <li><input type="checkbox" id="100506"><label for="100506">HTML Export Wizard</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/autopi/01110000.html">HTML Export</a></li>\
			</ul></li>\
    <li><input type="checkbox" id="100510"><label for="100510">Document Converter Wizard</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/autopi/01130000.html">Document Converter</a></li>\
			</ul></li>\
    <li><a target="_top" href="en-GB/text/shared/autopi/01150000.html">Euro Converter Wizard</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1006"><label for="1006">Configuring LibreOffice</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/configure_overview.html">Configuring LibreOffice</a></li>\
    <li><a target="_top" href="en-GB/text/shared/01/packagemanager.html">Extension Manager</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/flat_icons.html">Changing Icon Views</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/edit_symbolbar.html">Adding Buttons to Toolbars</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/workfolder.html">Changing Your Working Directory</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/standard_template.html">Changing Default Templates</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_addressbook.html">Registering an Address Book</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/formfields.html">Inserting and Editing Buttons</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1007"><label for="1007">Working with the User Interface</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/navigator_setcursor.html">Navigation to Quickly Reach Objects</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/navigator.html">Navigator for Document Overview</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/autohide.html">Showing, Docking and Hiding Windows</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/navpane_on.html">Showing Navigation Pane of the Help</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/textmode_change.html">Switching Between Insert Mode and Overwrite Mode</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/floating_toolbar.html">Using Toolbars</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="10071"><label for="10071">Digital Signatures</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/digital_signatures.html">About Digital Signatures</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/digitalsign_send.html">Applying Digital Signatures</a></li>\
    <li><a target="_top" href="en-GB/text/shared/01/digitalsignaturespdf.html">Digital Signature in PDF Export</a></li>\
    <li><a target="_top" href="en-GB/text/shared/01/signexistingpdf.html">Signing Existing PDF</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/01/addsignatureline.html">Adding Signature Line in Text Documents</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/01/signsignatureline.html">Signing the Signature Line</a></li>\
                </ul></li>\
    <li><input type="checkbox" id="1008"><label for="1008">Printing, Faxing, Sending</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/labels_database.html">Printing Address Labels</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/print_blackwhite.html">Printing in Black and White</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/email.html">Sending Documents as E-mail</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/fax.html">Sending Faxes and Configuring LibreOffice for Faxing</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1009"><label for="1009">Drag & Drop</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/dragdrop.html">Dragging-and-Dropping Within a LibreOffice Document</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/dragdroptext.html">Moving and Copying Text in Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/dragdrop_table.html">Copying Spreadsheet Areas to Text Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/dragdrop_graphic.html">Copying Graphics Between Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/dragdrop_fromgallery.html">Copying Graphics From the Gallery</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/dragdrop_beamer.html">Drag-and-Drop With the Data Source View</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1010"><label for="1010">Copy and Paste</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/copy_drawfunctions.html">Copying Drawing Objects Into Other Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/dragdrop_graphic.html">Copying Graphics Between Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/dragdrop_fromgallery.html">Copying Graphics From the Gallery</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/dragdrop_table.html">Copying Spreadsheet Areas to Text Documents</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1012"><label for="1012">Charts and Diagrams</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/chart_insert.html">Inserting Charts</a></li>\
    <li><a target="_top" href="en-GB/text/schart/main0000.html">Charts in LibreOffice</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1013"><label for="1013">Load, Save, Import, Export</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/doc_open.html">Opening Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/import_ms.html">Opening documents saved in other formats</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/doc_save.html">Saving Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/doc_autosave.html">Saving Documents Automatically</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/export_ms.html">Saving Documents in Other Formats</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_dbase2office.html">Importing and Exporting Data in Text Format</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1014"><label for="1014">Links and References</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/hyperlink_insert.html">Inserting Hyperlinks</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/hyperlink_rel_abs.html">Relative and Absolute Links</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/hyperlink_edit.html">Editing Hyperlinks</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1015"><label for="1015">Document Version Tracking</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/redlining_doccompare.html">Comparing Versions of a Document</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/redlining_docmerge.html">Merging Versions</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/redlining_enter.html">Recording Changes</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/redlining.html">Recording and Displaying Changes</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/redlining_accept.html">Accepting or Rejecting Changes</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/redlining_versions.html">Version Management</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1016"><label for="1016">Labels and Business Cards</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/labels.html">Creating and Printing Labels and Business Cards</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1018"><label for="1018">Inserting External Data</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/copytable2application.html">Inserting Data From Spreadsheets</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/copytext2application.html">Inserting Data From Text Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/insert_bitmap.html">Inserting, Editing, Saving Bitmaps</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/dragdrop_gallery.html">Adding Graphics to the Gallery</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1019"><label for="1019">Automatic Functions</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/autocorr_url.html">Turning off Automatic URL Recognition</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1020"><label for="1020">Searching and Replacing</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_search2.html">Searching With a Form Filter</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_search.html">Searching Tables and Form Documents</a></li>\
    <li><a target="_top" href="en-GB/text/shared/01/02100001.html">List of Regular Expressions</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="1021"><label for="1021">Guides</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/guide/linestyles.html">Applying Line Styles</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/text_color.html">Changing the Colour of Text</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/change_title.html">Changing the Title of a Document</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/round_corner.html">Creating Round Corners</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/background.html">Defining Background Colours or Background Graphics</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/lineend_define.html">Defining Line Ends</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/linestyle_define.html">Defining Line Styles</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/insert_graphic_drawit.html">Editing Graphic Objects</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/line_intext.html">Drawing Lines in Text</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/aaa_start.html">First Steps</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/gallery_insert.html">Inserting Objects From the Gallery</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/space_hyphen.html">Inserting Non-breaking Spaces, Hyphens and Soft Hyphens</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/insert_specialchar.html">Inserting Special Characters</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/tabs.html">Inserting and Editing Tab Stops</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/protection.html">Protecting Content in LibreOffice</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/redlining_protect.html">Protecting Records</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/pageformat_max.html">Selecting the Maximum Printable Area on a Page</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/measurement_units.html">Selecting Measurement Units</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/language_select.html">Selecting the Document Language</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_tabledefine.html">Table Design</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/numbering_stop.html">Turning off Bullets and Numbering for Individual Paragraphs</a></li>\
		</ul></li>\
	</ul></li></ul>\
    <ul><li><input type="checkbox" id="09"><label for="09">Database Functionality</label><ul>\
    <li><input type="checkbox" id="0901"><label for="0901">General Information</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/explorer/database/main.html">LibreOffice Database</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/database_main.html">Database Overview</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_new.html">Creating a New Database</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_tables.html">Working with Tables</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_queries.html">Working with Queries</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_forms.html">Working with Forms</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_reports.html">Creating Reports</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_register.html">Registering and Deleting a Database</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_im_export.html">Importing and Exporting Data in Base</a></li>\
    <li><a target="_top" href="en-GB/text/shared/guide/data_enter_sql.html">Executing SQL Commands</a></li>\
		</ul></li>\
	</ul></li></ul>\
    <ul><li><input type="checkbox" id="04"><label for="04">Presentations and Drawings</label><ul>\
    <li><input type="checkbox" id="0401"><label for="0401">General Information and User Interface Usage</label><ul>\
    <li><a target="_top" href="en-GB/text/simpress/main0000.html">Welcome to the LibreOffice Impress Help</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0000.html">Welcome to the LibreOffice Draw Help</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0503.html">LibreOffice Impress Features</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0503.html">LibreOffice Draw Features</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/keyboard.html">Using Shortcut Keys in LibreOffice Impress</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/keyboard.html">Shortcut Keys for Drawing Objects</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/04/01020000.html">Shortcut Keys for LibreOffice Impress</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/04/presenter.html">Presenter Console Keyboard Shortcuts</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/04/01020000.html">Shortcut Keys for Drawings</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/main.html">Instructions for Using LibreOffice Impress</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/main.html">Instructions for Using LibreOffice Draw</a></li>\
         </ul></li>\
    <li><input type="checkbox" id="0402"><label for="0402">Command and Menu Reference</label><ul>\
    <li><input type="checkbox" id="040201"><label for="040201">Presentations (LibreOffice Impress)</label><ul>\
    <li><input type="checkbox" id="04020101"><label for="04020101">Menus</label><ul>\
    <li><a target="_top" href="en-GB/text/simpress/main0100.html">Menus</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0101.html">File</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0102.html">Edit</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0103.html">View</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0104.html">Insert</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0105.html">Format</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0117.html">Slide</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0114.html">Slide Show</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0106.html">Tools</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0107.html">Window</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0108.html">Help</a></li>\
                 </ul></li>\
    <li><input type="checkbox" id="04020102"><label for="04020102">Toolbars</label><ul>\
    <li><a target="_top" href="en-GB/text/simpress/main0200.html">Toolbars</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0202.html">Line and Filling Bar</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0203.html">Text Formatting Bar</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0204.html">Slide View Bar</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0206.html">Status Bar</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0209.html">Rulers</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0210.html">Drawing Bar</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0211.html">Outline Bar</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0212.html">Slide Sorter Bar</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0213.html">Options Bar</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0214.html">Image Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0201.html">Standard Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0213.html">Form Navigation Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0226.html">Form Design Toolbar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0227.html">Edit Points Bar</a></li>\
                 </ul></li>\
             </ul></li>\
    <li><input type="checkbox" id="040202"><label for="040202">Drawings (LibreOffice Draw)</label><ul>\
    <li><input type="checkbox" id="04020201"><label for="04020201">Menus</label><ul>\
    <li><a target="_top" href="en-GB/text/sdraw/main0100.html">Menus</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0101.html">File</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0102.html">Edit</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0103.html">View</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0104.html">Insert</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0105.html">Format</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0106.html">Tools</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0113.html">Modify</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/main0107.html">Window</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0108.html">Help</a></li>\
                 </ul></li>\
    <li><input type="checkbox" id="04020202"><label for="04020202">Toolbars</label><ul>\
    <li><a target="_top" href="en-GB/text/sdraw/main0200.html">Toolbars</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0210.html">Drawing Bar</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/main0213.html">Options Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0201.html">Standard Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0213.html">Form Navigation Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0226.html">Form Design Toolbar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0227.html">Edit Points Bar</a></li>\
                 </ul></li>\
             </ul></li>\
         </ul></li>\
    <li><input type="checkbox" id="0403"><label for="0403">Loading, Saving, Importing and Exporting</label><ul>\
    <li><a target="_top" href="en-GB/text/simpress/guide/html_export.html">Saving a Presentation in HTML Format</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/html_import.html">Importing HTML Pages Into Presentations</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/palette_files.html">Loading Colour, Gradient, and Hatching Lists</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/animated_gif_save.html">Exporting Animations in GIF Format</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/table_insert.html">Including Spreadsheets in Slides</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/graphic_insert.html">Inserting Graphics</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/page_copy.html">Copying Slides From Other Presentations</a></li>\
         </ul></li>\
    <li><input type="checkbox" id="0404"><label for="0404">Formatting</label><ul>\
    <li><a target="_top" href="en-GB/text/simpress/guide/palette_files.html">Loading Colour, Gradient, and Hatching Lists</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/line_arrow_styles.html">Loading Line and Arrow Styles</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/color_define.html">Defining Custom Colours</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/gradient.html">Creating Gradient Fills</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/eyedropper.html">Replacing Colours</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/align_arrange.html">Arranging, Aligning and Distributing Objects</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/background.html">Changing the Slide Background Fill</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/footer.html">Adding a Header or a Footer to All Slides</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/masterpage.html">Applying a Slide Design to a Master Slide</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/move_object.html">Moving Objects</a></li>\
         </ul></li>\
    <li><input type="checkbox" id="0405"><label for="0405">Printing</label><ul>\
    <li><a target="_top" href="en-GB/text/simpress/guide/printing.html">Printing Presentations</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/print_tofit.html">Printing a Slide to Fit a Paper Size</a></li>\
         </ul></li>\
    <li><input type="checkbox" id="0406"><label for="0406">Effects</label><ul>\
    <li><a target="_top" href="en-GB/text/simpress/guide/animated_gif_save.html">Exporting Animations in GIF Format</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/animated_objects.html">Animating Objects in Presentation Slides</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/animated_slidechange.html">Animating Slide Transitions</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/cross_fading.html">Cross-Fading Two Objects</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/animated_gif_create.html">Creating Animated GIF Images</a></li>\
         </ul></li>\
    <li><input type="checkbox" id="0407"><label for="0407">Objects, Graphics and Bitmaps</label><ul>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/combine_etc.html">Combining Objects and Constructing Shapes</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/draw_sector.html">Drawing Sectors and Segments</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/duplicate_object.html">Duplicating Objects</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/rotate_object.html">Rotating Objects</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/join_objects3d.html">Assembling 3-D Objects</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/join_objects.html">Connecting Lines</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/text2curve.html">Converting Text Characters into Drawing Objects</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/vectorize.html">Converting Bitmap Images into Vector Graphics</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/3d_create.html">Converting 2-D Objects to Curves, Polygons, and 3-D Objects</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/line_arrow_styles.html">Loading Line and Arrow Styles</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/line_draw.html">Drawing Curves</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/line_edit.html">Editing Curves</a></li>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/graphic_insert.html">Inserting Graphics</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/table_insert.html">Including Spreadsheets in Slides</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/move_object.html">Moving Objects</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/select_object.html">Selecting Underlying Objects</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/orgchart.html">Creating a Flowchart</a></li>\
         </ul></li>\
    <li><input type="checkbox" id="0408"><label for="0408">Groups and Layers</label><ul>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/groups.html">Grouping Objects</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/layers.html">About Layers</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/layer_new.html">Inserting Layers</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/layer_tipps.html">Working With Layers</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/layer_move.html">Moving Objects to a Different Layer</a></li>\
         </ul></li>\
    <li><input type="checkbox" id="0409"><label for="0409">Text in Presentations and Drawings</label><ul>\
    <li><a target="_top" href="en-GB/text/sdraw/guide/text_enter.html">Adding Text</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/text2curve.html">Converting Text Characters into Drawing Objects</a></li>\
         </ul></li>\
    <li><input type="checkbox" id="0410"><label for="0410">Viewing</label><ul>\
    <li><a target="_top" href="en-GB/text/simpress/guide/arrange_slides.html">Changing the Slide Order</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/change_scale.html">Zooming With the Keypad</a></li>\
         </ul></li>\
    <li><input type="checkbox" id="0411"><label for="0411">Slide Shows</label><ul>\
    <li><a target="_top" href="en-GB/text/simpress/guide/show.html">Showing a Slide Show</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/presenter_console.html">Using the Presenter Console</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/impress_remote.html">Impress Remote Guide</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/individual.html">Creating a Custom Slide Show</a></li>\
    <li><a target="_top" href="en-GB/text/simpress/guide/rehearse_timings.html">Rehearse Timings of Slide Changes</a></li>\
         </ul></li>\
     </ul></li></ul>\
    <ul><li><input type="checkbox" id="03"><label for="03">Formulae</label><ul>\
    <li><input type="checkbox" id="0301"><label for="0301">General Information and User Interface Usage</label><ul>\
    <li><a target="_top" href="en-GB/text/smath/main0000.html">Welcome to the LibreOffice Math Help</a></li>\
    <li><a target="_top" href="en-GB/text/smath/main0503.html">LibreOffice Math Features</a></li>\
    <li><a target="_top" href="en-GB/text/smath/guide/main.html">Instructions for Using LibreOffice Math</a></li>\
    <li><a target="_top" href="en-GB/text/smath/guide/keyboard.html">Shortcuts (LibreOffice Math Accessibility)</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0302"><label for="0302">Command and Menu Reference</label><ul>\
    <li><a target="_top" href="en-GB/text/smath/main0100.html">Menus</a></li>\
    <li><a target="_top" href="en-GB/text/smath/main0200.html">Toolbars</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0303"><label for="0303">Working with Formulae</label><ul>\
    <li><a target="_top" href="en-GB/text/smath/guide/align.html">Manually Aligning Formula Parts</a></li>\
    <li><a target="_top" href="en-GB/text/smath/guide/attributes.html">Changing Default Attributes</a></li>\
    <li><a target="_top" href="en-GB/text/smath/guide/brackets.html">Merging Formula Parts in Brackets</a></li>\
    <li><a target="_top" href="en-GB/text/smath/guide/comment.html">Entering Comments</a></li>\
    <li><a target="_top" href="en-GB/text/smath/guide/newline.html">Entering Line Breaks</a></li>\
    <li><a target="_top" href="en-GB/text/smath/guide/parentheses.html">Inserting Brackets</a></li>\
		</ul></li>\
	</ul></li></ul>\
    <ul><li><input type="checkbox" id="02"><label for="02">Text Documents</label><ul>\
    <li><input type="checkbox" id="0201"><label for="0201">General Information and User Interface Usage</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/main0000.html">Welcome to the LibreOffice Writer Help</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0503.html">LibreOffice Writer Features</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/main.html">Instructions for Using LibreOffice Writer</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/resize_navigator.html">Docking and Resizing Windows</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/04/01020000.html">Shortcut Keys for LibreOffice Writer</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/words_count.html">Counting Words</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/keyboard.html">Using Shortcut Keys (LibreOffice Writer Accessibility)</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0202"><label for="0202">Command and Menu Reference</label><ul>\
    <li><input type="checkbox" id="020201"><label for="020201">Menus</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/main0100.html">Menus</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0101.html">File</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0102.html">Edit</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0103.html">View</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0104.html">Insert</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0105.html">Format</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0115.html">Styles</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0110.html">Table</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0120.html">Form Menu</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0106.html">Tools</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0107.html">Window</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0108.html">Help</a></li>\
			</ul></li>\
    <li><input type="checkbox" id="020202"><label for="020202">Toolbars</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/main0200.html">Toolbars</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0202.html">Formatting Bar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0203.html">Image Bar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0204.html">Table Bar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0205.html">Drawing Object Properties Bar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0206.html">Bullets and Numbering Bar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0208.html">Status Bar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0210.html">Print Preview</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0213.html">Rulers</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0214.html">Formula Bar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0215.html">Frame Bar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0216.html">OLE Object Bar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/main0220.html">Text Object Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0201.html">Standard Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0212.html">Table Data Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0213.html">Form Navigation Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0214.html">Query Design Bar</a></li>\
    <li><a target="_top" href="en-GB/text/shared/main0226.html">Form Design Toolbar</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/librelogo/LibreLogo.html">LibreLogo Toolbar</a></li>\
			</ul></li>\
		</ul></li>\
    <li><input type="checkbox" id="0203"><label for="0203">Creating Text Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/text_nav_keyb.html">Navigating and Selecting With the Keyboard</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/text_direct_cursor.html">Using the Direct Cursor</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0204"><label for="0204">Graphics in Text Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/insert_graphic.html">Inserting Graphics</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/insert_graphic_dialog.html">Inserting a Graphic From a File</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/insert_graphic_gallery.html">Inserting Graphics From the Gallery With Drag-and-Drop</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/insert_graphic_scan.html">Inserting a Scanned Image</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/insert_graphic_fromchart.html">Inserting a Calc Chart into a Text Document</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/insert_graphic_fromdraw.html">Inserting Graphics From LibreOffice Draw or Impress</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0205"><label for="0205">Tables in Text Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/number_date_conv.html">Turning Number Recognition On or Off in Tables</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/tablemode.html">Modifying Rows and Columns by Keyboard</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/table_delete.html">Deleting Tables or the Contents of a Table</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/table_insert.html">Inserting Tables</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/table_repeat_multiple_headers.html">Repeating a Table Header on a New Page</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/table_sizing.html">Resizing Rows and Columns in a Text Table</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/table_cells.html">Adding or Deleting a Row or Column to a Table Using the Keyboard</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0206"><label for="0206">Objects in Text Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/anchor_object.html">Positioning Objects</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/wrap.html">Wrapping Text Around Objects</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0207"><label for="0207">Sections and Frames in Text Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/sections.html">Using Sections</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/text_frame.html">Inserting, Editing and Linking Text Frames</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/section_edit.html">Editing Sections</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/section_insert.html">Inserting Sections</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0208"><label for="0208">Tables of Contents and Indexes</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/chapter_numbering.html">Chapter Numbering</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/indices_userdef.html">User-Defined Indexes</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/indices_toc.html">Creating a Table of Contents</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/indices_index.html">Creating Alphabetical Indexes</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/indices_multidoc.html">Indexes Covering Several Documents</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/indices_literature.html">Creating a Bibliography</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/indices_delete.html">Editing or Deleting Index and Table Entries</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/indices_edit.html">Updating, Editing and Deleting Indexes and Tables of Contents</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/indices_enter.html">Defining Index or Table of Contents Entries</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/indices_form.html">Formatting an Index or a Table of Contents</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0209"><label for="0209">Fields in Text Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/fields.html">About Fields</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/fields_date.html">Inserting a Fixed or Variable Date Field</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/field_convert.html">Converting a Field into Text</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0210"><label for="0210">Navigating Text Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/dragdroptext.html">Moving and Copying Text in Documents</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/arrange_chapters.html">Rearranging a Document by Using the Navigator</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/hyperlinks.html">Inserting Hyperlinks With the Navigator</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/navigator.html">Navigator for Text Documents</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0211"><label for="0211">Calculating in Text Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/calculate_multitable.html">Calculating Across Tables</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/calculate.html">Calculating in Text Documents</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/calculate_clipboard.html">Calculating and Pasting the Result of a Formula in a Text Document</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/calculate_intable.html">Calculating Cell Totals in Tables</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/calculate_intext.html">Calculating Complex Formulae in Text Documents</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/calculate_intext2.html">Displaying the Result of a Table Calculation in a Different Table</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0212"><label for="0212">Formatting Text Documents</label><ul>\
    <li><input type="checkbox" id="021201"><label for="021201">Templates and Styles</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/templates_styles.html">Templates and Styles</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/even_odd_sdw.html">Alternating Page Styles on Odd and Even Pages</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/change_header.html">Creating a Page Style Based on the Current Page</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/load_styles.html">Using Styles From Another Document or Template</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/stylist_fromselect.html">Creating New Styles From Selections</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/stylist_update.html">Updating Styles From Selections</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/template_create.html">Creating a Document Template</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/template_default.html">Changing the Default Template</a></li>\
			</ul></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/pageorientation.html">Changing Page Orientation (Landscape or Portrait)</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/text_capital.html">Changing the Case of Text</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/hidden_text.html">Hiding Text</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/header_pagestyles.html">Defining Different Headers and Footers</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/header_with_chapter.html">Inserting a Chapter Name and Number in a Header or a Footer</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/shortcut_writing.html">Applying Text Formatting While You Type</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/reset_format.html">Resetting Font Attributes</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/stylist_fillformat.html">Applying Styles in Fill Format Mode</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/wrap.html">Wrapping Text Around Objects</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/text_centervert.html">Using a Frame to Centre Text on a Page</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/text_emphasize.html">Emphasising Text</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/text_rotate.html">Rotating Text</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/page_break.html">Inserting and Deleting Page Breaks</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/pagestyles.html">Creating and Applying Page Styles</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/subscript.html">Making Text Superscript or Subscript</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0213"><label for="0213">Special Text Elements</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/captions.html">Using Captions</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/conditional_text.html">Conditional Text</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/conditional_text2.html">Conditional Text for Page Counts</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/fields_date.html">Inserting a Fixed or Variable Date Field</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/fields_enter.html">Adding Input Fields</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/footer_nextpage.html">Inserting Page Numbers of Continuation Pages</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/footer_pagenumber.html">Inserting Page Numbers in Footers</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/hidden_text.html">Hiding Text</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/header_pagestyles.html">Defining Different Headers and Footers</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/header_with_chapter.html">Inserting a Chapter Name and Number in a Header or a Footer</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/fields_userdata.html">Querying User Data in Fields or Conditions</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/footnote_usage.html">Inserting and Editing Footnotes or Endnotes</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/footnote_with_line.html">Spacing Between Footnotes</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/header_footer.html">About Headers and Footers</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/header_with_line.html">Formatting Headers or Footers</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/text_animation.html">Animating Text</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/form_letters_main.html">Creating a Form Letter</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0214"><label for="0214">Automatic Functions</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/autocorr_except.html">Adding Exceptions to the AutoCorrect List</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/autotext.html">Using AutoText</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/auto_numbering.html">Creating Numbered or Bulleted Lists as You Type</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/auto_off.html">Turning Off AutoCorrect</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/auto_spellcheck.html">Automatically Check Spelling</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/number_date_conv.html">Turning Number Recognition On or Off in Tables</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/using_hyphen.html">Hyphenation</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0215"><label for="0215">Numbering and Lists</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/captions_numbers.html">Adding Chapter Numbers to Captions</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/auto_numbering.html">Creating Numbered or Bulleted Lists as You Type</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/chapter_numbering.html">Chapter Numbering</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/insert_tab_innumbering.html">Changing the Outline Level of Numbered and Bulleted Lists</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/join_numbered_lists.html">Combining Numbered Lists</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/numbering_lines.html">Adding Line Numbers</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/numbering_paras.html">Modifying Numbering in a Numbered List</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/number_sequence.html">Defining Number Ranges</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/using_numbered_lists2.html">Adding Numbering</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/using_numbering.html">Numbering and Numbering Styles</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/using_numbered_lists.html">Adding Bullets</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0216"><label for="0216">Spell Checking, Thesaurus and Languages</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/auto_spellcheck.html">Automatically Check Spelling</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/delete_from_dict.html">Removing Words From a User-Defined Dictionary</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/using_thesaurus.html">Thesaurus</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/spellcheck_dialog.html">Checking Spelling Manually</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0218"><label for="0218">Troubleshooting Tips</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/insert_beforetable.html">Inserting Text Before a Table at the Top of Page</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/jump2statusbar.html">Going to Specific Bookmark</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0219"><label for="0219">Loading, Saving, Importing and Exporting</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/send2html.html">Saving Text Documents in HTML Format</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/textdoc_inframe.html">Inserting an Entire Text Document</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0220"><label for="0220">Master Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/globaldoc.html">Master Documents and Sub-documents</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0221"><label for="0221">Links and References</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/references.html">Inserting Cross-References</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/hyperlinks.html">Inserting Hyperlinks With the Navigator</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0222"><label for="0222">Printing</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/printer_tray.html">Selecting printer paper trays</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/print_preview.html">Previewing a Page Before Printing</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/print_small.html">Printing Multiple Pages on One Sheet</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/pagestyles.html">Creating and Applying Page Styles</a></li>\
		</ul></li>\
    <li><input type="checkbox" id="0223"><label for="0223">Searching and Replacing</label><ul>\
    <li><a target="_top" href="en-GB/text/swriter/guide/search_regexp.html">Using Wildcards in Text Searches</a></li>\
    <li><a target="_top" href="en-GB/text/shared/01/02100001.html">List of Regular Expressions</a></li>\
		</ul></li>\
	</ul></li></ul>\
    <ul><li><input type="checkbox" id="06"><label for="06">HTML Documents</label><ul>\
    <li><a target="_top" href="en-GB/text/shared/07/09000000.html">Web Pages</a></li>\
    <li><a target="_top" href="en-GB/text/shared/02/01170700.html">HTML Filters and Forms</a></li>\
    <li><a target="_top" href="en-GB/text/swriter/guide/send2html.html">Saving Text Documents in HTML Format</a></li>\
	</ul></li></ul>\
';

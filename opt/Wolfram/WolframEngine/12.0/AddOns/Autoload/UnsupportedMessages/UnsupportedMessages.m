(* ::Package:: *)

(* suppress not-supported message within this file *)
Off[General::PiUnsupported]

BeginPackage["UnsupportedMessages`"]

System`GetAllUnsupported[] := getAllUnsupported[];


Begin["`Private`"]

General::PiUnsupported = "`1` is not currently supported on the Raspberry Pi.";
makeNotSupportedHead::null = "Attempt to redefine Null, please check your code.";

$NotSupportedHeads = {};
getAllUnsupported[] := Association[{"Heads" -> $NotSupportedHeads}];


SetAttributes[makeNotSupportedHead, HoldAll];
makeNotSupportedHead[Null] := Message[makeNotSupportedHead::null];
makeNotSupportedHead[head_] :=
    Module[{newRules, downValues},
    (* force loading of symbol*)
      head;
      PrependTo[DownValues[head], HoldPattern[head[___]] :> (Message[head::PiUnsupported, HoldForm@head]; $Failed)];
      Protect[head];
      AppendTo[$NotSupportedHeads, HoldForm@head];
    ];


(*List of Unsupported functions on Pi*)
unsupported = {"AudioCapture", "FindTextualAnswer", "ImageRestyle", "SequencePredict", "SpellingCorrectionList", "SystemInstall", "TextStructure"};
Map[Function[Unprotect[#]; ClearAll[#]], unsupported];
Map[makeNotSupportedHead, ToExpression[unsupported]];

End[]

EndPackage[]

On[General::PiUnsupported]

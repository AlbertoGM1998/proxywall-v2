(* Paclet Info File *)

(* created 2017/08/09*)

Paclet[
    Name -> "OAuth",
    Version -> "12.0.35",
    MathematicaVersion -> "12.0+",
    Loading -> Automatic,
    Description->"OAuth",
    Category->"Services",
    Extensions -> 
        {
            {"Kernel", Symbols -> 
                {}
            , Root -> "Kernel", Context -> 
                {"OAuthLoader`", "OAuth`"}
            }
        }
]



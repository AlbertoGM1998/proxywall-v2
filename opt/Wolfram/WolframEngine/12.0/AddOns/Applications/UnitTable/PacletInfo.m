(* Paclet Info File *)

(* created 2019/05/30*)

Paclet[
    Name -> "UnitTable",
    Version -> "12.0.1",
    MathematicaVersion -> "12.0+",
    Loading -> Automatic,
    Updating -> Automatic,
    Extensions -> 
        {
            {"Kernel", Symbols -> 
                {"Internal`$DimensionRules", "QuantityUnits`$UnitList", "QuantityUnits`$UnitTable"}
            , Root -> "Kernel", HiddenImport -> True, Context -> 
                {"UnitTable`"}
            }
        }
]



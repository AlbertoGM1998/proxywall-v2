(* Paclet Info File *)

(* created 2018/12/18*)

Paclet[
    Name -> "DemonstrationsTools",
    Version -> "2.01",
    MathematicaVersion -> "12+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"DemonstrationsToolsLoader`", "DemonstrationsTools`"}
            }, 
            {"FrontEnd", Prepend -> True}
        }
]



(* Paclet Info File *)

(* created 2015/05/01*)

Paclet[
    Name -> "DataDropClient",
    Version -> "1.7.0",
    MathematicaVersion -> "11.2+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"DataDropClient`", "DataDropClientLoader`"}
            }
        }
]



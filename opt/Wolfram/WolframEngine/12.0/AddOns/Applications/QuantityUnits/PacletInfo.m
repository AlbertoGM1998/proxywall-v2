(* Paclet Info File *)

(* created 2016/07/20*)

Paclet[
    Name -> "QuantityUnits",
    Version -> "2.0.1",
    MathematicaVersion -> "12.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"QuantityUnitsLoader`", "QuantityUnits`"}
            }
        }
]



(* Paclet Info File *)

(* created 2017/10/11*)

Paclet[
    Name -> "InflationAdjust",
    Version -> "1.3.0",
    MathematicaVersion -> "10.1+",
    Loading -> Automatic,
    Extensions -> 
        {
            {"Kernel", Symbols -> 
                {"System`InflationAdjust", "System`InflationMethod"}
            , Root -> "Kernel", Context -> 
                {"InflationAdjustLoader`", "InflationAdjust`"}
            }
        }
]



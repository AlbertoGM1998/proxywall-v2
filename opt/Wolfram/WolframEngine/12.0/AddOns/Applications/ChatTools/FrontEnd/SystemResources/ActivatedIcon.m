(* ::Package:: *)

GraphicsBox[TagBox[RasterBox[CompressedData["
      1:eJztmc1qFEEUhQcVdRHE3urKXmUrvXEb6I3byDxAgmkRQpREaHyDnkdoH2FE
      fIBZJCaR/DiJZrIQhYHgTtDOIiIugmXd8dyZM2Um092TLGJScEzVrZ9bUh/3
      VvXcmX4yGV2qVCoL1+0/k1PxxPz81PMHN22jOrfw+NHczMP7c89mHs3M35u+
      bI1jVi+srlTOfjHGVMY3J4xVVerDtLi4yBqzemV1YPXbyowgWWPNatbqKvvJ
      s6//XVzsWcU4syDPGdtxqVVI7abYyvBi5/lWjaLziLOoIGd3rX62Wi2zv79v
      Dg8PO5K62JbXV0zz647ZznZzaetby2x+2TYrW2+FufdWt4pwVqvVDKlplev/
      U6bYtVOrkNrir9S52Xm+VaMgZ8JJ2yrJ4wNjI2rHzF2RIvOEl6LzSnJ2Qxjb
      29szg4r0CWvb31u5WVOtfdwQ1j5YXSvIWYxzi6wyq+SUOGuLD2rHzF3BtULZ
      e959Ip508o/ww2uBnwz9KWxNtI3ywfNlDs2PdE3UdW4bfgNei3zEGCO2upVH
      e23Arn+LcPZaYtawImPWPm0W5ky0+jeuPS3IWUTtCDYPbc+qDluD7BHikQE/
      vsNPhr4UNh3bEWxSr2J+TPNl7TbV+/xYBU4cTrGnGGMM9uw5LDWIl5C4yBBv
      AuVHzhzjZJ5P82R8wjkQLCSoJ2BRWWmQfwO75/j1wZny10bbx1pFOTuQ/Dis
      yJjljZVSnEkOxX2tLGcebCHadchH3qvDnoARH/w1iIsM8SZQfmjdWJlU31ir
      e25YL0F9kJ8Y832szX597DklFpqaA3GGCZ3pkXlUz9dtU3zyiMfQmRvifqex
      MOK8yftBOyDujLJtyuXNzl1sWJExi0tLpTiT+5r186ssZ2wjNnzYhZvMUD7C
      uaYUo9rKiFt0XbdN8cljzp25rp+I82bNuVvqXnGGmjNZmuf6WMrDGfEZuXmY
      4lRDc94Aztz9qPrGnTZnS29G4uzgJOIZZFyZXt7MNMY4ufDIcxvEGfEZaR6l
      MYP8uJz9s08di5zVBG98XwrLxDPUE3DEsVF5Dly2juCs741B9pOIZ2chb3bv
      Z8g/3XhGY9Qe8JkTL4XiGeoJOKpTzjzOj8tZ243LGnfBWMx7gS0Z770B9H6W
      0JgMY5Qb5kzzXDbeu+sxvwF8KGch9fnkV+9yIa2TIefquKKcvcz7Dlj//G6U
      d8DsiO/NmPo1jgQU4zTPab1J569vAO3rnhvsCXHDnAXozzRnDvETUp9PfvUu
      19kr3Z/6vpnh/DR3JsSMnK+++6pk775XnZjkvl1TjGsqI9RXR1+d96B5XNem
      OKvv0E5fAc7yfddYG+m7xk6J7xoDv5/V+t+bEjOqsKc0J9bzp9ikzMg4D/Yq
      2TVWVWlem3MmbEf6Mb03SudtiTa/N7t7PU+i77SBsLa7u3ui32lXe99pb1/8
      HnB+dczvTqP85iT6cfG702D9AaoF2MY=
          "], {{0, 12}, {154, 0}}, {0, 255}, 
     ColorFunction -> RGBColor], 
    BoxForm`ImageTag["Byte", ColorSpace -> "RGB", 
     Interleaving -> True], Selectable -> False], 
   DefaultBaseStyle -> "ImageGraphics", ImageSizeRaw -> {154, 12}, 
   PlotRange -> {{0, 154}, {0, 12}}]

(* ::Package:: *)

GraphicsBox[TagBox[RasterBox[CompressedData["
      1:eJztmc9q1UAUxouKuihitroybyDZuBWycavkASo24sIqrRB8g9xHyCtk4wPc
      RUUt9c9tlV5BFALFnaDpQlGE0vFM+M71u2Nzc5N7BasGPk3OzJyZ6fw4c2bu
      het3rsbHFhYW1k7LP1eXksurq0v3r52Vj2hl7dbNleUbV1buLd9cXr10/bgY
      F0WZ6ITIGPNXaX19nXVStCLaFH0WmRl0AB+5aJH7mWW8R/2ZNLfCWzCiiOv3
      er1EZERBk2+p44v69J2JBl3HiPbhHBk7J9re2nhi3r98bj4Ot0z5ensqfdgZ
      mKePH5mdnR2zt7dn9vf3K9l3axO/X0UX2zCGv7dqIIp/F2PiOxOF9G37yzr6
      8kX9uvIp5hxzfcuIqBClTX1bHiyPznfSdg7EmO03nhNjp0Qv3zzfnJor1afh
      dsXX7u6uqXtsGTg705KxBGsWi0pR49+5yyN+C15b9BtOajPBV2jHXlfehjHE
      JRvDIrve7AfxrUS5jTcB3g3ZImUOvCTUPobNctiHr1KZAtsjf7B5ohw2W+63
      YOzu1sbj1nxZvX2xWcWqpgfx7EFLxmL6jmHzsJaeKIetT/YYcciAHd9hp0RZ
      BpvWNcqG7llon1B767uo60cUOPE3o34L2OyYPczJx9gN/c+MJbr3YV1D4qME
      H4GyQ/uqDx5iYix19tE+bDHa+Vqf2hstA2M55MM+aMHYpt0fuzD27Mmjak9s
      emwd5GddGfNgC7FuOeRjr8thT8GHrl+fmCgRZwJlh/wmyqP2DV+jdYG/tKGf
      BO19+OZ+fYw5w5wKmkN0CGM2VsR4z3W/rNs7mSn3m+KcR/yEVDcgxiIwZSiu
      aRvf+famZOx7m/yL9VDye5t7NT22Ds4CnRhjG3GhTFhmSv57Y00zik1F3V7r
      ri31o3HJcxmf0E+s7/geOL4DYq6agzs/rLnuk6wCZUbZm5YxYjPmvRdsFeA5
      dbji9/CQ8YyY+4MYO5hHHKP1GROtcamxhey/5NNNjBGbMeJMQXXq+nEZ+2Wc
      xLA5bM5Y+0RzHkjjUNgljuE7xR7pxkSOnXWM+dMyVcPYUdgrR/kY9pZRHKP1
      VHvgrneXOIb3FAzltE9O6sdlbOw8QfNpimODnnMmpFij+b7mY8pLSBz6hzAW
      9H7m9prblZSPuXGshC3Adx8KoKgFY7ftnUUXxt69eNom589nPFdy/q3xI6DY
      pnubvg9o7TXf17KUfJVgSZlhxnRvK4ufueCkfkIq86lfzd2qsWKO1p5Rvapf
      ynXG7sTAgu5xKfFiz5Ae7Hruy3t0riQfRY/OqKhTEk8Dyse4TM8B6r9iswVj
      9u7i1W++u/jW4e6i9n6sGD9X2lgRwZ5Rm8SJKSnxYuvpWTQiu8aoiNpZ/2N3
      Bw396Lj0HMLnymqsFMv4vDkq+5tEd7Dn7R3ZLHeww+Fw0h1sMI97/v86eprw
      W9KXGX9L0vxrrr8l/Qv6AXyCQuE=
          "], {{0, 12}, {153, 0}}, {0, 255}, 
     ColorFunction -> RGBColor], 
    BoxForm`ImageTag["Byte", ColorSpace -> "RGB", 
     Interleaving -> True], Selectable -> False], 
   DefaultBaseStyle -> "ImageGraphics", ImageSizeRaw -> {153, 12}, 
   PlotRange -> {{0, 153}, {0, 12}}]

(* ::Package:: *)

GraphicsBox[
  {Thickness[0.03333333333333333], 
   {FaceForm[{RGBColor[1., 1., 1.], Opacity[1.]}], 
    FilledCurveBox[{{{0, 2, 0}, {1, 3, 3}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}, {1, 3, 
     3}, {0, 1, 0}}}, {{{19.293, 8.0781}, {7.5, 8.0781}, {5.019, 8.0781}, {3., 10.098099999999999`}, {3., 
     12.5781}, {3., 22.5781}, {3., 25.059099999999997`}, {5.019, 27.0781}, {7.5, 27.0781}, {23.5, 
     27.0781}, {25.980999999999998`, 27.0781}, {28., 25.059099999999997`}, {28., 22.5781}, {28., 
     12.5781}, {28., 10.2661}, {26.247, 8.355099999999998}, {24., 8.105099999999998}, {24., 
     3.3710999999999993`}}}]}, 
   {FaceForm[{RGBColor[0.749, 0.749, 0.749], Opacity[1.]}], 
    FilledCurveBox[{{{0, 2, 0}, {1, 3, 3}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 
      0}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}, {1, 3, 3}}, {{1, 4, 3}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}, {0, 1, 
      0}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}}}, {CompressedData["
1:eJxTTMoPSmViYGAQBWIQDQYN5g6G5bqK8lOsHSACcnC+y7bPf6/8EEKTZ3FY
0Lnh4ctQSzgfIm8G5ye9i3Ky09CE81tOhhxcsUQZbt6RO1tbagLk4PbB+HtK
JkuwiBnD+XG7PHmYTps5eOj9V/z7jQ+ivsHCwYLRt4XXnxHOh6gXgvNdin7y
v9wu59A41bk757iVg0tm/odWTwWovI3DgRVL/ByCVeB8FPcC+Sj+AfJh/s0X
aj5wytEKNTyQwg8AsCRjdw==
      "], CompressedData["
1:eJxTTMoPSmViYGAQBmIQDQYN5g6G5bqK8lOsHPTvqrA1brWE8yHy1g4JIUHq
C05awPkQeTM4P+ldlJOdhiacz2IiaGazVwVuHkRe0QFmHzr/yJ2tLTUBQlC+
Mao8gxyc79Kd8/y3piiaPA/cPhgfxT1APop7gXyYf2DmofgXaB+q/xHhAwCA
wUkV
      "]}]}, 
   {FaceForm[{RGBColor[0.8670000000000001, 0.067, 0.], Opacity[1.]}], 
    FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 
      1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 
      0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0,
       2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 
      0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0,
       1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 
      0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0,
       1, 0}, {0, 1, 0}}, {{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0,
       1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 
      1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1,
       0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 
      0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}}}, {{{18.714800000000004`, 14.461400000000001`}, {
      18.907800000000005`, 12.527400000000002`}, {17.131800000000005`, 13.3084}, {17.120800000000003`, 
      15.0164}}, {{16.526800000000005`, 14.471400000000001`}, {16.535800000000005`, 13.191400000000002`}, {
      15.798800000000004`, 11.931400000000002`}, {15.798800000000004`, 13.393400000000002`}}, {{
      11.940800000000003`, 15.816400000000002`}, {13.219800000000003`, 15.419400000000001`}, {
      11.987800000000004`, 14.989400000000002`}, {10.511800000000004`, 15.312400000000002`}}, {{
      11.353800000000003`, 16.9614}, {12.550800000000006`, 17.3764}, {11.754800000000003`, 16.3844}, {
      10.418800000000005`, 15.9114}}, {{11.890800000000006`, 19.4254}, {12.932800000000004`, 18.1404}, {
      11.272800000000004`, 17.5654}, {9.985800000000005, 19.0094}}, {{19.730800000000002`, 17.5704}, {
      18.198800000000006`, 18.1414}, {19.120800000000003`, 19.4234}, {21.014800000000005`, 
      19.011400000000002`}}, {{19.342800000000004`, 16.3814}, {18.596800000000005`, 17.3564}, {
      19.643800000000002`, 16.965400000000002`}, {20.562800000000003`, 15.9344}}, {{19.148800000000005`, 
      15.8154}, {20.509800000000006`, 15.317400000000001`}, {19.011800000000004`, 14.990400000000001`}, {
      17.793800000000005`, 15.4144}}, {{17.623800000000003`, 17.6434}, {18.656800000000004`, 
      16.293400000000002`}, {16.933800000000005`, 15.7824}, {15.981800000000003`, 17.0604}}, {{
      18.027800000000003`, 18.927400000000002`}, {18.029800000000005`, 20.2864}, {18.853800000000003`, 
      21.367400000000004`}, {18.702800000000003`, 19.866400000000002`}}, {{15.798800000000004`, 
      19.282400000000003`}, {17.431800000000003`, 19.9434}, {17.429800000000004`, 18.2084}, {
      15.798800000000004`, 17.628400000000003`}}, {{15.200800000000005`, 17.6404}, {13.723800000000004`, 
      18.206400000000002`}, {13.650800000000004`, 19.930400000000002`}, {15.200800000000005`, 19.2844}}, {{
      12.141800000000003`, 21.4054}, {13.037800000000004`, 20.276400000000002`}, {13.096800000000005`, 
      18.886400000000002`}, {12.294800000000004`, 19.8744}}, {{12.442800000000004`, 16.2864}, {
      13.529800000000005`, 17.6414}, {15.023800000000005`, 17.0674}, {14.061800000000005`, 15.7834}}, {{
      14.541800000000004`, 15.4274}, {15.499800000000004`, 16.706400000000002`}, {16.452800000000003`, 
      15.428400000000002`}, {15.499800000000004`, 14.0174}}, {{15.201800000000004`, 11.932400000000001`}, {
      14.464800000000004`, 13.192400000000001`}, {14.469800000000003`, 14.470400000000001`}, {
      15.200800000000005`, 13.393400000000002`}}, {{18.318800000000003`, 21.6504}, {17.543800000000005`, 
      20.6334}, {16.351800000000004`, 20.1504}, {17.027800000000003`, 21.084400000000002`}}, {{
      16.477800000000002`, 21.3424}, {15.499800000000004`, 19.9934}, {14.521800000000004`, 21.3424}, {
      15.499800000000004`, 23.017400000000002`}}, {{13.970800000000004`, 21.084400000000002`}, {
      14.635800000000003`, 20.1674}, {13.517800000000005`, 20.6324}, {12.725800000000003`, 21.6304}}, {{
      13.867800000000004`, 13.307400000000001`}, {12.091800000000005`, 12.527400000000002`}, {
      12.285800000000004`, 14.461400000000001`}, {13.874800000000004`, 
      15.015400000000001`}}, CompressedData["
1:eJxTTMoPSmViYGDQAGIQXdzV9+TTf1MHL73/in/N9Bwm8VeZrd5n6rBN9Pfp
dyf1HFgquFU05Ewc+PYZzNSyMHQ4fdhpbSZQnsN2duh8bmMHmP4fTxIXXguB
8uebOly+94B7cqaxw7TUjuRYG2MHebEs38/vjB3Ug1kXT6oydlB0/Jh8JtbM
YckkK0bfFmOH/uASlen7zRxqE41CDbSMIfbNMHM4++7kYae3Bg6VVUt1nOeY
OuSH1a7blqTvkLTwmsn7HeYOkSDufz0HO6BzVvNaOFyw9rs4cY4eXN7IZxmX
m6qOwya9vMWMQP0u59OuPl+l7uBR9JP/5XQzhxaWo/2G39UcTrADPQS0v65Z
H6hD3SHq685bXUD3LYjTPC3Qru4QccroyEag+5+JyJ58ul/JgWfyyqbATCj/
v6ID0Hd9wUD/rwDaVtqk5FANdK4MMHwOWyp7VR9WhdhnbuhgumiL+Q+g/Jxn
sstfnNBzKFnTfTuDQclhz7QJwJDXg5v/unir6G9uPQe9b9PvTgban2fSsN2B
SdeBW3penCa3usNdN2ZQzDgIzDFetAXo/t6d2Zw/E9QdLgG9l7Va3YHJt4XX
f7063P+6V2YBbdR0iL2857HIXD2HqDaLa0dzVR2KwfbrQ+LhiIrDn7evD1gm
68PlYeEP08/9c0H6ZmD89D/5JH8JaH72vcKuPmD8SQXekq5JVHd4N89G5wow
frNA4kD3weIf5n5Y+mBbDLSQV88BANzJEfQ=
      "]}]}},
  AspectRatio->Automatic,
  ImageSize->{30., 29.},
  PlotRange->{{0., 30.}, {0., 29.}}]

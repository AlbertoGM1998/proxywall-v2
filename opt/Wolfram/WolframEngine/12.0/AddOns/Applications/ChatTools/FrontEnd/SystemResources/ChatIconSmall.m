(* ::Package:: *)

GraphicsBox[
  TagBox[RasterBox[CompressedData["
1:eJxTTMoPSmNiYGAo5gASQYnljkVFiZXBAkBOaF5xZnpeaopnXklqemqRRRIz
UFAUiEWAmIUBFTx58oQRiAVIxIwMmOaoPX/+/BQQv3v58uVrUjBID0gvyAyo
WdJA/uufP3/+JxeA9ILMgJo1/+vXr3jV//30EYzxAZAZILOA+Nbfv38x5P/9
/v3/x4WzYPb7npb/73tbwWyw2J8/mHYCzQCade3Zs2dPcdl5X5b3/8uM2P/v
+9rA+FV2wv970tzY/QAx7wZW8/79+//r9o3/r0uy/j/1sf//2ELr/2NL7f9P
Paz/v8pLAcuB1BBr3uvS7P8fpvWD/fgyJfL/l42rwfhNbcn/d221YLnXZTlE
m/cPGF8gvz2P9IXgKL//L6L9wWY/83cCy/1DSw/4zPtx5sT/J04m/584m/3/
vHb5/1e5yWD8ZdMaoLjp/yf2hmA1xJr3adHs/6+LMv6/Lkz//zzCB2JeThKE
nZ8KDo9Pi+cQHx9Q8H5i5/9PS+f/fxEfAsYgNkgMG4CZhyv9gdLYi7jg/1+3
bQSHGQiD2CAxPOnvOjH54+ue7WCMVw0kfyygYv59AzRLCr18AeK3BPB7oFvA
ifkfME0D+V+g5YsqerlFDADq4wfG4S2QmbA4IMccbGZ++fLlHzXMQzLzDihO
qWEezMynT5/2UMs8qJko9QcA5/ePHg==
    "], {{0, 21}, {20, 0}}, {0, 255},
    ColorFunction->RGBColor],
   BoxForm`ImageTag["Byte", ColorSpace -> "RGB", Interleaving -> True],
   Selectable->False],
  DefaultBaseStyle->"ImageGraphics",
  ImageSizeRaw->{20, 21},
  PlotRange->{{0, 20}, {0, 21}}]

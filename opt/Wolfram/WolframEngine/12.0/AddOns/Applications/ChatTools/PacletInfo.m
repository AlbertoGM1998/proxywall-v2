(* Paclet Info File *)

(* created 2018/09/13*)

Paclet[
    Name -> "ChatTools",
    Version -> "0.1.1905021227",
    MathematicaVersion -> "11.3+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"ChatToolsLoader`", "ChatTools`"}
            }, 
            {"Documentation", Language -> All}, 
            {"FrontEnd", Prepend -> True}
        }
]



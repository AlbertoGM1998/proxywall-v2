(* ::Package:: *)

(* Paclet Info File *)

(* created 2014/07/09*)

Paclet[
    Name -> "FormulaData",
    Version -> "1.1.1",
    MathematicaVersion -> "12+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"FormulaDataLoader`", "FormulaData`"}
            }
        }
]



(* Paclet Info File *)

(* created 2016/07/06*)

Paclet[
    Name -> "CompiledFunctionTools",
    Version -> "12.0.0",
    MathematicaVersion -> "12+",
    Loading -> "Manual",
    Extensions -> 
        {
            {"Kernel", Context -> "CompiledFunctionTools`"}, 
            {"Documentation", LinkBase -> "Compile", MainPage -> "Tutorials/Overview", Language -> All}
        }
]



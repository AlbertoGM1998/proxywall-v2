(* Paclet Info File *)

(* created 2019/03/04*)

Paclet[
    Name -> "UnityLink",
    Version -> "0.0.1",
    MathematicaVersion -> "12+",
    Description -> "The Wolfram Language's link to Unity game engine.",
    Creator -> "Wolfram Research Inc.",
    Loading -> Manual,
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> "UnityLink`"}, 
            {"FrontEnd"}, 
            {"Documentation", Resources -> 
                {"Guides/UnityLink"}
            , MainPage -> "Guides/UnityLink", Language -> All}
        }
]



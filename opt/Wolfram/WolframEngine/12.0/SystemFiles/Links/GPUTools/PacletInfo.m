(* ::Package:: *)

(* Paclet Info File *)

(* created 2009/09/07*)

Paclet[
    Name -> "GPUTools",
    Version -> "11.1.3",
    MathematicaVersion -> "11.1+",
    Extensions ->  {
            {"Kernel", Context -> "GPUTools`"}, 
            {"Documentation", MainPage -> "Tutorials/Overview", Language -> All}
        }
       
]



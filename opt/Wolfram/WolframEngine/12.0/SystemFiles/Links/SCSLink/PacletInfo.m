(* Paclet Info File *)

(* created 2017/10/28 *)

Paclet[
    Name -> "SCSLink",
    Version -> "1.0.0",
    MathematicaVersion -> "11.3+",
    Extensions -> 
        {
        	{"Kernel", Context -> "SCSLink`"}
            ,
            {"LibraryLink", Root -> "LibraryResources"}
        }
]



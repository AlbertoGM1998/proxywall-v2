JDBCDriver[
  "Name"->"PostgreSQL", 
  "Driver"->"org.postgresql.Driver", 
  "Protocol"->"jdbc:postgresql://",
  "Version"->DatabaseLink`Information`$VersionNumber,
  "Description"->"PostgreSQL using PostgreSQL JDBC Driver - Version 42.1.4."
]
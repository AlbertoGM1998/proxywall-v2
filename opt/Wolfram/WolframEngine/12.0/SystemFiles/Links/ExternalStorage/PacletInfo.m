Paclet[
	Name -> "ExternalStorage",
	Version -> "12",
	MathematicaVersion -> "11.1+",
	Description -> "Interface to external storage services.",
	Loading -> Automatic,
	Creator -> 
		"Sebastian Bodenstein <sebastianb@wolfram.com>",
	Extensions -> {
		{
			"Kernel",
			Symbols -> 
				{
					"ExternalStorage`IPFSObject",
					"ExternalStorage`IPFSDownload",
					"ExternalStorage`IPFSUpload",
					"ExternalStorage`S3BucketCreate",
					"ExternalStorage`S3BucketDelete",
					"ExternalStorage`S3Buckets",
					"ExternalStorage`S3Client",
					"ExternalStorage`S3Connect",
					"ExternalStorage`S3Download",
					"ExternalStorage`S3DownloadSubmit",
					"ExternalStorage`S3ObjectCopy",
					"ExternalStorage`S3ObjectDelete",
					"ExternalStorage`S3Keys",
					"ExternalStorage`S3TaskAbort",
					"ExternalStorage`S3TaskObject",
					"ExternalStorage`S3TaskRemove",
					"ExternalStorage`S3Tasks",
					"ExternalStorage`S3TaskWait",
					"ExternalStorage`S3Upload",
					"ExternalStorage`S3UploadSubmit"
				},
			Context -> "ExternalStorage`",
			HiddenImport -> None
		},
		{"LibraryLink"}
	}
]

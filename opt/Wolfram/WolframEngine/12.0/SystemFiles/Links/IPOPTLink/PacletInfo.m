(* Paclet Info File *)

(* created 2015/12/15 *)

Paclet[
    Name -> "IPOPTLink",
    Version -> "1.0.1",
    MathematicaVersion -> "10.4+",
    Extensions -> 
        {
        	{"Kernel", Context -> "IPOPTLink`"}
        	,
            {"Documentation", Language -> All}
            ,
            {"LibraryLink", Root -> "LibraryResources"}
        }
]



(* ::Package:: *)

Paclet[
	Name -> "DAALLink",
	Version -> "1.0",
	MathematicaVersion -> "11.0+",
	Description -> "Link to the Intel DAAL Library",
	Loading -> Automatic,
	Creator -> "Sebastian Bodenstein <sebastianb@wolfram.com>",
	Extensions -> {
		{"Kernel", Context -> {"DAALLink`"}, Symbols -> {
		}},
		{"LibraryLink"}
	}
]

(* Paclet Info File *)

Paclet[
	Name -> "ImageFileTools",
	Version -> "1.0",
	WolframVersion -> "12.0+",
	Updating -> Automatic,
	Extensions -> {
		{"Kernel", Root -> "Kernel", Context -> {"ImageFileTools`", "ImageFileTools`RLE`"}}
	}
]

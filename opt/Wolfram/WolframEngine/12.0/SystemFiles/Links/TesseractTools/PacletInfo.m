(* Paclet Info File *)

(* created 2015/11/16*)

Paclet[
    Name -> "TesseractTools",
    Version -> "11.0.0",
    MathematicaVersion -> "11+",
	Loading->"Automatic",
	Extensions -> {
	{"Kernel",
		Root -> "Kernel", 
		Context -> {"TesseractToolsImpl`", "TesseractTools`"}
		}
	}
]



Paclet[
    Name -> "XMPTools",
    Version -> "12.0.0",
	WolframVersion -> "11.0+",
	Updating -> Automatic,
    Extensions -> {
		{"Kernel", Root -> "Kernel", Context -> "XMPTools`"}
	}
]



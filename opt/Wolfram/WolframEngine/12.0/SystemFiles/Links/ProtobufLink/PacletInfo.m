Paclet[
	Name -> "ProtobufLink",
	Version -> "12",
	MathematicaVersion -> "11.3+",
	Description -> "Link to Google Protobuf",
	Loading -> Automatic,
	Creator -> 
		"Rafal Chojna <rafalc@wolfram.com>, Sebastian Bodenstein <sebastianb@wolfram.com>",
	Extensions -> {
		{"Kernel", Context -> "ProtobufLink`"}, 
		{"Documentation", Resources -> {},
		Language -> All
		},
		{"LibraryLink"}
	}
]

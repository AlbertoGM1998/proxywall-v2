(* Paclet Info File *)

(* created 2016/06/15*)

Paclet[
	Name -> "HDF5Tools",
	Version -> "12.0.0",
	WolframVersion -> "11+",
	Updating -> Automatic,
	Extensions ->
		{
			{"Kernel", Root -> "Kernel", Context -> "HDF5Tools`"},
			{"Application", Context -> "HDF5Tools`"}
		}
]



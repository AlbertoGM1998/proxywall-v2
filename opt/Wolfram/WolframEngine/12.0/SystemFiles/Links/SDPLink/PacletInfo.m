(* Paclet Info File *)

(* created 2017/04/11 *)

Paclet[
    Name -> "SDPLink",
    Version -> "1.0.0",
    MathematicaVersion -> "11.2+",
    Extensions -> 
        {
        	{"Kernel", Context -> "SDPLink`"}
            ,
            {"LibraryLink", Root -> "LibraryResources"}
        }
]



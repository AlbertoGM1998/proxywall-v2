(* Paclet Info File *)

(* created 2018/07/12*)

Paclet[
    Name -> "DataResource",
    Version -> "1.10.0",
    MathematicaVersion -> "12.0+",
    Loading -> Automatic,
    Extensions -> 
        {
            {"Kernel", Symbols -> 
                {"System`ResourceData"}
            , Root -> "Kernel", Context -> 
                {"DataResourceLoader`", "DataResource`"}
            }, 
            {"FrontEnd", Prepend -> True}
        }
]



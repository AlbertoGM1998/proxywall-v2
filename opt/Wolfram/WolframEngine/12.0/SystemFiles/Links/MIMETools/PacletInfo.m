(* Paclet Info File *)

Paclet[
    Name -> "MIMETools",
    Version -> "1.0.0",
    WolframVersion -> "11.0+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> "Kernel", Context -> "MIMETools`"}
    }
]

(* ::Package:: *)

Paclet[
    Name -> "MIDITools",
    Version -> "11.0.0",
    WolframVersion -> "10+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> "Kernel", Context -> "MIDITools`"}
    }
]



Paclet[
    Name -> "SVTools",
    Version -> "1.1",
    WolframVersion -> "11.3+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> "Kernel", Context -> "SVTools`"}
    }
]
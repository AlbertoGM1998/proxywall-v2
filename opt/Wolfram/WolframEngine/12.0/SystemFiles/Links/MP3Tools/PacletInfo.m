Paclet[
    Name -> "MP3Tools",
    Version -> "11.0.0",
    WolframVersion -> "10.0+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> "Kernel", Context -> "MP3Tools`"}
    }
]

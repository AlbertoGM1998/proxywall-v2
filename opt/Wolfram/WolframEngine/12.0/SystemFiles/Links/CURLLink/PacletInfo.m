(* Paclet Info File *)

(* created 2015/06/30*)

Paclet[
    Name -> "CURLLink",
    Version -> "11.5.0",
    MathematicaVersion -> "12.0+",
    Extensions -> 
        {
            {"Documentation", Resources -> 
                {"Guides/CURLLink"}
            , Language -> All}, 
            {"Application", Context -> "CURLLink`"}
        }
]



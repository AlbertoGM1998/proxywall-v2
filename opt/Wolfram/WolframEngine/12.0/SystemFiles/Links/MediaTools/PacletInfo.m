(* Paclet Info File *)

(* Created 2017/04/03 *)

Paclet[
    Name -> "MediaTools",
    Version -> "12.0.0",
    WolframVersion -> "11+",
    Updating -> Automatic,
	Extensions -> {
		{"Kernel", Root -> "Kernel", Context -> "MediaTools`"}
    }
]

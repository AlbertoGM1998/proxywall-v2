(* ::Package:: *)

Paclet[
    Name -> "RAWTools",
    Version -> "12.0.0",
    WolframVersion -> "11+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> "Kernel", Context -> "RAWTools`"}
    }
]



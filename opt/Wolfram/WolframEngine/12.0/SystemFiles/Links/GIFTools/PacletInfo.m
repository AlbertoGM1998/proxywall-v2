(* Paclet Info File *)

(* created 2016/06/28*)

Paclet[
    Name -> "GIFTools",
	Version -> "12.0.0",
	WolframVersion -> "11.1.0+",
	Updating -> Automatic,
	Extensions ->
		{
			{"Kernel", Root -> "Kernel", Context -> "GIFTools`"}
		}
]



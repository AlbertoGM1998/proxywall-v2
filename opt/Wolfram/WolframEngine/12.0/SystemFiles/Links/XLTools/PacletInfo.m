Paclet[
    Name -> "XLTools",
    Version -> "1.1",
    WolframVersion -> "12.0+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> "Kernel", Context -> "XLTools`"}
    }
]
(* Paclet Info File *)

(* created 2016/10/31*)

Paclet[
    Name -> "SpeechSynthesisTools",
    Version -> "12.0.0",
    WolframVersion -> "11+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> "Kernel", Context -> "SpeechSynthesisTools`"}
    }
]



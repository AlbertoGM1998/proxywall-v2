Paclet[
	Name -> "LightGBMLink",
	Version -> "11.2",
	MathematicaVersion -> "11.1+",
	Description -> "Link to the LightGBM library.",
	Loading -> Automatic,
	Creator -> {"Sebastian Bodenstein <sebastianb@wolfram.com>",
                    "Zbigniew Leyk <zleyk@wolfram.com>"},
	Extensions -> {
		{"Kernel", Context -> {"LightGBMLink`"},
                 Symbols -> {}},
		{"LibraryLink"}
	}
]

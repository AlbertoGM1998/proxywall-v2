(* Paclet Info File *)

Paclet[
	Name -> "HTTPLink",
	Version -> "0.9.0",
	MathematicaVersion -> "11.2+",
	Creator ->"Ian Johnson <ijohnson@wolfram.com>",
	Loading->Automatic,
	Extensions -> {
		{
			"Kernel",
			Root -> "Kernel",
			Context -> 
			{
				"HTTPLinkLoader`","HTTPLink`"
			},
			Symbols-> {}
		},
		{"LibraryLink"}
	}
]
(* ::Package:: *)

ImportExport`RegisterExport[
	"FITS",
	System`Convert`FITSDump`ExportFITSNew,
	"DefaultElement" -> Automatic,
	"BinaryFormat"   -> True
] 

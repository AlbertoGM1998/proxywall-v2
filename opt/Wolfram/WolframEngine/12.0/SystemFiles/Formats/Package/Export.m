(* ::Package:: *)

ImportExport`RegisterExport[
 "Package",
 System`Convert`PackageDump`ExportPackage,
 "FunctionChannels" -> {"Streams"},
 "DefaultElement" -> Automatic,
 "Options" -> {"Comments", PageWidth}
]

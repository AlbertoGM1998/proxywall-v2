(* ::Package:: *)

ImportExport`RegisterExport[
  "MO",
  WSM`PackageScope`saveModelToMo,
  "DefaultElement" -> Automatic,
  "Sources" -> {"WSM`"}
]

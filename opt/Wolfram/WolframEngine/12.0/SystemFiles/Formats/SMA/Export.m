(* ::Package:: *)

ImportExport`RegisterExport[
  "SMA",
  WSM`PackageScope`saveModelToSma,
  "DefaultElement" -> Automatic,
  "Sources" -> {"WSM`"}
]

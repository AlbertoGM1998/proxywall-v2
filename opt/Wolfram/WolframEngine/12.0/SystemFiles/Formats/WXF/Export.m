Begin["System`ConvertersDump`WXF`"];

ImportExport`RegisterExport[
	"WXF",
	ExportWXF,
	"FunctionChannels" -> {"FileNames"},
	"DefaultElement" -> "Expression",
	"BinaryFormat" -> True
];

End[];
ImportExport`RegisterImport[
	"Character32",
	Import[#1,{"Binary","Character32"},##2]&,
	"AvailableElements" -> {_Integer},
	"Sources" -> ImportExport`DefaultSources["Binary"],
	"FunctionChannels" -> {"Streams"},
	"BinaryFormat" -> True
]
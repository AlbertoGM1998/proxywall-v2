ImportExport`RegisterExport[
	"Character32",
	Export[#1,#2,{"Binary","Character32"},##3]&,
	"Sources" -> ImportExport`DefaultSources["Binary"],
	"FunctionChannels" -> {"Streams"},
	"BinaryFormat" -> True
]
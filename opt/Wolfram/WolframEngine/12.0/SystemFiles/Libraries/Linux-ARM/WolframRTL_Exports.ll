; ModuleID = '/Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/scratch/rtl_makefiles/lib/WolframRTL_Exports.bc'
source_filename = "/Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/scratch/rtl_makefiles/lib/WolframRTL_Exports.bc"

%struct.st_ZSpan = type { %struct.st_ZRow*, %struct.st_ZSpan*, %struct.st_ZSpan*, %struct.st_ZSpan*, %struct.st_ZBlock**, %struct.st_ZBlock**, %struct.st_ZBlock*, i32 }
%struct.st_ZRow = type { %struct.st_ZAllocator*, %struct.st_KMutex*, %struct.st_ZSpan*, %struct.st_ZSpan*, %struct.st_ZSpan*, %struct.st_ZSpan*, i32, i32, i32, i32, i32, i32 }
%struct.st_ZAllocator = type { %struct.st_ZAllocator*, %struct.st_ZAllocator*, %struct.st_ZRow**, %struct.st_MallocBlock*, %struct.st_MallocBlock**, i8*, %struct.st_KMutex*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.st_MallocBlock = type { %struct.st_MallocBlock**, %struct.st_MallocBlock*, %struct.st_MallocBlock*, %struct.st_ZAllocator* }
%struct.st_KMutex = type { i8* }
%struct.st_ZBlock = type { %union.ZHeader, [4 x i8] }
%union.ZHeader = type { i32 }
%struct.st_MaxMemoryCell = type { i32, %struct.st_MaxMemoryCell* }
%"class.std::ios_base::Init" = type { i8 }
%struct.st_PageInformation = type { i32, i32, i32, i32, i32 }
%"struct.std::complex" = type { { double, double } }
%struct.st_ParallelThread = type { i32, i32, i32, %struct.st_KMutex*, %struct.st_KMutex*, %struct.st_ParallelThreadsEnvironment*, %struct.st_ParallelThreadState*, %struct.st_KMutex*, i32, %struct.st_ParallelThread* }
%struct.st_ParallelThreadsEnvironment = type { i32, i32, %struct.st_KMutex*, %struct.st_KMutex*, %struct.st_KMutex*, %struct.st_KMutex*, i32, %struct.st_ParallelThread*, %struct.st_ParallelThread**, %struct.st_ParallelThread*, i32, i32 }
%struct.st_ParallelThreadState = type { i32, i32, i32, void (i8*, %struct.st_ParallelThread*)*, i8* }
%union.pthread_mutex_t = type { %"struct.(anonymous union)::__pthread_mutex_s" }
%"struct.(anonymous union)::__pthread_mutex_s" = type { i32, i32, i32, i32, i32, %union.ZHeader }
%"struct.std::pair" = type { i32, i32 }
%"struct.std::pair.4" = type { double, double }
%struct.st_ParallelThreadsSchedule = type { i32, i32, i32 }
%struct.st_MDataArray = type { double, i32*, i32, i32, i32, i32, i32, i8*, i64 }
%struct.st_WolframLibraryData = type { void (i8*)*, i32 (i32, i32, i32*, %struct.st_MDataArray**)*, void (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray**)*, i32 (%struct.st_MDataArray*)*, void (%struct.st_MDataArray*)*, void (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*, i32*, i32)*, i32 (%struct.st_MDataArray*, i32*, double)*, i32 (%struct.st_MDataArray*, i32*, %"struct.std::complex")*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray*, i32*, i32)*, i32 (%struct.st_MDataArray*, i32*, i32*)*, i32 (%struct.st_MDataArray*, i32*, double*)*, i32 (%struct.st_MDataArray*, i32*, %"struct.std::complex"*)*, i32 (%struct.st_MDataArray*, i32*, i32, %struct.st_MDataArray**)*, i32 (%struct.st_MDataArray*)*, i32* (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32* (%struct.st_MDataArray*)*, double* (%struct.st_MDataArray*)*, %"struct.std::complex"* (%struct.st_MDataArray*)*, void (i8*)*, i32 ()*, %struct.MLINK_STRUCT* (%struct.st_WolframLibraryData*)*, i32 (%struct.MLINK_STRUCT*)*, i32 (%struct.st_WolframLibraryData*, i8*, i32, i32, i8*)*, %struct.st_WolframRuntimeData*, %struct.st_WolframCompileLibrary_Functions*, i32, i32 (i8*, void (%struct.st_MInputStream*, i8*, i8*)*, i32 (i8*, i8*)*, i8*, void (i8*)*)*, i32 (i8*)*, i32 (i8*, void (%struct.st_MOutputStream*, i8*, i8*, i32)*, i32 (i8*, i8*)*, i8*, void (i8*)*)*, i32 (i8*)*, %struct.st_WolframIOLibrary_Functions*, %struct.MLENV_STRUCT* (%struct.st_WolframLibraryData*)*, %struct.st_WolframSparseLibrary_Functions*, %struct.st_WolframImageLibrary_Functions*, i32 (i8*, void (%struct.st_WolframLibraryData*, i32, i32)*)*, i32 (i8*)*, i32 (i8*, i32)*, i32 (i8*, i32 (%struct.st_WolframLibraryData*, i32, %struct.st_MDataArray*)*)*, i32 (i8*)*, i32 (i32, i32, %union.MArgument*, [1 x i32])*, i32 (i32)*, i32 (i8*, i8)*, i32 ()*, %struct.st_WolframRawArrayLibrary_Functions*, %struct.st_WolframNumericArrayLibrary_Functions* }
%struct.MLINK_STRUCT = type opaque
%struct.st_WolframRuntimeData = type opaque
%struct.st_WolframCompileLibrary_Functions = type { i32, %struct.M_TENSOR_INITIALIZATION_DATA_STRUCT* (%struct.st_WolframLibraryData*, i32)*, void (%struct.M_TENSOR_INITIALIZATION_DATA_STRUCT*)*, void (%struct.st_WolframLibraryData*, i32)*, %struct.st_MDataArray* ()*, i32 (%struct.st_MDataArray**, i32, i32, i32*)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray*, i32*)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*, i32*, i32)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i8**)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i8**)*, i32 (i8*, i8*, i32, i32*, i32)* (i32, i32)*, i32 (i32, i32, %struct.st_MDataArray*, i32, %struct.st_MDataArray**)*, i32 (i32, i32, i32, i8*, i32, i8*)*, i32 (i8*, i8*, i8*, i32, i32*, i32)* (i32, i32, i32)*, i32 (i32, i32, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, %struct.st_MDataArray**)*, i32 (i32, i32, i32, i8*, i32, i8*, i32, i8*)*, i32 (i32, double, i32, double*)*, i32 (i32, double, i32, %"struct.std::complex"*, i32*)*, i8* (%struct.st_WolframLibraryData*, i8*)*, i32 (%struct.st_WolframLibraryData*, i8*, i32, i32, i32, i32*, i8**, i32, i32, i8*)*, i8** (%struct.st_WolframLibraryData*, i32)*, i32 (%struct.st_WolframLibraryData*, i32, %union.MArgument*, [1 x i32])* (i8*, i8*)*, i32 (%struct.st_WolframLibraryData*, i32, %union.MArgument*, [1 x i32])* (i8*)*, i32 (i8*, i32, i32)*, %struct.st_MDataArray* (i8*, i32, i32)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray**)* }
%struct.M_TENSOR_INITIALIZATION_DATA_STRUCT = type { %struct.st_MDataArray**, i32, i32 }
%union.MArgument = type { i32* }
%struct.st_MInputStream = type { i8*, i8*, i8*, i32, i32, i32, i32, i8*, i8*, i8*, %struct.st_MInputStream*, i32 (%struct.st_MInputStream*, i8*, i32)*, i32 (%struct.st_MInputStream*, i64)*, i32 (%struct.st_MInputStream*)*, i64 (%struct.st_MInputStream*)*, i8* (%struct.st_MInputStream*)*, void (%struct.st_MInputStream*)*, i64 (%struct.st_MInputStream*)*, void (%struct.st_MInputStream*, i8*)*, i32 (%struct.st_MInputStream*)*, i32 (%struct.st_MInputStream*)*, void (%struct.st_MInputStream*)*, i32 (%struct.st_MInputStream*)* }
%struct.st_MOutputStream = type { i8*, i8*, i8*, i32, i32, i32, i32, i8*, i8*, i8*, %struct.st_MOutputStream*, i32 (%struct.st_MOutputStream*, i8*, i32)*, i32 (%struct.st_MOutputStream*)*, i64 (%struct.st_MOutputStream*)*, i8* (%struct.st_MOutputStream*)*, void (%struct.st_MOutputStream*)*, i32 (%struct.st_MOutputStream*)*, void (%struct.st_MOutputStream*, i8*)*, i32 (%struct.st_MOutputStream*)* }
%struct.st_WolframIOLibrary_Functions = type { i32 ()*, i32 (void (i32, i8*)*, i8*)*, void (i32, i8*, %struct.st_DataStore*)*, i32 (i32)*, i32 (i32)*, %struct.st_DataStore* ()*, void (%struct.st_DataStore*, i32)*, void (%struct.st_DataStore*, double)*, void (%struct.st_DataStore*, %"struct.std::complex")*, void (%struct.st_DataStore*, i8*)*, void (%struct.st_DataStore*, %struct.st_MDataArray*)*, void (%struct.st_DataStore*, %struct.st_MDataArray*)*, void (%struct.st_DataStore*, %struct.IMAGEOBJ_ENTRY*)*, void (%struct.st_DataStore*, %struct.st_DataStore*)*, void (%struct.st_DataStore*, i8*, i32)*, void (%struct.st_DataStore*, i8*, double)*, void (%struct.st_DataStore*, i8*, %"struct.std::complex")*, void (%struct.st_DataStore*, i8*, i8*)*, void (%struct.st_DataStore*, i8*, %struct.st_MDataArray*)*, void (%struct.st_DataStore*, i8*, %struct.st_MDataArray*)*, void (%struct.st_DataStore*, i8*, %struct.IMAGEOBJ_ENTRY*)*, void (%struct.st_DataStore*, i8*, %struct.st_DataStore*)*, i32 (i32)*, void (%struct.st_DataStore*)*, %struct.st_DataStore* (%struct.st_DataStore*)*, i32 (%struct.st_DataStore*)*, %struct.DataStoreNode_t* (%struct.st_DataStore*)*, %struct.DataStoreNode_t* (%struct.st_DataStore*)*, %struct.DataStoreNode_t* (%struct.DataStoreNode_t*)*, i32 (%struct.DataStoreNode_t*)*, i32 (%struct.DataStoreNode_t*, %union.MArgument*)*, i32 (%struct.DataStoreNode_t*, i8**)*, void (%struct.st_DataStore*, i32)*, void (%struct.st_DataStore*, i8*, i32)*, void (%struct.st_DataStore*, %struct.st_MDataArray*)*, void (%struct.st_DataStore*, i8*, %struct.st_MDataArray*)*, void (%struct.st_DataStore*, %struct.st_MSparseArray*)*, void (%struct.st_DataStore*, i8*, %struct.st_MSparseArray*)* }
%struct.st_DataStore = type opaque
%struct.IMAGEOBJ_ENTRY = type { i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32*, i32, i32, i32, i32, i8*, double*, double*, i8*, i32, void (i8*, i8*, i32)*, void (i8*, i8*, i32, i32, i32)*, void (i8*, i8*, i32, i32, i32, i32, i32)*, void (i8*, i8*, i32, i32, i32, i32, i32, i32)*, void (%struct.IMAGEOBJ_ENTRY*, double, i8*)*, i8* (%struct.IMAGEOBJ_ENTRY*, double*, i32)*, i32, i32 }
%struct.DataStoreNode_t = type opaque
%struct.st_MSparseArray = type { %struct.st_SparseArrayAllocation*, %struct.st_MTensorSparseArrayAllocation*, i32*, i32*, %struct.st_TensorProperty*, i32, i32, i32 }
%struct.st_SparseArrayAllocation = type { %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i8* }
%struct.st_MTensorSparseArrayAllocation = type { %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_SparseArraySkeleton* }
%struct.st_SparseArraySkeleton = type { i8*, i8*, i32*, i32*, %struct.st_TensorProperty*, i32, i32, i32 }
%struct.st_TensorProperty = type { double, i32*, i32, i32, i32 }
%struct.MLENV_STRUCT = type opaque
%struct.st_WolframSparseLibrary_Functions = type { i32 (%struct.st_MSparseArray*, %struct.st_MSparseArray**)*, void (%struct.st_MSparseArray*)*, void (%struct.st_MSparseArray*)*, void (%struct.st_MSparseArray*)*, i32 (%struct.st_MSparseArray*)*, i32 (%struct.st_MSparseArray*)*, i32* (%struct.st_MSparseArray*)*, %struct.st_MDataArray** (%struct.st_MSparseArray*)*, %struct.st_MDataArray** (%struct.st_MSparseArray*)*, %struct.st_MDataArray** (%struct.st_MSparseArray*)*, %struct.st_MDataArray** (%struct.st_MSparseArray*)*, i32 (%struct.st_MSparseArray*, %struct.st_MDataArray**)*, i32 (%struct.st_MSparseArray*, %struct.st_MDataArray*, %struct.st_MSparseArray**)*, i32 (%struct.st_MSparseArray*, %struct.st_MDataArray**)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MSparseArray**)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MSparseArray**)* }
%struct.st_WolframImageLibrary_Functions = type { i32 (i32, i32, i32, i32, i32, i32, %struct.IMAGEOBJ_ENTRY**)*, i32 (i32, i32, i32, i32, i32, i32, i32, %struct.IMAGEOBJ_ENTRY**)*, i32 (%struct.IMAGEOBJ_ENTRY*, %struct.IMAGEOBJ_ENTRY**)*, void (%struct.IMAGEOBJ_ENTRY*)*, void (%struct.IMAGEOBJ_ENTRY*)*, void (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, i8*)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, i8*)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, i16*)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, float*)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, double*)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, i8)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, i8)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, i16)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, float)*, i32 (%struct.IMAGEOBJ_ENTRY*, i32*, i32, double)*, i8* (%struct.IMAGEOBJ_ENTRY*)*, i8* (%struct.IMAGEOBJ_ENTRY*)*, i8* (%struct.IMAGEOBJ_ENTRY*)*, i16* (%struct.IMAGEOBJ_ENTRY*)*, float* (%struct.IMAGEOBJ_ENTRY*)*, double* (%struct.IMAGEOBJ_ENTRY*)*, %struct.IMAGEOBJ_ENTRY* (%struct.IMAGEOBJ_ENTRY*, i32, i32)* }
%struct.st_WolframRawArrayLibrary_Functions = type { i32 (i32, i32, i32*, %struct.st_MDataArray**)*, void (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray**)*, void (%struct.st_MDataArray*)*, void (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32* (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i8* (%struct.st_MDataArray*)*, %struct.st_MDataArray* (%struct.st_MDataArray*, i32)* }
%struct.st_WolframNumericArrayLibrary_Functions = type { i32 (i32, i32, i32*, %struct.st_MDataArray**)*, void (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray**)*, void (%struct.st_MDataArray*)*, void (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32* (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i8* (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32, double)* }
%struct.MemoryLinkedList_struct = type { i8*, %struct.MemoryLinkedList_struct* }
%struct.MBag_struct = type { %struct.MemoryLinkedList_struct*, %struct.MemoryLinkedList_struct**, i32, i32, i8*, i8*, i32, i32, i32 }
%struct.part_ind = type { i32, i32, i32*, i32, i32 }
%struct.MBagArray_struct = type { i32, i32, %struct.MBag_struct** }
%struct.RuntimeData_struct = type { i8*, %struct.MBagArray_struct*, %struct.BPdata*, %struct.MemoryLinkedList_struct*, i8*, i8*, i8*, %struct.MemoryLinkedList_struct*, %struct.MemoryLinkedList_struct*, i32, %struct.st_WolframLibraryData* }
%struct.BPdata = type opaque
%struct.RandomGenerator_struct = type { %struct.RandomGeneratorMethodData_struct*, %struct.RandomGeneratorFunctions_struct*, void (%struct.RandomGenerator_struct*, i32*, i32)*, i8* }
%struct.RandomGeneratorMethodData_struct = type { i32, i8*, i32 }
%struct.RandomGeneratorFunctions_struct = type { i32 (i32, %struct.RandomGenerator_struct*)*, i32 (i32*, i32, %struct.RandomGenerator_struct*)*, i32 (i32*, i32, i32, i32, %struct.RandomGenerator_struct*)*, i32 (double*, i32, double, double, %struct.RandomGenerator_struct*)*, i32 (i8**, i32, i8*, i8*, %struct.RandomGenerator_struct*)*, i32 (i8**, i32, i8*, i8*, double, %struct.RandomGenerator_struct*)*, %struct.RandomDistributionFunctionArray_struct* }
%struct.RandomDistributionFunctionArray_struct = type opaque
%struct._Unwind_Control_Block = type { i64, void (i32, %struct._Unwind_Control_Block*)*, %struct.st_PageInformation, %struct.anon.0, %struct.anon.1, %struct.anon.2 }
%struct.anon.0 = type { i32, [5 x i32] }
%struct.anon.1 = type { [4 x i32] }
%struct.anon.2 = type { i32, i32*, i32, i32 }
%struct.__jmp_buf_tag = type { [64 x i32], i32, %struct.__sigset_t, [4 x i8] }
%struct.__sigset_t = type { [32 x i32] }
%"class.std::exception" = type { i32 (...)** }
%class.WolframRuntimeException = type { %"class.std::exception", i32 }
%"class.std::__cxx11::basic_string" = type { %struct.st_KMutex, i32, %union.anon }
%union.anon = type { i32, [12 x i8] }
%"struct.std::complex.203" = type { { float, float } }
%struct.lldiv_t = type { i64, i64 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_put"* }
%"class.std::ios_base" = type { i32 (...)**, i32, i32, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i32 }
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i32, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type { i32 (...)**, i32 }
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet", %struct.__locale_struct*, i8, [3 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [2 x i8] }>
%struct.__locale_struct = type { [13 x %struct.__locale_data*], i16*, i32*, i32*, [13 x i8*] }
%struct.__locale_data = type opaque
%"class.std::num_put" = type { %"class.std::locale::facet" }
%"class.std::vector" = type { %"struct.std::_Vector_base" }
%"struct.std::_Vector_base" = type { %"struct.std::_Vector_base<std::future<void>, std::allocator<std::future<void> > >::_Vector_impl" }
%"struct.std::_Vector_base<std::future<void>, std::allocator<std::future<void> > >::_Vector_impl" = type { %"class.std::future"*, %"class.std::future"*, %"class.std::future"* }
%"class.std::future" = type { %"class.std::__basic_future" }
%"class.std::__basic_future" = type { %"class.std::shared_ptr" }
%"class.std::shared_ptr" = type { %"class.std::__shared_ptr" }
%"class.std::__shared_ptr" = type { %"class.std::__future_base::_State_baseV2"*, %"class.std::__shared_count" }
%"class.std::__future_base::_State_baseV2" = type { i32 (...)**, %"class.std::unique_ptr", %"class.std::__atomic_futex_unsigned", %"struct.std::atomic_flag", %union.ZHeader }
%"class.std::unique_ptr" = type { %"class.std::tuple" }
%"class.std::tuple" = type { %"struct.std::_Tuple_impl" }
%"struct.std::_Tuple_impl" = type { %"struct.std::_Head_base.1" }
%"struct.std::_Head_base.1" = type { %"struct.std::__future_base::_Result_base"* }
%"struct.std::__future_base::_Result_base" = type { i32 (...)**, %struct.st_KMutex }
%"class.std::__atomic_futex_unsigned" = type { %"class.std::thread" }
%"class.std::thread" = type { %union.ZHeader }
%"struct.std::atomic_flag" = type { %"class.std::ios_base::Init" }
%"class.std::__shared_count" = type { %"class.std::_Sp_counted_base"* }
%"class.std::_Sp_counted_base" = type { i32 (...)**, i32, i32 }
%union.pthread_attr_t = type { i32, [32 x i8] }
%"class.std::unique_ptr.25" = type { %"class.std::tuple.26" }
%"class.std::tuple.26" = type { %"struct.std::_Tuple_impl.27" }
%"struct.std::_Tuple_impl.27" = type { %"struct.std::_Head_base.30" }
%"struct.std::_Head_base.30" = type { %"class.std::exception"* }
%"class.std::__future_base::_Deferred_state" = type { %"class.std::__future_base::_State_baseV2", %"class.std::unique_ptr.14", %"struct.std::_Bind_simple" }
%"class.std::unique_ptr.14" = type { %"class.std::tuple.15" }
%"class.std::tuple.15" = type { %"struct.std::_Tuple_impl.16" }
%"struct.std::_Tuple_impl.16" = type { %"struct.std::_Head_base.17" }
%"struct.std::_Head_base.17" = type { %"struct.std::__future_base::_Result"* }
%"struct.std::__future_base::_Result" = type { %"struct.std::__future_base::_Result_base" }
%"struct.std::_Bind_simple" = type { %"class.std::tuple.9" }
%"class.std::tuple.9" = type { %"struct.std::_Tuple_impl.10" }
%"struct.std::_Tuple_impl.10" = type { %"struct.std::_Head_base.11" }
%"struct.std::_Head_base.11" = type { %class.anon.7 }
%class.anon.7 = type { i64*, i64*, i32, i32, i32, %class.anon*, %"class.std::vector.2"* }
%class.anon = type { i64 (i64)* }
%"class.std::vector.2" = type { %"struct.std::_Vector_base.3" }
%"struct.std::_Vector_base.3" = type { %"struct.std::_Vector_base<error, std::allocator<error> >::_Vector_impl" }
%"struct.std::_Vector_base<error, std::allocator<error> >::_Vector_impl" = type { i32*, i32*, i32* }
%"union.std::_Any_data" = type { %"union.std::_Nocopy_types" }
%"union.std::_Nocopy_types" = type { { i32, i32 } }
%"class.std::function" = type { %"class.std::_Function_base", void (%"class.std::unique_ptr"*, %"union.std::_Any_data"*)* }
%"class.std::_Function_base" = type { %"union.std::_Any_data", i1 (%"union.std::_Any_data"*, %"union.std::_Any_data"*, i32)* }
%"class.std::_Sp_counted_ptr_inplace.53" = type { %"class.std::_Sp_counted_base", %"class.std::_Sp_counted_ptr_inplace<std::__future_base::_Deferred_state<std::_Bind_simple<(lambda at /Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/WolframRTL/compilerruntime/src/utils/parallel_for.hpp:87:29) ()>, void>, std::allocator<std::__future_base::_Deferred_state<std::_Bind_simple<(lambda at /Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/WolframRTL/compilerruntime/src/utils/parallel_for.hpp:87:29) ()>, void> >, __gnu_cxx::_S_atomic>::_Impl" }
%"class.std::_Sp_counted_ptr_inplace<std::__future_base::_Deferred_state<std::_Bind_simple<(lambda at /Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/WolframRTL/compilerruntime/src/utils/parallel_for.hpp:87:29) ()>, void>, std::allocator<std::__future_base::_Deferred_state<std::_Bind_simple<(lambda at /Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/WolframRTL/compilerruntime/src/utils/parallel_for.hpp:87:29) ()>, void> >, __gnu_cxx::_S_atomic>::_Impl" = type { %"struct.__gnu_cxx::__aligned_buffer.55" }
%"struct.__gnu_cxx::__aligned_buffer.55" = type { %"union.std::aligned_storage<52, 4>::type" }
%"union.std::aligned_storage<52, 4>::type" = type { [52 x i8] }
%"class.std::type_info" = type { i32 (...)**, i8* }
%"class.std::__future_base::_Async_state_commonV2" = type { %"class.std::__future_base::_State_baseV2", %"class.std::thread", %union.ZHeader }
%"struct.std::thread::_State_impl" = type { %"class.std::exception", %"struct.std::_Bind_simple.31" }
%"struct.std::_Bind_simple.31" = type { %"class.std::tuple.32" }
%"class.std::tuple.32" = type { %"struct.std::_Tuple_impl.33" }
%"struct.std::_Tuple_impl.33" = type { %"struct.std::_Head_base.34" }
%"struct.std::_Head_base.34" = type { %class.anon.24 }
%class.anon.24 = type { %"class.std::__future_base::_Async_state_impl"* }
%"class.std::__future_base::_Async_state_impl" = type { %"class.std::__future_base::_Async_state_commonV2", %"class.std::unique_ptr.14", %"struct.std::_Bind_simple" }
%"class.std::future_error" = type { %"class.std::logic_error", %"struct.std::error_condition" }
%"class.std::logic_error" = type { %"class.std::exception", %"struct.std::__cow_string" }
%"struct.std::__cow_string" = type { %struct.st_KMutex }
%"struct.std::error_condition" = type { i32, %"class.std::exception"* }
%"class.std::_Sp_counted_ptr_inplace" = type { %"class.std::_Sp_counted_base", %"class.std::_Sp_counted_ptr_inplace<std::__future_base::_Async_state_impl<std::_Bind_simple<(lambda at /Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/WolframRTL/compilerruntime/src/utils/parallel_for.hpp:87:29) ()>, void>, std::allocator<std::__future_base::_Async_state_impl<std::_Bind_simple<(lambda at /Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/WolframRTL/compilerruntime/src/utils/parallel_for.hpp:87:29) ()>, void> >, __gnu_cxx::_S_atomic>::_Impl" }
%"class.std::_Sp_counted_ptr_inplace<std::__future_base::_Async_state_impl<std::_Bind_simple<(lambda at /Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/WolframRTL/compilerruntime/src/utils/parallel_for.hpp:87:29) ()>, void>, std::allocator<std::__future_base::_Async_state_impl<std::_Bind_simple<(lambda at /Developer/jenkins/workspace/Component.LLVM.RuntimeLibrary.Linux-ARM.12.0/WolframRTL/compilerruntime/src/utils/parallel_for.hpp:87:29) ()>, void> >, __gnu_cxx::_S_atomic>::_Impl" = type { %"struct.__gnu_cxx::__aligned_buffer" }
%"struct.__gnu_cxx::__aligned_buffer" = type { %"union.std::aligned_storage<60, 4>::type" }
%"union.std::aligned_storage<60, 4>::type" = type { [60 x i8] }
%struct.st_IntegerArrayHashTable = type { %struct.st_hashtable*, i32, i8*, i32 }
%struct.st_hashtable = type { %struct.st_hash_entry**, i32 (%struct.st_hash_entry*)*, void (%struct.st_hash_entry*, %struct.st_hash_entry*)*, i32 (%struct.st_hash_entry*, %struct.st_hash_entry*)*, void (%struct.st_hash_entry*)*, void (%struct.st_hash_entry*)*, void (%struct.st_hashtable*)*, i32, i32, i32, i32, i32, i32 }
%struct.st_hash_entry = type { i8*, i8*, i32, %struct.st_hash_entry* }
%struct.st_WolframLibraryData.259 = type { void (i8*)*, i32 (i32, i32, i32*, %struct.st_MDataArray**)*, void (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray**)*, i32 (%struct.st_MDataArray*)*, void (%struct.st_MDataArray*)*, void (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*, i32*, i32)*, i32 (%struct.st_MDataArray*, i32*, double)*, i32 (%struct.st_MDataArray*, i32*, %"struct.std::complex")*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray*, i32*, i32)*, i32 (%struct.st_MDataArray*, i32*, i32*)*, i32 (%struct.st_MDataArray*, i32*, double*)*, i32 (%struct.st_MDataArray*, i32*, %"struct.std::complex"*)*, i32 (%struct.st_MDataArray*, i32*, i32, %struct.st_MDataArray**)*, i32 (%struct.st_MDataArray*)*, i32* (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*)*, i32* (%struct.st_MDataArray*)*, double* (%struct.st_MDataArray*)*, %"struct.std::complex"* (%struct.st_MDataArray*)*, void (i8*)*, i32 ()*, %struct.MLINK_STRUCT* (%struct.st_WolframLibraryData.259*)*, i32 (%struct.MLINK_STRUCT*)*, i32 (%struct.st_WolframLibraryData.259*, i8*, i32, i32, i8*)*, %struct.st_WolframRuntimeData*, %struct.st_WolframCompileLibrary_Functions.245*, i32, i32 (i8*, void (%struct.st_MInputStream*, i8*, i8*)*, i32 (i8*, i8*)*, i8*, void (i8*)*)*, i32 (i8*)*, i32 (i8*, void (%struct.st_MOutputStream*, i8*, i8*, i32)*, i32 (i8*, i8*)*, i8*, void (i8*)*)*, i32 (i8*)*, %struct.st_WolframIOLibrary_Functions*, %struct.MLENV_STRUCT* (%struct.st_WolframLibraryData.259*)*, %struct.st_WolframSparseLibrary_Functions*, %struct.st_WolframImageLibrary_Functions*, i32 (i8*, void (%struct.st_WolframLibraryData.259*, i32, i32)*)*, i32 (i8*)*, i32 (i8*, i32)*, i32 (i8*, i32 (%struct.st_WolframLibraryData.259*, i32, %struct.st_MDataArray*)*)*, i32 (i8*)*, i32 (i32, i32, %union.MArgument*, [1 x i32])*, i32 (i32)*, i32 (i8*, i8)*, i32 ()*, %struct.st_WolframRawArrayLibrary_Functions*, %struct.st_WolframNumericArrayLibrary_Functions* }
%struct.st_WolframCompileLibrary_Functions.245 = type { i32, %struct.M_TENSOR_INITIALIZATION_DATA_STRUCT* (%struct.st_WolframLibraryData.259*, i32)*, void (%struct.M_TENSOR_INITIALIZATION_DATA_STRUCT*)*, void (%struct.st_WolframLibraryData.259*, i32)*, %struct.st_MDataArray* ()*, i32 (%struct.st_MDataArray**, i32, i32, i32*)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray*, i32*)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*, i32*, i32)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i8**)*, i32 (%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i8**)*, i32 (i8*, i8*, i32, i32*, i32)* (i32, i32)*, i32 (i32, i32, %struct.st_MDataArray*, i32, %struct.st_MDataArray**)*, i32 (i32, i32, i32, i8*, i32, i8*)*, i32 (i8*, i8*, i8*, i32, i32*, i32)* (i32, i32, i32)*, i32 (i32, i32, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, %struct.st_MDataArray**)*, i32 (i32, i32, i32, i8*, i32, i8*, i32, i8*)*, i32 (i32, double, i32, double*)*, i32 (i32, double, i32, %"struct.std::complex"*, i32*)*, i8* (%struct.st_WolframLibraryData.259*, i8*)*, i32 (%struct.st_WolframLibraryData.259*, i8*, i32, i32, i32, i32*, i8**, i32, i32, i8*)*, i8** (%struct.st_WolframLibraryData.259*, i32)*, {}* (i8*, i8*)*, {}* (i8*)*, i32 (i8*, i32, i32)*, %struct.st_MDataArray* (i8*, i32, i32)*, i32 (%struct.st_MDataArray*, %struct.st_MDataArray**)* }
%struct.MULTIPLE_ARRAY_HASH_STRUCT = type { i32, i32*, i32**, i32 }
%struct.idc_num_t = type { i32, %"union.idc_num_t::num_t" }
%"union.idc_num_t::num_t" = type { %"struct.std::complex" }
%struct.part_message_data_struct = type { [2 x i32], void (i8*, %struct.part_message_data_struct*)*, i8*, i8* }
%"class.std::_Hashtable" = type { %"struct.std::__detail::_Hash_node_base"**, i32, %"struct.std::__detail::_Hash_node_base", i32, %"struct.std::__detail::_Prime_rehash_policy", %"struct.std::__detail::_Hash_node_base"* }
%"struct.std::__detail::_Hash_node_base" = type { %"struct.std::__detail::_Hash_node_base"* }
%"struct.std::__detail::_Prime_rehash_policy" = type { float, i32 }
%"struct.std::pair.38" = type <{ %"struct.std::__detail::_Node_const_iterator", i8, [3 x i8] }>
%"struct.std::__detail::_Node_const_iterator" = type { %"struct.std::__detail::_Node_iterator_base" }
%"struct.std::__detail::_Node_iterator_base" = type { %"struct.std::__detail::_Hash_node"* }
%"struct.std::__detail::_Hash_node" = type { %"struct.std::__detail::_Hash_node_value_base", i32 }
%"struct.std::__detail::_Hash_node_value_base" = type { %"struct.std::__detail::_Hash_node_base", %"struct.__gnu_cxx::__aligned_buffer.342" }
%"struct.__gnu_cxx::__aligned_buffer.342" = type { %"union.std::aligned_storage<28, 4>::type" }
%"union.std::aligned_storage<28, 4>::type" = type { [28 x i8] }
%"struct.std::pair.37" = type { %"class.std::__cxx11::basic_string", i32 }
%"struct.std::pair.42" = type { i8, i32 }
%struct.st_MNumericArrayConvertData = type { double, i32, i8 }
%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i32, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i32, i32, [40 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%union.pthread_cond_t = type { %struct.anon }
%struct.anon = type { i32, i32, i64, i64, i64, i8*, i32, i32 }
%struct.RandomGeneratorMethod_struct = type { i8*, i32, %struct.RandomGenerator_struct* (i8*)*, %struct.RandomGenerator_struct* (%struct.RandomGenerator_struct*)*, void (%struct.RandomGenerator_struct*)*, %struct.RandomGeneratorMethod_struct*, i8* (%struct.RandomGenerator_struct*)*, %struct.RandomGenerator_struct* (i8*)*, i8* (i8*)*, i8* }
%struct.random_state_entry_struct = type { i32, %struct.RandomGenerator_struct*, %struct.random_state_entry_struct*, %struct.random_state_entry_struct* }
%struct.RandomState_struct = type { %struct.random_state_entry_struct*, i8*, i32 }
%struct.buffer_data_struct = type { i32*, i32, i32, i32, i32 }
%struct.RandomBuffer_struct = type { %struct.RandomGeneratorMethodData_struct*, %struct.RandomGeneratorFunctions_struct*, void (%struct.RandomGenerator_struct*, i32*, i32)*, %struct.RandomBufferState_struct* }
%struct.RandomBufferState_struct = type { %struct.buffer_data_struct*, i32 (i32*, i32*, i32*, %struct.RandomGenerator_struct*)*, %struct.RandomGenerator_struct* }
%struct.PositionTree_struct = type { i32, %struct.PositionTree_struct*, %struct.PositionTree_struct*, double }
%struct.PMT32State_struct = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32* }

declare void @_Z14InitAllocationi(i32)

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.value(metadata, metadata, metadata) #0

declare void @MCinterrupt()

declare i8* @kCalloc(i32, i32, i32, i32)

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1) #1

declare i8* @kRealloc(i8*, i32, i32, i32)

declare void @nomem()

declare i32 @puts(i8*)

declare void @exit(i32)

declare i8* @Malloc_fun(i32)

declare i8* @MallocAlign16_fun(i32)

declare i8* @MallocAlign_mreal_fun(i32)

declare i8* @Calloc_fun(i32, i32)

declare i8* @CallocAlign16_fun(i32, i32)

declare i8* @CallocAlign_mreal_fun(i32, i32)

declare i8* @Realloc_fun(i8*, i32)

declare i8* @ReallocAlign_mreal_fun(i8*, i32)

declare void @Free_fun(i8*)

declare i8* @Malloc_fun_for_gmp(i32)

declare i8* @Realloc_fun_for_gmp(i8*, i32, i32)

declare void @Free_fun_for_gmp(i8*, i32)

declare void @_Z10init_alloci(i32)

declare %struct.st_ZSpan**** @_Z26New_AllocationPageMapTablev()

declare i8* @malloc(i32)

declare void @_Z29Delete_AllocationPageMapTablePPPP8st_ZSpan(%struct.st_ZSpan****)

declare void @free(i8*)

declare %struct.st_ZSpan* @_Z29AllocationPageMap_addOrRemoveP9st_ZBlockjP8st_ZSpan(%struct.st_ZBlock*, i32, %struct.st_ZSpan*)

declare i32 @MemoryInUse()

declare void @_ZTH16thread_allocator()

declare %struct.st_ZAllocator** @_ZTW16thread_allocator()

declare i32 @MaxMemoryUsed()

declare i32 @MaxMemoryListPush(%struct.st_MaxMemoryCell**)

declare i32 @MaxMemoryListPop(%struct.st_MaxMemoryCell**)

declare i32 @MaxMemoryListLength(%struct.st_MaxMemoryCell*)

declare i32* @FullMemoryInformation(i32*, i32)

declare void @CompactifyMemory()

declare void @freeMemorySizeArray(i32*)

declare i32 @MCinuse()

declare i32 @MCliminuse()

declare void @MCsetlimit(i32)

declare void @MC_check(i32)

declare void @RecordCUDDMemory(i32)

declare void @_GLOBAL__sub_I_alloc_page.cpp()

declare %"class.std::ios_base::Init"* @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*)

declare %"class.std::ios_base::Init"* @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*)

declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*)

declare i8* @_Z12ZPAGE_MALLOCj(i32)

declare i8* @mmap(i8*, i32, i32, i32, i32, i32)

declare void @_Z10ZPAGE_FREEPvj(i8*, i32)

declare i32 @munmap(i8*, i32)

declare %struct.st_PageInformation* @_Z19New_PageInformationv()

declare i32 @sysconf(i32)

; Function Attrs: nounwind readnone speculatable
declare i32 @llvm.ctlz.i32(i32, i1) #0

declare void @_Z22Delete_PageInformationP18st_PageInformation(%struct.st_PageInformation*)

declare i64 @getSystemMemory()

declare %struct.st_ZSpan* @_Z9New_ZSpanP7st_ZRow(%struct.st_ZRow*)

declare void @_Z12Delete_ZSpanP8st_ZSpan(%struct.st_ZSpan*)

declare void @_Z19MergeThreadSpanDataP7st_ZRow(%struct.st_ZRow*)

declare %struct.st_ZAllocator* @_Z19ZAllocator_getOrAddv()

declare %struct.st_ZAllocator* @_ZL14ZAllocator_addv()

declare void @_Z17ZAllocator_removev()

declare void @_Z17Delete_ZAllocatorP13st_ZAllocatori(%struct.st_ZAllocator*, i32)

declare void @_Z20FreeThreadMallocDataP13st_ZAllocator(%struct.st_ZAllocator*)

declare void @Delete_ZAllocatorList()

declare i8* @GetAllocatorList()

declare void @InitializeMemory()

declare void @ReclaimExitMemory()

declare i32 @zalloc_round(i32)

declare i8* @_Z11zalloc_nullji(i32, i32)

declare i8* @zalloc(i32, i32, i32)

declare void @zfree(i8*)

declare i32 @zalloc_size(i8*)

declare i8* @zrealloc(i8*, i32, i32, i32)

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* nocapture writeonly, i8* nocapture readonly, i32, i1) #1

declare void @_GLOBAL__sub_I_system_malloc.cpp()

declare void @_Z17InitRTLArithmetici(i32)

declare void @_GLOBAL__sub_I_bitoputils.cpp()

declare i32 @BigitBitLength(i32)

declare i32 @MintBitLength(i32)

declare i32 @BigitBitCount(i32)

; Function Attrs: nounwind readnone speculatable
declare i32 @llvm.ctpop.i32(i32) #0

declare i32 @MintBitCount(i32)

declare i32 @numzbitsbigit(i32)

; Function Attrs: nounwind readnone speculatable
declare i32 @llvm.cttz.i32(i32, i1) #0

declare i32 @numzbitsint(i32)

declare i32* @New_BV(i32)

declare void @BV_Mark(i32*, i32)

declare i32 @BV_SetQ(i32*, i32)

declare void @_GLOBAL__sub_I_compare.cpp()

declare i32 @CompareDoubleDouble(double, double, double)

declare i32 @__gxx_personality_v0(...)

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start.p0i8(i64, i8* nocapture) #1

declare double @modf(double, double*)

; Function Attrs: nounwind readnone speculatable
declare double @llvm.exp2.f64(double) #0

declare double @ldexp(double, i32)

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end.p0i8(i64, i8* nocapture) #1

declare i32 @CompareFloatFloat(float, float, float)

declare float @modff(float, float*)

; Function Attrs: nounwind readnone speculatable
declare float @llvm.exp2.f32(float) #0

declare float @ldexpf(float, i32)

declare i32 @near1_double(double, double, double)

declare i32 @near1_float(float, float, float)

declare i32 @near1_reim_mcomplex(double, %"struct.std::complex")

declare double @ToleranceShiftFactor(double)

declare float @ToleranceShiftFactorFloat(float)

declare i32 @TestException(double)

; Function Attrs: nounwind readnone speculatable
declare double @llvm.fabs.f64(double) #0

declare void @_GLOBAL__sub_I_machfuns1_0.cpp()

declare i32 @machfunc_i_total(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN10vfun_totalIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_totalIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL7plus_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE19EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32*)

declare i32 @_ZN10vfun_totalIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_totalIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL7plus_opIiiLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE19EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32*)

declare i32 @_ZL11vfun1_0_runIiiEiPFiPT_PKT0_RKiPS5_EPFS0_RKS0_SB_ES1_S4_S6_S7_(i32 (i32*, i32*, i32*, i32*)*, i32 (i32*, i32*)*, i32*, i32*, i32*, i32*)

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i1) #1

declare void @_ZL13vfun1_0_rtfunIiiEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

; Function Attrs: nounwind readnone
declare i32 @llvm.eh.typeid.for(i8*) #2

declare i8* @__cxa_begin_catch(i8*)

declare void @__cxa_end_catch()

declare i32 @__pthread_key_create(i32*, void (i8*)*)

declare i32 @pthread_mutex_lock(%union.pthread_mutex_t*)

declare void @_ZSt20__throw_system_errori(i32)

declare i32 @pthread_mutex_unlock(%union.pthread_mutex_t*)

; Function Attrs: nounwind readnone speculatable
declare { i32, i1 } @llvm.sadd.with.overflow.i32(i32, i32) #0

declare i8* @__cxa_allocate_exception(i32)

declare void @__cxa_throw(i8*, i8*, i8*)

declare void @__clang_call_terminate(i8*)

declare void @_ZSt9terminatev()

declare i32 @machfunc_d_total(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN10vfun_totalIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_totalIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL7plus_opIddLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS7_2opE19EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double*, double*)

declare i32 @_ZN10vfun_totalIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_totalIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL7plus_opIddLNS_13runtime_flagsE0EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS7_2opE19EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double*, double*)

declare i32 @_ZL11vfun1_0_runIddEiPFiPT_PKT0_RKiPS5_EPFS0_RKS0_SB_ES1_S4_S6_S7_(i32 (double*, double*, i32*, i32*)*, double (double*, double*)*, double*, double*, i32*, i32*)

declare void @_ZL13vfun1_0_rtfunIddEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_c_total(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN10vfun_totalISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_totalISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare %"struct.std::complex" @_ZN3wrt9scalar_op6binaryL7plus_opISt7complexIdES4_LNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE19EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(%"struct.std::complex"*, %"struct.std::complex"*)

declare i32 @_ZN10vfun_totalISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_totalISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare %"struct.std::complex" @_ZN3wrt9scalar_op6binaryL7plus_opISt7complexIdES4_LNS_13runtime_flagsE0EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE19EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(%"struct.std::complex"*, %"struct.std::complex"*)

declare void @_ZL13vfun1_0_rtfunISt7complexIdES1_EvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_i_min(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN8vfun_minIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_minIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL6min_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE16EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32*)

declare i32 @_ZN8vfun_minIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_minIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL6min_opIiiLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE16EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32*)

declare i32 @machfunc_d_min(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN8vfun_minIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_minIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL6min_opIddLNS_13runtime_flagsE1EEENSt9enable_ifIXooaasr17is_floating_pointIT_EE5valuentsr10is_complexIT0_EE5valueaasr17is_floating_pointIS6_EE5valuentsr10is_complexIS5_EE5valueENS1_6detail7op_infoILNS7_2opE16EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double*, double*)

declare i32 @_ZN8vfun_minIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_minIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL6min_opIddLNS_13runtime_flagsE0EEENSt9enable_ifIXooaasr17is_floating_pointIT_EE5valuentsr10is_complexIT0_EE5valueaasr17is_floating_pointIS6_EE5valuentsr10is_complexIS5_EE5valueENS1_6detail7op_infoILNS7_2opE16EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double*, double*)

; Function Attrs: nounwind readnone speculatable
declare double @llvm.minnum.f64(double, double) #0

declare i32 @machfunc_i_max(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN8vfun_maxIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_maxIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL6max_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE15EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32*)

declare i32 @_ZN8vfun_maxIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_maxIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL6max_opIiiLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE15EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32*)

declare i32 @machfunc_d_max(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN8vfun_maxIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_maxIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL6max_opIddLNS_13runtime_flagsE1EEENSt9enable_ifIXooaasr17is_floating_pointIT_EE5valuentsr10is_complexIT0_EE5valueaasr17is_floating_pointIS6_EE5valuentsr10is_complexIS5_EE5valueENS1_6detail7op_infoILNS7_2opE15EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double*, double*)

declare i32 @_ZN8vfun_maxIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_maxIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL6max_opIddLNS_13runtime_flagsE0EEENSt9enable_ifIXooaasr17is_floating_pointIT_EE5valuentsr10is_complexIT0_EE5valueaasr17is_floating_pointIS6_EE5valuentsr10is_complexIS5_EE5valueENS1_6detail7op_infoILNS7_2opE15EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double*, double*)

; Function Attrs: nounwind readnone speculatable
declare double @llvm.maxnum.f64(double, double) #0

declare i32 @machfunc_i_minmax(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN11vfun_minmaxISt4pairIiiEiE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKiRS8_S9_(%"struct.std::pair"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_minmaxISt4pairIiiEiE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKiRS8_S9_(%"struct.std::pair"*, i32*, i32*, i32*)

declare void @_ZN3wrt9scalar_op6binaryL9minmax_opISt4pairIiiES4_LNS_13runtime_flagsE1EEENSt9enable_ifIXaasr7is_pairIT_EE5valuesr7is_pairIT0_EE5valueENS1_6detail7op_infoILNS9_2opE17EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKS3_INSH_19first_argument_typeESL_ERKS3_INSH_20second_argument_typeESP_E(%"struct.std::pair"*, %"struct.std::pair"*, %"struct.std::pair"*)

declare i32 @_ZN11vfun_minmaxISt4pairIiiEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKiRS8_S9_(%"struct.std::pair"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_minmaxISt4pairIiiEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKiRS8_S9_(%"struct.std::pair"*, i32*, i32*, i32*)

declare void @_ZN3wrt9scalar_op6binaryL9minmax_opISt4pairIiiES4_LNS_13runtime_flagsE0EEENSt9enable_ifIXaasr7is_pairIT_EE5valuesr7is_pairIT0_EE5valueENS1_6detail7op_infoILNS9_2opE17EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKS3_INSH_19first_argument_typeESL_ERKS3_INSH_20second_argument_typeESP_E(%"struct.std::pair"*, %"struct.std::pair"*, %"struct.std::pair"*)

declare void @_ZL13vfun1_0_rtfunISt4pairIiiEiEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_d_minmax(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN11vfun_minmaxISt4pairIddEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKdRKiPSA_(%"struct.std::pair.4"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_minmaxISt4pairIddEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKdRKiPSA_(%"struct.std::pair.4"*, double*, i32*, i32*)

declare %"struct.std::pair.4" @_ZN3wrt9scalar_op6binaryL9minmax_opISt4pairIddES4_LNS_13runtime_flagsE1EEENSt9enable_ifIXaasr7is_pairIT_EE5valuesr7is_pairIT0_EE5valueENS1_6detail7op_infoILNS9_2opE17EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKS3_INSH_19first_argument_typeESL_ERKS3_INSH_20second_argument_typeESP_E(%"struct.std::pair.4"*, %"struct.std::pair.4"*)

declare i32 @_ZN11vfun_minmaxISt4pairIddEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKdRKiPSA_(%"struct.std::pair.4"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_minmaxISt4pairIddEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKdRKiPSA_(%"struct.std::pair.4"*, double*, i32*, i32*)

declare %"struct.std::pair.4" @_ZN3wrt9scalar_op6binaryL9minmax_opISt4pairIddES4_LNS_13runtime_flagsE0EEENSt9enable_ifIXaasr7is_pairIT_EE5valuesr7is_pairIT0_EE5valueENS1_6detail7op_infoILNS9_2opE17EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKS3_INSH_19first_argument_typeESL_ERKS3_INSH_20second_argument_typeESP_E(%"struct.std::pair.4"*, %"struct.std::pair.4"*)

declare void @_ZL13vfun1_0_rtfunISt4pairIddEdEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_i_maxabs(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN11vfun_maxabsIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @machfunc_d_maxabs(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN11vfun_maxabsIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @machfunc_c_maxabs(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL13vfun1_0_rtfunIdSt7complexIdEEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare double @cabs({ double, double })

declare i32 @machfunc_d_fpexception(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN16vfun_fpexceptionIidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN16vfun_fpexceptionIidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL9bit_or_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr18is_signed_integralIT_EE5valuesr18is_signed_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE4EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32*)

declare i32 @_ZN16vfun_fpexceptionIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN16vfun_fpexceptionIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL9bit_or_opIiiLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr18is_signed_integralIT_EE5valuesr18is_signed_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE4EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32*)

declare void @_ZL13vfun1_0_rtfunIidEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_c_fpexception(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN16vfun_fpexceptionIiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPiPKS1_RKiPSA_(i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN16vfun_fpexceptionIiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPiPKS1_RKiPSA_(i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN16vfun_fpexceptionIiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPiPKS1_RKiPSA_(i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN16vfun_fpexceptionIiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPiPKS1_RKiPSA_(i32*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL13vfun1_0_rtfunIiSt7complexIdEEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_d_sin(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_sin(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_cos(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_cos(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_tan(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_tan(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_d_cscPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_c_cscPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_d_secPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_c_secPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_d_cotPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_c_cotPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_sinh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_sinh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_cosh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_cosh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_tanh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_tanh(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_cschPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_c_cschPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_sechPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_c_sechPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_cothPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_c_cothPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_asin(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_asin(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_acos(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_acos(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_atan(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_atan(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_acscPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_c_acscPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_asecPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_c_asecPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_acotPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_c_acotPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_asinh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_asinh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_acosh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_acosh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_atanh(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_atanh(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_d_acschPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_c_acschPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_d_asechPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_c_asechPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_d_acothPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_c_acothPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_exp(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_exp(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_expm1(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_expm1(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_log(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_log(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_log1p(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_log1p(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_log2(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_log2(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_log2(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_log10(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_log10(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_log10(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_abs(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_abs(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_abs(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_i_argPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_d_argPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_arg(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_conj(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_conj(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_conj(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL13machfunc_i_imPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL13machfunc_d_imPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL13machfunc_c_imPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL13machfunc_i_rePvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL13machfunc_d_rePvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL13machfunc_c_rePvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_minus(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_minus(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_minus(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_i_signPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_signPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_c_signPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_i_posPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_d_posPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_i_negPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL14machfunc_d_negPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_i_nposPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_nposPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_i_nnegPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_nnegPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_i_roundPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_round(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_i_floorPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_floor(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_i_ceilingPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_ceiling(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL19machfunc_i_fracpartPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_fracpart(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_fracpart(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_i_intpartPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_intpart(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_i_evenqPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_d_evenqPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_i_oddqPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_oddqPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_square(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_square(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_square(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_sqrt(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_sqrt(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_cbrt(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_rsqrt(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_rsqrt(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_recip(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_recip(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL17machfunc_i_bitnotPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL20machfunc_i_bitlengthPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_intexp(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_intlen(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_sinc(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_sinc(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_fibonacci(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_fibonacci(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_fibonacci(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_lucasl(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_lucasl(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_lucasl(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_gudermannian(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_gudermannian(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_inversegudermannian(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_inversegudermannian(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_haversine(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_haversine(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_inversehaversine(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_inversehaversine(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_erfc(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_erf(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_gamma(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_gamma(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_loggamma(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_i_unitizePvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_d_unitizePvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_c_unitizePvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_i_mod1PvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_mod1PvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_c_mod1PvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_logistic(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_logistic(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_i_rampPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_d_rampPvPKviPKij(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_i_abssquare(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_d_abssquare(i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_c_abssquare(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN14vfun_abssquareIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun1_rtfunIdSt7complexIdEEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @feclearexcept(i32)

declare i32 @fetestexcept(i32)

declare i32 @_ZN14vfun_abssquareIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare void @_ZL11vfun1_rtfunIddEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN14vfun_abssquareIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_abssquareIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare void @_ZL11vfun1_rtfunIiiEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

; Function Attrs: nounwind readnone speculatable
declare { i32, i1 } @llvm.smul.with.overflow.i32(i32, i32) #0

declare i32 @_ZN9vfun_rampIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_rampIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_rampIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_rampIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_rampIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_rampIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_rampIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rampIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rampIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rampIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_logisticISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_logisticISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_logisticISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_logisticISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_logisticISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_logisticISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun1_rtfunISt7complexIdES1_EvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare { double, double } @cexp({ double, double })

declare { double, double } @__divdc3(double, double, double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL11logistic_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr28is_floating_point_or_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE50EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(%"struct.std::complex"*)

declare i32 @_ZN13vfun_logisticIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_logisticIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_logisticIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_logisticIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_logisticIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_logisticIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @exp(double)

declare i32 @_ZN9vfun_mod1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_mod1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_mod1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_mod1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_mod1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_mod1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_mod1IiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPiPKS1_RKiPSA_(i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPiPKS1_RKiPSA_(i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPiPKS1_RKiPSA_(i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPiPKS1_RKiPSA_(i32*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun1_rtfunIiSt7complexIdEEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN12vfun_unitizeIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare void @_ZL11vfun1_rtfunIidEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN12vfun_unitizeIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_loggammaIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_loggammaIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_loggammaIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_loggammaIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_loggammaIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_loggammaIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @lgamma(double)

declare i32 @_ZN10vfun_gammaIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_gammaIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_gammaIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_gammaIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_gammaIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_gammaIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @tgamma(double)

declare i32 @_ZN10vfun_gammaIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_gammaIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_gammaIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_gammaIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_erfIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_erfIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_erfIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_erfIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_erfIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_erfIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @erf(double)

declare i32 @_ZN9vfun_erfcIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_erfcIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_erfcIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_erfcIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_erfcIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_erfcIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @erfc(double)

declare i32 @_ZN21vfun_inversehaversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare double @sqrt(double)

declare double @hypot(double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8asinh_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE13EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare { double, double } @__muldc3(double, double, double, double)

declare double @frexp(double, i32*)

declare double @log(double)

declare { double, double } @clog({ double, double })

declare { double, double } @_ZN3wrt9scalar_op5unaryL8log1p_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE47EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare double @log1p(double)

declare double @atan2(double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL19inversehaversine_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE44EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare i32 @_ZN21vfun_inversehaversineIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN21vfun_inversehaversineIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @asin(double)

declare double @_ZN3wrt9scalar_op5unaryL19inversehaversine_opIdLNS_13runtime_flagsE1EEENSt9enable_ifIXsr17is_floating_pointIT_EE5valueENS1_6detail7op_infoILNS6_2opE44EN7rt_typeIS5_E4typeEE11result_typeEE4typeERKNSC_13argument_typeE(double)

declare i32 @_ZN14vfun_haversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_haversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_haversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_haversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_haversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_haversineISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @csin({ double, double })

declare { double, double } @_ZN3wrt9scalar_op5unaryL12haversine_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr28is_floating_point_or_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE40EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare i32 @_ZN14vfun_haversineIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_haversineIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_haversineIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_haversineIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_haversineIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_haversineIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @sin(double)

declare i32 @_ZN24vfun_inversegudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL22inversegudermannian_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE43EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare { double, double } @ctan({ double, double })

declare { double, double } @_ZN3wrt9scalar_op5unaryL22inversegudermannian_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE43EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare i32 @_ZN24vfun_inversegudermannianIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN24vfun_inversegudermannianIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @tan(double)

declare double @atanh(double)

declare i32 @_ZN17vfun_gudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8atanh_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE15EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(%"struct.std::complex"*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8rsqrt_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE64EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL15gudermannian_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE39EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare i32 @_ZN17vfun_gudermannianIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN17vfun_gudermannianIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @tanh(double)

declare double @atan(double)

declare i32 @_ZN11vfun_lucaslISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @cpow({ double, double }, { double, double })

declare { double, double } @ccos({ double, double })

declare i32 @_ZN11vfun_lucaslIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @pow(double, double)

declare double @cos(double)

declare i32 @_ZN11vfun_lucaslIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_lucaslIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL12fibonacci_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr28is_floating_point_or_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE34EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(%"struct.std::complex"*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL12fibonacci_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr28is_floating_point_or_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE34EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(%"struct.std::complex"*)

declare i32 @_ZN14vfun_fibonacciIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_fibonacciIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_sincISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sincISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sincISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sincISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sincISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sincISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sincIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sincIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sincIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sincIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sincIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sincIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #0

declare i32 @_ZN14vfun_bitlengthIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_bitlengthIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_bitlengthIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN14vfun_bitlengthIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bitnotIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bitnotIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bitnotIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bitnotIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_recipISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_recipISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_recipISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_recipISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_recipISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_recipISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_recipIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_recipIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_recipIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_recipIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_recipIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_recipIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8rsqrt_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE64EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare i32 @_ZN10vfun_rsqrtIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_rsqrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cbrtIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cbrtIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cbrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cbrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cbrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cbrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @cbrt(double)

declare i32 @_ZN9vfun_sqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sqrtIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_squareISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_squareISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_squareISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_squareISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_squareISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_squareISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_squareIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_squareIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_squareIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_squareIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_squareIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_squareIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_squareIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_squareIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_squareIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_squareIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_oddqIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_oddqIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_oddqIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_oddqIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_oddqIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_oddqIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_oddqIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_oddqIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_evenqIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_evenqIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_evenqIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_evenqIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_evenqIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_evenqIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_evenqIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_evenqIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_intpartIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN12vfun_intpartIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN12vfun_intpartIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN12vfun_intpartIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

; Function Attrs: nounwind readnone speculatable
declare double @llvm.trunc.f64(double) #0

declare i32 @_ZN12vfun_intpartIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_intpartIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_intpartIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_intpartIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_fracpartIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_ceilingIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN12vfun_ceilingIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN12vfun_ceilingIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN12vfun_ceilingIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

; Function Attrs: nounwind readnone speculatable
declare double @llvm.ceil.f64(double) #0

declare i32 @_ZN12vfun_ceilingIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_ceilingIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_ceilingIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_ceilingIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_floorIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_floorIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_floorIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_floorIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

; Function Attrs: nounwind readnone speculatable
declare double @llvm.floor.f64(double) #0

declare i32 @_ZN10vfun_floorIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_floorIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_floorIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_floorIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_roundIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_roundIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_roundIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_roundIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

; Function Attrs: nounwind readnone speculatable
declare double @llvm.nearbyint.f64(double) #0

declare i32 @_ZN10vfun_roundIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_roundIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_roundIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_roundIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_nnegIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_nnegIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_nnegIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_nnegIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_nnegIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_nnegIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_nnegIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_nnegIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_nposIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_nposIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_nposIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_nposIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_nposIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_nposIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_nposIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_nposIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_negIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_negIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_negIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_negIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_negIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_negIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_negIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_negIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_posIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_posIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_posIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_posIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_posIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_posIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_posIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_posIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_signISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_signISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_signISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_signISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_signISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_signISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_signIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_signIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_signIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_signIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_signIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_signIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_signIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_signIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_minusISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_minusISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_minusISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_minusISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_minusISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_minusISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_minusIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_minusIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_minusIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_minusIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_minusIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_minusIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_minusIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_minusIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_minusIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_minusIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN7vfun_reIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN7vfun_reIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN7vfun_reIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN7vfun_reIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN7vfun_reIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN7vfun_reIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN7vfun_reIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN7vfun_reIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN7vfun_reIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN7vfun_reIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN7vfun_reIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN7vfun_reIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN7vfun_reIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN7vfun_reIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN7vfun_imIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN7vfun_imIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN7vfun_imIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN7vfun_imIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN7vfun_imIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN7vfun_imIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN7vfun_imIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN7vfun_imIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN7vfun_imIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN7vfun_imIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN7vfun_imIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN7vfun_imIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_conjISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_conjISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_conjISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_conjISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_conjISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_conjISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_conjIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_conjIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_conjIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_conjIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_conjIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_conjIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_conjIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_conjIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_conjIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_conjIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_argIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_argIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_argIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_argIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare double @carg({ double, double })

declare i32 @_ZN8vfun_argIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_argIidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_argIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_argIidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdRKiPS8_(i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_argIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_argIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_argIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_argIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_absIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_absIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_absIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_absIdSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_RKiPSA_(double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_absIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_absIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_absIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_absIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_absIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_absIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_absIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_absIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_absIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_absIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_log10ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log10ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log10ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log10ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log10ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log10ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare double @log10(double)

declare i32 @_ZN10vfun_log10IddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log10IddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log10IddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log10IddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log10IddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log10IddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log10IiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_log10IiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_log10IiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_log10IiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare void @div(%"struct.std::pair"*, i32, i32)

declare i32 @_ZN9vfun_log2ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_log2ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_log2ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_log2ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_log2ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_log2ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare double @log2(double)

declare i32 @_ZN9vfun_log2IddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_log2IddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_log2IddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_log2IddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_log2IddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_log2IddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_log2IiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_log2IiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_log2IiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_log2IiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_log1pISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log1pISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log1pISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log1pISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log1pISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_log1pISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8log1p_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE47EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare i32 @_ZN10vfun_log1pIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log1pIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log1pIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log1pIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log1pIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_log1pIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_expm1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_expm1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_expm1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_expm1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_expm1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_expm1ISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare double @expm1(double)

declare i32 @_ZN10vfun_expm1IddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_expm1IddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_expm1IddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_expm1IddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_expm1IddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_expm1IddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_expISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_expISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_expISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_expISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_expISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_expISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_expIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_expIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_expIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_expIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_expIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_expIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acothIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acothIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acothIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acothIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acothIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acothIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @_ZN3wrt9scalar_op5unaryL8acoth_opIdLNS_13runtime_flagsE1EEENSt9enable_ifIXsr17is_floating_pointIT_EE5valueENS1_6detail7op_infoILNS6_2opE6EN7rt_typeIS5_E4typeEE11result_typeEE4typeERKNSC_13argument_typeE(double)

declare i32 @_ZN10vfun_asechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8acosh_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE4EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare i32 @_ZN10vfun_asechIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asechIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asechIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asechIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asechIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asechIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @acosh(double)

declare double @_ZN3wrt9scalar_op5unaryL8asech_opIdLNS_13runtime_flagsE1EEENSt9enable_ifIXsr17is_floating_pointIT_EE5valueENS1_6detail7op_infoILNS6_2opE11EN7rt_typeIS5_E4typeEE11result_typeEE4typeERKNSC_13argument_typeE(double)

declare i32 @_ZN10vfun_acschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acschIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acschIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acschIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acschIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acschIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acschIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @asinh(double)

declare i32 @_ZN10vfun_atanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8atanh_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE15EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(%"struct.std::complex"*)

declare i32 @_ZN10vfun_atanhIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atanhIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atanhIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atanhIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atanhIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atanhIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acoshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acoshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acoshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acoshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acoshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_acoshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8acosh_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE4EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare i32 @_ZN10vfun_acoshIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acoshIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acoshIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acoshIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acoshIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_acoshIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_asinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8asinh_opISt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE13EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(double, double)

declare i32 @_ZN10vfun_asinhIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asinhIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asinhIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asinhIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asinhIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_asinhIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acotIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acotIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acotIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acotIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acotIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acotIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asecISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asecISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asecISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asecISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asecISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asecISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asecIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asecIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asecIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asecIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asecIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asecIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @acos(double)

declare i32 @_ZN9vfun_acscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acscIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acscIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acscIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acscIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acscIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acscIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_atanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_atanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_atanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_atanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_atanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_atanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_atanIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_atanIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_atanIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_atanIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_atanIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_atanIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_acosIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acosIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acosIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acosIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acosIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_acosIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_asinIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asinIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asinIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asinIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asinIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_asinIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cothISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @ctanh({ double, double })

declare i32 @_ZN9vfun_cothIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cothIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cothIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cothIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cothIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cothIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sechISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @ccosh({ double, double })

declare i32 @_ZN9vfun_sechIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sechIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sechIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sechIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sechIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sechIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @cosh(double)

declare i32 @_ZN9vfun_cschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_cschISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @csinh({ double, double })

declare i32 @_ZN9vfun_cschIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cschIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cschIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cschIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cschIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_cschIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare double @sinh(double)

declare i32 @_ZN9vfun_tanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_tanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_tanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_tanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_tanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_tanhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_tanhIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_tanhIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_tanhIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_tanhIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_tanhIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_tanhIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_coshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_coshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_coshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_coshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_coshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_coshISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_coshIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_coshIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_coshIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_coshIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_coshIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_coshIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sinhISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_sinhIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sinhIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sinhIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sinhIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sinhIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_sinhIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cotISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cotIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cotIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cotIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cotIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cotIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cotIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_secISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_secISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_secISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_secISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_secISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_secISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_secIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_secIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_secIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_secIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_secIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_secIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cscISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cscIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cscIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cscIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cscIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cscIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cscIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_tanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_tanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_tanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_tanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_tanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_tanISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_tanIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_tanIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_tanIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_tanIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_tanIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_tanIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cosISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_cosIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cosIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cosIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cosIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cosIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_cosIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_sinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_sinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_sinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_sinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_sinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_sinISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_sinIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_sinIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_sinIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_sinIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_sinIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_sinIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare void @_GLOBAL__sub_I_machfuns1.cpp()

declare i32 @machfunc_i_copy(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_copyIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_copyIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_copyIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_copyIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @machfunc_d_copy(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_copyIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_copyIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_copyIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_copyIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_copyIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_copyIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @machfunc_c_copy(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_copyISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_copyISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_copyISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_copyISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_copyISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_copyISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @machfunc_i_zero(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_zeroIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_zeroIiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_zeroIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_zeroIiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiRS6_S7_(i32*, i32*, i32*, i32*)

declare i32 @machfunc_d_zero(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_zeroIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_zeroIddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_zeroIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_zeroIddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_zeroIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_zeroIddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdRKiPS8_(double*, double*, i32*, i32*)

declare i32 @machfunc_c_zero(i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_zeroISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_zeroISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_zeroISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_zeroISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_zeroISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_zeroISt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @ReturnRangeType1(i32, i32, i32*)

declare i32 @ReturnType1(i32, i32)

declare void @_GLOBAL__sub_I_machfuns2_0.cpp()

declare i32 @machfunc_ii_dot(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN8vfun_dotIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_dotIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_dotIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_dotIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL7plus_opIiiLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE19EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE.35(i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL7plus_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE19EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE.36(i32*, i32*)

declare void @_ZL13vfun2_0_rtfunIiiiEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_ii_plus(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_id_plus(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ic_plus(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_di_plusPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_plus(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dc_plus(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_ci_plusPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_cd_plusPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cc_plus(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_subtract(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_id_subtract(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ic_subtract(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_di_subtract(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_subtract(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dc_subtract(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ci_subtract(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cd_subtract(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cc_subtract(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_times(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_id_times(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ic_times(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_di_times(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_times(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dc_times(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ci_times(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cd_times(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cc_times(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_id_div(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ic_div(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_di_div(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_div(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dc_div(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ci_div(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cd_div(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cc_div(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_mod(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_id_modPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_ic_modPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_di_modPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_mod(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_dc_modPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_ci_modPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_cd_modPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cc_mod(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_quotient(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL20machfunc_id_quotientPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL20machfunc_di_quotientPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_quotient(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_pow(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_id_pow(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ic_pow(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_di_pow(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_pow(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dc_pow(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ci_pow(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cd_pow(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cc_pow(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_id_logPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_ic_logPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_di_logPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_dd_logPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_dc_logPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_ci_logPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_cd_logPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL15machfunc_cc_logPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL17machfunc_id_atan2PvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL17machfunc_ic_atan2PvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL17machfunc_di_atan2PvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL17machfunc_dd_atan2PvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL17machfunc_dc_atan2PvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL17machfunc_ci_atan2PvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL17machfunc_cd_atan2PvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL17machfunc_cc_atan2PvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_bitand(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_bitor(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_bitxor(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_dd_chopPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL16machfunc_cd_chopPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_abserr(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_id_abserrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_ic_abserrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_di_abserrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_abserr(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_dc_abserrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_ci_abserrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_cd_abserrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cc_abserr(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_id_relerrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_ic_relerrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_di_relerrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_relerr(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_dc_relerrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_ci_relerrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL18machfunc_cd_relerrPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cc_relerr(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_maxabs(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_dd_maxabs(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_cc_maxabs(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_intexp(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_intlen(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_bitshiftleft(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_di_bitshiftleft(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ci_bitshiftleft(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ii_bitshiftright(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_di_bitshiftright(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @machfunc_ci_bitshiftright(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL19machfunc_ii_unitizePvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL19machfunc_dd_unitizePvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZL19machfunc_cd_unitizePvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPiPKS1_PKdRKiPSC_(i32*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPiPKS1_PKdRKiPSC_(i32*, %"struct.std::complex"*, double*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIiSt7complexIdEdEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPiPKS1_PKdRKiPSC_(i32*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPiPKS1_PKdRKiPSC_(i32*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPiPKS1_PKdRKiPSC_(i32*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPiPKS1_PKdRKiPSC_(i32*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPiPKS1_PKdRKiPSC_(i32*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPiPKS1_PKdRKiPSC_(i32*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIiddEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN12vfun_unitizeIiddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIiiiEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN12vfun_unitizeIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_unitizeIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare void @_ZL11vfun2_rtfunISt7complexIdES1_iEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL17bit_shiftright_opISt7complexIdEiLNS_13runtime_flagsE1EEENSt9enable_ifIXaaoosr11is_integralIT_EE5valuesr28is_floating_point_or_complexIS7_EE5valuesr18is_signed_integralIT0_EE5valueENS1_6detail7op_infoILNS9_2opE6EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(%"struct.std::complex"*, i32)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIddiEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL17bit_shiftright_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaaoosr11is_integralIT_EE5valuesr28is_floating_point_or_complexIS5_EE5valuesr18is_signed_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE6EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32)

declare i32 @_ZN19vfun_bit_shiftrightIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN19vfun_bit_shiftrightIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN18vfun_bit_shiftleftIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intlenIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intlenIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intlenIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intlenIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intlenIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intlenIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intlenIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intlenIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intexpIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intexpIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL9intexp_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE11EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32*, i32*)

declare i32 @_ZN11vfun_intexpIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intexpIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intexpIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intexpIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intexpIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_intexpIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIdSt7complexIdES1_EvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIdddEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_maxabsIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIdSt7complexIdEdEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIdSt7complexIdEiEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIddSt7complexIdEEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIdiSt7complexIdEEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN11vfun_relerrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIdidEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_relerrIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdES1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKS1_S9_RKiPSA_(double*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEdE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKS1_PKdRKiPSC_(double*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdSt7complexIdEiE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKS1_PKiRSA_SB_(double*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKdPKS1_RKiPSC_(double*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdiSt7complexIdEE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPdPKiPKS1_RS8_S9_(double*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

; Function Attrs: nounwind readnone speculatable
declare { i32, i1 } @llvm.ssub.with.overflow.i32(i32, i32) #0

declare i32 @_ZN11vfun_abserrIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_abserrIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare void @_ZL11vfun2_rtfunISt7complexIdES1_dEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL7chop_opISt7complexIdEdLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr10is_complexIT_EE5valuesr17is_floating_pointIT0_EE5valueENS1_6detail7op_infoILNS9_2opE8EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(%"struct.std::complex"*, double)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_chopIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bit_orIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bit_orIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bit_orIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bit_orIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bit_orIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bit_orIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bit_orIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_bit_orIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_andIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_andIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_andIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_andIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_andIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_andIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_andIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN12vfun_bit_andIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun2_rtfunISt7complexIdES1_S1_EvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opISt7complexIdES4_LNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8rsqrt_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE64EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.43(double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opISt7complexIdES4_LNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double, double)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opISt7complexIdEdLNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opISt7complexIdEdLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opISt7complexIdEiLNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, i32)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opISt7complexIdEiLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, i32)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun2_rtfunISt7complexIdEdS1_EvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opIdSt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opIdSt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun2_rtfunISt7complexIdEiS1_EvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opIiSt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(i32, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL8atan2_opIiSt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE2EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(i32, double, double)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2ISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_atan2IdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opISt7complexIdES4_LNS_13runtime_flagsE0EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opISt7complexIdES4_LNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double, double)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opISt7complexIdEdLNS_13runtime_flagsE0EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opISt7complexIdEdLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opISt7complexIdEiLNS_13runtime_flagsE0EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, i32)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opISt7complexIdEiLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, i32)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opIdSt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opIdSt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL6log_opIddLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS7_2opE13EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double, double)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL6log_opIdiLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS7_2opE13EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double, i32)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opIiSt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(i32, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6log_opIiSt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE13EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(i32, double, double)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL6log_opIidLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr28is_floating_point_or_complexIT_EE5valuesr28is_floating_point_or_complexIT0_EE5valueENS1_6detail7op_infoILNS7_2opE13EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32, double)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_logIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opISt7complexIdEdLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr10is_complexIT_EE5valuesr17is_floating_pointIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opISt7complexIdEdLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr10is_complexIT_EE5valuesr17is_floating_pointIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opISt7complexIdEiLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr10is_complexIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(%"struct.std::complex"*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdiLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(double, i32)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdEiLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex"*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opISt7complexIdEiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr10is_complexIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(%"struct.std::complex"*, i32*)

declare double @_ZN3wrt9scalar_op6binaryL6pow_opIdiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double, i32)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opIdSt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXaaoosr11is_integralIT_EE5valuesr17is_floating_pointIS7_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opIdSt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXaaoosr11is_integralIT_EE5valuesr17is_floating_pointIS7_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opIiSt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXaaoosr11is_integralIT_EE5valuesr17is_floating_pointIS7_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(i32, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opIiSt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXaaoosr11is_integralIT_EE5valuesr17is_floating_pointIS7_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(i32, double, double)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_powIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL6pow_opIiiLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr18is_signed_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL6pow_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr18is_signed_integralIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(i32, i32*)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i32)

declare i32 @_ZN8vfun_powIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_powIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKdS7_RKiPS8_(i32*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIidiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKdPKiRS8_S9_(i32*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIidiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKdPKiRS8_S9_(i32*, double*, i32*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIidiEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN13vfun_quotientIidiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKdPKiRS8_S9_(i32*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIidiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKdPKiRS8_S9_(i32*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIidiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKdPKiRS8_S9_(i32*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIidiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKdPKiRS8_S9_(i32*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIidiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKdPKiRS8_S9_(i32*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIidiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKdPKiRS8_S9_(i32*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiPKdRS6_S7_(i32*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiPKdRS6_S7_(i32*, i32*, double*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIiidEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN13vfun_quotientIiidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiPKdRS6_S7_(i32*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiPKdRS6_S7_(i32*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiPKdRS6_S7_(i32*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiPKdRS6_S7_(i32*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiPKdRS6_S7_(i32*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiPKdRS6_S7_(i32*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_quotientIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opISt7complexIdES4_LNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double, double)

declare double @fmod(double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opISt7complexIdES4_LNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double, double)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opISt7complexIdEdLNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opISt7complexIdEdLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opISt7complexIdEiLNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, i32)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opISt7complexIdEiLNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, i32)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opIdSt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opIdSt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(double, double, double)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opIiSt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(i32, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6mod_opIiSt7complexIdELNS_13runtime_flagsE1EEENSt9enable_ifIXoosr10is_complexIT_EE5valuesr10is_complexIT0_EE5valueENS1_6detail7op_infoILNS9_2opE18EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(i32, double, double)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN8vfun_modIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN8vfun_modIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN11vfun_divideIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN10vfun_timesIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN10vfun_timesIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN13vfun_subtractIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_dE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKdRKiPSC_(%"struct.std::complex"*, %"struct.std::complex"*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEdS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKdPKS1_RKiPSC_(%"struct.std::complex"*, double*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdS7_RKiPS8_(double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIddiE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKdPKiRS8_S9_(double*, double*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusISt7complexIdEiS1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKiPKS1_RS8_S9_(%"struct.std::complex"*, i32*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class2EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIdidE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class3EEEiPdPKiPKdRS6_S7_(double*, i32*, double*, i32*, i32*)

declare i32 @_ZN9vfun_plusIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_plusIiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPiPKiS7_RS6_S7_(i32*, i32*, i32*, i32*, i32*)

declare void @_GLOBAL__sub_I_machfuns2.cpp()

declare i32 @machfunc_bb_bitxor(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN12vfun_bit_xorIhhhE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPhPKhS7_RKiPS8_(i8*, i8*, i8*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIhhhE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPhPKhS7_RKiPS8_(i8*, i8*, i8*, i32*, i32*)

declare void @_ZL11vfun2_rtfunIhhhEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN12vfun_bit_xorIhhhE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPhPKhS7_RKiPS8_(i8*, i8*, i8*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIhhhE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class2EEEiPhPKhS7_RKiPS8_(i8*, i8*, i8*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIhhhE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class3EEEiPhPKhS7_RKiPS8_(i8*, i8*, i8*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIhhhE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPhPKhS7_RKiPS8_(i8*, i8*, i8*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIhhhE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class2EEEiPhPKhS7_RKiPS8_(i8*, i8*, i8*, i32*, i32*)

declare i32 @_ZN12vfun_bit_xorIhhhE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class3EEEiPhPKhS7_RKiPS8_(i8*, i8*, i8*, i32*, i32*)

declare i32 @_Z16machfunc_ci_rootPvPKvS1_iPKij(i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare { double, double } @_ZN3wrt9scalar_op6binaryL7root_opISt7complexIdEiLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr10is_complexIT_EE5valuesr18is_signed_integralIT0_EE5valueENS1_6detail7op_infoILNS9_2opE22EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(%"struct.std::complex"*, i32)

declare { double, double } @_ZN3wrt9scalar_op6binaryL7root_opISt7complexIdEiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr10is_complexIT_EE5valuesr18is_signed_integralIT0_EE5valueENS1_6detail7op_infoILNS9_2opE22EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE(%"struct.std::complex"*, i32)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class2EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_rootISt7complexIdES1_iE7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class3EEEiPS1_PKS1_PKiRSA_SB_(%"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*, i32*)

declare i32 @ReturnRangeType2(i32, i32, i32)

declare i32 @ReturnType2(i32, i32, i32)

declare void @_GLOBAL__sub_I_machfuns3.cpp()

declare i32 @machfunc_iii_axpy(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_axpyIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_axpyIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_axpyIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_axpyIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare void @_ZL11vfun3_rtfunIiiiiEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @_ZN3wrt9scalar_op7ternaryL7axpy_opIiiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaaaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valuesr11is_integralIT1_EE5valueENS1_6detail7op_infoILNS8_2opE3EN7rt_typeIS5_E4typeENSB_IS6_E4typeENSB_IS7_E4typeEE11result_typeEE4typeERKNSI_19first_argument_typeERKNSI_20second_argument_typeERKNSI_19third_argument_typeE(i32, i32, i32*)

declare i32 @machfunc_ddd_axpy(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_axpyIddddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axpyIddddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axpyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axpyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axpyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axpyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare void @_ZL11vfun3_rtfunIddddEvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_ccc_axpy(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_axpyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axpyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axpyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axpyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axpyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axpyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare void @_ZL11vfun3_rtfunISt7complexIdES1_S1_S1_EvPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_iii_axmy(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_axmyIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_axmyIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_axmyIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_axmyIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @machfunc_ddd_axmy(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_axmyIddddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axmyIddddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axmyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axmyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axmyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_axmyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @machfunc_ccc_axmy(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_axmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_axmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @machfunc_iii_ymax(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_ymaxIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxIiiiiE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPiPKiS7_S7_RS6_S7_(i32*, i32*, i32*, i32*, i32*, i32*)

declare i32 @_ZN3wrt9scalar_op7ternaryL7ymax_opIiiiLNS_13runtime_flagsE1EEENSt9enable_ifIXaaaasr11is_integralIT_EE5valuesr11is_integralIT0_EE5valuesr11is_integralIT1_EE5valueENS1_6detail7op_infoILNS8_2opE5EN7rt_typeIS5_E4typeENSB_IS6_E4typeENSB_IS7_E4typeEE11result_typeEE4typeERKNSI_19first_argument_typeERKNSI_20second_argument_typeERKNSI_19third_argument_typeE(i32, i32, i32*)

declare i32 @machfunc_ddd_ymax(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_ymaxIddddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxIddddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxIddddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxIddddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxIddddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxIddddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @machfunc_ccc_ymax(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN9vfun_ymaxISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN9vfun_ymaxISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @machfunc_ddd_adxmy(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN10vfun_adxmyIddddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyIddddE7iterateILN3wrt13runtime_flagsE1ELS3_0EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_1EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class1EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyIddddE7iterateILN3wrt13runtime_flagsE0ELS3_0EL15increment_class0EEEiPdPKdS7_S7_RKiPS8_(double*, double*, double*, double*, i32*, i32*)

declare i32 @machfunc_ccc_adxmy(i8*, i8*, i8*, i8*, i32, i32*, i32)

declare i32 @_ZN10vfun_adxmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE1ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_1EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class1EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @_ZN10vfun_adxmyISt7complexIdES1_S1_S1_E7iterateILN3wrt13runtime_flagsE0ELS5_0EL15increment_class0EEEiPS1_PKS1_S9_S9_RKiPSA_(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32*, i32*)

declare i32 @machfunc_ddd_compplus(double*, double*, double*, double*, i32, i32*, i32)

declare i32 @machfunc_ccc_compplus(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32*, i32)

declare void @_GLOBAL__sub_I_machnorms.cpp()

declare i32 @machfunc_d_vectornorm(double*, i32, double*, i32, i32, i32)

declare double @dasum_(i32*, double*, i32*)

declare double @dnrm2_(i32*, double*, i32*)

declare i32 @idamax_(i32*, double*, i32*)

declare i32 @machfunc_c_vectornorm(double*, i32, %"struct.std::complex"*, i32, i32, i32)

declare double @dznrm2_(i32*, %"struct.std::pair.4"*, i32*)

declare i32 @machfunc_d_weightedvectornorm(double*, i32, double*, double*, i32, i32, i32)

declare i32 @machfunc_c_weightedvectornorm(double*, i32, %"struct.std::complex"*, double*, i32, i32, i32)

declare i32 @machfunc_d_vectornormweights(double*, double, double, double*, i32, i32)

declare i32 @machfunc_c_vectornormweights(double*, double, double, %"struct.std::complex"*, i32, i32)

declare i32 @machfunc_d_scaledvectornorm(double*, i32, double, double, double*, double*, i32, i32, i32)

declare i32 @machfunc_c_scaledvectornorm(double*, i32, double, double, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32, i32)

declare void @_GLOBAL__sub_I_parallel_params.cpp()

declare i32 @getProcessorCount()

declare i32 @getHyperThreadCount()

declare i32 @getParallelThreadNumber()

declare i32 @setParallelThreadNumber(i32)

declare void @restoreParallelThreadNumber(i32)

declare void @_Z20init_parallel_paramsi(i32)

declare i32 @ParallelThreadsEnvironment_initializedQ()

declare %struct.st_ParallelThreadsSchedule* @New_ParallelThreadsSchedule(i32, i32)

declare %struct.st_ParallelThreadsSchedule* @New_ParallelThreadsScheduleSingle()

declare void @Delete_ParallelThreadsSchedule(%struct.st_ParallelThreadsSchedule*)

declare void @InitializeParallelThreads()

declare %struct.st_ParallelThreadsEnvironment* @_ZL30New_ParallelThreadsEnvironmenti(i32)

declare void @_ZL17runParallelThreadPv(i8*)

declare void @DeinitializeParallelThreads()

declare void @ParallelThreads_addStartFunction(void (%struct.st_ParallelThread*)*)

declare i32 @ParallelThreads_For(i8*, void (i8*, %struct.st_ParallelThreadsEnvironment*)*, void (i8*, %struct.st_ParallelThread*)*, i32, i32, i32, %struct.st_ParallelThreadsSchedule*)

declare i32 @ParallelThreads_Reduce(i8*, void (i8*, %struct.st_ParallelThreadsEnvironment*)*, void (i8*, %struct.st_ParallelThread*)*, i32, i32, i32)

declare i32 @ParallelThreads_Iterate(i8*, void (i8*, %struct.st_ParallelThread*)*, i32)

declare i32 @ParallelThread_RangeStart(%struct.st_ParallelThread*)

declare i32 @ParallelThread_RangeEnd(%struct.st_ParallelThread*)

declare i32 @ParallelThread_RangeStep(%struct.st_ParallelThread*)

declare i32 @ParallelThread_ID(%struct.st_ParallelThread*)

declare void @ParallelThread_synchronizeLock(%struct.st_ParallelThread*)

declare void @ParallelThread_synchronizeUnlock(%struct.st_ParallelThread*)

declare void @ParallelThread_cancel(%struct.st_ParallelThread*, i32)

declare i32 @ParallelThread_canceledQ(%struct.st_ParallelThread*)

declare i32 @ParallelThreadsEnvironment_numThreads(%struct.st_ParallelThreadsEnvironment*)

declare i32* @ParallelThreadID(i32*)

declare void @_Z21init_parallel_threadsi(i32)

declare void @_GLOBAL__sub_I_s_machfuns1.cpp()

declare i32 @s_machfunc_i_abs(i32*, i32, i32)

declare i32 @s_machfunc_d_abs(double*, double, i32)

declare i32 @s_machfunc_c_abs(double*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_conj(i32*, i32, i32)

declare i32 @s_machfunc_d_conj(double*, double, i32)

declare i32 @s_machfunc_c_conj(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_c_arg(double*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_abssquare(i32*, i32, i32)

declare i32 @s_machfunc_d_abssquare(double*, double, i32)

declare i32 @s_machfunc_c_abssquare(double*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_exp(double*, double, i32)

declare i32 @s_machfunc_c_exp(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_expm1(double*, double, i32)

declare i32 @s_machfunc_c_expm1(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_log(double*, double, i32)

declare i32 @s_machfunc_c_log(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_log1p(double*, double, i32)

declare i32 @s_machfunc_c_log1p(%"struct.std::complex"*, %"struct.std::complex", i32)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8log1p_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE47EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.78(double, double)

declare i32 @s_machfunc_i_log2(i32*, i32, i32)

declare i32 @s_machfunc_d_log2(double*, double, i32)

declare i32 @s_machfunc_c_log2(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_log10(i32*, i32, i32)

declare i32 @s_machfunc_d_log10(double*, double, i32)

declare i32 @s_machfunc_c_log10(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_zero(i32*, i32, i32)

declare i32 @s_machfunc_d_zero(double*, double, i32)

declare i32 @s_machfunc_c_zero(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_copy(i32*, i32, i32)

declare i32 @s_machfunc_d_copy(double*, double, i32)

declare i32 @s_machfunc_c_copy(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_minus(i32*, i32, i32)

declare i32 @s_machfunc_d_minus(double*, double, i32)

declare i32 @s_machfunc_c_minus(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_re(i32*, i32, i32)

declare i32 @s_machfunc_d_re(double*, double, i32)

declare i32 @s_machfunc_c_re(double*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_im(i32*, i32, i32)

declare i32 @s_machfunc_d_im(i32*, double, i32)

declare i32 @s_machfunc_c_im(double*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_recip(double*, double, i32)

declare i32 @s_machfunc_c_recip(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_sqrt(double*, double, i32)

declare i32 @s_machfunc_c_sqrt(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_cbrt(double*, double, i32)

declare i32 @s_machfunc_d_rsqrt(double*, double, i32)

declare i32 @s_machfunc_c_rsqrt(%"struct.std::complex"*, %"struct.std::complex", i32)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8rsqrt_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE64EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.79(double, double)

declare i32 @s_machfunc_i_square(i32*, i32, i32)

declare i32 @s_machfunc_d_square(double*, double, i32)

declare i32 @s_machfunc_c_square(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_round(i32*, i32, i32)

declare i32 @s_machfunc_d_round(i32*, double, i32)

declare i32 @s_machfunc_i_floor(i32*, i32, i32)

declare i32 @s_machfunc_d_floor(i32*, double, i32)

declare i32 @s_machfunc_i_ceiling(i32*, i32, i32)

declare i32 @s_machfunc_d_ceiling(i32*, double, i32)

declare i32 @s_machfunc_i_mod1(i32*, i32, i32)

declare i32 @s_machfunc_d_mod1(double*, double, i32)

declare i32 @s_machfunc_c_mod1(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_fracpart(i32*, i32, i32)

declare i32 @s_machfunc_d_fracpart(double*, double, i32)

declare i32 @s_machfunc_c_fracpart(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_intpart(i32*, i32, i32)

declare i32 @s_machfunc_d_intpart(i32*, double, i32)

declare i32 @s_machfunc_i_sign(i32*, i32, i32)

declare i32 @s_machfunc_d_sign(i32*, double, i32)

declare i32 @s_machfunc_c_sign(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_sin(double*, double, i32)

declare i32 @s_machfunc_c_sin(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_sinh(double*, double, i32)

declare i32 @s_machfunc_c_sinh(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_cos(double*, double, i32)

declare i32 @s_machfunc_c_cos(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_cosh(double*, double, i32)

declare i32 @s_machfunc_c_cosh(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_tan(double*, double, i32)

declare i32 @s_machfunc_c_tan(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_tanh(double*, double, i32)

declare i32 @s_machfunc_c_tanh(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_csc(double*, double, i32)

declare i32 @s_machfunc_c_csc(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_csch(double*, double, i32)

declare i32 @s_machfunc_c_csch(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_sec(double*, double, i32)

declare i32 @s_machfunc_c_sec(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_sech(double*, double, i32)

declare i32 @s_machfunc_c_sech(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_cot(double*, double, i32)

declare i32 @s_machfunc_c_cot(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_coth(double*, double, i32)

declare i32 @s_machfunc_c_coth(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_asin(double*, double, i32)

declare i32 @s_machfunc_c_asin(%"struct.std::complex"*, %"struct.std::complex", i32)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8asinh_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE13EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.80(double, double)

declare i32 @s_machfunc_d_asinh(double*, double, i32)

declare i32 @s_machfunc_c_asinh(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_acos(double*, double, i32)

declare i32 @s_machfunc_c_acos(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_acosh(double*, double, i32)

declare i32 @s_machfunc_c_acosh(%"struct.std::complex"*, %"struct.std::complex", i32)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8acosh_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE4EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.81(double, double)

declare i32 @s_machfunc_d_atan(double*, double, i32)

declare i32 @s_machfunc_c_atan(%"struct.std::complex"*, %"struct.std::complex", i32)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8atanh_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE15EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.82(%"struct.std::complex"*)

declare i32 @s_machfunc_d_atanh(double*, double, i32)

declare i32 @s_machfunc_c_atanh(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_acsc(double*, double, i32)

declare i32 @s_machfunc_c_acsc(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_acsch(double*, double, i32)

declare i32 @s_machfunc_c_acsch(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_asec(double*, double, i32)

declare i32 @s_machfunc_c_asec(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_asech(double*, double, i32)

declare i32 @s_machfunc_c_asech(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_acot(double*, double, i32)

declare i32 @s_machfunc_c_acot(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_acoth(double*, double, i32)

declare i32 @s_machfunc_c_acoth(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_pos(i32*, i32, i32)

declare i32 @s_machfunc_d_pos(i32*, double, i32)

declare i32 @s_machfunc_i_neg(i32*, i32, i32)

declare i32 @s_machfunc_d_neg(i32*, double, i32)

declare i32 @s_machfunc_i_npos(i32*, i32, i32)

declare i32 @s_machfunc_d_npos(i32*, double, i32)

declare i32 @s_machfunc_i_nneg(i32*, i32, i32)

declare i32 @s_machfunc_d_nneg(i32*, double, i32)

declare i32 @s_machfunc_i_evenq(i32*, i32, i32)

declare i32 @s_machfunc_i_oddq(i32*, i32, i32)

declare i32 @s_machfunc_i_bitlength(i32*, i32, i32)

declare i32 @s_machfunc_i_bitnot(i32*, i32, i32)

declare i32 @s_machfunc_i_ramp(i32*, i32, i32)

declare i32 @s_machfunc_d_ramp(double*, double, i32)

declare i32 @s_machfunc_i_unitize(i32*, i32, i32)

declare i32 @s_machfunc_d_unitize(i32*, double, i32)

declare i32 @s_machfunc_c_unitize(i32*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_sinc(double*, double, i32)

declare i32 @s_machfunc_c_sinc(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_fibonacci(i32*, i32, i32)

declare i32 @s_machfunc_d_fibonacci(double*, double, i32)

declare i32 @s_machfunc_c_fibonacci(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_i_lucasl(i32*, i32, i32)

declare i32 @s_machfunc_d_lucasl(double*, double, i32)

declare i32 @s_machfunc_c_lucasl(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_erf(double*, double, i32)

declare i32 @s_machfunc_d_erfc(double*, double, i32)

declare i32 @s_machfunc_i_gamma(i32*, i32, i32)

declare i32 @s_machfunc_d_gamma(double*, double, i32)

declare i32 @s_machfunc_d_loggamma(double*, double, i32)

declare i32 @s_machfunc_d_gudermannian(double*, double, i32)

declare i32 @s_machfunc_c_gudermannian(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_inversegudermannian(double*, double, i32)

declare i32 @s_machfunc_c_inversegudermannian(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_haversine(double*, double, i32)

declare i32 @s_machfunc_c_haversine(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_inversehaversine(double*, double, i32)

declare i32 @s_machfunc_c_inversehaversine(%"struct.std::complex"*, %"struct.std::complex", i32)

declare i32 @s_machfunc_d_logistic(double*, double, i32)

declare i32 @s_machfunc_c_logistic(%"struct.std::complex"*, %"struct.std::complex", i32)

declare void @_GLOBAL__sub_I_s_machfuns2.cpp()

declare i32 @s_machfunc_ii_plus(i32*, i32, i32, i32)

declare i32 @s_machfunc_id_plus(double*, i32, double, i32)

declare i32 @s_machfunc_di_plus(double*, double, i32, i32)

declare i32 @s_machfunc_dd_plus(double*, double, double, i32)

declare i32 @s_machfunc_ic_plus(%"struct.std::complex"*, i32, %"struct.std::complex", i32)

declare i32 @s_machfunc_ci_plus(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_dc_plus(%"struct.std::complex"*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cd_plus(%"struct.std::complex"*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_cc_plus(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_ii_subtract(i32*, i32, i32, i32)

declare i32 @s_machfunc_id_subtract(double*, i32, double, i32)

declare i32 @s_machfunc_di_subtract(double*, double, i32, i32)

declare i32 @s_machfunc_dd_subtract(double*, double, double, i32)

declare i32 @s_machfunc_ic_subtract(%"struct.std::complex"*, i32, %"struct.std::complex", i32)

declare i32 @s_machfunc_ci_subtract(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_dc_subtract(%"struct.std::complex"*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cd_subtract(%"struct.std::complex"*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_cc_subtract(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_ii_times(i32*, i32, i32, i32)

declare i32 @s_machfunc_id_times(double*, i32, double, i32)

declare i32 @s_machfunc_di_times(double*, double, i32, i32)

declare i32 @s_machfunc_dd_times(double*, double, double, i32)

declare i32 @s_machfunc_ic_times(%"struct.std::complex"*, i32, %"struct.std::complex", i32)

declare i32 @s_machfunc_ci_times(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_dc_times(%"struct.std::complex"*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cd_times(%"struct.std::complex"*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_cc_times(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_id_div(double*, i32, double, i32)

declare i32 @s_machfunc_di_div(double*, double, i32, i32)

declare i32 @s_machfunc_dd_div(double*, double, double, i32)

declare i32 @s_machfunc_ic_div(%"struct.std::complex"*, i32, %"struct.std::complex", i32)

declare i32 @s_machfunc_ci_div(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_dc_div(%"struct.std::complex"*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cd_div(%"struct.std::complex"*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_cc_div(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_ii_abserr(i32*, i32, i32, i32)

declare i32 @s_machfunc_id_abserr(double*, i32, double, i32)

declare i32 @s_machfunc_di_abserr(double*, double, i32, i32)

declare i32 @s_machfunc_dd_abserr(double*, double, double, i32)

declare i32 @s_machfunc_ic_abserr(double*, i32, %"struct.std::complex", i32)

declare i32 @s_machfunc_ci_abserr(double*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_dc_abserr(double*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cd_abserr(double*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_cc_abserr(double*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_ii_maxabs(i32*, i32, i32, i32)

declare i32 @s_machfunc_dd_maxabs(double*, double, double, i32)

declare i32 @s_machfunc_cc_maxabs(double*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_id_relerr(double*, i32, double, i32)

declare i32 @s_machfunc_di_relerr(double*, double, i32, i32)

declare i32 @s_machfunc_dd_relerr(double*, double, double, i32)

declare i32 @s_machfunc_ic_relerr(double*, i32, %"struct.std::complex", i32)

declare i32 @s_machfunc_ci_relerr(double*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_dc_relerr(double*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cd_relerr(double*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_cc_relerr(double*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_id_log(double*, i32, double, i32)

declare i32 @s_machfunc_di_log(double*, double, i32, i32)

declare i32 @s_machfunc_dd_log(double*, double, double, i32)

declare i32 @s_machfunc_ic_log(%"struct.std::complex"*, i32, %"struct.std::complex", i32)

declare i32 @s_machfunc_ci_log(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_dc_log(%"struct.std::complex"*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cd_log(%"struct.std::complex"*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_cc_log(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_id_atan2(double*, i32, double, i32)

declare i32 @s_machfunc_di_atan2(double*, double, i32, i32)

declare i32 @s_machfunc_dd_atan2(double*, double, double, i32)

declare i32 @s_machfunc_ic_atan2(%"struct.std::complex"*, i32, %"struct.std::complex", i32)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8rsqrt_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE64EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.85(double, double)

declare i32 @s_machfunc_ci_atan2(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_dc_atan2(%"struct.std::complex"*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cd_atan2(%"struct.std::complex"*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_cc_atan2(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_ii_mod(i32*, i32, i32, i32)

declare i32 @s_machfunc_id_mod(double*, i32, double, i32)

declare i32 @s_machfunc_di_mod(double*, double, i32, i32)

declare i32 @s_machfunc_dd_mod(double*, double, double, i32)

declare i32 @s_machfunc_ic_mod(%"struct.std::complex"*, i32, %"struct.std::complex", i32)

declare i32 @s_machfunc_ci_mod(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_dc_mod(%"struct.std::complex"*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cd_mod(%"struct.std::complex"*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_cc_mod(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_ii_pow(i32*, i32, i32, i32)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_.86(i32, i32)

declare i32 @s_machfunc_di_pow(double*, double, i32, i32)

declare double @_ZN3wrt9scalar_op6binaryL6pow_opIdiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE.89(double, i32)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdiLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_.90(double, i32)

declare i32 @s_machfunc_ci_pow(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdEiLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_.91(%"struct.std::complex"*, i32*)

declare i32 @s_machfunc_id_pow(double*, i32, double, i32)

declare i32 @s_machfunc_dd_pow(double*, double, double, i32)

declare i32 @s_machfunc_cd_pow(%"struct.std::complex"*, %"struct.std::complex", double, i32)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opISt7complexIdEdLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr10is_complexIT_EE5valuesr17is_floating_pointIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE.92(double, double, double)

declare { double, double } @_ZN3wrt9scalar_op6binaryL6pow_opISt7complexIdEdLNS_13runtime_flagsE0EEENSt9enable_ifIXaasr10is_complexIT_EE5valuesr17is_floating_pointIT0_EE5valueENS1_6detail7op_infoILNS9_2opE20EN7rt_typeIS7_E4typeENSC_IS8_E4typeEE11result_typeEE4typeERKNSH_19first_argument_typeERKNSH_20second_argument_typeE.93(double, double, double)

declare i32 @s_machfunc_ic_pow(%"struct.std::complex"*, i32, %"struct.std::complex", i32)

declare i32 @s_machfunc_dc_pow(%"struct.std::complex"*, double, %"struct.std::complex", i32)

declare i32 @s_machfunc_cc_pow(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_ii_quotient(i32*, i32, i32, i32)

declare i32 @s_machfunc_di_quotient(i32*, double, i32, i32)

declare i32 @s_machfunc_id_quotient(i32*, i32, double, i32)

declare i32 @s_machfunc_dd_quotient(i32*, double, double, i32)

declare i32 @s_machfunc_ci_root(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_ii_bitand(i32*, i32, i32, i32)

declare i32 @s_machfunc_ii_bitor(i32*, i32, i32, i32)

declare i32 @s_machfunc_ii_bitxor(i32*, i32, i32, i32)

declare i32 @s_machfunc_ii_bitshiftleft(i32*, i32, i32, i32)

declare i32 @s_machfunc_di_bitshiftleft(double*, double, i32, i32)

declare i32 @s_machfunc_ci_bitshiftleft(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_ii_bitshiftright(i32*, i32, i32, i32)

declare i32 @s_machfunc_di_bitshiftright(double*, double, i32, i32)

declare i32 @s_machfunc_ci_bitshiftright(%"struct.std::complex"*, %"struct.std::complex", i32, i32)

declare i32 @s_machfunc_ii_intexp(i32*, i32, i32, i32)

declare i32 @s_machfunc_ii_intlen(i32*, i32, i32, i32)

declare i32 @s_machfunc_dd_chop(double*, double, double, i32)

declare i32 @s_machfunc_cd_chop(%"struct.std::complex"*, %"struct.std::complex", double, i32)

declare i32 @s_machfunc_ii_unitize(i32*, i32, i32, i32)

declare i32 @s_machfunc_dd_unitize(i32*, double, double, i32)

declare i32 @s_machfunc_cd_unitize(i32*, %"struct.std::complex", double, i32)

declare void @_GLOBAL__sub_I_s_machfuns3.cpp()

declare i32 @s_machfunc_iii_axpy(i32*, i32, i32, i32, i32)

declare i32 @s_machfunc_ddd_axpy(double*, double, double, double, i32)

declare i32 @s_machfunc_ccc_axpy(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_iii_axmy(i32*, i32, i32, i32, i32)

declare i32 @s_machfunc_ddd_axmy(double*, double, double, double, i32)

declare i32 @s_machfunc_ccc_axmy(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_iii_ymax(i32*, i32, i32, i32, i32)

declare i32 @s_machfunc_ddd_ymax(double*, double, double, double, i32)

declare i32 @s_machfunc_ccc_ymax(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_ddd_adxmy(double*, double, double, double, i32)

declare i32 @s_machfunc_ccc_adxmy(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", %"struct.std::complex", i32)

declare i32 @s_machfunc_ddd_compplus(double*, double, double, double*, i32)

declare i32 @s_machfunc_ccc_compplus(%"struct.std::complex"*, %"struct.std::complex", %"struct.std::complex", %"struct.std::complex"*, i32)

declare void @_GLOBAL__sub_I_vector_parallel_params.cpp()

declare i32 @getVectorParallelLengthThreshold(i32)

declare void @setVectorParallelLengthThreshold(i32, i32)

declare void @_Z27init_vector_parallel_paramsi(i32)

declare void @_GLOBAL__sub_I_vector_vendor_params.cpp()

declare i32 @getVectorVendorLengthThreshold(i32)

declare void @setVectorVendorLengthThreshold(i32, i32)

declare void @_Z25init_vector_vendor_paramsi(i32)

declare i32 @MTensor_getsetMTensor(%struct.st_MDataArray**, %struct.st_MDataArray*, i32*, i32, i32)

declare i32 @memcmp(i8*, i8*, i32)

declare i32 @initializeWolframDLLFunctions(%struct.st_WolframLibraryData*, i32)

declare void @_ZL17UTF8String_disownPc(i8*)

declare i32 @_ZL17MTensor_newFunDLLiiPKiPP13st_MDataArray(i32, i32, i32*, %struct.st_MDataArray**)

declare void @_ZL12MTensor_freeP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL13MTensor_cloneP13st_MDataArrayPS0_(%struct.st_MDataArray*, %struct.st_MDataArray**)

declare i32 @_ZL18MTensor_shareCountP13st_MDataArray(%struct.st_MDataArray*)

declare void @_ZL14MTensor_disownP13st_MDataArray(%struct.st_MDataArray*)

declare void @_ZL17MTensor_disownAllP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL18MTensor_setIntegerP13st_MDataArrayPii(%struct.st_MDataArray*, i32*, i32)

declare i32 @_ZL15MTensor_setRealP13st_MDataArrayPid(%struct.st_MDataArray*, i32*, double)

declare i32 @_ZL18MTensor_setComplexP13st_MDataArrayPiSt7complexIdE(%struct.st_MDataArray*, i32*, %"struct.std::complex")

declare i32 @_ZL18MTensor_setMTensorP13st_MDataArrayS0_Pii(%struct.st_MDataArray*, %struct.st_MDataArray*, i32*, i32)

declare i32 @_ZL18MTensor_getIntegerP13st_MDataArrayPiS1_(%struct.st_MDataArray*, i32*, i32*)

declare i32 @_ZL15MTensor_getRealP13st_MDataArrayPiPd(%struct.st_MDataArray*, i32*, double*)

declare i32 @_ZL18MTensor_getComplexP13st_MDataArrayPiPSt7complexIdE(%struct.st_MDataArray*, i32*, %"struct.std::complex"*)

declare i32 @_ZL18MTensor_getMTensorP13st_MDataArrayPiiPS0_(%struct.st_MDataArray*, i32*, i32, %struct.st_MDataArray**)

declare i32 @_ZL15MTensor_GetRankP13st_MDataArray(%struct.st_MDataArray*)

declare i32* @_ZL21MTensor_GetDimensionsP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL15MTensor_GetTypeP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL27MTensor_GetNumberOfElementsP13st_MDataArray(%struct.st_MDataArray*)

declare i32* @_ZL22MTensor_GetIntegerDataP13st_MDataArray(%struct.st_MDataArray*)

declare double* @_ZL19MTensor_GetRealDataP13st_MDataArray(%struct.st_MDataArray*)

declare %"struct.std::complex"* @_ZL22MTensor_GetComplexDataP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL6AbortQv()

declare %struct.st_WolframCompileLibrary_Functions* @getWolframCompileDLLFunctions(i32)

declare void @_ZL29WF_WolframLibraryData_cleanUpP21st_WolframLibraryDatai(%struct.st_WolframLibraryData*, i32)

declare i32 @_ZL19WF_MTensor_allocatePP13st_MDataArrayiiPi(%struct.st_MDataArray**, i32, i32, i32*)

declare i32 @_ZL15WF_MTensor_copyPP13st_MDataArrayS0_(%struct.st_MDataArray**, %struct.st_MDataArray*)

declare i32 @_ZL21WF_MTensor_copyUniquePP13st_MDataArrayS0_(%struct.st_MDataArray**, %struct.st_MDataArray*)

declare i32 @_ZL29MTensor_getMTensorInitializedPP13st_MDataArrayS0_Pii(%struct.st_MDataArray**, %struct.st_MDataArray*, i32*, i32)

declare i32 @_ZL18WF_MTensor_getPartPP13st_MDataArrayS0_iPiPPv(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i8**)

declare i32 @_ZL18WF_MTensor_setPartPP13st_MDataArrayS0_iPiPPv(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i8**)

declare i32 (i8*, i8*, i32, i32*, i32)* @_ZL23WF_getUnaryMathFunctionii(i32, i32)

declare i32 (i8*, i8*, i8*, i32, i32*, i32)* @_ZL24WF_getBinaryMathFunctioniii(i32, i32, i32)

declare i32 @_ZL21CompareComplexes_WLV1idiPSt7complexIdE(i32, double, i32, %"struct.std::complex"*)

declare %struct.st_MDataArray* @_ZL19LoadRankZeroMTensorPvii(i8*, i32, i32)

declare i32 @_ZL26WF_MTensor_fillFromMTensorP13st_MDataArrayPS0_(%struct.st_MDataArray*, %struct.st_MDataArray**)

declare void @DeinitializeCompileLibraryFunctions()

declare void @WolframRTL_initialize(i32)

declare %struct.st_WolframLibraryData* @WolframLibraryData_new(i32)

declare void @WolframLibraryData_free(%struct.st_WolframLibraryData*)

declare i32 @MemoryLinkedList_visit(%struct.MemoryLinkedList_struct*, i32 (i8*, i8*)*, i8*)

declare %struct.MemoryLinkedList_struct* @MemoryLinkedList_newLink(i32)

declare %struct.MemoryLinkedList_struct* @MemoryLinkedList_copy(%struct.MemoryLinkedList_struct*, i8* (i8*)*)

declare void @MemoryLinkedList_delete(%struct.MemoryLinkedList_struct*, void (i8*)*)

declare %struct.MBag_struct* @MBag_new(i32)

declare void @MBag_delete(%struct.MBag_struct*)

declare void @MBag_stuff(%struct.MBag_struct*, i32, i8*)

declare i32 @MBag_toMTensor(%struct.st_MDataArray**, %struct.MBag_struct*)

declare i32 @MBag_part(i8*, %struct.MBag_struct*, %struct.part_ind*)

declare %struct.MBagArray_struct* @MBagArray_new()

declare void @MBagArray_cleanUp(%struct.MBagArray_struct*)

declare void @MBagArray_delete(%struct.MBagArray_struct*)

declare i32 @MBagArray_newBag(%struct.MBagArray_struct*, i32)

declare void @_GLOBAL__sub_I_callback_functions.cpp()

declare i32 @checkFloatingPointException(i8*, i32, i32)

declare %struct.RuntimeData_struct* @RuntimeData_new(i32)

declare void @RuntimeData_free(%struct.RuntimeData_struct*)

declare void @RuntimeData_cleanUp(%struct.RuntimeData_struct*, i32)

declare void @ClearDLLInitializedMTensors(%struct.RuntimeData_struct*, i32)

declare void @CompiledBlockRandomClear(%struct.RuntimeData_struct*)

declare void @FreeDLLAllocatedMemory(%struct.RuntimeData_struct*)

declare %struct.M_TENSOR_INITIALIZATION_DATA_STRUCT* @GetInitializedMTensors(%struct.st_WolframLibraryData*, i32)

declare void @ReleaseInitializedMTensors(%struct.M_TENSOR_INITIALIZATION_DATA_STRUCT*)

declare i8** @getDLLFunctionArgumentSpace(%struct.st_WolframLibraryData*, i32)

declare i32 @CompiledBagPart(i8*, i32, i32, i8*, %struct.RuntimeData_struct*)

declare i8* @CompiledGetRandomGeneratorName(i32)

declare i32 @CompiledSeedRandom(i32*, i32)

declare void @CompiledBlockRandom(%struct.RuntimeData_struct*, i32)

declare i32 (%struct.st_WolframLibraryData*, i32, %union.MArgument*, [1 x i32])* @GetDLLFunctionPointer(i8*, i8*)

declare i32 @EvaluateExpression(%struct.st_WolframLibraryData*, i8*, i32, i32, i8*)

declare i32 @RegisterLibraryExpressionManager(i8*, void (%struct.st_WolframLibraryData*, i32, i32)*)

declare i32 @UnregisterLibraryExpressionManager(i8*)

declare i32 @ReleaseManagedLibraryExpression(i8*, i32)

declare i32 @RegisterLibraryCallbackManager(i8*, i32 (%struct.st_WolframLibraryData*, i32, %struct.st_MDataArray*)*)

declare i32 @UnregisterLibraryCallbackManager(i8*)

declare i32 @CallLibraryCallbackFunction(i32, i32, %union.MArgument*, [1 x i32])

declare i32 @ReleaseLibraryCallbackFunction(i32)

declare i32 @evaluateFunctionExpression(%struct.st_WolframLibraryData*, i8*, i32, i32, i32, i32*, i8**, i32, i32, i8*)

declare i8* @getFunctionExpressionPointer(%struct.st_WolframLibraryData*, i8*)

declare %struct.MLINK_STRUCT* @getMathLink_DLL(%struct.st_WolframLibraryData*)

declare %struct.MLENV_STRUCT* @getMathLinkEnvironment_DLL(%struct.st_WolframLibraryData*)

declare i32 @processMathLink_DLL(%struct.MLINK_STRUCT*)

declare void @DLLMessageFunction(i8*)

declare i32 @countDLLSharedReferences(i8*)

declare i32 @removeDLLSharedReference(i8*)

declare void @removeAllDLLSharedReferences(i8*)

declare i32 @registerInputStreamMethod(i8*, void (%struct.st_MInputStream*, i8*, i8*)*, i32 (i8*, i8*)*, i8*, void (i8*)*)

declare i32 @unregisterInputStreamMethod(i8*)

declare i32 @registerOutputStreamMethod(i8*, void (%struct.st_MOutputStream*, i8*, i8*, i32)*, i32 (i8*, i8*)*, i8*, void (i8*)*)

declare i32 @unregisterOutputStreamMethod(i8*)

declare i32 @CreateAsynchronousTaskWithoutThread()

declare i32 @RemoveAsynchronousTask(i32)

declare i32 @CreateAsynchronousTaskWithThread(void (i32, i8*)*, i8*)

declare void @RaiseAsyncEvent(i32, i8*, %struct.st_DataStore*)

declare i32 @AsynchronousTaskAliveQ(i32)

declare i32 @AsynchronousTaskStartedQ(i32)

declare %struct.st_DataStore* @CreateDataStore()

declare void @DeleteDataStore(%struct.st_DataStore*)

declare %struct.st_DataStore* @CopyDataStore(%struct.st_DataStore*)

declare void @DataStore_addBoolean(%struct.st_DataStore*, i32)

declare void @DataStore_addInteger(%struct.st_DataStore*, i32)

declare void @DataStore_addReal(%struct.st_DataStore*, double)

declare void @DataStore_addComplex(%struct.st_DataStore*, %"struct.std::complex")

declare void @DataStore_addString(%struct.st_DataStore*, i8*)

declare void @DataStore_addMTensor(%struct.st_DataStore*, %struct.st_MDataArray*)

declare void @DataStore_addMNumericArray(%struct.st_DataStore*, %struct.st_MDataArray*)

declare void @DataStore_addMImage(%struct.st_DataStore*, %struct.IMAGEOBJ_ENTRY*)

declare void @DataStore_addMSparseArray(%struct.st_DataStore*, %struct.st_MSparseArray*)

declare void @DataStore_addDataStore(%struct.st_DataStore*, %struct.st_DataStore*)

declare void @DataStore_addNamedBoolean(%struct.st_DataStore*, i8*, i32)

declare void @DataStore_addNamedInteger(%struct.st_DataStore*, i8*, i32)

declare void @DataStore_addNamedReal(%struct.st_DataStore*, i8*, double)

declare void @DataStore_addNamedComplex(%struct.st_DataStore*, i8*, %"struct.std::complex")

declare void @DataStore_addNamedString(%struct.st_DataStore*, i8*, i8*)

declare void @DataStore_addNamedMTensor(%struct.st_DataStore*, i8*, %struct.st_MDataArray*)

declare void @DataStore_addNamedMNumericArray(%struct.st_DataStore*, i8*, %struct.st_MDataArray*)

declare void @DataStore_addNamedMImage(%struct.st_DataStore*, i8*, %struct.IMAGEOBJ_ENTRY*)

declare void @DataStore_addNamedMSparseArray(%struct.st_DataStore*, i8*, %struct.st_MSparseArray*)

declare void @DataStore_addNamedDataStore(%struct.st_DataStore*, i8*, %struct.st_DataStore*)

declare %struct.DataStoreNode_t* @DataStore_getFirstNode(%struct.st_DataStore*)

declare %struct.DataStoreNode_t* @DataStore_getLastNode(%struct.st_DataStore*)

declare i32 @DataStore_getLength(%struct.st_DataStore*)

declare %struct.DataStoreNode_t* @DataStoreNode_getNextNode(%struct.DataStoreNode_t*)

declare i32 @DataStoreNode_getDataType(%struct.DataStoreNode_t*)

declare i32 @DataStoreNode_getData(%struct.DataStoreNode_t*, %union.MArgument*)

declare i32 @DataStoreNode_getName(%struct.DataStoreNode_t*, i8**)

declare i32 @validatePathLib(i8*, i8)

declare i32 @protectedModeQ()

declare %struct.st_WolframImageLibrary_Functions* @getWolframImageLibraryFunctions(i32)

declare i32 @_ZL12MImage_new2Diii16MImage_Data_Type14MImage_CS_TypeiPP14IMAGEOBJ_ENTRY(i32, i32, i32, i32, i32, i32, %struct.IMAGEOBJ_ENTRY**)

declare i32 @_ZL12MImage_new3Diiii16MImage_Data_Type14MImage_CS_TypeiPP14IMAGEOBJ_ENTRY(i32, i32, i32, i32, i32, i32, i32, %struct.IMAGEOBJ_ENTRY**)

declare i32 @_ZL12MImage_cloneP14IMAGEOBJ_ENTRYPS0_(%struct.IMAGEOBJ_ENTRY*, %struct.IMAGEOBJ_ENTRY**)

declare void @_ZL11MImage_freeP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare void @_ZL13MImage_disownP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare void @_ZL16MImage_disownAllP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL17MImage_shareCountP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL18MImage_getDataTypeP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL18MImage_getRowCountP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL21MImage_getColumnCountP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL20MImage_getSliceCountP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL14MImage_getRankP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL18MImage_getChannelsP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL20MImage_alphaChannelQP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL19MImage_interleavedQP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL20MImage_getColorSpaceP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL25MImage_getFlattenedLengthP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i32 @_ZL13MImage_getBitP14IMAGEOBJ_ENTRYPiiPa(%struct.IMAGEOBJ_ENTRY*, i32*, i32, i8*)

declare i32 @_ZL14MImage_getByteP14IMAGEOBJ_ENTRYPiiPh(%struct.IMAGEOBJ_ENTRY*, i32*, i32, i8*)

declare i32 @_ZL15MImage_getBit16P14IMAGEOBJ_ENTRYPiiPt(%struct.IMAGEOBJ_ENTRY*, i32*, i32, i16*)

declare i32 @_ZL16MImage_getReal32P14IMAGEOBJ_ENTRYPiiPf(%struct.IMAGEOBJ_ENTRY*, i32*, i32, float*)

declare i32 @_ZL14MImage_getRealP14IMAGEOBJ_ENTRYPiiPd(%struct.IMAGEOBJ_ENTRY*, i32*, i32, double*)

declare i32 @_ZL13MImage_setBitP14IMAGEOBJ_ENTRYPiia(%struct.IMAGEOBJ_ENTRY*, i32*, i32, i8)

declare i32 @_ZL14MImage_setByteP14IMAGEOBJ_ENTRYPiih(%struct.IMAGEOBJ_ENTRY*, i32*, i32, i8)

declare i32 @_ZL15MImage_setBit16P14IMAGEOBJ_ENTRYPiit(%struct.IMAGEOBJ_ENTRY*, i32*, i32, i16)

declare i32 @_ZL16MImage_setReal32P14IMAGEOBJ_ENTRYPiif(%struct.IMAGEOBJ_ENTRY*, i32*, i32, float)

declare i32 @_ZL14MImage_setRealP14IMAGEOBJ_ENTRYPiid(%struct.IMAGEOBJ_ENTRY*, i32*, i32, double)

declare i8* @_ZL17MImage_getRawDataP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i8* @_ZL17MImage_getBitDataP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i8* @_ZL18MImage_getByteDataP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare i16* @_ZL19MImage_getBit16DataP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare float* @_ZL20MImage_getReal32DataP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare double* @_ZL18MImage_getRealDataP14IMAGEOBJ_ENTRY(%struct.IMAGEOBJ_ENTRY*)

declare %struct.IMAGEOBJ_ENTRY* @_ZL18MImage_convertTypeP14IMAGEOBJ_ENTRY16MImage_Data_Typei(%struct.IMAGEOBJ_ENTRY*, i32, i32)

declare void @DeinitializeImageLibraryFunctions()

declare %struct.st_WolframNumericArrayLibrary_Functions* @getWolframNumericArrayLibraryFunctions(i32)

declare i32 @_ZL20MNumericArray_newDLL23MNumericArray_Data_TypeiPKiPP13st_MDataArray(i32, i32, i32*, %struct.st_MDataArray**)

declare void @_ZL18MNumericArray_freeP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL19MNumericArray_cloneP13st_MDataArrayPS0_(%struct.st_MDataArray*, %struct.st_MDataArray**)

declare void @_ZL20MNumericArray_disownP13st_MDataArray(%struct.st_MDataArray*)

declare void @_ZL23MNumericArray_disownAllP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL24MNumericArray_shareCountP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL24MNumericArray_getTypeDLLP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL24MNumericArray_getRankDLLP13st_MDataArray(%struct.st_MDataArray*)

declare i32* @_ZL30MNumericArray_getDimensionsDLLP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL32MNumericArray_getFlattenedLengthP13st_MDataArray(%struct.st_MDataArray*)

declare i8* @_ZL21MNumericArray_getDataP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL25MNumericArray_convertTypePP13st_MDataArrayS0_23MNumericArray_Data_Type28MNumericArray_Convert_Methodd(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32, double)

declare void @DeinitializeNumericArrayLibraryFunctions()

declare %struct.st_WolframRawArrayLibrary_Functions* @getWolframRawArrayLibraryFunctions(i32)

declare i32 @_ZL16MRawArray_newDLL19MRawArray_Data_TypeiPKiPP13st_MDataArray(i32, i32, i32*, %struct.st_MDataArray**)

declare void @_ZL14MRawArray_freeP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL15MRawArray_cloneP13st_MDataArrayPS0_(%struct.st_MDataArray*, %struct.st_MDataArray**)

declare void @_ZL16MRawArray_disownP13st_MDataArray(%struct.st_MDataArray*)

declare void @_ZL19MRawArray_disownAllP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL20MRawArray_shareCountP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL20MRawArray_getTypeDLLP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL20MRawArray_getRankDLLP13st_MDataArray(%struct.st_MDataArray*)

declare i32* @_ZL26MRawArray_getDimensionsDLLP13st_MDataArray(%struct.st_MDataArray*)

declare i32 @_ZL28MRawArray_getFlattenedLengthP13st_MDataArray(%struct.st_MDataArray*)

declare i8* @_ZL17MRawArray_getDataP13st_MDataArray(%struct.st_MDataArray*)

declare %struct.st_MDataArray* @_ZL21MRawArray_convertTypeP13st_MDataArray19MRawArray_Data_Type(%struct.st_MDataArray*, i32)

declare void @DeinitializeRawArrayLibraryFunctions()

declare %struct.st_WolframSparseLibrary_Functions* @getWolframSparseLibraryFunctions(i32)

declare i32 @_ZL18MSparseArray_cloneP15st_MSparseArrayPS0_(%struct.st_MSparseArray*, %struct.st_MSparseArray**)

declare void @_ZL17MSparseArray_freeP15st_MSparseArray(%struct.st_MSparseArray*)

declare void @_ZL19MSparseArray_disownP15st_MSparseArray(%struct.st_MSparseArray*)

declare void @_ZL22MSparseArray_disownAllP15st_MSparseArray(%struct.st_MSparseArray*)

declare i32 @_ZL23MSparseArray_shareCountP15st_MSparseArray(%struct.st_MSparseArray*)

declare i32 @_ZL28MSparseArray_getRankFunctionP15st_MSparseArray(%struct.st_MSparseArray*)

declare i32* @_ZL29MTensor_GetDimensionsFunctionP15st_MSparseArray(%struct.st_MSparseArray*)

declare %struct.st_MDataArray** @_ZL37MSparseArray_getImplicitValueFunctionP15st_MSparseArray(%struct.st_MSparseArray*)

declare %struct.st_MDataArray** @_ZL38MSparseArray_getExplicitValuesFunctionP15st_MSparseArray(%struct.st_MSparseArray*)

declare %struct.st_MDataArray** @_ZL35MSparseArray_getRowPointersFunctionP15st_MSparseArray(%struct.st_MSparseArray*)

declare %struct.st_MDataArray** @_ZL37MSparseArray_getColumnIndicesFunctionP15st_MSparseArray(%struct.st_MSparseArray*)

declare i32 @_ZL41MSparseArray_getExplicitPositionsFunctionP15st_MSparseArrayPP13st_MDataArray(%struct.st_MSparseArray*, %struct.st_MDataArray**)

declare void @DeinitializeSparseLibraryFunctions()

declare %struct.st_WolframIOLibrary_Functions* @getWolframIOLibraryFunctions(i32)

declare void @DeinitializeIOLibraryFunctions()

declare void @_Z15InitRTLCompileri(i32)

declare %struct.st_MDataArray* @CreatePackedArray(i32, i32, i32*)

declare %struct.st_MDataArray* @Runtime_CreateNumericArray(i32, i32, i32*)

declare void @FreeMTensor(%struct.st_MDataArray*)

declare i64 @MTensor_RefCountIncrement(%struct.st_MDataArray*)

declare i64 @MTensor_RefCountDecrement(%struct.st_MDataArray*)

declare i32 @MTensor_getParts(%struct.st_MDataArray*, %struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32, i32*, i8**)

declare i32 @MTensor_setParts(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32, i32*, i8**)

declare i32 @MNumericArray_getParts(%struct.st_MDataArray*, %struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32, i32*, i8**)

declare i32 @MTensorMathUnary(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32)

declare i32 @MTensorMathBinary(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @VectorDot_I(i32*, i32*, i32, i32*, i32, i32, i32)

declare i32 @VectorDot_R64(double*, double*, i32, double*, i32, i32, i32)

declare i32 @VectorDot_CR64(i8*, i8*, i32, i8*, i32, i32, i32)

declare i32 @MTensorGeneralDot(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @Runtime_IteratorCount_I_I_I_I(i32, i32, i32)

declare i32 @Runtime_IteratorCount_R_R_R_I(double, double, double)

declare i32 @Runtime_UniformRandomMIntegers(i32*, i32, i32, i32)

declare i32 @Runtime_UniformRandomMReals(double*, i32, double, double)

declare i32 @Runtime_UniformRandomMComplexes(i8*, i32, double, double, double, double)

declare i32 @Runtime_SeedRandom(i32, i32)

declare %struct.RandomGenerator_struct* @Runtime_GetCurrentRandomGenerator()

declare i32 (i32, %struct.RandomGenerator_struct*)* @Runtime_RandomGenerator_getBitFunction(%struct.RandomGenerator_struct*)

declare i32 @Runtime_MTensor_TakeDrop(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i8**, i32)

declare i32 @Runtime_MTensor_Partition(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @Runtime_MTensor_Reverse(%struct.st_MDataArray**, %struct.st_MDataArray*)

declare i32 @Runtime_MTensor_Flatten(%struct.st_MDataArray**, %struct.st_MDataArray*, i32)

declare i32 @Runtime_MTensor_Rotate(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*)

declare i32 @Runtime_MTensor_12TransposeConjugate(%struct.st_MDataArray**, %struct.st_MDataArray*, i32)

declare i32 @Runtime_MTensor_ndimTransposeConjugate(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @Runtime_MTensor_Sort(%struct.st_MDataArray**, %struct.st_MDataArray*)

declare i32 @Runtime_MTensor_computeSortPermutation(%struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @_ZL22abortWatchCallbackInitPKc(i8*)

declare %"struct.std::pair"* @CompilerError_null()

declare %"struct.std::pair"* @New_CompilerError(i32, i32)

declare void @Delete_CompilerError(%"struct.std::pair"*)

declare void @InitializeWolframRTL(i8*, i8*, i8*)

declare %struct.st_MDataArray* @getEternalMTensor()

declare void @_Z17init_compiler_rtli(i32)

declare void @_GLOBAL__sub_I_exceptions.cpp()

declare void @Unwind_Resume_Wolfram(%struct._Unwind_Control_Block*)

declare void @_Unwind_Resume(%struct._Unwind_Control_Block*)

declare void @setExceptionStyle(i32)

declare i32 @getExceptionActive()

declare i32 @setExceptionActive(i32)

declare i32* @SetJumpStack_Push()

declare i32 @SetJumpStack_PushAux()

declare i32 @_setjmp(%struct.__jmp_buf_tag*)

declare void @SetJumpStack_Pop()

declare %"struct.std::pair"* @catchExceptionHandler(i32, i8**)

declare i32 @throwWolframException(i32)

declare void @longjmp(%struct.__jmp_buf_tag*, i32)

declare %"class.std::exception"* @_ZNSt9exceptionD2Ev(%"class.std::exception"*)

declare void @_ZN23WolframRuntimeExceptionD0Ev(%class.WolframRuntimeException*)

declare i8* @_ZNK23WolframRuntimeException4whatEv(%class.WolframRuntimeException*)

declare i32 @vsnprintf(i8*, i32, i8*, [1 x i32])

declare void @_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_jPKS8_St9__va_listEjSB_z(%"class.std::__cxx11::basic_string"*, i32 (i8*, i32, i8*, [1 x i32])*, i32, i8*, ...)

declare void @_ZdlPv(i8*)

declare void @__cxa_call_unexpected(i8*)

; Function Attrs: nounwind
declare void @llvm.va_start(i8*) #3

; Function Attrs: nounwind
declare void @llvm.va_end(i8*) #3

declare i8* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERjj(%"class.std::__cxx11::basic_string"*, i32*, i32)

declare i32* @getExceptionActiveHandle()

declare i32 @dummyFunction(i32)

declare i32* @getAbortWatchHandle()

declare i32 @checkAbortWatch()

declare i32 @checkAbortWatchThrow()

declare i32 @NonParallelDo_Closure(i8*, i8*, i32, i32)

declare i32 @NonParallelDo(i32 (i32)*, i32, i32)

declare %struct.st_MDataArray* @mtensor_null()

declare %struct.st_MDataArray* @mnumericarray_null()

declare i32 @testRealType(double, i32)

declare void @_GLOBAL__sub_I_array_binary.cpp()

declare void @_GLOBAL__sub_I_array_cast.cpp()

declare void @_GLOBAL__sub_I_binary.cpp()

declare i8 @binary_abserr_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_abserr_Integer8_Integer8(i8, i8)

declare i1 @_ZN3wrt9scalar_op6binary6detail16subtract_checkedIaEEbT_S4_PS4_(i8, i8, i8*)

declare i1 @_ZN3wrtL23check_integral_overflowIsaEENSt9enable_ifIXaasr18is_signed_integralIT_EE5valuesr11is_integralIT0_EE5valueEbE4typeERKS2_(i16)

declare i16 @binary_abserr_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_abserr_Integer16_Integer16(i16, i16)

declare i1 @_ZN3wrt9scalar_op6binary6detail16subtract_checkedIsEEbT_S4_PS4_(i16, i16, i16*)

declare i1 @_ZN3wrtL23check_integral_overflowIisEENSt9enable_ifIXaasr18is_signed_integralIT_EE5valuesr11is_integralIT0_EE5valueEbE4typeERKS2_(i32)

declare i32 @binary_abserr_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_abserr_Integer32_Integer32(i32, i32)

declare i64 @binary_abserr_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_abserr_Integer64_Integer64(i64, i64)

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.ssub.with.overflow.i64(i64, i64) #0

declare i8 @binary_abserr_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_abserr_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_abserr_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_abserr_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_abserr_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_abserr_UnsignedInteger32_UnsignedInteger32(i32, i32)

; Function Attrs: nounwind readnone speculatable
declare { i32, i1 } @llvm.usub.with.overflow.i32(i32, i32) #0

declare i64 @binary_abserr_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_abserr_UnsignedInteger64_UnsignedInteger64(i64, i64)

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.usub.with.overflow.i64(i64, i64) #0

declare i16 @binary_abserr_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_abserr_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_abserr_Real32_Real32(float, float)

; Function Attrs: nounwind readnone speculatable
declare float @llvm.fabs.f32(float) #0

declare float @checked_binary_abserr_Real32_Real32(float, float)

declare double @binary_abserr_Real64_Real64(double, double)

declare double @checked_binary_abserr_Real64_Real64(double, double)

declare float @binary_abserr_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare float @cabsf({ float, float })

declare float @checked_binary_abserr_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare double @binary_abserr_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare double @checked_binary_abserr_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_atan2_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_atan2_Integer8_Integer8(i8, i8)

declare i16 @binary_atan2_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_atan2_Integer16_Integer16(i16, i16)

declare i32 @binary_atan2_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_atan2_Integer32_Integer32(i32, i32)

declare i64 @binary_atan2_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_atan2_Integer64_Integer64(i64, i64)

declare i8 @binary_atan2_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_atan2_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_atan2_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_atan2_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_atan2_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_atan2_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_atan2_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_atan2_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_atan2_Real16_Real16([1 x i32], [1 x i32])

declare float @atan2f(float, float)

declare i16 @checked_binary_atan2_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_atan2_Real32_Real32(float, float)

declare float @checked_binary_atan2_Real32_Real32(float, float)

declare double @binary_atan2_Real64_Real64(double, double)

declare double @checked_binary_atan2_Real64_Real64(double, double)

declare void @binary_atan2_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare { float, float } @__mulsc3(float, float, float, float)

declare { float, float } @_ZN3wrt9scalar_op5unaryL8rsqrt_opISt7complexIfELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE64EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(float, float)

declare float @logf(float)

declare { float, float } @clogf({ float, float })

declare float @sqrtf(float)

declare void @__assert_fail(i8*, i8*, i32, i8*)

declare void @checked_binary_atan2_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_atan2_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8rsqrt_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE64EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.203(double, double)

declare void @checked_binary_atan2_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @binary_bit_and_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_bit_and_Integer8_Integer8(i8, i8)

declare i16 @binary_bit_and_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_bit_and_Integer16_Integer16(i16, i16)

declare i32 @binary_bit_and_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_bit_and_Integer32_Integer32(i32, i32)

declare i64 @binary_bit_and_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_bit_and_Integer64_Integer64(i64, i64)

declare i8 @binary_bit_and_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_bit_and_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_bit_and_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_bit_and_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_bit_and_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_bit_and_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_bit_and_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_bit_and_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_bit_and_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_bit_and_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_bit_and_Real32_Real32(float, float)

declare float @checked_binary_bit_and_Real32_Real32(float, float)

declare double @binary_bit_and_Real64_Real64(double, double)

declare double @checked_binary_bit_and_Real64_Real64(double, double)

declare %"struct.std::complex.203" @binary_bit_and_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_binary_bit_and_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex" @binary_bit_and_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare %"struct.std::complex" @checked_binary_bit_and_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_bit_or_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_bit_or_Integer8_Integer8(i8, i8)

declare i16 @binary_bit_or_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_bit_or_Integer16_Integer16(i16, i16)

declare i32 @binary_bit_or_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_bit_or_Integer32_Integer32(i32, i32)

declare i64 @binary_bit_or_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_bit_or_Integer64_Integer64(i64, i64)

declare i8 @binary_bit_or_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_bit_or_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_bit_or_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_bit_or_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_bit_or_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_bit_or_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_bit_or_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_bit_or_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_bit_or_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_bit_or_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_bit_or_Real32_Real32(float, float)

declare float @checked_binary_bit_or_Real32_Real32(float, float)

declare double @binary_bit_or_Real64_Real64(double, double)

declare double @checked_binary_bit_or_Real64_Real64(double, double)

declare %"struct.std::complex.203" @binary_bit_or_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_binary_bit_or_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex" @binary_bit_or_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare %"struct.std::complex" @checked_binary_bit_or_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_bit_shiftleft_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_bit_shiftleft_Integer8_Integer8(i8, i8)

declare i16 @binary_bit_shiftleft_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_bit_shiftleft_Integer16_Integer16(i16, i16)

declare i32 @binary_bit_shiftleft_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_bit_shiftleft_Integer32_Integer32(i32, i32)

declare i64 @binary_bit_shiftleft_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_bit_shiftleft_Integer64_Integer64(i64, i64)

declare i8 @binary_bit_shiftleft_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_bit_shiftleft_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_bit_shiftleft_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_bit_shiftleft_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_bit_shiftleft_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_bit_shiftleft_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_bit_shiftleft_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_bit_shiftleft_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_bit_shiftleft_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_bit_shiftleft_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_bit_shiftleft_Real32_Real32(float, float)

declare float @checked_binary_bit_shiftleft_Real32_Real32(float, float)

declare double @binary_bit_shiftleft_Real64_Real64(double, double)

declare double @checked_binary_bit_shiftleft_Real64_Real64(double, double)

declare void @binary_bit_shiftleft_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @checked_binary_bit_shiftleft_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_bit_shiftleft_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_bit_shiftleft_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @binary_bit_shiftright_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_bit_shiftright_Integer8_Integer8(i8, i8)

declare i16 @binary_bit_shiftright_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_bit_shiftright_Integer16_Integer16(i16, i16)

declare i32 @binary_bit_shiftright_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_bit_shiftright_Integer32_Integer32(i32, i32)

declare i64 @binary_bit_shiftright_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_bit_shiftright_Integer64_Integer64(i64, i64)

declare i8 @binary_bit_shiftright_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_bit_shiftright_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_bit_shiftright_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_bit_shiftright_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_bit_shiftright_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_bit_shiftright_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_bit_shiftright_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_bit_shiftright_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_bit_shiftright_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_bit_shiftright_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_bit_shiftright_Real32_Real32(float, float)

declare float @checked_binary_bit_shiftright_Real32_Real32(float, float)

declare double @binary_bit_shiftright_Real64_Real64(double, double)

declare double @checked_binary_bit_shiftright_Real64_Real64(double, double)

declare void @binary_bit_shiftright_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @checked_binary_bit_shiftright_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_bit_shiftright_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_bit_shiftright_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @binary_bit_xor_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_bit_xor_Integer8_Integer8(i8, i8)

declare i16 @binary_bit_xor_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_bit_xor_Integer16_Integer16(i16, i16)

declare i32 @binary_bit_xor_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_bit_xor_Integer32_Integer32(i32, i32)

declare i64 @binary_bit_xor_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_bit_xor_Integer64_Integer64(i64, i64)

declare i8 @binary_bit_xor_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_bit_xor_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_bit_xor_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_bit_xor_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_bit_xor_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_bit_xor_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_bit_xor_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_bit_xor_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_bit_xor_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_bit_xor_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_bit_xor_Real32_Real32(float, float)

declare float @checked_binary_bit_xor_Real32_Real32(float, float)

declare double @binary_bit_xor_Real64_Real64(double, double)

declare double @checked_binary_bit_xor_Real64_Real64(double, double)

declare %"struct.std::complex.203" @binary_bit_xor_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_binary_bit_xor_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex" @binary_bit_xor_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare %"struct.std::complex" @checked_binary_bit_xor_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_chop_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_chop_Integer8_Integer8(i8, i8)

declare i16 @binary_chop_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_chop_Integer16_Integer16(i16, i16)

declare i32 @binary_chop_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_chop_Integer32_Integer32(i32, i32)

declare i64 @binary_chop_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_chop_Integer64_Integer64(i64, i64)

declare i8 @binary_chop_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_chop_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_chop_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_chop_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_chop_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_chop_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_chop_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_chop_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_chop_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_chop_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_chop_Real32_Real32(float, float)

declare float @checked_binary_chop_Real32_Real32(float, float)

declare double @binary_chop_Real64_Real64(double, double)

declare double @checked_binary_chop_Real64_Real64(double, double)

declare void @binary_chop_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @checked_binary_chop_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_chop_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_chop_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @binary_divide_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_divide_Integer8_Integer8(i8, i8)

declare i16 @binary_divide_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_divide_Integer16_Integer16(i16, i16)

declare i32 @binary_divide_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_divide_Integer32_Integer32(i32, i32)

declare i64 @binary_divide_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_divide_Integer64_Integer64(i64, i64)

declare i8 @binary_divide_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_divide_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_divide_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_divide_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_divide_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_divide_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_divide_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_divide_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_divide_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_divide_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_divide_Real32_Real32(float, float)

declare float @checked_binary_divide_Real32_Real32(float, float)

declare double @binary_divide_Real64_Real64(double, double)

declare double @checked_binary_divide_Real64_Real64(double, double)

declare void @binary_divide_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare { float, float } @__divsc3(float, float, float, float)

declare void @checked_binary_divide_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_divide_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_divide_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i32 @binary_fpexception_Integer8_Integer8(i8, i8)

declare i32 @checked_binary_fpexception_Integer8_Integer8(i8, i8)

declare i32 @binary_fpexception_Integer16_Integer16(i16, i16)

declare i32 @checked_binary_fpexception_Integer16_Integer16(i16, i16)

declare i32 @binary_fpexception_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_fpexception_Integer32_Integer32(i32, i32)

declare i32 @binary_fpexception_Integer64_Integer64(i64, i64)

declare i32 @checked_binary_fpexception_Integer64_Integer64(i64, i64)

declare i32 @binary_fpexception_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i32 @checked_binary_fpexception_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i32 @binary_fpexception_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @checked_binary_fpexception_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_fpexception_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_fpexception_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @binary_fpexception_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i32 @checked_binary_fpexception_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i32 @binary_fpexception_Real16_Real16([1 x i32], [1 x i32])

declare i32 @checked_binary_fpexception_Real16_Real16([1 x i32], [1 x i32])

declare i32 @binary_fpexception_Real32_Real32(float, float)

declare i32 @checked_binary_fpexception_Real32_Real32(float, float)

declare i32 @binary_fpexception_Real64_Real64(double, double)

declare i32 @checked_binary_fpexception_Real64_Real64(double, double)

declare i32 @binary_fpexception_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare i32 @checked_binary_fpexception_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare i32 @binary_fpexception_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i32 @checked_binary_fpexception_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_intexp_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_intexp_Integer8_Integer8(i8, i8)

declare i16 @binary_intexp_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_intexp_Integer16_Integer16(i16, i16)

declare i32 @binary_intexp_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_intexp_Integer32_Integer32(i32, i32)

declare i64 @binary_intexp_Integer64_Integer64(i64, i64)

; Function Attrs: nounwind readnone speculatable
declare i64 @llvm.cttz.i64(i64, i1) #0

declare void @lldiv(%struct.lldiv_t*, i64, i64)

declare i64 @checked_binary_intexp_Integer64_Integer64(i64, i64)

declare i8 @binary_intexp_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_intexp_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_intexp_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_intexp_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_intexp_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_intexp_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_intexp_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_intexp_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_intexp_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_intexp_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_intexp_Real32_Real32(float, float)

declare float @checked_binary_intexp_Real32_Real32(float, float)

declare double @binary_intexp_Real64_Real64(double, double)

declare double @checked_binary_intexp_Real64_Real64(double, double)

declare %"struct.std::complex.203" @binary_intexp_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_binary_intexp_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex" @binary_intexp_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare %"struct.std::complex" @checked_binary_intexp_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_intlen_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_intlen_Integer8_Integer8(i8, i8)

declare i16 @binary_intlen_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_intlen_Integer16_Integer16(i16, i16)

declare i32 @binary_intlen_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_intlen_Integer32_Integer32(i32, i32)

declare i64 @binary_intlen_Integer64_Integer64(i64, i64)

; Function Attrs: nounwind readnone speculatable
declare i64 @llvm.ctlz.i64(i64, i1) #0

declare i64 @checked_binary_intlen_Integer64_Integer64(i64, i64)

declare i8 @binary_intlen_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_intlen_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_intlen_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_intlen_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_intlen_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_intlen_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_intlen_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_intlen_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_intlen_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_intlen_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_intlen_Real32_Real32(float, float)

declare float @checked_binary_intlen_Real32_Real32(float, float)

declare double @binary_intlen_Real64_Real64(double, double)

declare double @checked_binary_intlen_Real64_Real64(double, double)

declare %"struct.std::complex.203" @binary_intlen_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_binary_intlen_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex" @binary_intlen_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare %"struct.std::complex" @checked_binary_intlen_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_log_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_log_Integer8_Integer8(i8, i8)

declare i16 @binary_log_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_log_Integer16_Integer16(i16, i16)

declare i32 @binary_log_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_log_Integer32_Integer32(i32, i32)

declare i64 @binary_log_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_log_Integer64_Integer64(i64, i64)

declare i8 @binary_log_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_log_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_log_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_log_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_log_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_log_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_log_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_log_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_log_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_log_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_log_Real32_Real32(float, float)

declare float @checked_binary_log_Real32_Real32(float, float)

declare double @binary_log_Real64_Real64(double, double)

declare double @checked_binary_log_Real64_Real64(double, double)

declare void @binary_log_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @checked_binary_log_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_log_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_log_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @binary_maxabs_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_maxabs_Integer8_Integer8(i8, i8)

declare i16 @binary_maxabs_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_maxabs_Integer16_Integer16(i16, i16)

declare i32 @binary_maxabs_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_maxabs_Integer32_Integer32(i32, i32)

declare i64 @binary_maxabs_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_maxabs_Integer64_Integer64(i64, i64)

declare i8 @binary_maxabs_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_maxabs_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_maxabs_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_maxabs_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_maxabs_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_maxabs_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_maxabs_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_maxabs_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_maxabs_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_maxabs_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_maxabs_Real32_Real32(float, float)

; Function Attrs: nounwind readnone speculatable
declare float @llvm.maxnum.f32(float, float) #0

declare float @checked_binary_maxabs_Real32_Real32(float, float)

declare double @binary_maxabs_Real64_Real64(double, double)

declare double @checked_binary_maxabs_Real64_Real64(double, double)

declare float @binary_maxabs_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare float @checked_binary_maxabs_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare double @binary_maxabs_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare double @checked_binary_maxabs_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_max_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_max_Integer8_Integer8(i8, i8)

declare i16 @binary_max_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_max_Integer16_Integer16(i16, i16)

declare i32 @binary_max_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_max_Integer32_Integer32(i32, i32)

declare i64 @binary_max_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_max_Integer64_Integer64(i64, i64)

declare i8 @binary_max_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_max_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_max_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_max_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_max_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_max_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_max_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_max_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_max_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_max_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_max_Real32_Real32(float, float)

declare float @checked_binary_max_Real32_Real32(float, float)

declare double @binary_max_Real64_Real64(double, double)

declare double @checked_binary_max_Real64_Real64(double, double)

declare %"struct.std::complex.203" @binary_max_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_binary_max_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex" @binary_max_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare %"struct.std::complex" @checked_binary_max_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_min_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_min_Integer8_Integer8(i8, i8)

declare i16 @binary_min_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_min_Integer16_Integer16(i16, i16)

declare i32 @binary_min_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_min_Integer32_Integer32(i32, i32)

declare i64 @binary_min_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_min_Integer64_Integer64(i64, i64)

declare i8 @binary_min_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_min_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_min_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_min_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_min_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_min_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_min_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_min_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_min_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_min_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_min_Real32_Real32(float, float)

; Function Attrs: nounwind readnone speculatable
declare float @llvm.minnum.f32(float, float) #0

declare float @checked_binary_min_Real32_Real32(float, float)

declare double @binary_min_Real64_Real64(double, double)

declare double @checked_binary_min_Real64_Real64(double, double)

declare %"struct.std::complex.203" @binary_min_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_binary_min_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex" @binary_min_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare %"struct.std::complex" @checked_binary_min_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_mod_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_mod_Integer8_Integer8(i8, i8)

declare i16 @binary_mod_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_mod_Integer16_Integer16(i16, i16)

declare i32 @binary_mod_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_mod_Integer32_Integer32(i32, i32)

declare i64 @binary_mod_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_mod_Integer64_Integer64(i64, i64)

declare i8 @binary_mod_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_mod_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_mod_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_mod_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_mod_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_mod_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_mod_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_mod_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_mod_Real16_Real16([1 x i32], [1 x i32])

declare float @fmodf(float, float)

declare i16 @checked_binary_mod_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_mod_Real32_Real32(float, float)

declare float @checked_binary_mod_Real32_Real32(float, float)

declare double @binary_mod_Real64_Real64(double, double)

declare double @checked_binary_mod_Real64_Real64(double, double)

declare void @binary_mod_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare i32 @fegetround()

; Function Attrs: nounwind readnone speculatable
declare float @llvm.nearbyint.f32(float) #0

declare void @checked_binary_mod_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_mod_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_mod_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @binary_plus_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_plus_Integer8_Integer8(i8, i8)

; Function Attrs: nounwind readnone speculatable
declare { i8, i1 } @llvm.sadd.with.overflow.i8(i8, i8) #0

declare i16 @binary_plus_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_plus_Integer16_Integer16(i16, i16)

; Function Attrs: nounwind readnone speculatable
declare { i16, i1 } @llvm.sadd.with.overflow.i16(i16, i16) #0

declare i32 @binary_plus_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_plus_Integer32_Integer32(i32, i32)

declare i64 @binary_plus_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_plus_Integer64_Integer64(i64, i64)

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.sadd.with.overflow.i64(i64, i64) #0

declare i8 @binary_plus_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_plus_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_plus_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_plus_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_plus_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_plus_UnsignedInteger32_UnsignedInteger32(i32, i32)

; Function Attrs: nounwind readnone speculatable
declare { i32, i1 } @llvm.uadd.with.overflow.i32(i32, i32) #0

declare i64 @binary_plus_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_plus_UnsignedInteger64_UnsignedInteger64(i64, i64)

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.uadd.with.overflow.i64(i64, i64) #0

declare i16 @binary_plus_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_plus_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_plus_Real32_Real32(float, float)

declare float @checked_binary_plus_Real32_Real32(float, float)

declare double @binary_plus_Real64_Real64(double, double)

declare double @checked_binary_plus_Real64_Real64(double, double)

declare void @binary_plus_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @checked_binary_plus_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_plus_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_plus_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @binary_quotient_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_quotient_Integer8_Integer8(i8, i8)

declare i16 @binary_quotient_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_quotient_Integer16_Integer16(i16, i16)

declare i32 @binary_quotient_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_quotient_Integer32_Integer32(i32, i32)

declare i64 @binary_quotient_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_quotient_Integer64_Integer64(i64, i64)

declare i8 @binary_quotient_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_quotient_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_quotient_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_quotient_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_quotient_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_quotient_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_quotient_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_quotient_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i32 @binary_quotient_Real16_Real16([1 x i32], [1 x i32])

declare i32 @checked_binary_quotient_Real16_Real16([1 x i32], [1 x i32])

declare i32 @binary_quotient_Real32_Real32(float, float)

; Function Attrs: nounwind readnone speculatable
declare float @llvm.floor.f32(float) #0

declare i32 @checked_binary_quotient_Real32_Real32(float, float)

declare i32 @binary_quotient_Real64_Real64(double, double)

declare i32 @checked_binary_quotient_Real64_Real64(double, double)

declare i32 @binary_quotient_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare i32 @checked_binary_quotient_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare i32 @binary_quotient_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i32 @checked_binary_quotient_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_root_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_root_Integer8_Integer8(i8, i8)

declare i16 @binary_root_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_root_Integer16_Integer16(i16, i16)

declare i32 @binary_root_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_root_Integer32_Integer32(i32, i32)

declare i64 @binary_root_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_root_Integer64_Integer64(i64, i64)

declare i8 @binary_root_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_root_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_root_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_root_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_root_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_root_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_root_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_root_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_root_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_root_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_root_Real32_Real32(float, float)

declare float @checked_binary_root_Real32_Real32(float, float)

declare double @binary_root_Real64_Real64(double, double)

declare double @checked_binary_root_Real64_Real64(double, double)

declare %"struct.std::complex.203" @binary_root_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_binary_root_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare %"struct.std::complex" @binary_root_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare %"struct.std::complex" @checked_binary_root_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_relerr_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_relerr_Integer8_Integer8(i8, i8)

declare i16 @binary_relerr_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_relerr_Integer16_Integer16(i16, i16)

declare i32 @binary_relerr_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_relerr_Integer32_Integer32(i32, i32)

declare i64 @binary_relerr_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_relerr_Integer64_Integer64(i64, i64)

declare i8 @binary_relerr_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_relerr_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_relerr_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_relerr_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_relerr_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_relerr_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_relerr_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_relerr_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_relerr_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_relerr_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_relerr_Real32_Real32(float, float)

declare float @checked_binary_relerr_Real32_Real32(float, float)

declare double @binary_relerr_Real64_Real64(double, double)

declare double @checked_binary_relerr_Real64_Real64(double, double)

declare float @binary_relerr_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare float @checked_binary_relerr_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare double @binary_relerr_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare double @checked_binary_relerr_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_subtract_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_subtract_Integer8_Integer8(i8, i8)

declare i16 @binary_subtract_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_subtract_Integer16_Integer16(i16, i16)

declare i32 @binary_subtract_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_subtract_Integer32_Integer32(i32, i32)

declare i64 @binary_subtract_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_subtract_Integer64_Integer64(i64, i64)

declare i8 @binary_subtract_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_subtract_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_subtract_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_subtract_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_subtract_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_subtract_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_subtract_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_subtract_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i16 @binary_subtract_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_subtract_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_subtract_Real32_Real32(float, float)

declare float @checked_binary_subtract_Real32_Real32(float, float)

declare double @binary_subtract_Real64_Real64(double, double)

declare double @checked_binary_subtract_Real64_Real64(double, double)

declare void @binary_subtract_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @checked_binary_subtract_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_subtract_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_subtract_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @binary_times_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_times_Integer8_Integer8(i8, i8)

declare i16 @binary_times_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_times_Integer16_Integer16(i16, i16)

declare i32 @binary_times_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_times_Integer32_Integer32(i32, i32)

declare i64 @binary_times_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_times_Integer64_Integer64(i64, i64)

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.smul.with.overflow.i64(i64, i64) #0

declare i8 @binary_times_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_times_UnsignedInteger8_UnsignedInteger8(i8, i8)

; Function Attrs: nounwind readnone speculatable
declare { i8, i1 } @llvm.umul.with.overflow.i8(i8, i8) #0

declare i16 @binary_times_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_times_UnsignedInteger16_UnsignedInteger16(i16, i16)

; Function Attrs: nounwind readnone speculatable
declare { i16, i1 } @llvm.umul.with.overflow.i16(i16, i16) #0

declare i32 @binary_times_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_times_UnsignedInteger32_UnsignedInteger32(i32, i32)

; Function Attrs: nounwind readnone speculatable
declare { i32, i1 } @llvm.umul.with.overflow.i32(i32, i32) #0

declare i64 @binary_times_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_times_UnsignedInteger64_UnsignedInteger64(i64, i64)

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.umul.with.overflow.i64(i64, i64) #0

declare i16 @binary_times_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_times_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_times_Real32_Real32(float, float)

declare float @checked_binary_times_Real32_Real32(float, float)

declare double @binary_times_Real64_Real64(double, double)

declare double @checked_binary_times_Real64_Real64(double, double)

declare void @binary_times_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @checked_binary_times_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_times_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_times_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @binary_unitize_Integer8_Integer8(i8, i8)

declare i8 @checked_binary_unitize_Integer8_Integer8(i8, i8)

declare i16 @binary_unitize_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_unitize_Integer16_Integer16(i16, i16)

declare i32 @binary_unitize_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_unitize_Integer32_Integer32(i32, i32)

declare i64 @binary_unitize_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_unitize_Integer64_Integer64(i64, i64)

declare i8 @binary_unitize_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_unitize_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i16 @binary_unitize_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_unitize_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i32 @binary_unitize_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_unitize_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i64 @binary_unitize_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_unitize_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i32 @binary_unitize_Real16_Real16([1 x i32], [1 x i32])

declare i32 @_ZN3wrtL7compareIdN10half_float4halfES2_EENSt9enable_ifIXaasr17is_floating_pointIT_EE5valueooaasr17is_floating_pointIT0_EE5valuentsr10is_complexIT1_EE5valueaasr17is_floating_pointIS6_EE5valuentsr10is_complexIS5_EE5valueEiE4typeERKS4_RKS5_RKS6_(i16, i16)

declare i32 @__cxa_guard_acquire(i32*)

declare void @__cxa_guard_release(i32*)

declare i32 @checked_binary_unitize_Real16_Real16([1 x i32], [1 x i32])

declare i32 @binary_unitize_Real32_Real32(float, float)

declare i32 @_ZN3wrtL7compareIdffEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valueooaasr17is_floating_pointIT0_EE5valuentsr10is_complexIT1_EE5valueaasr17is_floating_pointIS4_EE5valuentsr10is_complexIS3_EE5valueEiE4typeERKS2_RKS3_RKS4_(float, float)

declare i32 @checked_binary_unitize_Real32_Real32(float, float)

declare i32 @binary_unitize_Real64_Real64(double, double)

declare i32 @_ZN3wrtL7compareIdddEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valueooaasr17is_floating_pointIT0_EE5valuentsr10is_complexIT1_EE5valueaasr17is_floating_pointIS4_EE5valuentsr10is_complexIS3_EE5valueEiE4typeERKS2_RKS3_RKS4_(double, double)

declare i32 @checked_binary_unitize_Real64_Real64(double, double)

declare i32 @binary_unitize_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare i32 @checked_binary_unitize_ComplexReal32_ComplexReal32(%"struct.std::complex.203", %"struct.std::complex.203")

declare i32 @binary_unitize_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i32 @checked_binary_unitize_ComplexReal64_ComplexReal64(%"struct.std::complex", %"struct.std::complex")

declare i8 @binary_pow_Integer8_Integer8(i8, i8)

declare %"class.std::basic_ostream"* @_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_i(%"class.std::basic_ostream"*, i8*, i32)

declare %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"*, i8*)

declare i32 @strlen(i8*)

declare i8 @checked_binary_pow_Integer8_Integer8(i8, i8)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIaaLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i8)

declare i8 @binary_pow_Integer8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_pow_Integer8_UnsignedInteger8(i8, i8)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIahLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i8)

declare i8 @binary_pow_Integer8_Integer16(i8, i16)

declare i8 @checked_binary_pow_Integer8_Integer16(i8, i16)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIasLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i16)

declare i8 @binary_pow_Integer8_UnsignedInteger16(i8, i16)

declare i8 @checked_binary_pow_Integer8_UnsignedInteger16(i8, i16)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIatLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i16)

declare i8 @binary_pow_Integer8_Integer32(i8, i32)

declare i8 @checked_binary_pow_Integer8_Integer32(i8, i32)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIaiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i32)

declare i8 @binary_pow_Integer8_UnsignedInteger32(i8, i32)

declare i8 @checked_binary_pow_Integer8_UnsignedInteger32(i8, i32)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIajLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i32)

declare i8 @binary_pow_Integer8_Integer64(i8, i64)

declare i8 @checked_binary_pow_Integer8_Integer64(i8, i64)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIaxLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i64)

declare i8 @binary_pow_Integer8_UnsignedInteger64(i8, i64)

declare i8 @checked_binary_pow_Integer8_UnsignedInteger64(i8, i64)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIayLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i64)

declare i16 @binary_pow_Integer8_Real16(i8, [1 x i32])

declare float @powf(float, float)

declare i16 @checked_binary_pow_Integer8_Real16(i8, [1 x i32])

declare float @binary_pow_Integer8_Real32(i8, float)

declare float @checked_binary_pow_Integer8_Real32(i8, float)

declare void @binary_pow_Integer8_ComplexReal32(float*, float*, i8, float, float)

declare { float, float } @cexpf({ float, float })

declare void @checked_binary_pow_Integer8_ComplexReal32(float*, float*, i8, float, float)

declare double @binary_pow_Integer8_Real64(i8, double)

declare double @checked_binary_pow_Integer8_Real64(i8, double)

declare void @binary_pow_Integer8_ComplexReal64(double*, double*, i8, double, double)

declare void @checked_binary_pow_Integer8_ComplexReal64(double*, double*, i8, double, double)

declare i8 @binary_pow_UnsignedInteger8_Integer8(i8, i8)

declare i8 @checked_binary_pow_UnsignedInteger8_Integer8(i8, i8)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIhaLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i8)

declare i8 @binary_pow_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @checked_binary_pow_UnsignedInteger8_UnsignedInteger8(i8, i8)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIhhLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i8)

declare i8 @binary_pow_UnsignedInteger8_Integer16(i8, i16)

declare i8 @checked_binary_pow_UnsignedInteger8_Integer16(i8, i16)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIhsLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i16)

declare i8 @binary_pow_UnsignedInteger8_UnsignedInteger16(i8, i16)

declare i8 @checked_binary_pow_UnsignedInteger8_UnsignedInteger16(i8, i16)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIhtLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i16)

declare i8 @binary_pow_UnsignedInteger8_Integer32(i8, i32)

declare i8 @checked_binary_pow_UnsignedInteger8_Integer32(i8, i32)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIhiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i32)

declare i8 @binary_pow_UnsignedInteger8_UnsignedInteger32(i8, i32)

declare i8 @checked_binary_pow_UnsignedInteger8_UnsignedInteger32(i8, i32)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIhjLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i32)

declare i8 @binary_pow_UnsignedInteger8_Integer64(i8, i64)

declare i8 @checked_binary_pow_UnsignedInteger8_Integer64(i8, i64)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIhxLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i64)

declare i8 @binary_pow_UnsignedInteger8_UnsignedInteger64(i8, i64)

declare i8 @checked_binary_pow_UnsignedInteger8_UnsignedInteger64(i8, i64)

declare i8 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIhyLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i8, i64)

declare i16 @binary_pow_UnsignedInteger8_Real16(i8, [1 x i32])

declare i16 @checked_binary_pow_UnsignedInteger8_Real16(i8, [1 x i32])

declare float @binary_pow_UnsignedInteger8_Real32(i8, float)

declare float @checked_binary_pow_UnsignedInteger8_Real32(i8, float)

declare void @binary_pow_UnsignedInteger8_ComplexReal32(float*, float*, i8, float, float)

declare void @checked_binary_pow_UnsignedInteger8_ComplexReal32(float*, float*, i8, float, float)

declare double @binary_pow_UnsignedInteger8_Real64(i8, double)

declare double @checked_binary_pow_UnsignedInteger8_Real64(i8, double)

declare void @binary_pow_UnsignedInteger8_ComplexReal64(double*, double*, i8, double, double)

declare void @checked_binary_pow_UnsignedInteger8_ComplexReal64(double*, double*, i8, double, double)

declare i16 @binary_pow_Integer16_Integer8(i16, i8)

declare i16 @checked_binary_pow_Integer16_Integer8(i16, i8)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIsaLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i8)

declare i16 @binary_pow_Integer16_UnsignedInteger8(i16, i8)

declare i16 @checked_binary_pow_Integer16_UnsignedInteger8(i16, i8)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIshLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i8)

declare i16 @binary_pow_Integer16_Integer16(i16, i16)

declare i16 @checked_binary_pow_Integer16_Integer16(i16, i16)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIssLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i16)

declare i16 @binary_pow_Integer16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_pow_Integer16_UnsignedInteger16(i16, i16)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIstLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i16)

declare i16 @binary_pow_Integer16_Integer32(i16, i32)

declare i16 @checked_binary_pow_Integer16_Integer32(i16, i32)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIsiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i32)

declare i16 @binary_pow_Integer16_UnsignedInteger32(i16, i32)

declare i16 @checked_binary_pow_Integer16_UnsignedInteger32(i16, i32)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIsjLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i32)

declare i16 @binary_pow_Integer16_Integer64(i16, i64)

declare i16 @checked_binary_pow_Integer16_Integer64(i16, i64)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIsxLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i64)

declare i16 @binary_pow_Integer16_UnsignedInteger64(i16, i64)

declare i16 @checked_binary_pow_Integer16_UnsignedInteger64(i16, i64)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIsyLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i64)

declare i16 @binary_pow_Integer16_Real16(i16, [1 x i32])

declare i16 @checked_binary_pow_Integer16_Real16(i16, [1 x i32])

declare float @binary_pow_Integer16_Real32(i16, float)

declare float @checked_binary_pow_Integer16_Real32(i16, float)

declare void @binary_pow_Integer16_ComplexReal32(float*, float*, i16, float, float)

declare void @checked_binary_pow_Integer16_ComplexReal32(float*, float*, i16, float, float)

declare double @binary_pow_Integer16_Real64(i16, double)

declare double @checked_binary_pow_Integer16_Real64(i16, double)

declare void @binary_pow_Integer16_ComplexReal64(double*, double*, i16, double, double)

declare void @checked_binary_pow_Integer16_ComplexReal64(double*, double*, i16, double, double)

declare i16 @binary_pow_UnsignedInteger16_Integer8(i16, i8)

declare i16 @checked_binary_pow_UnsignedInteger16_Integer8(i16, i8)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opItaLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i8)

declare i16 @binary_pow_UnsignedInteger16_UnsignedInteger8(i16, i8)

declare i16 @checked_binary_pow_UnsignedInteger16_UnsignedInteger8(i16, i8)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIthLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i8)

declare i16 @binary_pow_UnsignedInteger16_Integer16(i16, i16)

declare i16 @checked_binary_pow_UnsignedInteger16_Integer16(i16, i16)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opItsLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i16)

declare i16 @binary_pow_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @checked_binary_pow_UnsignedInteger16_UnsignedInteger16(i16, i16)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIttLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i16)

declare i16 @binary_pow_UnsignedInteger16_Integer32(i16, i32)

declare i16 @checked_binary_pow_UnsignedInteger16_Integer32(i16, i32)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opItiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i32)

declare i16 @binary_pow_UnsignedInteger16_UnsignedInteger32(i16, i32)

declare i16 @checked_binary_pow_UnsignedInteger16_UnsignedInteger32(i16, i32)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opItjLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i32)

declare i16 @binary_pow_UnsignedInteger16_Integer64(i16, i64)

declare i16 @checked_binary_pow_UnsignedInteger16_Integer64(i16, i64)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opItxLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i64)

declare i16 @binary_pow_UnsignedInteger16_UnsignedInteger64(i16, i64)

declare i16 @checked_binary_pow_UnsignedInteger16_UnsignedInteger64(i16, i64)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opItyLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i16, i64)

declare i16 @binary_pow_UnsignedInteger16_Real16(i16, [1 x i32])

declare i16 @checked_binary_pow_UnsignedInteger16_Real16(i16, [1 x i32])

declare float @binary_pow_UnsignedInteger16_Real32(i16, float)

declare float @checked_binary_pow_UnsignedInteger16_Real32(i16, float)

declare void @binary_pow_UnsignedInteger16_ComplexReal32(float*, float*, i16, float, float)

declare void @checked_binary_pow_UnsignedInteger16_ComplexReal32(float*, float*, i16, float, float)

declare double @binary_pow_UnsignedInteger16_Real64(i16, double)

declare double @checked_binary_pow_UnsignedInteger16_Real64(i16, double)

declare void @binary_pow_UnsignedInteger16_ComplexReal64(double*, double*, i16, double, double)

declare void @checked_binary_pow_UnsignedInteger16_ComplexReal64(double*, double*, i16, double, double)

declare i32 @binary_pow_Integer32_Integer8(i32, i8)

declare i32 @checked_binary_pow_Integer32_Integer8(i32, i8)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIiaLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i8)

declare i32 @binary_pow_Integer32_UnsignedInteger8(i32, i8)

declare i32 @checked_binary_pow_Integer32_UnsignedInteger8(i32, i8)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIihLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i8)

declare i32 @binary_pow_Integer32_Integer16(i32, i16)

declare i32 @checked_binary_pow_Integer32_Integer16(i32, i16)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIisLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i16)

declare i32 @binary_pow_Integer32_UnsignedInteger16(i32, i16)

declare i32 @checked_binary_pow_Integer32_UnsignedInteger16(i32, i16)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIitLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i16)

declare i32 @binary_pow_Integer32_Integer32(i32, i32)

declare i32 @checked_binary_pow_Integer32_Integer32(i32, i32)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIiiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_.208(i32, i32)

declare i32 @binary_pow_Integer32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_pow_Integer32_UnsignedInteger32(i32, i32)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIijLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i32)

declare i32 @binary_pow_Integer32_Integer64(i32, i64)

declare i32 @checked_binary_pow_Integer32_Integer64(i32, i64)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIixLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i64)

declare i32 @binary_pow_Integer32_UnsignedInteger64(i32, i64)

declare i32 @checked_binary_pow_Integer32_UnsignedInteger64(i32, i64)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIiyLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i64)

declare i16 @binary_pow_Integer32_Real16(i32, [1 x i32])

declare i16 @checked_binary_pow_Integer32_Real16(i32, [1 x i32])

declare float @binary_pow_Integer32_Real32(i32, float)

declare float @checked_binary_pow_Integer32_Real32(i32, float)

declare void @binary_pow_Integer32_ComplexReal32(float*, float*, i32, float, float)

declare void @checked_binary_pow_Integer32_ComplexReal32(float*, float*, i32, float, float)

declare double @binary_pow_Integer32_Real64(i32, double)

declare double @checked_binary_pow_Integer32_Real64(i32, double)

declare void @binary_pow_Integer32_ComplexReal64(double*, double*, i32, double, double)

declare void @checked_binary_pow_Integer32_ComplexReal64(double*, double*, i32, double, double)

declare i32 @binary_pow_UnsignedInteger32_Integer8(i32, i8)

declare i32 @checked_binary_pow_UnsignedInteger32_Integer8(i32, i8)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIjaLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i8)

declare i32 @binary_pow_UnsignedInteger32_UnsignedInteger8(i32, i8)

declare i32 @checked_binary_pow_UnsignedInteger32_UnsignedInteger8(i32, i8)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIjhLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i8)

declare i32 @binary_pow_UnsignedInteger32_Integer16(i32, i16)

declare i32 @checked_binary_pow_UnsignedInteger32_Integer16(i32, i16)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIjsLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i16)

declare i32 @binary_pow_UnsignedInteger32_UnsignedInteger16(i32, i16)

declare i32 @checked_binary_pow_UnsignedInteger32_UnsignedInteger16(i32, i16)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIjtLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i16)

declare i32 @binary_pow_UnsignedInteger32_Integer32(i32, i32)

declare i32 @checked_binary_pow_UnsignedInteger32_Integer32(i32, i32)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIjiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i32)

declare i32 @binary_pow_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @checked_binary_pow_UnsignedInteger32_UnsignedInteger32(i32, i32)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIjjLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i32)

declare i32 @binary_pow_UnsignedInteger32_Integer64(i32, i64)

declare i32 @checked_binary_pow_UnsignedInteger32_Integer64(i32, i64)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIjxLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i64)

declare i32 @binary_pow_UnsignedInteger32_UnsignedInteger64(i32, i64)

declare i32 @checked_binary_pow_UnsignedInteger32_UnsignedInteger64(i32, i64)

declare i32 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIjyLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i32, i64)

declare i16 @binary_pow_UnsignedInteger32_Real16(i32, [1 x i32])

declare i16 @checked_binary_pow_UnsignedInteger32_Real16(i32, [1 x i32])

declare float @binary_pow_UnsignedInteger32_Real32(i32, float)

declare float @checked_binary_pow_UnsignedInteger32_Real32(i32, float)

declare void @binary_pow_UnsignedInteger32_ComplexReal32(float*, float*, i32, float, float)

declare void @checked_binary_pow_UnsignedInteger32_ComplexReal32(float*, float*, i32, float, float)

declare double @binary_pow_UnsignedInteger32_Real64(i32, double)

declare double @checked_binary_pow_UnsignedInteger32_Real64(i32, double)

declare void @binary_pow_UnsignedInteger32_ComplexReal64(double*, double*, i32, double, double)

declare void @checked_binary_pow_UnsignedInteger32_ComplexReal64(double*, double*, i32, double, double)

declare i64 @binary_pow_Integer64_Integer8(i64, i8)

declare i64 @checked_binary_pow_Integer64_Integer8(i64, i8)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIxaLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i8)

declare i64 @binary_pow_Integer64_UnsignedInteger8(i64, i8)

declare i64 @checked_binary_pow_Integer64_UnsignedInteger8(i64, i8)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIxhLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i8)

declare i64 @binary_pow_Integer64_Integer16(i64, i16)

declare i64 @checked_binary_pow_Integer64_Integer16(i64, i16)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIxsLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i16)

declare i64 @binary_pow_Integer64_UnsignedInteger16(i64, i16)

declare i64 @checked_binary_pow_Integer64_UnsignedInteger16(i64, i16)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIxtLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i16)

declare i64 @binary_pow_Integer64_Integer32(i64, i32)

declare i64 @checked_binary_pow_Integer64_Integer32(i64, i32)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIxiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i32)

declare i64 @binary_pow_Integer64_UnsignedInteger32(i64, i32)

declare i64 @checked_binary_pow_Integer64_UnsignedInteger32(i64, i32)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIxjLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i32)

declare i64 @binary_pow_Integer64_Integer64(i64, i64)

declare i64 @checked_binary_pow_Integer64_Integer64(i64, i64)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIxxLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i64)

declare i64 @binary_pow_Integer64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_pow_Integer64_UnsignedInteger64(i64, i64)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIxyLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i64)

declare i16 @binary_pow_Integer64_Real16(i64, [1 x i32])

declare i16 @checked_binary_pow_Integer64_Real16(i64, [1 x i32])

declare float @binary_pow_Integer64_Real32(i64, float)

declare float @checked_binary_pow_Integer64_Real32(i64, float)

declare void @binary_pow_Integer64_ComplexReal32(float*, float*, i64, float, float)

declare void @checked_binary_pow_Integer64_ComplexReal32(float*, float*, i64, float, float)

declare double @binary_pow_Integer64_Real64(i64, double)

declare double @checked_binary_pow_Integer64_Real64(i64, double)

declare void @binary_pow_Integer64_ComplexReal64(double*, double*, i64, double, double)

declare void @checked_binary_pow_Integer64_ComplexReal64(double*, double*, i64, double, double)

declare i64 @binary_pow_UnsignedInteger64_Integer8(i64, i8)

declare i64 @checked_binary_pow_UnsignedInteger64_Integer8(i64, i8)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIyaLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i8)

declare i64 @binary_pow_UnsignedInteger64_UnsignedInteger8(i64, i8)

declare i64 @checked_binary_pow_UnsignedInteger64_UnsignedInteger8(i64, i8)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIyhLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i8)

declare i64 @binary_pow_UnsignedInteger64_Integer16(i64, i16)

declare i64 @checked_binary_pow_UnsignedInteger64_Integer16(i64, i16)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIysLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i16)

declare i64 @binary_pow_UnsignedInteger64_UnsignedInteger16(i64, i16)

declare i64 @checked_binary_pow_UnsignedInteger64_UnsignedInteger16(i64, i16)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIytLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i16)

declare i64 @binary_pow_UnsignedInteger64_Integer32(i64, i32)

declare i64 @checked_binary_pow_UnsignedInteger64_Integer32(i64, i32)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIyiLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i32)

declare i64 @binary_pow_UnsignedInteger64_UnsignedInteger32(i64, i32)

declare i64 @checked_binary_pow_UnsignedInteger64_UnsignedInteger32(i64, i32)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIyjLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i32)

declare i64 @binary_pow_UnsignedInteger64_Integer64(i64, i64)

declare i64 @checked_binary_pow_UnsignedInteger64_Integer64(i64, i64)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIyxLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i64)

declare i64 @binary_pow_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @checked_binary_pow_UnsignedInteger64_UnsignedInteger64(i64, i64)

declare i64 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIyyLNS_13runtime_flagsE1EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(i64, i64)

declare i16 @binary_pow_UnsignedInteger64_Real16(i64, [1 x i32])

declare i16 @checked_binary_pow_UnsignedInteger64_Real16(i64, [1 x i32])

declare float @binary_pow_UnsignedInteger64_Real32(i64, float)

declare float @checked_binary_pow_UnsignedInteger64_Real32(i64, float)

declare void @binary_pow_UnsignedInteger64_ComplexReal32(float*, float*, i64, float, float)

declare void @checked_binary_pow_UnsignedInteger64_ComplexReal32(float*, float*, i64, float, float)

declare double @binary_pow_UnsignedInteger64_Real64(i64, double)

declare double @checked_binary_pow_UnsignedInteger64_Real64(i64, double)

declare void @binary_pow_UnsignedInteger64_ComplexReal64(double*, double*, i64, double, double)

declare void @checked_binary_pow_UnsignedInteger64_ComplexReal64(double*, double*, i64, double, double)

declare i16 @binary_pow_Real16_Integer8([1 x i32], i8)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIN10half_float4halfEaLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(i16, i8)

declare i16 @checked_binary_pow_Real16_Integer8([1 x i32], i8)

declare i16 @binary_pow_Real16_UnsignedInteger8([1 x i32], i8)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIN10half_float4halfEhLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(i16, i8)

declare i16 @checked_binary_pow_Real16_UnsignedInteger8([1 x i32], i8)

declare i16 @binary_pow_Real16_Integer16([1 x i32], i16)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIN10half_float4halfEsLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(i16, i16)

declare i16 @checked_binary_pow_Real16_Integer16([1 x i32], i16)

declare i16 @binary_pow_Real16_UnsignedInteger16([1 x i32], i16)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIN10half_float4halfEtLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(i16, i16)

declare i16 @checked_binary_pow_Real16_UnsignedInteger16([1 x i32], i16)

declare i16 @binary_pow_Real16_Integer32([1 x i32], i32)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIN10half_float4halfEiLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(i16, i32)

declare i16 @checked_binary_pow_Real16_Integer32([1 x i32], i32)

declare i16 @binary_pow_Real16_UnsignedInteger32([1 x i32], i32)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIN10half_float4halfEjLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(i16, i32)

declare i16 @checked_binary_pow_Real16_UnsignedInteger32([1 x i32], i32)

declare i16 @binary_pow_Real16_Integer64([1 x i32], i64)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIN10half_float4halfExLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(i16, i64)

declare i16 @checked_binary_pow_Real16_Integer64([1 x i32], i64)

declare i16 @binary_pow_Real16_UnsignedInteger64([1 x i32], i64)

declare i16 @_ZN3wrt9scalar_op6binaryL9pow_xi_opIN10half_float4halfEyLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(i16, i64)

declare i16 @checked_binary_pow_Real16_UnsignedInteger64([1 x i32], i64)

declare i16 @binary_pow_Real16_Real16([1 x i32], [1 x i32])

declare i16 @checked_binary_pow_Real16_Real16([1 x i32], [1 x i32])

declare float @binary_pow_Real16_Real32([1 x i32], float)

declare float @checked_binary_pow_Real16_Real32([1 x i32], float)

declare void @binary_pow_Real16_ComplexReal32(float*, float*, [1 x i32], float, float)

declare void @checked_binary_pow_Real16_ComplexReal32(float*, float*, [1 x i32], float, float)

declare double @binary_pow_Real16_Real64([1 x i32], double)

declare double @checked_binary_pow_Real16_Real64([1 x i32], double)

declare void @binary_pow_Real16_ComplexReal64(double*, double*, [1 x i32], double, double)

declare void @checked_binary_pow_Real16_ComplexReal64(double*, double*, [1 x i32], double, double)

declare float @binary_pow_Real32_Integer8(float, i8)

declare float @_ZN3wrt9scalar_op6binaryL9pow_xi_opIfaLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(float, i8)

declare float @checked_binary_pow_Real32_Integer8(float, i8)

declare float @binary_pow_Real32_UnsignedInteger8(float, i8)

declare float @_ZN3wrt9scalar_op6binaryL9pow_xi_opIfhLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(float, i8)

declare float @checked_binary_pow_Real32_UnsignedInteger8(float, i8)

declare float @binary_pow_Real32_Integer16(float, i16)

declare float @_ZN3wrt9scalar_op6binaryL9pow_xi_opIfsLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(float, i16)

declare float @checked_binary_pow_Real32_Integer16(float, i16)

declare float @binary_pow_Real32_UnsignedInteger16(float, i16)

declare float @_ZN3wrt9scalar_op6binaryL9pow_xi_opIftLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(float, i16)

declare float @checked_binary_pow_Real32_UnsignedInteger16(float, i16)

declare float @binary_pow_Real32_Integer32(float, i32)

declare float @_ZN3wrt9scalar_op6binaryL9pow_xi_opIfiLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(float, i32)

declare float @checked_binary_pow_Real32_Integer32(float, i32)

declare float @binary_pow_Real32_UnsignedInteger32(float, i32)

declare float @_ZN3wrt9scalar_op6binaryL9pow_xi_opIfjLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(float, i32)

declare float @checked_binary_pow_Real32_UnsignedInteger32(float, i32)

declare float @binary_pow_Real32_Integer64(float, i64)

declare float @_ZN3wrt9scalar_op6binaryL9pow_xi_opIfxLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(float, i64)

declare float @checked_binary_pow_Real32_Integer64(float, i64)

declare float @binary_pow_Real32_UnsignedInteger64(float, i64)

declare float @_ZN3wrt9scalar_op6binaryL9pow_xi_opIfyLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(float, i64)

declare float @checked_binary_pow_Real32_UnsignedInteger64(float, i64)

declare float @binary_pow_Real32_Real16(float, [1 x i32])

declare float @checked_binary_pow_Real32_Real16(float, [1 x i32])

declare float @binary_pow_Real32_Real32(float, float)

declare float @checked_binary_pow_Real32_Real32(float, float)

declare void @binary_pow_Real32_ComplexReal32(float*, float*, float, float, float)

declare void @checked_binary_pow_Real32_ComplexReal32(float*, float*, float, float, float)

declare double @binary_pow_Real32_Real64(float, double)

declare double @checked_binary_pow_Real32_Real64(float, double)

declare void @binary_pow_Real32_ComplexReal64(double*, double*, float, double, double)

declare void @checked_binary_pow_Real32_ComplexReal64(double*, double*, float, double, double)

declare void @binary_pow_ComplexReal32_Integer8(float*, float*, float, float, i8)

declare { float, float } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIfEaLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex.203"*, i8*)

declare void @checked_binary_pow_ComplexReal32_Integer8(float*, float*, float, float, i8)

declare float @_ZN3wrt9scalar_op6binaryL6pow_opIfaLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(float*, i8)

declare void @binary_pow_ComplexReal32_UnsignedInteger8(float*, float*, float, float, i8)

declare { float, float } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIfEhLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex.203"*, i8*)

declare void @checked_binary_pow_ComplexReal32_UnsignedInteger8(float*, float*, float, float, i8)

declare void @binary_pow_ComplexReal32_Integer16(float*, float*, float, float, i16)

declare { float, float } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIfEsLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex.203"*, i16*)

declare void @checked_binary_pow_ComplexReal32_Integer16(float*, float*, float, float, i16)

declare float @_ZN3wrt9scalar_op6binaryL6pow_opIfsLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(float*, i16)

declare void @binary_pow_ComplexReal32_UnsignedInteger16(float*, float*, float, float, i16)

declare { float, float } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIfEtLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex.203"*, i16*)

declare void @checked_binary_pow_ComplexReal32_UnsignedInteger16(float*, float*, float, float, i16)

declare void @binary_pow_ComplexReal32_Integer32(float*, float*, float, float, i32)

declare { float, float } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIfEiLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex.203"*, i32*)

declare void @checked_binary_pow_ComplexReal32_Integer32(float*, float*, float, float, i32)

declare float @_ZN3wrt9scalar_op6binaryL6pow_opIfiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(float*, i32)

declare void @binary_pow_ComplexReal32_UnsignedInteger32(float*, float*, float, float, i32)

declare { float, float } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIfEjLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex.203"*, i32*)

declare void @checked_binary_pow_ComplexReal32_UnsignedInteger32(float*, float*, float, float, i32)

declare void @binary_pow_ComplexReal32_Integer64(float*, float*, float, float, i64)

declare { float, float } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIfExLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex.203"*, i64*)

declare void @checked_binary_pow_ComplexReal32_Integer64(float*, float*, float, float, i64)

declare float @_ZN3wrt9scalar_op6binaryL6pow_opIfxLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(float*, i64)

declare void @binary_pow_ComplexReal32_UnsignedInteger64(float*, float*, float, float, i64)

declare { float, float } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIfEyLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex.203"*, i64*)

declare void @checked_binary_pow_ComplexReal32_UnsignedInteger64(float*, float*, float, float, i64)

declare void @binary_pow_ComplexReal32_Real16(float*, float*, float, float, [1 x i32])

declare void @checked_binary_pow_ComplexReal32_Real16(float*, float*, float, float, [1 x i32])

declare void @binary_pow_ComplexReal32_Real32(float*, float*, float, float, float)

declare void @checked_binary_pow_ComplexReal32_Real32(float*, float*, float, float, float)

declare void @binary_pow_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare { float, float } @cpowf({ float, float }, { float, float })

declare void @checked_binary_pow_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float)

declare void @binary_pow_ComplexReal32_Real64(float*, float*, float, float, double)

declare void @checked_binary_pow_ComplexReal32_Real64(float*, float*, float, float, double)

declare void @binary_pow_ComplexReal32_ComplexReal64(double*, double*, float, float, double, double)

declare void @checked_binary_pow_ComplexReal32_ComplexReal64(double*, double*, float, float, double, double)

declare double @binary_pow_Real64_Integer8(double, i8)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdaLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(double, i8)

declare double @checked_binary_pow_Real64_Integer8(double, i8)

declare double @binary_pow_Real64_UnsignedInteger8(double, i8)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdhLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(double, i8)

declare double @checked_binary_pow_Real64_UnsignedInteger8(double, i8)

declare double @binary_pow_Real64_Integer16(double, i16)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdsLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(double, i16)

declare double @checked_binary_pow_Real64_Integer16(double, i16)

declare double @binary_pow_Real64_UnsignedInteger16(double, i16)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdtLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(double, i16)

declare double @checked_binary_pow_Real64_UnsignedInteger16(double, i16)

declare double @binary_pow_Real64_Integer32(double, i32)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdiLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_.209(double, i32)

declare double @checked_binary_pow_Real64_Integer32(double, i32)

declare double @binary_pow_Real64_UnsignedInteger32(double, i32)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdjLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(double, i32)

declare double @checked_binary_pow_Real64_UnsignedInteger32(double, i32)

declare double @binary_pow_Real64_Integer64(double, i64)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdxLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(double, i64)

declare double @checked_binary_pow_Real64_Integer64(double, i64)

declare double @binary_pow_Real64_UnsignedInteger64(double, i64)

declare double @_ZN3wrt9scalar_op6binaryL9pow_xi_opIdyLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS6_RKS5_(double, i64)

declare double @checked_binary_pow_Real64_UnsignedInteger64(double, i64)

declare double @binary_pow_Real64_Real16(double, [1 x i32])

declare double @checked_binary_pow_Real64_Real16(double, [1 x i32])

declare double @binary_pow_Real64_Real32(double, float)

declare double @checked_binary_pow_Real64_Real32(double, float)

declare void @binary_pow_Real64_ComplexReal32(float*, float*, double, float, float)

declare void @checked_binary_pow_Real64_ComplexReal32(float*, float*, double, float, float)

declare double @binary_pow_Real64_Real64(double, double)

declare double @checked_binary_pow_Real64_Real64(double, double)

declare void @binary_pow_Real64_ComplexReal64(double*, double*, double, double, double)

declare void @checked_binary_pow_Real64_ComplexReal64(double*, double*, double, double, double)

declare void @binary_pow_ComplexReal64_Integer8(double*, double*, double, double, i8)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdEaLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex"*, i8*)

declare void @checked_binary_pow_ComplexReal64_Integer8(double*, double*, double, double, i8)

declare double @_ZN3wrt9scalar_op6binaryL6pow_opIdaLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double, i8)

declare void @binary_pow_ComplexReal64_UnsignedInteger8(double*, double*, double, double, i8)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdEhLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex"*, i8*)

declare void @checked_binary_pow_ComplexReal64_UnsignedInteger8(double*, double*, double, double, i8)

declare void @binary_pow_ComplexReal64_Integer16(double*, double*, double, double, i16)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdEsLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex"*, i16*)

declare void @checked_binary_pow_ComplexReal64_Integer16(double*, double*, double, double, i16)

declare double @_ZN3wrt9scalar_op6binaryL6pow_opIdsLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double, i16)

declare void @binary_pow_ComplexReal64_UnsignedInteger16(double*, double*, double, double, i16)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdEtLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex"*, i16*)

declare void @checked_binary_pow_ComplexReal64_UnsignedInteger16(double*, double*, double, double, i16)

declare void @binary_pow_ComplexReal64_Integer32(double*, double*, double, double, i32)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdEiLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_.210(%"struct.std::complex"*, i32*)

declare void @checked_binary_pow_ComplexReal64_Integer32(double*, double*, double, double, i32)

declare double @_ZN3wrt9scalar_op6binaryL6pow_opIdiLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE.211(double, i32)

declare void @binary_pow_ComplexReal64_UnsignedInteger32(double*, double*, double, double, i32)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdEjLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex"*, i32*)

declare void @checked_binary_pow_ComplexReal64_UnsignedInteger32(double*, double*, double, double, i32)

declare void @binary_pow_ComplexReal64_Integer64(double*, double*, double, double, i64)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdExLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex"*, i64*)

declare void @checked_binary_pow_ComplexReal64_Integer64(double*, double*, double, double, i64)

declare double @_ZN3wrt9scalar_op6binaryL6pow_opIdxLNS_13runtime_flagsE1EEENSt9enable_ifIXaasr17is_floating_pointIT_EE5valuesr11is_integralIT0_EE5valueENS1_6detail7op_infoILNS7_2opE20EN7rt_typeIS5_E4typeENSA_IS6_E4typeEE11result_typeEE4typeERKNSF_19first_argument_typeERKNSF_20second_argument_typeE(double, i64)

declare void @binary_pow_ComplexReal64_UnsignedInteger64(double*, double*, double, double, i64)

declare { double, double } @_ZN3wrt9scalar_op6binaryL9pow_xi_opISt7complexIdEyLNS_13runtime_flagsE0EEENSt9enable_ifIXsr11is_integralIT0_EE5valueET_E4typeERKS8_RKS7_(%"struct.std::complex"*, i64*)

declare void @checked_binary_pow_ComplexReal64_UnsignedInteger64(double*, double*, double, double, i64)

declare void @binary_pow_ComplexReal64_Real16(double*, double*, double, double, [1 x i32])

declare void @checked_binary_pow_ComplexReal64_Real16(double*, double*, double, double, [1 x i32])

declare void @binary_pow_ComplexReal64_Real32(double*, double*, double, double, float)

declare void @checked_binary_pow_ComplexReal64_Real32(double*, double*, double, double, float)

declare void @binary_pow_ComplexReal64_ComplexReal32(double*, double*, double, double, float, float)

declare void @checked_binary_pow_ComplexReal64_ComplexReal32(double*, double*, double, double, float, float)

declare void @binary_pow_ComplexReal64_Real64(double*, double*, double, double, double)

declare void @checked_binary_pow_ComplexReal64_Real64(double*, double*, double, double, double)

declare void @binary_pow_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare void @checked_binary_pow_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double)

declare i8 @integer_safe_cast_Integer8_Integer8(i8)

declare i8 @integer_safe_cast_Integer8_UnsignedInteger8(i8)

declare i16 @integer_safe_cast_Integer8_Integer16(i8)

declare i16 @integer_safe_cast_Integer8_UnsignedInteger16(i8)

declare i32 @integer_safe_cast_Integer8_Integer32(i8)

declare i32 @integer_safe_cast_Integer8_UnsignedInteger32(i8)

declare i64 @integer_safe_cast_Integer8_Integer64(i8)

declare i64 @integer_safe_cast_Integer8_UnsignedInteger64(i8)

declare i8 @integer_safe_cast_UnsignedInteger8_Integer8(i8)

declare i8 @integer_safe_cast_UnsignedInteger8_UnsignedInteger8(i8)

declare i16 @integer_safe_cast_UnsignedInteger8_Integer16(i8)

declare i16 @integer_safe_cast_UnsignedInteger8_UnsignedInteger16(i8)

declare i32 @integer_safe_cast_UnsignedInteger8_Integer32(i8)

declare i32 @integer_safe_cast_UnsignedInteger8_UnsignedInteger32(i8)

declare i64 @integer_safe_cast_UnsignedInteger8_Integer64(i8)

declare i64 @integer_safe_cast_UnsignedInteger8_UnsignedInteger64(i8)

declare i8 @integer_safe_cast_Integer16_Integer8(i16)

declare i8 @integer_safe_cast_Integer16_UnsignedInteger8(i16)

declare i16 @integer_safe_cast_Integer16_Integer16(i16)

declare i16 @integer_safe_cast_Integer16_UnsignedInteger16(i16)

declare i32 @integer_safe_cast_Integer16_Integer32(i16)

declare i32 @integer_safe_cast_Integer16_UnsignedInteger32(i16)

declare i64 @integer_safe_cast_Integer16_Integer64(i16)

declare i64 @integer_safe_cast_Integer16_UnsignedInteger64(i16)

declare i8 @integer_safe_cast_UnsignedInteger16_Integer8(i16)

declare i8 @integer_safe_cast_UnsignedInteger16_UnsignedInteger8(i16)

declare i16 @integer_safe_cast_UnsignedInteger16_Integer16(i16)

declare i16 @integer_safe_cast_UnsignedInteger16_UnsignedInteger16(i16)

declare i32 @integer_safe_cast_UnsignedInteger16_Integer32(i16)

declare i32 @integer_safe_cast_UnsignedInteger16_UnsignedInteger32(i16)

declare i64 @integer_safe_cast_UnsignedInteger16_Integer64(i16)

declare i64 @integer_safe_cast_UnsignedInteger16_UnsignedInteger64(i16)

declare i8 @integer_safe_cast_Integer32_Integer8(i32)

declare i8 @integer_safe_cast_Integer32_UnsignedInteger8(i32)

declare i16 @integer_safe_cast_Integer32_Integer16(i32)

declare i16 @integer_safe_cast_Integer32_UnsignedInteger16(i32)

declare i32 @integer_safe_cast_Integer32_Integer32(i32)

declare i32 @integer_safe_cast_Integer32_UnsignedInteger32(i32)

declare i64 @integer_safe_cast_Integer32_Integer64(i32)

declare i64 @integer_safe_cast_Integer32_UnsignedInteger64(i32)

declare i8 @integer_safe_cast_UnsignedInteger32_Integer8(i32)

declare i8 @integer_safe_cast_UnsignedInteger32_UnsignedInteger8(i32)

declare i16 @integer_safe_cast_UnsignedInteger32_Integer16(i32)

declare i16 @integer_safe_cast_UnsignedInteger32_UnsignedInteger16(i32)

declare i32 @integer_safe_cast_UnsignedInteger32_Integer32(i32)

declare i32 @integer_safe_cast_UnsignedInteger32_UnsignedInteger32(i32)

declare i64 @integer_safe_cast_UnsignedInteger32_Integer64(i32)

declare i64 @integer_safe_cast_UnsignedInteger32_UnsignedInteger64(i32)

declare i8 @integer_safe_cast_Integer64_Integer8(i64)

declare i8 @integer_safe_cast_Integer64_UnsignedInteger8(i64)

declare i16 @integer_safe_cast_Integer64_Integer16(i64)

declare i16 @integer_safe_cast_Integer64_UnsignedInteger16(i64)

declare i32 @integer_safe_cast_Integer64_Integer32(i64)

declare i32 @integer_safe_cast_Integer64_UnsignedInteger32(i64)

declare i64 @integer_safe_cast_Integer64_Integer64(i64)

declare i64 @integer_safe_cast_Integer64_UnsignedInteger64(i64)

declare i8 @integer_safe_cast_UnsignedInteger64_Integer8(i64)

declare i8 @integer_safe_cast_UnsignedInteger64_UnsignedInteger8(i64)

declare i16 @integer_safe_cast_UnsignedInteger64_Integer16(i64)

declare i16 @integer_safe_cast_UnsignedInteger64_UnsignedInteger16(i64)

declare i32 @integer_safe_cast_UnsignedInteger64_Integer32(i64)

declare i32 @integer_safe_cast_UnsignedInteger64_UnsignedInteger32(i64)

declare i64 @integer_safe_cast_UnsignedInteger64_Integer64(i64)

declare i64 @integer_safe_cast_UnsignedInteger64_UnsignedInteger64(i64)

declare void @_GLOBAL__sub_I_ternary.cpp()

declare i8 @ternary_adxmy_Integer8_Integer8_Integer8(i8, i8, i8)

declare i8 @checked_ternary_adxmy_Integer8_Integer8_Integer8(i8, i8, i8)

declare i16 @ternary_adxmy_Integer16_Integer16_Integer16(i16, i16, i16)

declare i16 @checked_ternary_adxmy_Integer16_Integer16_Integer16(i16, i16, i16)

declare i32 @ternary_adxmy_Integer32_Integer32_Integer32(i32, i32, i32)

declare i32 @checked_ternary_adxmy_Integer32_Integer32_Integer32(i32, i32, i32)

declare i64 @ternary_adxmy_Integer64_Integer64_Integer64(i64, i64, i64)

declare i64 @checked_ternary_adxmy_Integer64_Integer64_Integer64(i64, i64, i64)

declare i8 @ternary_adxmy_UnsignedInteger8_UnsignedInteger8_UnsignedInteger8(i8, i8, i8)

declare i8 @checked_ternary_adxmy_UnsignedInteger8_UnsignedInteger8_UnsignedInteger8(i8, i8, i8)

declare i16 @ternary_adxmy_UnsignedInteger16_UnsignedInteger16_UnsignedInteger16(i16, i16, i16)

declare i16 @checked_ternary_adxmy_UnsignedInteger16_UnsignedInteger16_UnsignedInteger16(i16, i16, i16)

declare i32 @ternary_adxmy_UnsignedInteger32_UnsignedInteger32_UnsignedInteger32(i32, i32, i32)

declare i32 @checked_ternary_adxmy_UnsignedInteger32_UnsignedInteger32_UnsignedInteger32(i32, i32, i32)

declare i64 @ternary_adxmy_UnsignedInteger64_UnsignedInteger64_UnsignedInteger64(i64, i64, i64)

declare i64 @checked_ternary_adxmy_UnsignedInteger64_UnsignedInteger64_UnsignedInteger64(i64, i64, i64)

declare i16 @ternary_adxmy_Real16_Real16_Real16([1 x i32], [1 x i32], [1 x i32])

declare i16 @checked_ternary_adxmy_Real16_Real16_Real16([1 x i32], [1 x i32], [1 x i32])

declare float @ternary_adxmy_Real32_Real32_Real32(float, float, float)

declare float @checked_ternary_adxmy_Real32_Real32_Real32(float, float, float)

declare double @ternary_adxmy_Real64_Real64_Real64(double, double, double)

declare double @checked_ternary_adxmy_Real64_Real64_Real64(double, double, double)

declare void @ternary_adxmy_ComplexReal32_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float, float, float)

declare void @checked_ternary_adxmy_ComplexReal32_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float, float, float)

declare void @ternary_adxmy_ComplexReal64_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double, double, double)

declare void @checked_ternary_adxmy_ComplexReal64_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double, double, double)

declare i8 @ternary_axmy_Integer8_Integer8_Integer8(i8, i8, i8)

declare i8 @checked_ternary_axmy_Integer8_Integer8_Integer8(i8, i8, i8)

declare i16 @ternary_axmy_Integer16_Integer16_Integer16(i16, i16, i16)

declare i16 @checked_ternary_axmy_Integer16_Integer16_Integer16(i16, i16, i16)

declare i32 @ternary_axmy_Integer32_Integer32_Integer32(i32, i32, i32)

declare i32 @checked_ternary_axmy_Integer32_Integer32_Integer32(i32, i32, i32)

declare i64 @ternary_axmy_Integer64_Integer64_Integer64(i64, i64, i64)

declare i64 @checked_ternary_axmy_Integer64_Integer64_Integer64(i64, i64, i64)

declare i8 @ternary_axmy_UnsignedInteger8_UnsignedInteger8_UnsignedInteger8(i8, i8, i8)

declare i8 @checked_ternary_axmy_UnsignedInteger8_UnsignedInteger8_UnsignedInteger8(i8, i8, i8)

declare i16 @ternary_axmy_UnsignedInteger16_UnsignedInteger16_UnsignedInteger16(i16, i16, i16)

declare i16 @checked_ternary_axmy_UnsignedInteger16_UnsignedInteger16_UnsignedInteger16(i16, i16, i16)

declare i32 @ternary_axmy_UnsignedInteger32_UnsignedInteger32_UnsignedInteger32(i32, i32, i32)

declare i32 @checked_ternary_axmy_UnsignedInteger32_UnsignedInteger32_UnsignedInteger32(i32, i32, i32)

declare i64 @ternary_axmy_UnsignedInteger64_UnsignedInteger64_UnsignedInteger64(i64, i64, i64)

declare i64 @checked_ternary_axmy_UnsignedInteger64_UnsignedInteger64_UnsignedInteger64(i64, i64, i64)

declare i16 @ternary_axmy_Real16_Real16_Real16([1 x i32], [1 x i32], [1 x i32])

declare i16 @checked_ternary_axmy_Real16_Real16_Real16([1 x i32], [1 x i32], [1 x i32])

declare float @ternary_axmy_Real32_Real32_Real32(float, float, float)

declare float @checked_ternary_axmy_Real32_Real32_Real32(float, float, float)

declare double @ternary_axmy_Real64_Real64_Real64(double, double, double)

declare double @checked_ternary_axmy_Real64_Real64_Real64(double, double, double)

declare void @ternary_axmy_ComplexReal32_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float, float, float)

declare void @checked_ternary_axmy_ComplexReal32_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float, float, float)

declare void @ternary_axmy_ComplexReal64_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double, double, double)

declare void @checked_ternary_axmy_ComplexReal64_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double, double, double)

declare i8 @ternary_axpy_Integer8_Integer8_Integer8(i8, i8, i8)

declare i8 @checked_ternary_axpy_Integer8_Integer8_Integer8(i8, i8, i8)

declare i16 @ternary_axpy_Integer16_Integer16_Integer16(i16, i16, i16)

declare i16 @checked_ternary_axpy_Integer16_Integer16_Integer16(i16, i16, i16)

declare i32 @ternary_axpy_Integer32_Integer32_Integer32(i32, i32, i32)

declare i32 @checked_ternary_axpy_Integer32_Integer32_Integer32(i32, i32, i32)

declare i64 @ternary_axpy_Integer64_Integer64_Integer64(i64, i64, i64)

declare i64 @checked_ternary_axpy_Integer64_Integer64_Integer64(i64, i64, i64)

declare i8 @ternary_axpy_UnsignedInteger8_UnsignedInteger8_UnsignedInteger8(i8, i8, i8)

declare i8 @checked_ternary_axpy_UnsignedInteger8_UnsignedInteger8_UnsignedInteger8(i8, i8, i8)

declare i16 @ternary_axpy_UnsignedInteger16_UnsignedInteger16_UnsignedInteger16(i16, i16, i16)

declare i16 @checked_ternary_axpy_UnsignedInteger16_UnsignedInteger16_UnsignedInteger16(i16, i16, i16)

declare i32 @ternary_axpy_UnsignedInteger32_UnsignedInteger32_UnsignedInteger32(i32, i32, i32)

declare i32 @checked_ternary_axpy_UnsignedInteger32_UnsignedInteger32_UnsignedInteger32(i32, i32, i32)

declare i64 @ternary_axpy_UnsignedInteger64_UnsignedInteger64_UnsignedInteger64(i64, i64, i64)

declare i64 @checked_ternary_axpy_UnsignedInteger64_UnsignedInteger64_UnsignedInteger64(i64, i64, i64)

declare i16 @ternary_axpy_Real16_Real16_Real16([1 x i32], [1 x i32], [1 x i32])

declare i16 @checked_ternary_axpy_Real16_Real16_Real16([1 x i32], [1 x i32], [1 x i32])

declare float @ternary_axpy_Real32_Real32_Real32(float, float, float)

declare float @checked_ternary_axpy_Real32_Real32_Real32(float, float, float)

declare double @ternary_axpy_Real64_Real64_Real64(double, double, double)

declare double @checked_ternary_axpy_Real64_Real64_Real64(double, double, double)

declare void @ternary_axpy_ComplexReal32_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float, float, float)

declare void @checked_ternary_axpy_ComplexReal32_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float, float, float)

declare void @ternary_axpy_ComplexReal64_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double, double, double)

declare void @checked_ternary_axpy_ComplexReal64_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double, double, double)

declare i8 @ternary_ymax_Integer8_Integer8_Integer8(i8, i8, i8)

declare i8 @checked_ternary_ymax_Integer8_Integer8_Integer8(i8, i8, i8)

declare i16 @ternary_ymax_Integer16_Integer16_Integer16(i16, i16, i16)

declare i16 @checked_ternary_ymax_Integer16_Integer16_Integer16(i16, i16, i16)

declare i32 @ternary_ymax_Integer32_Integer32_Integer32(i32, i32, i32)

declare i32 @checked_ternary_ymax_Integer32_Integer32_Integer32(i32, i32, i32)

declare i64 @ternary_ymax_Integer64_Integer64_Integer64(i64, i64, i64)

declare i64 @checked_ternary_ymax_Integer64_Integer64_Integer64(i64, i64, i64)

declare i8 @ternary_ymax_UnsignedInteger8_UnsignedInteger8_UnsignedInteger8(i8, i8, i8)

declare i8 @checked_ternary_ymax_UnsignedInteger8_UnsignedInteger8_UnsignedInteger8(i8, i8, i8)

declare i16 @ternary_ymax_UnsignedInteger16_UnsignedInteger16_UnsignedInteger16(i16, i16, i16)

declare i16 @checked_ternary_ymax_UnsignedInteger16_UnsignedInteger16_UnsignedInteger16(i16, i16, i16)

declare i32 @ternary_ymax_UnsignedInteger32_UnsignedInteger32_UnsignedInteger32(i32, i32, i32)

declare i32 @checked_ternary_ymax_UnsignedInteger32_UnsignedInteger32_UnsignedInteger32(i32, i32, i32)

declare i64 @ternary_ymax_UnsignedInteger64_UnsignedInteger64_UnsignedInteger64(i64, i64, i64)

declare i64 @checked_ternary_ymax_UnsignedInteger64_UnsignedInteger64_UnsignedInteger64(i64, i64, i64)

declare i16 @ternary_ymax_Real16_Real16_Real16([1 x i32], [1 x i32], [1 x i32])

declare i16 @checked_ternary_ymax_Real16_Real16_Real16([1 x i32], [1 x i32], [1 x i32])

declare float @ternary_ymax_Real32_Real32_Real32(float, float, float)

declare float @checked_ternary_ymax_Real32_Real32_Real32(float, float, float)

declare double @ternary_ymax_Real64_Real64_Real64(double, double, double)

declare double @checked_ternary_ymax_Real64_Real64_Real64(double, double, double)

declare void @ternary_ymax_ComplexReal32_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float, float, float)

declare void @checked_ternary_ymax_ComplexReal32_ComplexReal32_ComplexReal32(float*, float*, float, float, float, float, float, float)

declare void @ternary_ymax_ComplexReal64_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double, double, double)

declare void @checked_ternary_ymax_ComplexReal64_ComplexReal64_ComplexReal64(double*, double*, double, double, double, double, double, double)

declare void @_GLOBAL__sub_I_fibonacci_table.cpp()

declare void @_GLOBAL__sub_I_gamma_table.cpp()

declare void @_GLOBAL__sub_I_lucasl_table.cpp()

declare void @_GLOBAL__sub_I_unary.cpp()

declare i8 @unary_abs_Integer8(i8)

declare i8 @checked_unary_abs_Integer8(i8)

declare i16 @unary_abs_Integer16(i16)

declare i16 @checked_unary_abs_Integer16(i16)

declare i32 @unary_abs_Integer32(i32)

declare i32 @checked_unary_abs_Integer32(i32)

declare i64 @unary_abs_Integer64(i64)

declare i64 @checked_unary_abs_Integer64(i64)

declare i8 @unary_abs_UnsignedInteger8(i8)

declare i8 @checked_unary_abs_UnsignedInteger8(i8)

declare i16 @unary_abs_UnsignedInteger16(i16)

declare i16 @checked_unary_abs_UnsignedInteger16(i16)

declare i32 @unary_abs_UnsignedInteger32(i32)

declare i32 @checked_unary_abs_UnsignedInteger32(i32)

declare i64 @unary_abs_UnsignedInteger64(i64)

declare i64 @checked_unary_abs_UnsignedInteger64(i64)

declare i16 @unary_abs_Real16([1 x i32])

declare i16 @checked_unary_abs_Real16([1 x i32])

declare float @unary_abs_Real32(float)

declare float @checked_unary_abs_Real32(float)

declare double @unary_abs_Real64(double)

declare double @checked_unary_abs_Real64(double)

declare float @unary_abs_ComplexReal32(float, float)

declare float @checked_unary_abs_ComplexReal32(float, float)

declare double @unary_abs_ComplexReal64(double, double)

declare double @checked_unary_abs_ComplexReal64(double, double)

declare i8 @unary_abssquare_Integer8(i8)

declare i8 @checked_unary_abssquare_Integer8(i8)

declare i16 @unary_abssquare_Integer16(i16)

declare i16 @checked_unary_abssquare_Integer16(i16)

declare i32 @unary_abssquare_Integer32(i32)

declare i32 @checked_unary_abssquare_Integer32(i32)

declare i64 @unary_abssquare_Integer64(i64)

declare i64 @checked_unary_abssquare_Integer64(i64)

declare i8 @unary_abssquare_UnsignedInteger8(i8)

declare i8 @checked_unary_abssquare_UnsignedInteger8(i8)

declare i16 @unary_abssquare_UnsignedInteger16(i16)

declare i16 @checked_unary_abssquare_UnsignedInteger16(i16)

declare i32 @unary_abssquare_UnsignedInteger32(i32)

declare i32 @checked_unary_abssquare_UnsignedInteger32(i32)

declare i64 @unary_abssquare_UnsignedInteger64(i64)

declare i64 @checked_unary_abssquare_UnsignedInteger64(i64)

declare i16 @unary_abssquare_Real16([1 x i32])

declare i16 @checked_unary_abssquare_Real16([1 x i32])

declare float @unary_abssquare_Real32(float)

declare float @checked_unary_abssquare_Real32(float)

declare double @unary_abssquare_Real64(double)

declare double @checked_unary_abssquare_Real64(double)

declare float @unary_abssquare_ComplexReal32(%"struct.std::complex.203")

declare float @checked_unary_abssquare_ComplexReal32(%"struct.std::complex.203")

declare double @unary_abssquare_ComplexReal64(%"struct.std::complex")

declare double @checked_unary_abssquare_ComplexReal64(%"struct.std::complex")

declare i8 @unary_acos_Integer8(i8)

declare i8 @checked_unary_acos_Integer8(i8)

declare i16 @unary_acos_Integer16(i16)

declare i16 @checked_unary_acos_Integer16(i16)

declare i32 @unary_acos_Integer32(i32)

declare i32 @checked_unary_acos_Integer32(i32)

declare i64 @unary_acos_Integer64(i64)

declare i64 @checked_unary_acos_Integer64(i64)

declare i8 @unary_acos_UnsignedInteger8(i8)

declare i8 @checked_unary_acos_UnsignedInteger8(i8)

declare i16 @unary_acos_UnsignedInteger16(i16)

declare i16 @checked_unary_acos_UnsignedInteger16(i16)

declare i32 @unary_acos_UnsignedInteger32(i32)

declare i32 @checked_unary_acos_UnsignedInteger32(i32)

declare i64 @unary_acos_UnsignedInteger64(i64)

declare i64 @checked_unary_acos_UnsignedInteger64(i64)

declare i16 @unary_acos_Real16([1 x i32])

declare float @acosf(float)

declare i16 @checked_unary_acos_Real16([1 x i32])

declare float @unary_acos_Real32(float)

declare float @checked_unary_acos_Real32(float)

declare double @unary_acos_Real64(double)

declare double @checked_unary_acos_Real64(double)

declare void @unary_acos_ComplexReal32(float*, float*, float, float)

declare { float, float } @_ZN3wrt9scalar_op5unaryL8asinh_opISt7complexIfELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE13EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(float, float)

declare float @frexpf(float, i32*)

declare float @hypotf(float, float)

declare { float, float } @_ZN3wrt9scalar_op5unaryL8log1p_opISt7complexIfELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE47EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(float, float)

declare float @log1pf(float)

declare void @checked_unary_acos_ComplexReal32(float*, float*, float, float)

declare void @unary_acos_ComplexReal64(double*, double*, double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8asinh_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE13EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.238(double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8log1p_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE47EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.239(double, double)

declare void @checked_unary_acos_ComplexReal64(double*, double*, double, double)

declare i8 @unary_acosh_Integer8(i8)

declare i8 @checked_unary_acosh_Integer8(i8)

declare i16 @unary_acosh_Integer16(i16)

declare i16 @checked_unary_acosh_Integer16(i16)

declare i32 @unary_acosh_Integer32(i32)

declare i32 @checked_unary_acosh_Integer32(i32)

declare i64 @unary_acosh_Integer64(i64)

declare i64 @checked_unary_acosh_Integer64(i64)

declare i8 @unary_acosh_UnsignedInteger8(i8)

declare i8 @checked_unary_acosh_UnsignedInteger8(i8)

declare i16 @unary_acosh_UnsignedInteger16(i16)

declare i16 @checked_unary_acosh_UnsignedInteger16(i16)

declare i32 @unary_acosh_UnsignedInteger32(i32)

declare i32 @checked_unary_acosh_UnsignedInteger32(i32)

declare i64 @unary_acosh_UnsignedInteger64(i64)

declare i64 @checked_unary_acosh_UnsignedInteger64(i64)

declare i16 @unary_acosh_Real16([1 x i32])

declare float @acoshf(float)

declare i16 @checked_unary_acosh_Real16([1 x i32])

declare float @unary_acosh_Real32(float)

declare float @checked_unary_acosh_Real32(float)

declare double @unary_acosh_Real64(double)

declare double @checked_unary_acosh_Real64(double)

declare void @unary_acosh_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_acosh_ComplexReal32(float*, float*, float, float)

declare void @unary_acosh_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_acosh_ComplexReal64(double*, double*, double, double)

declare i8 @unary_acot_Integer8(i8)

declare i8 @checked_unary_acot_Integer8(i8)

declare i16 @unary_acot_Integer16(i16)

declare i16 @checked_unary_acot_Integer16(i16)

declare i32 @unary_acot_Integer32(i32)

declare i32 @checked_unary_acot_Integer32(i32)

declare i64 @unary_acot_Integer64(i64)

declare i64 @checked_unary_acot_Integer64(i64)

declare i8 @unary_acot_UnsignedInteger8(i8)

declare i8 @checked_unary_acot_UnsignedInteger8(i8)

declare i16 @unary_acot_UnsignedInteger16(i16)

declare i16 @checked_unary_acot_UnsignedInteger16(i16)

declare i32 @unary_acot_UnsignedInteger32(i32)

declare i32 @checked_unary_acot_UnsignedInteger32(i32)

declare i64 @unary_acot_UnsignedInteger64(i64)

declare i64 @checked_unary_acot_UnsignedInteger64(i64)

declare i16 @unary_acot_Real16([1 x i32])

declare float @atanf(float)

declare i16 @checked_unary_acot_Real16([1 x i32])

declare float @unary_acot_Real32(float)

declare float @checked_unary_acot_Real32(float)

declare double @unary_acot_Real64(double)

declare double @checked_unary_acot_Real64(double)

declare void @unary_acot_ComplexReal32(float*, float*, float, float)

declare { float, float } @_ZN3wrt9scalar_op5unaryL8atanh_opISt7complexIfELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE15EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(%"struct.std::complex.203"*)

declare { float, float } @_ZN3wrt9scalar_op5unaryL8rsqrt_opISt7complexIfELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE64EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.241(float, float)

declare void @checked_unary_acot_ComplexReal32(float*, float*, float, float)

declare void @unary_acot_ComplexReal64(double*, double*, double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8atanh_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE15EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.245(%"struct.std::complex"*)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8rsqrt_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE64EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.246(double, double)

declare void @checked_unary_acot_ComplexReal64(double*, double*, double, double)

declare i8 @unary_acoth_Integer8(i8)

declare i8 @checked_unary_acoth_Integer8(i8)

declare i16 @unary_acoth_Integer16(i16)

declare i16 @checked_unary_acoth_Integer16(i16)

declare i32 @unary_acoth_Integer32(i32)

declare i32 @checked_unary_acoth_Integer32(i32)

declare i64 @unary_acoth_Integer64(i64)

declare i64 @checked_unary_acoth_Integer64(i64)

declare i8 @unary_acoth_UnsignedInteger8(i8)

declare i8 @checked_unary_acoth_UnsignedInteger8(i8)

declare i16 @unary_acoth_UnsignedInteger16(i16)

declare i16 @checked_unary_acoth_UnsignedInteger16(i16)

declare i32 @unary_acoth_UnsignedInteger32(i32)

declare i32 @checked_unary_acoth_UnsignedInteger32(i32)

declare i64 @unary_acoth_UnsignedInteger64(i64)

declare i64 @checked_unary_acoth_UnsignedInteger64(i64)

declare i16 @unary_acoth_Real16([1 x i32])

declare float @atanhf(float)

declare i16 @checked_unary_acoth_Real16([1 x i32])

declare float @unary_acoth_Real32(float)

declare float @checked_unary_acoth_Real32(float)

declare double @unary_acoth_Real64(double)

declare double @checked_unary_acoth_Real64(double)

declare void @unary_acoth_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_acoth_ComplexReal32(float*, float*, float, float)

declare void @unary_acoth_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_acoth_ComplexReal64(double*, double*, double, double)

declare i8 @unary_acsc_Integer8(i8)

declare i8 @checked_unary_acsc_Integer8(i8)

declare i16 @unary_acsc_Integer16(i16)

declare i16 @checked_unary_acsc_Integer16(i16)

declare i32 @unary_acsc_Integer32(i32)

declare i32 @checked_unary_acsc_Integer32(i32)

declare i64 @unary_acsc_Integer64(i64)

declare i64 @checked_unary_acsc_Integer64(i64)

declare i8 @unary_acsc_UnsignedInteger8(i8)

declare i8 @checked_unary_acsc_UnsignedInteger8(i8)

declare i16 @unary_acsc_UnsignedInteger16(i16)

declare i16 @checked_unary_acsc_UnsignedInteger16(i16)

declare i32 @unary_acsc_UnsignedInteger32(i32)

declare i32 @checked_unary_acsc_UnsignedInteger32(i32)

declare i64 @unary_acsc_UnsignedInteger64(i64)

declare i64 @checked_unary_acsc_UnsignedInteger64(i64)

declare i16 @unary_acsc_Real16([1 x i32])

declare float @asinf(float)

declare i16 @checked_unary_acsc_Real16([1 x i32])

declare float @unary_acsc_Real32(float)

declare float @checked_unary_acsc_Real32(float)

declare double @unary_acsc_Real64(double)

declare double @checked_unary_acsc_Real64(double)

declare void @unary_acsc_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_acsc_ComplexReal32(float*, float*, float, float)

declare void @unary_acsc_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_acsc_ComplexReal64(double*, double*, double, double)

declare i8 @unary_acsch_Integer8(i8)

declare i8 @checked_unary_acsch_Integer8(i8)

declare i16 @unary_acsch_Integer16(i16)

declare i16 @checked_unary_acsch_Integer16(i16)

declare i32 @unary_acsch_Integer32(i32)

declare i32 @checked_unary_acsch_Integer32(i32)

declare i64 @unary_acsch_Integer64(i64)

declare i64 @checked_unary_acsch_Integer64(i64)

declare i8 @unary_acsch_UnsignedInteger8(i8)

declare i8 @checked_unary_acsch_UnsignedInteger8(i8)

declare i16 @unary_acsch_UnsignedInteger16(i16)

declare i16 @checked_unary_acsch_UnsignedInteger16(i16)

declare i32 @unary_acsch_UnsignedInteger32(i32)

declare i32 @checked_unary_acsch_UnsignedInteger32(i32)

declare i64 @unary_acsch_UnsignedInteger64(i64)

declare i64 @checked_unary_acsch_UnsignedInteger64(i64)

declare i16 @unary_acsch_Real16([1 x i32])

declare float @asinhf(float)

declare i16 @checked_unary_acsch_Real16([1 x i32])

declare float @unary_acsch_Real32(float)

declare float @checked_unary_acsch_Real32(float)

declare double @unary_acsch_Real64(double)

declare double @checked_unary_acsch_Real64(double)

declare void @unary_acsch_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_acsch_ComplexReal32(float*, float*, float, float)

declare void @unary_acsch_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_acsch_ComplexReal64(double*, double*, double, double)

declare i8 @unary_arg_Integer8(i8)

declare i8 @checked_unary_arg_Integer8(i8)

declare i16 @unary_arg_Integer16(i16)

declare i16 @checked_unary_arg_Integer16(i16)

declare i32 @unary_arg_Integer32(i32)

declare i32 @checked_unary_arg_Integer32(i32)

declare i64 @unary_arg_Integer64(i64)

declare i64 @checked_unary_arg_Integer64(i64)

declare i8 @unary_arg_UnsignedInteger8(i8)

declare i8 @checked_unary_arg_UnsignedInteger8(i8)

declare i16 @unary_arg_UnsignedInteger16(i16)

declare i16 @checked_unary_arg_UnsignedInteger16(i16)

declare i32 @unary_arg_UnsignedInteger32(i32)

declare i32 @checked_unary_arg_UnsignedInteger32(i32)

declare i64 @unary_arg_UnsignedInteger64(i64)

declare i64 @checked_unary_arg_UnsignedInteger64(i64)

declare i32 @unary_arg_Real16([1 x i32])

declare i32 @checked_unary_arg_Real16([1 x i32])

declare i32 @unary_arg_Real32(float)

declare i32 @checked_unary_arg_Real32(float)

declare i32 @unary_arg_Real64(double)

declare i32 @checked_unary_arg_Real64(double)

declare float @unary_arg_ComplexReal32(float, float)

declare float @cargf({ float, float })

declare float @checked_unary_arg_ComplexReal32(float, float)

declare double @unary_arg_ComplexReal64(double, double)

declare double @checked_unary_arg_ComplexReal64(double, double)

declare i8 @unary_asec_Integer8(i8)

declare i8 @checked_unary_asec_Integer8(i8)

declare i16 @unary_asec_Integer16(i16)

declare i16 @checked_unary_asec_Integer16(i16)

declare i32 @unary_asec_Integer32(i32)

declare i32 @checked_unary_asec_Integer32(i32)

declare i64 @unary_asec_Integer64(i64)

declare i64 @checked_unary_asec_Integer64(i64)

declare i8 @unary_asec_UnsignedInteger8(i8)

declare i8 @checked_unary_asec_UnsignedInteger8(i8)

declare i16 @unary_asec_UnsignedInteger16(i16)

declare i16 @checked_unary_asec_UnsignedInteger16(i16)

declare i32 @unary_asec_UnsignedInteger32(i32)

declare i32 @checked_unary_asec_UnsignedInteger32(i32)

declare i64 @unary_asec_UnsignedInteger64(i64)

declare i64 @checked_unary_asec_UnsignedInteger64(i64)

declare i16 @unary_asec_Real16([1 x i32])

declare i16 @checked_unary_asec_Real16([1 x i32])

declare float @unary_asec_Real32(float)

declare float @checked_unary_asec_Real32(float)

declare double @unary_asec_Real64(double)

declare double @checked_unary_asec_Real64(double)

declare void @unary_asec_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_asec_ComplexReal32(float*, float*, float, float)

declare void @unary_asec_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_asec_ComplexReal64(double*, double*, double, double)

declare i8 @unary_asech_Integer8(i8)

declare i8 @checked_unary_asech_Integer8(i8)

declare i16 @unary_asech_Integer16(i16)

declare i16 @checked_unary_asech_Integer16(i16)

declare i32 @unary_asech_Integer32(i32)

declare i32 @checked_unary_asech_Integer32(i32)

declare i64 @unary_asech_Integer64(i64)

declare i64 @checked_unary_asech_Integer64(i64)

declare i8 @unary_asech_UnsignedInteger8(i8)

declare i8 @checked_unary_asech_UnsignedInteger8(i8)

declare i16 @unary_asech_UnsignedInteger16(i16)

declare i16 @checked_unary_asech_UnsignedInteger16(i16)

declare i32 @unary_asech_UnsignedInteger32(i32)

declare i32 @checked_unary_asech_UnsignedInteger32(i32)

declare i64 @unary_asech_UnsignedInteger64(i64)

declare i64 @checked_unary_asech_UnsignedInteger64(i64)

declare i16 @unary_asech_Real16([1 x i32])

declare i16 @checked_unary_asech_Real16([1 x i32])

declare float @unary_asech_Real32(float)

declare float @checked_unary_asech_Real32(float)

declare double @unary_asech_Real64(double)

declare double @checked_unary_asech_Real64(double)

declare void @unary_asech_ComplexReal32(float*, float*, float, float)

declare { float, float } @_ZN3wrt9scalar_op5unaryL8acosh_opISt7complexIfELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE4EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE(float, float)

declare void @checked_unary_asech_ComplexReal32(float*, float*, float, float)

declare void @unary_asech_ComplexReal64(double*, double*, double, double)

declare { double, double } @_ZN3wrt9scalar_op5unaryL8acosh_opISt7complexIdELNS_13runtime_flagsE0EEENSt9enable_ifIXsr10is_complexIT_EE5valueENS1_6detail7op_infoILNS8_2opE4EN7rt_typeIS7_E4typeEE11result_typeEE4typeERKNSE_13argument_typeE.248(double, double)

declare void @checked_unary_asech_ComplexReal64(double*, double*, double, double)

declare i8 @unary_asin_Integer8(i8)

declare i8 @checked_unary_asin_Integer8(i8)

declare i16 @unary_asin_Integer16(i16)

declare i16 @checked_unary_asin_Integer16(i16)

declare i32 @unary_asin_Integer32(i32)

declare i32 @checked_unary_asin_Integer32(i32)

declare i64 @unary_asin_Integer64(i64)

declare i64 @checked_unary_asin_Integer64(i64)

declare i8 @unary_asin_UnsignedInteger8(i8)

declare i8 @checked_unary_asin_UnsignedInteger8(i8)

declare i16 @unary_asin_UnsignedInteger16(i16)

declare i16 @checked_unary_asin_UnsignedInteger16(i16)

declare i32 @unary_asin_UnsignedInteger32(i32)

declare i32 @checked_unary_asin_UnsignedInteger32(i32)

declare i64 @unary_asin_UnsignedInteger64(i64)

declare i64 @checked_unary_asin_UnsignedInteger64(i64)

declare i16 @unary_asin_Real16([1 x i32])

declare i16 @checked_unary_asin_Real16([1 x i32])

declare float @unary_asin_Real32(float)

declare float @checked_unary_asin_Real32(float)

declare double @unary_asin_Real64(double)

declare double @checked_unary_asin_Real64(double)

declare void @unary_asin_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_asin_ComplexReal32(float*, float*, float, float)

declare void @unary_asin_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_asin_ComplexReal64(double*, double*, double, double)

declare i8 @unary_asinh_Integer8(i8)

declare i8 @checked_unary_asinh_Integer8(i8)

declare i16 @unary_asinh_Integer16(i16)

declare i16 @checked_unary_asinh_Integer16(i16)

declare i32 @unary_asinh_Integer32(i32)

declare i32 @checked_unary_asinh_Integer32(i32)

declare i64 @unary_asinh_Integer64(i64)

declare i64 @checked_unary_asinh_Integer64(i64)

declare i8 @unary_asinh_UnsignedInteger8(i8)

declare i8 @checked_unary_asinh_UnsignedInteger8(i8)

declare i16 @unary_asinh_UnsignedInteger16(i16)

declare i16 @checked_unary_asinh_UnsignedInteger16(i16)

declare i32 @unary_asinh_UnsignedInteger32(i32)

declare i32 @checked_unary_asinh_UnsignedInteger32(i32)

declare i64 @unary_asinh_UnsignedInteger64(i64)

declare i64 @checked_unary_asinh_UnsignedInteger64(i64)

declare i16 @unary_asinh_Real16([1 x i32])

declare i16 @checked_unary_asinh_Real16([1 x i32])

declare float @unary_asinh_Real32(float)

declare float @checked_unary_asinh_Real32(float)

declare double @unary_asinh_Real64(double)

declare double @checked_unary_asinh_Real64(double)

declare void @unary_asinh_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_asinh_ComplexReal32(float*, float*, float, float)

declare void @unary_asinh_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_asinh_ComplexReal64(double*, double*, double, double)

declare i8 @unary_atan_Integer8(i8)

declare i8 @checked_unary_atan_Integer8(i8)

declare i16 @unary_atan_Integer16(i16)

declare i16 @checked_unary_atan_Integer16(i16)

declare i32 @unary_atan_Integer32(i32)

declare i32 @checked_unary_atan_Integer32(i32)

declare i64 @unary_atan_Integer64(i64)

declare i64 @checked_unary_atan_Integer64(i64)

declare i8 @unary_atan_UnsignedInteger8(i8)

declare i8 @checked_unary_atan_UnsignedInteger8(i8)

declare i16 @unary_atan_UnsignedInteger16(i16)

declare i16 @checked_unary_atan_UnsignedInteger16(i16)

declare i32 @unary_atan_UnsignedInteger32(i32)

declare i32 @checked_unary_atan_UnsignedInteger32(i32)

declare i64 @unary_atan_UnsignedInteger64(i64)

declare i64 @checked_unary_atan_UnsignedInteger64(i64)

declare i16 @unary_atan_Real16([1 x i32])

declare i16 @checked_unary_atan_Real16([1 x i32])

declare float @unary_atan_Real32(float)

declare float @checked_unary_atan_Real32(float)

declare double @unary_atan_Real64(double)

declare double @checked_unary_atan_Real64(double)

declare void @unary_atan_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_atan_ComplexReal32(float*, float*, float, float)

declare void @unary_atan_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_atan_ComplexReal64(double*, double*, double, double)

declare i8 @unary_atanh_Integer8(i8)

declare i8 @checked_unary_atanh_Integer8(i8)

declare i16 @unary_atanh_Integer16(i16)

declare i16 @checked_unary_atanh_Integer16(i16)

declare i32 @unary_atanh_Integer32(i32)

declare i32 @checked_unary_atanh_Integer32(i32)

declare i64 @unary_atanh_Integer64(i64)

declare i64 @checked_unary_atanh_Integer64(i64)

declare i8 @unary_atanh_UnsignedInteger8(i8)

declare i8 @checked_unary_atanh_UnsignedInteger8(i8)

declare i16 @unary_atanh_UnsignedInteger16(i16)

declare i16 @checked_unary_atanh_UnsignedInteger16(i16)

declare i32 @unary_atanh_UnsignedInteger32(i32)

declare i32 @checked_unary_atanh_UnsignedInteger32(i32)

declare i64 @unary_atanh_UnsignedInteger64(i64)

declare i64 @checked_unary_atanh_UnsignedInteger64(i64)

declare i16 @unary_atanh_Real16([1 x i32])

declare i16 @checked_unary_atanh_Real16([1 x i32])

declare float @unary_atanh_Real32(float)

declare float @checked_unary_atanh_Real32(float)

declare double @unary_atanh_Real64(double)

declare double @checked_unary_atanh_Real64(double)

declare void @unary_atanh_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_atanh_ComplexReal32(float*, float*, float, float)

declare void @unary_atanh_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_atanh_ComplexReal64(double*, double*, double, double)

declare i8 @unary_bit_count_Integer8(i8)

declare i8 @checked_unary_bit_count_Integer8(i8)

declare i16 @unary_bit_count_Integer16(i16)

declare i16 @checked_unary_bit_count_Integer16(i16)

declare i32 @unary_bit_count_Integer32(i32)

declare i32 @checked_unary_bit_count_Integer32(i32)

declare i64 @unary_bit_count_Integer64(i64)

; Function Attrs: nounwind readnone speculatable
declare i64 @llvm.ctpop.i64(i64) #0

declare i64 @checked_unary_bit_count_Integer64(i64)

declare i8 @unary_bit_count_UnsignedInteger8(i8)

declare i8 @checked_unary_bit_count_UnsignedInteger8(i8)

declare i16 @unary_bit_count_UnsignedInteger16(i16)

declare i16 @checked_unary_bit_count_UnsignedInteger16(i16)

declare i32 @unary_bit_count_UnsignedInteger32(i32)

declare i32 @checked_unary_bit_count_UnsignedInteger32(i32)

declare i64 @unary_bit_count_UnsignedInteger64(i64)

declare i64 @checked_unary_bit_count_UnsignedInteger64(i64)

declare i16 @unary_bit_count_Real16([1 x i32])

declare i16 @checked_unary_bit_count_Real16([1 x i32])

declare float @unary_bit_count_Real32(float)

declare float @checked_unary_bit_count_Real32(float)

declare double @unary_bit_count_Real64(double)

declare double @checked_unary_bit_count_Real64(double)

declare %"struct.std::complex.203" @unary_bit_count_ComplexReal32(%"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_unary_bit_count_ComplexReal32(%"struct.std::complex.203")

declare %"struct.std::complex" @unary_bit_count_ComplexReal64(%"struct.std::complex")

declare %"struct.std::complex" @checked_unary_bit_count_ComplexReal64(%"struct.std::complex")

declare i8 @unary_bit_length_Integer8(i8)

declare i8 @checked_unary_bit_length_Integer8(i8)

declare i16 @unary_bit_length_Integer16(i16)

declare i16 @checked_unary_bit_length_Integer16(i16)

declare i32 @unary_bit_length_Integer32(i32)

declare i32 @checked_unary_bit_length_Integer32(i32)

declare i64 @unary_bit_length_Integer64(i64)

declare i64 @checked_unary_bit_length_Integer64(i64)

declare i8 @unary_bit_length_UnsignedInteger8(i8)

declare i8 @checked_unary_bit_length_UnsignedInteger8(i8)

declare i16 @unary_bit_length_UnsignedInteger16(i16)

declare i16 @checked_unary_bit_length_UnsignedInteger16(i16)

declare i32 @unary_bit_length_UnsignedInteger32(i32)

declare i32 @checked_unary_bit_length_UnsignedInteger32(i32)

declare i64 @unary_bit_length_UnsignedInteger64(i64)

declare i64 @checked_unary_bit_length_UnsignedInteger64(i64)

declare i16 @unary_bit_length_Real16([1 x i32])

declare i16 @checked_unary_bit_length_Real16([1 x i32])

declare float @unary_bit_length_Real32(float)

declare float @checked_unary_bit_length_Real32(float)

declare double @unary_bit_length_Real64(double)

declare double @checked_unary_bit_length_Real64(double)

declare %"struct.std::complex.203" @unary_bit_length_ComplexReal32(%"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_unary_bit_length_ComplexReal32(%"struct.std::complex.203")

declare %"struct.std::complex" @unary_bit_length_ComplexReal64(%"struct.std::complex")

declare %"struct.std::complex" @checked_unary_bit_length_ComplexReal64(%"struct.std::complex")

declare i8 @unary_bit_not_Integer8(i8)

declare i8 @checked_unary_bit_not_Integer8(i8)

declare i16 @unary_bit_not_Integer16(i16)

declare i16 @checked_unary_bit_not_Integer16(i16)

declare i32 @unary_bit_not_Integer32(i32)

declare i32 @checked_unary_bit_not_Integer32(i32)

declare i64 @unary_bit_not_Integer64(i64)

declare i64 @checked_unary_bit_not_Integer64(i64)

declare i8 @unary_bit_not_UnsignedInteger8(i8)

declare i8 @checked_unary_bit_not_UnsignedInteger8(i8)

declare i16 @unary_bit_not_UnsignedInteger16(i16)

declare i16 @checked_unary_bit_not_UnsignedInteger16(i16)

declare i32 @unary_bit_not_UnsignedInteger32(i32)

declare i32 @checked_unary_bit_not_UnsignedInteger32(i32)

declare i64 @unary_bit_not_UnsignedInteger64(i64)

declare i64 @checked_unary_bit_not_UnsignedInteger64(i64)

declare i16 @unary_bit_not_Real16([1 x i32])

declare i16 @checked_unary_bit_not_Real16([1 x i32])

declare float @unary_bit_not_Real32(float)

declare float @checked_unary_bit_not_Real32(float)

declare double @unary_bit_not_Real64(double)

declare double @checked_unary_bit_not_Real64(double)

declare %"struct.std::complex.203" @unary_bit_not_ComplexReal32(%"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_unary_bit_not_ComplexReal32(%"struct.std::complex.203")

declare %"struct.std::complex" @unary_bit_not_ComplexReal64(%"struct.std::complex")

declare %"struct.std::complex" @checked_unary_bit_not_ComplexReal64(%"struct.std::complex")

declare i8 @unary_cbrt_Integer8(i8)

declare i8 @checked_unary_cbrt_Integer8(i8)

declare i16 @unary_cbrt_Integer16(i16)

declare i16 @checked_unary_cbrt_Integer16(i16)

declare i32 @unary_cbrt_Integer32(i32)

declare i32 @checked_unary_cbrt_Integer32(i32)

declare i64 @unary_cbrt_Integer64(i64)

declare i64 @checked_unary_cbrt_Integer64(i64)

declare i8 @unary_cbrt_UnsignedInteger8(i8)

declare i8 @checked_unary_cbrt_UnsignedInteger8(i8)

declare i16 @unary_cbrt_UnsignedInteger16(i16)

declare i16 @checked_unary_cbrt_UnsignedInteger16(i16)

declare i32 @unary_cbrt_UnsignedInteger32(i32)

declare i32 @checked_unary_cbrt_UnsignedInteger32(i32)

declare i64 @unary_cbrt_UnsignedInteger64(i64)

declare i64 @checked_unary_cbrt_UnsignedInteger64(i64)

declare i16 @unary_cbrt_Real16([1 x i32])

declare float @cbrtf(float)

declare i16 @checked_unary_cbrt_Real16([1 x i32])

declare float @unary_cbrt_Real32(float)

declare float @checked_unary_cbrt_Real32(float)

declare double @unary_cbrt_Real64(double)

declare double @checked_unary_cbrt_Real64(double)

declare %"struct.std::complex.203" @unary_cbrt_ComplexReal32(%"struct.std::complex.203")

declare %"struct.std::complex.203" @checked_unary_cbrt_ComplexReal32(%"struct.std::complex.203")

declare %"struct.std::complex" @unary_cbrt_ComplexReal64(%"struct.std::complex")

declare %"struct.std::complex" @checked_unary_cbrt_ComplexReal64(%"struct.std::complex")

declare i8 @unary_ceiling_Integer8(i8)

declare i8 @checked_unary_ceiling_Integer8(i8)

declare i16 @unary_ceiling_Integer16(i16)

declare i16 @checked_unary_ceiling_Integer16(i16)

declare i32 @unary_ceiling_Integer32(i32)

declare i32 @checked_unary_ceiling_Integer32(i32)

declare i64 @unary_ceiling_Integer64(i64)

declare i64 @checked_unary_ceiling_Integer64(i64)

declare i8 @unary_ceiling_UnsignedInteger8(i8)

declare i8 @checked_unary_ceiling_UnsignedInteger8(i8)

declare i16 @unary_ceiling_UnsignedInteger16(i16)

declare i16 @checked_unary_ceiling_UnsignedInteger16(i16)

declare i32 @unary_ceiling_UnsignedInteger32(i32)

declare i32 @checked_unary_ceiling_UnsignedInteger32(i32)

declare i64 @unary_ceiling_UnsignedInteger64(i64)

declare i64 @checked_unary_ceiling_UnsignedInteger64(i64)

declare i32 @unary_ceiling_Real16([1 x i32])

declare i32 @checked_unary_ceiling_Real16([1 x i32])

declare i32 @unary_ceiling_Real32(float)

; Function Attrs: nounwind readnone speculatable
declare float @llvm.ceil.f32(float) #0

declare i32 @checked_unary_ceiling_Real32(float)

declare i32 @unary_ceiling_Real64(double)

declare i32 @checked_unary_ceiling_Real64(double)

declare i32 @unary_ceiling_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_ceiling_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_ceiling_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_ceiling_ComplexReal64(%"struct.std::complex")

declare i8 @unary_conj_Integer8(i8)

declare i8 @checked_unary_conj_Integer8(i8)

declare i16 @unary_conj_Integer16(i16)

declare i16 @checked_unary_conj_Integer16(i16)

declare i32 @unary_conj_Integer32(i32)

declare i32 @checked_unary_conj_Integer32(i32)

declare i64 @unary_conj_Integer64(i64)

declare i64 @checked_unary_conj_Integer64(i64)

declare i8 @unary_conj_UnsignedInteger8(i8)

declare i8 @checked_unary_conj_UnsignedInteger8(i8)

declare i16 @unary_conj_UnsignedInteger16(i16)

declare i16 @checked_unary_conj_UnsignedInteger16(i16)

declare i32 @unary_conj_UnsignedInteger32(i32)

declare i32 @checked_unary_conj_UnsignedInteger32(i32)

declare i64 @unary_conj_UnsignedInteger64(i64)

declare i64 @checked_unary_conj_UnsignedInteger64(i64)

declare i16 @unary_conj_Real16([1 x i32])

declare i16 @checked_unary_conj_Real16([1 x i32])

declare float @unary_conj_Real32(float)

declare float @checked_unary_conj_Real32(float)

declare double @unary_conj_Real64(double)

declare double @checked_unary_conj_Real64(double)

declare void @unary_conj_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_conj_ComplexReal32(float*, float*, float, float)

declare void @unary_conj_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_conj_ComplexReal64(double*, double*, double, double)

declare i8 @unary_copy_Integer8(i8)

declare i8 @checked_unary_copy_Integer8(i8)

declare i16 @unary_copy_Integer16(i16)

declare i16 @checked_unary_copy_Integer16(i16)

declare i32 @unary_copy_Integer32(i32)

declare i32 @checked_unary_copy_Integer32(i32)

declare i64 @unary_copy_Integer64(i64)

declare i64 @checked_unary_copy_Integer64(i64)

declare i8 @unary_copy_UnsignedInteger8(i8)

declare i8 @checked_unary_copy_UnsignedInteger8(i8)

declare i16 @unary_copy_UnsignedInteger16(i16)

declare i16 @checked_unary_copy_UnsignedInteger16(i16)

declare i32 @unary_copy_UnsignedInteger32(i32)

declare i32 @checked_unary_copy_UnsignedInteger32(i32)

declare i64 @unary_copy_UnsignedInteger64(i64)

declare i64 @checked_unary_copy_UnsignedInteger64(i64)

declare i16 @unary_copy_Real16([1 x i32])

declare i16 @checked_unary_copy_Real16([1 x i32])

declare float @unary_copy_Real32(float)

declare float @checked_unary_copy_Real32(float)

declare double @unary_copy_Real64(double)

declare double @checked_unary_copy_Real64(double)

declare void @unary_copy_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_copy_ComplexReal32(float*, float*, float, float)

declare void @unary_copy_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_copy_ComplexReal64(double*, double*, double, double)

declare i8 @unary_cos_Integer8(i8)

declare i8 @checked_unary_cos_Integer8(i8)

declare i16 @unary_cos_Integer16(i16)

declare i16 @checked_unary_cos_Integer16(i16)

declare i32 @unary_cos_Integer32(i32)

declare i32 @checked_unary_cos_Integer32(i32)

declare i64 @unary_cos_Integer64(i64)

declare i64 @checked_unary_cos_Integer64(i64)

declare i8 @unary_cos_UnsignedInteger8(i8)

declare i8 @checked_unary_cos_UnsignedInteger8(i8)

declare i16 @unary_cos_UnsignedInteger16(i16)

declare i16 @checked_unary_cos_UnsignedInteger16(i16)

declare i32 @unary_cos_UnsignedInteger32(i32)

declare i32 @checked_unary_cos_UnsignedInteger32(i32)

declare i64 @unary_cos_UnsignedInteger64(i64)

declare i64 @checked_unary_cos_UnsignedInteger64(i64)

declare i16 @unary_cos_Real16([1 x i32])

declare float @cosf(float)

declare i16 @checked_unary_cos_Real16([1 x i32])

declare float @unary_cos_Real32(float)

declare float @checked_unary_cos_Real32(float)

declare double @unary_cos_Real64(double)

declare double @checked_unary_cos_Real64(double)

declare void @unary_cos_ComplexReal32(float*, float*, float, float)

declare { float, float } @ccosf({ float, float })

declare void @checked_unary_cos_ComplexReal32(float*, float*, float, float)

declare void @unary_cos_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_cos_ComplexReal64(double*, double*, double, double)

declare i8 @unary_cosh_Integer8(i8)

declare i8 @checked_unary_cosh_Integer8(i8)

declare i16 @unary_cosh_Integer16(i16)

declare i16 @checked_unary_cosh_Integer16(i16)

declare i32 @unary_cosh_Integer32(i32)

declare i32 @checked_unary_cosh_Integer32(i32)

declare i64 @unary_cosh_Integer64(i64)

declare i64 @checked_unary_cosh_Integer64(i64)

declare i8 @unary_cosh_UnsignedInteger8(i8)

declare i8 @checked_unary_cosh_UnsignedInteger8(i8)

declare i16 @unary_cosh_UnsignedInteger16(i16)

declare i16 @checked_unary_cosh_UnsignedInteger16(i16)

declare i32 @unary_cosh_UnsignedInteger32(i32)

declare i32 @checked_unary_cosh_UnsignedInteger32(i32)

declare i64 @unary_cosh_UnsignedInteger64(i64)

declare i64 @checked_unary_cosh_UnsignedInteger64(i64)

declare i16 @unary_cosh_Real16([1 x i32])

declare float @coshf(float)

declare i16 @checked_unary_cosh_Real16([1 x i32])

declare float @unary_cosh_Real32(float)

declare float @checked_unary_cosh_Real32(float)

declare double @unary_cosh_Real64(double)

declare double @checked_unary_cosh_Real64(double)

declare void @unary_cosh_ComplexReal32(float*, float*, float, float)

declare { float, float } @ccoshf({ float, float })

declare void @checked_unary_cosh_ComplexReal32(float*, float*, float, float)

declare void @unary_cosh_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_cosh_ComplexReal64(double*, double*, double, double)

declare i8 @unary_cot_Integer8(i8)

declare i8 @checked_unary_cot_Integer8(i8)

declare i16 @unary_cot_Integer16(i16)

declare i16 @checked_unary_cot_Integer16(i16)

declare i32 @unary_cot_Integer32(i32)

declare i32 @checked_unary_cot_Integer32(i32)

declare i64 @unary_cot_Integer64(i64)

declare i64 @checked_unary_cot_Integer64(i64)

declare i8 @unary_cot_UnsignedInteger8(i8)

declare i8 @checked_unary_cot_UnsignedInteger8(i8)

declare i16 @unary_cot_UnsignedInteger16(i16)

declare i16 @checked_unary_cot_UnsignedInteger16(i16)

declare i32 @unary_cot_UnsignedInteger32(i32)

declare i32 @checked_unary_cot_UnsignedInteger32(i32)

declare i64 @unary_cot_UnsignedInteger64(i64)

declare i64 @checked_unary_cot_UnsignedInteger64(i64)

declare i16 @unary_cot_Real16([1 x i32])

declare float @tanf(float)

declare i16 @checked_unary_cot_Real16([1 x i32])

declare float @unary_cot_Real32(float)

declare float @checked_unary_cot_Real32(float)

declare double @unary_cot_Real64(double)

declare double @checked_unary_cot_Real64(double)

declare void @unary_cot_ComplexReal32(float*, float*, float, float)

declare { float, float } @ctanf({ float, float })

declare void @checked_unary_cot_ComplexReal32(float*, float*, float, float)

declare void @unary_cot_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_cot_ComplexReal64(double*, double*, double, double)

declare i8 @unary_coth_Integer8(i8)

declare i8 @checked_unary_coth_Integer8(i8)

declare i16 @unary_coth_Integer16(i16)

declare i16 @checked_unary_coth_Integer16(i16)

declare i32 @unary_coth_Integer32(i32)

declare i32 @checked_unary_coth_Integer32(i32)

declare i64 @unary_coth_Integer64(i64)

declare i64 @checked_unary_coth_Integer64(i64)

declare i8 @unary_coth_UnsignedInteger8(i8)

declare i8 @checked_unary_coth_UnsignedInteger8(i8)

declare i16 @unary_coth_UnsignedInteger16(i16)

declare i16 @checked_unary_coth_UnsignedInteger16(i16)

declare i32 @unary_coth_UnsignedInteger32(i32)

declare i32 @checked_unary_coth_UnsignedInteger32(i32)

declare i64 @unary_coth_UnsignedInteger64(i64)

declare i64 @checked_unary_coth_UnsignedInteger64(i64)

declare i16 @unary_coth_Real16([1 x i32])

declare float @tanhf(float)

declare i16 @checked_unary_coth_Real16([1 x i32])

declare float @unary_coth_Real32(float)

declare float @checked_unary_coth_Real32(float)

declare double @unary_coth_Real64(double)

declare double @checked_unary_coth_Real64(double)

declare void @unary_coth_ComplexReal32(float*, float*, float, float)

declare { float, float } @ctanhf({ float, float })

declare void @checked_unary_coth_ComplexReal32(float*, float*, float, float)

declare void @unary_coth_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_coth_ComplexReal64(double*, double*, double, double)

declare i8 @unary_csc_Integer8(i8)

declare i8 @checked_unary_csc_Integer8(i8)

declare i16 @unary_csc_Integer16(i16)

declare i16 @checked_unary_csc_Integer16(i16)

declare i32 @unary_csc_Integer32(i32)

declare i32 @checked_unary_csc_Integer32(i32)

declare i64 @unary_csc_Integer64(i64)

declare i64 @checked_unary_csc_Integer64(i64)

declare i8 @unary_csc_UnsignedInteger8(i8)

declare i8 @checked_unary_csc_UnsignedInteger8(i8)

declare i16 @unary_csc_UnsignedInteger16(i16)

declare i16 @checked_unary_csc_UnsignedInteger16(i16)

declare i32 @unary_csc_UnsignedInteger32(i32)

declare i32 @checked_unary_csc_UnsignedInteger32(i32)

declare i64 @unary_csc_UnsignedInteger64(i64)

declare i64 @checked_unary_csc_UnsignedInteger64(i64)

declare i16 @unary_csc_Real16([1 x i32])

declare float @sinf(float)

declare i16 @checked_unary_csc_Real16([1 x i32])

declare float @unary_csc_Real32(float)

declare float @checked_unary_csc_Real32(float)

declare double @unary_csc_Real64(double)

declare double @checked_unary_csc_Real64(double)

declare void @unary_csc_ComplexReal32(float*, float*, float, float)

declare { float, float } @csinf({ float, float })

declare void @checked_unary_csc_ComplexReal32(float*, float*, float, float)

declare void @unary_csc_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_csc_ComplexReal64(double*, double*, double, double)

declare i8 @unary_csch_Integer8(i8)

declare i8 @checked_unary_csch_Integer8(i8)

declare i16 @unary_csch_Integer16(i16)

declare i16 @checked_unary_csch_Integer16(i16)

declare i32 @unary_csch_Integer32(i32)

declare i32 @checked_unary_csch_Integer32(i32)

declare i64 @unary_csch_Integer64(i64)

declare i64 @checked_unary_csch_Integer64(i64)

declare i8 @unary_csch_UnsignedInteger8(i8)

declare i8 @checked_unary_csch_UnsignedInteger8(i8)

declare i16 @unary_csch_UnsignedInteger16(i16)

declare i16 @checked_unary_csch_UnsignedInteger16(i16)

declare i32 @unary_csch_UnsignedInteger32(i32)

declare i32 @checked_unary_csch_UnsignedInteger32(i32)

declare i64 @unary_csch_UnsignedInteger64(i64)

declare i64 @checked_unary_csch_UnsignedInteger64(i64)

declare i16 @unary_csch_Real16([1 x i32])

declare float @sinhf(float)

declare i16 @checked_unary_csch_Real16([1 x i32])

declare float @unary_csch_Real32(float)

declare float @checked_unary_csch_Real32(float)

declare double @unary_csch_Real64(double)

declare double @checked_unary_csch_Real64(double)

declare void @unary_csch_ComplexReal32(float*, float*, float, float)

declare { float, float } @csinhf({ float, float })

declare void @checked_unary_csch_ComplexReal32(float*, float*, float, float)

declare void @unary_csch_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_csch_ComplexReal64(double*, double*, double, double)

declare i8 @unary_erf_Integer8(i8)

declare i8 @checked_unary_erf_Integer8(i8)

declare i16 @unary_erf_Integer16(i16)

declare i16 @checked_unary_erf_Integer16(i16)

declare i32 @unary_erf_Integer32(i32)

declare i32 @checked_unary_erf_Integer32(i32)

declare i64 @unary_erf_Integer64(i64)

declare i64 @checked_unary_erf_Integer64(i64)

declare i8 @unary_erf_UnsignedInteger8(i8)

declare i8 @checked_unary_erf_UnsignedInteger8(i8)

declare i16 @unary_erf_UnsignedInteger16(i16)

declare i16 @checked_unary_erf_UnsignedInteger16(i16)

declare i32 @unary_erf_UnsignedInteger32(i32)

declare i32 @checked_unary_erf_UnsignedInteger32(i32)

declare i64 @unary_erf_UnsignedInteger64(i64)

declare i64 @checked_unary_erf_UnsignedInteger64(i64)

declare i16 @unary_erf_Real16([1 x i32])

declare float @erff(float)

declare i16 @checked_unary_erf_Real16([1 x i32])

declare float @unary_erf_Real32(float)

declare float @checked_unary_erf_Real32(float)

declare double @unary_erf_Real64(double)

declare double @checked_unary_erf_Real64(double)

declare void @unary_erf_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_erf_ComplexReal32(float*, float*, float, float)

declare void @unary_erf_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_erf_ComplexReal64(double*, double*, double, double)

declare i8 @unary_erfc_Integer8(i8)

declare i8 @checked_unary_erfc_Integer8(i8)

declare i16 @unary_erfc_Integer16(i16)

declare i16 @checked_unary_erfc_Integer16(i16)

declare i32 @unary_erfc_Integer32(i32)

declare i32 @checked_unary_erfc_Integer32(i32)

declare i64 @unary_erfc_Integer64(i64)

declare i64 @checked_unary_erfc_Integer64(i64)

declare i8 @unary_erfc_UnsignedInteger8(i8)

declare i8 @checked_unary_erfc_UnsignedInteger8(i8)

declare i16 @unary_erfc_UnsignedInteger16(i16)

declare i16 @checked_unary_erfc_UnsignedInteger16(i16)

declare i32 @unary_erfc_UnsignedInteger32(i32)

declare i32 @checked_unary_erfc_UnsignedInteger32(i32)

declare i64 @unary_erfc_UnsignedInteger64(i64)

declare i64 @checked_unary_erfc_UnsignedInteger64(i64)

declare i16 @unary_erfc_Real16([1 x i32])

declare float @erfcf(float)

declare i16 @checked_unary_erfc_Real16([1 x i32])

declare float @unary_erfc_Real32(float)

declare float @checked_unary_erfc_Real32(float)

declare double @unary_erfc_Real64(double)

declare double @checked_unary_erfc_Real64(double)

declare void @unary_erfc_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_erfc_ComplexReal32(float*, float*, float, float)

declare void @unary_erfc_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_erfc_ComplexReal64(double*, double*, double, double)

declare i32 @unary_evenq_Integer8(i8)

declare i32 @checked_unary_evenq_Integer8(i8)

declare i32 @unary_evenq_Integer16(i16)

declare i32 @checked_unary_evenq_Integer16(i16)

declare i32 @unary_evenq_Integer32(i32)

declare i32 @checked_unary_evenq_Integer32(i32)

declare i32 @unary_evenq_Integer64(i64)

declare i32 @checked_unary_evenq_Integer64(i64)

declare i32 @unary_evenq_UnsignedInteger8(i8)

declare i32 @checked_unary_evenq_UnsignedInteger8(i8)

declare i32 @unary_evenq_UnsignedInteger16(i16)

declare i32 @checked_unary_evenq_UnsignedInteger16(i16)

declare i32 @unary_evenq_UnsignedInteger32(i32)

declare i32 @checked_unary_evenq_UnsignedInteger32(i32)

declare i32 @unary_evenq_UnsignedInteger64(i64)

declare i32 @checked_unary_evenq_UnsignedInteger64(i64)

declare i32 @unary_evenq_Real16([1 x i32])

declare i32 @checked_unary_evenq_Real16([1 x i32])

declare i32 @unary_evenq_Real32(float)

declare i32 @checked_unary_evenq_Real32(float)

declare i32 @unary_evenq_Real64(double)

declare i32 @checked_unary_evenq_Real64(double)

declare i32 @unary_evenq_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_evenq_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_evenq_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_evenq_ComplexReal64(%"struct.std::complex")

declare i8 @unary_exp_Integer8(i8)

declare i8 @checked_unary_exp_Integer8(i8)

declare i16 @unary_exp_Integer16(i16)

declare i16 @checked_unary_exp_Integer16(i16)

declare i32 @unary_exp_Integer32(i32)

declare i32 @checked_unary_exp_Integer32(i32)

declare i64 @unary_exp_Integer64(i64)

declare i64 @checked_unary_exp_Integer64(i64)

declare i8 @unary_exp_UnsignedInteger8(i8)

declare i8 @checked_unary_exp_UnsignedInteger8(i8)

declare i16 @unary_exp_UnsignedInteger16(i16)

declare i16 @checked_unary_exp_UnsignedInteger16(i16)

declare i32 @unary_exp_UnsignedInteger32(i32)

declare i32 @checked_unary_exp_UnsignedInteger32(i32)

declare i64 @unary_exp_UnsignedInteger64(i64)

declare i64 @checked_unary_exp_UnsignedInteger64(i64)

declare i16 @unary_exp_Real16([1 x i32])

declare float @expf(float)

declare i16 @checked_unary_exp_Real16([1 x i32])

declare float @unary_exp_Real32(float)

declare float @checked_unary_exp_Real32(float)

declare double @unary_exp_Real64(double)

declare double @checked_unary_exp_Real64(double)

declare void @unary_exp_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_exp_ComplexReal32(float*, float*, float, float)

declare void @unary_exp_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_exp_ComplexReal64(double*, double*, double, double)

declare i8 @unary_expm1_Integer8(i8)

declare i8 @checked_unary_expm1_Integer8(i8)

declare i16 @unary_expm1_Integer16(i16)

declare i16 @checked_unary_expm1_Integer16(i16)

declare i32 @unary_expm1_Integer32(i32)

declare i32 @checked_unary_expm1_Integer32(i32)

declare i64 @unary_expm1_Integer64(i64)

declare i64 @checked_unary_expm1_Integer64(i64)

declare i8 @unary_expm1_UnsignedInteger8(i8)

declare i8 @checked_unary_expm1_UnsignedInteger8(i8)

declare i16 @unary_expm1_UnsignedInteger16(i16)

declare i16 @checked_unary_expm1_UnsignedInteger16(i16)

declare i32 @unary_expm1_UnsignedInteger32(i32)

declare i32 @checked_unary_expm1_UnsignedInteger32(i32)

declare i64 @unary_expm1_UnsignedInteger64(i64)

declare i64 @checked_unary_expm1_UnsignedInteger64(i64)

declare i16 @unary_expm1_Real16([1 x i32])

declare float @expm1f(float)

declare i16 @checked_unary_expm1_Real16([1 x i32])

declare float @unary_expm1_Real32(float)

declare float @checked_unary_expm1_Real32(float)

declare double @unary_expm1_Real64(double)

declare double @checked_unary_expm1_Real64(double)

declare void @unary_expm1_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_expm1_ComplexReal32(float*, float*, float, float)

declare void @unary_expm1_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_expm1_ComplexReal64(double*, double*, double, double)

declare i8 @unary_fibonacci_Integer8(i8)

declare i8 @checked_unary_fibonacci_Integer8(i8)

declare i16 @unary_fibonacci_Integer16(i16)

declare i16 @checked_unary_fibonacci_Integer16(i16)

declare i32 @unary_fibonacci_Integer32(i32)

declare i32 @checked_unary_fibonacci_Integer32(i32)

declare i64 @unary_fibonacci_Integer64(i64)

declare i64 @checked_unary_fibonacci_Integer64(i64)

declare i8 @unary_fibonacci_UnsignedInteger8(i8)

declare i8 @checked_unary_fibonacci_UnsignedInteger8(i8)

declare i16 @unary_fibonacci_UnsignedInteger16(i16)

declare i16 @checked_unary_fibonacci_UnsignedInteger16(i16)

declare i32 @unary_fibonacci_UnsignedInteger32(i32)

declare i32 @checked_unary_fibonacci_UnsignedInteger32(i32)

declare i64 @unary_fibonacci_UnsignedInteger64(i64)

declare i64 @checked_unary_fibonacci_UnsignedInteger64(i64)

declare i16 @unary_fibonacci_Real16([1 x i32])

declare i16 @checked_unary_fibonacci_Real16([1 x i32])

declare float @unary_fibonacci_Real32(float)

declare float @checked_unary_fibonacci_Real32(float)

declare double @unary_fibonacci_Real64(double)

declare double @checked_unary_fibonacci_Real64(double)

declare void @unary_fibonacci_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_fibonacci_ComplexReal32(float*, float*, float, float)

declare void @unary_fibonacci_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_fibonacci_ComplexReal64(double*, double*, double, double)

declare i8 @unary_floor_Integer8(i8)

declare i8 @checked_unary_floor_Integer8(i8)

declare i16 @unary_floor_Integer16(i16)

declare i16 @checked_unary_floor_Integer16(i16)

declare i32 @unary_floor_Integer32(i32)

declare i32 @checked_unary_floor_Integer32(i32)

declare i64 @unary_floor_Integer64(i64)

declare i64 @checked_unary_floor_Integer64(i64)

declare i8 @unary_floor_UnsignedInteger8(i8)

declare i8 @checked_unary_floor_UnsignedInteger8(i8)

declare i16 @unary_floor_UnsignedInteger16(i16)

declare i16 @checked_unary_floor_UnsignedInteger16(i16)

declare i32 @unary_floor_UnsignedInteger32(i32)

declare i32 @checked_unary_floor_UnsignedInteger32(i32)

declare i64 @unary_floor_UnsignedInteger64(i64)

declare i64 @checked_unary_floor_UnsignedInteger64(i64)

declare i32 @unary_floor_Real16([1 x i32])

declare i32 @checked_unary_floor_Real16([1 x i32])

declare i32 @unary_floor_Real32(float)

declare i32 @checked_unary_floor_Real32(float)

declare i32 @unary_floor_Real64(double)

declare i32 @checked_unary_floor_Real64(double)

declare i32 @unary_floor_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_floor_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_floor_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_floor_ComplexReal64(%"struct.std::complex")

declare i32 @unary_fpexception_Integer8(i8)

declare i32 @checked_unary_fpexception_Integer8(i8)

declare i32 @unary_fpexception_Integer16(i16)

declare i32 @checked_unary_fpexception_Integer16(i16)

declare i32 @unary_fpexception_Integer32(i32)

declare i32 @checked_unary_fpexception_Integer32(i32)

declare i32 @unary_fpexception_Integer64(i64)

declare i32 @checked_unary_fpexception_Integer64(i64)

declare i32 @unary_fpexception_UnsignedInteger8(i8)

declare i32 @checked_unary_fpexception_UnsignedInteger8(i8)

declare i32 @unary_fpexception_UnsignedInteger16(i16)

declare i32 @checked_unary_fpexception_UnsignedInteger16(i16)

declare i32 @unary_fpexception_UnsignedInteger32(i32)

declare i32 @checked_unary_fpexception_UnsignedInteger32(i32)

declare i32 @unary_fpexception_UnsignedInteger64(i64)

declare i32 @checked_unary_fpexception_UnsignedInteger64(i64)

declare i32 @unary_fpexception_Real16([1 x i32])

declare i32 @checked_unary_fpexception_Real16([1 x i32])

declare i32 @unary_fpexception_Real32(float)

declare i32 @checked_unary_fpexception_Real32(float)

declare i32 @unary_fpexception_Real64(double)

declare i32 @checked_unary_fpexception_Real64(double)

declare i32 @unary_fpexception_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_fpexception_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_fpexception_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_fpexception_ComplexReal64(%"struct.std::complex")

declare i8 @unary_fracpart_Integer8(i8)

declare i8 @checked_unary_fracpart_Integer8(i8)

declare i16 @unary_fracpart_Integer16(i16)

declare i16 @checked_unary_fracpart_Integer16(i16)

declare i32 @unary_fracpart_Integer32(i32)

declare i32 @checked_unary_fracpart_Integer32(i32)

declare i64 @unary_fracpart_Integer64(i64)

declare i64 @checked_unary_fracpart_Integer64(i64)

declare i8 @unary_fracpart_UnsignedInteger8(i8)

declare i8 @checked_unary_fracpart_UnsignedInteger8(i8)

declare i16 @unary_fracpart_UnsignedInteger16(i16)

declare i16 @checked_unary_fracpart_UnsignedInteger16(i16)

declare i32 @unary_fracpart_UnsignedInteger32(i32)

declare i32 @checked_unary_fracpart_UnsignedInteger32(i32)

declare i64 @unary_fracpart_UnsignedInteger64(i64)

declare i64 @checked_unary_fracpart_UnsignedInteger64(i64)

declare i16 @unary_fracpart_Real16([1 x i32])

declare i16 @checked_unary_fracpart_Real16([1 x i32])

declare float @unary_fracpart_Real32(float)

declare float @checked_unary_fracpart_Real32(float)

declare double @unary_fracpart_Real64(double)

declare double @checked_unary_fracpart_Real64(double)

declare void @unary_fracpart_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_fracpart_ComplexReal32(float*, float*, float, float)

declare void @unary_fracpart_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_fracpart_ComplexReal64(double*, double*, double, double)

declare i8 @unary_gamma_Integer8(i8)

declare i8 @checked_unary_gamma_Integer8(i8)

declare i16 @unary_gamma_Integer16(i16)

declare i16 @checked_unary_gamma_Integer16(i16)

declare i32 @unary_gamma_Integer32(i32)

declare i32 @checked_unary_gamma_Integer32(i32)

declare i64 @unary_gamma_Integer64(i64)

declare i64 @checked_unary_gamma_Integer64(i64)

declare i8 @unary_gamma_UnsignedInteger8(i8)

declare i8 @checked_unary_gamma_UnsignedInteger8(i8)

declare i16 @unary_gamma_UnsignedInteger16(i16)

declare i16 @checked_unary_gamma_UnsignedInteger16(i16)

declare i32 @unary_gamma_UnsignedInteger32(i32)

declare i32 @checked_unary_gamma_UnsignedInteger32(i32)

declare i64 @unary_gamma_UnsignedInteger64(i64)

declare i64 @checked_unary_gamma_UnsignedInteger64(i64)

declare i16 @unary_gamma_Real16([1 x i32])

declare float @tgammaf(float)

declare i16 @checked_unary_gamma_Real16([1 x i32])

declare float @unary_gamma_Real32(float)

declare float @checked_unary_gamma_Real32(float)

declare double @unary_gamma_Real64(double)

declare double @checked_unary_gamma_Real64(double)

declare void @unary_gamma_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_gamma_ComplexReal32(float*, float*, float, float)

declare void @unary_gamma_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_gamma_ComplexReal64(double*, double*, double, double)

declare i8 @unary_gudermannian_Integer8(i8)

declare i8 @checked_unary_gudermannian_Integer8(i8)

declare i16 @unary_gudermannian_Integer16(i16)

declare i16 @checked_unary_gudermannian_Integer16(i16)

declare i32 @unary_gudermannian_Integer32(i32)

declare i32 @checked_unary_gudermannian_Integer32(i32)

declare i64 @unary_gudermannian_Integer64(i64)

declare i64 @checked_unary_gudermannian_Integer64(i64)

declare i8 @unary_gudermannian_UnsignedInteger8(i8)

declare i8 @checked_unary_gudermannian_UnsignedInteger8(i8)

declare i16 @unary_gudermannian_UnsignedInteger16(i16)

declare i16 @checked_unary_gudermannian_UnsignedInteger16(i16)

declare i32 @unary_gudermannian_UnsignedInteger32(i32)

declare i32 @checked_unary_gudermannian_UnsignedInteger32(i32)

declare i64 @unary_gudermannian_UnsignedInteger64(i64)

declare i64 @checked_unary_gudermannian_UnsignedInteger64(i64)

declare i16 @unary_gudermannian_Real16([1 x i32])

declare i16 @checked_unary_gudermannian_Real16([1 x i32])

declare float @unary_gudermannian_Real32(float)

declare float @checked_unary_gudermannian_Real32(float)

declare double @unary_gudermannian_Real64(double)

declare double @checked_unary_gudermannian_Real64(double)

declare void @unary_gudermannian_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_gudermannian_ComplexReal32(float*, float*, float, float)

declare void @unary_gudermannian_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_gudermannian_ComplexReal64(double*, double*, double, double)

declare i8 @unary_haversine_Integer8(i8)

declare i8 @checked_unary_haversine_Integer8(i8)

declare i16 @unary_haversine_Integer16(i16)

declare i16 @checked_unary_haversine_Integer16(i16)

declare i32 @unary_haversine_Integer32(i32)

declare i32 @checked_unary_haversine_Integer32(i32)

declare i64 @unary_haversine_Integer64(i64)

declare i64 @checked_unary_haversine_Integer64(i64)

declare i8 @unary_haversine_UnsignedInteger8(i8)

declare i8 @checked_unary_haversine_UnsignedInteger8(i8)

declare i16 @unary_haversine_UnsignedInteger16(i16)

declare i16 @checked_unary_haversine_UnsignedInteger16(i16)

declare i32 @unary_haversine_UnsignedInteger32(i32)

declare i32 @checked_unary_haversine_UnsignedInteger32(i32)

declare i64 @unary_haversine_UnsignedInteger64(i64)

declare i64 @checked_unary_haversine_UnsignedInteger64(i64)

declare i16 @unary_haversine_Real16([1 x i32])

declare i16 @checked_unary_haversine_Real16([1 x i32])

declare float @unary_haversine_Real32(float)

declare float @checked_unary_haversine_Real32(float)

declare double @unary_haversine_Real64(double)

declare double @checked_unary_haversine_Real64(double)

declare void @unary_haversine_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_haversine_ComplexReal32(float*, float*, float, float)

declare void @unary_haversine_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_haversine_ComplexReal64(double*, double*, double, double)

declare i8 @unary_im_Integer8(i8)

declare i8 @checked_unary_im_Integer8(i8)

declare i16 @unary_im_Integer16(i16)

declare i16 @checked_unary_im_Integer16(i16)

declare i32 @unary_im_Integer32(i32)

declare i32 @checked_unary_im_Integer32(i32)

declare i64 @unary_im_Integer64(i64)

declare i64 @checked_unary_im_Integer64(i64)

declare i8 @unary_im_UnsignedInteger8(i8)

declare i8 @checked_unary_im_UnsignedInteger8(i8)

declare i16 @unary_im_UnsignedInteger16(i16)

declare i16 @checked_unary_im_UnsignedInteger16(i16)

declare i32 @unary_im_UnsignedInteger32(i32)

declare i32 @checked_unary_im_UnsignedInteger32(i32)

declare i64 @unary_im_UnsignedInteger64(i64)

declare i64 @checked_unary_im_UnsignedInteger64(i64)

declare i32 @unary_im_Real16([1 x i32])

declare i32 @checked_unary_im_Real16([1 x i32])

declare i32 @unary_im_Real32(float)

declare i32 @checked_unary_im_Real32(float)

declare i32 @unary_im_Real64(double)

declare i32 @checked_unary_im_Real64(double)

declare float @unary_im_ComplexReal32(%"struct.std::complex.203")

declare float @checked_unary_im_ComplexReal32(%"struct.std::complex.203")

declare double @unary_im_ComplexReal64(%"struct.std::complex")

declare double @checked_unary_im_ComplexReal64(%"struct.std::complex")

declare i8 @unary_intpart_Integer8(i8)

declare i8 @checked_unary_intpart_Integer8(i8)

declare i16 @unary_intpart_Integer16(i16)

declare i16 @checked_unary_intpart_Integer16(i16)

declare i32 @unary_intpart_Integer32(i32)

declare i32 @checked_unary_intpart_Integer32(i32)

declare i64 @unary_intpart_Integer64(i64)

declare i64 @checked_unary_intpart_Integer64(i64)

declare i8 @unary_intpart_UnsignedInteger8(i8)

declare i8 @checked_unary_intpart_UnsignedInteger8(i8)

declare i16 @unary_intpart_UnsignedInteger16(i16)

declare i16 @checked_unary_intpart_UnsignedInteger16(i16)

declare i32 @unary_intpart_UnsignedInteger32(i32)

declare i32 @checked_unary_intpart_UnsignedInteger32(i32)

declare i64 @unary_intpart_UnsignedInteger64(i64)

declare i64 @checked_unary_intpart_UnsignedInteger64(i64)

declare i32 @unary_intpart_Real16([1 x i32])

declare i32 @checked_unary_intpart_Real16([1 x i32])

declare i32 @unary_intpart_Real32(float)

; Function Attrs: nounwind readnone speculatable
declare float @llvm.trunc.f32(float) #0

declare i32 @checked_unary_intpart_Real32(float)

declare i32 @unary_intpart_Real64(double)

declare i32 @checked_unary_intpart_Real64(double)

declare i32 @unary_intpart_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_intpart_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_intpart_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_intpart_ComplexReal64(%"struct.std::complex")

declare i8 @unary_inversegudermannian_Integer8(i8)

declare i8 @checked_unary_inversegudermannian_Integer8(i8)

declare i16 @unary_inversegudermannian_Integer16(i16)

declare i16 @checked_unary_inversegudermannian_Integer16(i16)

declare i32 @unary_inversegudermannian_Integer32(i32)

declare i32 @checked_unary_inversegudermannian_Integer32(i32)

declare i64 @unary_inversegudermannian_Integer64(i64)

declare i64 @checked_unary_inversegudermannian_Integer64(i64)

declare i8 @unary_inversegudermannian_UnsignedInteger8(i8)

declare i8 @checked_unary_inversegudermannian_UnsignedInteger8(i8)

declare i16 @unary_inversegudermannian_UnsignedInteger16(i16)

declare i16 @checked_unary_inversegudermannian_UnsignedInteger16(i16)

declare i32 @unary_inversegudermannian_UnsignedInteger32(i32)

declare i32 @checked_unary_inversegudermannian_UnsignedInteger32(i32)

declare i64 @unary_inversegudermannian_UnsignedInteger64(i64)

declare i64 @checked_unary_inversegudermannian_UnsignedInteger64(i64)

declare i16 @unary_inversegudermannian_Real16([1 x i32])

declare i16 @checked_unary_inversegudermannian_Real16([1 x i32])

declare float @unary_inversegudermannian_Real32(float)

declare float @checked_unary_inversegudermannian_Real32(float)

declare double @unary_inversegudermannian_Real64(double)

declare double @checked_unary_inversegudermannian_Real64(double)

declare void @unary_inversegudermannian_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_inversegudermannian_ComplexReal32(float*, float*, float, float)

declare void @unary_inversegudermannian_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_inversegudermannian_ComplexReal64(double*, double*, double, double)

declare i8 @unary_inversehaversine_Integer8(i8)

declare i8 @checked_unary_inversehaversine_Integer8(i8)

declare i16 @unary_inversehaversine_Integer16(i16)

declare i16 @checked_unary_inversehaversine_Integer16(i16)

declare i32 @unary_inversehaversine_Integer32(i32)

declare i32 @checked_unary_inversehaversine_Integer32(i32)

declare i64 @unary_inversehaversine_Integer64(i64)

declare i64 @checked_unary_inversehaversine_Integer64(i64)

declare i8 @unary_inversehaversine_UnsignedInteger8(i8)

declare i8 @checked_unary_inversehaversine_UnsignedInteger8(i8)

declare i16 @unary_inversehaversine_UnsignedInteger16(i16)

declare i16 @checked_unary_inversehaversine_UnsignedInteger16(i16)

declare i32 @unary_inversehaversine_UnsignedInteger32(i32)

declare i32 @checked_unary_inversehaversine_UnsignedInteger32(i32)

declare i64 @unary_inversehaversine_UnsignedInteger64(i64)

declare i64 @checked_unary_inversehaversine_UnsignedInteger64(i64)

declare i16 @unary_inversehaversine_Real16([1 x i32])

declare i16 @checked_unary_inversehaversine_Real16([1 x i32])

declare float @unary_inversehaversine_Real32(float)

declare float @checked_unary_inversehaversine_Real32(float)

declare double @unary_inversehaversine_Real64(double)

declare double @checked_unary_inversehaversine_Real64(double)

declare void @unary_inversehaversine_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_inversehaversine_ComplexReal32(float*, float*, float, float)

declare void @unary_inversehaversine_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_inversehaversine_ComplexReal64(double*, double*, double, double)

declare i8 @unary_log_Integer8(i8)

declare i8 @checked_unary_log_Integer8(i8)

declare i16 @unary_log_Integer16(i16)

declare i16 @checked_unary_log_Integer16(i16)

declare i32 @unary_log_Integer32(i32)

declare i32 @checked_unary_log_Integer32(i32)

declare i64 @unary_log_Integer64(i64)

declare i64 @checked_unary_log_Integer64(i64)

declare i8 @unary_log_UnsignedInteger8(i8)

declare i8 @checked_unary_log_UnsignedInteger8(i8)

declare i16 @unary_log_UnsignedInteger16(i16)

declare i16 @checked_unary_log_UnsignedInteger16(i16)

declare i32 @unary_log_UnsignedInteger32(i32)

declare i32 @checked_unary_log_UnsignedInteger32(i32)

declare i64 @unary_log_UnsignedInteger64(i64)

declare i64 @checked_unary_log_UnsignedInteger64(i64)

declare i16 @unary_log_Real16([1 x i32])

declare i16 @checked_unary_log_Real16([1 x i32])

declare float @unary_log_Real32(float)

declare float @checked_unary_log_Real32(float)

declare double @unary_log_Real64(double)

declare double @checked_unary_log_Real64(double)

declare void @unary_log_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_log_ComplexReal32(float*, float*, float, float)

declare void @unary_log_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_log_ComplexReal64(double*, double*, double, double)

declare i8 @unary_log10_Integer8(i8)

declare i8 @checked_unary_log10_Integer8(i8)

declare i16 @unary_log10_Integer16(i16)

declare i16 @checked_unary_log10_Integer16(i16)

declare i32 @unary_log10_Integer32(i32)

declare i32 @checked_unary_log10_Integer32(i32)

declare i64 @unary_log10_Integer64(i64)

declare i64 @checked_unary_log10_Integer64(i64)

declare i8 @unary_log10_UnsignedInteger8(i8)

declare i8 @checked_unary_log10_UnsignedInteger8(i8)

declare i16 @unary_log10_UnsignedInteger16(i16)

declare i16 @checked_unary_log10_UnsignedInteger16(i16)

declare i32 @unary_log10_UnsignedInteger32(i32)

declare i32 @checked_unary_log10_UnsignedInteger32(i32)

declare i64 @unary_log10_UnsignedInteger64(i64)

declare i64 @checked_unary_log10_UnsignedInteger64(i64)

declare i16 @unary_log10_Real16([1 x i32])

declare float @log10f(float)

declare i16 @checked_unary_log10_Real16([1 x i32])

declare float @unary_log10_Real32(float)

declare float @checked_unary_log10_Real32(float)

declare double @unary_log10_Real64(double)

declare double @checked_unary_log10_Real64(double)

declare void @unary_log10_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_log10_ComplexReal32(float*, float*, float, float)

declare void @unary_log10_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_log10_ComplexReal64(double*, double*, double, double)

declare i8 @unary_log1p_Integer8(i8)

declare i8 @checked_unary_log1p_Integer8(i8)

declare i16 @unary_log1p_Integer16(i16)

declare i16 @checked_unary_log1p_Integer16(i16)

declare i32 @unary_log1p_Integer32(i32)

declare i32 @checked_unary_log1p_Integer32(i32)

declare i64 @unary_log1p_Integer64(i64)

declare i64 @checked_unary_log1p_Integer64(i64)

declare i8 @unary_log1p_UnsignedInteger8(i8)

declare i8 @checked_unary_log1p_UnsignedInteger8(i8)

declare i16 @unary_log1p_UnsignedInteger16(i16)

declare i16 @checked_unary_log1p_UnsignedInteger16(i16)

declare i32 @unary_log1p_UnsignedInteger32(i32)

declare i32 @checked_unary_log1p_UnsignedInteger32(i32)

declare i64 @unary_log1p_UnsignedInteger64(i64)

declare i64 @checked_unary_log1p_UnsignedInteger64(i64)

declare i16 @unary_log1p_Real16([1 x i32])

declare i16 @checked_unary_log1p_Real16([1 x i32])

declare float @unary_log1p_Real32(float)

declare float @checked_unary_log1p_Real32(float)

declare double @unary_log1p_Real64(double)

declare double @checked_unary_log1p_Real64(double)

declare void @unary_log1p_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_log1p_ComplexReal32(float*, float*, float, float)

declare void @unary_log1p_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_log1p_ComplexReal64(double*, double*, double, double)

declare i8 @unary_log2_Integer8(i8)

declare i8 @checked_unary_log2_Integer8(i8)

declare i16 @unary_log2_Integer16(i16)

declare i16 @checked_unary_log2_Integer16(i16)

declare i32 @unary_log2_Integer32(i32)

declare i32 @checked_unary_log2_Integer32(i32)

declare i64 @unary_log2_Integer64(i64)

declare i64 @checked_unary_log2_Integer64(i64)

declare i8 @unary_log2_UnsignedInteger8(i8)

declare i8 @checked_unary_log2_UnsignedInteger8(i8)

declare i16 @unary_log2_UnsignedInteger16(i16)

declare i16 @checked_unary_log2_UnsignedInteger16(i16)

declare i32 @unary_log2_UnsignedInteger32(i32)

declare i32 @checked_unary_log2_UnsignedInteger32(i32)

declare i64 @unary_log2_UnsignedInteger64(i64)

declare i64 @checked_unary_log2_UnsignedInteger64(i64)

declare i16 @unary_log2_Real16([1 x i32])

declare float @log2f(float)

declare i16 @checked_unary_log2_Real16([1 x i32])

declare float @unary_log2_Real32(float)

declare float @checked_unary_log2_Real32(float)

declare double @unary_log2_Real64(double)

declare double @checked_unary_log2_Real64(double)

declare void @unary_log2_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_log2_ComplexReal32(float*, float*, float, float)

declare void @unary_log2_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_log2_ComplexReal64(double*, double*, double, double)

declare i8 @unary_loggamma_Integer8(i8)

declare i8 @checked_unary_loggamma_Integer8(i8)

declare i16 @unary_loggamma_Integer16(i16)

declare i16 @checked_unary_loggamma_Integer16(i16)

declare i32 @unary_loggamma_Integer32(i32)

declare i32 @checked_unary_loggamma_Integer32(i32)

declare i64 @unary_loggamma_Integer64(i64)

declare i64 @checked_unary_loggamma_Integer64(i64)

declare i8 @unary_loggamma_UnsignedInteger8(i8)

declare i8 @checked_unary_loggamma_UnsignedInteger8(i8)

declare i16 @unary_loggamma_UnsignedInteger16(i16)

declare i16 @checked_unary_loggamma_UnsignedInteger16(i16)

declare i32 @unary_loggamma_UnsignedInteger32(i32)

declare i32 @checked_unary_loggamma_UnsignedInteger32(i32)

declare i64 @unary_loggamma_UnsignedInteger64(i64)

declare i64 @checked_unary_loggamma_UnsignedInteger64(i64)

declare i16 @unary_loggamma_Real16([1 x i32])

declare float @lgammaf(float)

declare i16 @checked_unary_loggamma_Real16([1 x i32])

declare float @unary_loggamma_Real32(float)

declare float @checked_unary_loggamma_Real32(float)

declare double @unary_loggamma_Real64(double)

declare double @checked_unary_loggamma_Real64(double)

declare void @unary_loggamma_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_loggamma_ComplexReal32(float*, float*, float, float)

declare void @unary_loggamma_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_loggamma_ComplexReal64(double*, double*, double, double)

declare i8 @unary_logistic_Integer8(i8)

declare i8 @checked_unary_logistic_Integer8(i8)

declare i16 @unary_logistic_Integer16(i16)

declare i16 @checked_unary_logistic_Integer16(i16)

declare i32 @unary_logistic_Integer32(i32)

declare i32 @checked_unary_logistic_Integer32(i32)

declare i64 @unary_logistic_Integer64(i64)

declare i64 @checked_unary_logistic_Integer64(i64)

declare i8 @unary_logistic_UnsignedInteger8(i8)

declare i8 @checked_unary_logistic_UnsignedInteger8(i8)

declare i16 @unary_logistic_UnsignedInteger16(i16)

declare i16 @checked_unary_logistic_UnsignedInteger16(i16)

declare i32 @unary_logistic_UnsignedInteger32(i32)

declare i32 @checked_unary_logistic_UnsignedInteger32(i32)

declare i64 @unary_logistic_UnsignedInteger64(i64)

declare i64 @checked_unary_logistic_UnsignedInteger64(i64)

declare i16 @unary_logistic_Real16([1 x i32])

declare i16 @checked_unary_logistic_Real16([1 x i32])

declare float @unary_logistic_Real32(float)

declare float @checked_unary_logistic_Real32(float)

declare double @unary_logistic_Real64(double)

declare double @checked_unary_logistic_Real64(double)

declare void @unary_logistic_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_logistic_ComplexReal32(float*, float*, float, float)

declare void @unary_logistic_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_logistic_ComplexReal64(double*, double*, double, double)

declare i8 @unary_lucasl_Integer8(i8)

declare i8 @checked_unary_lucasl_Integer8(i8)

declare i16 @unary_lucasl_Integer16(i16)

declare i16 @checked_unary_lucasl_Integer16(i16)

declare i32 @unary_lucasl_Integer32(i32)

declare i32 @checked_unary_lucasl_Integer32(i32)

declare i64 @unary_lucasl_Integer64(i64)

declare i64 @checked_unary_lucasl_Integer64(i64)

declare i8 @unary_lucasl_UnsignedInteger8(i8)

declare i8 @checked_unary_lucasl_UnsignedInteger8(i8)

declare i16 @unary_lucasl_UnsignedInteger16(i16)

declare i16 @checked_unary_lucasl_UnsignedInteger16(i16)

declare i32 @unary_lucasl_UnsignedInteger32(i32)

declare i32 @checked_unary_lucasl_UnsignedInteger32(i32)

declare i64 @unary_lucasl_UnsignedInteger64(i64)

declare i64 @checked_unary_lucasl_UnsignedInteger64(i64)

declare i16 @unary_lucasl_Real16([1 x i32])

declare i16 @checked_unary_lucasl_Real16([1 x i32])

declare float @unary_lucasl_Real32(float)

declare float @checked_unary_lucasl_Real32(float)

declare double @unary_lucasl_Real64(double)

declare double @checked_unary_lucasl_Real64(double)

declare void @unary_lucasl_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_lucasl_ComplexReal32(float*, float*, float, float)

declare void @unary_lucasl_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_lucasl_ComplexReal64(double*, double*, double, double)

declare i8 @unary_minus_Integer8(i8)

declare i8 @checked_unary_minus_Integer8(i8)

declare i16 @unary_minus_Integer16(i16)

declare i16 @checked_unary_minus_Integer16(i16)

declare i32 @unary_minus_Integer32(i32)

declare i32 @checked_unary_minus_Integer32(i32)

declare i64 @unary_minus_Integer64(i64)

declare i64 @checked_unary_minus_Integer64(i64)

declare i8 @unary_minus_UnsignedInteger8(i8)

declare i8 @checked_unary_minus_UnsignedInteger8(i8)

declare i16 @unary_minus_UnsignedInteger16(i16)

declare i16 @checked_unary_minus_UnsignedInteger16(i16)

declare i32 @unary_minus_UnsignedInteger32(i32)

declare i32 @checked_unary_minus_UnsignedInteger32(i32)

declare i64 @unary_minus_UnsignedInteger64(i64)

declare i64 @checked_unary_minus_UnsignedInteger64(i64)

declare i16 @unary_minus_Real16([1 x i32])

declare i16 @checked_unary_minus_Real16([1 x i32])

declare float @unary_minus_Real32(float)

declare float @checked_unary_minus_Real32(float)

declare double @unary_minus_Real64(double)

declare double @checked_unary_minus_Real64(double)

declare void @unary_minus_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_minus_ComplexReal32(float*, float*, float, float)

declare void @unary_minus_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_minus_ComplexReal64(double*, double*, double, double)

declare i8 @unary_mod1_Integer8(i8)

declare i8 @checked_unary_mod1_Integer8(i8)

declare i16 @unary_mod1_Integer16(i16)

declare i16 @checked_unary_mod1_Integer16(i16)

declare i32 @unary_mod1_Integer32(i32)

declare i32 @checked_unary_mod1_Integer32(i32)

declare i64 @unary_mod1_Integer64(i64)

declare i64 @checked_unary_mod1_Integer64(i64)

declare i8 @unary_mod1_UnsignedInteger8(i8)

declare i8 @checked_unary_mod1_UnsignedInteger8(i8)

declare i16 @unary_mod1_UnsignedInteger16(i16)

declare i16 @checked_unary_mod1_UnsignedInteger16(i16)

declare i32 @unary_mod1_UnsignedInteger32(i32)

declare i32 @checked_unary_mod1_UnsignedInteger32(i32)

declare i64 @unary_mod1_UnsignedInteger64(i64)

declare i64 @checked_unary_mod1_UnsignedInteger64(i64)

declare i16 @unary_mod1_Real16([1 x i32])

declare i16 @checked_unary_mod1_Real16([1 x i32])

declare float @unary_mod1_Real32(float)

declare float @checked_unary_mod1_Real32(float)

declare double @unary_mod1_Real64(double)

declare double @checked_unary_mod1_Real64(double)

declare void @unary_mod1_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_mod1_ComplexReal32(float*, float*, float, float)

declare void @unary_mod1_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_mod1_ComplexReal64(double*, double*, double, double)

declare i32 @unary_neg_Integer8(i8)

declare i32 @checked_unary_neg_Integer8(i8)

declare i32 @unary_neg_Integer16(i16)

declare i32 @checked_unary_neg_Integer16(i16)

declare i32 @unary_neg_Integer32(i32)

declare i32 @checked_unary_neg_Integer32(i32)

declare i32 @unary_neg_Integer64(i64)

declare i32 @checked_unary_neg_Integer64(i64)

declare i32 @unary_neg_UnsignedInteger8(i8)

declare i32 @checked_unary_neg_UnsignedInteger8(i8)

declare i32 @unary_neg_UnsignedInteger16(i16)

declare i32 @checked_unary_neg_UnsignedInteger16(i16)

declare i32 @unary_neg_UnsignedInteger32(i32)

declare i32 @checked_unary_neg_UnsignedInteger32(i32)

declare i32 @unary_neg_UnsignedInteger64(i64)

declare i32 @checked_unary_neg_UnsignedInteger64(i64)

declare i32 @unary_neg_Real16([1 x i32])

declare i32 @checked_unary_neg_Real16([1 x i32])

declare i32 @unary_neg_Real32(float)

declare i32 @checked_unary_neg_Real32(float)

declare i32 @unary_neg_Real64(double)

declare i32 @checked_unary_neg_Real64(double)

declare i32 @unary_neg_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_neg_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_neg_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_neg_ComplexReal64(%"struct.std::complex")

declare i32 @unary_nneg_Integer8(i8)

declare i32 @checked_unary_nneg_Integer8(i8)

declare i32 @unary_nneg_Integer16(i16)

declare i32 @checked_unary_nneg_Integer16(i16)

declare i32 @unary_nneg_Integer32(i32)

declare i32 @checked_unary_nneg_Integer32(i32)

declare i32 @unary_nneg_Integer64(i64)

declare i32 @checked_unary_nneg_Integer64(i64)

declare i32 @unary_nneg_UnsignedInteger8(i8)

declare i32 @checked_unary_nneg_UnsignedInteger8(i8)

declare i32 @unary_nneg_UnsignedInteger16(i16)

declare i32 @checked_unary_nneg_UnsignedInteger16(i16)

declare i32 @unary_nneg_UnsignedInteger32(i32)

declare i32 @checked_unary_nneg_UnsignedInteger32(i32)

declare i32 @unary_nneg_UnsignedInteger64(i64)

declare i32 @checked_unary_nneg_UnsignedInteger64(i64)

declare i32 @unary_nneg_Real16([1 x i32])

declare i32 @checked_unary_nneg_Real16([1 x i32])

declare i32 @unary_nneg_Real32(float)

declare i32 @checked_unary_nneg_Real32(float)

declare i32 @unary_nneg_Real64(double)

declare i32 @checked_unary_nneg_Real64(double)

declare i32 @unary_nneg_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_nneg_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_nneg_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_nneg_ComplexReal64(%"struct.std::complex")

declare i32 @unary_npos_Integer8(i8)

declare i32 @checked_unary_npos_Integer8(i8)

declare i32 @unary_npos_Integer16(i16)

declare i32 @checked_unary_npos_Integer16(i16)

declare i32 @unary_npos_Integer32(i32)

declare i32 @checked_unary_npos_Integer32(i32)

declare i32 @unary_npos_Integer64(i64)

declare i32 @checked_unary_npos_Integer64(i64)

declare i32 @unary_npos_UnsignedInteger8(i8)

declare i32 @checked_unary_npos_UnsignedInteger8(i8)

declare i32 @unary_npos_UnsignedInteger16(i16)

declare i32 @checked_unary_npos_UnsignedInteger16(i16)

declare i32 @unary_npos_UnsignedInteger32(i32)

declare i32 @checked_unary_npos_UnsignedInteger32(i32)

declare i32 @unary_npos_UnsignedInteger64(i64)

declare i32 @checked_unary_npos_UnsignedInteger64(i64)

declare i32 @unary_npos_Real16([1 x i32])

declare i32 @checked_unary_npos_Real16([1 x i32])

declare i32 @unary_npos_Real32(float)

declare i32 @checked_unary_npos_Real32(float)

declare i32 @unary_npos_Real64(double)

declare i32 @checked_unary_npos_Real64(double)

declare i32 @unary_npos_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_npos_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_npos_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_npos_ComplexReal64(%"struct.std::complex")

declare i32 @unary_oddq_Integer8(i8)

declare i32 @checked_unary_oddq_Integer8(i8)

declare i32 @unary_oddq_Integer16(i16)

declare i32 @checked_unary_oddq_Integer16(i16)

declare i32 @unary_oddq_Integer32(i32)

declare i32 @checked_unary_oddq_Integer32(i32)

declare i32 @unary_oddq_Integer64(i64)

declare i32 @checked_unary_oddq_Integer64(i64)

declare i32 @unary_oddq_UnsignedInteger8(i8)

declare i32 @checked_unary_oddq_UnsignedInteger8(i8)

declare i32 @unary_oddq_UnsignedInteger16(i16)

declare i32 @checked_unary_oddq_UnsignedInteger16(i16)

declare i32 @unary_oddq_UnsignedInteger32(i32)

declare i32 @checked_unary_oddq_UnsignedInteger32(i32)

declare i32 @unary_oddq_UnsignedInteger64(i64)

declare i32 @checked_unary_oddq_UnsignedInteger64(i64)

declare i32 @unary_oddq_Real16([1 x i32])

declare i32 @checked_unary_oddq_Real16([1 x i32])

declare i32 @unary_oddq_Real32(float)

declare i32 @checked_unary_oddq_Real32(float)

declare i32 @unary_oddq_Real64(double)

declare i32 @checked_unary_oddq_Real64(double)

declare i32 @unary_oddq_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_oddq_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_oddq_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_oddq_ComplexReal64(%"struct.std::complex")

declare i8 @unary_one_Integer8(i8)

declare i8 @checked_unary_one_Integer8(i8)

declare i16 @unary_one_Integer16(i16)

declare i16 @checked_unary_one_Integer16(i16)

declare i32 @unary_one_Integer32(i32)

declare i32 @checked_unary_one_Integer32(i32)

declare i64 @unary_one_Integer64(i64)

declare i64 @checked_unary_one_Integer64(i64)

declare i8 @unary_one_UnsignedInteger8(i8)

declare i8 @checked_unary_one_UnsignedInteger8(i8)

declare i16 @unary_one_UnsignedInteger16(i16)

declare i16 @checked_unary_one_UnsignedInteger16(i16)

declare i32 @unary_one_UnsignedInteger32(i32)

declare i32 @checked_unary_one_UnsignedInteger32(i32)

declare i64 @unary_one_UnsignedInteger64(i64)

declare i64 @checked_unary_one_UnsignedInteger64(i64)

declare i16 @unary_one_Real16([1 x i32])

declare i16 @checked_unary_one_Real16([1 x i32])

declare float @unary_one_Real32(float)

declare float @checked_unary_one_Real32(float)

declare double @unary_one_Real64(double)

declare double @checked_unary_one_Real64(double)

declare void @unary_one_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_one_ComplexReal32(float*, float*, float, float)

declare void @unary_one_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_one_ComplexReal64(double*, double*, double, double)

declare i32 @unary_pos_Integer8(i8)

declare i32 @checked_unary_pos_Integer8(i8)

declare i32 @unary_pos_Integer16(i16)

declare i32 @checked_unary_pos_Integer16(i16)

declare i32 @unary_pos_Integer32(i32)

declare i32 @checked_unary_pos_Integer32(i32)

declare i32 @unary_pos_Integer64(i64)

declare i32 @checked_unary_pos_Integer64(i64)

declare i32 @unary_pos_UnsignedInteger8(i8)

declare i32 @checked_unary_pos_UnsignedInteger8(i8)

declare i32 @unary_pos_UnsignedInteger16(i16)

declare i32 @checked_unary_pos_UnsignedInteger16(i16)

declare i32 @unary_pos_UnsignedInteger32(i32)

declare i32 @checked_unary_pos_UnsignedInteger32(i32)

declare i32 @unary_pos_UnsignedInteger64(i64)

declare i32 @checked_unary_pos_UnsignedInteger64(i64)

declare i32 @unary_pos_Real16([1 x i32])

declare i32 @checked_unary_pos_Real16([1 x i32])

declare i32 @unary_pos_Real32(float)

declare i32 @checked_unary_pos_Real32(float)

declare i32 @unary_pos_Real64(double)

declare i32 @checked_unary_pos_Real64(double)

declare i32 @unary_pos_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_pos_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_pos_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_pos_ComplexReal64(%"struct.std::complex")

declare i8 @unary_ramp_Integer8(i8)

declare i8 @checked_unary_ramp_Integer8(i8)

declare i16 @unary_ramp_Integer16(i16)

declare i16 @checked_unary_ramp_Integer16(i16)

declare i32 @unary_ramp_Integer32(i32)

declare i32 @checked_unary_ramp_Integer32(i32)

declare i64 @unary_ramp_Integer64(i64)

declare i64 @checked_unary_ramp_Integer64(i64)

declare i8 @unary_ramp_UnsignedInteger8(i8)

declare i8 @checked_unary_ramp_UnsignedInteger8(i8)

declare i16 @unary_ramp_UnsignedInteger16(i16)

declare i16 @checked_unary_ramp_UnsignedInteger16(i16)

declare i32 @unary_ramp_UnsignedInteger32(i32)

declare i32 @checked_unary_ramp_UnsignedInteger32(i32)

declare i64 @unary_ramp_UnsignedInteger64(i64)

declare i64 @checked_unary_ramp_UnsignedInteger64(i64)

declare i16 @unary_ramp_Real16([1 x i32])

declare i16 @checked_unary_ramp_Real16([1 x i32])

declare float @unary_ramp_Real32(float)

declare float @checked_unary_ramp_Real32(float)

declare double @unary_ramp_Real64(double)

declare double @checked_unary_ramp_Real64(double)

declare void @unary_ramp_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_ramp_ComplexReal32(float*, float*, float, float)

declare void @unary_ramp_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_ramp_ComplexReal64(double*, double*, double, double)

declare i8 @unary_re_Integer8(i8)

declare i8 @checked_unary_re_Integer8(i8)

declare i16 @unary_re_Integer16(i16)

declare i16 @checked_unary_re_Integer16(i16)

declare i32 @unary_re_Integer32(i32)

declare i32 @checked_unary_re_Integer32(i32)

declare i64 @unary_re_Integer64(i64)

declare i64 @checked_unary_re_Integer64(i64)

declare i8 @unary_re_UnsignedInteger8(i8)

declare i8 @checked_unary_re_UnsignedInteger8(i8)

declare i16 @unary_re_UnsignedInteger16(i16)

declare i16 @checked_unary_re_UnsignedInteger16(i16)

declare i32 @unary_re_UnsignedInteger32(i32)

declare i32 @checked_unary_re_UnsignedInteger32(i32)

declare i64 @unary_re_UnsignedInteger64(i64)

declare i64 @checked_unary_re_UnsignedInteger64(i64)

declare i16 @unary_re_Real16([1 x i32])

declare i16 @checked_unary_re_Real16([1 x i32])

declare float @unary_re_Real32(float)

declare float @checked_unary_re_Real32(float)

declare double @unary_re_Real64(double)

declare double @checked_unary_re_Real64(double)

declare float @unary_re_ComplexReal32(%"struct.std::complex.203")

declare float @checked_unary_re_ComplexReal32(%"struct.std::complex.203")

declare double @unary_re_ComplexReal64(%"struct.std::complex")

declare double @checked_unary_re_ComplexReal64(%"struct.std::complex")

declare i8 @unary_recip_Integer8(i8)

declare i8 @checked_unary_recip_Integer8(i8)

declare i16 @unary_recip_Integer16(i16)

declare i16 @checked_unary_recip_Integer16(i16)

declare i32 @unary_recip_Integer32(i32)

declare i32 @checked_unary_recip_Integer32(i32)

declare i64 @unary_recip_Integer64(i64)

declare i64 @checked_unary_recip_Integer64(i64)

declare i8 @unary_recip_UnsignedInteger8(i8)

declare i8 @checked_unary_recip_UnsignedInteger8(i8)

declare i16 @unary_recip_UnsignedInteger16(i16)

declare i16 @checked_unary_recip_UnsignedInteger16(i16)

declare i32 @unary_recip_UnsignedInteger32(i32)

declare i32 @checked_unary_recip_UnsignedInteger32(i32)

declare i64 @unary_recip_UnsignedInteger64(i64)

declare i64 @checked_unary_recip_UnsignedInteger64(i64)

declare i16 @unary_recip_Real16([1 x i32])

declare i16 @checked_unary_recip_Real16([1 x i32])

declare float @unary_recip_Real32(float)

declare float @checked_unary_recip_Real32(float)

declare double @unary_recip_Real64(double)

declare double @checked_unary_recip_Real64(double)

declare void @unary_recip_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_recip_ComplexReal32(float*, float*, float, float)

declare void @unary_recip_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_recip_ComplexReal64(double*, double*, double, double)

declare i8 @unary_round_Integer8(i8)

declare i8 @checked_unary_round_Integer8(i8)

declare i16 @unary_round_Integer16(i16)

declare i16 @checked_unary_round_Integer16(i16)

declare i32 @unary_round_Integer32(i32)

declare i32 @checked_unary_round_Integer32(i32)

declare i64 @unary_round_Integer64(i64)

declare i64 @checked_unary_round_Integer64(i64)

declare i8 @unary_round_UnsignedInteger8(i8)

declare i8 @checked_unary_round_UnsignedInteger8(i8)

declare i16 @unary_round_UnsignedInteger16(i16)

declare i16 @checked_unary_round_UnsignedInteger16(i16)

declare i32 @unary_round_UnsignedInteger32(i32)

declare i32 @checked_unary_round_UnsignedInteger32(i32)

declare i64 @unary_round_UnsignedInteger64(i64)

declare i64 @checked_unary_round_UnsignedInteger64(i64)

declare i32 @unary_round_Real16([1 x i32])

declare i32 @checked_unary_round_Real16([1 x i32])

declare i32 @unary_round_Real32(float)

declare i32 @checked_unary_round_Real32(float)

declare i32 @unary_round_Real64(double)

declare i32 @checked_unary_round_Real64(double)

declare i32 @unary_round_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_round_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_round_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_round_ComplexReal64(%"struct.std::complex")

declare i8 @unary_rsqrt_Integer8(i8)

declare i8 @checked_unary_rsqrt_Integer8(i8)

declare i16 @unary_rsqrt_Integer16(i16)

declare i16 @checked_unary_rsqrt_Integer16(i16)

declare i32 @unary_rsqrt_Integer32(i32)

declare i32 @checked_unary_rsqrt_Integer32(i32)

declare i64 @unary_rsqrt_Integer64(i64)

declare i64 @checked_unary_rsqrt_Integer64(i64)

declare i8 @unary_rsqrt_UnsignedInteger8(i8)

declare i8 @checked_unary_rsqrt_UnsignedInteger8(i8)

declare i16 @unary_rsqrt_UnsignedInteger16(i16)

declare i16 @checked_unary_rsqrt_UnsignedInteger16(i16)

declare i32 @unary_rsqrt_UnsignedInteger32(i32)

declare i32 @checked_unary_rsqrt_UnsignedInteger32(i32)

declare i64 @unary_rsqrt_UnsignedInteger64(i64)

declare i64 @checked_unary_rsqrt_UnsignedInteger64(i64)

declare i16 @unary_rsqrt_Real16([1 x i32])

declare i16 @checked_unary_rsqrt_Real16([1 x i32])

declare float @unary_rsqrt_Real32(float)

declare float @checked_unary_rsqrt_Real32(float)

declare double @unary_rsqrt_Real64(double)

declare double @checked_unary_rsqrt_Real64(double)

declare void @unary_rsqrt_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_rsqrt_ComplexReal32(float*, float*, float, float)

declare void @unary_rsqrt_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_rsqrt_ComplexReal64(double*, double*, double, double)

declare i8 @unary_sec_Integer8(i8)

declare i8 @checked_unary_sec_Integer8(i8)

declare i16 @unary_sec_Integer16(i16)

declare i16 @checked_unary_sec_Integer16(i16)

declare i32 @unary_sec_Integer32(i32)

declare i32 @checked_unary_sec_Integer32(i32)

declare i64 @unary_sec_Integer64(i64)

declare i64 @checked_unary_sec_Integer64(i64)

declare i8 @unary_sec_UnsignedInteger8(i8)

declare i8 @checked_unary_sec_UnsignedInteger8(i8)

declare i16 @unary_sec_UnsignedInteger16(i16)

declare i16 @checked_unary_sec_UnsignedInteger16(i16)

declare i32 @unary_sec_UnsignedInteger32(i32)

declare i32 @checked_unary_sec_UnsignedInteger32(i32)

declare i64 @unary_sec_UnsignedInteger64(i64)

declare i64 @checked_unary_sec_UnsignedInteger64(i64)

declare i16 @unary_sec_Real16([1 x i32])

declare i16 @checked_unary_sec_Real16([1 x i32])

declare float @unary_sec_Real32(float)

declare float @checked_unary_sec_Real32(float)

declare double @unary_sec_Real64(double)

declare double @checked_unary_sec_Real64(double)

declare void @unary_sec_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_sec_ComplexReal32(float*, float*, float, float)

declare void @unary_sec_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_sec_ComplexReal64(double*, double*, double, double)

declare i8 @unary_sech_Integer8(i8)

declare i8 @checked_unary_sech_Integer8(i8)

declare i16 @unary_sech_Integer16(i16)

declare i16 @checked_unary_sech_Integer16(i16)

declare i32 @unary_sech_Integer32(i32)

declare i32 @checked_unary_sech_Integer32(i32)

declare i64 @unary_sech_Integer64(i64)

declare i64 @checked_unary_sech_Integer64(i64)

declare i8 @unary_sech_UnsignedInteger8(i8)

declare i8 @checked_unary_sech_UnsignedInteger8(i8)

declare i16 @unary_sech_UnsignedInteger16(i16)

declare i16 @checked_unary_sech_UnsignedInteger16(i16)

declare i32 @unary_sech_UnsignedInteger32(i32)

declare i32 @checked_unary_sech_UnsignedInteger32(i32)

declare i64 @unary_sech_UnsignedInteger64(i64)

declare i64 @checked_unary_sech_UnsignedInteger64(i64)

declare i16 @unary_sech_Real16([1 x i32])

declare i16 @checked_unary_sech_Real16([1 x i32])

declare float @unary_sech_Real32(float)

declare float @checked_unary_sech_Real32(float)

declare double @unary_sech_Real64(double)

declare double @checked_unary_sech_Real64(double)

declare void @unary_sech_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_sech_ComplexReal32(float*, float*, float, float)

declare void @unary_sech_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_sech_ComplexReal64(double*, double*, double, double)

declare i8 @unary_sign_Integer8(i8)

declare i8 @checked_unary_sign_Integer8(i8)

declare i16 @unary_sign_Integer16(i16)

declare i16 @checked_unary_sign_Integer16(i16)

declare i32 @unary_sign_Integer32(i32)

declare i32 @checked_unary_sign_Integer32(i32)

declare i64 @unary_sign_Integer64(i64)

declare i64 @checked_unary_sign_Integer64(i64)

declare i8 @unary_sign_UnsignedInteger8(i8)

declare i8 @checked_unary_sign_UnsignedInteger8(i8)

declare i16 @unary_sign_UnsignedInteger16(i16)

declare i16 @checked_unary_sign_UnsignedInteger16(i16)

declare i32 @unary_sign_UnsignedInteger32(i32)

declare i32 @checked_unary_sign_UnsignedInteger32(i32)

declare i64 @unary_sign_UnsignedInteger64(i64)

declare i64 @checked_unary_sign_UnsignedInteger64(i64)

declare i32 @unary_sign_Real16([1 x i32])

declare i32 @checked_unary_sign_Real16([1 x i32])

declare i32 @unary_sign_Real32(float)

declare i32 @checked_unary_sign_Real32(float)

declare i32 @unary_sign_Real64(double)

declare i32 @checked_unary_sign_Real64(double)

declare void @unary_sign_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_sign_ComplexReal32(float*, float*, float, float)

declare void @unary_sign_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_sign_ComplexReal64(double*, double*, double, double)

declare i8 @unary_sin_Integer8(i8)

declare i8 @checked_unary_sin_Integer8(i8)

declare i16 @unary_sin_Integer16(i16)

declare i16 @checked_unary_sin_Integer16(i16)

declare i32 @unary_sin_Integer32(i32)

declare i32 @checked_unary_sin_Integer32(i32)

declare i64 @unary_sin_Integer64(i64)

declare i64 @checked_unary_sin_Integer64(i64)

declare i8 @unary_sin_UnsignedInteger8(i8)

declare i8 @checked_unary_sin_UnsignedInteger8(i8)

declare i16 @unary_sin_UnsignedInteger16(i16)

declare i16 @checked_unary_sin_UnsignedInteger16(i16)

declare i32 @unary_sin_UnsignedInteger32(i32)

declare i32 @checked_unary_sin_UnsignedInteger32(i32)

declare i64 @unary_sin_UnsignedInteger64(i64)

declare i64 @checked_unary_sin_UnsignedInteger64(i64)

declare i16 @unary_sin_Real16([1 x i32])

declare i16 @checked_unary_sin_Real16([1 x i32])

declare float @unary_sin_Real32(float)

declare float @checked_unary_sin_Real32(float)

declare double @unary_sin_Real64(double)

declare double @checked_unary_sin_Real64(double)

declare void @unary_sin_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_sin_ComplexReal32(float*, float*, float, float)

declare void @unary_sin_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_sin_ComplexReal64(double*, double*, double, double)

declare i8 @unary_sinc_Integer8(i8)

declare i8 @checked_unary_sinc_Integer8(i8)

declare i16 @unary_sinc_Integer16(i16)

declare i16 @checked_unary_sinc_Integer16(i16)

declare i32 @unary_sinc_Integer32(i32)

declare i32 @checked_unary_sinc_Integer32(i32)

declare i64 @unary_sinc_Integer64(i64)

declare i64 @checked_unary_sinc_Integer64(i64)

declare i8 @unary_sinc_UnsignedInteger8(i8)

declare i8 @checked_unary_sinc_UnsignedInteger8(i8)

declare i16 @unary_sinc_UnsignedInteger16(i16)

declare i16 @checked_unary_sinc_UnsignedInteger16(i16)

declare i32 @unary_sinc_UnsignedInteger32(i32)

declare i32 @checked_unary_sinc_UnsignedInteger32(i32)

declare i64 @unary_sinc_UnsignedInteger64(i64)

declare i64 @checked_unary_sinc_UnsignedInteger64(i64)

declare i16 @unary_sinc_Real16([1 x i32])

declare i16 @checked_unary_sinc_Real16([1 x i32])

declare float @unary_sinc_Real32(float)

declare float @checked_unary_sinc_Real32(float)

declare double @unary_sinc_Real64(double)

declare double @checked_unary_sinc_Real64(double)

declare void @unary_sinc_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_sinc_ComplexReal32(float*, float*, float, float)

declare void @unary_sinc_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_sinc_ComplexReal64(double*, double*, double, double)

declare i8 @unary_sinh_Integer8(i8)

declare i8 @checked_unary_sinh_Integer8(i8)

declare i16 @unary_sinh_Integer16(i16)

declare i16 @checked_unary_sinh_Integer16(i16)

declare i32 @unary_sinh_Integer32(i32)

declare i32 @checked_unary_sinh_Integer32(i32)

declare i64 @unary_sinh_Integer64(i64)

declare i64 @checked_unary_sinh_Integer64(i64)

declare i8 @unary_sinh_UnsignedInteger8(i8)

declare i8 @checked_unary_sinh_UnsignedInteger8(i8)

declare i16 @unary_sinh_UnsignedInteger16(i16)

declare i16 @checked_unary_sinh_UnsignedInteger16(i16)

declare i32 @unary_sinh_UnsignedInteger32(i32)

declare i32 @checked_unary_sinh_UnsignedInteger32(i32)

declare i64 @unary_sinh_UnsignedInteger64(i64)

declare i64 @checked_unary_sinh_UnsignedInteger64(i64)

declare i16 @unary_sinh_Real16([1 x i32])

declare i16 @checked_unary_sinh_Real16([1 x i32])

declare float @unary_sinh_Real32(float)

declare float @checked_unary_sinh_Real32(float)

declare double @unary_sinh_Real64(double)

declare double @checked_unary_sinh_Real64(double)

declare void @unary_sinh_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_sinh_ComplexReal32(float*, float*, float, float)

declare void @unary_sinh_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_sinh_ComplexReal64(double*, double*, double, double)

declare i8 @unary_sqrt_Integer8(i8)

declare i8 @checked_unary_sqrt_Integer8(i8)

declare i16 @unary_sqrt_Integer16(i16)

declare i16 @checked_unary_sqrt_Integer16(i16)

declare i32 @unary_sqrt_Integer32(i32)

declare i32 @checked_unary_sqrt_Integer32(i32)

declare i64 @unary_sqrt_Integer64(i64)

declare i64 @checked_unary_sqrt_Integer64(i64)

declare i8 @unary_sqrt_UnsignedInteger8(i8)

declare i8 @checked_unary_sqrt_UnsignedInteger8(i8)

declare i16 @unary_sqrt_UnsignedInteger16(i16)

declare i16 @checked_unary_sqrt_UnsignedInteger16(i16)

declare i32 @unary_sqrt_UnsignedInteger32(i32)

declare i32 @checked_unary_sqrt_UnsignedInteger32(i32)

declare i64 @unary_sqrt_UnsignedInteger64(i64)

declare i64 @checked_unary_sqrt_UnsignedInteger64(i64)

declare i16 @unary_sqrt_Real16([1 x i32])

declare i16 @checked_unary_sqrt_Real16([1 x i32])

declare float @unary_sqrt_Real32(float)

declare float @checked_unary_sqrt_Real32(float)

declare double @unary_sqrt_Real64(double)

declare double @checked_unary_sqrt_Real64(double)

declare void @unary_sqrt_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_sqrt_ComplexReal32(float*, float*, float, float)

declare void @unary_sqrt_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_sqrt_ComplexReal64(double*, double*, double, double)

declare i8 @unary_square_Integer8(i8)

declare i8 @checked_unary_square_Integer8(i8)

declare i16 @unary_square_Integer16(i16)

declare i16 @checked_unary_square_Integer16(i16)

declare i32 @unary_square_Integer32(i32)

declare i32 @checked_unary_square_Integer32(i32)

declare i64 @unary_square_Integer64(i64)

declare i64 @checked_unary_square_Integer64(i64)

declare i8 @unary_square_UnsignedInteger8(i8)

declare i8 @checked_unary_square_UnsignedInteger8(i8)

declare i16 @unary_square_UnsignedInteger16(i16)

declare i16 @checked_unary_square_UnsignedInteger16(i16)

declare i32 @unary_square_UnsignedInteger32(i32)

declare i32 @checked_unary_square_UnsignedInteger32(i32)

declare i64 @unary_square_UnsignedInteger64(i64)

declare i64 @checked_unary_square_UnsignedInteger64(i64)

declare i16 @unary_square_Real16([1 x i32])

declare i16 @checked_unary_square_Real16([1 x i32])

declare float @unary_square_Real32(float)

declare float @checked_unary_square_Real32(float)

declare double @unary_square_Real64(double)

declare double @checked_unary_square_Real64(double)

declare void @unary_square_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_square_ComplexReal32(float*, float*, float, float)

declare void @unary_square_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_square_ComplexReal64(double*, double*, double, double)

declare i8 @unary_tan_Integer8(i8)

declare i8 @checked_unary_tan_Integer8(i8)

declare i16 @unary_tan_Integer16(i16)

declare i16 @checked_unary_tan_Integer16(i16)

declare i32 @unary_tan_Integer32(i32)

declare i32 @checked_unary_tan_Integer32(i32)

declare i64 @unary_tan_Integer64(i64)

declare i64 @checked_unary_tan_Integer64(i64)

declare i8 @unary_tan_UnsignedInteger8(i8)

declare i8 @checked_unary_tan_UnsignedInteger8(i8)

declare i16 @unary_tan_UnsignedInteger16(i16)

declare i16 @checked_unary_tan_UnsignedInteger16(i16)

declare i32 @unary_tan_UnsignedInteger32(i32)

declare i32 @checked_unary_tan_UnsignedInteger32(i32)

declare i64 @unary_tan_UnsignedInteger64(i64)

declare i64 @checked_unary_tan_UnsignedInteger64(i64)

declare i16 @unary_tan_Real16([1 x i32])

declare i16 @checked_unary_tan_Real16([1 x i32])

declare float @unary_tan_Real32(float)

declare float @checked_unary_tan_Real32(float)

declare double @unary_tan_Real64(double)

declare double @checked_unary_tan_Real64(double)

declare void @unary_tan_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_tan_ComplexReal32(float*, float*, float, float)

declare void @unary_tan_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_tan_ComplexReal64(double*, double*, double, double)

declare i8 @unary_tanh_Integer8(i8)

declare i8 @checked_unary_tanh_Integer8(i8)

declare i16 @unary_tanh_Integer16(i16)

declare i16 @checked_unary_tanh_Integer16(i16)

declare i32 @unary_tanh_Integer32(i32)

declare i32 @checked_unary_tanh_Integer32(i32)

declare i64 @unary_tanh_Integer64(i64)

declare i64 @checked_unary_tanh_Integer64(i64)

declare i8 @unary_tanh_UnsignedInteger8(i8)

declare i8 @checked_unary_tanh_UnsignedInteger8(i8)

declare i16 @unary_tanh_UnsignedInteger16(i16)

declare i16 @checked_unary_tanh_UnsignedInteger16(i16)

declare i32 @unary_tanh_UnsignedInteger32(i32)

declare i32 @checked_unary_tanh_UnsignedInteger32(i32)

declare i64 @unary_tanh_UnsignedInteger64(i64)

declare i64 @checked_unary_tanh_UnsignedInteger64(i64)

declare i16 @unary_tanh_Real16([1 x i32])

declare i16 @checked_unary_tanh_Real16([1 x i32])

declare float @unary_tanh_Real32(float)

declare float @checked_unary_tanh_Real32(float)

declare double @unary_tanh_Real64(double)

declare double @checked_unary_tanh_Real64(double)

declare void @unary_tanh_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_tanh_ComplexReal32(float*, float*, float, float)

declare void @unary_tanh_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_tanh_ComplexReal64(double*, double*, double, double)

declare i8 @unary_unitize_Integer8(i8)

declare i8 @checked_unary_unitize_Integer8(i8)

declare i16 @unary_unitize_Integer16(i16)

declare i16 @checked_unary_unitize_Integer16(i16)

declare i32 @unary_unitize_Integer32(i32)

declare i32 @checked_unary_unitize_Integer32(i32)

declare i64 @unary_unitize_Integer64(i64)

declare i64 @checked_unary_unitize_Integer64(i64)

declare i8 @unary_unitize_UnsignedInteger8(i8)

declare i8 @checked_unary_unitize_UnsignedInteger8(i8)

declare i16 @unary_unitize_UnsignedInteger16(i16)

declare i16 @checked_unary_unitize_UnsignedInteger16(i16)

declare i32 @unary_unitize_UnsignedInteger32(i32)

declare i32 @checked_unary_unitize_UnsignedInteger32(i32)

declare i64 @unary_unitize_UnsignedInteger64(i64)

declare i64 @checked_unary_unitize_UnsignedInteger64(i64)

declare i32 @unary_unitize_Real16([1 x i32])

declare i32 @checked_unary_unitize_Real16([1 x i32])

declare i32 @unary_unitize_Real32(float)

declare i32 @checked_unary_unitize_Real32(float)

declare i32 @unary_unitize_Real64(double)

declare i32 @checked_unary_unitize_Real64(double)

declare i32 @unary_unitize_ComplexReal32(%"struct.std::complex.203")

declare i32 @checked_unary_unitize_ComplexReal32(%"struct.std::complex.203")

declare i32 @unary_unitize_ComplexReal64(%"struct.std::complex")

declare i32 @checked_unary_unitize_ComplexReal64(%"struct.std::complex")

declare i8 @unary_zero_Integer8(i8)

declare i8 @checked_unary_zero_Integer8(i8)

declare i16 @unary_zero_Integer16(i16)

declare i16 @checked_unary_zero_Integer16(i16)

declare i32 @unary_zero_Integer32(i32)

declare i32 @checked_unary_zero_Integer32(i32)

declare i64 @unary_zero_Integer64(i64)

declare i64 @checked_unary_zero_Integer64(i64)

declare i8 @unary_zero_UnsignedInteger8(i8)

declare i8 @checked_unary_zero_UnsignedInteger8(i8)

declare i16 @unary_zero_UnsignedInteger16(i16)

declare i16 @checked_unary_zero_UnsignedInteger16(i16)

declare i32 @unary_zero_UnsignedInteger32(i32)

declare i32 @checked_unary_zero_UnsignedInteger32(i32)

declare i64 @unary_zero_UnsignedInteger64(i64)

declare i64 @checked_unary_zero_UnsignedInteger64(i64)

declare i16 @unary_zero_Real16([1 x i32])

declare i16 @checked_unary_zero_Real16([1 x i32])

declare float @unary_zero_Real32(float)

declare float @checked_unary_zero_Real32(float)

declare double @unary_zero_Real64(double)

declare double @checked_unary_zero_Real64(double)

declare void @unary_zero_ComplexReal32(float*, float*, float, float)

declare void @checked_unary_zero_ComplexReal32(float*, float*, float, float)

declare void @unary_zero_ComplexReal64(double*, double*, double, double)

declare void @checked_unary_zero_ComplexReal64(double*, double*, double, double)

declare i8* @to_string_Integer8(i8)

declare i8* @strcpy(i8*, i8*)

declare i8* @to_string_Integer16(i16)

declare i8* @to_string_Integer32(i32)

declare i8* @to_string_Integer64(i64)

declare i8* @to_string_UnsignedInteger8(i8)

declare i8* @to_string_UnsignedInteger16(i16)

declare i8* @to_string_UnsignedInteger32(i32)

declare i8* @to_string_UnsignedInteger64(i64)

declare i8* @to_string_Real16([1 x i32])

declare i8* @to_string_Real32(float)

declare i8* @to_string_Real64(double)

declare i8* @to_string_ComplexReal32(%"struct.std::complex.203")

declare void @_ZSt20__throw_length_errorPKc(i8*)

declare %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcj(%"class.std::__cxx11::basic_string"*, i8*, i32)

declare %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEjjPKcj(%"class.std::__cxx11::basic_string"*, i32, i32, i8*, i32)

declare i8* @to_string_ComplexReal64(%"struct.std::complex")

declare void @to_string_free(i8*)

declare void @_GLOBAL__sub_I_traits.cpp()

declare void @_GLOBAL__sub_I_logger.cpp()

declare void @_GLOBAL__sub_I_parallel_for.cpp()

declare i32 @parallel_for_Integer64_Integer64(i64*, i64, i64*, i64 (i64)*)

declare i32 @_ZNSt6thread20hardware_concurrencyEv()

declare void @_ZNSt6vectorISt6futureIvESaIS1_EE7reserveEj(%"class.std::vector"*, i32)

declare void @_ZSt17__throw_bad_allocv()

declare i8* @_Znwj(i32)

declare %"struct.std::__future_base::_Result_base"* @_ZNSt13__future_base12_Result_baseC2Ev(%"struct.std::__future_base::_Result_base"*)

declare i32 @pthread_create(i32*, %union.pthread_attr_t*, i8* (i8*)*, i8*)

declare void @_ZNSt6thread15_M_start_threadESt10unique_ptrINS_6_StateESt14default_deleteIS1_EEPFvvE(%"class.std::thread"*, %"class.std::unique_ptr.25"*, void ()*)

declare %"class.std::exception"* @_ZNSt3_V216generic_categoryEv()

declare void @__cxa_rethrow()

declare void @_ZSt20__throw_future_errori(i32)

declare void @_ZNSt6vectorISt6futureIvESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_(%"class.std::vector"*, %"class.std::future"*)

declare i1 @_ZNSt28__atomic_futex_unsigned_base19_M_futex_wait_untilEPjjbNSt6chrono8durationIxSt5ratioILx1ELx1EEEENS2_IxS3_ILx1ELx1000000000EEEE(%"class.std::ios_base::Init"*, i32*, i32, i1, [1 x i64], [1 x i64])

declare %"class.std::__future_base::_Deferred_state"* @"_ZNSt13__future_base15_Deferred_stateISt12_Bind_simpleIFZN5utilsL12parallel_forINS2_16execution_policyIvEEPxS6_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SC_T1_SD_OT2_EUlvE_vEEvED2Ev"(%"class.std::__future_base::_Deferred_state"*)

declare void @"_ZNSt13__future_base15_Deferred_stateISt12_Bind_simpleIFZN5utilsL12parallel_forINS2_16execution_policyIvEEPxS6_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SC_T1_SD_OT2_EUlvE_vEEvED0Ev"(%"class.std::__future_base::_Deferred_state"*)

declare void @"_ZNSt13__future_base15_Deferred_stateISt12_Bind_simpleIFZN5utilsL12parallel_forINS2_16execution_policyIvEEPxS6_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SC_T1_SD_OT2_EUlvE_vEEvE17_M_complete_asyncEv"(%"class.std::__future_base::_Deferred_state"*)

declare i1 @"_ZNKSt13__future_base15_Deferred_stateISt12_Bind_simpleIFZN5utilsL12parallel_forINS2_16execution_policyIvEEPxS6_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SC_T1_SD_OT2_EUlvE_vEEvE21_M_is_deferred_futureEv"(%"class.std::__future_base::_Deferred_state"*)

declare void @"_ZNSt17_Function_handlerIFSt10unique_ptrINSt13__future_base12_Result_baseENS2_8_DeleterEEvENS1_12_Task_setterIS0_INS1_7_ResultIvEES3_ESt12_Bind_simpleIFZN5utilsL12parallel_forINSB_16execution_policyIvEEPxSF_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SL_T1_SM_OT2_EUlvE_vEEvEEE9_M_invokeERKSt9_Any_data"(%"class.std::unique_ptr"*, %"union.std::_Any_data"*)

declare i1 @"_ZNSt14_Function_base13_Base_managerINSt13__future_base12_Task_setterISt10unique_ptrINS1_7_ResultIvEENS1_12_Result_base8_DeleterEESt12_Bind_simpleIFZN5utilsL12parallel_forINSA_16execution_policyIvEEPxSE_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SK_T1_SL_OT2_EUlvE_vEEvEEE10_M_managerERSt9_Any_dataRKST_St18_Manager_operation"(%"union.std::_Any_data"*, %"union.std::_Any_data"*, i32)

declare void @_ZNSt13__future_base13_State_baseV29_M_do_setEPSt8functionIFSt10unique_ptrINS_12_Result_baseENS3_8_DeleterEEvEEPb(%"class.std::__future_base::_State_baseV2"*, %"class.std::function"*, i8*)

declare void @_ZZSt9call_onceIMNSt13__future_base13_State_baseV2EFvPSt8functionIFSt10unique_ptrINS0_12_Result_baseENS4_8_DeleterEEvEEPbEJPS1_S9_SA_EEvRSt9once_flagOT_DpOT0_ENUlvE0_8__invokeEv()

declare void @__once_proxy()

declare i32 @pthread_once(i32*, void ()*)

declare void @_ZNSt28__atomic_futex_unsigned_base19_M_futex_notify_allEPj(i32*)

declare void @_ZSt25__throw_bad_function_callv()

declare void @_ZSt17current_exceptionv(%struct.st_KMutex*)

declare void @_ZNSt15__exception_ptr13exception_ptr4swapERS0_(%struct.st_KMutex*, %struct.st_KMutex*)

declare %struct.st_KMutex* @_ZNSt15__exception_ptr13exception_ptrD1Ev(%struct.st_KMutex*)

declare %"class.std::_Sp_counted_ptr_inplace.53"* @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base15_Deferred_stateISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EED2Ev"(%"class.std::_Sp_counted_ptr_inplace.53"*)

declare void @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base15_Deferred_stateISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EED0Ev"(%"class.std::_Sp_counted_ptr_inplace.53"*)

declare void @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base15_Deferred_stateISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv"(%"class.std::_Sp_counted_ptr_inplace.53"*)

declare void @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base15_Deferred_stateISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv"(%"class.std::_Sp_counted_ptr_inplace.53"*)

declare i8* @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base15_Deferred_stateISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info"(%"class.std::_Sp_counted_ptr_inplace.53"*, %"class.std::type_info"*)

declare i32 @strcmp(i8*, i8*)

declare %"class.std::__future_base::_State_baseV2"* @_ZNSt13__future_base13_State_baseV2D2Ev(%"class.std::__future_base::_State_baseV2"*)

declare void @_ZNSt13__future_base13_State_baseV2D0Ev(%"class.std::__future_base::_State_baseV2"*)

declare void @_ZNSt13__future_base13_State_baseV217_M_complete_asyncEv(%"class.std::__future_base::_State_baseV2"*)

declare i1 @_ZNKSt13__future_base13_State_baseV221_M_is_deferred_futureEv(%"class.std::__future_base::_State_baseV2"*)

declare %"class.std::__future_base::_Async_state_commonV2"* @_ZNSt13__future_base21_Async_state_commonV2D2Ev(%"class.std::__future_base::_Async_state_commonV2"*)

declare void @_ZNSt13__future_base21_Async_state_commonV2D0Ev(%"class.std::__future_base::_Async_state_commonV2"*)

declare void @_ZNSt13__future_base21_Async_state_commonV217_M_complete_asyncEv(%"class.std::__future_base::_Async_state_commonV2"*)

declare void @_ZNSt6thread4joinEv(%"class.std::thread"*)

declare void @_ZZSt9call_onceIMSt6threadFvvEJPS0_EEvRSt9once_flagOT_DpOT0_ENUlvE0_8__invokeEv()

declare %"class.std::exception"* @_ZNSt6thread6_StateD2Ev(%"class.std::exception"*)

declare void @"_ZNSt6thread11_State_implISt12_Bind_simpleIFZNSt13__future_base17_Async_state_implIS1_IFZN5utilsL12parallel_forINS4_16execution_policyIvEEPxS8_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SE_T1_SF_OT2_EUlvE_vEEvEC1EOSK_EUlvE_vEEED0Ev"(%"struct.std::thread::_State_impl"*)

declare void @"_ZNSt6thread11_State_implISt12_Bind_simpleIFZNSt13__future_base17_Async_state_implIS1_IFZN5utilsL12parallel_forINS4_16execution_policyIvEEPxS8_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SE_T1_SF_OT2_EUlvE_vEEvEC1EOSK_EUlvE_vEEE6_M_runEv"(%"struct.std::thread::_State_impl"*)

declare void @_ZNSt13__future_base13_State_baseV216_M_break_promiseESt10unique_ptrINS_12_Result_baseENS2_8_DeleterEE(%"class.std::__future_base::_State_baseV2"*, %"class.std::unique_ptr"*)

declare %"class.std::exception"* @_ZSt15future_categoryv()

declare %"class.std::future_error"* @_ZNSt12future_errorC2ESt10error_code(%"class.std::future_error"*, [2 x i32])

declare void @_ZSt18make_exception_ptrISt12future_errorENSt15__exception_ptr13exception_ptrET_(%struct.st_KMutex*, %"class.std::future_error"*)

declare %"class.std::future_error"* @_ZNSt12future_errorD1Ev(%"class.std::future_error"*)

declare %"class.std::logic_error"* @_ZNSt11logic_errorC2ERKS_(%"class.std::logic_error"*, %"class.std::logic_error"*)

declare %"class.std::logic_error"* @_ZNSt11logic_errorC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%"class.std::logic_error"*, %"class.std::__cxx11::basic_string"*)

declare void @_ZNSt13__future_base7_ResultIvE10_M_destroyEv(%"struct.std::__future_base::_Result"*)

declare %"struct.std::__future_base::_Result_base"* @_ZNSt13__future_base12_Result_baseD2Ev(%"struct.std::__future_base::_Result_base"*)

declare void @_ZNSt13__future_base7_ResultIvED0Ev(%"struct.std::__future_base::_Result"*)

declare %"class.std::__future_base::_Async_state_impl"* @"_ZNSt13__future_base17_Async_state_implISt12_Bind_simpleIFZN5utilsL12parallel_forINS2_16execution_policyIvEEPxS6_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SC_T1_SD_OT2_EUlvE_vEEvED2Ev"(%"class.std::__future_base::_Async_state_impl"*)

declare void @"_ZNSt13__future_base17_Async_state_implISt12_Bind_simpleIFZN5utilsL12parallel_forINS2_16execution_policyIvEEPxS6_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SC_T1_SD_OT2_EUlvE_vEEvED0Ev"(%"class.std::__future_base::_Async_state_impl"*)

declare %"class.std::unique_ptr.14"* @_ZNSt10unique_ptrINSt13__future_base7_ResultIvEENS0_12_Result_base8_DeleterEED2Ev(%"class.std::unique_ptr.14"*)

declare %"class.std::_Sp_counted_ptr_inplace"* @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base17_Async_state_implISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EED2Ev"(%"class.std::_Sp_counted_ptr_inplace"*)

declare void @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base17_Async_state_implISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EED0Ev"(%"class.std::_Sp_counted_ptr_inplace"*)

declare void @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base17_Async_state_implISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv"(%"class.std::_Sp_counted_ptr_inplace"*)

declare void @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base17_Async_state_implISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv"(%"class.std::_Sp_counted_ptr_inplace"*)

declare i8* @"_ZNSt23_Sp_counted_ptr_inplaceINSt13__future_base17_Async_state_implISt12_Bind_simpleIFZN5utilsL12parallel_forINS3_16execution_policyIvEEPxS7_Z32parallel_for_Integer64_Integer64E3$_0EE5errorRKT_T0_SD_T1_SE_OT2_EUlvE_vEEvEESaISK_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info"(%"class.std::_Sp_counted_ptr_inplace"*, %"class.std::type_info"*)

declare void @_GLOBAL__sub_I_sqrt_limits.cpp()

; Function Attrs: argmemonly nounwind
declare {}* @llvm.invariant.start.p0i8(i64, i8* nocapture) #1

declare void @_Z11InitHashingi(i32)

declare void @set_default_hash_function(i8*)

declare void @_Z14init_byte_hashi(i32)

declare i32 @crc_32_hash(i8*, i32)

declare i64 @crc_64_hash(i8*, i32)

declare void @_Z13init_crc_hashi(i32)

declare i32 @fnv_32_hash(i8*, i32)

declare i64 @fnv_64_hash(i8*, i32)

declare void @_Z13init_fnv_hashi(i32)

declare %struct.st_IntegerArrayHashTable* @New_IntegerArrayHashTable(i32, i32, i32, void (%struct.st_hash_entry*)*)

declare void @_ZL22IntegerArrayHashDeleteP13st_hash_entry(%struct.st_hash_entry*)

declare void @_ZL26IntegerArrayHashCopyDeleteP13st_hash_entry(%struct.st_hash_entry*)

declare i32 @_ZL27IntegerArrayPositionHashFunP13st_hash_entry(%struct.st_hash_entry*)

declare void @_ZL19IntegerArrayHashSetP13st_hash_entryS0_(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare i32 @_ZL29IntegerArrayPositionHashEqualP13st_hash_entryS0_(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare void @_ZL19IntegerArrayHashNewP13st_hash_entry(%struct.st_hash_entry*)

declare i32 @_ZL19IntegerArrayHashFunP13st_hash_entry(%struct.st_hash_entry*)

declare void @_ZL23IntegerArrayHashCopySetP13st_hash_entryS0_(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare i32 @_ZL21IntegerArrayHashEqualP13st_hash_entryS0_(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare void @Delete_IntegerArrayHashTable(%struct.st_IntegerArrayHashTable*)

declare %struct.st_hash_entry* @IntegerArrayHashTable_GetOrAdd(%struct.st_IntegerArrayHashTable*, i32*, i32*)

declare %struct.st_hash_entry* @IntegerArrayHashTable_Add(%struct.st_IntegerArrayHashTable*, i32*)

declare %struct.st_hash_entry* @IntegerArrayHashTable_Get(%struct.st_IntegerArrayHashTable*, i32*)

declare i32 @IntegerArrayHashTable_Remove(%struct.st_IntegerArrayHashTable*, %struct.st_hash_entry*)

declare i32 @IntegerArrayHashTable_Length(%struct.st_IntegerArrayHashTable*)

declare i32 @IntegerArrayHashTable_ByteCount(%struct.st_IntegerArrayHashTable*, i32 (%struct.st_hash_entry*)*)

declare void @_Z17init_hash_integeri(i32)

declare void @FreeString(%struct.st_hash_entry*)

declare void @HashEntry_NewGeneral(%struct.st_hash_entry*)

declare i32 @StringHash(%struct.st_hash_entry*)

declare void @SetHashString(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare i32 @EqualHashString(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare void @_GLOBAL__sub_I_hashtable.cpp()

declare %struct.st_hashtable* @New_HashTable(i32, i32 (%struct.st_hash_entry*)*, void (%struct.st_hash_entry*, %struct.st_hash_entry*)*, i32 (%struct.st_hash_entry*, %struct.st_hash_entry*)*, void (%struct.st_hash_entry*)*, void (%struct.st_hash_entry*)*)

declare void @FreeOrEmpty_HashTable(%struct.st_hashtable*, i32)

declare i8* @HashTable_GetValue(%struct.st_hashtable*, i8*)

declare %struct.st_hash_entry* @HashTable_GetUsingFunction(%struct.st_hashtable*, i8*)

declare %struct.st_hash_entry* @HashTable_Add(%struct.st_hashtable*, i8*)

declare %struct.st_hash_entry* @HashTable_GetOrAdd(%struct.st_hashtable*, i8*, i32*)

declare i32 @HashTable_RemoveElem(%struct.st_hashtable*, i8*)

declare i32 @HashTable_RemoveCommon(%struct.st_hashtable*, %struct.st_hash_entry*, i32)

declare void @HashTable_AddValue(%struct.st_hashtable*, i8*, i8*)

declare i32 @HashTable_OrOverAllEntries(%struct.st_hashtable*, i32 (%struct.st_hash_entry*, i8*)*, i8*)

declare i32 @HashTable_ByteCount(%struct.st_hashtable*, i32 (%struct.st_hash_entry*)*)

declare void @_Z14init_hashtablei(i32)

declare i32 @murmur3_32_hash(i8*, i32)

declare i32 @murmur3_32_128_hash(i8*, i32)

declare i32 @murmur3_32_32_hash(i8*, i32)

declare i64 @murmur3_64_128_hash(i8*, i32)

declare void @_Z17init_murmur3_hashi(i32)

declare i32 @sip24_32_hash(i8*, i32)

declare i64 @sip24_64_hash(i8*, i32)

declare void @_Z15init_sip24_hashi(i32)

declare void @_Z16InitRTLConstantsi(i32)

declare void @StartOutOfMemoryAbort(i32)

declare i32 @watchhandle_c(i8*)

declare void @DeinitializeRuntimeLibrary(i32)

declare void @ReinitializeRuntimeLibrary()

declare void @InitRuntimeLibrary(i32)

declare void @InitializeRuntimeLibrary()

declare void @_Z15InitInterpreteri(i32)

declare void @_GLOBAL__sub_I_function_calls.cpp()

declare void @AddInterpreterFunctionCall(i8*, i32 (%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])*, i32)

declare {}* @GetFunctionCallFunction(i8*)

declare i32 @_ZL18interpret_fc_errorP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @GetFunctionCallFlags(i8*)

declare void @InitializeFunctionCalls()

declare void @DeinitializeFunctionCalls()

declare void @_Z19init_function_callsi(i32)

declare i32 @_ZL23interpret_fc_dimensionsP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL17interpret_fc_sortP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL21interpret_fc_orderingP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL29interpret_fc_ordering_integerP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL26interpret_fc_integerdigitsP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL19interpret_fc_medianP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL17interpret_fc_takeP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL17interpret_fc_dropP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL24interpret_fc_min_integerP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL31interpret_fc_min_integer_tensorP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL32interpret_fc_min_integer_generalP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL21interpret_fc_min_realP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL28interpret_fc_min_real_tensorP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL29interpret_fc_min_real_generalP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL24interpret_fc_max_integerP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL31interpret_fc_max_integer_tensorP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL32interpret_fc_max_integer_generalP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL21interpret_fc_max_realP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL28interpret_fc_max_real_tensorP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL29interpret_fc_max_real_generalP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL19interpret_fc_dot_vvP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL16interpret_fc_dotP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL23interpret_fc_outer_listP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL24interpret_fc_outer_timesP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL23interpret_fc_outer_plusP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL19interpret_fc_insertP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL19interpret_fc_deleteP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL20interpret_fc_reverseP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL17interpret_fc_joinP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL26interpret_fc_join_at_levelP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL18interpret_fc_unionP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL23interpret_fc_complementP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL25interpret_fc_intersectionP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL24interpret_fc_rotate_leftP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL25interpret_fc_rotate_rightP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL35interpret_fc_iterator_count_integerP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL32interpret_fc_iterator_count_realP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL20interpret_fc_flattenP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL21interpret_fc_positionP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL18interpret_fc_countP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL20interpret_fc_memberQP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL18interpret_fc_freeQP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL21interpret_fc_orderedQP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL22interpret_fc_partitionP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL22interpret_fc_transposeP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL32interpret_fc_conjugate_transposeP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL21interpret_fc_pad_leftP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL22interpret_fc_pad_rightP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL18interpret_fc_totalP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL22interpret_fc_total_allP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL23interpret_fc_accumulateP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL24interpret_fc_vector_normP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL24interpret_fc_differencesP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL19interpret_fc_ratiosP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL22interpret_fc_small_detP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL26interpret_fc_RandomIntegerP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL27interpret_fc_RandomIntegersP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL23interpret_fc_RandomRealP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL24interpret_fc_RandomRealsP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL26interpret_fc_RandomComplexP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL28interpret_fc_RandomComplexesP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL25interpret_fc_RandomChoiceP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL32interpret_fc_RandomChoiceWeightsP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL25interpret_fc_RandomSampleP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL32interpret_fc_RandomSampleWeightsP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL23interpret_fc_SeedRandomP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL29interpret_fc_BlockRandomBeginP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL27interpret_fc_BlockRandomEndP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL25interpret_fc_RandomNormalP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL23interpret_fc_RandomBetaP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL25interpret_fc_LegacyRandomP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL18interpret_fc_tallyP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL16interpret_fc_bagP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL28interpret_fc_stuffbag_scalarP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL21interpret_fc_stuffbagP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL20interpret_fc_bagpartP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL27interpret_fc_compare_tensorP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL26interpret_fc_coerce_tensorP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL24interpret_fc_copy_tensorP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL18interpret_fc_crossP21st_WolframLibraryDataiP9MArgumentS1_(%struct.st_WolframLibraryData.259*, i32, %union.MArgument*, [1 x i32])

declare i32 @_ZL20CompiledRandomSamplePP13st_MDataArrayS0_S0_i(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @_ZL20CompiledRandomChoicePvP13st_MDataArrayS1_S1_(i8*, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*)

; Function Attrs: argmemonly nounwind
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1) #1

declare i32 @_ZL26interpret_fc_takedrop_impliP9MArgumentS_i(i32, %union.MArgument*, [1 x i32], i32)

declare i32 @InsertTensorToCompiledTable(%struct.st_MDataArray*, %struct.st_MDataArray*, i32*)

declare i32 @CompiledGetTensorElement(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32)

declare i32 @CompiledSetTensorElement(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @CompiledPartIndexSize(i32, i32*, i8**, %struct.part_ind*)

declare void @CompiledPartToIndex(i8*, i32, %struct.part_ind*, i32**)

declare i32 @CompiledCollectedPart(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32, i32*, i8**)

declare i32 @CompiledCollectedPartNumericArray(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32, i32*, i8**)

declare i32 @CompiledCollectedSetPart(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32, i32*, i8**)

declare i32 @GetStartEndPosForTakeOrDrop(i32, i32*, i32*, i32*, i32*)

declare i32 @CompareReals(i32, double, i32, double*)

declare i32 @CompareRealsNoTolerance(i32, i32, double*)

declare i32 @CompareComplexes(i32, double, i32, %"struct.std::complex"*, i32*)

declare i32 @CompareComplexesNoTolerance(i32, i32, %"struct.std::complex"*, i32*)

declare i32 @CompareMTensors(i32, double, i32, %struct.st_MDataArray**)

declare i32 @IteratorCountI(i32, i32, i32, i32*)

declare i32 @IteratorCountR(double, double, double, i32*)

declare %struct.st_SparseArrayAllocation* @SparseArrayAllocation_new()

declare void @SparseArrayAllocation_delete(%struct.st_SparseArrayAllocation**)

declare %struct.st_MTensorSparseArrayAllocation* @MTensorSparseArrayAllocation_new()

declare void @MTensorSparseArrayAllocation_delete(%struct.st_MTensorSparseArrayAllocation**)

declare i32 @ConsistentSparseArrayAllocationQ(%struct.st_SparseArrayAllocation*, %struct.st_SparseArrayAllocation*)

declare void @MSparseArray_deleteSparseArray(%struct.st_MSparseArray*)

declare %struct.st_SparseArraySkeleton* @SparseArraySkeleton_new(i32)

declare void @SparseArraySkeleton_delete(%struct.st_SparseArraySkeleton*)

declare %struct.st_MSparseArray* @MSparseArray_new(i32)

declare void @MSparseArray_delete(%struct.st_MSparseArray*)

declare void @SparseArraySkeleton_allocIndexMTensors(%struct.st_SparseArraySkeleton*, %struct.st_MDataArray**, %struct.st_MDataArray**)

declare void @MSparseArray_allocIndices(%struct.st_MSparseArray*)

declare %struct.st_MSparseArray* @MSparseArray_newAllocated(i32, i32*, i32)

declare %struct.st_MSparseArray* @MSparseArray_copy(%struct.st_MSparseArray*)

declare void @MSparseArray_setImplicitValue(%struct.st_MSparseArray*, i32, i8*)

declare %struct.st_MDataArray* @MSparseArray_allocValues(%struct.st_MSparseArray*, i32)

declare i32 @MSparseArray_byteCount(%struct.st_MSparseArray*)

declare void @MSparseArray_getData(%struct.st_MSparseArray*, i32*, i32**, i32*, i32**, i32**, i32*, %struct.st_MDataArray**, %struct.st_MDataArray**)

declare i32 @SparseArray_explicitPositions(%struct.st_SparseArraySkeleton*, %struct.st_MDataArray**)

declare i32 @MSparseArray_fromPositions(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MSparseArray**)

declare i32 @MSparseArray_fromMTensor(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MSparseArray**)

declare i32 @MSparseArray_resetImplicit(%struct.st_MSparseArray*, %struct.st_MDataArray*, %struct.st_MSparseArray**)

declare void @_Z16InitMSparseArrayi(i32)

declare %struct.MULTIPLE_ARRAY_HASH_STRUCT* @MultipleArrayHash_new(i32, i32*, i32, i32*, i32**)

declare void @MultipleArrayHash_clear(%struct.MULTIPLE_ARRAY_HASH_STRUCT*, i32)

declare void @MultipleArrayHash_delete(%struct.MULTIPLE_ARRAY_HASH_STRUCT*)

declare void @_Z15init_hash_indexi(i32)

declare i32 @MSparseArray_toMTensor(%struct.st_MSparseArray*, %struct.st_MDataArray**)

declare void @_GLOBAL__sub_I_mach_sparse_blas1.cpp()

declare i32 @SparseVector_dot_SparseVector_mint(i32*, i32*, i32*, i32, i32*, i32*, i32, i32*, i32*, i32)

declare i32 @SparseVector_dot_SparseVector_double(double*, i32*, i32*, i32, i32*, double*, i32, i32*, double*, i32)

declare i32 @SparseVector_dot_SparseVector_complex(%"struct.std::complex"*, i32*, i32*, i32, i32*, %"struct.std::complex"*, i32, i32*, %"struct.std::complex"*, i32)

declare i32 @SparseVector_dot_mintVector(i32*, i32, i32*, i32*, i32*, i32)

declare i32 @SparseVector_dot_doubleVector(double*, i32, i32*, double*, double*, i32)

declare i32 @SparseVector_dot_complexVector(%"struct.std::complex"*, i32, i32*, %"struct.std::complex"*, %"struct.std::complex"*, i32)

declare i32 @SparseMatrix_dot_SparseVector_mint(i32*, i32*, i32, i32, i32, i32*, i32*, i32*, i32, i32*, i32*, i32*, i32, i32)

declare i32 @SparseMatrix_dot_SparseVector_double(i32*, i32*, i32, i32, i32, i32*, i32*, double*, i32, i32*, double*, double*, i32, i32)

declare i32 @SparseMatrix_dot_SparseVector_complex(i32*, i32*, i32, i32, i32, i32*, i32*, %"struct.std::complex"*, i32, i32*, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32)

declare i32 @SparseVector_dot_mintArray(i32*, i32, i32, i32*, i32*, i32*, i32, i32, i32)

declare i32 @SparseVector_dot_doubleArray(double*, i32, i32, i32*, double*, double*, i32, i32, i32)

declare i32 @SparseVector_dot_complexArray(%"struct.std::complex"*, i32, i32, i32*, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32, i32)

declare i32 @rowmaxnz(i32, i32*)

declare i32 @machfunc_icsrmv(i32, i32, i32*, i32*, i32*, i32*, i32*, i32, i32)

declare i32 @machfunc_dcsrmv(i32, i32, i32*, i32*, double*, double*, double*, i32, i32)

declare i32 @machfunc_zcsrmv(i32, i32, i32*, i32*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32)

declare i32 @machfunc_icsrmm(i32, i32, i32, i32*, i32*, i32*, i32*, i32*, i32, i32)

declare void @_ZL11init_icsrmmPvP29st_ParallelThreadsEnvironment(i8*, %struct.st_ParallelThreadsEnvironment*)

declare void @_ZL10icsrmm_funPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @machfunc_dcsrmm(i32, i32, i32, i32*, i32*, double*, double*, double*, i32, i32)

declare i32 @machfunc_zcsrmm(i32, i32, i32, i32*, i32*, %"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32)

declare i32 @machfunc_icsrmultcsr(i32, i32, i32, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32)

declare i32 @_ZL19csrmultcsr_symboliciiiPiS_S_S_S_(i32, i32, i32*, i32*, i32*, i32*, i32*)

declare i32 @machfunc_dcsrmultcsr(i32, i32, i32, i32*, i32*, double*, i32*, i32*, double*, i32*, i32*, double*, i32)

declare i32 @machfunc_zcsrmultcsr(i32, i32, i32, i32*, i32*, %"struct.std::complex"*, i32*, i32*, %"struct.std::complex"*, i32*, i32*, %"struct.std::complex"*, i32)

declare i32 @machfunc_pcsrmultcsr(i32, i32, i32, i32*, i32*, i32*, i32*, i32*, i32*)

declare void @_Z21init_mach_sparse_blasi(i32)

declare i32 @SparseArraySkeleton_checkSorted(%struct.st_SparseArraySkeleton*)

declare void @_Z11InitMTensori(i32)

declare void @_GLOBAL__sub_I_iblas.cpp()

declare i32 @iabsmax(i32, i32*)

declare i32 @iscalarmul(i32*, i32, i32, i32, i32)

declare i32 @mblas_igemv(i8, i32, i32, i32*, i32*, i32*, i32)

declare void @_ZL10init_igemvPvP29st_ParallelThreadsEnvironment(i8*, %struct.st_ParallelThreadsEnvironment*)

declare void @_ZL9igemv_funPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @TensorDot_igemv(%struct.st_MDataArray**, i8, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @mblas_igemm(i32, i32, i32, i32*, i32*, i32*, i32)

declare void @_ZL10init_igemmPvP29st_ParallelThreadsEnvironment(i8*, %struct.st_ParallelThreadsEnvironment*)

declare void @_ZL9igemm_funPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @TensorDot_igemm(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @IDCNumToType(%struct.idc_num_t*, i32)

declare i32 @MTensor_getByteCount(%struct.st_MDataArray*)

declare %struct.st_MDataArray* @MTensor_new()

declare %struct.st_MDataArray* @MTensor_newAllocatedCommon(i32, i32, i32*, i32)

declare i32 @MTensor_allocateMemoryCommon(%struct.st_MDataArray*, i32, i32, i32*, i32)

declare i32 @_ZL20MTensor_allocateDataP13st_MDataArrayiiPKiii(%struct.st_MDataArray*, i32, i32, i32, i32)

declare void @MTensor_delete(%struct.st_MDataArray*)

declare void @MTensor_freeMemory(%struct.st_MDataArray*)

declare void @MTensorArray_delete(i32, %struct.st_MDataArray**)

declare i32 @NumberOfElements_common(i32, i32*, i32)

declare %struct.st_MDataArray* @MTensor_newUsingDataArray(i32, i32, i32*, i8*)

declare %struct.st_MDataArray* @MTensor_newCopyFromDataArray(i32, i32, i32*, i8*)

declare void @MTensor_resetDimensions(%struct.st_MDataArray*, i32, i32*)

declare void @MTensor_reallocateAndCopyData(%struct.st_MDataArray*)

declare i32 @MTensor_allocateCommon(%struct.st_MDataArray**, i32, i32, i32*, i32)

declare i32 @MTensor_allocateMemoryLikeCommon(%struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @iMTensor_copyUniqueInExpr(%struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @MTensor_copyUniqueCommon(%struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @MTensor_copy(%struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @MTensor_makeCopy(%struct.st_MDataArray**, %struct.st_MDataArray*)

declare void @GenericData_copySomeCoerce(i8*, i32, i32, i32, i8*, i32, i32, i32, i32)

declare void @MTensor_copyContiguousData(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, i32)

declare void @MTensor_copySomeData(%struct.st_MDataArray*, i32, i32, %struct.st_MDataArray*, i32, i32, i32)

declare void @MTensor_copySomeDataCoerce(%struct.st_MDataArray*, i32, i32, %struct.st_MDataArray*, i32, i32, i32)

declare double @TensorDataToReal(%struct.st_MDataArray*, i32, i32*)

declare void @MTensor_initialize(%struct.st_MDataArray*, i32)

declare i32 @MTensor_maxCompatibleDimensions(%struct.st_MDataArray*, %struct.st_MDataArray*, i32*, i32**)

declare i32 @MTensor_compatibleDimensionsQ(%struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @MTensor_equivalentQ(%struct.st_MDataArray*, %struct.st_MDataArray*)

declare %struct.st_MDataArray* @LoadNewRankZeroMTensor(i8*, i32)

declare %struct.st_MDataArray** @LoadSharedRankZeroMTensor(i8*, i32, i32)

declare void @InitializeMTensorThreads()

declare void @DeinitializeMTensorThreads()

declare void @_Z12init_mtensori(i32)

declare void @TensorMemory_addNew(%struct.st_MDataArray*)

declare void @TensorMemory_free(%struct.st_MDataArray*)

declare void @initializeTensorMemoryCheck(void (i8*)*)

declare i32 @_ZL6key_fnP13st_hash_entry(%struct.st_hash_entry*)

declare void @_ZL6set_fnP13st_hash_entryS0_(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare i32 @_ZL8equal_fnP13st_hash_entryS0_(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare void @_ZL6new_fnP13st_hash_entry(%struct.st_hash_entry*)

declare void @_ZL10destroy_fnP13st_hash_entry(%struct.st_hash_entry*)

declare void @runTensorMemoryCheck(i32)

declare i32 @Tensor_select_or_set_part(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_ZL28DataArray_select_or_set_partP13st_MDataArrayS0_iP8part_indi(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_ZL29CopySpecifiedPartsOfDataArrayP13st_MDataArrayiS0_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayIaEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayIhEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayIsEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayItEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayIiEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayIjEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayIxEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayIyEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayIfEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayIdEvP13st_MDataArrayiS1_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayISt7complexIfEEvP13st_MDataArrayiS3_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare void @_Z32tplCopySpecifiedPartsOfDataArrayISt7complexIdEEvP13st_MDataArrayiS3_iP8part_indi(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare i32 @NumericArray_select_or_set_part(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, %struct.part_ind*, i32)

declare i32 @NormalizeSeqSpec(i32, i32, i32*, i32*, i32*, i32*)

declare i32 @part_check_tensor_dims_impl(i32, i32*, i32, %struct.part_ind*, i32*, %struct.part_message_data_struct*)

declare i32 @setpart_check_tensor_impl(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, %struct.part_ind*, %struct.part_message_data_struct*)

declare i32 @_ZL8LoopII_IP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL7LoopOp2P13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopIR_IP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopIR_RP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopIR_CP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopIC_RP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopIC_CP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopRI_IP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopRI_RP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopRI_CP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopRR_IP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopRR_RP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopRR_CP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopRC_RP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopRC_CP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopCI_RP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopCI_CP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopCR_RP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopCR_CP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopCC_RP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL8LoopCC_CP13st_MDataArrayS0_S0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL7LoopI_IP13st_MDataArrayS0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL7LoopOp1P13st_MDataArrayS0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL7LoopR_IP13st_MDataArrayS0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL7LoopR_RP13st_MDataArrayS0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL7LoopR_CP13st_MDataArrayS0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL7LoopC_IP13st_MDataArrayS0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL7LoopC_RP13st_MDataArrayS0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @_ZL7LoopC_CP13st_MDataArrayS0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare void @_GLOBAL__sub_I_tensorarith.cpp()

declare i32 @TensorAddTo(%struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @_ZL16TensorOp2InPlaceP13st_MDataArrayS0_ij(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32)

declare i32 @TensorDivideBy(%struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @TensorSubtractFrom(%struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @TensorTimesBy(%struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @TensorPlus(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @TensorDivide(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @TensorSubtract(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @TensorTimes(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @Math_T_T(i32, i32, %struct.st_MDataArray*, i32, %struct.st_MDataArray**)

declare i32 @Math_V_V(i32, i32, i32, i8*, i32, i8*)

declare i32 @Math_TT_T(i32, i32, %struct.st_MDataArray*, %struct.st_MDataArray*, i32, %struct.st_MDataArray**)

declare i32 @Math_VV_V(i32, i32, i32, i8*, i32, i8*, i32, i8*)

declare void @_GLOBAL__sub_I_tensorfuns.cpp()

declare i32 @CanCoerceTensor(%struct.st_MDataArray*, i32, double)

declare i32 @CoerceTensor(%struct.st_MDataArray*, i32, %struct.st_MDataArray*)

declare i32 @CoercePartOfTensor(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, i32)

declare i32 @MTensor_listPart(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare void @_ZL24MTensor_internalListPartP13st_MDataArrayS0_S0_(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @MTensor_doPartialConjugate(%struct.st_MDataArray*, i32, i32, i32, i32, i32)

declare i32 @MRealArrayOverflowQ(double*, i32)

declare i32 @MComplexArrayOverflowQ(%"struct.std::complex"*, i32)

declare i32 @TensorOverflowQ(%struct.st_MDataArray*)

declare i32 @MTensor_take(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i32*, i32*, i32*)

declare i32 @MTensor_drop(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i32*, i32*, i32*)

declare void @_ZL11rTensorDropP13st_MDataArrayiiiPiS0_S1_(%struct.st_MDataArray*, i32, i32, i32, i32*, %struct.st_MDataArray*, i32*)

declare i32 @MTensor_outerList(%struct.st_MDataArray**, i32, %struct.st_MDataArray**, i32*)

declare i32 @MTensor_outerPlus(%struct.st_MDataArray**, i32, %struct.st_MDataArray**, i32*, i32)

declare i32 @MTensor_outerTimes(%struct.st_MDataArray**, i32, %struct.st_MDataArray**, i32*, i32)

declare i32 @PreparePositions(%struct.st_MDataArray*, i32*, i32, i32)

declare i32 @GetInsertedDimension(%struct.st_MDataArray*, i32*)

declare i32 @MTensor_insert(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @MTensor_Join(%struct.st_MDataArray**, %struct.st_MDataArray**, i32)

declare i32 @MTensor_joinDimension(%struct.st_MDataArray*, i32, %struct.st_MDataArray**, i32)

declare i32 @MTensor_Union(%struct.st_MDataArray**, %struct.st_MDataArray**, i32)

declare i32 @MTensor_computeIC(%struct.st_MDataArray**, %struct.st_MDataArray**, i32, i32)

declare i32 @MTensor_toplevelDelete(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @MTensor_reverse(%struct.st_MDataArray**, %struct.st_MDataArray*)

declare i32 @MTensor_rotate(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*)

declare void @_ZL10RotateDataPiS_iS_S_i(i32*, i32*, i32, i32*, i32*, i32)

declare i32 @MTensor_flatten(%struct.st_MDataArray**, %struct.st_MDataArray*, i32)

declare i32 @TensorOrderedQ(%struct.st_MDataArray*)

declare i32 @MTensor_position(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, double)

declare i32 @_ZL21IntPositionMatchPartQP13st_MDataArrayS0_iid(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32, double)

declare i32 @_ZL22RealPositionMatchPartQP13st_MDataArrayS0_iid(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32, double)

declare i32 @_ZL25ComplexPositionMatchPartQP13st_MDataArrayS0_iid(%struct.st_MDataArray*, %struct.st_MDataArray*, i32, i32, double)

declare i32 @TensorCount(%struct.st_MDataArray*, %struct.st_MDataArray*, double)

declare i32 @TensorMemberQ(%struct.st_MDataArray*, %struct.st_MDataArray*, double)

declare i32 @TensorFreeQ(%struct.st_MDataArray*, %struct.st_MDataArray*, double)

declare i32 @TensorPartition(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32* @_ZL19PartitionIntTensor0PiS_S_iiS_S_ii(i32*, i32*, i32*, i32, i32, i32*, i32*, i32, i32)

declare double* @_ZL20PartitionRealTensor0PdPiS0_iiS0_S0_ii(double*, i32*, i32*, i32, i32, i32*, i32*, i32, i32)

declare %"struct.std::complex"* @_ZL18PartitionCxTensor0PSt7complexIdEPiS2_iiS2_S2_ii(%"struct.std::complex"*, i32*, i32*, i32, i32, i32*, i32*, i32, i32)

declare %"struct.std::complex"* @_ZL21PartitionCxTensorDeepPSt7complexIdEiiiiPiS2_i(%"struct.std::complex"*, i32, i32, i32, i32, i32*, i32*, i32)

declare %"struct.std::complex"* @_ZL14MakeCxPartDeepPSt7complexIdEiiiiPi(%"struct.std::complex"*, i32, i32, i32, i32, i32*)

declare double* @_ZL23PartitionRealTensorDeepPdiiiiPiS0_i(double*, i32, i32, i32, i32, i32*, i32*, i32)

declare double* @_ZL16MakeRealPartDeepPdiiiiPi(double*, i32, i32, i32, i32, i32*)

declare i32* @_ZL22PartitionIntTensorDeepPiiiiiS_S_i(i32*, i32, i32, i32, i32, i32*, i32*, i32)

declare i32* @_ZL15MakeIntPartDeepPiiiiiS_(i32*, i32, i32, i32, i32, i32*)

declare i32 @TransposeComplexBlock(i32, i32, i32, %"struct.std::complex"*)

declare i32 @TransposeComplex(i32, i32, %"struct.std::complex"*)

declare i32 @PermutationType(%struct.st_MDataArray*)

declare i32 @TransposeResultDimensions(%struct.st_MDataArray*, i32, i32*, %struct.st_MDataArray*)

declare i32 @MTensor_12TransposeConjugate(%struct.st_MDataArray**, %struct.st_MDataArray*, i32)

declare void @_ZL26MTensor_TransposeSubmatrixP13st_MDataArrayiS0_iiii(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, i32, i32, i32)

declare i32 @MTensor_ndimTranspose(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare void @InitializePosition(i32, i32*)

declare i32 @FlatIndex(i32, i32*, i32*)

declare i32 @IncrementPosition(i32, i32*, i32*)

declare i32 @IncrementPositionBy(i32, i32*, i32*, i32)

declare i32 @MTensor_pad(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32*, i32*)

declare void @_ZL4tileiP13st_MDataArrayiPiS0_S1_S1_S1_(i32, %struct.st_MDataArray*, i32, i32*, %struct.st_MDataArray*, i32*, i32*, i32*)

declare void @_ZL7overlayiP13st_MDataArrayiPiS0_iS1_S1_(i32, %struct.st_MDataArray*, i32, i32*, %struct.st_MDataArray*, i32, i32*, i32*)

declare i32 @MTensor_accumulate(%struct.st_MDataArray**, %struct.st_MDataArray*, i32)

declare i32 @MTensor_differences(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i32*, i32, i32)

declare double @MReal_smallDet(i32, double**)

declare i32 @CheckIntegerTensor(%struct.st_MDataArray*, i32)

declare i32 @TensorSameQ(double, i32, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @MTensor_tally(%struct.st_MDataArray**, %struct.st_MDataArray*)

declare i32 @TensorFirstOrLastInOrder(%struct.st_MDataArray*, i32)

declare i32 @MergeSortPermutation_mint(i32*, i32*, i32*, i32, i32)

declare i32 @MergeSortPermutation_double(double*, i32*, i32*, i32, i32)

declare i32 @MTensor_computeSortPermutation(%struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @_ZL28MergeSortPermutation_complexPKSt7complexIdEPiS3_ii(%"struct.std::complex"*, i32*, i32*, i32, i32)

declare i32 @MTensor_Sort(%struct.st_MDataArray**, %struct.st_MDataArray*)

declare void @_GLOBAL__sub_I_tensor_blas1.cpp()

declare i32 @machfunc_idot(i32*, i32*, i32, i32*, i32, i32, i32)

declare i32 @machfunc_ddot(double*, double*, i32, double*, i32, i32, i32)

declare double @ddot_(i32*, double*, i32*, double*, i32*)

declare i32 @machfunc_cdot(%"struct.std::complex"*, i32, %"struct.std::complex"*, i32, %"struct.std::complex"*, i32, i32, i32)

declare void @zdotc_(%"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*)

declare void @zdotu_(%"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*)

declare void @mblas_zdotc(%"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*)

declare void @mblas_zdotu(%"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*)

declare void @_GLOBAL__sub_I_tensor_blas2.cpp()

declare i32 @machfunc_igemv(i8, i32, i32, i32*, i32*, i32, i32*, i32, i32*, i32*, i32, i32)

declare i32 @machfunc_dgemv(i32, i32, double*, double*, i32, double*, i32, double*, double*, i32, i32)

declare i32 @dgemv_(i8*, i32*, i32*, double*, double*, i32*, double*, i32*, double*, double*, i32*)

declare i32 @machfunc_cgemv(i32, i32, %"struct.std::complex"*, %"struct.std::complex"*, i32, %"struct.std::complex"*, i32, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32)

declare i32 @zgemv_(i8*, i32*, i32*, %"struct.std::pair.4"*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, %"struct.std::pair.4"*, i32*)

declare i32 @machfunc_dgemtv(i32, i32, double*, double*, i32, double*, i32, double*, double*, i32, i32)

declare i32 @machfunc_cgemtv(i32, i32, %"struct.std::complex"*, %"struct.std::complex"*, i32, %"struct.std::complex"*, i32, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32)

declare i32 @TensorDotMatrixVector(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @TensorDotVectorMatrix(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @TensorGEMV(i8, i32, i32, %struct.idc_num_t*, %struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, i32, %struct.idc_num_t*, %struct.st_MDataArray*, i32, i32, i32)

declare i32 @TensorGER(i32, i32, %struct.idc_num_t*, %struct.st_MDataArray*, i32, i32, %struct.st_MDataArray*, i32, i32, %struct.st_MDataArray*, i32, i32, i32)

declare i32 @dger_(i32*, i32*, double*, double*, i32*, double*, i32*, double*, i32*)

declare i32 @zgerc_(i32*, i32*, %"struct.std::pair.4"*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*)

declare i32 @zgeru_(i32*, i32*, %"struct.std::pair.4"*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*)

declare void @_GLOBAL__sub_I_tensor_blas3.cpp()

declare i32 @machfunc_igemm(i8, i8, i32, i32, i32, i32*, i32*, i32, i32*, i32, i32*, i32*, i32, i32)

declare i32 @MReal_smallMM(double*, double*, double*, i32, i32, i32)

declare i32 @MComplex_smallMM(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32, i32)

declare i32 @MReal_smallMTM(double*, double*, double*, i32, i32, i32)

declare i32 @MComplex_smallMTM(%"struct.std::complex"*, %"struct.std::complex"*, %"struct.std::complex"*, i32, i32, i32)

declare i32 @TensorDotMatrixMatrix(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @_ZL21machfunc_dzgemm_smalliiiP13st_MDataArrayS0_S0_(i32, i32, i32, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @TensorGEMM(i8, i8, i32, i32, i32, %struct.idc_num_t*, %struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, %struct.idc_num_t*, %struct.st_MDataArray*, i32, i32)

declare i32 @dgemm_(i8*, i8*, i32*, i32*, i32*, double*, double*, i32*, double*, i32*, double*, double*, i32*)

declare i32 @zgemm_(i8*, i8*, i32*, i32*, i32*, %"struct.std::pair.4"*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, i32*, %"struct.std::pair.4"*, %"struct.std::pair.4"*, i32*)

declare i32 @MTensor_nDot(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, i32)

declare i32 @TensorTrace(%struct.st_MDataArray*, i8*, i32)

declare %struct.st_TensorProperty* @TensorProperty_new(i32)

declare void @TensorProperty_delete(%struct.st_TensorProperty*)

declare void @TensorProperty_copyValues(%struct.st_TensorProperty*, %struct.st_TensorProperty*)

declare %struct.st_TensorProperty* @TensorProperty_copy(%struct.st_TensorProperty*)

declare void @_Z17InitMNumericArrayi(i32)

declare void @_GLOBAL__sub_I_mnumericarray.cpp()

declare float @double_to_float(double, i32*, i32)

; Function Attrs: nounwind readnone speculatable
declare double @llvm.copysign.f64(double, double) #0

declare i32 @MNumericArrayConvertMethod_fromString(i8*)

declare void @_ZSt19__throw_logic_errorPKc(i8*)

declare i32 @_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_21MNumericArrayMethodIdESaIS9_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_(%"class.std::_Hashtable"*, %"class.std::__cxx11::basic_string"*)

declare i32 @_ZSt11_Hash_bytesPKvjj(i8*, i32, i32)

declare i8* @MNumericArrayConvertMethodString(i32)

declare i32 @MNumericArrayType_fromString(i8*)

declare i32 @_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_19MNumericArrayTypeIdESaIS9_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_(%"class.std::_Hashtable"*, %"class.std::__cxx11::basic_string"*)

declare i8* @MNumericArrayTypeString(i32)

declare void @DeinitializeMNumericArray()

declare i32 @MNumericArray_getArrayBytes(%struct.st_MDataArray*)

declare i32 @MNumericArray_getByteCount(%struct.st_MDataArray*)

declare %struct.st_MDataArray* @MNumericArray_newAllocated(i32, i32)

declare %struct.st_MDataArray* @_ZL32MNumericArray_newAllocatedCommoniiPKiPKvb(i32, i32, i32*, i8*, i1)

declare %struct.st_MDataArray* @MNumericArray_newAllocatedRank(i32, i32, i32*)

declare %struct.st_MDataArray* @MNumericArray_newAllocatedRankUsingDims(i32, i32, i32*)

declare %struct.st_MDataArray* @MNumericArray_newUsingDataArray(i32, i32, i32*, i8*)

declare %struct.st_MDataArray* @MNumericArray_newUsingDataArrayAndDims(i32, i32, i32*, i8*)

declare void @MNumericArray_delete(%struct.st_MDataArray*)

declare %struct.st_MDataArray* @MNumericArray_copy(%struct.st_MDataArray*)

declare void @MNumericArray_copyContiguousData(%struct.st_MDataArray*, i32, %struct.st_MDataArray*, i32, i32)

declare void @MNumericArray_copySomeData(%struct.st_MDataArray*, i32, i32, %struct.st_MDataArray*, i32, i32, i32)

declare void @MNumericArray_resetDimensions(%struct.st_MDataArray*, i32, i32*)

declare void @_Z18init_mnumericarrayi(i32)

declare void @_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_21MNumericArrayMethodIdESaIS9_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE6rehashEj(%"class.std::_Hashtable"*, i32)

declare void @_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_21MNumericArrayMethodIdESaIS9_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_S8_EEEES6_INSB_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_(%"struct.std::pair.38"*, %"class.std::_Hashtable"*, %"struct.std::pair.37"*)

declare void @_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_19MNumericArrayTypeIdESaIS9_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE6rehashEj(%"class.std::_Hashtable"*, i32)

declare void @_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_19MNumericArrayTypeIdESaIS9_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_S8_EEEES6_INSB_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_(%"struct.std::pair.38"*, %"class.std::_Hashtable"*, %"struct.std::pair.37"*)

declare i32 @_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_19MNumericArrayTypeIdESaIS9_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEjjPNSB_10_Hash_nodeIS9_Lb1EEE(%"class.std::_Hashtable"*, i32, i32, %"struct.std::__detail::_Hash_node"*)

declare void @_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEjjj(%"struct.std::pair.42"*, %"struct.std::__detail::_Prime_rehash_policy"*, i32, i32, i32)

declare i32 @_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEj(%"struct.std::__detail::_Prime_rehash_policy"*, i32)

declare i32 @_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_21MNumericArrayMethodIdESaIS9_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEjjPNSB_10_Hash_nodeIS9_Lb1EEE(%"class.std::_Hashtable"*, i32, i32, %"struct.std::__detail::_Hash_node"*)

declare void @_GLOBAL__sub_I_mnumericarray_compare.cpp()

declare i32 @_Z25MNumericArray_equivalentQP13st_MDataArrayS0_(%struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @MNumericArray_sameQ(double, i32, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @_ZL24MNumericArray_sameQ_implRKdP13st_MDataArrayS2_(double*, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare void @_GLOBAL__sub_I_mnumericarray_convert.cpp()

declare i32 @isInRangeUnsigned(double, i64)

declare i32 @isInRangeSigned(double, i64, i64)

declare i32 @MNumericArray_convert_common(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, %struct.st_MNumericArrayConvertData*)

declare i32 @_ZL26MNumericArray_convert_implPP13st_MDataArrayS0_RKiRKP27st_MNumericArrayConvertData(%struct.st_MDataArray**, i32, i8*, i32*, %struct.st_MNumericArrayConvertData*)

declare i32 @_ZN3wrt13mnumericarray7convertL10check_implIfdEENSt9enable_ifIXaaaasr17is_floating_pointIT0_EE5valuesr17is_floating_pointIT_EE5valuesr19is_larger_type_sizeIS4_S5_EE5valueEiE4typeEPS5_RKS4_P27st_MNumericArrayConvertData(float*, double, %struct.st_MNumericArrayConvertData*)

declare i32 @_ZL26MNumericArray_convert_implISt7complexIfEEiPP13st_MDataArrayPKT_RKiRKP27st_MNumericArrayConvertData(i32, i8*, %"struct.std::complex.203"*, i32*, %struct.st_MNumericArrayConvertData*)

declare i32 @_ZL26MNumericArray_convert_implISt7complexIdEEiPP13st_MDataArrayPKT_RKiRKP27st_MNumericArrayConvertData(i32, i8*, %"struct.std::complex"*, i32*, %struct.st_MNumericArrayConvertData*)

declare i32 @MNumericArray_convert(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32)

declare i32 @MNumericArray_fillFromMTensor(%struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MNumericArrayConvertData*)

declare i32 @MNumericArray_take(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i32*, i32*, i32*)

declare i32 @MNumericArray_drop(%struct.st_MDataArray**, %struct.st_MDataArray*, i32, i32*, i32*, i32*, i32*)

declare void @_ZL17rNumericArrayDropP13st_MDataArrayiiiPiS0_S1_(%struct.st_MDataArray*, i32, i32, i32, i32*, %struct.st_MDataArray*, i32*)

declare i32 @MNumericArray_join(%struct.st_MDataArray**, %struct.st_MDataArray**, i32)

declare i32 @MNumericArray_flatten(%struct.st_MDataArray**, %struct.st_MDataArray*, i32)

declare i32 @MNumericArray_clip(%struct.st_MDataArray**, %struct.st_MDataArray*, %struct.st_MDataArray*, %struct.st_MDataArray*)

declare i32 @MRawArrayType_fromString(i8*)

declare i8* @MRawArrayTypeString(i32)

declare %struct.st_MDataArray* @MRawArray_convert(%struct.st_MDataArray*, i32, i32)

declare void @KThread_initStartStopFunctions()

declare void @KThread_deleteStartStopFunctions()

declare i32 @KThread_addStartFunction(void ()*)

declare i32 @KThread_addStopFunction(void ()*)

declare void @KThread_runStartFunctions()

declare void @KThread_runStopFunction(i32)

declare void @KThread_runSomeStopFunctions(i32*, i32)

declare void @KThread_runStopFunctions()

declare i32 @getPhysicalProcessorCount()

declare %struct._IO_FILE* @popen(i8*, i8*)

declare i32 @fread(i8*, i32, i32, %struct._IO_FILE*)

declare i32 @sscanf(i8*, i8*, ...)

declare i32 @pclose(%struct._IO_FILE*)

declare i32 @getLogicalProcessorCount()

declare void @Delete_KThread(%struct.st_KMutex*)

declare void @KThread_Sleep(i32)

declare i32 @nanosleep(%"struct.std::pair"*, %"struct.std::pair"*)

declare %struct.st_KMutex* @KThreadCreate(void (i8*)*, i8*, i32)

declare i32 @pthread_attr_init(%union.pthread_attr_t*)

declare i32 @pthread_attr_getschedparam(%union.pthread_attr_t*, %union.ZHeader*)

declare i32 @pthread_attr_setschedparam(%union.pthread_attr_t*, %union.ZHeader*)

declare i32 @pthread_attr_setdetachstate(%union.pthread_attr_t*, i32)

declare i8* @_ZL9threadRunPv(i8*)

declare i32 @pthread_attr_destroy(%union.pthread_attr_t*)

declare void @KThreadRawExit()

declare void @pthread_exit(i8*)

declare %struct.st_KMutex* @New_KMutex()

declare i32 @pthread_mutexattr_init(%union.ZHeader*)

declare i32 @pthread_mutexattr_settype(%union.ZHeader*, i32)

declare i32 @pthread_mutex_init(%union.pthread_mutex_t*, %union.ZHeader*)

declare void @Delete_KMutex(%struct.st_KMutex*)

declare i32 @pthread_mutex_destroy(%union.pthread_mutex_t*)

declare void @KMutexLock(%struct.st_KMutex*)

declare void @KMutexUnlock(%struct.st_KMutex*)

declare i32 @KMutexTryLock(%struct.st_KMutex*)

declare i32 @pthread_mutex_trylock(%union.pthread_mutex_t*)

declare %struct.st_KMutex* @New_KCondition()

declare i32 @pthread_cond_init(%union.pthread_cond_t*, %union.ZHeader*)

declare void @Delete_KCondition(%struct.st_KMutex*)

declare i32 @pthread_cond_destroy(%union.pthread_cond_t*)

declare void @KConditionWait(%struct.st_KMutex*, %struct.st_KMutex*)

declare i32 @pthread_cond_wait(%union.pthread_cond_t*, %union.pthread_mutex_t*)

declare i32 @KConditionTimedWait(%struct.st_KMutex*, %struct.st_KMutex*, i32)

declare i32 @gettimeofday(%"struct.std::pair"*, %"struct.std::pair"*)

declare i32 @pthread_cond_timedwait(%union.pthread_cond_t*, %union.pthread_mutex_t*, %"struct.std::pair"*)

declare void @KConditionSignal(%struct.st_KMutex*)

declare i32 @pthread_cond_signal(%union.pthread_cond_t*)

declare i32 @KThreadWaitForExit(%struct.st_KMutex*)

declare i32 @pthread_join(i32, i8**)

declare %struct.st_KMutex* @New_KThreadLocalStorage()

declare i32 @pthread_key_create(i32*, void (i8*)*)

declare void @Delete_KThreadLocalStorage(%struct.st_KMutex*)

declare i32 @pthread_key_delete(i32)

declare void @KThreadLocalStorageSet(%struct.st_KMutex*, i8*)

declare i32 @pthread_setspecific(i32, i8*)

declare i8* @KThreadLocalStorageGet(%struct.st_KMutex*)

declare i8* @pthread_getspecific(i32)

declare i32 @getProcessPriorityClass()

declare i32* @__errno_location()

declare i32 @getpriority(i32, i32)

declare i32 @setProcessPriorityClass(i32)

declare i32 @setpriority(i32, i32, i32)

declare i64 @KThreadID()

declare i32 @pthread_self()

declare void @_Z10InitRandomi(i32)

declare void @_GLOBAL__sub_I_randomgenerator.cpp()

declare %struct.RandomGenerator_struct* @RandomGenerator_newGeneric()

declare i32 @Generic_randomMIntegers(i32*, i32, i32, i32, %struct.RandomGenerator_struct*)

declare i32 @Generic_randomMReals(double*, i32, double, double, %struct.RandomGenerator_struct*)

declare %struct.RandomDistributionFunctionArray_struct* @RandomDistributionFunctionArray_copy(%struct.RandomDistributionFunctionArray_struct*)

declare void @RandomDistributionFunctionArray_delete(%struct.RandomDistributionFunctionArray_struct*)

declare %struct.RandomGenerator_struct* @RandomGenerator_copyGeneric(%struct.RandomGenerator_struct*)

declare void @RandomGenerator_deleteGeneric(%struct.RandomGenerator_struct*)

declare %struct.RandomGeneratorMethodData_struct* @RandomGeneratorMethodData_new(i32, i8*)

declare void @RandomGeneratorMethodData_delete(%struct.RandomGeneratorMethodData_struct*)

declare %struct.RandomGeneratorMethod_struct* @RandomGeneratorMethod_new(i8*)

declare %struct.RandomGenerator_struct* @RandomGeneratorMethod_getCurrent(%struct.RandomGeneratorMethod_struct*)

declare void @RandomGeneratorMethod_setCurrent(%struct.RandomGeneratorMethod_struct*, %struct.RandomGenerator_struct*)

declare i32 @RandomGeneratorMethod_find(i8*)

declare %struct.RandomGeneratorMethod_struct* @RandomGeneratorMethod_get(i32)

declare void @RandomGeneratorMethod_addToMethodList(%struct.RandomGeneratorMethod_struct*)

declare void @RandomGeneratorMethod_add(i8*, %struct.RandomGenerator_struct* (i8*)*, %struct.RandomGenerator_struct* (%struct.RandomGenerator_struct*)*, void (%struct.RandomGenerator_struct*)*)

declare void @RandomGenerator_refIncr(%struct.RandomGenerator_struct*)

declare void @RandomGenerator_free(%struct.RandomGenerator_struct*)

declare void @RandomGenerator_refDecr(%struct.RandomGenerator_struct*)

declare %struct.RandomGenerator_struct* @RandomGenerator_clone(%struct.RandomGenerator_struct*)

declare void @CurrentRandomGenerator_set(%struct.RandomGenerator_struct*)

declare %struct.RandomGenerator_struct* @CurrentRandomGenerator_get()

declare %struct.RandomGenerator_struct* @GetCurrentRandomGenerator()

declare %struct.random_state_entry_struct* @RandomStateEntry_new()

declare %struct.random_state_entry_struct* @RandomStateEntry_add(%struct.random_state_entry_struct*, %struct.RandomGenerator_struct*)

declare void @RandomStateEntry_delete(%struct.random_state_entry_struct*)

declare void @RandomStateEntry_seed(%struct.random_state_entry_struct*, i32*, i32)

declare i32 @clock()

declare %struct.RandomState_struct* @RandomState_new()

declare void @RandomState_count(%struct.RandomState_struct*)

declare void @_ZL23RandomState_countThreadPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare void @RandomState_discount(%struct.RandomState_struct*)

declare void @_ZL26RandomState_discountThreadPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare void @RandomState_delete(%struct.RandomState_struct*)

declare void @_ZL24RandomState_deleteThreadPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare i32 @GetParallelGeneratorIndex()

declare void @ClearParallelRandomGenerators()

declare void @_ZL37ClearThreadRandomGeneratorsIterateFunPvP17st_ParallelThread(i8*, %struct.st_ParallelThread*)

declare void @_ZL27ClearThreadRandomGeneratorsv()

declare %struct.RandomState_struct* @RandomState_fromSpecification(i8*, i8*, i32*, i32)

declare %struct.RandomState_struct* @_ZL19RandomState_fromAllPji(i32*, i32)

declare %struct.RandomState_struct* @GetRandomState(i8*)

declare void @RestoreRandomState(%struct.RandomState_struct*)

declare void @InitializeRandomThreads()

declare void @DeinitializeRandomThreads()

declare void @_Z24init_rtl_randomgeneratori(i32)

declare void @_GLOBAL__sub_I_randombuffer.cpp()

declare %struct.buffer_data_struct* @BufferData_new(i32)

declare %struct.buffer_data_struct* @BufferData_copy(%struct.buffer_data_struct*)

declare void @BufferData_clear(%struct.buffer_data_struct*)

declare void @BufferData_delete(%struct.buffer_data_struct*)

declare %struct.RandomBuffer_struct* @RandomBuffer_new(i32 (i32*, i32*, i32*, %struct.RandomGenerator_struct*)*, i32)

declare i32 @_ZL20RandomBuffer_getBitsiP22RandomGenerator_struct(i32, %struct.RandomGenerator_struct*)

declare i32 @_ZL25RandomBuffer_randomBigitsPjiP22RandomGenerator_struct(i32*, i32, %struct.RandomGenerator_struct*)

declare i32 @_ZL28RandomBuffer_randomMIntegersPiiiiP22RandomGenerator_struct(i32*, i32, i32, i32, %struct.RandomGenerator_struct*)

declare i32 @_ZL25RandomBuffer_randomMRealsPdiddP22RandomGenerator_struct(double*, i32, double, double, %struct.RandomGenerator_struct*)

declare void @_ZL20RandomBuffer_setSeedP22RandomGenerator_structPji(%struct.RandomGenerator_struct*, i32*, i32)

declare %struct.RandomBuffer_struct* @RandomBuffer_copy(%struct.RandomBuffer_struct*)

declare void @RandomBuffer_delete(%struct.RandomBuffer_struct*)

declare i32 @UniformRandomMReals(double*, i32, double, double)

declare i32 @UniformRandomMComplexes(%"struct.std::complex"*, i32, %"struct.std::complex", %"struct.std::complex")

declare i32 @UniformRandomMIntegers(i32*, i32, i32, i32)

declare i32 @AccumulateWeightMTensor(%struct.st_MDataArray**, %struct.st_MDataArray*)

declare i32 @RandomChoicePositionsM(i32*, i32, double*, i32, %struct.RandomGenerator_struct*)

declare i32 @RandomChoices(i32*, i32, i32, %struct.st_MDataArray*)

declare %struct.st_hashtable* @PositionHashTable_new(i32)

declare i32 @_ZL15PositionHashFunP13st_hash_entry(%struct.st_hash_entry*)

declare void @_ZL15PositionHashSetP13st_hash_entryS0_(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare i32 @_ZL17PositionHashEqualP13st_hash_entryS0_(%struct.st_hash_entry*, %struct.st_hash_entry*)

declare void @_ZL15PositionHashNewP13st_hash_entry(%struct.st_hash_entry*)

declare void @_ZL18PositionHashDeleteP13st_hash_entry(%struct.st_hash_entry*)

declare i32 @SimpleRandomSamplePositionsM(i32*, i32, i32, %struct.RandomGenerator_struct*)

declare i32 @RandomSamplePositionsM(i32*, i32, %struct.st_MDataArray*, %struct.RandomGenerator_struct*)

declare %struct.PositionTree_struct* @_ZL16MakePositionTreeiPiPd(i32, i32*, double*)

declare i32 @_ZL20ChooseSamplePositionP19PositionTree_structd(%struct.PositionTree_struct*, double)

declare void @_ZL19PositionTree_deleteP19PositionTree_struct(%struct.PositionTree_struct*)

declare i32 @RandomSamples(i32*, i32, i32, %struct.st_MDataArray*)

declare i32 @AcceptanceRejectionMRealVector(double*, i32, double*, double*, i32, double*, i32 (double*, double*)*)

declare i32 @BetaMRealVector(double*, i32, double, double)

declare i32 @CA5State_checkParameters(i32, i32, i32)

declare %struct.RandomGenerator_struct* @CA5BufferState_alloc(i8*)

declare void @_ZL11CA5_setSeedP22RandomGenerator_structPji(%struct.RandomGenerator_struct*, i32*, i32)

declare i32 @_ZL19CA5_fillBufferArrayPjPiS0_P22RandomGenerator_struct(i32*, i32*, i32*, %struct.RandomGenerator_struct*)

declare %struct.RandomGenerator_struct* @CA5BufferState_clone(%struct.RandomGenerator_struct*)

declare void @CA5BufferState_free(%struct.RandomGenerator_struct*)

declare void @_Z18init_rtl_randomCA5i(i32)

declare %struct.RandomGenerator_struct* @MersenneTwister_alloc(i8*)

declare i32 @_ZL31MersenneTwister_fillBufferArrayPjPiS0_P22RandomGenerator_struct(i32*, i32*, i32*, %struct.RandomGenerator_struct*)

declare void @_ZL23MersenneTwister_setSeedP22RandomGenerator_structPji(%struct.RandomGenerator_struct*, i32*, i32)

declare %struct.RandomGenerator_struct* @MersenneTwister_clone(%struct.RandomGenerator_struct*)

declare void @MersenneTwister_free(%struct.RandomGenerator_struct*)

declare i32 @PMT32_getNextIndex()

declare i32 @PMT32_checkParameters(i32)

declare %struct.RandomGenerator_struct* @PMT32_alloc(i8*)

declare void @_ZL13PMT32_setSeedP22RandomGenerator_structPji(%struct.RandomGenerator_struct*, i32*, i32)

declare i32 @_ZL21PMT32_fillBufferArrayPjPiS0_P22RandomGenerator_struct(i32*, i32*, i32*, %struct.RandomGenerator_struct*)

declare void @_ZL14PMT32_generatePjP17PMT32State_struct(i32*, %struct.PMT32State_struct*)

declare %struct.RandomGenerator_struct* @PMT32_clone(%struct.RandomGenerator_struct*)

declare void @PMT32_free(%struct.RandomGenerator_struct*)

declare void @_Z17init_rtl_randomMTi(i32)

attributes #0 = { nounwind readnone speculatable }
attributes #1 = { argmemonly nounwind }
attributes #2 = { nounwind readnone }
attributes #3 = { nounwind }

(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     31282,        702]
NotebookOptionsPosition[     28843,        637]
NotebookOutlinePosition[     31118,        699]
CellTagsIndexPosition[     31075,        696]
WindowTitle->Wolfram Product Activation
WindowFrame->ModalDialog*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 PaneSelectorBox[{True->
  PanelBox[
   TagBox[GridBox[{
      {
       PaneBox[
        DynamicBox[FEPrivate`ImportImage[
          FrontEnd`FileName[{"Dialogs", "Activation"}, "ErrorIcon.png"]]],
        Alignment->Center,
        ImageSize->{Automatic, 40}], 
       PaneBox[
        StyleBox[
         StyleBox[
          
          DynamicBox[FEPrivate`FrontEndResource[
           "WelcomeScreen", "ExpiringMessageDefault"]], "ControlStyle",
          StripOnInput->False,
          FontWeight->Bold],
         StripOnInput->False,
         LineIndent->0,
         LinebreakAdjustments->{1, 100, 0, 0, 100}],
        Alignment->Left,
        ImageMargins->{{5, 0}, {0, 0}},
        ImageSize->Full]}
     },
     AutoDelete->False,
     GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Center}}},
     GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
    "Grid"],
   Appearance->{
    "Default" -> 
     FrontEnd`FileName[{"Dialogs", "Activation"}, "ErrorBackground.9.png"]},
   FrameMargins->{{25, 0}, {0, 0}}]}, FEPrivate`SameQ[
   FEPrivate`LessEqual[FEPrivate`$DaysUntilExpiration, 15], True],
  ImageMargins->0,
  ImageSize->825]],
 CellMargins->FEPrivate`If[
   FEPrivate`SameQ[
    FEPrivate`LessEqual[FEPrivate`$DaysUntilExpiration, 15], True], {{
   0, -4}, {0, -1}}, {{0, -4}, {0, -14}}],
 CellBracketOptions->{"Color"->RGBColor[0.269993, 0.308507, 0.6]},
 CellHorizontalScrolling->True,
 PageBreakAbove->True,
 PageBreakWithin->False,
 ShowAutoStyles->True,
 LineSpacing->{1.25, 0},
 AutoItalicWords->{},
 ScriptMinSize->9,
 ShowStringCharacters->False,
 FontFamily:>CurrentValue["PanelFontFamily"],
 FontSize:>CurrentValue[
  "PanelFontSize"],ExpressionUUID->"4bd2fd68-3264-41d6-84b5-8b4fe7e6a24a"],

Cell[BoxData[
 DynamicModuleBox[{$CellContext`errorcode$$ = 0}, 
  OverlayBox[{
   PaneSelectorBox[{True->
    DynamicBox[FEPrivate`ImportImage[
      FrontEnd`FileName[{"Dialogs", "Activation"}, "SignInBackground.png"]],
     ImageSizeCache->{35., {1., 8.}}], False->
    DynamicBox[FEPrivate`ImportImage[
      FrontEnd`FileName[{"Dialogs", "Activation"}, 
       "SignInBackground-Error.png"]],
     ImageSizeCache->{35., {1., 8.}}]}, 
    Dynamic[$CellContext`errorcode$$ === 0],
    ImageSize->{Automatic, Automatic}], 
   FrameBox[
    DynamicModuleBox[{$CellContext`userid$$ = "", $CellContext`userpwd$$ = 
     "", $CellContext`errorMessagePane$$, $CellContext`ERRORCODE$$, \
$CellContext`errorDisplay$$}, 
     TagBox[GridBox[{
        {
         FrameBox[
          DynamicBox[ToBoxes[$CellContext`errorDisplay$$, StandardForm],
           ImageSizeCache->{823., {70., 25.}}],
          FrameMargins->0,
          FrameStyle->None,
          StripOnInput->False]},
        {
         TagBox[GridBox[{
            {
             FrameBox["\<\"\"\>",
              FrameMargins->0,
              FrameStyle->None,
              ImageSize->{526, 100},
              StripOnInput->False], 
             TagBox[GridBox[{
                {
                 FrameBox[
                  DynamicWrapperBox[
                   
                   DynamicModuleBox[{$CellContext`uidfieldhint$$ = 
                    "Wolfram ID", $CellContext`pwdfieldhint$$ = "Password"}, 
                    PaneBox[
                    TagBox[GridBox[{
                    {
                    PaneBox[
                    StyleBox[
                    StyleBox[
                    
                    DynamicBox[FEPrivate`FrontEndResource[
                    "ActivationDialog", "SignInTitle"],
                    ImageSizeCache->{178., {3., 10.}}], "DialogStyle", 
                    "ControlStyle",
                    StripOnInput->False],
                    StripOnInput->False,
                    LineIndent->0],
                    ImageMargins->{{0, 0}, {15, 0}}]},
                    {
                    PaneBox[
                    InputFieldBox[Dynamic[$CellContext`userid$$], String,
                    BoxID -> "unameboxid",
                    Appearance->FEPrivate`If[
                    FEPrivate`SameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], 
                    "Pending"], None, Automatic],
                    Background->FEPrivate`If[
                    FEPrivate`SameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], 
                    "Pending"], 
                    GrayLevel[0.5], Automatic],
                    Enabled->FEPrivate`UnsameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], "Pending"],
                    FieldHint->$CellContext`uidfieldhint$$,
                    FieldSize->{{1., 
                    DirectedInfinity[1]}, 1},
                    FrameMargins->{{Automatic, Automatic}, {Automatic, 4}},
                    ImageSize->{225, 25}],
                    ImageMargins->{{0, 0}, {15, 0}}]},
                    {
                    PaneBox[
                    InputFieldBox[Dynamic[$CellContext`userpwd$$], String,
                    BoxID -> "pwboxid",
                    Appearance->FEPrivate`If[
                    FEPrivate`SameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], 
                    "Pending"], None, Automatic],
                    Background->FEPrivate`If[
                    FEPrivate`SameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], 
                    "Pending"], 
                    GrayLevel[0.5], Automatic],
                    Enabled->FEPrivate`UnsameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], "Pending"],
                    FieldHint->$CellContext`pwdfieldhint$$,
                    FieldMasked->True,
                    FieldSize->{{1., 
                    DirectedInfinity[1]}, 1},
                    FrameMargins->{{Automatic, Automatic}, {Automatic, 4}},
                    ImageSize->{225, 25}],
                    ImageMargins->{{0, 0}, {15, 0}}]},
                    {
                    PaneBox[
                    TagBox[
                    TagBox[
                    TagBox[GridBox[{
                    {
                    PaneSelectorBox[{True->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "CheckboxOn.png"]],
                    ImageSizeCache->{35., {1., 8.}}], False->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "CheckboxOff.png"]],
                    ImageSizeCache->{35., {1., 8.}}]}, FrontEnd`CurrentValue[
                    FrontEnd`$FrontEnd, {
                    PrivateFrontEndOptions, "DialogSettings", "Login", 
                    "RememberMe"}, True]], 
                    PaneSelectorBox[{False->
                    StyleBox[
                    
                    DynamicBox[FEPrivate`FrontEndResource[
                    "ActivationDialog", "RememberMeText"],
                    ImageSizeCache->{88., {0., 10.}}], "DialogStyle", 
                    "ControlStyle",
                    StripOnInput->False,
                    LineColor->GrayLevel[0.5],
                    FrontFaceColor->GrayLevel[0.5],
                    BackFaceColor->GrayLevel[0.5],
                    GraphicsColor->GrayLevel[0.5],
                    FontColor->GrayLevel[0.5]], True->
                    StyleBox[
                    
                    DynamicBox[FEPrivate`FrontEndResource[
                    "ActivationDialog", "RememberMeText"],
                    ImageSizeCache->{88., {0., 10.}}], "DialogStyle", 
                    "ControlStyle",
                    StripOnInput->False]}, FEPrivate`And[
                    FEPrivate`UnsameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], "Pending"]
                    , True]]}
                    },
                    AutoDelete->False,
                    
                    GridBoxAlignment->{
                    "Columns" -> {{Left}}, "Rows" -> {{Bottom}}},
                    
                    GridBoxItemSize->{
                    "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
                    "Grid"],
                    EventHandlerTag[{"MouseClicked" :> FEPrivate`If[
                    FEPrivate`UnsameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], 
                    "Pending"], 
                    FEPrivate`Set[
                    
                    FrontEnd`CurrentValue[$FrontEnd, {
                    PrivateFrontEndOptions, "DialogSettings", "Login", 
                    "RememberMe"}], 
                    FEPrivate`Not[
                    
                    FrontEnd`CurrentValue[$FrontEnd, {
                    PrivateFrontEndOptions, "DialogSettings", "Login", 
                    "RememberMe"}, True]]]], PassEventsDown -> True, Method -> 
                    "Preemptive", PassEventsUp -> True}]],
                    MouseAppearanceTag["LinkHand"]],
                    ImageMargins->{{0, 0}, {15, 0}}]},
                    {
                    PaneBox[
                    PaneBox[
                    TagBox[GridBox[{
                    {
                    ButtonBox[
                    PaneSelectorBox[{False->
                    PaneSelectorBox[{False->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-Default.png"]],
                    ImageSizeCache->{45., {1., 10.}}], True->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-Hover.png"]]]}, Dynamic[
                    CurrentValue["MouseOver"]],
                    FrameMargins->0,
                    ImageSize->Automatic], True->
                    TagBox[
                    DynamicModuleBox[{Typeset`var = 1}, 
                    OverlayBox[{
                    AnimatorBox[Dynamic[Typeset`var], {1, 12, 1},
                    AnimationRate->11,
                    AnimationRunTime->0.,
                    AppearanceElements->{},
                    ImageSize->{1, 1}], 
                    PaneSelectorBox[{1->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-0.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 2->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-1.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 3->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-2.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 4->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-3.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 5->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-4.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 6->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-5.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 7->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-6.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 8->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-7.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 9->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-8.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 10->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-9.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 11->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-10.png"]],
                    ImageSizeCache->{45., {1., 10.}}], 12->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "SignIn-loading-11.png"]],
                    ImageSizeCache->{45., {1., 10.}}]}, Dynamic[Typeset`var],
                    ImageSize->All]}]],
                    {"ListAnimator", AnimationRate -> 11}]}, FEPrivate`SameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], 
                    "Pending"]],
                    
                    Appearance->{"Default" -> 
                    Image[RawArray["UnsignedInteger8",{{{255, 255, 255, 0}, {
                    255, 255, 255, 0}}, {{255, 255, 255, 0}, {255, 255, 255, 
                    0}}, {{255, 255, 255, 0}, {255, 255, 255, 0}}, {{255, 255,
                     255, 0}, {255, 255, 255, 0}}, {{255, 255, 255, 0}, {255, 
                    255, 255, 0}}, {{255, 255, 255, 0}, {255, 255, 255, 
                    0}}, {{255, 255, 255, 0}, {255, 255, 255, 0}}, {{255, 255,
                     255, 0}, {255, 255, 255, 0}}, {{255, 255, 255, 0}, {255, 
                    255, 255, 0}}, {{255, 255, 255, 0}, {255, 255, 255, 
                    0}}, {{0, 0, 0, 0}, {255, 255, 255, 0}}, {{255, 255, 255, 
                    0}, {255, 255, 255, 0}}, {{255, 255, 255, 0}, {255, 255, 
                    255, 0}}, {{255, 255, 255, 0}, {255, 255, 255, 0}}, {{255,
                     255, 255, 0}, {255, 255, 255, 0}}, {{255, 255, 255, 0}, {
                    255, 255, 255, 0}}, {{255, 255, 255, 0}, {255, 255, 255, 
                    0}}}], "Byte", ColorSpace -> "RGB", Interleaving -> True],
                     "ButtonType" -> "Default"},
                    
                    ButtonFunction:>
                    Module[{$CellContext`loginReturn$, \
$CellContext`errorCode$}, $CellContext`loginReturn$ = Quiet[
                    
                    CloudConnect[$CellContext`userid$$, \
$CellContext`userpwd$$, "Prompt" -> 
                    False]]; $CellContext`userid$$ = ($CellContext`userpwd$$ = 
                    ""); $CellContext`errorCode$ = 
                    CurrentValue["WolframCloudLoginError"]; If[
                    
                    Or[$CellContext`loginReturn$ === $Failed, \
$CellContext`errorCode$ =!= 0], FrontEnd`MoveCursorToInputField[
                    EvaluationNotebook[], 
                    "unameboxid"]; $CellContext`ERRORCODE$$ = \
$CellContext`errorCode$; Null]],
                    Enabled->FEPrivate`SameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], False],
                    Evaluator->Automatic,
                    ImageSize->{Automatic, All},
                    Method->"Queued"], 
                    PaneBox[
                    ButtonBox[
                    PaneBox[
                    StyleBox[
                    
                    DynamicBox[FEPrivate`FrontEndResource[
                    "ActivationDialog", "CancelButtonText"],
                    
                    ImageSizeCache->{16.15478515625, {62.7421875, 
                    12.568359375}}], "DialogStyle", "ControlStyle",
                    StripOnInput->False],
                    FrameMargins->{{10, 10}, {0, 0}}],
                    
                    Appearance->{"Default" -> 
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "CancelButton-Default.9.png"], "Hover" -> 
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "CancelButton-Hover.9.png"], "Pressed" -> 
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "CancelButton-Pressed.9.png"], "ButtonType" -> "Cancel"},
                    ButtonFunction:>FrontEndExecute[
                    FrontEnd`FrontEndToken["FrontEndQuitNonInteractive"]],
                    Enabled->True,
                    Evaluator->Automatic,
                    ImageSize->{Automatic, All},
                    Method->"Preemptive"],
                    Alignment->Right,
                    ImageSize->Full]}
                    },
                    AutoDelete->False,
                    
                    GridBoxItemSize->{
                    "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
                    "Grid"],
                    ImageSize->225],
                    ImageMargins->{{0, 0}, {15, 0}}]},
                    {
                    TagBox[
                    TagBox[
                    PaneSelectorBox[{False->
                    StyleBox[
                    StyleBox[
                    TemplateBox[{DynamicBox[
                    FEPrivate`FrontEndResource[
                    "ActivationDialog", "ForgottenPWText"]],
                    TemplateBox[{2}, "Spacer1"],"\"\[RightGuillemet]\""},
                    "RowDefault"], "DialogStyle", "ControlStyle",
                    StripOnInput->False,
                    LineColor->GrayLevel[0.5],
                    FrontFaceColor->GrayLevel[0.5],
                    BackFaceColor->GrayLevel[0.5],
                    GraphicsColor->GrayLevel[0.5],
                    FontColor->GrayLevel[0.5]],
                    StripOnInput->False,
                    LineBreakWithin->False], True->
                    StyleBox[
                    StyleBox[
                    TemplateBox[{DynamicBox[
                    FEPrivate`FrontEndResource[
                    "ActivationDialog", "ForgottenPWText"]],
                    TemplateBox[{2}, "Spacer1"],"\"\[RightGuillemet]\""},
                    "RowDefault"], "DialogStyle", "ControlStyle",
                    StripOnInput->False,
                    LineColor->RGBColor[0.878431, 0.513725, 0.133333],
                    FrontFaceColor->RGBColor[0.878431, 0.513725, 0.133333],
                    BackFaceColor->RGBColor[0.878431, 0.513725, 0.133333],
                    GraphicsColor->RGBColor[0.878431, 0.513725, 0.133333],
                    FontColor->RGBColor[0.878431, 0.513725, 0.133333]],
                    StripOnInput->False,
                    LineBreakWithin->False]}, FrontEnd`CurrentValue[
                    "MouseOver"]],
                    EventHandlerTag[{"MouseClicked" :> FEPrivate`If[
                    FEPrivate`UnsameQ[
                    FrontEnd`CurrentValue["WolframCloudConnected"], 
                    "Pending"], 
                    FrontEndExecute[
                    FrontEnd`NotebookLocate[{
                    URL[
                    "https://user.wolfram.com/portal/password/forgot.html"], 
                    None}]]], PassEventsDown -> True, Method -> "Preemptive", 
                    PassEventsUp -> True}]],
                    MouseAppearanceTag["LinkHand"]]}
                    },
                    AutoDelete->False,
                    
                    GridBoxAlignment->{
                    "Columns" -> {{Left}}, "Rows" -> {{Center}}},
                    
                    GridBoxItemSize->{
                    "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
                    GridBoxSpacings->{"Columns" -> {{0}}, "Rows" -> {{0}}}],
                    "Grid"]],
                    DynamicModuleValues:>{},
                    
                    Initialization:>($CellContext`uidfieldhint$$ := 
                    FE`Evaluate[
                    FEPrivate`FrontEndResource[
                    "ActivationDialog", 
                    "UserIDFieldHint"]]; $CellContext`pwdfieldhint$$ := 
                    FE`Evaluate[
                    FEPrivate`FrontEndResource[
                    "ActivationDialog", 
                    "UserPWFieldHint"]])], $CellContext`ERRORCODE$$ = 
                    CurrentValue[
                    "WolframCloudLoginError"]; $CellContext`errorDisplay$$ = \
$CellContext`errorMessagePane$$[$CellContext`ERRORCODE$$]; \
$CellContext`errorcode$$ = $CellContext`ERRORCODE$$,
                   ImageSizeCache->{227., {109., 115.}}],
                  FrameMargins->0,
                  FrameStyle->None,
                  StripOnInput->False]},
                {
                 PaneSelectorBox[{True->
                  TagBox[
                   ButtonBox[
                    StyleBox[
                    PaneSelectorBox[{False->
                    StyleBox[
                    TemplateBox[{DynamicBox[
                    FEPrivate`FrontEndResource[
                    "ActivationDialog", "ActivateMethodLabel"]],
                    TemplateBox[{2}, "Spacer1"],"\"\[RightGuillemet]\""},
                    "RowDefault"], "DialogStyle", "ControlStyle",
                    StripOnInput->False,
                    LineColor->GrayLevel[0.5],
                    FrontFaceColor->GrayLevel[0.5],
                    BackFaceColor->GrayLevel[0.5],
                    GraphicsColor->GrayLevel[0.5],
                    FontColor->GrayLevel[0.5]], True->
                    StyleBox[
                    TemplateBox[{DynamicBox[
                    FEPrivate`FrontEndResource[
                    "ActivationDialog", "ActivateMethodLabel"]],
                    TemplateBox[{2}, "Spacer1"],"\"\[RightGuillemet]\""},
                    "RowDefault"], "DialogStyle", "ControlStyle",
                    StripOnInput->False,
                    LineColor->RGBColor[0.878431, 0.513725, 0.133333],
                    FrontFaceColor->RGBColor[0.878431, 0.513725, 0.133333],
                    BackFaceColor->RGBColor[0.878431, 0.513725, 0.133333],
                    GraphicsColor->RGBColor[0.878431, 0.513725, 0.133333],
                    FontColor->RGBColor[0.878431, 0.513725, 0.133333]]}, 
                    FrontEnd`CurrentValue["MouseOver"]],
                    StripOnInput->False,
                    LineBreakWithin->False],
                    Appearance->None,
                    ButtonFunction:>DialogReturn[
                    SetOptions[
                    FrontEnd`DialogOpen["ActivationDialog.nb"], 
                    TaggingRules -> {
                    "ActivationState" -> "Method", "ErrorMessage" -> None, 
                    "Unsecured" -> False, "Reactivate" -> True}]],
                    Evaluator->Automatic,
                    ImageMargins->{{0, 0}, {0, 5}},
                    ImageSize->{All, Automatic},
                    Method->"Preemptive"],
                   MouseAppearanceTag["LinkHand"]], False->"\<\"\"\>"}, 
                  FEPrivate`Or[
                   FEPrivate`SameQ[
                    FEPrivate`Runtime`CurrentEnvironment[][
                    "CanActivateManually"], True], 
                   FEPrivate`SameQ[
                    FEPrivate`Runtime`CurrentEnvironment[][
                    "CanActivateUsingLicenseServer"], True]],
                  ImageSize->{All, All}]},
                {
                 PaneBox[
                  TagBox[
                   TagBox[
                    PaneSelectorBox[{False->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "HelpIcon-Default.png"]],
                    ImageSizeCache->{35., {1., 8.}}], True->
                    DynamicBox[FEPrivate`ImportImage[
                    
                    FrontEnd`FileName[{"Dialogs", "Activation"}, 
                    "HelpIcon-Hover.png"]]]}, Dynamic[
                    CurrentValue["MouseOver"]],
                    FrameMargins->0,
                    ImageSize->Automatic],
                    
                    EventHandlerTag[{
                    "MouseClicked" :> 
                    FE`hyperlinkCoded[
                    "http://www.wolfram.com/desktop", "source=signinscreen"], 
                    PassEventsDown -> True, Method -> "Preemptive", 
                    PassEventsUp -> True}]],
                   MouseAppearanceTag["LinkHand"]],
                  ImageMargins->{{255, 0}, {20, 40}}]}
               },
               DefaultBaseStyle->"Column",
               GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Top}}},
               
               GridBoxItemSize->{
                "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
               GridBoxSpacings->{"Columns" -> {{0}}, "Rows" -> {{0}}}],
              "Column"]}
           },
           AutoDelete->False,
           GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Top}}},
           GridBoxItemSize->{
            "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
           GridBoxSpacings->{"Columns" -> {{0}}, "Rows" -> {{0}}}],
          "Grid"]}
       },
       BaselinePosition->{{1, 1}, Baseline},
       DefaultBaseStyle->"Column",
       GridBoxAlignment->{"Columns" -> {{Left}}},
       GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
       
       GridBoxSpacings->{"Columns" -> {{Automatic}}, "Rows" -> {{0}}}],
      "Column"],
     DynamicModuleValues:>{{
       DownValues[$CellContext`errorMessagePane$$] = {HoldPattern[
            $CellContext`errorMessagePane$$[
             Pattern[$CellContext`errorcode, 
              Blank[]]]] :> With[{$CellContext`message = FE`Evaluate[
               FEPrivate`FrontEndResource[
               "WolframCloudLoginErrors", $CellContext`errorcode]]}, 
            Pane[
             If[$CellContext`message === $Failed, "", 
              Style[$CellContext`message, "ControlStyle"]], 
             ImageSize -> {823, 44}, ImageMargins -> {{0, 0}, {51, 0}}, 
             Alignment -> {Left, Center}, 
             FrameMargins -> {{70, 0}, {0, 0}}]]}, 
        Attributes[$CellContext`errorMessagePane$$] = {HoldFirst}}},
     Initialization:>($CellContext`ERRORCODE$$ = 
       CurrentValue["WolframCloudLoginError"]; 
      SetAttributes[$CellContext`errorMessagePane$$, 
        HoldFirst]; $CellContext`errorMessagePane$$[
         Pattern[$CellContext`errorcode, 
          Blank[]]] := With[{$CellContext`message = FE`Evaluate[
            FEPrivate`FrontEndResource[
            "WolframCloudLoginErrors", $CellContext`errorcode]]}, 
         Pane[
          If[$CellContext`message === $Failed, "", 
           Style[$CellContext`message, "ControlStyle"]], 
          ImageSize -> {823, 44}, ImageMargins -> {{0, 0}, {51, 0}}, 
          Alignment -> {Left, Center}, 
          FrameMargins -> {{70, 0}, {0, 
           0}}]]; $CellContext`errorDisplay$$ = \
$CellContext`errorMessagePane$$[$CellContext`ERRORCODE$$]; Null),
     UnsavedVariables:>{$CellContext`ERRORCODE$$, \
$CellContext`errorDisplay$$}],
    FrameMargins->0,
    FrameStyle->None,
    StripOnInput->False]}, All, 2,
   Alignment->{Left, Top},
   ImageSize->825],
  DynamicModuleValues:>{}]],
 CellMargins->{{0, 0}, {0, -1}},
 CellBracketOptions->{"Color"->RGBColor[0.269993, 0.308507, 0.6]},
 CellHorizontalScrolling->True,
 PageBreakAbove->True,
 PageBreakWithin->False,
 ShowAutoStyles->True,
 LineSpacing->{1.25, 0},
 AutoItalicWords->{},
 ScriptMinSize->9,
 ShowStringCharacters->False,
 FontFamily:>CurrentValue["PanelFontFamily"],
 FontSize:>CurrentValue[
  "PanelFontSize"],ExpressionUUID->"4f2afddd-f8a0-4989-8bec-92162125dd3a"]
},
NotebookEventActions->{"ReturnKeyDown" :> FE`Evaluate[
    FEPrivate`FindAndClickDefaultButton[]], {"MenuCommand", "EvaluateCells"} :> 
  FE`Evaluate[
    FEPrivate`FindAndClickDefaultButton[]], {
   "MenuCommand", "HandleShiftReturn"} :> FE`Evaluate[
    FEPrivate`FindAndClickDefaultButton[]], {
   "MenuCommand", "EvaluateNextCell"} :> FE`Evaluate[
    FEPrivate`FindAndClickDefaultButton[]], "EscapeKeyDown" :> (FE`Evaluate[
     FEPrivate`FindAndClickCancelButton[]]; DialogReturn[$Canceled]), 
  "WindowClose" :> (FE`Evaluate[
     FEPrivate`FindAndClickCancelButton[]]; DialogReturn[$Canceled])},
NotebookDynamicExpression:>Refresh[
  FrontEnd`MoveCursorToInputField[
   EvaluationNotebook[], "unameboxid"], None],
WindowSize->{FitAll, FitAll},
WindowMargins->Automatic,
WindowFrame->"ModalDialog",
WindowTitle->FEPrivate`FrontEndResource["ActivationDialog", "WindowTitle"],
CellContext->"Global`",
Background->GrayLevel[1],
FrontEndVersion->"12.0 for Mac OS X x86 (64-bit) (November 14, 2018)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[StyleDefinitions -> "SystemDialog.nb"]], 
   Cell[
    StyleData["LoginErrorLink"], FontColor -> Dynamic[
      If[
       CurrentValue["MouseOver"], 
       RGBColor[0.823529411764706, 0.490196078431373, 0.133333333333333], 
       RGBColor[0.88, 0.02, 0]]]], 
   Cell[
    StyleData[
    "CloudConnectionErrorLink", StyleDefinitions -> 
     StyleData["LoginErrorLink"]], 
    ButtonBoxOptions -> {
     Appearance -> None, 
      ButtonFunction :> (FE`hyperlinkCoded[
       "http://www.wolfram.com/support/contact/email/", 
        "source=signinscreen"]& ), Evaluator -> Automatic}], 
   Cell[
    StyleData[
    "DesktopAccessErrorLink", StyleDefinitions -> 
     StyleData["LoginErrorLink"]], 
    ButtonBoxOptions -> {
     Appearance -> None, 
      ButtonFunction :> (FE`hyperlinkCoded[
       "https://www.wolframcloud.com/app/account/", "source=signinscreen"]& ),
       Evaluator -> Automatic}]}, Visible -> False, FrontEndVersion -> 
  "12.0 for Mac OS X x86 (64-bit) (November 14, 2018)", StyleDefinitions -> 
  "Default.nb"],
Modal -> True
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[603, 21, 1759, 51, 1, InheritFromParent,ExpressionUUID->"4bd2fd68-3264-41d6-84b5-8b4fe7e6a24a",
 PageBreakAbove->True,
 PageBreakWithin->False],
Cell[2365, 74, 26474, 561, 422, InheritFromParent,ExpressionUUID->"4f2afddd-f8a0-4989-8bec-92162125dd3a",
 PageBreakAbove->True,
 PageBreakWithin->False]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature KJNJqJcwpu0eIKk5JOQh3wnP *)

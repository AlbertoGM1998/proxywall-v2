(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      4366,        127]
NotebookOptionsPosition[      3828,        105]
NotebookOutlinePosition[      4410,        127]
CellTagsIndexPosition[      4367,        124]
WindowTitle->Citation Styles
WindowFrame->Palette*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 TagBox[GridBox[{
    {
     PaneBox[
      TemplateBox[{DynamicBox[
         ToBoxes[
          PopupMenu[
           Dynamic[
            CurrentValue[
             InputNotebook[], {PrivateNotebookOptions, "JournalStyle"}]], 
           FrontEndExecute[
            Bib`QueryCitationStylesPacket[
             InputNotebook[]]]], StandardForm], SingleEvaluation -> True, 
         SynchronousUpdating -> False],DynamicBox[
         FEPrivate`FrontEndResource["InsertCitationDialog", "StyleLabel"]]},
       "Labeled",
       DisplayFunction->(GridBox[{{
           ItemBox[#2, DefaultBaseStyle -> "LabeledLabel"], 
           TagBox[
            ItemBox[
             PaneBox[
              TagBox[#, "SkipImageSizeLevel"], 
              Alignment -> {Center, Baseline}, BaselinePosition -> Baseline], 
             DefaultBaseStyle -> "Labeled"], "SkipImageSizeLevel"]}}, 
         GridBoxAlignment -> {"Columns" -> {{Center}}, "Rows" -> {{Center}}}, 
         AutoDelete -> False, 
         GridBoxItemSize -> {
          "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
         BaselinePosition -> {1, 2}]& ),
       InterpretationFunction->(RowBox[{"Labeled", "[", 
          RowBox[{#, ",", #2, ",", "Left"}], "]"}]& )],
      ImageMargins->{{10, 50}, {30, 10}}]},
    {
     ItemBox[
      StyleBox[
       DynamicBox[FEPrivate`FrontEndResource[
        "FEExpressions", "ChoiceButtonsOrder"][{
          ButtonBox[
           DynamicBox[
            FEPrivate`FrontEndResourceString["okButtonText"]], ButtonFunction :> 
           FrontEndExecute[{
              Bib`SetCitationStylePacket[
               InputNotebook[], 
               CurrentValue[
                InputNotebook[], {PrivateNotebookOptions, "JournalStyle"}]], 
              FrontEnd`NotebookClose[
               FrontEnd`ButtonNotebook[]]}], Appearance -> "DefaultButton", 
           Evaluator -> None, Method -> "Preemptive"], 
          ButtonBox[
           DynamicBox[
            FEPrivate`FrontEndResourceString["cancelButtonText"]], 
           ButtonFunction :> FrontEndExecute[
             FrontEnd`NotebookClose[
              FrontEnd`ButtonNotebook[]]], Appearance -> "CancelButton", 
           Evaluator -> None, Method -> "Preemptive"]}],
        ImageSizeCache->{192., {20.5, 25.5}}],
       Deployed->True,
       DynamicUpdating->True,
       ButtonBoxOptions->{ImageMargins->{{2, 2}, {10, 10}},
       ImageSize->Dynamic[
         CurrentValue["DefaultButtonSize"]]},
       GridBoxOptions->{AutoDelete->False,
       GridBoxItemSize->{
        "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}}],
      Alignment->Right,
      StripOnInput->False]}
   },
   DefaultBaseStyle->"Column",
   GridBoxAlignment->{"Columns" -> {{Left}}},
   GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
  "Column"]],
 CellMargins->{{0, 0}, {0, 0}},
 CellBracketOptions->{"Color"->RGBColor[0.269993, 0.308507, 0.6]},
 CellHorizontalScrolling->True,
 PageBreakAbove->True,
 PageBreakWithin->False,
 ShowAutoStyles->True,
 LineSpacing->{1.25, 0},
 AutoItalicWords->{},
 ScriptMinSize->9,
 ShowStringCharacters->False,
 FontFamily:>CurrentValue["PanelFontFamily"],
 FontSize:>CurrentValue["PanelFontSize"]]
},
Evaluator->"System",
WindowSize->All,
WindowMargins->FrontEnd`CurrentValue[
 FrontEnd`$FrontEnd, {
  PrivateFrontEndOptions, "DialogSettings", "CitationStyles", WindowMargins}, 
  Automatic],
WindowElements->{},
WindowTitle->Dynamic[
  FEPrivate`FrontEndResource["InsertCitationDialog", "WindowTitle2"]],
FrontEndVersion->"11.1 for Microsoft Windows (64-bit) (September 6, 2016)",
StyleDefinitions->"SystemPalette.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[588, 21, 3236, 82, 119, InheritFromParent,
 PageBreakAbove->True,
 PageBreakWithin->False]
}
]
*)

(* End of internal cache information *)


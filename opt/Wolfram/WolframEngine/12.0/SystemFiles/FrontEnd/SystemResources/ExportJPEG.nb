(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      6991,        192]
NotebookOptionsPosition[      5497,        150]
NotebookOutlinePosition[      6979,        192]
CellTagsIndexPosition[      6936,        189]
WindowTitle->JPEG Options
WindowFrame->ModalDialog*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {
    ItemBox[GridBox[{
       {
        DynamicBox[ToBoxes[
          FEPrivate`FrontEndResource["ExportDialogs", "JPEGColorSpace"], 
          StandardForm],
         ImageSizeCache->{168., {3., 9.}}], 
        PopupMenuBox[Dynamic[
          
          CurrentValue[$FrontEnd, {
           ConversionOptions, "ExportOptions", "JPEG", "ColorSpace"}, 
           Automatic]], {Automatic->"Automatic", CMYKColor->"CMYKColor", 
         GrayLevel->"GrayLevel", RGBColor->"RGBColor"},
         ImageSize->All]},
       {
        DynamicBox[ToBoxes[
          FEPrivate`FrontEndResource["ExportDialogs", "JPEGCompressionLevel"],
           StandardForm],
         ImageSizeCache->{153., {3., 9.}}], 
        InputFieldBox[Dynamic[
          
          CurrentValue[$FrontEnd, {
           ConversionOptions, "ExportOptions", "JPEG", "CompressionLevel"}, 
           0.25]], Number,
         FieldSize->3]},
       {
        DynamicBox[ToBoxes[
          FEPrivate`FrontEndResource["ExportDialogs", "JPEGSmoothingFactor"], 
          StandardForm],
         ImageSizeCache->{149., {3., 9.}}], 
        InputFieldBox[Dynamic[
          
          CurrentValue[$FrontEnd, {
           ConversionOptions, "ExportOptions", "JPEG", "Smoothing"}, 0]], 
         Number,
         FieldSize->3]},
       {
        RowBox[{
         CheckboxBox[Dynamic[
           
           CurrentValue[$FrontEnd, {
            ConversionOptions, "ExportOptions", "JPEG", "Progressive"}, 
            False]]], "\[InvisibleSpace]", 
         TogglerBox[Dynamic[
           
           CurrentValue[$FrontEnd, {
            ConversionOptions, "ExportOptions", "JPEG", "Progressive"}, 
            False]], {True->
          DynamicBox[ToBoxes[
            FEPrivate`FrontEndResource[
            "ExportDialogs", "JPEGProgressiveFile"], StandardForm],
           ImageSizeCache->{116., {3., 9.}}], False->
          DynamicBox[ToBoxes[
            FEPrivate`FrontEndResource[
            "ExportDialogs", "JPEGProgressiveFile"], StandardForm],
           ImageSizeCache->{116., {3., 9.}}]}, 
          DynamicBox[ToBoxes[
            
            CurrentValue[$FrontEnd, {
             ConversionOptions, "ExportOptions", "JPEG", "Progressive"}, 
             False], StandardForm],
           ImageSizeCache->{26., {0., 9.}}]]}], "\<\"\"\>"}
      },
      AutoDelete->False,
      GridBoxAlignment->{"Columns" -> {Right, Left}},
      GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{3}}}],
     Alignment->Center], "\[SpanFromLeft]"},
   {GridBox[{
      {
       ButtonBox[
        DynamicBox[FEPrivate`FrontEndResource[
         "ImportDialogs", "DefaultsButtonLabel"],
         ImageSizeCache->{121., {3., 9.}}],
        ButtonFunction:>FrontEndExecute[
          FrontEnd`SaveConversionOptions[]]], 
       ButtonBox[
        DynamicBox[FEPrivate`FrontEndResource[
         "ImportDialogs", "RestoreButtonLabel"],
         ImageSizeCache->{97., {1., 9.}}],
        ButtonFunction:>((FEPrivate`Set[
           
           CurrentValue[$FrontEnd, {
            ConversionOptions, "ExportOptions", "JPEG"}], {}]; 
         FrontEndExecute[{
            FrontEnd`SaveConversionOptions[]}])& )]}
     }], 
    StyleBox[
     DynamicBox[FEPrivate`FrontEndResource[
      "FEExpressions", "ChoiceButtonsOrder"][{
        ButtonBox[
         DynamicBox[
          FEPrivate`FrontEndResourceString["okButtonText"]], ButtonFunction :> 
         DialogReturn[], Appearance -> "DefaultButton", Evaluator -> 
         Automatic, Method -> "Preemptive"], 
        ButtonBox[
         DynamicBox[
          FEPrivate`FrontEndResourceString["cancelButtonText"]], 
         ButtonFunction :> (FrontEndExecute[
            FrontEnd`RestoreConversionOptions[]]; DialogReturn[]), Appearance -> 
         "CancelButton", Evaluator -> Automatic, Method -> "Preemptive"]}],
      ImageSizeCache->{192., {20., 26.}}],
     Deployed->True,
     DynamicUpdating->True,
     ButtonBoxOptions->{ImageMargins->{{2, 2}, {10, 10}},
     ImageSize->Dynamic[
       CurrentValue["DefaultButtonSize"]]},
     GridBoxOptions->{AutoDelete->False,
     GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}}]}
  },
  AutoDelete->False,
  GridBoxAlignment->{
   "Columns" -> {Left, Right}, "Rows" -> {{Baseline -> Center}}},
  GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
  GridBoxSpacings->{"Columns" -> {0, 2, 0}, "Rows" -> {0, 1, 0}}]],
 CellMargins->{{8, 8}, {8, 8}},
 CellBracketOptions->{"Color"->RGBColor[0.269993, 0.308507, 0.6]},
 CellHorizontalScrolling->True,
 PageBreakAbove->True,
 PageBreakWithin->False,
 ShowAutoStyles->True,
 LineSpacing->{1.25, 0},
 AutoItalicWords->{},
 ScriptMinSize->9,
 ShowStringCharacters->False,
 FontFamily:>CurrentValue["PanelFontFamily"],
 FontSize:>CurrentValue[
  "PanelFontSize"],ExpressionUUID->"52a125a0-d80f-4c64-8422-bf9636f03b10"]
},
NotebookEventActions->{"ReturnKeyDown" :> FE`Evaluate[
    FEPrivate`FindAndClickDefaultButton[]], {"MenuCommand", "EvaluateCells"} :> 
  FE`Evaluate[
    FEPrivate`FindAndClickDefaultButton[]], {
   "MenuCommand", "HandleShiftReturn"} :> FE`Evaluate[
    FEPrivate`FindAndClickDefaultButton[]], {
   "MenuCommand", "EvaluateNextCell"} :> FE`Evaluate[
    FEPrivate`FindAndClickDefaultButton[]], "EscapeKeyDown" :> (FE`Evaluate[
     FEPrivate`FindAndClickCancelButton[]]; DialogReturn[$Canceled]), 
  "WindowClose" :> (FE`Evaluate[
     FEPrivate`FindAndClickCancelButton[]]; DialogReturn[$Canceled])},
WindowSize->All,
WindowMargins->Automatic,
WindowFrame->"ModalDialog",
WindowTitle->Dynamic[
  FEPrivate`FrontEndResource["ExportDialogs", "JPEGLabel"]],
ButtonBoxOptions->{Appearance->Automatic,
BaselinePosition->Axis,
Evaluator->None,
ImageSize->FrontEnd`CurrentValue["DefaultButtonSize"],
Method->"Preemptive"},
CheckboxBoxOptions->{ImageMargins->{{0, 5}, {0, 0}},
ImageSize->Small},
InputFieldBoxOptions->{ContinuousAction->True},
PopupMenuBoxOptions->{BaseStyle->"DialogStyle",
ImageMargins->{{1, 1}, {2, 2}}},
RadioButtonBoxOptions->{ImageMargins->{{1, 5}, {2, 2}},
ImageSize->Small},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"SystemDialog.nb",
Modal -> True
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[589, 21, 4904, 127, 236, InheritFromParent,ExpressionUUID->"52a125a0-d80f-4c64-8422-bf9636f03b10",
 PageBreakAbove->True,
 PageBreakWithin->False]
}
]
*)

(* End of internal cache information *)


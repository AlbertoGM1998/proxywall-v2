(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      2606,         82]
NotebookOptionsPosition[      1752,         53]
NotebookOutlinePosition[      2589,         81]
CellTagsIndexPosition[      2546,         78]
WindowTitle->Wolfram System Information
WindowFrame->Palette*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 PaneBox[
  DynamicModuleBox[{$CellContext`done$$, $CellContext`sysinfo$$}, 
   PaneSelectorBox[{False->
    PaneBox[
     DynamicBox[FEPrivate`FrontEndResource[
      "FEExpressions", "InfiniteProgress"]],
     ImageMargins->20], True->
    DynamicBox[ToBoxes[$CellContext`sysinfo$$, StandardForm],
     DestroyAfterEvaluation->True]}, Dynamic[
     TrueQ[$CellContext`done$$]],
    ImageSize->Automatic],
   DynamicModuleValues:>{},
   Initialization:>($CellContext`done$$ = False; $CellContext`sysinfo$$ = 
     SystemInformation[]; $CellContext`done$$ = True),
   SynchronousInitialization->False,
   UnsavedVariables:>{$CellContext`done$$, $CellContext`sysinfo$$}],
  ImageMargins->8]],
 CellMargins->{{0, 0}, {0, 0}},
 CellBracketOptions->{"Color"->RGBColor[0.269993, 0.308507, 0.6]},
 CellHorizontalScrolling->True,
 PageBreakAbove->True,
 PageBreakWithin->False,
 ShowAutoStyles->True,
 LineSpacing->{1.25, 0},
 AutoItalicWords->{},
 ScriptMinSize->9,
 ShowStringCharacters->False,
 FontFamily:>CurrentValue["PanelFontFamily"],
 FontSize:>CurrentValue[
  "PanelFontSize"],ExpressionUUID->"7bb78937-59b7-419a-999a-2bc1a3862739"]
},
Saveable->False,
WindowSize->All,
WindowMargins->{{Automatic, 0}, {Automatic, 0}},
WindowElements->{"VerticalScrollBar"},
WindowTitle->Dynamic[
  FEPrivate`Switch[
   FEPrivate`Runtime`CurrentEnvironment[]["Name"], "Player Pro", 
   FEPrivate`FrontEndResource[
   "FEStrings", "aboutboxSysInfoPlayerProPaletteTitle"], "Player", 
   FEPrivate`FrontEndResource[
   "FEStrings", "aboutboxSysInfoPlayerPaletteTitle"], 
   Blank[], 
   FEPrivate`FrontEndResource["FEStrings", "aboutboxSysInfoPaletteTitle"]]],
FrontEndVersion->"11.1 for Mac OS X x86 (32-bit, 64-bit Kernel) (April 18, \
2017)",
StyleDefinitions->"Palette.nb",
ScrollingOptions -> {"PagewiseScrolling" -> False}
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[599, 21, 1149, 30, 31, InheritFromParent, "ExpressionUUID" -> \
"7bb78937-59b7-419a-999a-2bc1a3862739",
 PageBreakAbove->True,
 PageBreakWithin->False]
}
]
*)

(* End of internal cache information *)
(* NotebookSignature 4QqYHZO7dYqo0xAgP01IMe9K *)

(* Paclet Info File *)

(* created 2017/11/24*)

Paclet[
    Name -> "MobileMessaging",
    Version -> "12.0.100",
    MathematicaVersion -> "11.3+",
    Loading -> Automatic,
    Extensions ->
        {
            {"Kernel", Symbols ->
                {
                 "MobileMessaging`MobileMessaging",
                 "System`$MobilePhone"
                },
                Root -> "Kernel",
                HiddenImport -> "MobileMessaging`",
                Context -> {"MobileMessagingLoader`", "MobileMessaging`"}
            }
        }
]

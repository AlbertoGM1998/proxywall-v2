(* Paclet Info File *)

(* created 2016/02/25*)

Paclet[
    Name -> "WikipediaData",
    Version -> "11.99.1",
    MathematicaVersion -> "10.3+",
    Loading -> Automatic,
    Extensions -> 
        {
            {"Kernel", Symbols -> 
                {"System`WikipediaData", "System`WikipediaSearch"}
            , Root -> "Kernel", Context -> 
                {"WikipediaDataLoader`", "WikipediaData`"}
            }
        }
]



(* Paclet Info File *)

(* created 2016/06/24*)

Paclet[
    Name -> "ServiceConnection_PubChem",
    Version -> "12.0.80",
    MathematicaVersion -> "10.4+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"PubChem`", "PubChemLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/PubChem", Language -> All}
        }
]



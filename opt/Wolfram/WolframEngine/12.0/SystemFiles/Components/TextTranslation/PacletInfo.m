(* Paclet Info File *)

(* created 2017/02/07*)

Paclet[
    Name -> "TextTranslation",
    Version -> "12.0.80",
    MathematicaVersion -> "11.1+",
    Loading -> Automatic,
    Extensions ->
        {
            {"Kernel", Symbols ->
                {"System`TextTranslation"}
            , Root -> "Kernel", Context ->
                {"TextTranslationLoader`", "TextTranslation`"}
            }
        }
]

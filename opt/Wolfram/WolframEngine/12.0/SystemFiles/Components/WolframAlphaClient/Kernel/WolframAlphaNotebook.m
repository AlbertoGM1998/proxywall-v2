(* ::Package:: *)

(* ::Section::Closed:: *)
(*Package header*)


(*
	AlphaIntegration`CreateWolframAlphaNotebook,
	AlphaIntegration`NaturalLanguageInputAssistant,
	AlphaIntegration`NaturalLanguageInputBoxes,
	AlphaIntegration`NaturalLanguageInputParse,
	AlphaIntegration`NaturalLanguageInputEvaluate,
	AlphaIntegration`DeleteGeneratedContent,
	AlphaIntegration`DuplicatePreviousCell
*)

Begin["WolframAlphaClient`Private`"];



(*$AlphaQueryExtrusionMonitor = True;*)



$WolframAlphaNotebookWANParse = True;


$WolframAlphaNotebookShowSteps = True;


$ShowStepsQueryUsesParse = True;


$CloudWolframAlphaNotebookQ := TrueQ[$CloudEvaluation]


(* ::Section::Closed:: *)
(*AlphaIntegration`NaturalLanguageInputParse*)


$NaturalLanguageInputParseProgress = ProgressIndicator[Appearance -> "Percolate", ImageSize -> {35, 10}];


$NaturalLanguageInputParseProgressDelay = 2.0;


(*
When calling AlphaIntegration`ExtrusionEvaluate with $WolframAlphaNotebook set to True:
* We need to block replaceEvaluationCell, to avoid any side effects.
* A return value of Null means failure.
* A return value of XMLElement["examplepage", __] means that the example page should be shown.
* Any other return value will go through chooseAndReturnAValue.
* In this mode, that utility should always return a list of the form {Defer[parse], assumptions}
*)


ClearAll[AlphaIntegration`NaturalLanguageInputParse];

AlphaIntegration`NaturalLanguageInputParse["", opts___] = {Failure["NoQuery", <|"Query" -> ""|>], {}};

AlphaIntegration`NaturalLanguageInputParse[str_, opts___] :=
	(* 
		With the Block on $WolframAlphaNotebook below, that will put ExtrusionEvaluate into a mode
		where it returns {Defer[parse], assumptions} or Null.
	*)
	Block[{
			result,
			replaceEvaluationCell,
			sbsProgress,
			$WolframAlphaNotebook = True
		},
		If[$Notebooks && !CurrentValue["InternetConnectionAvailable"],
			Return @ {Failure["Offline", <|"Query" -> str|>], {}}];
		If[$Notebooks && !CurrentValue["AllowDownloads"],
			Return @ {Failure["NetworkDisabled", <|"Query" -> str|>], {}}];
		If[(*
				When not using wanparse.jsp, some gymnastics are needed
				to detect and respond to SBS queries:
			*)
			Not @ TrueQ @ $WolframAlphaNotebookWANParse &&
			$WolframAlphaNotebookShowSteps &&
			MemberQ[result = PreProcessSBS[str], "StepByStepQuery" -> True] &&
			(sbsProgress = True; sbsConfirmedQ[result]),
			(* If this is a step-by-step query, show a step-by-step result *)
			With[{query = "QueryString" /. result},
				Return[{Defer[AlphaIntegration`WolframAlphaStepByStep[query]], {}}]
			]
			(* otherwise, keep going *)
		];
		
		replaceEvaluationCell[___] := Null;
		Replace[
			If[TrueQ[sbsProgress],
				(* if there's already a progress indicator, no need to put up another *)
				doNaturalLanguageParse[str, opts],
				Monitor[
					doNaturalLanguageParse[str, opts],
					$NaturalLanguageInputParseProgress,
					$NaturalLanguageInputParseProgressDelay
				]
			],
			{
				{Defer[expr_], assumptions_} :> {Defer[expr], assumptions},
				$TimedOut :> {Failure["TimedOut", <|"Query" -> str|>], {}},
				{None, assumptions_} :> {Failure["NoParse", <|"Query" -> str|>], assumptions},
				Null -> {Failure["NoParse", <|"Query" -> str|>], {}}
			}
		]
	]


doNaturalLanguageParse[str_, opts___] :=
	If[TrueQ[$WolframAlphaNotebookWANParse],
		doQuerySideEffects[str];
		WolframAlpha[str, "WolframAlphaNotebookParse", opts],
		AlphaIntegration`ExtrusionEvaluate[str, InputForm, opts]
	]


sbsConfirmedQ[{___, "QueryString" -> query_String, ___}] := 
	Module[{sbsresult, tmpobj},
		tmpobj = PrintTemporary[Internal`LoadingPanel[Row[{
					stringResource["StepByStepCheck"],
					ProgressIndicator[Appearance -> "Ellipsis"]
				}]]];
		sbsresult = AlphaIntegration`WolframAlphaStepByStep[query];
		NotebookDelete[tmpobj];
		Switch[sbsresult,
			{___, "ReturnType" -> Except["Reinterpreted"], ___},
				(* Cache the result so we don't have to duplicate the query in AlphaIntegration`NaturalLanguageInputEvaluate *)
				$cachedSBSResults = {query, sbsresult};
				True,
			_,
				PrintTemporary[Internal`LoadingPanel[Row[{
					stringResource["NoStepByStepContinuing"],
					ProgressIndicator[Appearance -> "Ellipsis"]
				}]]];
				False
		]
	];
sbsConfirmedQ[other_] := False


(* ::Section::Closed:: *)
(*AlphaIntegration`NaturalLanguageInputAssistant*)


(* ::Subsection::Closed:: *)
(*NaturalLanguageInputAssistant*)


Options[AlphaIntegration`NaturalLanguageInputAssistant] = {InputAssumptions -> {}};

AlphaIntegration`NaturalLanguageInputAssistant[querystring_String, parse: (Automatic | _Defer), OptionsPattern[]] := 
DynamicModule[{Typeset`querydata, Typeset`update=0},
	Typeset`querydata = Association[
		"query" -> querystring,
		"assumptions" -> OptionValue[InputAssumptions],
		"parse" -> Replace[parse, Automatic -> None],
		"inputpredictions" -> {},
		"otherpredictions" -> {},
		"allassumptions" -> {},
		"summarizeassumptions" -> Automatic,
		"summarizepredictions" -> True
	];
	If[MatchQ[parse, _Defer],
		(* use the given parse without going to W|A *)
		Typeset`querydata["inputpredictions"] = getInputPredictions[parse];
		Typeset`querydata["otherpredictions"] = getOtherPredictions[Typeset`querydata];
		,
		(* otherwise, get a fresh parse from W|A *)
		updateNaturalLanguageAssistant[Typeset`querydata, Typeset`update, False]
	];
	Dynamic[AlphaIntegration`NaturalLanguageInputBoxes[1, Typeset`querydata, Typeset`update], TrackedSymbols -> {}],
	BaseStyle -> {Deployed -> True}
]


(* Other parses aren't handled yet *)
AlphaIntegration`NaturalLanguageInputAssistant[querystring_String, parse_, OptionsPattern[]] := 
	AlphaIntegration`NaturalLanguageInputAssistant[stringResource["ActionNotSupported"], Defer[parse;]]



SetAttributes[AlphaIntegration`NaturalLanguageInputBoxes, HoldAll];

AlphaIntegration`NaturalLanguageInputBoxes[version: 1, qd_, update_] := 
	Style[
		EventHandler[
			Dynamic[
				update;
				Column[
					Flatten[{
						naturalLanguageInputField[qd, update],
						naturalLanguageControlBar[qd, update],
						naturalLanguageAssumptions[qd, update],
						naturalLanguagePredictions[qd, update]
					}],
					BaselinePosition -> {1,1},
					Spacings -> {0,0}
				],
				TrackedSymbols :> {update}
			],
			{"MenuCommand", "HandleShiftReturn"} :>
				(updateNaturalLanguageAssistant[qd, update, True]),
			{"MenuCommand", "EvaluateCells"} :>
				(updateNaturalLanguageAssistant[qd, update, True]),
			Method -> "Queued"
		],
		"ControlStyle",
		ShowAutoStyles -> False
	]

AlphaIntegration`NaturalLanguageInputBoxes[___] := 
	Style[
		"Displaying this Natural Language interface requires a more recent version.",
		"ControlStyle",
		FontColor -> GrayLevel[0.6]
	]


(* ::Subsection::Closed:: *)
(*updateNaturalLanguageAssistant*)


SetAttributes[updateNaturalLanguageAssistant, HoldAll];

updateNaturalLanguageAssistant[qd_, update_, evaluateQ_] := 
	(
		(*
			When evaluating a cell, the insertion point should move after that
			cell, and after any output or output-like cells that follow it. This
			ensures that the output predictions can show up, for example.
		*)
		If[evaluateQ, moveAfterPreviousOutputs[EvaluationCell[], EvaluationNotebook[]]];
		
		(*
			Replace any sequence of newline characters with a space character,
			for now. This will need to be reassessed if the W|A parser starts
			interpreting newlines in interesting ways (e.g. for entering grids).
		*)
		qd["query"] = StringReplace[qd["query"], ("\n" | "\r" | "\[IndentingNewLine]").. -> " "];
		{qd["parse"], qd["allassumptions"]} = AlphaIntegration`NaturalLanguageInputParse[qd["query"], InputAssumptions -> qd["assumptions"]];
		If[MatchQ[qd["parse"], _Defer],
			qd["inputpredictions"] = getInputPredictions[qd["parse"]];
			qd["otherpredictions"] = getOtherPredictions[qd];
			,
			qd["inputpredictions"] = {};
			qd["otherpredictions"] = {};
		];
		
		update++;
		If[evaluateQ,
			If[BoxForm`sufficientVersionQ[12.0],
				MathLink`CallFrontEnd[FrontEnd`CellEvaluate[EvaluationCell[]]],
				(* Before this packet, there was no way to do this without moving the selection. *)
				SelectionMove[EvaluationCell[], All, Cell];
				FrontEndTokenExecute[EvaluationNotebook[], "EvaluateCells"]
			]
		]
	)


(* ::Subsection::Closed:: *)
(*naturalLanguageInputField*)


SetAttributes[naturalLanguageInputField, HoldAll];

naturalLanguageInputField[qd_, update_] := 
	Panel[
		Grid[{{
			Pane[
				Style["\[FreeformPrompt]", FontSize -> 10, FontColor -> RGBColor[0.949219, 0.4375, 0.128906]],
				BaselinePosition -> Scaled[0.15]
			],
			InputField[Dynamic[qd["query"], (qd["query"] = #; qd["assumptions"] = {})&],
				String,
				BaseStyle -> {"NaturalLanguageInputField", AutoIndent -> False},
				BaselinePosition -> Baseline,
				ContinuousAction -> If[$CloudWolframAlphaNotebookQ, False, True],
				FrameMargins -> {{5,5},{2,2}},
				Appearance -> None,
				ImageSize -> {Scaled[1], Automatic},
				System`ReturnEntersInput -> False,
				System`TrapSelection -> False
			],
			If[qd["allassumptions"] === {} && qd["inputpredictions"] === {} && qd["otherpredictions"] === {},
				Nothing,
				naturalLanguageCellMenu[EvaluationCell[], qd, update]
			]
		}}, Spacings -> {0,0}],
		Alignment -> {Left, Center},
		DefaultBaseStyle -> {},
		FrameMargins -> {{3,5},{0,0}},
		ImageMargins -> 0,
		BaselinePosition -> Baseline,
		Appearance -> {"Default" -> imageResource["NLInputQuery.9.png"]}
	]


SetAttributes[naturalLanguageCellMenu, HoldRest];

naturalLanguageCellMenu[cellobj_, qd_, update_] := 
	Button[
		Mouseover[
			bitmapResource["HideQueryDetails"],
			bitmapResource["HideQueryDetailsHot"]
		],
		SelectionMove[cellobj, All, Cell];
		Which[
			TrueQ @ CurrentValue["OptionKey"],
				If[
					MatchQ[Developer`CellInformation[cellobj], {___, "FirstCellInGroup" -> True, ___}],
					SelectionMove[cellobj, All, CellGroup];
				];		
				NotebookWrite[ParentNotebook[cellobj], Cell[qd["query"], "NaturalLanguageInput"], All],
			TrueQ @ CurrentValue[cellobj, {TaggingRules, "WolframAlphaNotebook", "HideDetails"}, False],
				CurrentValue[cellobj, {TaggingRules, "WolframAlphaNotebook", "HideDetails"}] = False,
			True,
				CurrentValue[cellobj, {TaggingRules, "WolframAlphaNotebook", "HideDetails"}] = True
		],
		Appearance -> {"Default" -> None},
		DefaultBaseStyle -> {},
		BaseStyle -> {},
		BaselinePosition -> Baseline,
		Tooltip -> Dynamic[If[
			TrueQ @ CurrentValue[cellobj, {TaggingRules, "WolframAlphaNotebook", "HideDetails"}, False],
			ToBoxes @ FrontEndResource["WAStrings", "ShowQueryDetails"],
			ToBoxes @ FrontEndResource["WAStrings", "HideQueryDetails"]
		]]
	]


(* ::Subsection::Closed:: *)
(*naturalLanguageControlBar*)


(* 
The booleans "summarizepredictions" and "summarizeassumptions" used to indicate
whether to show an abbreviated display for the predictions and assumptions.

We are now moving to a new graphic design where this abbreviated display is no
longer present. So from now on, we will interpret "summarize" as "hide", which
is currently implemented via TaggingRules given to DeployedNLInput cells.
*)


hideQueryDetailsQ[] := 
	TrueQ @ CurrentValue[EvaluationCell[], {TaggingRules, "WolframAlphaNotebook", "HideDetails"}, False]


SetAttributes[naturalLanguageControlBar, HoldAll];


naturalLanguageControlBar[qd_, update_] := {} /; hideQueryDetailsQ[]


naturalLanguageControlBar[qd_, update_] :=
	Replace[
		{
			If[qd["allassumptions"] === {}, {},
				controlBarButton[
					"InputAssumptionsIcon.png",
					"InputAssumptionsIconHover.png",
					"InputAssumptionsIconSelected.png",
					stringResource["ShowAssumptions"],
					Evaluate[qd["summarizeassumptions"] =!= True],
					{qd["summarizepredictions"], qd["summarizeassumptions"]} =
						{True, !TrueQ[qd["summarizeassumptions"]]};
					update++
				]
			],
			If[!MemberQ[qd["otherpredictions"], "StepByStep"], {},
				controlBarButton[
					"ShowStepsIcon.png",
					"ShowStepsIconHover.png",
					None,
					stringResource["ShowStepsButton"],
					False,
					If[TrueQ[$ShowStepsQueryUsesParse],
						(* use the parse coerced to a string, to support relative session references *)
						cellPrintSelectAndEvaluate[
							Cell["show steps " <>
								Replace[qd["parse"], {
									a_Defer :> NaturalLanguage`NaturalLanguage[a],
									_ :> qd["query"]
								}],
								"NaturalLanguageInput"
							]
						],
						(* use the query directly *)
						cellPrintSelectAndEvaluate[Cell["show steps " <> qd["query"], "NaturalLanguageInput"]]
					]
				]
			],
			If[qd["inputpredictions"] === {}, {},
				controlBarButton[
					"InputPredictionsIcon.png",
					"InputPredictionsIconHover.png",
					"InputPredictionsIconSelected.png",
					stringResource["ShowPredictions"],
					Evaluate[qd["summarizepredictions"] =!= True],
					{qd["summarizeassumptions"], qd["summarizepredictions"]} =
						{True, !TrueQ[qd["summarizepredictions"]]};
					update++
				]
			],
			If[!MemberQ[qd["otherpredictions"], "FullResults"], {},
				controlBarButton[
					"FullResultsIcon.png",
					"FullResultsIconHover.png",
					None,
					stringResource["FullResults"],
					False,
					cellPrintSelectAndEvaluate[Cell[qd["query"], "WolframAlphaLong"]]
				]
			]
		} // Flatten,
		{
			buttons: {__} :>
				Panel[
					Row[buttons, "     "],
					Appearance -> {"Default" -> imageResource["NLInputControlBar.9.png"]},
					Alignment -> {Left, Center},
					BaseStyle -> {LineIndent -> 0, LinebreakAdjustments -> {1., 10, 1, 0, 1}},
					ImageSize -> Scaled[1],
					FrameMargins -> 3
				],
			_ :> {}
		}
	]


SetAttributes[controlBarButton, HoldRest]
controlBarButton[icon_, iconhover_, iconselected_, label_, selectedQ_, func_, opts___] := 
	Button[
		If[iconselected === None,
			Mouseover[
				iconAndLabel[icon, Style[label, "NaturalLanguageControlBarButton"]],
				iconAndLabel[iconhover, Style[label, "NaturalLanguageControlBarButtonActive"]],
				ImageSize -> All,
				BaselinePosition -> Baseline
			],
			Framed[
				PaneSelector[{
					True -> iconAndLabel[iconselected, Style[label, "NaturalLanguageControlBarButtonSelected"], "NLDownPointerSelected"],
					False -> Mouseover[
						iconAndLabel[icon, Style[label, "NaturalLanguageControlBarButton"], "NLDownPointer"],
						iconAndLabel[iconhover, Style[label, "NaturalLanguageControlBarButtonActive"], "NLDownPointerHot"],
						ImageSize -> All,
						BaselinePosition -> Baseline
					]
					},
					selectedQ,
					ImageSize -> All,
					BaselinePosition -> Baseline
				],
				Background -> If[selectedQ, RGBColor[0.55682,0.6706,0.7725], None],
				FrameStyle -> If[selectedQ, RGBColor[0.55682,0.6706,0.7725], None],
				FrameMargins -> {{4,4},{0,0}},
				RoundingRadius -> 3,
				BaselinePosition -> Baseline
			]
		],
		func,
		Appearance -> None,
		BaseStyle -> {},
		DefaultBaseStyle -> {},
		BaselinePosition -> Baseline,
		opts
	]


iconAndLabel[icon_, label_, pointer_ : None] := 
	Grid[{{
			Pane[iconResource[icon], BaselinePosition -> Scaled[0.25]],
			If[pointer === None,
				label,
				Row[{label, bitmapResource[pointer]}]
			]
		}},
		Alignment -> {Left, Baseline},
		Spacings -> 0.3,
		BaselinePosition -> {1,2}
	]


(* ::Subsection::Closed:: *)
(*naturalLanguageAssumptions*)


formulaAssumptionPat = XMLElement["assumption", {___, "type" -> Alternatives @@ $FormulaAssumptionTypes, ___}, _];
formulaAssumptionsQ[allassumptions_] := MatchQ[allassumptions, {XMLElement["assumptions", _, {___, formulaAssumptionPat, ___}]}]


SetAttributes[naturalLanguageAssumptions, HoldAll];

naturalLanguageAssumptions[qd_, update_] := {} /; qd["allassumptions"] === {}

naturalLanguageAssumptions[qd_, update_] := {} /; qd["summarizeassumptions"]

naturalLanguageAssumptions[qd_, update_] := {} /; hideQueryDetailsQ[]


naturalLanguageAssumptions[qd_, update_] :=
	Block[{formattedAssumptions, $WolframAlphaNotebook = True},
		formattedAssumptions = FormatAllAssumptions[
			"Assumptions",
			qd["allassumptions"],
			Dynamic[qd["query"]], Dynamic[qd["query"]], Dynamic[{}]
		];
		If[formattedAssumptions === {{},{}}, Return @ {}];
		
		formattedAssumptions = Replace[formattedAssumptions, Framed[Column[x_, ___], ___] :> x, {1}];
		
		(* FIXME: Style hacks that should be moved into WolframAlphaClient.m when the time comes *)
		formattedAssumptions = formattedAssumptions //. {
			"DialogStyle" -> "DialogStyles",
			Button[args___] :> Button[args, BaseStyle -> {}] /; FreeQ[Hold[{args}], BaseStyle],
			ActionMenu[args___] :> ActionMenu[args, DefaultBaseStyle -> {}] /; FreeQ[Hold[{args}], DefaultBaseStyle]
		};
		
		(* Change the button / action menu functions to the appropriate functions in this new interface *)
		formattedAssumptions = formattedAssumptions /. {
			HoldPattern[updateWithAssumptions][nb_, assumptioninputs_, Dynamic[q_], Dynamic[opts_]] :> 
			(
				qd["assumptions"] = removeDuplicateAssumptions[qd["assumptions"], assumptioninputs];
				updateNaturalLanguageAssistant[qd, update, True]
			),
			HoldPattern[setFormulaVariables][prefixes_, inputs_, Dynamic[q_], Dynamic[opts_]] :>
			(
				qd["assumptions"] = removeDuplicateAssumptions[
					qd["assumptions"],
					MapThread[assumptionFromInputAndPrefix, {inputs, prefixes}]
				];
				updateNaturalLanguageAssistant[qd, update, True]
			)
		};
		
		assumptionsPanel @
		Pane[#, ImageMargins -> {{5,0},{0,0}}]& @ 
		Column[
			Flatten[{
				formattedAssumptions
			}],
			BaseStyle -> "NaturalLanguageAssumptions",
			Dividers -> Center,
			FrameStyle -> LightGray,
			Spacings -> 2
		]
	]
		


assumptionsPanel[expr_] := 
	Panel[expr,
		Appearance -> {"Default" -> imageResource["NLInputAssumptions.9.png"]},
		ImageSize -> Scaled[1],
		Alignment -> {Left, Center},
		FrameMargins -> 5,
		ImageMargins -> 0
	]


(* ::Subsection::Closed:: *)
(*predictive interface utilities*)


$showInputPredictions = True;
$showOtherPredictions = True;
$stepByStepQTimeConstraint = 0.5;


loadPredictiveInterface[] := loadPredictiveInterface[] =
	(PredictiveInterface`PredictionControls[]; Null) (* forced autoloading *)


getInputPredictions[Defer[expr_]] := 
	Module[{semanticType, alternateTypes, predictions},
		loadPredictiveInterface[];
		If[$showInputPredictions =!= True, Return[{"", {}, {}}]];
		{semanticType, alternateTypes, predictions} =
			PredictiveInterfaceDump`GetSemanticTypesAndPredictions[expr,
					"PredictionType" -> "Input",
					"DocumentType" -> "AlphaNotebook"
				];
		With[{filtered = PredictiveInterfaceDump`removeIndirectPredictions[predictions]},
			If[ListQ[filtered], predictions = filtered] ];
		If[ListQ[predictions], predictions, {}]
	]



(* Utility to facilitate testing: *)
wanbInputPredictions[Defer[expr_]] := 
	Module[{predToRule, itemToRule},
		predToRule[{_, Predictions`Prediction[_, _, label_, HoldComplete[e_]&], _}] := label :> e;
		predToRule[{_, Predictions`Prediction[_, _, _, items: {__List}], _}] := itemToRule /@ items;
		itemToRule[{label_, HoldComplete[e_]&}] := label :> e;
		Flatten[predToRule /@ getInputPredictions[Defer[expr]]]
	]


(*
Most input predictions come from the PredictiveInterface code, but we choose to add
or not add two such buttons at display time: "show steps" and "Wolfram|Alpha results".

The "show steps" button should be shown whenever there is a parse for which the
StepByStepQ utility returns True.

The "Wolfram|Alpha results" button should be shown whenever there is a parse.
*)

(*
Update: The show steps button and the W|A results button are now handled separately,
in the naturalLanguageControlBar utility.
*)

SetAttributes[showInputPredictions, HoldAll]

showInputPredictions[qd_, update_] := 
Module[{controls},
	loadPredictiveInterface[];
	controls = If[
		TrueQ[$showInputPredictions] && MatchQ[qd["inputpredictions"], {__}],
		(* FIXME: There's some work still to do here, but this will let most things work... *)
		Replace[
			Block[{
				PredictiveInterfaceDump`$DefaultButtonAppearance = (Appearance -> None),
				PredictiveInterfaceDump`$DefaultFrameMargins = 0 },
				PredictiveInterfaceDump`predictionsToControls[Dynamic[0], PredictiveInterfaceDump`KeyCellObjects[None, None], qd["inputpredictions"], True, "ButtonStyles"]
			],
			Style[Row[{controls__}, rowopts___], styleopts___] :> ({controls} /. PredictiveInterface`DoPredictionAction -> DoNLPredictionAction)
		],
		(* otherwise, there are no input predictions *)
		{}
	] // Flatten;
	
	If[controls === {}, {},
		predictionsPanel @ 
		Pane[
			Style[Row[controls, Style[RawBoxes["|"], GrayLevel[0.8]]],
				LineIndent -> 0,
				LinebreakAdjustments -> {1., 10, 1, 0, 1}
			],
			BaselinePosition -> Baseline,
			ImageMargins -> {{5,0},{0,5}}
		]
	]
]



predictionsPanel[expr_] := 
	Panel[expr,
		Appearance -> {"Default" -> imageResource["NLInputPredictions.9.png"]},
		Alignment -> {Left, Center},
		ImageSize -> Scaled[1],
		FrameMargins -> 5,
		ImageMargins -> 0
	]


getOtherPredictions[qd_] :=
	Flatten[{
		If[!StringStartsQ[qd["query"], "show steps "] && stepByStepPredictionQ[Replace[qd["parse"], Defer[expr_] :> Hold[expr]]], "StepByStep", {}],
		If[fullResultsPredictionQ[qd], "FullResults", {}]
	}]


stepByStepPredictionQ[heldparse_] :=
	TimeConstrained[
		And[
			TrueQ[$showOtherPredictions],
			Not[MatchQ[heldparse, Hold[_WolframAlpha | _AlphaIntegration`WolframAlphaStepByStep]]],
			TrueQ[$WolframAlphaNotebookShowSteps],
			TrueQ[StepByStepQ[heldparse, ReleaseHold @ heldparse]] // Quiet
			(* FIXME: How to avoid duplicate evaluation of the parse -- once here, and once for the output? *)
		],
		$stepByStepQTimeConstraint,
		False
	]

fullResultsPredictionQ[qd_] := And[
		TrueQ[$showOtherPredictions]
	]



(*
For a given prediction, the DoNLPredictionAction should create a new
"DeployedNLInput" cell which already includes a parse and
corresponding input predictions, and then evaluate it.
*)
DoNLPredictionAction[Dynamic[mode_], keys_, {label_, action_}, inOutType_, inputQ_] :=
	Module[{in, out, theAction, expr, newquery, boxes},
		loadPredictiveInterface[];
		
		in = out = HoldComplete[None]; (* FIXME *)
		theAction = PredictiveInterfaceDump`addCellExpressions[action, keys];
		If[Head[theAction] =!= Function, Beep[]; Return[$Failed]];
		
		expr = PredictiveInterfaceDump`buildActionExpression[in, out, theAction, inOutType, inputQ];
		If[!MatchQ[expr, HoldComplete[_]], Beep[]; Return[$Failed]];
		
		expr = Defer @@ expr;
		(* Produce a new NL query from the WL parse. *)
		newquery = Symbol["NaturalLanguage`NaturalLanguage"][expr];
		If[!StringQ[newquery], newquery = label];
		boxes = BoxData[ToBoxes[AlphaIntegration`NaturalLanguageInputAssistant[newquery, expr]]];
		cellPrintSelectAndEvaluate[Cell[boxes, "DeployedNLInput"]]
	]


loadSemanticTypes[] := loadSemanticTypes[] =
	(Predictions`MakePredictions[Null]; Null) (* forced autoloading *)


SetAttributes[expressionSemanticTypes, HoldFirst]

expressionSemanticTypes[expr_, mode: "Input" | "Output"] :=
	Module[{semanticTypes, typeTree},
		loadSemanticTypes[];
		semanticTypes = With[{out = If[mode === "Input", Null, expr]},
			Replace[
				Predictions`Private`AttributeVectorsAndSemanticTypes[expr, out, mode],
				{ {attributes_, types_List, ___} :> types, _ :> {} }
			]
		];
		typeTree = Predictions`Private`AssembleTypeTrees[semanticTypes, Predictions`Private`SupertypeRules[]];
		typeTree = ReverseSortBy[typeTree, Last][[All, 1]];
		DeleteDuplicates[Cases[typeTree, (s_String)[___] :> s, Infinity]]
	]


(* ::Subsection::Closed:: *)
(*naturalLanguagePredictions*)


SetAttributes[naturalLanguagePredictions, HoldAll];

naturalLanguagePredictions[qd_, update_] := {} /; qd["query"] === "" || !MatchQ[qd["parse"], _Defer]

naturalLanguagePredictions[qd_, update_] := {} /; qd["summarizepredictions"]

naturalLanguagePredictions[qd_, update_] := {} /; hideQueryDetailsQ[]

naturalLanguagePredictions[qd_, update_] := showInputPredictions[qd, update]


(* ::Section::Closed:: *)
(*AlphaIntegration`NaturalLanguageInputEvaluate*)


(* An empty query shouldn't do anything. *)
AlphaIntegration`NaturalLanguageInputEvaluate[query: "", fmt_] := Null


(* If we're evaluating a fresh cell, do the parse, and then insert and evaluate the interface cell. *)
AlphaIntegration`NaturalLanguageInputEvaluate[query_String, fmt_] := 
Module[{cellsToDelete, boxes},
	cellsToDelete = previousOutputs[EvaluationCell[], EvaluationNotebook[]];
	boxes = BoxData[ToBoxes[AlphaIntegration`NaturalLanguageInputAssistant[query, Automatic]]];
	(* replace the "NaturalLanguageInput" cell with a "DeployedNLInput" cell, and then evaluate it *)
	MathLink`CallFrontEnd[FrontEnd`RewriteExpressionPacket[Cell[boxes, "DeployedNLInput"]]];
	AlphaIntegration`NaturalLanguageInputEvaluate[boxes, fmt, cellsToDelete]
]


(*
Accept a RowBox of strings as a proxy for the StringJoin'd string, which is
what the Cloud will be sending. CLOUD-12925
*)
AlphaIntegration`NaturalLanguageInputEvaluate[BoxData[query: (_RowBox | _String)], fmt_] := 
Module[{querystring},
	querystring = query //. RowBox[{strs__String}] :> StringJoin[strs];
	AlphaIntegration`NaturalLanguageInputEvaluate[querystring, fmt] /; StringQ[querystring]
]


(* For purposes of this interface, the format type can be effectively ignored. *)
AlphaIntegration`NaturalLanguageInputEvaluate[BoxData[FormBox[boxes_, ___]], fmt_, rest___] :=
	AlphaIntegration`NaturalLanguageInputEvaluate[BoxData[boxes], fmt, rest]


(* If we're evaluating an interface cell, remove any previous outputs, and then insert new ones. *)
AlphaIntegration`NaturalLanguageInputEvaluate[BoxData[HoldPattern[DynamicModuleBox][vars_, __]], fmt_, cellsToDelete_ : Automatic] := 
Module[{qd, protected},
	qd = FirstCase[Hold[vars], HoldPattern[Set][Typeset`querydata$$, data_] :> data, $Failed, Infinity];
	WolframAlphaNotebookQuery[$Line] = {qd["query"], qd["assumptions"]};
	
	(* delete any previous results *)
	Switch[cellsToDelete,
		{__CellObject}, NotebookDelete @ cellsToDelete,
		Automatic, deletePreviousOutputs[EvaluationCell[], EvaluationNotebook[]]
	];
	
	Replace[qd["parse"], {
		(* If there was no query, revert to the simple cell. *)
		Failure["NoQuery", ___] :>
			MathLink`CallFrontEnd[FrontEnd`RewriteExpressionPacket[Cell["", "NaturalLanguageInput"]]],
		(* If there was a failure, show it as input, but do not produce an output cell. *)
		Failure[tag_String, ___] :> writeFailureInput[tag],
		(* If this query was flagged as a possible SBS query, try to go get the steps. *)
		Defer[AlphaIntegration`WolframAlphaStepByStep[query_String, ___]] :> 
			Module[{tmpobj, sbsresult},
				If[MatchQ[$cachedSBSResults, {query, _}],
					(* if we cached these results when running sbsConfirmedQ for this query, use the cache *)
					sbsresult = $cachedSBSResults[[2]],
					(* otherwise, do the query *)
					tmpobj = PrintTemporary[Internal`LoadingPanel[Row[{
						stringResource["StepByStepCheck"],
						ProgressIndicator[Appearance -> "Ellipsis"]
					}]]];
					sbsresult = First[qd["parse"]];
					NotebookDelete[tmpobj]
				];
				(* either way, clear the cache *)
				$cachedSBSResults = {};
				Switch[FirstCase[sbsresult, ("ReturnType" -> type_) :> type, "NoParse"],
					"NoParse",
						writeFailureInput["NoParse"],
					"NoStepByStep" | "Reinterpreted",
						writeFailureInput["NoStepByStep"];
						FirstCase[sbsresult, ("Result" -> res_) :> res, $Failed],
					"HasStepByStep" | _,
						FirstCase[sbsresult, ("Result" -> res_) :> res, $Failed]		
				]
			],
		(* If the parse contains a Placeholder, write the new WLInput, but don't evaluate it *)
		Defer[expr_] /; !FreeQ[Defer[expr], _Placeholder] :> (
			writeNewWolframLanguageInput[EvaluationCell[], EvaluationNotebook[], qd["parse"], False, fmt];
			Null
		),
		(* If there was any other parse, show it in a new WLInput cell, and evaluate it *)
		Defer[expr_] :> (
			(* Set In[n] to be the parse -- FIXME: Is there a better way to do this? *)
			Internal`WithLocalSettings[
				protected = Unprotect[In], In[$Line] := expr, Protect @@ protected];
			If[StringStartsQ[qd["query"], "show steps "],
				(* If steps were requested but a parse was returned, then there were no steps *)
				writeFailureInput["NoStepByStep"],
				writeNewWolframLanguageInput[EvaluationCell[], EvaluationNotebook[], qd["parse"], True, fmt]
			];
			AlphaIntegration`NaturalLanguageInputEvaluate[Defer[expr]]
		),
		(* If there is a direct hit on an example page, construct and show the page. *)
		XMLElement["examplepage", {___, "url" -> url_String, ___}, _] :>
			showExamplesAsWolframAlphaNotebook[url],
		(* Otherwise, there was no parse. Don't produce any new inputs or outputs. *)
		(None | _) :>
			((*Beep[];*) Missing["NoParse", qd["query"]]; Null)
	}]
]


(* Any other box structure should be coerced to a string and treated as a fresh query *)
AlphaIntegration`NaturalLanguageInputEvaluate[boxes_, fmt_, rest___] :=
	Module[{str},
		str = Replace[
			MathLink`CallFrontEnd[ FrontEnd`ExportPacket[boxes, "InputText"] ],
			{ {a_String, ___} :> a, else_ :> None }
		];
		AlphaIntegration`NaturalLanguageInputEvaluate[str, fmt] /; StringQ[str]
	]


(*
The one-argument form is used when evaluating exprs in DeployedWLInput cells.
This is where we change the default behavior of Integrate, Plot, and any other
functions, when it is decided that we want different defaults in W|A notebooks.
*)
AlphaIntegration`NaturalLanguageInputEvaluate[Defer[expr_]] :=
Module[{originalSettings},
	Internal`WithLocalSettings[
		originalSettings = Options @@@ {
			{Integrate, GeneratedParameters},
			{Plot, AxesOrigin},
			{ListPlot, AxesOrigin}
		};
		SetOptions[Integrate, GeneratedParameters -> C];
		SetOptions[Plot, AxesOrigin -> {0, 0}];
		SetOptions[ListPlot, AxesOrigin -> {0, 0}];
		,
		(*
			Allow messages to be generated during initial evaluation of expr,
			but Quiet any messages that would be generated upon exiting
			WithLocalSettings. This addresses 357770.
		*)
		Quiet[expr, None, All]
		,
		MapThread[SetOptions, {{Integrate, Plot, ListPlot}, originalSettings}];
	] 
] // Quiet


(* The procedure for determining previously generated output cells is:

	- If OutputAutoOverwrite is False for the notebook, stop.
	- Otherwise, examine the cell immediately after the input cell.
		- If it's not Deletable, stop.
		- If it's not CellAutoOverwrite, stop.
		- Otherwise, mark it for deletion, and examine the next cell.

This is not quite the same as the Desktop front end's algorithm. The FE checks
for Evaluatable -> False right after Deletable. But we can't do that, because we
have to be able to delete "DeployedWLInput" cells, which can be evaluated.

The FE also does something special if it encounters a cell group. But we're not
going to bother with that for now.
*)

previousOutputs[cellobj_, nbobj_] := 
	Module[{cells, objs = {}},
		If[Not @ TrueQ @ AbsoluteCurrentValue[nbobj, OutputAutoOverwrite], Return[{}]];
		cells = NextCell[cellobj, All];
		If[!MatchQ[cells, {__CellObject}], Return[{}]];
		Do[
			If[
				TrueQ @ AbsoluteCurrentValue[cell, Deletable] &&
				TrueQ @ AbsoluteCurrentValue[cell, CellAutoOverwrite], AppendTo[objs, cell], Break[]],
			{cell, cells}
		];
		objs
	]


deletePreviousOutputs[cellobj_, nbobj_] :=
	Replace[previousOutputs[cellobj, nbobj], {
		cells: {__CellObject} :> NotebookDelete[cells],
		_ :> None
	}]

moveAfterPreviousOutputs[cellobj_, nbobj_] := 
	Replace[previousOutputs[cellobj, nbobj], {
		{___, lastcell_CellObject} :> SelectionMove[lastcell, After, Cell],
		_ :> SelectionMove[cellobj, After, Cell]
	}]


writeFailureInput[tag: ("Offline" | "NetworkDisabled" | "TimedOut" | "NoParse" | "NoStepByStep")] := 
	CellPrint @ Cell[BoxData[ToBoxes[
		Grid[{{
			Switch[tag,
				"Offline", bitmapResource["NetworkFailureVector", BaselinePosition -> Scaled[0.1]],
				"NetworkDisabled", bitmapResource["NetworkDisabledVector", BaselinePosition -> Scaled[0.1]],
				"TimedOut", bitmapResource["NetworkFailureVector", BaselinePosition -> Scaled[0.1]],
				"NoParse", bitmapResource["WAErrorIndicator"],
				"NoStepByStep", bitmapResource["WAErrorIndicator"]
			],
			Style[cachedMessageTemplate["WAStrings", "MessageTemplate:" <> tag], Deployed -> False]
			}},
			Spacings -> 0.3,
			Alignment -> Left
		]]],
		"DeployedWLInput",
		"NaturalLanguageFailure"
	]


writeNewWolframLanguageInput[cellobj_, nbobj_, Defer[expr_], label_, fmt_] := 
	Module[{boxes, obj, help},
		Which[
			MatchQ[Defer[expr], Defer[_WolframAlphaResult]],
				boxes = ToBoxes[Iconize[
					Unevaluated[expr],
					Tooltip[
						stringResource["WolframAlphaResultIconizeLabel"],
						ToString[Unevaluated[expr], InputForm]
					]
				]];
				help = None
			,
			(* otherwise, we have a real parse *)
			True,
				boxes = AlphaQueryMakeBoxes[expr, "WolframAlphaNotebookForm"];
				help = constructHelpCell[Defer[expr]];
		];
		obj = cellPrintReturnObject @ Cell[
			BoxData[ If[fmt === TraditionalForm, FormBox[boxes, TraditionalForm], boxes] ],
			"DeployedWLInput",
			If[TrueQ[label],
				(* Fake the CellLabel, since this isn't actually the cell that was evaluated to get the output *)
				CellLabel -> ("In[" <> ToString[$Line] <>"]:="),
				Unevaluated[Sequence[]]
			]
		];
		If[Head[help] === Cell, attachHelpCell[obj, help]];
		obj
	]


(* ::Section::Closed:: *)
(*AlphaIntegration`CreateWolframAlphaNotebook*)


WolframAlphaNotebookPut[Notebook[cells_, opts___]] := 
	Module[{size, margins},
		{size, margins} = Replace[
			FE`Evaluate[FEPrivate`NewWindowSizeAndPosition[]],
			Except[{_,_}] :> {Automatic, Automatic}
		];
		NotebookPut[Notebook[cells, opts,
			StyleDefinitions -> "WolframAlphaNotebook.nb",
			WindowSize -> size,
			WindowMargins -> margins
		]]
	]


AlphaIntegration`CreateWolframAlphaNotebook[opts___] := 
	Module[{nbobj},
		nbobj = WolframAlphaNotebookPut[Notebook[{Cell["", "NaturalLanguageInput"]}, opts]];
		(* Move the insertion point into the first NLInput cell *)
		Replace[
			Cells[nbobj, CellStyle -> "NaturalLanguageInput"],
			{cellobj_, ___} :> SelectionMove[cellobj, After, CellContents]
		];
		nbobj
	]


(* ::Section::Closed:: *)
(*Example page support*)


showExamplesAsWolframAlphaNotebook[url_String] := 
	showExamplesAsWolframAlphaNotebook[Import[StringReplace[url, "-content.html" -> ".xml"], "XML"]]


showExamplesAsWolframAlphaNotebook[xml_] :=
	Module[{examples},
		examples = Flatten[Cases[xml, XMLElement["example", _, a_] :> a, Infinity]];
		examples = wanbExampleCell /@ examples;
		WolframAlphaNotebookPut[Notebook[examples]]
	]


wanbExampleCell[XMLElement["category", _, {a_String}]] := Cell[a, "Title"]
wanbExampleCell[XMLElement["section-title", _, {a_String}]] := Cell[a, "Section"]
wanbExampleCell[XMLElement["section-title", _, {XMLElement["link", _, {a_String}]}]] := Cell[a, "Section"]
wanbExampleCell[XMLElement["caption", _, {a_String}]] := Cell[a, "Text"]
wanbExampleCell[XMLElement["input", _, {a_String}]] := Cell[a, "NaturalLanguageInput"]


(* ::Section::Closed:: *)
(*Utilities*)


(* There's a packet for doing this in the 12.0 front end *)
cellPrintReturnObject[c_Cell] := 
	MathLink`CallFrontEnd[FrontEnd`CellPrintReturnObject[c]] /; BoxForm`sufficientVersionQ[12.0]

(* Otherwise, we have to fiddle with the selection *)
cellPrintReturnObject[c_Cell] :=
	Module[{before, after, nb, obj},
		nb = EvaluationNotebook[];
		before = Cells[nb];
		CellPrint[c];
		after = Cells[nb];
		Replace[Complement[after, before], {{one_CellObject} :> one, _ :> $Failed}]
	]


cellPrintSelectAndEvaluate[c_Cell] := 
	Replace[cellPrintReturnObject[c], {
		obj_CellObject :> (
			CurrentValue[obj, GeneratedCell] = Inherited;
			CurrentValue[obj, CellAutoOverwrite] = Inherited;
			(*
				The correct location for the insertion point after the cell has been
				sent for evaluation is immediately following that cell.
			*)
			If[$CloudWolframAlphaNotebookQ,
				(* This gets the selection right on Cloud, despite CLOUD-15696. Reported as CLOUD-15699 *)
				MathLink`CallFrontEnd[FrontEnd`CellEvaluate[obj]],
				(* This gets the selection right on Desktop *)
				SelectionMove[obj, All, Expression];
				FrontEndTokenExecute["HandleShiftReturn"]
			];
			(*SelectionMove[obj, After, Cell];*)
		),
		_ :> $Failed
	}]


notebookWriteReturnObject[nbobj_NotebookObject, args__] := 
	Module[{before, after, obj},
		before = Cells[nbobj];
		NotebookWrite[nbobj, args];
		after = Cells[nbobj];
		(* FIXME: Obviously this fails if the NotebookWrite doesn't result in a new cell. *)
		Replace[Complement[after, before], {{one_CellObject} :> one, _ :> $Failed}]
	]


(* 
The AlphaIntegration`DuplicatePreviousCell utility implements Insert Input and
Output From Above in a W|A notebook.

For Input: Look for the previous "NaturalLanguageInput" or "DeployedNLInput"
style cell. Extract the query string, and depending on the selection, write it
into a new "NaturalLanguageInput" cell or an existing cell.

For Output: Look for the previous "Output" style cell and copy it down to an
"Input" style cell, or into an existing cell.
*)


AlphaIntegration`DuplicatePreviousCell[nbobj_NotebookObject, style: ("Input" | "Output")] := 
	Module[{styles, cellobj, convertcell},
		styles = If[style === "Input", {"NaturalLanguageInput", "DeployedNLInput"}, {"Output"}];
		
		(* If there's the wrong kind of selection, do nothing *)
		If[!MatchQ[CurrentValue[nbobj, "SelectionType"], "CellCaret" | "TextCaret" | "TextRange"],
			Beep[]; Return[$Failed]
		];
		(* Otherwise, the selection is between cells or inside a cell. Either way, PreviousCell will work.*)
		cellobj = PreviousCell[NotebookSelection[nbobj], CellStyle -> styles];

		convertcell = If[style === "Input", convertToNaturalLanguageInputCell, convertToInputCell];
		
		Replace[cellobj, {
			obj_CellObject :> NotebookWrite[nbobj, convertcell[NotebookRead[obj]], After],
			_ :> (Beep[]; $Failed)
		}]
	]


convertToInputCell[Cell[a_, style__String, opts___]] := 
	Cell[a, "Input", Sequence @@ DeleteCases[Flatten[{opts}], _[CellLabel | TaggingRules | CellChangeTimes, _]]]


convertToNaturalLanguageInputCell[Cell[query_String, style__String, opts___]] := 
	Cell[query, "NaturalLanguageInput", Sequence @@ DeleteCases[Flatten[{opts}], _[CellLabel | TaggingRules | CellChangeTimes, _]]]

convertToNaturalLanguageInputCell[Cell[BoxData[boxes_], style__String, opts___]] :=
	convertToNaturalLanguageInputCell[boxes, opts]
convertToNaturalLanguageInputCell[FormBox[boxes_, ___], opts___] :=
	convertToNaturalLanguageInputCell[boxes, opts]
convertToNaturalLanguageInputCell[DynamicModuleBox[{___, _[Typeset`querydata$$, qd_Association], ___}, __], opts___] := 
	convertToNaturalLanguageInputCell @ Cell[qd["query"], "NaturalLanguageInput", opts]
	
convertToNaturalLanguageInputCell[else_, opts___] :=
	convertToNaturalLanguageInputCell @ Cell["", "NaturalLanguageInput", opts]


(*
AlphaIntegration`DeleteGeneratedContent is used by Delete All Output in a W|A
notebook. It deletes all the content that "DeleteGeneratedCells" does, plus it
discards previous search results, assumptions, input predictions, etc, by
replacing "DeployedNLInput" cells with "NaturalLanguageInput" cells.
*)
AlphaIntegration`DeleteGeneratedContent[nbobj_NotebookObject] := 
	Module[{cells},
		(* delete all generated cells *)
		cells = Cells[nbobj, GeneratedCell -> True];
		If[ListQ[cells], NotebookDelete[cells, AutoScroll ->  True]];
		
		(* replace "DeployedNLInput" cells with "NaturalLanguageInput" cells *)
		cells = Cells[nbobj, CellStyle -> "DeployedNLInput"];
		If[!ListQ[cells], Return[]];
		Do[
			Replace[NotebookRead[cell], {
				Cell[BoxData[HoldPattern[DynamicModuleBox][vars_, __]], "DeployedNLInput", opts___] :> 
					With[{qd = FirstCase[Hold[vars], HoldPattern[Set][Typeset`querydata$$, data_] :> data, $Failed, Infinity]},
						If[StringQ[qd["query"]],
							NotebookWrite[cell,
								Cell[qd["query"], "NaturalLanguageInput",
									Sequence @@ DeleteCases[Flatten[{opts}], _[CellLabel, _]]],
								None,
								AutoScroll -> False
							]
						]
					]
			}],
			{cell, cells}
		]	
	];


(* ::Section::Closed:: *)
(*Package footer*)


End[];


Paclet[
    Name -> "WolframAlphaClient",
    Version -> "4.6.7",
    MathematicaVersion -> "12+",
    Root -> ".",
    Internal -> True,
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> "WolframAlphaClient`"}, 
            {"FrontEnd", Prepend -> True}
        }
]

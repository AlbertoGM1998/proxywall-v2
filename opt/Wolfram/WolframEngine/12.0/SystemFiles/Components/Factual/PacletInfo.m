(* Paclet Info File *)

(* created 2015/11/20*)

Paclet[
    Name -> "ServiceConnection_Factual",
    Version -> "12.0.80",
    MathematicaVersion -> "10.2+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Factual`", "FactualFunctions`", "FactualLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Factual", Language -> All}
        }
]



(* Paclet Info File *)

(* created 2016/02/29*)

Paclet[
    Name -> "ServiceConnection_Wikipedia",
    Version -> "12.0.80",
    MathematicaVersion -> "10.3+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Wikipedia`", "WikipediaFunctions`", "WikipediaLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Wikipedia", Language -> All}
        }
]



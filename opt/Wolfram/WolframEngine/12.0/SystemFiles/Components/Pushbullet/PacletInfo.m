(* Paclet Info File *)

(* created 2016/05/26*)

Paclet[
    Name -> "ServiceConnection_Pushbullet",
    Version -> "12.0.80",
    MathematicaVersion -> "10.2+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Pushbullet`", "PushbulletLoad`", "PushbulletAPIFunctions`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Pushbullet", Language -> All}
        }
]



Paclet[
	Name -> "GraphStore",
	Version -> "1.0.0",
	MathematicaVersion -> "12.0+",
	Updating -> Automatic,
	Extensions -> {
		{"Kernel", Root -> "Kernel", Context -> {"GraphStore`"}},
		{"Documentation", Language -> All}
	}
]

(* Paclet Info File *)

(* created 2018/03/23*)

Paclet[
    Name -> "ServiceConnection_Twitter",
    Version -> "11.3.1",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Twitter`", "TwitterFunctions`", "TwitterLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Twitter", Language -> All}
        }
]



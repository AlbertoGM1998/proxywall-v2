(* Paclet Info File *)

(* created 2017/06/23*)

Paclet[
    Name -> "ServiceConnection_SurveyMonkey",
    Version -> "11.2.6",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"SurveyMonkey`", "SurveyMonkeyLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/SurveyMonkey", Language -> All}
        }
]



Paclet[
    Name -> "CompiledLibrary",
    Version -> "0.9",
    WolframVersion -> "12+",
    Updating -> Automatic,
    Loading -> Automatic,
    Extensions -> {
        {"Kernel", HiddenImport -> {},
        	Context -> {"CompiledLibrary`"},
        	Symbols -> {
        		"CompiledLibrary`FunctionCompileLibraryFunctionLoad",
                "CompiledLibrary`CompiledLibraryLoadFunction"}}
    }
]

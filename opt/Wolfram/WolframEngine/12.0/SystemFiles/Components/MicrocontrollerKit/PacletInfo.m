(* Paclet Info File *)

(* created 2017/05/30*)

Paclet[
    Name -> "MicrocontrollerKit",
    Version -> "1.0.2",
    MathematicaVersion -> "12.0+",
    Creator -> "Suba Thomas, Wolfram Research Inc.",
    Loading -> Automatic,
    Extensions -> 
        {
            {"Kernel", 
            	Root -> "Kernel", 
            	Context -> {"MicrocontrollerKit`"}
            }, 
            {"Documentation", 
            	Language -> "English",
            	Resources -> {"Guides/MicrocontrollerKit"},  
            	MainPage -> "Guides/MicrocontrollerKit"
            }
        }
]



(* Paclet Info File *)

(* created 2019/01/18*)

Paclet[
    Name -> "ResourceSystemClient",
    Version -> "1.12.0",
    MathematicaVersion -> "12.0+",
    Loading -> Automatic,
    Extensions -> 
        {
            {"Kernel", Symbols -> 
                {"System`$PublisherID", "System`$ResourceSystemBase", "System`PublisherID", "System`ResourceAcquire", "System`ResourceObject", "System`ResourceRegister", "System`ResourceRemove", "System`ResourceSearch", "System`ResourceSubmissionObject", "System`ResourceSubmit", "System`ResourceSystemBase", "System`ResourceUpdate"}
            , Root -> "Kernel", Context -> 
                {"ResourceSystemClientLoader`", "ResourceSystemClient`"}
            }, 
            {"FrontEnd", Prepend -> True}
        }
]



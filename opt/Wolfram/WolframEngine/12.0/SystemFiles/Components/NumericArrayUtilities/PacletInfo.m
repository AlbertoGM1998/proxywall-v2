Paclet[
	Name -> "NumericArrayUtilities",
	Version -> "1.0",
	MathematicaVersion -> "10.4+",
	Description -> "Utilities for doing high-performance numeric computations.",
	Loading -> Automatic,
	Creator -> "Sebastian Bodenstein <sebastianb@wolfram.com>, Taliesin Beynon <taliesinb@wolfram.com>",
	Extensions -> {
		{"Kernel", Context -> "NumericArrayUtilities`"},
		{"LibraryLink"},
		{"Resource",
			Root -> "Resources",
    		Resources -> {
	    		{"PrecompiledCharsMap", "BPE/precompiled_charmaps.wxf"},
	    		{"SentencePieceModelProto", "BPE/sentencepiece_model.proto"}
    		}
		}
	}
]

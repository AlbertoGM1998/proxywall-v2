(* Paclet Info File *)

(* created 2017/01/05*)

Paclet[
    Name -> "ServiceConnection_RunKeeper",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"RunKeeper`", "RunKeeperLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/RunKeeper", Language -> All}
        }
]



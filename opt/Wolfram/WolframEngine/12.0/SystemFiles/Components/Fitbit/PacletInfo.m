(* Paclet Info File *)

(* created 2017/02/15*)

Paclet[
    Name -> "ServiceConnection_Fitbit",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Fitbit`", "FitbitLoad`", "FitbitFunctions`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Fitbit", Language -> All}
        }
]



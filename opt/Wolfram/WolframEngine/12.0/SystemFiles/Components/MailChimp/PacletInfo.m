(* Paclet Info File *)

(* created 2016/02/19*)

Paclet[
    Name -> "ServiceConnection_MailChimp",
    Version -> "12.0.80",
    MathematicaVersion -> "10.2+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"MailChimp`", "MailChimpLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/MailChimp", Language -> All}
        }
]



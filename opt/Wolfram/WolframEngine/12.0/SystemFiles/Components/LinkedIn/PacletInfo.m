(* Paclet Info File *)

(* created 2017/07/07*)

Paclet[
    Name -> "ServiceConnection_LinkedIn",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"LinkedIn`", "LinkedInLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/LinkedIn", Language -> All}
        }
]



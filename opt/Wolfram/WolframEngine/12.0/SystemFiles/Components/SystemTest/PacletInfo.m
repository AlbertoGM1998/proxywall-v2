(* ::Package:: *)

(* Paclet Info File *)

(* created Aug. 9 2017*)

Paclet[
    Name -> "SystemTest",
    Version -> "0.0.1",
    MathematicaVersion -> "11.2+",
    Loading -> Automatic,
    Extensions -> {
        {"Kernel",
            Root -> "Kernel",
            Context -> {
                "SystemTest`"
            },
            Symbols -> {
                "System`SystemTest"
            }
        },
        {"Application", Context -> "SystemTest`"}
    }
]



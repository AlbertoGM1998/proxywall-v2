# NodeJS.wl

This file registers NodeJS as an external evaluator for the WL

* "ProgramFileFunction" points to ``PacletManager`PacletResource["ExternalEvaluate_NodeJS", "NodeJSREPL"]`` which is `eval.js`, the core code that sets up the connection between NodeJS and WL

# TODO

* move the icons to a separate resource file

BeginPackage["LLVMLink`LLVMInformation`"]
$LLVMInformation
Begin["`Private`"]
$LLVMInformation = <|
	"LLVM_DEFAULT_TARGET_TRIPLE" -> "armv7l-unknown-linux-gnueabihf",
	"LLVM_ENABLE_ABI_BREAKING_CHECKS" -> False,
	"LLVM_ENABLE_THREADS" -> True,
	"LLVM_HAS_ATOMICS" -> True,
	"LLVM_HOST_TRIPLE" -> "armv7l-unknown-linux-gnueabihf",
	"LLVM_NATIVE_ARCH" -> "ARM",
	"LLVM_NATIVE_ASMPARSER" -> "LLVMInitializeARMAsmParser",
	"LLVM_NATIVE_ASMPRINTER" -> "LLVMInitializeARMAsmPrinter",
	"LLVM_NATIVE_DISASSEMBLER" -> "LLVMInitializeARMDisassembler",
	"LLVM_NATIVE_TARGET" -> "LLVMInitializeARMTarget",
	"LLVM_NATIVE_TARGETINFO" -> "LLVMInitializeARMTargetInfo",
	"LLVM_NATIVE_TARGETMC" -> "LLVMInitializeARMTargetMC",
	"LLVM_ON_UNIX" -> True,
	"LLVM_ON_WIN32" -> False,
	"LLVM_USE_INTEL_JITEVENTS" -> False,
	"LLVM_USE_OPROFILE" -> False,
	"LLVM_VERSION" -> 7.0,
	"LLVM_VERSION_MAJOR" -> 7,
	"LLVM_VERSION_MINOR" -> 0,
	"LLVM_VERSION_PATCH" -> 1,
	"LLVM_VERSION_STRING" -> "7.0.1"
|>;
End[]
EndPackage[]


BeginPackage["LLVMLink`BuildInfo`"]



Begin["`Private`"]

LLVMLink`Internal`$BuildInfo = <|
    "Date" -> "Wed May 29 08:39:36 2019",
    "Branch" -> "release/B12_0_1",
    "CommitSHA" -> "4a63eacf6211ca154b440a048743671b35a354bc",
    "CommitMessage" -> "Updated the paclet version in the V12 release branch to 1.0.0.1.",
    "RefSpec" -> "refs/heads/release/B12_0_1"
|>

End[]

EndPackage[]

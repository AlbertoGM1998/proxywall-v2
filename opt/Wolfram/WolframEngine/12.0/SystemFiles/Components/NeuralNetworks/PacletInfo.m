Paclet[
	Name -> "NeuralNetworks",
	MathematicaVersion -> "12.0+",
	Version -> "12.0.10", (* <- BUMP FINAL VERSION NUMBER WHENEVER AN UPGRADE RULE IS INTRODUCED *)
	Description -> "Neural Network functionality",
	Creator -> "Taliesin Beynon <taliesinb@wolfram.com>, Sebastian Bodenstein <sebastianb@wolfram.com>",
	Loading -> Automatic,
	Extensions -> {
		{"Resource", Root -> "Resources", Resources -> {}},
		{"Kernel", Root -> "Kernel", Context -> {"NeuralNetworks`"}, HiddenImport -> True, Symbols -> {
			"System`NetInitialize",
			"System`NetPort",
			"System`NetPortGradient",
			"System`NetSharedArray",
			"System`NetInsertSharedArrays",
			"System`NetTrain", 
			"System`NetMeasurements", 
			"System`NetEvaluationMode",
			"System`NetGraph",
			"System`NetChain",
			"System`NetExtract",
			"System`NetReplacePart",
			"System`NetEncoder",
			"System`NetDecoder",
			"System`NetMapOperator",
			"System`NetMapThreadOperator",
			"System`NetFoldOperator",
			"System`NetNestOperator",
			"System`NetPairEmbeddingOperator",
			"System`NetBidirectionalOperator",
			"System`ConstantTimesLayer",
			"System`ConstantPlusLayer",
			"System`ConstantArrayLayer",
			"System`ElementwiseLayer",
			"System`ThreadingLayer",
			"System`BatchNormalizationLayer",
			"System`InstanceNormalizationLayer",
			"System`NormalizationLayer",
			"System`ConvolutionLayer",
			"System`CrossEntropyLossLayer",
			"System`ResizeLayer",
			"System`AggregationLayer",
			"System`DeconvolutionLayer",
			"System`SpatialTransformationLayer",
			"System`ImageAugmentationLayer",
			"System`DropoutLayer",
			"System`EmbeddingLayer",
			"System`UnitVectorLayer",
			"System`FlattenLayer",
			"System`ReplicateLayer",
			"System`DotPlusLayer",
			"System`LinearLayer",
			"System`DotLayer",
			"System`CatenateLayer",
			"System`MeanSquaredLossLayer",
			"System`MeanAbsoluteLossLayer",
			"System`ContrastiveLossLayer",
			"System`TotalLayer",
			"System`PoolingLayer",
			"System`PaddingLayer",
			"System`ReshapeLayer",
			"System`TransposeLayer",
			"System`SoftmaxLayer",
			"System`SummationLayer",
			"System`PartLayer",
			"System`LocalResponseNormalizationLayer",
			"System`GatedRecurrentLayer",
			"System`BasicRecurrentLayer",
			"System`LearningRate",
			"System`LongShortTermMemoryLayer",
			"System`SequenceLastLayer",
			"System`SequenceReverseLayer",
			"System`SequenceMostLayer",
			"System`SequenceRestLayer",
			"System`SequenceAttentionLayer",
			"System`AttentionLayer",
			"System`OrderingLayer",
			"System`CTCLossLayer",
			"System`ExtractLayer",
			"System`NetModel",
			"System`NetInformation",
			"System`NetFlatten",
			"System`NetReplace",
			"System`NetRename",
			"System`NetTake",
			"System`NetDrop",
			"System`NetDelete",
			"System`NetInsert",
			"System`NetAppend",
			"System`NetPrepend",
			"System`NetJoin",
			"System`AppendLayer",
			"System`PrependLayer",
			"System`NetStateObject",
			"System`NetTrainResultsObject",
			"System`LossFunction",
			"System`TrainingProgressMeasurements",
			"System`TrainingStoppingCriterion", (*TODO: move us to kernel*)
			"NeuralNetworks`WLNetExport",
			"NeuralNetworks`WLNetImport",
			"NeuralNetworks`MXNetImport",
			"NeuralNetworks`MXNetExport",
			"NeuralNetworks`TestTargetDevice",
			"NeuralNetworks`NetModelInternal",
			"NeuralNetworks`ValidNetQ"
		}}
	}
]

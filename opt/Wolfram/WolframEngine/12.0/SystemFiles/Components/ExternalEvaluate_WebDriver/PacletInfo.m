(* Paclet Info File *)

Paclet[
	Name -> "ExternalEvaluate_WebDriver",
	Version -> "12.0.1",
	MathematicaVersion -> "11.3+",
	Extensions -> {
		{
			"Resource",
			Root -> "ExternalEvaluate",
			Resources -> {
		    	{"System","WebDriver.wl"}
	    	}
		},
		{
			"Resource",
			Root -> "Resources",
			Resources -> {}
		}	
	}
]

(* Paclet Info File *)

(* created 2018/04/17*)

Paclet[
    Name -> "ServiceConnection_Facebook",
    Version -> "12.0.80",
    MathematicaVersion -> "11+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Facebook`", "FacebookLoad`", "FacebookFunctions`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Facebook", Language -> All}
        }
]



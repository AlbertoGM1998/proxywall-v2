(* Paclet Info File *)

Paclet[
	Name -> "SystemInstall_Python",
	Version -> "1.0.0",
	MathematicaVersion -> "11.3+",
	Extensions -> {
		{
			"Resource",
			Root -> "SystemInstall",
			Resources -> {
		    	{"System","Python.wl"}
	    	}
		}
	}
]

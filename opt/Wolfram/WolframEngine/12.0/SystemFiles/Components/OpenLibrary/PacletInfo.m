(* Paclet Info File *)

(* created 2015/11/03*)

Paclet[
    Name -> "ServiceConnection_OpenLibrary",
    Version -> "12.0.80",
    MathematicaVersion -> "10.2+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"OpenLibrary`", "OpenLibraryLoad`", "OpenLibraryFunctions`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/OpenLibrary", Language -> All}
        }
]



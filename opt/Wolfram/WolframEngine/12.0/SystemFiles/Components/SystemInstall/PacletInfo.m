(* Paclet Info File *)

(* created 2017.04.21*)

Paclet[
	Name -> "SystemInstall",
	Version -> "0.9.0",
	MathematicaVersion -> "11.3+",
	Loading->Automatic,
	Extensions -> {
		{
			"Kernel",
			Root -> "Kernel",
			Context -> {"SystemInstallLoader`","SystemInstall`"},
			Symbols-> {
				"System`SystemInstall"
			}
		}
	}
]
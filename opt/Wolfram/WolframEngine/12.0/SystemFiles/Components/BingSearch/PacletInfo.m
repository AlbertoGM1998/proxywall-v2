(* Paclet Info File *)

(* created 2017/02/03*)

Paclet[
    Name -> "ServiceConnection_BingSearch",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"BingSearch`", "BingSearchLoad`", "BingSearchFunctions`"}
            }, 
            {"FrontEnd"}, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/BingSearch", Language -> All}
        }
]



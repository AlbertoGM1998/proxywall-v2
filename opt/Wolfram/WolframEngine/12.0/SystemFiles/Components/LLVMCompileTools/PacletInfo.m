(* Paclet Info File *)

(* created 2016/06/22*)

Paclet[
    Name -> "LLVMCompileTools",
    Version -> "1.0.0.1",
    WolframVersion -> "12+",
    Updating -> Automatic,
    Extensions->{
    	{"Kernel", Context->"LLVMCompileTools`"}
    }
]



(* Paclet Info File *)

(* created 2018/03/27*)

Paclet[
    Name -> "ExternalEvaluate",
    Version -> "20.1.1",
    MathematicaVersion -> "11.3+",
    Loading -> Automatic,
    Extensions -> {
        {"Kernel", 
            Symbols -> {
                "System`ExternalSessions", 
                "System`ExternalObject", 
                "System`ExternalFunction",
                "System`ExternalValue",
                "System`ExternalEvaluate", 
                "System`ExternalSessionObject", 
                "System`StartExternalSession", 
                "System`FindExternalEvaluators", 
                "System`RegisterExternalEvaluator", 
                "System`UnregisterExternalEvaluator", 
                "ExternalEvaluate`FE`ExternalCellEvaluate"
            },
            HiddenImport -> True,
            Root -> "Kernel", 
            Context -> {"ExternalEvaluate`"}
        }
    }
]



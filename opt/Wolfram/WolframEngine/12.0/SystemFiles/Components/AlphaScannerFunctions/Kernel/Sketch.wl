(* ::Package:: *)

(* ::Chapter::Closed:: *)
(*prolog*)


BeginPackage["AlphaScannerFunctions`", {"AlphaScannerFunctions`CommonFunctions`"}];


Sketch::usage= "Produces a basic two-dimensional sketch of a graphics primitive and other \"geometrical\" objects.";


Sketch3D::usage= "Produces a basic three-dimensional sketch of a graphics primitive and other \"geometrical\" objects.";


Begin["`Sketch`Private`"];


(* ::Chapter:: *)
(*main code*)


(* ::Section::Closed:: *)
(*style definitions*)


(* ::Subsection::Closed:: *)
(*2d*)


sketchDisplayRules = {
	_Annulus -> {Opacity[0.5]},
    _Arrow -> {Opacity[0.5], Arrowheads[Large]},
    _Ball -> {Opacity[0.5]},
    _BezierCurve -> {},
    _BSplineCurve -> {},
	_Circle -> {Opacity[0.75], Thick},
	_CircleThrough -> {Opacity[0.75], Thick},
    _Circumsphere -> {},
    _ConicHullRegion -> {Opacity[0.5]},
	_Disk -> {Opacity[0.5]},
	_DiskSegment -> {Opacity[0.5]},
	_Ellipsoid -> {Opacity[0.5]},
	_FilledCurve -> {Opacity[0.5]},
	_GraphicsComplex -> {},
	_GraphicsGroup -> {},
	_HalfLine -> {Opacity[0.75],Thick},
	_HalfPlane -> {Opacity[0.35]},
	_Hyperplane -> {Opacity[0.5]},
	_InfiniteLine -> {Opacity[0.75], Thick},
	_Inset -> {},
	_Insphere -> {},
	_JoinedCurve -> {Opacity[0.5]},
	_Line -> {Opacity[0.75], Thick},
	_Locator->{},
	_Parallelepiped -> {Opacity[0.5]},
	_Parallelogram -> {Opacity[0.5]},
	_Point -> {PointSize[Large]}, 
    _Polygon -> {Opacity[0.5]},
    _Rectangle -> {Opacity[0.5]},
    _RegularPolygon -> {Opacity[0.5]},
    _Simplex -> {Opacity[0.5]},
    _Sphere -> {} (* to handle output from circumsphere/insphere *), 
    _StadiumShape -> {Opacity[0.5]},
    _Text -> {},
    _Triangle -> {Opacity[0.5]}
};
sketchSupportedPatterns = Keys[sketchDisplayRules];


(* ::Subsection::Closed:: *)
(*3D*)


sketch3DDisplayRules = {
    (* 3d *)
    _Arrow -> {Opacity[0.75], Arrowheads[Large]},
    _Ball -> {Opacity[0.5]},
    _BezierCurve -> {},
    _BSplineCurve -> {},
    _CapsuleShape -> {Opacity[0.5]},
    _Circumsphere -> {},
    _Cone -> {Opacity[0.5]},
    _ConicHullRegion -> {Opacity[0.5]},
    _Cylinder -> {Opacity[0.5]},
    _Cube -> {Opacity[0.5]},
    _Cuboid -> {Opacity[0.5]},
    _Dodecahedron -> {Opacity[0.5]},
    _Ellipsoid -> {Opacity[0.5]},
    Entity["Polyhedron", _]-> ({Opacity[0.5], Cases[#["Graphics3D"], _GraphicsComplex, {0,Infinity}]}&),
    EntityClass["Polyhedron", _]-> ({Opacity[0.5], Cases[Through[EntityList[#]["Graphics3D"]], _GraphicsComplex, {0,Infinity}]}&),
    _GraphicsComplex -> {},
    _GraphicsGroup -> {},
    _HalfLine -> {Opacity[0.75],Thick},
    _HalfPlane -> {Opacity[0.35]},
    _Hexahedron -> {Opacity[0.35]},
    _Hyperplane -> {Opacity[0.35]},
    _Icosahedron -> {Opacity[0.5], Thick},
    _InfiniteLine -> {Opacity[0.75], Thick},
    _Inset -> {},
    _Insphere -> {},
    _Line -> {Opacity[0.75], Thick},
    _Octahedron -> {Opacity[0.5], Thick},
    _Parallelepiped -> {Opacity[0.5]},
    _Point -> {PointSize[Large]},
    _Polygon -> {Opacity[0.5]},
    _Polyhedron -> {Opacity[0.5](*, GrayLevel[.3, .2]*)},
    _Prism -> {Opacity[0.5]},
    _Pyramid -> {Opacity[0.5]},
    _RegularPolygon -> {Opacity[0.5]},
    _Sphere -> {Opacity[0.5]},
    _SphericalShell -> {Opacity[0.5]},
    _Simplex -> {Opacity[0.5]},
    _Tetrahedron -> {Opacity[0.5]},
    _Text -> {},
    _Triangle -> {Opacity[0.75], Thick},
    _Tube -> {Opacity[0.5]}
};
sketch3DSupportedPatterns = Keys[sketch3DDisplayRules];


(* ::Section::Closed:: *)
(*Sketch*)


(* options and messages *)
ClearAll[Sketch]
Sketch::incmp= "Input `1` is not supported by Sketch.";
Options[Sketch]= Normal@Merge[ (* Sketch takes all the options of Graphics, but with altered defaults *)
  {Options[Graphics],
    {
      FormatType -> Automatic,
      Frame -> True,
      GridLines->Automatic
    }
  }, Last
];


(* 2D Case *)
Sketch[input_, optsIn: OptionsPattern[]]:= Module[
    {
       inputHead= Head[input], res, func, funcList,
       opts = Normal@Merge[{Options[Sketch],optsIn}, Last]
    },
    Which[
      (* case 1: list input *)
      Head[input] === List
      ,
	  If[ (* if all heads at level 1 are supported *)
         And@@((Or@@Through[(MatchQ /@ sketchSupportedPatterns)[#]])&/@input)
         , (* then *)
         funcList= If[Head[#]===List, Function[in, Style[in, #]], #]& /@ Replace[input, sketchDisplayRules, {1}];
         res= funcList[[1]][input[[1]]];
         res= MapThread[Apply[#1,{#2}]&, {funcList, input}];
         Graphics[res, Sequence@@opts]
         , (* else *)
         ASFMessage[Sketch::incmp, SelectFirst[input, !(Or@@Through[(MatchQ /@ sketchSupportedPatterns)[#]])&]]; $Failed     
      ],
      (* case 2: non-List, supported head *)
      Or@@Through[(MatchQ /@ sketchSupportedPatterns)[input]]
      ,
      func= If[Head[#]===List, Function[in, Style[in, #]], #]& @ Replace[input, sketchDisplayRules];
      res= func[input];
      Graphics[{res}, Sequence@@opts]
      ,
      (* default: else throw a message *)
      True
      , 
      ASFMessage[Sketch::incmp, input]; $Failed
    ]
]


(* ::Section::Closed:: *)
(*Sketch3D*)


(* options and messages *)
ClearAll[Sketch3D]
Sketch3D::incmp= "Input `1` is not supported by Sketch3D.";
Options[Sketch3D]= Normal@Merge[ (* Sketch3D takes all the options of Graphics3D, but with altered defaults *)
  {Options[Graphics3D],
    {
      Axes -> True,
      FaceGrids -> All,
      FormatType -> Automatic,
      RotationAction -> "Clip"
    }
  }, Last
];


(* generic 3D Case *)
Sketch3D[input_, optsIn: OptionsPattern[]]:= Module[
    {
       res, styleList, func, funcList,
       opts = Normal@Merge[{Options[Sketch3D], optsIn}, Last]
    },
     Which[
      (* case 1: list input *)
      Head[input] === List
      ,
	  If[ (* if all heads at level 1 are supported *)
         And@@((Or@@Through[(MatchQ /@ sketch3DSupportedPatterns)[#]])&/@input)
         , (* then *)
         funcList= If[Head[#]===List, Function[in, Style[in, #]], #]& /@ Replace[input, sketch3DDisplayRules, {1}];
         res= funcList[[1]][input[[1]]];
         res= MapThread[Apply[#1,{#2}]&, {funcList, input}];
         Graphics3D[res, Sequence@@opts]
         , (* else *)
         ASFMessage[Sketch3D::incmp, SelectFirst[input, !(Or@@Through[(MatchQ /@ sketch3DSupportedPatterns)[#]])&]]; $Failed     
      ],
      (* case 2: non-List, supported head *)
      Or@@Through[(MatchQ /@ sketch3DSupportedPatterns)[input]]
      ,
      (*res = Style[input, Head[input]/.styleReplacements];*)
      func= If[Head[#]===List, Function[in, Style[in, #]], #]& @ Replace[input, sketch3DDisplayRules];
      res= func[input];
      Graphics3D[{res}, Sequence@@opts]
      ,
      (* default: else throw a message *)
      True
      , 
      ASFMessage[Sketch3D::incmp, input]; $Failed
    ]
]


Sketch3D[ec: EntityClass["Polyhedron", _], optsIn: OptionsPattern[]]:= Module[
	{
		ents= EntityList[ec]
	},
	Sketch3D[#, optsIn]&/@ents
]


(* ::Chapter:: *)
(*tests*)


(* ::Section::Closed:: *)
(*basic functionality (2d)*)


(* ::Subsection:: *)
(*Annulus*)


(* ::Input:: *)
(*Sketch[Annulus[]]*)


(* ::Subsection:: *)
(*Arrow*)


(* ::Input:: *)
(*Sketch[Arrow[{{0,0},{3,-1}}]]*)


(* ::Subsection:: *)
(*Ball*)


(* ::Input:: *)
(*Sketch[Ball[2]]*)


(* ::Subsection:: *)
(*BezierCurve*)


(* ::Input:: *)
(*pts = {{0, 0}, {1, 1}, {2, -1}, {3, 0}};*)
(*Sketch[BezierCurve[pts]]*)


(* ::Subsection:: *)
(*BSplineCurve*)


(* ::Input:: *)
(*pts = {{0, 0}, {1, 1}, {2, -1}, {3, 0}};*)
(*Sketch[BSplineCurve[pts]]*)


(* ::Subsection:: *)
(*Circle*)


(* ::Input:: *)
(*Sketch[Circle[{0,0.2},{2,1}]]*)


(* ::Subsection:: *)
(*CircleThrough*)


(* ::Input:: *)
(*pts={{0, 0}, {1, 0}, {0, 1}};*)
(*Sketch[CircleThrough[pts]]*)


(* ::Subsection:: *)
(*Circumsphere*)


(* ::Input:: *)
(*pts={{0, 0}, {1, 0}, {0, 1}};*)
(*Sketch[{Triangle@pts,Circumsphere@pts}]*)


(* ::Subsection:: *)
(*ConicHullRegion*)


(* ::Input:: *)
(*Sketch[ConicHullRegion[{{0, 0}}, {{1, -1}, {1, 1}}]]*)


(* ::Subsection:: *)
(*Disk*)


(* ::Input:: *)
(*Sketch[Disk[{0,0.2},{2,1}]]*)


(* ::Input:: *)
(*Sketch[Disk[{0,0.2},{2,1},{Pi/4,Pi/3}]]*)


(* ::Subsection:: *)
(*DiskSegment*)


(* ::Input:: *)
(*Sketch[Table[DiskSegment[{0, 2},{2,3},{0,theta}],{theta, Pi/8, Pi, Pi/8}]]*)


(* ::Subsection:: *)
(*Ellipsoid*)


(* ::Input:: *)
(*Sketch[Ellipsoid[{0,0},{3,2}]]*)


(* ::Subsection:: *)
(*FilledCurve*)


(* ::Input:: *)
(*a={{-1,0},{0,1},{1,0}};b={{0,-(2/3)},{-1,0}};*)
(*Sketch[FilledCurve[{BezierCurve[a],Line[b]}]]*)


(* ::Subsection:: *)
(*GraphicsComplex*)


(* ::Input:: *)
(*v={{1,0},{0,1},{-1,0},{0,-1}};*)
(*Sketch[GraphicsComplex[v,Polygon[{1,2,3,4}]]]*)


(* ::Subsection:: *)
(*GraphicsGroup*)


(* ::Input:: *)
(*Sketch[GraphicsGroup[{Disk[{1, 0}],Pink, Disk[{0, 0}]}]]*)


(* ::Subsection:: *)
(*HalfLine*)


(* ::Input:: *)
(*Sketch[HalfLine[{0,1},{0.4,0.5}]]*)


(* ::Subsection:: *)
(*HalfPlane*)


(* ::Input:: *)
(*Sketch[HalfPlane[{0, 0}, {1, -1}, {1, 1}]]*)


(* ::Subsection:: *)
(*Hyperplane*)


(* ::Input:: *)
(*Sketch[Hyperplane[{2, 1}]]*)


(* ::Subsection:: *)
(*InfiniteLine*)


(* ::Input:: *)
(*Sketch[InfiniteLine[{{0,0},{3,1}}]]*)


(* ::Subsection:: *)
(*Inset*)


(* ::Input:: *)
(*Sketch[{Circle[],Inset[Plot[Tan[x],{x,-3,3}]]}]*)


(* ::Subsection:: *)
(*Insphere*)


(* ::Input:: *)
(*pts={{0, 0}, {1, 0}, {0, 1}};*)
(*Sketch[{Triangle@pts, Insphere@pts}]*)


(* ::Subsection:: *)
(*JoinedCurve*)


(* ::Input:: *)
(*a={{-1,0},{0,1},{1,0}};b={{0,-(2/3)},{-1,0}};*)
(*Sketch[JoinedCurve[{BezierCurve[a],Line[b]}]]*)


(* ::Subsection:: *)
(*Line*)


(* ::Input:: *)
(*Sketch[Line[{{0,0},{3,1}}]]*)


(* ::Subsection:: *)
(*Locator*)


(* ::Input:: *)
(*Sketch[{Circle[],Locator[{0, 0}]}]*)


(* ::Subsection:: *)
(*Parallelogram*)


(* ::Input:: *)
(*Sketch[Parallelogram[{0, 0}, {{1, 0}, {1, 1}}]]*)


(* ::Subsection:: *)
(*Parallelepiped*)


(* ::Input:: *)
(*Sketch[Parallelepiped[{0,0},{{2,1},{1,2}}]]*)


(* ::Subsection:: *)
(*Polygon*)


(* ::Input:: *)
(*poly=Polygon@@List[AnglePath[{1,0},Prepend[Table[2Pi/5, 4],0]]];*)
(*Sketch[poly]*)


(* ::Subsection:: *)
(*Rectangle*)


(* ::Input:: *)
(*Sketch[Rectangle[{0,0.2},{2,1}]]*)


(* ::Subsection:: *)
(*RegularPolygon*)


(* ::Input:: *)
(*Sketch[RegularPolygon[5]]*)


(* ::Subsection:: *)
(*Simplex*)


(* ::Input:: *)
(*Sketch[Simplex[{{0, 0}, {1, 1}, {2, 0}}]]*)


(* ::Subsection:: *)
(*StadiumShape*)


(* ::Input:: *)
(*Sketch[StadiumShape[]]*)


(* ::Subsection:: *)
(*Text*)


(* ::Input:: *)
(*Sketch[{Circle[], Text[x^2 + y^2< 1, {0, 0}]}]*)


(* ::Subsection:: *)
(*Triangle (AASTriangle, 	ASATriangle , SSATriangle, SSSTriangle evaluate thru Triangle)*)


(* ::Input:: *)
(*Sketch[Triangle[{{0,0},{0,1},{1,0}}]]*)


(* ::Input:: *)
(*Sketch[AASTriangle[Pi/6, Pi/3, 1]]*)


(* ::Input:: *)
(*Sketch[ASATriangle[Pi/6, 1, Pi/3]]*)


(* ::Input:: *)
(*Sketch[SASTriangle[1, Pi/2, 2]]*)


(* ::Input:: *)
(*Sketch[SSSTriangle[3, 4, 5]]*)


(* ::Section::Closed:: *)
(*list input (2d)*)


(* ::Input:: *)
(*Sketch[{Triangle[{{0,0},{0,1},{1,0}}], Line[{{0.1,0.1},{1.5, 0.6}}], Point[{0.5,0.7}]}]*)


(* ::Section::Closed:: *)
(*basic functionality (3d)*)


(* ::Subsection::Closed:: *)
(*Arrow*)


(* ::Input:: *)
(*Sketch3D[Arrow[{{0,0,0},{3,-1,1}}]]*)


(* ::Subsection::Closed:: *)
(*Ball*)


(* ::Input:: *)
(*Sketch3D[Ball[]]*)


(* ::Subsection::Closed:: *)
(*CapsuleShape*)


(* ::Input:: *)
(*Sketch3D[CapsuleShape[]]*)


(* ::Subsection::Closed:: *)
(*Circumsphere*)


(* ::Input:: *)
(*input=Cube[];*)
(*Sketch3D[{input, Circumsphere[input]}]*)


(* ::Subsection::Closed:: *)
(*Cone*)


(* ::Input:: *)
(*Sketch3D[Cone[]]*)


(* ::Subsection::Closed:: *)
(*ConicHullRegion*)


(* ::Input:: *)
(*Sketch3D[ConicHullRegion[{{0, 0, 0}}, {{-10, 2, 3}, {1, 1, 0}}]]*)


(* ::Subsection::Closed:: *)
(*Cube*)


(* ::Input:: *)
(*Sketch3D[Cube[]]*)


(* ::Subsection::Closed:: *)
(*Cuboid*)


(* ::Input:: *)
(*Sketch3D[Cuboid[]]*)


(* ::Subsection::Closed:: *)
(*Cylinder*)


(* ::Input:: *)
(*Sketch3D[Cylinder[]]*)


(* ::Subsection::Closed:: *)
(*Dodecahedron*)


(* ::Input:: *)
(*Sketch3D[Dodecahedron[]]*)


(* ::Subsection::Closed:: *)
(*Ellipsoid*)


(* ::Input:: *)
(*Sketch3D[Ellipsoid[{0, 0, 0}, {4, 3, 2}]]*)


(* ::Subsection::Closed:: *)
(*GraphicsComplex*)


(* ::Input:: *)
(*v={{0,0,0},{2,0,0},{2,2,0},{0,2,0},{1,1,2}};*)
(*i={{1,2,5},{2,3,5},{3,4,5},{4,1,5}};*)
(*Sketch3D[GraphicsComplex[v,Polygon[i]]]*)


(* ::Subsection::Closed:: *)
(*GraphicsGroup*)


(* ::Input:: *)
(*Sketch3D[GraphicsGroup[{Ellipsoid[{0, 0, 0}, {1,0.3,1}],Octahedron[]}]]*)


(* ::Subsection::Closed:: *)
(*HalfLine*)


(* ::Input:: *)
(*Sketch3D[HalfLine[{0,0,0},{1,2,3}]]*)


(* ::Subsection::Closed:: *)
(*HalfPlane*)


(* ::Input:: *)
(*Sketch3D[HalfPlane[{{0,0,0},{1,0,0}},{0,1,1}]]*)


(* ::Subsection::Closed:: *)
(*Hexahedron*)


(* ::Input:: *)
(*Sketch3D[Hexahedron[{{0,0,0},{1,0,0},{2,1,0},{1,1,0},{0,0,1},{1,0,1},{2,1,1},{1,1,1}}]]*)


(* ::Subsection::Closed:: *)
(*Hyperplane*)


(* ::Input:: *)
(*Sketch3D[Hyperplane[{-1, -1, 1}, {1, 2, 3}]]*)


(* ::Subsection::Closed:: *)
(*Icosahedron*)


(* ::Input:: *)
(*Sketch3D[Icosahedron[]]*)


(* ::Subsection::Closed:: *)
(*InfiniteLine*)


(* ::Input:: *)
(*Sketch3D[InfiniteLine[{0,0,0},{1,2,3}]]*)


(* ::Subsection::Closed:: *)
(*Inset*)


(* ::Input:: *)
(*Sketch3D[{Sphere[{0, -1, 0}, 0.5], *)
(*  Cuboid[{-0.25, 0.5, -0.25}, {0.25, 1, 0.25}], *)
(*  Inset[Graphics[{Disk[{0, 0}, .25]}, ImageSize -> 50], {0, 0, 0}]}, *)
(* PlotRange -> 2]*)


(* ::Subsection::Closed:: *)
(*Insphere*)


(* ::Input:: *)
(*input=Cube[];*)
(*Sketch3D[{input, Insphere[input]}]*)


(* ::Subsection::Closed:: *)
(*Octahedron*)


(* ::Input:: *)
(*Sketch3D[Octahedron[]]*)


(* ::Subsection::Closed:: *)
(*Parallelepiped*)


(* ::Input:: *)
(*Sketch3D[Parallelepiped[{0,0,0},{{2,1,0},{1,2,0},{1,1,1}}]]*)


(* ::Subsection::Closed:: *)
(*Polygon*)


(* ::Input:: *)
(*poly=Polygon[Append[#,0]&/@AnglePath[{1,0},Prepend[Table[2Pi/5, 4],0]]];*)
(*Sketch3D[poly]*)


(* ::Subsection::Closed:: *)
(*Polyhedron*)


(* ::Input:: *)
(*poly= Polyhedron[{{0., 0., 0.6}, {-0.3, -0.5, -0.2}, {-0.3, *)
(*    0.5, -0.2}, {0.6, 0., -0.2}}, {{2, 3, 4}, {3, 2, 1}, {4, 1, *)
(*    2}, {1, 4, 3}}];*)
(*Sketch3D[poly]*)


(* ::Subsection::Closed:: *)
(*Polyhedron Entity*)


(* ::Input:: *)
(*Sketch3D[Entity["Polyhedron","SnubCube"]]*)


(* ::Input:: *)
(*Sketch3D[{Entity["Polyhedron","SnubCube"]}]*)


(* ::Subsection::Closed:: *)
(*Polyhedron named EntityClass	*)


(* ::Input:: *)
(*Sketch3D[EntityClass["Polyhedron","Deltahedron"]]*)


(* ::Subsection::Closed:: *)
(*Polyhedron implicit EntityClass	*)


(* ::Input:: *)
(*Sketch3D[EntityClass["Polyhedron","VertexCount"->11]]*)


(* ::Input:: *)
(*Sketch3D[EntityClass["Polyhedron","EdgeCount"->26]]*)


(* ::Subsection::Closed:: *)
(*Sphere*)


(* ::Input:: *)
(*Sketch3D[Sphere[{0,0,0},1]]*)


(* ::Subsection::Closed:: *)
(*SphericalShell*)


(* ::Input:: *)
(*Sketch3D[SphericalShell[]]*)


(* ::Subsection::Closed:: *)
(*Text*)


(* ::Input:: *)
(*Sketch3D[{Sphere[], Text[x^2 + y^2 + z^2 < 1, {0, 0, 0}]}]*)


(* ::Subsection::Closed:: *)
(*Tetrahedron*)


(* ::Input:: *)
(*Sketch3D[Tetrahedron[]]*)


(* ::Subsection::Closed:: *)
(*Tube*)


(* ::Input:: *)
(*Sketch3D[Tube[{{0, 0, 0}, {1, 1, 1}}, .1]]*)


(* ::Subsection::Closed:: *)
(*Prism*)


(* ::Input:: *)
(*Sketch3D[ Prism[{{1, 0, 1}, {0, 0, 0}, {2, 0, 0}, {1, 2, 1}, {0, 2, 0}, {2, 2, 0}}]]*)


(* ::Subsection::Closed:: *)
(*Pyramid*)


(* ::Input:: *)
(*Sketch3D[Pyramid[{{0, 0, 0}, {2, 0, 0}, {2, 2, 0}, {0, 2, 0}, {1, 1, 2}}]]*)


(* ::Subsection::Closed:: *)
(*Simplex*)


(* ::Input:: *)
(*Sketch3D[Simplex[{{0, 0, 1}, {1, 0, 0}, {1, 0, 1}, {1, 1, 1}}]]*)


(* ::Section::Closed:: *)
(*list input (3d)*)


(* ::Input:: *)
(*Sketch3D[{Cuboid[], Cone[]}]*)


(* ::Input:: *)
(*Sketch3D[EntityList[EntityClass["Polyhedron","VertexCount"->11]]]*)


(* ::Input:: *)
(*Sketch3D[{*)
(*	Triangle[{{0,0, 0},{0,1, 0},{1,0,1}}], *)
(*	Line[{{0.5, 0, -0.5}, {0.5, 0.5, 1}}],*)
(*	Point[{0.5, 0.5, 0.5}]*)
(*}]*)


(* ::Section::Closed:: *)
(*Options*)


(* ::Input:: *)
(*Sketch[Line[{{0,0},{3,1}}],Frame->False]*)


(* ::Section::Closed:: *)
(*Messages*)


(* ::Input:: *)
(*Block[{AlphaScannerFunctions`$ResourceSystemMode=False},Sketch[{Annulus[],blah}]]*)


(* ::Input:: *)
(*Block[{AlphaScannerFunctions`$ResourceSystemMode=False},Sketch3D[{Circle[],blah}]]*)


(* ::Chapter:: *)
(*testing helpers: extractSketchParts, extractSketch3DParts*)


(* ::Section::Closed:: *)
(*extractSketchParts*)


ClearAll[extractSketchParts]
extractSketchParts[input_]:= {
	Extract[input, {1}] /. Style[first_, rest___]:> first
	,
	List@@Drop[input, {1}]
}


(* ::Input:: *)
(*extractSketchParts[Sketch[Annulus[]]]*)


(* ::Section::Closed:: *)
(*extractSketch3DParts*)


ClearAll[extractSketch3DParts]
Attributes[extractSketch3DParts] = {Listable};
extractSketch3DParts[input_]:= {
	Extract[input, {1}] /. Style[first_, rest___]:> first
	,
	List@@Drop[input, {1}]
}


(* ::Input:: *)
(*extractSketch3DParts[Arrow[{{0,0,0},{3,-1,1}}]]*)


(* ::Chapter::Closed:: *)
(*epilog*)


End[];


EndPackage[]

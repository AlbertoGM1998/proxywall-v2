(* Paclet Info File *)

(* created 2017/03/01*)

Paclet[
    Name -> "ServiceConnection_GooglePlus",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"GooglePlus`", "GooglePlusLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/GooglePlus", Language -> All}
        }
]



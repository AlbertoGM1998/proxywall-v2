Paclet[
	Name -> "Streaming",
	Version -> "1.4",
	MathematicaVersion -> "11.0+",
	Description -> "Streaming framework",
	Creator -> "Leonid Shifrin <leonids@wolfram.com>",
	Loading -> Automatic,
	Extensions -> {
		{
			"Kernel",
			Context -> {"StreamingLoader`", {"Streaming`", "StreamingMain.m"}},
			HiddenImport -> None,
			Symbols -> {
				"Streaming`StreamingDataset",
				"Streaming`StreamingMap"
			}
		}
	}
]

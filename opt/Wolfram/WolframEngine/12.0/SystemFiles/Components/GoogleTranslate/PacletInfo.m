(* Paclet Info File *)

(* created 2017/02/03*)

Paclet[
    Name -> "ServiceConnection_GoogleTranslate",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"GoogleTranslate`", "GoogleTranslateLoad`", "GoogleTranslateFunctions`"}
            }, 
            {"FrontEnd"}, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/GoogleTranslate", Language -> All}
        }
]



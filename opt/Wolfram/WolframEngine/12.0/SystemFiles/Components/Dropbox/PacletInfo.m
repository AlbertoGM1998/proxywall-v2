(* Paclet Info File *)

(* created 2016/12/12*)

Paclet[
    Name -> "ServiceConnection_Dropbox",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Dropbox`", "DropboxLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Dropbox", Language -> All}
        }
]



(* Paclet Info File *)

(* created 2016/04/18*)

Paclet[
	Name -> "LowLevelLinearAlgebra",
	Version -> "1.0.0",
	MathematicaVersion -> "11+",
	Extensions -> {
			{"Kernel", Root->"Kernel", Context -> "LinearAlgebra`"},

			(**)
			(* Package Documentation *)
			(**)
			{
				"Documentation",
				Language -> All,
				MainPage -> "Guides/BLASGuide"
			}
		}
]



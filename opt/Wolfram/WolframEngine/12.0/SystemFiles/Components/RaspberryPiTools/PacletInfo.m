(* ::Package:: *)

(* Paclet Info File *)

(* created 2015/06/05*)

Paclet[
    Name -> "RaspberryPiTools",
    Version -> "12.0.0",
    MathematicaVersion -> "11.2+",
    Creator -> "Brett Haines <bhaines@wolfram.com>",
    Loading -> Automatic,
    Extensions -> {
		{"LibraryLink",
			SystemID->"Linux-ARM"
		},
        {"Kernel", 
			Root->"Kernel", 
			Context->{"RaspberryPiTools`","RaspberryPiToolsImpl`"},
			Symbols->{
				"System`CurrentImage",
				"System`ImageCapture"
			}
		},
        {"Documentation",
			MainPage->"ReferencePages/Devices/SenseHAT", 
			Language->"English"
		}
    }
]

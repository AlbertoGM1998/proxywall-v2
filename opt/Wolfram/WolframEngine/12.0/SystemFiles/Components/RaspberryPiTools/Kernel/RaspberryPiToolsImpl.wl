(* ::Package:: *)

(* RaspberryPiTools Paclet *)
(* Brett Haines, July 2015 *)
(* This paclet adds functionality for Raspberry Pi addons.  It currently supports the Pi Camera and 
the Sense HAT addons. *)

BeginPackage["RaspberryPiTools`"]

Begin["`Private`"]

(* Change the definition of CurrentImage to use the device driver *)
Unprotect[System`CurrentImage];
Attributes[System`CurrentImage] = {};
ClearAll[System`CurrentImage];
System`CurrentImage[ args___ ] := currentImagePi[ args ];
Protect[System`CurrentImage];

(* Re-define ImageCapture on Pi *)
Unprotect[System`ImageCapture];
Attributes[System`ImageCapture] = {};
ClearAll[System`ImageCapture];
System`ImageCapture[ args___ ] := currentImagePi[ ];
Protect[System`ImageCapture];

(* Needs Statements *)
Needs["PacletManager`"]
Needs["MRAALink`"];

(* Contstants *)
defaultRaspiCamWidth = 2592;
defaultRaspiCamHeight = 1944;
minRaspiCamWidth = 300;
defaultRaspiCamRatio = 0.75;

(* Messages *)
DeviceOpen::cameraDisabled="The camera is disabled.  Please enable it via raspi-config.";
DeviceOpen::cameraDisconnected="The camera is not detected.  Please ensure the camera module is connected properly.";
DeviceOpen::cameraUnknown="An unknown error has occurred.";
DeviceRead::badCameraParams="Too many parameters were given.";
DeviceRead::invalidCameraSizes="Height and width must both be positive integers, where width is less than 2593 and height is less than 1945.";
DeviceOpen::noDevice="No SenseHAT device was detected.";
DeviceRead::unknownFunction="Unknown sensor function given, please enter one of the following:\ntemperature, humidity, pressure, acceleration, rotation, magnetic field, orientation";
DeviceRead::orientationErr="Error reading orientation.  Please ensure your Sense HAT is connected properly.";
DeviceWrite::writePixelErr="Error writing to LED array."
OpenRead::configFailure="Could not open or read from configuration file."

(* Persistent system variables *)
$isPiCameraConnected = 0;
Protect[$isPiCameraConnected];

(*this function uses apt-cache to check if an aptitude package exists or not*)
(*it is guaranteed to always return true or false for any given string*)
(*note that apt-cache may print off some stuff to stderr if the package doesn't exist at all*)
aptitudePackageInstalledQ[package_String] := StringTrim[
	First[
		StringSplit[
			StringTrim[
				Join[
					StringSplit[
						(*we use apt-cache policy to see if a package is installed or not*)
						Import["!apt-cache policy " <> package, "Text"],
						"\n"
					],
					(*we join this with the following strings because if the package doesn't exist at all, the empty string is returned*)
					{"","Installed:(none)"}
				][[2]]
			],
			"Installed:"
		]
	]
] != "(none)";


(*configFileParse parses out from a file the lines that don't start with a comment, and also cleans out comments from lines with other valid data in them*)
(*it returns a list of the lines, all cleaned of trailing and beginning whitespace characters*)
configFileParse[fName_,commentCharacter_String:"#"]:=Module[
	{
		line,
		modules = {},
		fileName = AbsoluteFileName[fName],
		file
	},
	(
		(*open the file with BinaryFormat set to True so we can use ReadLine properly*)
		file=OpenRead[fileName,BinaryFormat->True];
		If[file===$Failed, Message[OpenRead::configFailure]; Return[$Failed]];
		(*initially, read a line before entering the while loop*)
		line = ReadLine[file];
		(*now read the entirety of the file, one line at a time, and if the line is a comment or empty,*)
		(*ignore it, else parse it out*)
		While[line =!= EndOfFile,
			If[line =!= "" && StringTake[StringTrim[line], 1] =!= commentCharacter,
				(*THEN*)
				(*the line isn't a comment*)
				If[MemberQ[Characters[line], commentCharacter],
					(*THEN*)
					(*there is a comment somewhere im this line, so we have to trim it*)
					AppendTo[modules,StringTrim[StringDrop[#,First@StringPosition[#,commentCharacter~~___]]&[line]]],
					(*ELSE*)
					(*there isn't a comment, so we can just trim out the whitespace it*)
					AppendTo[modules, StringTrim[line]]
				]
				(*ELSE*)
				(*this line is either empty or a comment, so ignore it*)
			];
			line = ReadLine[file];
		];
		Return[modules];
	)
]/;StringLength[commentCharacter]===1
(*only match the pattern if commentCharacter is actually a single character*)


(*============================================================================*)
(* RaspiCam Code *)
(*============================================================================*)

(* Create DeviceRead error messages *)
DeviceRead::raspiCamError="`1`";
raspiCamMessages=<|
	0->"No error",
	1->"Error creating camera component",
	2->"Error setting image format",
	3->"Error creating preview",
	4->"Error setting preview port parameters",
	5->"Error creating camera-preview connection",
	6->"Error enabling camera-preview",
	7->"Error creating encoder",
	8->"Error setting output format of encoder",
	9->"Error enabling encoder",
	10->"Error creating camera-encoder connection",
	11->"Error enabling camera-encoder connection",
	12->"Error enabling encoder output port",
	13->"Error getting buffer from pool queue",
	14->"Error sending buffer to encoder output",
	15->General::nomem
|>;

(* Load the dynamic library RaspiSillLink *)
LibraryLoad["libRaspberryPiTools"];
lib1="libRaspberryPiTools";

(* Link to function in library that gets an image from the camera *)
getImageBytes = LibraryFunctionLoad[lib1, "GetByteData", {Integer, Integer}, {_Integer, 1, "Automatic"}];

(* Link to functions that return default (max) image width and height, then read them in *)
getMaxWidth = LibraryFunctionLoad[lib1, "GetMaxWidth", {}, Integer];
getMaxHeight = LibraryFunctionLoad[lib1, "GetMaxHeight", {}, Integer];
maxRaspiCamWidth = getMaxWidth[];
maxRaspiCamHeight = getMaxHeight[];

(* If the max values cannot be read, default to the pre-defined constants *)
If[maxRaspiCamWidth==Null, maxRaspiCamWidth=defaultRaspiCamWidth];
If[maxRaspiCamHeight==Null, maxRaspiCamHeight=defaultRaspiCamHeight];

(* Create wrapper function for library function *)
raspiCam[{ihandle_,dhandle_},args___]:=Module[
	{
		argsList=Flatten[{args}], 
		mult, 
		width, 
		height, 
		bytes, 
		string, 
		img
	},

	Switch[ Length[argsList],
		2, {width,height} = argsList,
		1, {width,height} = {argsList[[1]],argsList[[1]]*defaultRaspiCamRatio},
		0, {width,height} = {maxRaspiCamWidth,maxRaspiCamHeight},
		_, Message[DeviceRead::badCameraParams]; Return[$Failed];
	];
	
	(* Ensure height and width aren't negative, zero, or too large *)
	If[ width <= 0 || width > maxRaspiCamWidth || height <= 0 || height > maxRaspiCamHeight,
		(*THEN*)
		Message[DeviceRead::invalidCameraSizes]; $Failed
	];

	(* Deal with too-small images bug by scaling up, taking the image, then scaling back down *)
	mult = 1;
	If[ width < minRaspiCamWidth || height < minRaspiCamWidth, 
		(*THEN*)
		mult = minRaspiCamWidth / Min[ width, height ];
	];
	
	(*Get the returned bytes for the image*)
	bytes = getImageBytes[ Floor[mult*width], Floor[mult*height] ];
	If[Length[bytes]===1,
		(*THEN*)
		(*the image acuisition failed, and the tensor is just the error code*)
		(
			Message[DeviceRead::raspiCamError,raspiCamMessages[First[bytes]]];
			$Failed
		),
		(*ELSE*)
		(*valid image, just take the rest of the bytes and interpret that*)
		(
			(*First coerce the bytes into a string*)
			string = FromCharacterCode[Rest[bytes]];
			(*now import the string into an image*)
			img = ImportString[string,"JPEG"];
			(*finally resize it to whatever the user requested*)
			ImageResize[img,width]
		)
	]
];

(* Ensure the camera is connected to the Pi. This function runs when DeviceOpen is called. *)
checkCamera[args___] := Module[ {vcout},
	
	(*  $isPiCameraConnected has 3 states:
		0 -> No check has been performed on the camera
		1 -> The camera is not present
		2 -> The camera is present
	*)
	If[ $isPiCameraConnected === 0 ,

		(* Unprotect the camera connection variable so it can be set *)
		Unprotect[$isPiCameraConnected];

		vcout = TimeConstrained[RunProcess[{"/opt/vc/bin/vcgencmd", "get_camera"}, "StandardOutput"], 3];
		Switch[ vcout,

			"supported=1 detected=1\n",
			$isPiCameraConnected = 2;
			Protect[$isPiCameraConnected];
			Return[{{}}],

			"supported=1 detected=0\n",
			Message[DeviceOpen::cameraDisconnected]; 
			$isPiCameraConnected = 1;
			Protect[$isPiCameraConnected];
			Return[$Failed],

			"supported=0 detected=0\n",
			Message[DeviceOpen::cameraDisabled];
			$isPiCameraConnected = 1;
			Protect[$isPiCameraConnected];
			Return[$Failed],
		
			_,
			Message[DeviceOpen::cameraUnknown];
			$isPiCameraConnected = 1;
			Protect[$isPiCameraConnected];
			Return[$Failed]
		],
		
		(* If $isPiCameraConnected isn't 0, return accordingly *)
		If[ $isPiCameraConnected === 2, 
			(* If the camera is connected... *)
			Return[{{}}],
			(* ...else, *)
			Message[DeviceOpen::cameraDisconnected]; Return[$Failed]
		];
	]
];
   
(* Function to use with DeviceFind driver function. *)
findCamera[]:= checkCamera[];

(* Options for the CurrentImage wrapper function *)
Options[currentImagePi]={"ImageSize"->Medium, "RasterSize"->{maxRaspiCamWidth,maxRaspiCamHeight}};

(* Wrapper functions for CurrentImage *)
currentImagePi[ opts:OptionsPattern[] ] := Module[ 
	{res},
	If[ (res = DeviceRead["RaspiCam", OptionValue["RasterSize"]]) =!= $Failed,
		Image[res,ImageSize->OptionValue["ImageSize"]],
		$Failed
	]
];

currentImagePi[ num_Integer, opts:OptionsPattern[] ] := 
	Table[ currentImagePi[ FilterRules[{opts},Options[currentImagePi]] ], num ];


(*============================================================================*)
(* SenseHAT Code *)
(*============================================================================*)

(* Helper function to turn 3 color values from 0-255 into 2 bytes that will be written to the output stream *)
encodePixel[color_List]:=Block[{r,g,b,bits},
	(* Ensure 3 values are given for R,G,B *)
	If[Length[color]!=3,Return[$Failed]];

	(* Condense the 3 values to a single 16 bit integer *)
	r=BitAnd[BitShiftRight[color[[1]],3],FromDigits["1F",16]];
	g=BitAnd[BitShiftRight[color[[2]],2],FromDigits["3F",16]];
	b=BitAnd[BitShiftRight[color[[3]],3],FromDigits["1F",16]];
	bits=BitShiftLeft[r,11]+BitShiftLeft[g,5]+b;

	(* Convert integer into 2 byte values *)
	bits=Reverse[IntegerDigits[bits,256,2]];
	Return[bits];
];

(* Helper function to turn off all LEDs *)
clearLEDArray[]:=Block[{stream},
	stream=OpenWrite["/dev/fb1","BinaryFormat"->True];
	If[stream === $Failed,
		Message[DeviceWrite::writePixelErr]; Return[]
	];
	BinaryWrite[stream,Table[encodePixel[{0,0,0}],{64}]];
	Close[stream];
];


(* Display a list of pixel values *)
displayList[img_List]:=Block[
	{d,display,stream},

	(* Fix list structure if not depth 3 *)
	d=Depth[img];
	Which[
		d>3,  display=Flatten[img,d-3],
		d==2, display=Partition[img,3],
		True, display=img;
	];

	(* Encode groups of 3 pixels into 16-bit ints *)
	display=Flatten[encodePixel/@display];

	(* Open stream to LED array *)
	stream=OpenWrite["/dev/fb1","BinaryFormat"->True];
	If[stream === $Failed,
		Message[DeviceWrite::writePixelErr]; $Failed
	];

	(* Display pixels *)
	BinaryWrite[stream,display];

	(* Close the stream *)
	Close[stream];
];

(* Display an Image object resized to 8x8 pixels*)
displayImage[img_Image]:=Block[
	{ imgData },

	(* Turn image into a list of 196 byte values *)
	imgData=Flatten[ImageData[ImageResize[img,{8,8}],"Byte"],1];

	(* Display like any other list *)
	displayList[imgData];
];

(* Display a string by scrolling it across the LED array *)
displayString[str_String, color_List, scroll_Real]:=Block[
	{imgData, frames, strImg},

	(* If str is empty, clear the LEDs *)
	If[ str=="", clearLEDArray[]; Return[]; ];

	(* Rasterize string to an image with black background *)
	strImg=ImageCrop[Rasterize[Style[str, White], Background -> Black]];

	(* Resize image to max 8 pixels tall *)
	strImg=ImageResize[strImg, {Automatic,{8}}, Resampling->"Nearest"];

	(* Pad image on left and right *)
	strImg=ImagePad[strImg,{{8,8},{0,0}},Black];

	(* Get the number of frames that will be displayed *)
	frames = ImageDimensions[strImg][[1]] - 8;

	(* For each frame, display a vertical slice of the image *)
	For[ n=1, n<frames, n++,
		displayList[ImageData[ImageTake[strImg, All, {n,n+7}],"Byte"]];
		Pause[scroll];
	];
	clearLEDArray[];
];

(* Display a given pattern on the LED array *)
Options[displayLEDs]={"Color"->{255,255,255}, "ScrollSpeed"->0.05};

displayLEDs[{ihandle_,dhandle_},args___,opts:OptionsPattern[]]:=Block[
	{ color, scroll },

	(* Error Checking *)
	If[Length[OptionValue["Color"]]!=3, color={255,255,255}, color=OptionValue["Color"]];
	If[Head[OptionValue["ScrollSpeed"]]=!=Real, scroll=0.05, scroll=OptionValue["ScrollSpeed"]];

	(* Call display functions based on type of input *)
	Switch[ Head[args],
		Image,  displayImage[args],
		String, displayString[args, color, scroll],
		List,   displayList[args];
	];
];


(* SENSOR FUNCTIONS AND HELPERS *)

(* Helper function to convert a binary string to a number using 2's complement *)
tc16bit[digits_List]:=(Block[{},
	Return[-digits[[1]]*2^15 + Total[Table[digits[[i]]*2^(16 - i), {i, 2, 16}]]];
]);

(* Helper function to cleanly read from a specific register *)
i2CRegisterRead[sensor_DeviceObject, register_String]:=(
	DeviceWrite[sensor,FromDigits[register,16]];
	Return[DeviceRead[sensor]];
);

senseHatReadTemperature[]:=Module[
	{binaryDigits, pressureSensor, temp, tempOutHigh, tempOutLow},
	
	pressureSensor=DeviceOpen["I2C",92];
	
	(* Initialize the sensor *)
	DeviceWrite[pressureSensor,{FromDigits["20",16],FromDigits["C4",16]}];
	DeviceWrite[pressureSensor,{FromDigits["10",16],FromDigits["05",16]}];
	DeviceWrite[pressureSensor,{FromDigits["2E",16],FromDigits["C0",16]}];
	DeviceWrite[pressureSensor,{FromDigits["21",16],FromDigits["40",16]}];

	(* Read data from the temperature registers *)
	tempOutLow=i2CRegisterRead[pressureSensor,"2B"];
	tempOutHigh=i2CRegisterRead[pressureSensor,"2C"];

	binaryDigits=Join[IntegerDigits[tempOutHigh,2,8],IntegerDigits[tempOutLow,2,8]];
	
	(* Sometimes this fails on the first reading, so double check *)
	If[ tc16bit[binaryDigits]==0,
		Pause[0.5];
		tempOutLow=i2CRegisterRead[pressureSensor,"2B"];
		tempOutHigh=i2CRegisterRead[pressureSensor,"2C"];
		binaryDigits=Join[IntegerDigits[tempOutHigh,2,8],IntegerDigits[tempOutLow,2,8]];
	];
	
	(* Convert this binary string to degrees Celsius, formula taken form sensor manual, page 34 *)
	temp=(tc16bit[binaryDigits]/480)+42.5;

	(* Close the device to prevent memory leaks *)
	DeviceClose[pressureSensor];

	(* Return the temperature *)
	Return[temp];
];

senseHatReadPressure[]:=Module[
	{binaryDigits, pressureSensor, pres, presOutHigh, presOutLow, presOutXLow},
	pressureSensor=DeviceOpen["I2C",92];
	
	(* Initialize the sensor *)
	DeviceWrite[pressureSensor,{FromDigits["20",16],FromDigits["C4",16]}];
	DeviceWrite[pressureSensor,{FromDigits["10",16],FromDigits["05",16]}];
	DeviceWrite[pressureSensor,{FromDigits["2E",16],FromDigits["C0",16]}];
	DeviceWrite[pressureSensor,{FromDigits["21",16],FromDigits["40",16]}];

	(* Read data from the pressure registers *)
	presOutXLow=i2CRegisterRead[pressureSensor,"28"];
	presOutLow=i2CRegisterRead[pressureSensor,"29"];
	presOutHigh=i2CRegisterRead[pressureSensor,"2A"];

	(* Convert this binary string to millibars, formula taken form sensor manual, page 33 *)
	binaryDigits=Join[IntegerDigits[presOutHigh,2,8],IntegerDigits[presOutLow,2,8],IntegerDigits[presOutXLow,2,8]];
	pres=tc16bit[binaryDigits]/4096;

	(* Close the device to prevent memory leaks *)
	DeviceClose[pressureSensor];

	(* Return the pressure *)
	Return[N[pres]];
];

senseHatReadHumidity[]:=Module[
	{binaryDigits, humiditySensor, hum, humOutHigh, humOutLow, modifiers},
	humiditySensor=DeviceOpen["I2C",95];
	
	(* Initialize the sensor *)
	DeviceWrite[humiditySensor,{FromDigits["20",16],FromDigits["87",16]}];
	DeviceWrite[humiditySensor,{FromDigits["10",16],FromDigits["1B",16]}];

	(* Calibrate humidity sensor *)
	modifiers=calibrateHumidity[humiditySensor];

	(* Read data from the humidity registers *)
	humOutLow=i2CRegisterRead[humiditySensor,"28"];
	humOutHigh=i2CRegisterRead[humiditySensor,"29"];

	(* Convert this binary string to pressure using two's complement *)
	binaryDigits=Join[IntegerDigits[humOutHigh,2,8],IntegerDigits[humOutLow,2,8]];
	hum=tc16bit[binaryDigits];

	(* Use calibration modifiers to correct humidity *)
	hum=hum*modifiers[[1]]+modifiers[[2]];

	(* Close the device to prevent memory leaks *)
	DeviceClose[humiditySensor];

	(* Return the humidity *)
	Return[N[hum]];
];

(* Get calibration data for the humidity sensor, modeled after RTHumidityHTS221.cpp *)
calibrateHumidity[humiditySensor_DeviceObject]:=Module[{h0,h1,h0t0out,h1t0out,humm,humc},
	h0=i2CRegisterRead[humiditySensor,"30"]/2;
	h1=i2CRegisterRead[humiditySensor,"31"]/2;
	h0t0out=tc16bit[Join[IntegerDigits[i2CRegisterRead[humiditySensor,"36"],2,8],IntegerDigits[i2CRegisterRead[humiditySensor,"37"],2,8]]];
	h1t0out=tc16bit[Join[IntegerDigits[i2CRegisterRead[humiditySensor,"3A"],2,8],IntegerDigits[i2CRegisterRead[humiditySensor,"3B"],2,8]]];
	humm=(h1-h0)/(h1t0out-h0t0out);
	humc=h0-(humm*h0t0out);
	Return[{humm,humc}];
];

senseHatReadGyroscope[]:=Module[
	{conv=(0.07/360), ndofSensor, pitch, roll, yaw, xHigh, xLow, yHigh, yLow, zHigh, zLow},
	
	ndofSensor=DeviceOpen["I2C",106];

	(* Read data from the x,y,z angle registers *)
	xLow=i2CRegisterRead[ndofSensor,"18"];
	xHigh=i2CRegisterRead[ndofSensor,"19"];
	yLow=i2CRegisterRead[ndofSensor,"1A"];
	yHigh=i2CRegisterRead[ndofSensor,"1B"];
	zLow=i2CRegisterRead[ndofSensor,"1C"];
	zHigh=i2CRegisterRead[ndofSensor,"1D"];

	(* Convert the raw data to pitch, roll, and yaw in revolutions per second *)
	roll=tc16bit[Join[IntegerDigits[xHigh,2,8],IntegerDigits[xLow,2,8]]];
	pitch=tc16bit[Join[IntegerDigits[yHigh,2,8],IntegerDigits[yLow,2,8]]];
	yaw=tc16bit[Join[IntegerDigits[zHigh,2,8],IntegerDigits[zLow,2,8]]];	

	(* Conversion factors taken from RTIMULSM9DS1.cpp and RTIMULib.ini *)
	pitch*=conv; roll*=-conv; yaw*=conv;

	(* Close the device to prevent memory leaks *)
	DeviceClose[ndofSensor];

	(* Return the gyroscope data *)
	Return[{roll,pitch,yaw}];
];

senseHatReadAccelerometer[]:=Module[
	{conv=0.000244, ndofSensor, x, xHigh, xLow, y, yHigh, yLow, z, zHigh, zLow},

	ndofSensor=DeviceOpen["I2C",106];

	(* Read data from the x,y,z angle registers *)
	xLow=i2CRegisterRead[ndofSensor,"28"];
	xHigh=i2CRegisterRead[ndofSensor,"29"];
	yLow=i2CRegisterRead[ndofSensor,"2A"];
	yHigh=i2CRegisterRead[ndofSensor,"2B"];
	zLow=i2CRegisterRead[ndofSensor,"2C"];
	zHigh=i2CRegisterRead[ndofSensor,"2D"];

	(* Convert the raw data to pitch, roll, and yaw in G's (Earth's gravity) *)
	x=tc16bit[Join[IntegerDigits[xHigh,2,8],IntegerDigits[xLow,2,8]]];
	y=tc16bit[Join[IntegerDigits[yHigh,2,8],IntegerDigits[yLow,2,8]]];
	z=tc16bit[Join[IntegerDigits[zHigh,2,8],IntegerDigits[zLow,2,8]]];	

	(* Conversion factors taken from RTIMULSM9DS1.cpp *)
	x*=-conv; y*=-conv; z*=conv;

	(* Close the device to prevent memory leaks *)
	DeviceClose[ndofSensor];

	(* Return the accelerometer data *)
	Return[{x,y,z}];
];

senseHatReadMagnetometer[]:=Block[
	{
		conv=0.014,magSensor,
		Mx,My,Mz,xHigh,xLow,yHigh,yLow,zHigh,zLow
	},

	magSensor=DeviceOpen["I2C",28];

	(* Read data from the x,y,z registers *)
	xLow=i2CRegisterRead[magSensor,"28"];
	xHigh=i2CRegisterRead[magSensor,"29"];
	yLow=i2CRegisterRead[magSensor,"2A"];
	yHigh=i2CRegisterRead[magSensor,"2B"];
	zLow=i2CRegisterRead[magSensor,"2C"];
	zHigh=i2CRegisterRead[magSensor,"2D"];

	(* Convert the raw binary data to ints *)
	Mx=tc16bit[Join[IntegerDigits[xHigh,2,8],IntegerDigits[xLow,2,8]]];
	My=tc16bit[Join[IntegerDigits[yHigh,2,8],IntegerDigits[yLow,2,8]]];
	Mz=tc16bit[Join[IntegerDigits[zHigh,2,8],IntegerDigits[zLow,2,8]]];	

	(* Conversion factor taken from RTIMULSM9DS1.cpp *)
	Mx*=-conv; My*=conv; Mz*=-conv;

	(* Close the device to prevent memory leaks *)
	DeviceClose[magSensor];

	(* Return the compass data *)
	Return[{Mx,My,Mz}];
];

senseHatCalculateOrientation[]:=Block[
	{Ax, Ay, Az, Mx, My, Mz, roll, pitch, yaw},

	(* Read acceleration and magnetometer data *)
	{Ax,Ay,Az}=senseHatReadAccelerometer[];
	{Mx,My,Mz}=senseHatReadMagnetometer[];
	
	(* Calculate roll and pitch from acceleration readings *)
	roll=ArcTan[Az,Ay];
	pitch=ArcTan[Sqrt[Ay^2+Az^2],-Ax];
	
	(* Calculate yaw from accel and mag readings *)
	yaw=-ArcTan[Mx*Cos[pitch]+Mz*Sin[pitch],Mx*Sin[roll]*Sin[pitch]+My*Cos[roll]-Mz*Sin[roll]*Cos[pitch]];

	(* Convert radians to degrees *)
	{roll,pitch,yaw}*=(180/Pi);

	Return[{roll,pitch,yaw}];
];

(* Device Read function *)
raspiSenseHat[{ihandle_,dhandle_},args___]:=( Module[ 
	{datatype=ToLowerCase[args]},

	(* Call internal functions *)
	If[StringMatchQ[datatype,"temperature"], Return[ Quantity[senseHatReadTemperature[],"Celsius"] ] ];
	If[StringMatchQ[datatype,"humidity"], Return[ Quantity[senseHatReadHumidity[],"%"] ] ];
	If[StringMatchQ[datatype,"pressure"], Return[ Quantity[senseHatReadPressure[],"mbar"] ] ];
	If[StringMatchQ[datatype,"rotation"], Return[ Map[Quantity[#,"Revolutions"/"Seconds"]&, senseHatReadGyroscope[]] ] ];
	If[StringMatchQ[datatype,"magnetic field"], Return[ Map[Quantity[#,"Microteslas"]&, senseHatReadMagnetometer[]] ] ];
	If[StringMatchQ[datatype,"orientation"], Return[ senseHatCalculateOrientation[] ] ];
	If[StringMatchQ[datatype,"acceleration"], Return[ Map[Quantity[#,"StandardAccelerationOfGravity"]&, senseHatReadAccelerometer[]] ] ];
	
	(* If the arg doesn't match anything else, return an error message *)
	Message[DeviceRead::unknownFunction];
	Return[$Failed];
]);

(* DRIVER FUNCTION *)

deviceOpenDriver[ihandle_]:=Module[
	{osfile, pres, hmdy, gyro, magn},

	(* Open connections to the 4 I2C addresses used by the SenseHAT *)
	pres=DeviceOpen["I2C",92];
	hmdy=DeviceOpen["I2C",95];
	gyro=DeviceOpen["I2C",106];
	magn=DeviceOpen["I2C",28];

	(* If any failed... *)
	If[AnyTrue[{pres,hmdy,gyro,magn}, #==$Failed &], 
		Message[DeviceOpen::noDevice];Return[$Failed] (* ...return $Failed *)
	];

	(* Boot gyro/accel sensor *)
	DeviceWrite[gyro,{FromDigits["22",16],128}];
	
	(* Reset mag sensor *)
	DeviceWrite[magn,{FromDigits["21",16],104}];

	(* Set gyro sample rate and full-scale range (59.5 hz, 2000 dps) *)
	DeviceWrite[gyro,{FromDigits["10",16],88}];

	(* Set gyro high pass filter to 'on' *)
	DeviceWrite[gyro,{FromDigits["12",16],64}];

	(* Set accel rate and full-scale range (50 hz, +/-8g) *)
	DeviceWrite[gyro,{FromDigits["20",16],88}];

	(* Zero out contol register 0x21 *)
	DeviceWrite[gyro,{FromDigits["21",16],0}];

	(* Set mag sample rate (20 hz) *)
	DeviceWrite[magn,{FromDigits["22",16],20}];

	(* Set mag full-scale range (+/-4 gauss) *)
	DeviceWrite[magn,{FromDigits["22",16],0}];

	(* Zero out control register 0x22 *)
	DeviceWrite[magn,{FromDigits["22",16],0}];

	(* Turn off fast-read and turn on block updates *)
	DeviceWrite[magn,{FromDigits["24",16],64}];

	(* Return the device handle *)
	Return[ihandle];
];


End[]


EndPackage[]

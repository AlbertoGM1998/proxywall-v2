(* Paclet Info File *)

(* created 2018/09/26*)

Paclet[
    Name -> "AnatomyGraphics3D",
    Version -> "11.0.0",
    MathematicaVersion -> "11.0+",
    Loading -> Automatic,
    Extensions -> 
        {
            {"Kernel", Symbols -> 
                {"System`AnatomyPlot3D", "System`AnatomyStyling", "System`AnatomySkinStyle"}
            , Root -> "Kernel", Context -> 
                {"AnatomyGraphics3DLoader`", "AnatomyGraphics3D`"}
            }
        }
]



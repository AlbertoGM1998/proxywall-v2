(* Paclet Info File *)

(* created 2017/02/03*)

Paclet[
    Name -> "ServiceConnection_MicrosoftTranslator",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"MicrosoftTranslator`", "MicrosoftTranslatorLoad`", "MicrosoftTranslatorFunctions`"}
            }, 
            {"FrontEnd"}, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/MicrosoftTranslator", Language -> All}
        }
]



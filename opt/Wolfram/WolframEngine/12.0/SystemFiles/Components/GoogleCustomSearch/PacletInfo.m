(* Paclet Info File *)

(* created 2017/02/03*)

Paclet[
    Name -> "ServiceConnection_GoogleCustomSearch",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"GoogleCustomSearch`", "GoogleCustomSearchLoad`", "GoogleCustomSearchFunctions`"}
            }, 
            {"FrontEnd"}, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/GoogleCustomSearch", Language -> All}
        }
]



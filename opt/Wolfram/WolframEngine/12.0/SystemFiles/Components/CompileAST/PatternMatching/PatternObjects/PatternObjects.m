

BeginPackage["CompileAST`PatternMatching`PatternObjects`", {
	"CompileAST`PatternMatching`PatternObjects`NamedPattern`",
	"CompileAST`PatternMatching`PatternObjects`OptionalPattern`",
	"CompileAST`PatternMatching`PatternObjects`PatternBindings`",
	"CompileAST`PatternMatching`PatternObjects`SequencePattern`",
	"CompileAST`PatternMatching`PatternObjects`SingleBlank`",
	"CompileAST`PatternMatching`PatternObjects`ConditionalPattern`",
	"CompileAST`PatternMatching`PatternObjects`Verbatim`",
	"CompileAST`PatternMatching`PatternObjects`ExceptPattern`"
}]
EndPackage[]

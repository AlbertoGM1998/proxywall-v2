BeginPackage["CompileAST`Export`Format`Information`", {
	"CompileAST`Export`Format`Information`Character`",
	"CompileAST`Export`Format`Information`Delimiter`",
	"CompileAST`Export`Format`Information`WhiteSpace`",
	"CompileAST`Export`Format`Information`Operator`",
	"CompileAST`Export`Format`Information`Precedence`",
	"CompileAST`Export`Format`Information`Helper`"
}]

(*
 * Most of the tables in this folder were taken from
 * Mathematica/Packages/Notation/Notation.m
 * The interface has been changed to work with UnicodeTable.m which
 * provides richer information
 **)
EndPackage[]
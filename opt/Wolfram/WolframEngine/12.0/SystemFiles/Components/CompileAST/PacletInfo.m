(* Paclet Info File *)

(* created 2017/05/19*)

Paclet[
    Name -> "CompileAST",
    Version -> "0.9",
    WolframVersion -> "12+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> ".", Context -> {"CompileAST`"}}
    }
]



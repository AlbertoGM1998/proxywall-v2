(* Paclet Info File *)

(* created 2017/02/28*)

Paclet[
    Name -> "ServiceConnection_FederalReserveEconomicData",
    Version -> "11.1.7",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"FederalReserveEconomicData`", "FederalReserveEconomicDataLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/FederalReserveEconomicData", Language -> All}
        }
]



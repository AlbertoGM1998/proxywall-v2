(* Paclet Info File *)

(* created 2015.04.03*)

Paclet[
	Name -> "MRAALink",
	Version -> "1.0.6",
	MathematicaVersion -> "10+",
	Extensions -> {
		{"LibraryLink",SystemID->"Linux-ARM"},
	  	{"Kernel",
	  		Root -> "Kernel",
	  		Context -> 
	  			{
		  			"MRAALinkImpl`","MRAALink`"
	  			}
	  	},
	    {"Documentation",
			Language->"English"
		}
	}
]
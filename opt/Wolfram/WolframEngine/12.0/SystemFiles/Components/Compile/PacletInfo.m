Paclet[
    Name -> "Compile",
    Version -> "1.0.0.1",
    WolframVersion -> "12+",
    Loading -> Automatic,
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", 
        	HiddenImport -> {},
        	Context -> {"Compile`"},
            Symbols -> {
                "System`FunctionCompile",
                "System`FunctionCompileExport",
                "System`FunctionCompileExportString",
                "System`FunctionCompileExportByteArray",
                "System`FunctionCompileExportLibrary",
                "Compile`Internal`RepairCompiledCodeFunction",
                "Compile`Internal`RuntimeErrorHandler"}
        }
    }
]


(*
Generated using

Map[#["Name"]&, WolframLanguageData[EntityClass["WolframLanguageSymbol", "Atomic"]]]
*)

BeginPackage["Compile`Utilities`Language`Atomic`"]

$AtomicSymbols

Begin["`Private`"]

$AtomicSymbols := $AtomicSymbols =
	{"Association", "Complex", "Dataset", "Graph", "BooleanFunction", 
	"Image", "Dispatch", "Rational", "SparseArray", "Image3D", 
	"MeshRegion", "BoundaryMeshRegion", "ColorProfileData", 
	"StructuredArray"} 
  
End[]

EndPackage[]
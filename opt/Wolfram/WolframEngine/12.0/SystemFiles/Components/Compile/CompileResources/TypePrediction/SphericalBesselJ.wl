(* Created with the Wolfram Language : www.wolfram.com *)
{{"MachineInteger", "Real64"} -> "Real64", {"MachineInteger", "Complex"["Real64"]} -> 
  "Complex"["Real64"], {"Real64", "MachineInteger"} -> "Real64", 
 {"Real64", "Real64"} -> "Real64", {"Real64", "Complex"["Real64"]} -> 
  "Complex"["Real64"], {"Complex"["Real64"], "MachineInteger"} -> 
  "Complex"["Real64"], {"Complex"["Real64"], "Real64"} -> 
  "Complex"["Real64"], {"Complex"["Real64"], "Complex"["Real64"]} -> 
  "Complex"["Real64"]}



BeginPackage["Compile`Core`IR`Instruction`Utilities`", {
	"Compile`Core`IR`Instruction`Utilities`InstructionVisitor`",
	"Compile`Core`IR`Instruction`Utilities`InstructionTraits`",
	"Compile`Core`IR`Instruction`Utilities`InstructionOperator`",
	"Compile`Core`IR`Instruction`Utilities`InstructionFields`",
	"Compile`Core`IR`Instruction`Utilities`InstructionRegistry`",
	"Compile`Core`IR`Instruction`Utilities`InstructionMatch`"
}]

EndPackage[]

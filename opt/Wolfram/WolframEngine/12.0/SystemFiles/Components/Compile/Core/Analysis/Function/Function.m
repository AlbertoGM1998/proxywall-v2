
BeginPackage["Compile`Core`Analysis`Function`", {
    "Compile`Core`Analysis`Function`FunctionInlineInformation`",
    "Compile`Core`Analysis`Function`FunctionInlineCost`",
    "Compile`Core`Analysis`Function`FunctionIsInlinable`",
    "Compile`Core`Analysis`Function`FunctionIsTrivialCall`",
    "Compile`Core`Analysis`Function`FunctionThrows`"
}]


EndPackage[]

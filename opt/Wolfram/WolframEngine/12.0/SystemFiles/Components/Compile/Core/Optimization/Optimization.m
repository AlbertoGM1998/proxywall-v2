

BeginPackage["Compile`Core`Optimization`", {
	"Compile`Core`Optimization`CollectBasicBlocks`",
	"Compile`Core`Optimization`CopyPropagation`",
	"Compile`Core`Optimization`ConstantPropagation`",
	"Compile`Core`Optimization`ConstantStackAllocation`",
	"Compile`Core`Optimization`DeadBranchElimination`",
	"Compile`Core`Optimization`DeadCodeElimination`",
	"Compile`Core`Optimization`CoerceCast`",
	"Compile`Core`Optimization`SimplifyExpression`",
	"Compile`Core`Optimization`EvaluateExpression`",
	"Compile`Core`Optimization`FuseListMap`",
	"Compile`Core`Optimization`ExpressionNormalization`",
	"Compile`Core`Optimization`CommonSubexpressionElimination`",
	"Compile`Core`Optimization`SelectInstructionRemoval`",
	"Compile`Core`Optimization`EmptyBlockRemoval`",
	"Compile`Core`Optimization`JumpThreading`",
	"Compile`Core`Optimization`FuseBasicBlocks`",
	"Compile`Core`Optimization`RecordFixedMTensor`",
	"Compile`Core`Optimization`RecordSetElement`",
	"Compile`Core`Optimization`RemoveRedundantStackAllocate`",
	"Compile`Core`Optimization`OptimizeBasicBlocks`",
	"Compile`Core`Optimization`NormalizeControlFlow`",
	"Compile`Core`Optimization`PhiElimination`",
	"Compile`Core`Optimization`DeadJumpElimination`"
}]

EndPackage[]

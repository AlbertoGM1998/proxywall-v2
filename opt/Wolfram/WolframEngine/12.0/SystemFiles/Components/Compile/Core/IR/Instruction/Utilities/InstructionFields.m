
BeginPackage["Compile`Core`IR`Instruction`Utilities`InstructionFields`"]

InstructionFields


Begin["`Private`"]


InstructionFields = {
	"id" -> 0,
	"mexpr",
	"_next",
	"_previous",
	"basicBlock" -> None,
	"visited" -> False,
	"properties" -> None
}

End[]
EndPackage[]

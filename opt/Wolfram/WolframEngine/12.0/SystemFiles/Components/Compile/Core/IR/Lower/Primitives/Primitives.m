


BeginPackage["Compile`Core`IR`Lower`Primitives`", {
	"Compile`Core`IR`Lower`Primitives`Operation`",
	"Compile`Core`IR`Lower`Primitives`Set`",
	"Compile`Core`IR`Lower`Primitives`Atom`",
	"Compile`Core`IR`Lower`Primitives`Module`",
	"Compile`Core`IR`Lower`Primitives`Symbol`",
	"Compile`Core`IR`Lower`Primitives`Compound`",
	"Compile`Core`IR`Lower`Primitives`FunctionVariable`",
	"Compile`Core`IR`Lower`Primitives`If`",
	"Compile`Core`IR`Lower`Primitives`Error`",
	"Compile`Core`IR`Lower`Primitives`List`",
	"Compile`Core`IR`Lower`Primitives`While`",
	"Compile`Core`IR`Lower`Primitives`Part`",
	"Compile`Core`IR`Lower`Primitives`Function`",
	"Compile`Core`IR`Lower`Primitives`NDSolve`",
	"Compile`Core`IR`Lower`Primitives`Inert`",
	"Compile`Core`IR`Lower`Primitives`Return`",
	"Compile`Core`IR`Lower`Primitives`StackAllocate`",
	"Compile`Core`IR`Lower`Primitives`Typed`",
	"Compile`Core`IR`Lower`Primitives`General`",
	"Compile`Core`IR`Lower`Primitives`Structural`",
	"Compile`Core`IR`Lower`Primitives`DeclareType`",
	"Compile`Core`IR`Lower`Primitives`Field`",
	"Compile`Core`IR`Lower`Primitives`SizeOf`",
	"Compile`Core`IR`Lower`Primitives`Break`",
	"Compile`Core`IR`Lower`Primitives`Continue`",
	"Compile`Core`IR`Lower`Primitives`SetProperty`",
	"Compile`Core`IR`Lower`Primitives`LanguagePrimitive`",
	"Compile`Core`IR`Lower`Primitives`Which`"
}]

EndPackage[]
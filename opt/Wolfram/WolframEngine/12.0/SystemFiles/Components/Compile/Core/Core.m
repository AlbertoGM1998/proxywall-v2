
BeginPackage["Compile`Core`", {
	"Compile`Core`Utilities`",
	"Compile`Core`IR`",
	"Compile`Core`Lint`",
	"Compile`Core`Analysis`",
	"Compile`Core`PassManager`",
	"Compile`Core`Transform`",
	"Compile`Core`Optimization`",
	"Compile`Core`CodeGeneration`",
	"Compile`Core`Debug`"
}]

EndPackage[]
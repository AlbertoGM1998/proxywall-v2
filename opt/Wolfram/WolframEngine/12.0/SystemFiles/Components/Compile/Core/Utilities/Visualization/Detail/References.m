(*

See

- http://publications.lib.chalmers.se/records/fulltext/161388.pdf
- https://cs.brown.edu/~rt/gdhandbook/chapters/hierarchical.pdf
- http://sydney.edu.au/engineering/it/~shhong/fab.pdf
- http://rtsys.informatik.uni-kiel.de/teaching/ss09/s-ober/osem09ss-msp-handout.pdf
- http://www.graphviz.org/Documentation/TSE93.pdf
- http://hci.stanford.edu/courses/cs448b/w09/lectures/20090204-GraphsAndTrees.pdf
*)
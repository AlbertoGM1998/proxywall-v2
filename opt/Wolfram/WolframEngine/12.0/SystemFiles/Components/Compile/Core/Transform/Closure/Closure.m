BeginPackage["Compile`Core`Transform`Closure`", {
	"Compile`Core`Transform`Closure`ResolveClosureVariableType`",
	"Compile`Core`Transform`Closure`DeclareClosureEnvironmentType`",
	"Compile`Core`Transform`Closure`PackClosureEnvironment`",
	"Compile`Core`Transform`Closure`PassClosureArguments`",
	"Compile`Core`Transform`Closure`UnpackClosureEnvironment`",
	"Compile`Core`Transform`Closure`DesugarClosureEnvironmentStructure`",
	"Compile`Core`Transform`Closure`ResolveClosure`",
	"Compile`Core`Transform`Closure`ResolveLambdaClosure`"
}]

EndPackage[]
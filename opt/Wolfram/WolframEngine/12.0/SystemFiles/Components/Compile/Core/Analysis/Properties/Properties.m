
BeginPackage["Compile`Core`Analysis`Properties`", {
    "Compile`Core`Analysis`Properties`BasicBlockLastPhi`",
    "Compile`Core`Analysis`Properties`LastBasicBlock`",
    "Compile`Core`Analysis`Properties`VariableValue`",
    "Compile`Core`Analysis`Properties`LocalCallInformation`",
    "Compile`Core`Analysis`Properties`ClosureVariablesProvided`",
    "Compile`Core`Analysis`Properties`HasClosure`",
    "Compile`Core`Analysis`Properties`FunctionCallLinkage`"
}]


EndPackage[]

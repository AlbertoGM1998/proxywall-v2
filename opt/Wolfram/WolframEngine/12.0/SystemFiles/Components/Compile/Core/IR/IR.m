
BeginPackage["Compile`Core`IR`", {
	"Compile`Core`IR`CompiledProgram`",
	"Compile`Core`IR`ConstantValue`",
	"Compile`Core`IR`Variable`",
	"Compile`Core`IR`Instruction`",
	"Compile`Core`IR`BasicBlock`",
	"Compile`Core`IR`FunctionModule`",
	"Compile`Core`IR`GlobalValue`",
	"Compile`Core`IR`ProgramModule`",
	"Compile`Core`IR`MetaData`",
	"Compile`Core`IR`ExternalDeclaration`",
	"Compile`Core`IR`FunctionDeclaration`",
	"Compile`Core`IR`TypeDeclaration`",
	"Compile`Core`IR`TypeName`",
	"Compile`Core`IR`LoopInformation`",
	"Compile`Core`IR`Lower`",
	"Compile`Core`IR`Internal`",
	"Compile`Core`IR`GetElementTrait`"
}]

EndPackage[]
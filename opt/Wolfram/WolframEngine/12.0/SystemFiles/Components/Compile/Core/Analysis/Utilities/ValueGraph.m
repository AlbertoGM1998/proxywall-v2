(* 
implements the value graph used in global value numbering a la Alpern, Wegman and Zadeck. See Muchnick p.348 for a nice discussion. 
https://www.jikesrvm.org/JavaDoc/org/jikesrvm/compilers/opt/ssa/ValueGraph.html
*)
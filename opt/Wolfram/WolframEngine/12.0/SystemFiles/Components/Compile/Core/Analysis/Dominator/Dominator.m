(**
 * http://www.cs.rice.edu/~keith/EMBED/dom.pdf
 * http://www.cs.nyu.edu/leunga/MLRISC/Doc/html/compiler-graphs.html
 *)

BeginPackage["Compile`Core`Analysis`Dominator`", {
    "Compile`Core`Analysis`Dominator`DominanceFrontier`",
    "Compile`Core`Analysis`Dominator`DominatorPass`",
    "Compile`Core`Analysis`Dominator`ImmediateDominator`",
    "Compile`Core`Analysis`Dominator`ImmediatePostDominator`",
    "Compile`Core`Analysis`Dominator`PostDominator`",
    "Compile`Core`Analysis`Dominator`StrictDominator`",
    "Compile`Core`Analysis`Dominator`StrictPostDominator`"
}]

EndPackage[]
## Papers

## Slides

https://github.com/suhorng/HaskellExercise/raw/gh-pages/ft27/slide/typeinfer.pdf

## Generalization

http://okmij.org/ftp/ML/generalization.html 


## Links

http://flowtype.org/
http://puredocs.bitbucket.org/pure.html#type-rules
https://github.com/Araq/Nim/blob/master/lib/system.nim#L35
http://wiki.squeak.org/squeak/464
http://gosu-lang.github.io/docs.html#basics
https://github.com/chrisvoncsefalvay/learn-julia-the-hard-way/blob/master/_chapters/06-ex3.md
http://julia.readthedocs.org/en/release-0.3/manual/conversion-and-promotion/
http://twelf.org/wiki/Main_Page
http://rustbyexample.com/trait.html
https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/Protocols.html#//apple_ref/doc/uid/TP40014097-CH25-XID_413

http://msdn.microsoft.com/en-us/library/hh156509.aspx
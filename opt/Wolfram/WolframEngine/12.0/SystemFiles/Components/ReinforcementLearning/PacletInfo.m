Paclet[
	Name -> "ReinforcementLearning",
	Version -> "1.0",
	MathematicaVersion -> "12+",
	Description -> "Reinforcement Learning Utilities",
	Loading -> Automatic,
	Updating -> Automatic,
	Creator -> 
		"Sebastian Bodenstein <sebastianb@wolfram.com>",
	Extensions -> {
		{"Kernel", Context -> "ReinforcementLearning`"}, 
		{"Documentation", Resources -> {},
		Language -> All
		},
		{"LibraryLink"},
		{"Resource",
			Root -> "Resources",
    		Resources -> {
	    		{"OpenAIGymLogo", "Bitmap/openai_gym.png"}
    	}
    }
	}
]

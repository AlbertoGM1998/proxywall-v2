(* ::Package:: *)

{
(* Macintosh-specific diacritical characters *)

"\.b4" -> "\[OptionKey]\[KeyBar]e ",

"\[AAcute]" -> "\[OptionKey]\[KeyBar]ea",

"\[CapitalAAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]a",

"\[EAcute]" -> "\[OptionKey]\[KeyBar]ee",

"\[CapitalEAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]e",

"\[IAcute]" -> "\[OptionKey]\[KeyBar]ei",

"\[CapitalIAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]i",

"\[OAcute]" -> "\[OptionKey]\[KeyBar]eo",

"\[CapitalOAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]o",

"\[UAcute]" -> "\[OptionKey]\[KeyBar]eu",

"\[CapitalUAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]u",

(*grave accent mark*)
(*"`" -> toKeys[" ", DeadKey -> toKeys["`", {"\[OptionKey]"}]]*)

"\[AGrave]" -> "\[OptionKey]\[KeyBar]`a",

"\[CapitalAGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]a",

"\[EGrave]" -> "\[OptionKey]\[KeyBar]`e",

"\[CapitalEGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]e",

"\[IGrave]" -> "\[OptionKey]\[KeyBar]`i",

"\[CapitalIGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]i",

"\[OGrave]" -> "\[OptionKey]\[KeyBar]`o",

"\[CapitalOGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]o",

"\[UGrave]" -> "\[OptionKey]\[KeyBar]`u",

"\[CapitalUGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]u",

(*double dot accent mark*)
"\.a8" -> "\[OptionKey]\[KeyBar]e ",

"\[ADoubleDot]" -> "\[OptionKey]\[KeyBar]ea",

"\[CapitalADoubleDot]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]a",

"\[EDoubleDot]" -> "\[OptionKey]\[KeyBar]ee",

"\[CapitalEDoubleDot]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]e",

"\[IDoubleDot]" -> "\[OptionKey]\[KeyBar]ei",

"\[CapitalIDoubleDot]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]i",

"\[ODoubleDot]" -> "\[OptionKey]\[KeyBar]eo",

"\[CapitalODoubleDot]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]o",

"\[UDoubleDot]" -> "\[OptionKey]\[KeyBar]eu",

"\[CapitalUDoubleDot]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]u",

(*hat accent mark*)
"\:02c6" -> "\[OptionKey]\[KeyBar]i ",

"\[AHat]" -> "\[OptionKey]\[KeyBar]ia",

"\[CapitalAHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]a",

"\[EHat]" -> "\[OptionKey]\[KeyBar]ie",

"\[CapitalEHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]e",

"\[IHat]" -> "\[OptionKey]\[KeyBar]ii",

"\[CapitalIHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]i",

"\[OHat]" -> "\[OptionKey]\[KeyBar]io",

"\[CapitalOHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]o",

"\[UHat]" -> "\[OptionKey]\[KeyBar]iu",

"\[CapitalUHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]u",

(*tilde accent mark*)
"\:02dc" -> "\[OptionKey]\[KeyBar]n ",

"\[ATilde]" -> "\[OptionKey]\[KeyBar]na",

"\[CapitalATilde]" -> "\[OptionKey]\[KeyBar]n\[ShiftKey]\[KeyBar]a",

"\[OTilde]" -> "\[OptionKey]\[KeyBar]no",

"\[CapitalOTilde]" -> "\[OptionKey]\[KeyBar]n\[ShiftKey]\[KeyBar]o"
}

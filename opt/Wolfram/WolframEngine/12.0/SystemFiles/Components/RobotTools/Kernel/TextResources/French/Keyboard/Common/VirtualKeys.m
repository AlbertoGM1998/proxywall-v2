(* ::Package:: *)

(*
VirtualKeys.m is a list of rules that specify the actual, underlying keys
that a user would type on an English keyboard to get the desired character
*)

{

"&" -> "1",

"\[EAcute]" -> "2",

"\"" -> "3",

"'" -> "4",

"(" -> "5",

"-" -> "6",

"\[EGrave]" -> "7",

"_" -> "8",

"\[CCedilla]" -> "9",

"\[AGrave]" -> "0"

}

(* ::Package:: *)

{
"Button1" -> java`awt`event`InputEvent`BUTTON1UMASK,

"Button2" -> java`awt`event`InputEvent`BUTTON2UMASK,

"Button3" -> java`awt`event`InputEvent`BUTTON3UMASK,

(* these are deprecated *)
"MouseButton1" -> java`awt`event`InputEvent`BUTTON1UMASK,

"MouseButton2" -> java`awt`event`InputEvent`BUTTON2UMASK,

"MouseButton3" -> java`awt`event`InputEvent`BUTTON3UMASK
}

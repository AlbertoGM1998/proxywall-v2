(* ::Package:: *)

{
"\\[F1Key]",

"\\[F2Key]",

"\\[F3Key]",

"\\[F4Key]",

"\\[F5Key]",

"\\[F6Key]",

"\\[F7Key]",

"\\[F8Key]",

"\\[F9Key]",

"\\[F10Key]",

"\\[F11Key]",

"\\[F12Key]",

"\\[F13Key]",

"\\[F14Key]",

"\\[F15Key]",

"\\[F16Key]",

"\\[F17Key]",

"\\[F18Key]",

"\\[F19Key]",

"\\[F20Key]",

"\\[F21Key]",

"\\[F22Key]",

"\\[F23Key]",

"\\[F24Key]",

"\\[PrintScreenKey]",

"\\[ScrollLockKey]",

"\\[PauseKey]",

"\\[BackspaceKey]",

"\\[CapsLockKey]",

"\\[InsertKey]",

"\\[HomeKey]",

"\\[PageUpKey]",

"\\[EndKey]",

"\\[PageDownKey]",

"\\[UpKey]",

"\\[LeftKey]",

"\\[DownKey]",

"\\[RightKey]",

"\\[NumLockKey]",

"\\[NumPad0Key]",

"\\[NumPad1Key]",

"\\[NumPad2Key]",

"\\[NumPad3Key]",

"\\[NumPad4Key]",

"\\[NumPad5Key]",

"\\[NumPad6Key]",

"\\[NumPad7Key]",

"\\[NumPad8Key]",

"\\[NumPad9Key]",

"\[EscapeKey]",

"\[SpaceKey]",

"\[ControlKey]",

"\[TabKey]",

"\[EnterKey]",

"\[ShiftKey]",

"\[DeleteKey]",

"`",

"1",

"2",

"3",

"4",

"5",

"6",

"7",

"8",

"9",

"0",

"-",

"=",

"q",

"w",

"e",

"r",

"t",

"y",

"u",

"i",

"o",

"p",

"[",

"",

"\\",

"a",

"s",

"d",

"f",

"g",

"h",

"j",

"k",

"l",

";",

"'",

"z",

"x",

"c",

"v",

"b",

"n",

"m",

",",

".",

"/",

(* add " " as a convenience so that \[RawSpace] isn't typed out, even though it is technically correct.
To type a space, the user should technically use \[SpaceKey], but this is the one case that there is a conflict. *)
" ",

"\[SystemEnterKey]"
}

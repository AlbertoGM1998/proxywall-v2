(* ::Package:: *)

{
"~" -> "\[ShiftKey]\[KeyBar]`",

"!" -> "\[ShiftKey]\[KeyBar]1",

"@" -> "\[ShiftKey]\[KeyBar]2",

"#" -> "\[ShiftKey]\[KeyBar]3",

"$" -> "\[ShiftKey]\[KeyBar]4",

"%" -> "\[ShiftKey]\[KeyBar]5",

"^" -> "\[ShiftKey]\[KeyBar]6",

"&" -> "\[ShiftKey]\[KeyBar]7",

"*" -> "\[ShiftKey]\[KeyBar]8",

"(" -> "\[ShiftKey]\[KeyBar]9",

")" -> "\[ShiftKey]\[KeyBar]0",

"_" -> "\[ShiftKey]\[KeyBar]-",

"+" -> "\[ShiftKey]\[KeyBar]=",

"Q" -> "\[ShiftKey]\[KeyBar]q",

"W" -> "\[ShiftKey]\[KeyBar]w",

"E" -> "\[ShiftKey]\[KeyBar]e",

"R" -> "\[ShiftKey]\[KeyBar]r",

"T" -> "\[ShiftKey]\[KeyBar]t",

"Y" -> "\[ShiftKey]\[KeyBar]y",

"U" -> "\[ShiftKey]\[KeyBar]u",

"I" -> "\[ShiftKey]\[KeyBar]i",

"O" -> "\[ShiftKey]\[KeyBar]o",

"P" -> "\[ShiftKey]\[KeyBar]p",

"{" -> "\[ShiftKey]\[KeyBar][",

"}" -> "\[ShiftKey]\[KeyBar]]",

"|" -> "\[ShiftKey]\[KeyBar]\\",

"A" -> "\[ShiftKey]\[KeyBar]a",

"S" -> "\[ShiftKey]\[KeyBar]s",

"D" -> "\[ShiftKey]\[KeyBar]d",

"F" -> "\[ShiftKey]\[KeyBar]f",

"G" -> "\[ShiftKey]\[KeyBar]g",

"H" -> "\[ShiftKey]\[KeyBar]h",

"J" -> "\[ShiftKey]\[KeyBar]j",

"K" -> "\[ShiftKey]\[KeyBar]k",

"L" -> "\[ShiftKey]\[KeyBar]l",

":" -> "\[ShiftKey]\[KeyBar];",

"\"" -> "\[ShiftKey]\[KeyBar]'",

"Z" -> "\[ShiftKey]\[KeyBar]z",

"X" -> "\[ShiftKey]\[KeyBar]x",

"C" -> "\[ShiftKey]\[KeyBar]c",

"V" -> "\[ShiftKey]\[KeyBar]v",

"B" -> "\[ShiftKey]\[KeyBar]b",

"N" -> "\[ShiftKey]\[KeyBar]n",

"M" -> "\[ShiftKey]\[KeyBar]m",

"<" -> "\[ShiftKey]\[KeyBar],",

">" -> "\[ShiftKey]\[KeyBar].",

"?" -> "\[ShiftKey]\[KeyBar]/"
}

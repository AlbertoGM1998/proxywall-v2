(* ::Package:: *)

(* This file defines the replacement rules for going from front end-defined keys in MenuSetup.tr, KeyEventTranslations.tr, etc. and
EventHandler and CurrentValue to RobotTools-recognized characters *)

{
"Alt" -> "\[AltKey]",

"AltKey" -> "\[AltKey]",

"Command" -> "\[CommandKey]",

"CommandKey" -> "\[CommandKey]",

"Ctrl" -> "\[ControlKey]",

"Control" -> "\[ControlKey]",

"ControlKey" -> "\[ControlKey]",

"Option" -> "\[OptionKey]",

"OptionKey" -> "\[OptionKey]",

"Shift" -> "\[ShiftKey]",

"ShiftKey" -> "\[ShiftKey]",

"Enter" -> "\[EnterKey]",

"Return" -> "\[ReturnKey]",

"ReturnKey" -> "\[ReturnKey]",

"EscapeKey" -> "\[EscapeKey]",

"Right" -> "\\[RightKey]",

"RightArrowKey" -> "\\[RightKey]",

"Left" -> "\\[LeftKey]",

"LeftArrowKey" -> "\\[LeftKey]",

"Up" -> "\\[UpKey]",

"UpArrowKey" -> "\\[UpKey]",

"Down" -> "\\[DownKey]",

"DownArrowKey" -> "\\[DownKey]",

"Delete" -> "\[DeleteKey]",

"Backspace" -> "\\[BackspaceKey]",

"End" -> "\\[EndKey]",

"Home" -> "\\[HomeKey]",

"F1" -> "\\[F1Key]",

"F2" -> "\\[F2Key]",

"F3" -> "\\[F3Key]",

"F4" -> "\\[F4Key]",

"F5" -> "\\[F5Key]",

"F6" -> "\\[F6Key]",

"F7" -> "\\[F7Key]",

"F8" -> "\\[F8Key]",

"F9" -> "\\[F9Key]",

"F10" -> "\\[F10Key]",

"F11" -> "\\[F11Key]",

"F12" -> "\\[F12Key]",

"F13" -> "\\[F13Key]",

"F14" -> "\\[F14Key]",

"F15" -> "\\[F15Key]",

"F16" -> "\\[F16Key]",

"F17" -> "\\[F17Key]",

"F18" -> "\\[F18Key]",

"F19" -> "\\[F19Key]",

"F20" -> "\\[F20Key]",

"F21" -> "\\[F21Key]",

"F22" -> "\\[F22Key]",

"F23" -> "\\[F23Key]",

"F24" -> "\\[F24Key]"
}

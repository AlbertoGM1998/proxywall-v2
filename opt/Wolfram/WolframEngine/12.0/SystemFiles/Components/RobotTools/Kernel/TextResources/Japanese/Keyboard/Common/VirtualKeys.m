(* ::Package:: *)

(*
VirtualKeys.m is a list of rules that specify the actual, underlying keys
that a user would type on an English keyboard to get the desired character
*)

{

"\"" -> "\[ShiftKey]\[LeftModified]2\[RightModified]",

"'" -> "\[ShiftKey]\[LeftModified]7\[RightModified]",

"&" -> "\[ShiftKey]\[LeftModified]6\[RightModified]",

"(" -> "\[ShiftKey]\[LeftModified]8\[RightModified]",

")" -> "\[ShiftKey]\[LeftModified]9\[RightModified]",

"=" -> "\[ShiftKey]\[LeftModified]-\[RightModified]",

"_" -> "-",

"~" -> "\[ShiftKey]\[LeftModified]=\[RightModified]",

"^" -> "=",

"`" -> "\[ShiftKey]\[LeftModified][\[RightModified]",

"@" -> "[",

"{" -> "\[ShiftKey]\[LeftModified]]\[RightModified]",

"[" -> "]",

"]" -> "\\",

"}" -> ";",

"+" -> "\[ShiftKey]\[LeftModified];\[RightModified]",

":" -> "'",

"*" -> "\[ShiftKey]\[LeftModified]'\[RightModified]"
}

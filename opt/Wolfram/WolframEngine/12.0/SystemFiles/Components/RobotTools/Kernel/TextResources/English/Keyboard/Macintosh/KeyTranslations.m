(* ::Package:: *)

{
(* Macintosh-specific diacritical characters *)

"\.b4" -> "\[OptionKey]\[KeyBar]e ",

"\[AAcute]" -> "\[OptionKey]\[KeyBar]ea",

"\[CapitalAAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]a",

"\[EAcute]" -> "\[OptionKey]\[KeyBar]ee",

"\[CapitalEAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]e",

"\[IAcute]" -> "\[OptionKey]\[KeyBar]ei",

"\[CapitalIAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]i",

"\[OAcute]" -> "\[OptionKey]\[KeyBar]eo",

"\[CapitalOAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]o",

"\[UAcute]" -> "\[OptionKey]\[KeyBar]eu",

"\[CapitalUAcute]" -> "\[OptionKey]\[KeyBar]e\[ShiftKey]\[KeyBar]u",

(*grave accent mark*)
(*"`" -> "\[ShiftKey]\[LeftModified] ", DeadKey -> "\[ShiftKey]\[LeftModified]`", {"\[OptionKey]"}]]*)

"\[AGrave]" -> "\[OptionKey]\[KeyBar]`a",

"\[CapitalAGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]a",

"\[EGrave]" -> "\[OptionKey]\[KeyBar]`e",

"\[CapitalEGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]e",

"\[IGrave]" -> "\[OptionKey]\[KeyBar]`i",

"\[CapitalIGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]i",

"\[OGrave]" -> "\[OptionKey]\[KeyBar]`o",

"\[CapitalOGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]o",

"\[UGrave]" -> "\[OptionKey]\[KeyBar]`u",

"\[CapitalUGrave]" -> "\[OptionKey]\[KeyBar]`\[ShiftKey]\[KeyBar]u",

(*double dot accent mark*)
"\.a8" -> "\[OptionKey]\[KeyBar]u ",

"\[ADoubleDot]" -> "\[OptionKey]\[KeyBar]ua",

"\[CapitalADoubleDot]" -> "\[OptionKey]\[KeyBar]u\[ShiftKey]\[KeyBar]a",

"\[EDoubleDot]" -> "\[OptionKey]\[KeyBar]ue",

"\[CapitalEDoubleDot]" -> "\[OptionKey]\[KeyBar]u\[ShiftKey]\[KeyBar]e",

"\[IDoubleDot]" -> "\[OptionKey]\[KeyBar]ui",

"\[CapitalIDoubleDot]" -> "\[OptionKey]\[KeyBar]u\[ShiftKey]\[KeyBar]i",

"\[ODoubleDot]" -> "\[OptionKey]\[KeyBar]uo",

"\[CapitalODoubleDot]" -> "\[OptionKey]\[KeyBar]u\[ShiftKey]\[KeyBar]o",

"\[UDoubleDot]" -> "\[OptionKey]\[KeyBar]uu",

"\[CapitalUDoubleDot]" -> "\[OptionKey]\[KeyBar]u\[ShiftKey]\[KeyBar]u",

(*hat accent mark*)
"\:02c6" -> "\[OptionKey]\[KeyBar]i ",

"\[AHat]" -> "\[OptionKey]\[KeyBar]ia",

"\[CapitalAHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]a",

"\[EHat]" -> "\[OptionKey]\[KeyBar]ie",

"\[CapitalEHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]e",

"\[IHat]" -> "\[OptionKey]\[KeyBar]ii",

"\[CapitalIHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]i",

"\[OHat]" -> "\[OptionKey]\[KeyBar]io",

"\[CapitalOHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]o",

"\[UHat]" -> "\[OptionKey]\[KeyBar]iu",

"\[CapitalUHat]" -> "\[OptionKey]\[KeyBar]i\[ShiftKey]\[KeyBar]u",

(*tilde accent mark*)
"\:02dc" -> "\[OptionKey]\[KeyBar]n ",

"\[ATilde]" -> "\[OptionKey]\[KeyBar]na",

"\[CapitalATilde]" -> "\[OptionKey]\[KeyBar]n\[ShiftKey]\[KeyBar]a",

"\[OTilde]" -> "\[OptionKey]\[KeyBar]no",

"\[CapitalOTilde]" -> "\[OptionKey]\[KeyBar]n\[ShiftKey]\[KeyBar]o"
}

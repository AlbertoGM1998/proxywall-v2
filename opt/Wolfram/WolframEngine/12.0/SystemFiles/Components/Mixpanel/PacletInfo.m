(* Paclet Info File *)

(* created 2017/07/18*)

Paclet[
    Name -> "ServiceConnection_Mixpanel",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Mixpanel`", "MixpanelFunctions`", "MixpanelLoad`"}
            }
        }
]



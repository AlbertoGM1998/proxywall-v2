(* Paclet Info File *)

(* created 2017/03/01*)

Paclet[
    Name -> "ServiceConnection_GoogleAnalytics",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"GoogleAnalytics`", "GoogleAnalyticsLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/GoogleAnalytics", Language -> All}
        }
]



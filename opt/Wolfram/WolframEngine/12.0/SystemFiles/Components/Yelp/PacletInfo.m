(* Paclet Info File *)

(* created 2016/05/26*)

Paclet[
    Name -> "ServiceConnection_Yelp",
    Version -> "12.0.80",
    MathematicaVersion -> "10.4+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Yelp`", "YelpFunctions`", "YelpLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Yelp", Language -> All}
        }
]



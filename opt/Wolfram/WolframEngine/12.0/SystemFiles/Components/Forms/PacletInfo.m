(* Paclet Info File *)

(* created 2014/05/06*)

Paclet[
    Name -> "Forms",
    Version -> "3.1",
    MathematicaVersion -> "10.1+",
    Description -> "Forms validation",
    Creator -> "Riccardo Di Virgilio <riccardod@wolfram.com>, Carlo Barbieri <carlob@wolfram.com>",
    Loading -> Automatic,
    Extensions -> {
        {"Kernel",
            HiddenImport -> True,
            Context -> {"Forms`"},
            Symbols -> {
                    "System`FormObject",
                    "System`FormTheme",
                    "System`PageTheme",
                    "System`FormFunction",
                    "System`FormPage",
                    "System`FormLayoutFunction",
                    "System`AppearanceRules",
                    "System`APIFunction",
                    "System`AutoSubmitting",
                    "System`AskFunction",
                    "System`Ask",
                    "System`AskAppend",
                    "System`AskConfirm",
                    "System`AskedValue",
                    "System`AskedQ",
                    "System`AskDisplay",
                    "System`AskTemplateDisplay",
                    "System`AskState",
                    "System`FormControl",
                    "System`AllowedCloudParameterExtensions",
                    "System`AllowedCloudExtraParameters"
                }
            }
        }
]



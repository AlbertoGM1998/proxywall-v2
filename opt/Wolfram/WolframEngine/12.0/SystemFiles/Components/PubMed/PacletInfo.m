(* Paclet Info File *)

(* created 2015/12/29*)

Paclet[
    Name -> "ServiceConnection_PubMed",
    Version -> "12.0.80",
    MathematicaVersion -> "10.4+",
    Extensions ->
        {
            {"Kernel", Root -> "Kernel", Context ->
                {"PubMed`", "PubMedLoad`"}
            },
            {"Documentation", MainPage -> "ReferencePages/Symbols/PubMed", Language -> All}
        }
]

Paclet[
    Name -> "TypeFramework",
    Version -> "0.9",
    WolframVersion -> "12+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> ".", Context -> {"TypeFramework`"}}
    }
]
(* Paclet Info File *)

(* created 2017/03/08*)

Paclet[
    Name -> "ServiceConnection_GoogleCalendar",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"GoogleCalendar`", "GoogleCalendarLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/GoogleCalendar", Language -> All}
        }
]



Paclet[
    Name -> "LLVMTools",
    Version -> "1.0.0.1",
    WolframVersion -> "12+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Root -> "Kernel", Context -> {"LLVMTools`"}}
    }
]

(* Paclet Info File *)

(* created 2017/03/08*)

Paclet[
    Name -> "ServiceConnection_Reddit",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Reddit`", "RedditLoad`", "RedditFunctions`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Reddit", Language -> All}
        }
]



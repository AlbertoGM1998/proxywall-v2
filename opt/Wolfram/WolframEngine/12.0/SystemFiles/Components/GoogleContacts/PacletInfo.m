(* Paclet Info File *)

(* created 2016/12/12*)

Paclet[
    Name -> "ServiceConnection_GoogleContacts",
    Version -> "12.0.80",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"GoogleContacts`", "GoogleContactsLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/GoogleContacts", Language -> All}
        }
]



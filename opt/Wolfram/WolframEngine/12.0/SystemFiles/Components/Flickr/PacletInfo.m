(* Paclet Info File *)

(* created 2017/02/27*)

Paclet[
    Name -> "ServiceConnection_Flickr",
    Version -> "11.1.8",
    MathematicaVersion -> "11.0+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"Flickr`", "FlickrLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/Flickr", Language -> All}
        }
]



(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      1473,         53]
NotebookOptionsPosition[      1004,         34]
NotebookOutlinePosition[      1577,         55]
CellTagsIndexPosition[      1534,         52]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"x", "^", Cell[BoxData[
     FormBox[
      TemplateBox[{"\"n\"","a","Named",BoxData},
       "NotebookTemplateSlot"], TextForm]]]}], ",", "x"}], "]"}]], "Input",
 CellFrameLabels->{{
    Cell[
     BoxData[
      TemplateBox[{"EvaluateDeleteInput"}, "NotebookTemplateCellBehavior"]]], 
    None}, {None, None}},
 CellChangeTimes->{{3.647023866141151*^9, 3.647023908676367*^9}}]
},
WindowSize->{808, 689},
WindowMargins->{{316, Automatic}, {Automatic, 50}},
DockedCells->FEPrivate`FrontEndResource[
 "NotebookTemplatingExpressions", "AuthoringDockedCell"],
TaggingRules->{"NotebookTemplateVersion" -> 1., "NotebookTemplate" -> True},
CellContext->Notebook,
ShowCellTags->True,
FrontEndVersion->"10.2 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 6, \
2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 442, 12, 40, "Input"]
}
]
*)

(* End of internal cache information *)

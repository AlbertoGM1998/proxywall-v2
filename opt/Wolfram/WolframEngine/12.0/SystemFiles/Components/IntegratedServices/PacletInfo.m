(* Paclet Info File *)

(* created 2017/12/13*)

Paclet[
    Name -> "IntegratedServices",
    Version -> "11.3.20",
    MathematicaVersion -> "11.1+",
    Loading -> Automatic,
    Extensions -> 
        {
            {"Kernel", Symbols -> 
                {
                 "System`$ServiceCreditsAvailable",
                 "IntegratedServices`$IntegratedServicesBase",
                 "IntegratedServices`BillingURL",
                 "IntegratedServices`CreatePurchasingDialogs",
                 "IntegratedServices`CreatePhoneVerificationDialogs",
                 "IntegratedServices`CreateQuotaDialogs",
                 "IntegratedServices`CreateTOSDialogs",
                 "IntegratedServices`IntegratedServices",
                 "IntegratedServices`RemoteServiceExecute",
                 "IntegratedServices`ServiceCreditsAvailable",
                 "IntegratedServices`ServiceCreditsLearnMoreURL"
                },
                Root -> "Kernel",
                HiddenImport -> "IntegratedServices`",
                Context -> {"IntegratedServicesLoader`", "IntegratedServices`"}
            }, 
            {"FrontEnd"}
        }
]



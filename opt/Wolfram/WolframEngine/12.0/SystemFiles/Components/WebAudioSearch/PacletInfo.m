(* Paclet Info File *)

(* created 2018/09/30*)

Paclet[
	Name -> "WebAudioSearch",
	Version -> "12.0.0",
	WolframVersion -> "12.0+",
	Loading -> Automatic,
	Extensions ->
	{
		{"Kernel",
			Symbols -> {"System`WebAudioSearch", "Audio`WebAudioSearchInformation"},
			Root -> "Kernel",
			HiddenImport -> True,
			Context -> {"WebAudioSearchLoader`","WebAudioSearch`"}
		},
		{"Documentation", MainPage -> "ReferencePages/Symbols/WebAudioSearch", Language -> "English"}
	}
]

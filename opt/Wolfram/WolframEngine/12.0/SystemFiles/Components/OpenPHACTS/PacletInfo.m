(* Paclet Info File *)

(* created 2016/02/23*)

Paclet[
    Name -> "ServiceConnection_OpenPHACTS",
    Version -> "12.0.80",
    MathematicaVersion -> "10.4+",
    Extensions -> 
        {
            {"Kernel", Root -> "Kernel", Context -> 
                {"OpenPHACTS`", "OpenPHACTSLoad`"}
            }, 
            {"Documentation", MainPage -> "ReferencePages/Symbols/OpenPHACTS", Language -> All}
        }
]



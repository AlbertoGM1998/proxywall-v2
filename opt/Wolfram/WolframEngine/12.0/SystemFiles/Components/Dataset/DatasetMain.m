(* This file loaded by Get/Needs["Dataset`"]. It must load the package files by calling DatasetLoader.m,
   and also ensure that Dataset` context is on $ContextPath, which is not done by DatasetLoader.
*)

Off[Atom::shdw];

BeginPackage["Dataset`"];
EndPackage[];

Get["DatasetLoader`"];

On[Atom::shdw];
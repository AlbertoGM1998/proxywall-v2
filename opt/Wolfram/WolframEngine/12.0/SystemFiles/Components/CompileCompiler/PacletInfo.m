Paclet[
    Name -> "CompileCompiler",
    Version -> "0.9",
    WolframVersion -> "12+",
    Updating -> Automatic,
    Extensions -> {
        {"Kernel", Context -> {"CompileCompiler`"}}
    }
]

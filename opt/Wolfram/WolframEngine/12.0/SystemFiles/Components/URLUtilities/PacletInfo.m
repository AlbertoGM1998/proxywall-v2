Paclet[
	Name -> "URLUtilities",
	Version -> "1.1.1",
	MathematicaVersion -> "12+",
	Description -> "URL Library",
	Creator -> "Riccardo Di Virgilio <riccardod@wolfram.com>",
	Loading -> Automatic,
	Extensions -> {
		{"Kernel", Context -> {"URLUtilities`"}, Symbols -> {
			"System`URLBuild",
			"System`URLParse",
			"System`URLEncode",
			"System`URLDecode",
			"System`URLQueryDecode",
			"System`URLQueryEncode",
			"System`URLShorten",
			"System`URLExpand",
			"System`URLExecute",
			"System`URLDownload",
			"System`URLRead",
			"System`URLSubmit",
			"System`URLDownloadSubmit",
			"System`FollowRedirects",
			"System`VerifySecurityCertificates",
			"System`Authentication",
			"System`CookieFunction",
			"System`HandlerFunctionsKeys",
		    "URLUtilities`URLCorrect",
		    "URLUtilities`URLCopyFile"
		}}
	}
]

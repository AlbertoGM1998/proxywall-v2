(* ::Package:: *)

Paclet[
	Name -> "MXNetLink",
	Version -> "12.0.1",
	MathematicaVersion -> "12.0+",
	Description -> "Link to the MXNet deep learning framework",
	Loading -> Automatic,
	Updating -> Automatic,
	Creator -> "Sebastian Bodenstein <sebastianb@wolfram.com>, Taliesin Beynon <taliesinb@wolfram.com>",
	Extensions -> {
		{"Kernel", Context -> {"MXNetLink`"}, Symbols -> {
		}},
		{"LibraryLink"}
	}
]

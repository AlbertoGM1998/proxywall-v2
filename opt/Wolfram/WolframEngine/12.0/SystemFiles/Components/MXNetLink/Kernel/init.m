BeginPackage["MXNetLink`"]

Begin["MXNetLink`Bootstrap`Private`"]

(* Bug 374931:
   This code is only present in MXNetLink paclet updates (12.0.1 and later) for WL 12.0. It 
   detects if NeuralNetworks is version 12.0.8 or earlier. In this case, loading will fail 
   because NN will refuse to load MXNL. Even though NN 12.0.9 was pushed before MXNL 12.0.1, 
   MXNL might still find an outdated NN because NN is updated every 3 days with WolframAutoUpdate, 
   MXNL has Updating -> Automatic in PacletInfo.m, so it always checks for updates (so new users 
   can get MXNL 12.0.1 before NN 12.0.9). 
   In case NN is outdated, this code updates it and makes sure the new NN gets loaded. 
   It goes like this:
   - the user requests NN functionality, old (12.0.7 or 12.0.8) NN gets autoloaded
   - before loading any source file, NN gets MXNL which updates & loads itself to this version
   - here, MXNL calls PacletUpdate["NeuralNetworks"], which dowlnloads the new paclet and deletes
     any previously downloaded NN paclets (so 12.0.8, because 12.0.7 is in the layout)
   - control is back to old NN which actually loads its source files. If NN is 12.0.8, these files 
     don't even exist anymore. Either way, we don't want old NN to be loaded, because it will check 
     the version of this MXNL and refuse to load.
   - so here in MXNL we patch the code in Kernel/init.m of old NN so that it loads the source files 
     of the new NN we've just downloaded instead of its own source code. We do it by setting 
     NeuralNetworks`Bootstrap`Private`$basePath to the location of the new paclet. We also have to 
     prevent the old init.m to overwrite it by setting it as non-mutable with SetMutationHandler

    Bug 374931 also has another, unrelated problem: MXNL is loaded by NN (up to 12.0.9 included) in 
    a PreemptProtect, which permanently hangs the front end when MXNL tries to create the panel 
    displaying its own update. This means that, unless dynamic updating is disabled, both NN 12.0.7 and
    12.0.8 will hang the front end before even triggering the code below, and users will have to
    force-quit and lose their session. At the start of a new session, MXNL is already updated and this
    code will kick in, updating NN if needed (if the MXNL update didn't complete before the user 
    force-quits, the FE will hang again).
*)
If[
	Order[
		{12, 0, 9},
		FromDigits /@ StringSplit[
			Lookup[PacletManager`PacletInformation["NeuralNetworks"], "Version", "0.0.0"],
			"."
		]
	] < 0,
	PacletManager`PacletUpdate["NeuralNetworks"];
	NeuralNetworks`Bootstrap`Private`$basePath = Lookup[
		PacletManager`PacletInformation["NeuralNetworks"], 
		"Location", 
		FileNameJoin @ {$UserBaseDirectory, "Paclets", "Respository", "NeuralNetworks-12.0.10"}
	];
	Language`SetMutationHandler[
		NeuralNetworks`Bootstrap`Private`$basePath, 
		Function[Null, Null, HoldAll]
	];
];

PreemptProtect[

Needs["GeneralUtilities`"];

GeneralUtilities`ClearPacletExportedSymbols["MXNetLink"];

getSources[] := (
	Get @ FileNameJoin[{DirectoryName @ $InputFileName, "LibraryLink.m"}];
);

startLibrary[] := (
	If[FailureQ @ MXNetLink`Bootstrap`LoadLibraries[], Return @ $Failed];
	MXNetLink`Bootstrap`SetupNullHandles[];
	MXNetLink`Bootstrap`LoadOperators[];
	MXNetLink`Bootstrap`RunPostloadCode[];
);

loadMX[{defFile_, symFile_}] := (
	Get[defFile];
	Get[symFile];
	startLibrary[];
);

isWorking[] := Quiet[
	MXNetLink`NDArrayGetNormal @ MXNetLink`NDArrayCreate[{1., 2.}] === {1., 2.}
];

(* we split the definitions we will save into pure code definitions and then
definitions that involve having loaded MXNetLink. This way we can save the 
version of the definitions that is free of any unsavable LibraryLink state. *)

saveMX[{defFile_, symFile_}] := (
	DumpSave[defFile, "MXNetLink`"];
	If[FailureQ @ MXNetLink`Bootstrap`LoadLibraries[], Return @ $Failed];
	MXNetLink`Bootstrap`SetupNullHandles[];
	If[!isWorking[], ReturnFailed[]]; 
	MXNetLink`Bootstrap`LoadOperators[];
	DumpSave[symFile, "MXNet`"];
	MXNetLink`Bootstrap`RunPostloadCode[];
);

(* We don't want to save the mx if $MXNetLibraryPath/$MXNetLinkLibraryPath are set, 
   bug 375458. So instead of starting library + dumping the mx as in saveMX, we just 
   do startLibrary. Note that in most cases we can even avoid that and do nothing, 
   because library paths are usually set by LoadLibraries (so if they are set, we 
   know libs have already been loaded). But by doing that we would rule out the 
   possibility to set the paths manually before getting MXNetLink. *) 
res = GeneralUtilities`PacletLoadCached["MXNetLink", 
	getSources, 
	If[
		Or[
			StringQ[MXNetLink`PackageScope`$MXNetLibraryPath], 
			StringQ[MXNetLink`PackageScope`$MXNetLinkLibraryPath]
		],
		Function[startLibrary[]],
		saveMX
	],
	loadMX, 
	{"Definitions.mx", "Symbols.mx"}
]	

If[MatchQ[res, "Disabled"],
	getSources[];
	startLibrary[]	
]

(* end of PreemptProtect*)
];

End[]
EndPackage[]

(* Paclet Info File *)

(* created 2018/05/31*)

Paclet[
Name -> "PDEModels",
Version -> "1.0.1",
Description -> "Package provides Partial Differential Equation models.",
MathematicaVersion -> "12.0+",
Extensions -> 
	{
		{"Kernel", Context -> "PDEModels`"},
	
		{"Documentation", Root -> "Acoustics/Documentation"},
		{"Documentation", Root -> "PDEModels/Documentation", MainPage -> "Tutorials/PDEModelsOverview"}
	}
]



# ExternalEvaluate_Python

* ExternalEvaluate -- This registers and supports the Python system for ExternalEvaluate
* Resources -- Contains the initialization code that starts the Python instance, as well as tests

![Alt text](../README/flowchart/EE-flow-python.jpg?raw=true "Flowchart of Python Evaluations")
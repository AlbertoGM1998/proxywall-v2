# MUnit.nb

The default stylesheet for user-level testing notebooks.

# TestResult.nb

Stylesheet for older MUnit notebook interfaces. Used by the old-style logger `NotebookLogger`. (Modern notebook logger is `AssociationNBLogger`).